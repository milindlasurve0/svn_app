<?php
class GrouplistsFriendlist extends AppModel {
	var $name = 'GrouplistsFriendlist';	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Grouplist' => array(
			'className' => 'Grouplist',
			'foreignKey' => 'grouplists_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Friendlist' => array(
			'className' => 'Friendlist',
			'foreignKey' => 'friendlists_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>