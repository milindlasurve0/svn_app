<?php
class AppsLog extends AppModel {
	var $name = 'AppsLog';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'App' => array(
			'className' => 'App',
			'foreignKey' => 'app_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AppsTransaction' => array(
			'className' => 'AppsTransaction',
			'foreignKey' => 'transaction_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>