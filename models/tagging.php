<?php
class Tagging extends AppModel {
	var $name = 'Tagging';
	
	var $hasMany = array(
		'UserTagging' => array(
			'className' => 'UserTagging',
			'foreignKey' => 'tagging_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
?>