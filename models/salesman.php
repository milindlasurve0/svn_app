<?php
class Salesman extends AppModel {
	var $name = 'Salesman';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Retailer' => array(
			'className' => 'Retailer',
			'foreignKey' => 'salesman_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)	
	);
	
}
?>