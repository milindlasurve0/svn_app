<?php
class Grouplist extends AppModel {
	var $name = 'Grouplist';
	var $hasAndBelongsToMany = array(
		'Friendlist' => array(
			'className' => 'Friendlist',
			'joinTable' => 'grouplists_friendlists',
			'foreignKey' => 'grouplists_id',
			'associationForeignKey' => 'friendlists_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'nickname',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
?>