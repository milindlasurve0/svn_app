<?php
class UserTagging extends AppModel {
	var $name = 'UserTagging';
	
	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tagging' => array(
			'className' => 'Tagging',
			'foreignKey' => 'tagging_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>