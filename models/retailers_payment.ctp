<?php
class RetailersPayment extends AppModel {
	var $name = 'RetailersPayment';	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Retailer' => array(
			'className' => 'Retailer',
			'foreignKey' => 'retailer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>