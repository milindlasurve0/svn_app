<?php
class Retailer extends AppModel {
	var $name = 'Retailer';

	
	
	var $hasAndBelongsToMany = array(
		'Coupon' => array(
			'className' => 'Coupon',
			'joinTable' => 'retailers_coupons',
			'foreignKey' => 'retailer_id',
			'associationForeignKey' => 'coupon_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
?>