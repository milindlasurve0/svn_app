<?php
class Category extends AppModel {
	var $name = 'Category';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'parent' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Message' => array(
			'className' => 'Message',
			'foreignKey' => 'category_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => 'rating desc',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Category' => array(
			'foreignKey' => 'parent',
			'className' => 'Category',
			'conditions' => 'Category.toshow = 1',
			'order' => 'id asc'
		)
	);


	var $hasAndBelongsToMany = array(
		'Package' => array(
			'className' => 'Package',
			'joinTable' => 'categories_packages',
			'foreignKey' => 'category_id',
			'associationForeignKey' => 'package_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
	
	function filterBindings($bindings = null) {
        if (empty($bindings) && !is_array($bindings)) {
            return false;
        }
        $relations = array('hasOne', 'hasMany', 'belongsTo', 'hasAndBelongsToMany');
        $unbind = array();
        foreach ($bindings as $binding) {
            foreach ($relations as $relation) {
                if (isset($this->$relation)) {
                    $currentRelation = $this->$relation;
                    if (isset($currentRelation) && isset($currentRelation[$binding])) {
                        $unbind[$relation][] = $binding;
                    }
                }
            }
        }
        if (!empty($unbind)) {
            $this->unbindModel($unbind);
        }
    }

}
?>