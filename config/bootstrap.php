<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php
 *
 * This is an application wide file to load any function that is not used within a class
 * define. You can also use this to include or require any files in your application.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * App::build(array(
 *     'plugins' => array('/full/path/to/plugins/', '/next/full/path/to/plugins/'),
 *     'models' =>  array('/full/path/to/models/', '/next/full/path/to/models/'),
 *     'views' => array('/full/path/to/views/', '/next/full/path/to/views/'),
 *     'controllers' => array('/full/path/to/controllers/', '/next/full/path/to/controllers/'),
 *     'datasources' => array('/full/path/to/datasources/', '/next/full/path/to/datasources/'),
 *     'behaviors' => array('/full/path/to/behaviors/', '/next/full/path/to/behaviors/'),
 *     'components' => array('/full/path/to/components/', '/next/full/path/to/components/'),
 *     'helpers' => array('/full/path/to/helpers/', '/next/full/path/to/helpers/'),
 *     'vendors' => array('/full/path/to/vendors/', '/next/full/path/to/vendors/'),
 *     'shells' => array('/full/path/to/shells/', '/next/full/path/to/shells/'),
 *     'locales' => array('/full/path/to/locale/', '/next/full/path/to/locale/')
 * ));
 *
 */
putenv('TZ=Asia/Calcutta');
define('encKey','PuTyOuRK3yHeReeswrwerwr');
define('SITE_NAME','http://www.atadka.com/');
define('SENDFLAG','1');
define('SCRIPTFLAG','2');
//define('SITE_NAME','http://www.ntadka.com/');
define('SITE_NAME_SEC','https://www.smstadka.com/');
define('SERVER_BACKUP','http://www.atadka.com/');
define('SERVER_PROTECTED','http://54.235.195.140/');
//define('USSD_SERVER','http://107.22.174.199/');
define('MAIN_SERVER','http://www.atadka.com/');
define('CODE_DEFINITION','You can also subscribe to this package by sending SMS as package-code to 09223178889');

/* In REDIS implementation*/
define('LOG_PATH','/var/www/myproject/app/tmp/');
//define('SMSAPIURL','http://smsapi.24x7sms.com/api_1.0/SendSMSXML.aspx');
define('SMSAPIURL','http://www.atadka.com/redis/SendSMSXML');
define('REDIS_HOST','127.0.0.1');
define('REDIS_PASSWORD','$avail$p@y!');
define('REDIS_PORT','6300');

//sms char limit
define('SMS_CHAR_LIMIT','140');
define('FREE_SMS_DAY_LIMIT','20');
define('FREE_SIGN','via smstadka.com');
define('MONEY_BACK_DAYS',7);
define('MAX_PACKAGE_TRIALS',5);
define('DUMMY_USER','02261512231');
define('VIRTUAL_NUMBER','9223178889');

define('MSG_DATE','2011-06-30 00:00:00');
define('MSG_DURATION','90');
/* Transaction Types*/
define('TRANS_USER_CREDIT','1'); //recharge by user
define('TRANS_ADMIN_DEBIT','2'); //debited on package subscription or message sending
define('TRANS_ADMIN_FREE_CREDIT','3'); //free credit by admin
define('TRANS_ADMIN_UNSCR_CREDIT','4'); //credit to user on unsubscription of package
define('TRANS_RETAIL','5'); //credit to user via retail channel


define('FREE_LOGIN_CREDIT','0'); //free credit to user on login
define('DEFAULT_MESSAGE_LENGTH',160);
define('ADSPACE',5);
define('EACH_MESSAGE_COST',10);

/* User Registration Types */
define('ONLINE_REG',1);
define('MISSCALL_REG',2);
define('RETAILER_REG',3);
define('REF_CODE_REG',4);
define('ONLINE_RETAILER_REG',5);
define('FORCE_RETAILER_REG',6);

/* Automated Packages*/
define('AUTOMATED_PACKS','75,76,26,30');
/* Money Back Packages*/
//define('MONEY_BACK_PACKS','24,25,26,27,28,29,30,31,32,75,76,77,79,80,81,82,83,67,68,69,52,53,54,55,56,57,58,59,60,61,62,63,64,65,135,138,78,142');
define('MONEY_BACK_PACKS','');
/*DONTS*/
define('DONT_SHOW_MSG','142');
define('DONT_SHOW_RECENT','58,61,62,63,142,155,156,157,158,159');
define('MUMBAI_NEWS','151');

/* DND Changes */
define('DND_FLAG',1);
define('TRANS_FLAG',1);
define('TIME_SLOT_START',915);
define('TIME_SLOT_END',2045);
define('TIME_SLOT_PERIOD','between 09:15 AM to 08:45 PM');
define('TIME_SLOT_ERR','You can send messages between 09:15 AM to 08:45 PM only.');
define('TIME_SLOT_ERR_REM','You can set message between 09:30 AM to 08:30 PM only.');
define('TIME_SLOT_ERR_NW','You can set message between 09:00 AM to 08:30 PM only.');

/* CRon Passwords*/
define('CRON_USERNAME','admin');
define('CRON_PASSWORD','5m5cr0n');

define('MOBILE_URL_PASSWORD','s1tadka');

/* Twitter details*/
define('BITLY_USER','smstadka'); 
define('BITLY_KEY','R_1740de7d233b01239b119b0f3891d521');
define('TWITTER_TEXT','To get funny SMSes, jokes, shayaris click here');
define('TWITTER_USERNAME','realtadka');
define('TWITTER_PASSWORD','asdf1234');


/* REF CODE*/
define('SHARER_CREDIT',2); 
define('REDEEM_AMT',5);

/* Version Numbers */
define('STYLE_CSS_VERSION','998');
define('STYLE_CSS_IE_VERSION','998');
define('M_STYLE_CSS_VERSION','980');
define('RETAIL_STYLE_CSS_VERSION','980');
define('SCRIPT_APP_JS_VERSION','997'); //script & app
define('MERGE_JS_VERSION','980'); //prototype, effects, carousel, dpEncodeRequest
define('MERGE_1_JS_VERSION','980'); //scriptaculous, calendar, controls
define('NETWORK_JS_VERSION','1'); //networks

/* File Types */
define('JS_TYPE','js');
define('CSS_TYPE','css');

/* Twitter Auth*/ //for dev.smstadka.com
//define('TWITTER_CONSUMER_KEY','D7CCtTh48T5v4qfkjyvQ'); 
//define('TWITTER_CONSUMER_SECRET','LMljbujFe4L6sJfAsDPOSjqSm6ls515rlF51rUVLqs'); 
//define('TWITTER_OAUTH_TOKEN','167478569-Pfh2V2YFYtQDBTrzZAzhUTgdniERqT9FNGQfDDGF');
//define('TWITTER_OAUTH_TOKEN_SECRET','wfI9CzVlbauajSyLkm3iWltYvIxGJeKQ0GSRNQ7iQ');

/* Twitter Auth*/ //for smstadka.com
//define('TWITTER_CONSUMER_KEY','zPdbgshxCciopGvfs2aKg'); 
//define('TWITTER_CONSUMER_SECRET','xJXKv9B9JrXhhcrhiy019Cscr9gvS70WwV1Ha7kpS2s');
//define('TWITTER_OAUTH_TOKEN','167478569-XyVJAktRBrecJBSAEj6gRkhG2j9VrVlFmfOnLOFN');
//define('TWITTER_OAUTH_TOKEN_SECRET','Z8LSLY9aATgWtiLu33S7pDND1VNnRCj2okX16PMM');
/* Categories array*/
define('CATEGORIES_IN_ORDER','32,3,34,61,36,35,37,38');
define('APPS_IN_ORDER','2,4,1');

/* Packages array*/
define('POPULAR_ARRAY','34,153,161,140,22,41,26,70,67,14,35,71,19,46,56,37');
//define('PICKING_ARRAY','68,70,19,54,75,67,74,39,26,4,73,21');
define('LATEST_ARRAY','73,38,55,18,42,54,43,65,50,60');
define('RECOM_ARRAY','39,23,52,26,69,37,19,70,12,41,56,72');
define('RECOMMENDED','34,153,140,161,22,41,14,140,142,147,26,70,67,56');

define('DASH_NEWS','140,15,16,17,18,21,71,75,76,77,78,79,80,81,82,83');
define('DASH_BOLLY','22,23,24,25,26,27,28,29,30,31,32');
define('DASH_TWEET','52,53,54,55,56,57,58,59,60,61,62,63,64,65,135,138');
define('DASH_CRICKET','19,20');
define('DASH_MARKET','70,72,73,74,84,85,86,87,88,89,98,99,100,102,103,104,105,106,107,108,109,110,111,112,113,114,115, 116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134');
define('DASH_TIPS','67,68,69');
define('DASH_QRY_LMT','10');

define('FUN_ID',32);
define('FUN_CAT','1,10,17,18');
define('PACK_LIMIT_L','8'); //for linear layout
define('PACK_LIMIT_G','18'); //for grid layout
define('FREQUENCY_DEFAULT','As it happens');


/* freesmsapi details */
define('FREESMSAPI_KEY_ASHISH','7f23bf3a8ce657ab9c24959b17b6b6a3'); 
define('FREESMSAPI_KEY_ANURAG','5364d954a33628cb983425c3c2173a89'); 

/* smsachariya  api details */
define('SMS_USER','anurag'); 
define('SMS_PASS','a12anurag');
define('SMS_SENDER','SMSTDK');

/* 24X7SMS api details */
define('SMS_USER_24X7SMS','SMSTDK');
define('SMS_PASS_24X7SMS','smstdk124');

/* database connection */
define('DB_HOST','localhost'); 
define('DB_USER','root');
define('DB_PASS','root');
define('DB_DB','smstadka'); 

/*ICC payment gateway details*/
define('ICC_MER_KEY','SMSTadka'); 
define('CLIENT_CODE','SMS457TAD');

/*OSS payment gateway details*/
define('OSS_MER_ID',2560); 
define('OSS_MER_CODE','MR00002560');
define('OSS_MER_NAME','MindsArray'); 
define('OSS_MER_AUTH_KEY','OSS_MR00002560_Minds77');
define('OSS_TRAN_TYPE_ID','2581'); 
define('OSS_TRAN_AUTH_KEY','OSS_MR00002560_Minds77_DR');
define('OSS_PROD_ID',255);
 

/* app reminder */
define('APP_REM_REC',5);
//define('APP_REM_REC_CHARGE',0.15);
define('APP_REM_MSG_LMT',290);
define('APP_REM_DEF_CNT',5);
define('APP_REM_MSG_FIXED',0);

/* app stock */
define('APP_STOCK_PRICE',0.50);
define('APP_STOCK_PRICE_WITH_NEWS',1);
define('MIN_TITLE_LENGTH',20);
define('MIN_WORD_COUNT',5);
define('MAX_STOCK_MSG_LENGTH',280); //TRAI changes
//define('MAX_STOCK_MSG_LENGTH',270);

/* Email Auth*/
define('EMAIL_ID','no-reply@mindsarray.com');
define('FROM_NAME','SMSTadka');
define('PASSWORD','mindsarray');

define('MARSH_EMAIL_ID','marshebs@gmail.com');
define('MARSH_PASSWORD','marsh123');

/* Cyclic Packages */
define('CYCLIC_LENGTH','32');
define('TRIAL_CYCLIC_LENGTH','3');
define('TRIAL_DAYS','2');
define('TRIAL_CODES','FUN,LOVE,CRICKET,BLYWOOD,VIDEO,NEWS,STOCK,FUN18,BIBLE,FUNPLAY,CRICPLAY');

/* Outside Products */
define('PNR_PRODUCT','10');
define('RECHARGE_CARD_50','11');
define('PLAYWIN_PKGS','12,13');
define('JOKES_PLUS','12');
define('CRICKET_PLUS','13');
define('PNR_HAPPY_JOURNEY','14');
define('PNR_SIGNATURE','15');
define('JOKES_PLUS_FUN','34');
define('JOKES_PLUS_SHAYARI','41');
define('PRODUCTS_ORDER','10,17,2,29,30,8,1,32,9,26,27,6,31,28,4,9,11,3');
define('PNR_PRODUCTS','10,14,15');
define('WOI_PRODUCT','17');

define('NO_REF_CODES','17');


define('SIGNATURE_LIMIT','60');

define('default_passwd','7de444954a802ec11b4cbb3bda06b85f8f1e0683'); //asdf1234


//gupshup settings
define('GS_API','http://enterprise.smsgupshup.com/GatewayAPI/rest?');
define('GS_UID','2000070568');
define('GS_PASSWD','s1tadka');
define('GS_API_LOW','http://enterprise.smsgupshup.com/GatewayAPI/rest?');
define('GS_UID_LOW','2000053789');
define('GS_PASSWD_LOW','MuHUwnsXB');
define('GS_UID_TADKA','2000053562');
define('GS_PASSWD_TADKA','s1tadka');
define('GS_VER','1.1');
define('GS_MSG_TYP','TEXT');
define('GS_AUTH_SCM','PLAIN');
define('GS_MASK','SMSTADKA');
define('GS_METHOD','sendMessage');
define('GS_NO_LIMIT','20');


define('USER_SMSLANE','aryaashish');
define('PASS_SMSLANE','a1ashish');
//Retailer Panel
define('MEMBER','1');
define('ADMIN','2');
define('SUPER_DISTRIBUTOR','4');
define('DISTRIBUTOR','5');
define('RETAILER','6');

//Receipt Type
define('RECEIPT_INVOICE','1');
define('RECEIPT_TOPUP','2');

define('NUM_PRODUCTS','15');

//Retailer Transaction Types
define('ADMIN_TRANSFER','0');//ref1 is admin_id & ref2 is super distributor id
define('SDIST_DIST_BALANCE_TRANSFER','1');//ref1 is superdistributor id and ref2 is distributor id
define('DIST_RETL_BALANCE_TRANSFER','2');//ref1 is distributor id and ref2 is retailer id
define('DISTRIBUTOR_ACTIVATION','3');//ref1 is distributor id and ref2 is retailersCouponids seperated by commas
define('RETAILER_ACTIVATION','4');//ref1 is retailer id and ref2 is product id
define('COMMISSION_SUPERDISTRIBUTOR','5');//ref1 is superdistributor id and ref2 is parent id
define('COMMISSION_DISTRIBUTOR','6');//ref1 is distributor id and ref2 is parent id
define('COMMISSION_RETAILER','7');//ref1 is retailer id and ref2 is parent id
define('TDS_SUPERDISTRIBUTOR','8');//ref1 is superdistributor id and ref2 is parent id
define('TDS_DISTRIBUTOR','9');//ref1 is distributor id and ref2 is parent id
define('TDS_RETAILER','10');//ref1 is retailer id and ref2 is parent id
define('REVERSAL_CARD_ACTIVATION','11');//ref1 is distributor id and ref2 is product id
define('REVERSAL_COMMISSION_SUPERDISTRIBUTOR','12');//ref1 is superdistributor id and ref2 is parent id
define('REVERSAL_COMMISSION_DISTRIBUTOR','13');//ref1 is distributor id and ref2 is parent id
define('REVERSAL_TDS_SUPERDISTRIBUTOR','14');//ref1 is superdistributor id and ref2 is parent id
define('REVERSAL_TDS_DISTRIBUTOR','15');//ref1 is distributor id and ref2 is parent id
define('DEBIT_NOTE','16');//ref1 is distributor id and ref2 is parent id
define('CREDIT_NOTE','17');//ref1 is distributor id and ref2 is parent id


//Calculation Constants
define('TDS_PERCENT','10');
define('SERVICE_TAX_PERCENT','10.3');

define('SMSTADKA_COMPANY','Mindsarray Technologies Pvt. Ltd.');
define('SMSTADKA_ADDRESS','A - 108, Kaveri, 1st Floor,
Chincholi Bunder Rd, Malad (W),
Mumbai - 400064, Maharashtra.');
define('SMSTADKA_CONTACT','+919769597418');
//cool video path
define('BASE_DIR','/var/www/html/smstadka/app/webroot/vidupload/');


define('PARTNER_PLAYWIN',1);
define('PARTNER_WHATSONINDIA',2);

define('VENDOR_OSS',3);
define('VENDOR_TSS',5);
define('SCHEME_VENDORS','3,5');
define('DAILY_PRIZE_SALE_LIMIT',800);
define('WEEKLY_PRIZE_SALE_LIMIT',700);
define('DAILY_PRIZE_DAYS_WINDOW',7);
define('SCHEME_FLAG',0);

define('RETAILER_TAG',10);

define('EALERT_USERNAME','test');
define('EALERT_PASSWORD','test');
//define('EALERT_USERNAME','ealert');
//define('EALERT_PASSWORD','wjsd72sd34s6');

/**
 * As of 1.3, additional rules for the inflector are added below
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */