<?php
class AppController extends Controller {
		
	var $components = array('Acl', 'Auth', 'Session','General','RequestHandler');
	var $uses = array('Category','Package','SMSApp');
	
	var $categoryMapping = array();
	var $packageCategories = array();
	var $popularPacks = array();
	
	 /**
     * Overwrite redirect function for 403 header bug from auth component.
     * 
     * (non-PHPdoc)
     * @see cake/libs/controller/Controller#redirect($url, $status, $exit)
     */
	
	
	function redirect($url, $status = null, $exit = true) {
 
        if ($status == 403 && $this->RequestHandler->isAjax()) {
 			$this->throwAjaxError(403, __("$url", true));
            $this->_stop();
            return;
        }
        else {
            parent::redirect($url, $status = null, $exit = true);
        }
    }
    
	/**
	 * Return header codes for AJAX errors.
	 * 
	 * @param $errorCode
	 * @param $message
	 * @return unknown_type
	 */
	protected function throwAjaxError ($errorCode = 400, $message = null) {
 
		if ($this->RequestHandler->isAjax() || (isset($this->isAjax) && $this->isAjax == true)) {
			switch ($errorCode) {
				case 400 :
				case 403 :
				    $defaultMessage = 'The request could not be processed because access is forbidden.';
                    header("'HTTP/1.0 403 Forbidden", true, 403);
                    echo ($message == null)?$defaultMessage:$message;
                    break;
				case 408 :
				case 409 :
					$defaultMessage = 'The request could not be processed because of conflict in the request.';
					header("HTTP/1.0 409 Conflict", true, 409);
					echo ($message == null)?$defaultMessage:$message;
					break;
				case 500 : 
					break;
			}
			$this->autoRender = false;
			$this->layout = 'ajax'; 
			Configure::write('debug', 0);
		}
		else {
			throw new Exception('Ajax Error should only be thrown for ajax requests.');
		}
	}
	
	
	function beforeFilter() {
		//Configure AuthComponent
		$this->Auth->fields = array(
			'username' => 'mobile', 
			'password' => 'password'
		);
		
		$this->Auth->authorize = 'actions';
		//$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
		//$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'view');
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
		//$this->Auth->allow('*');
		$this->Auth->allowedActions = array('display');
		
		$this->Auth->actionPath = 'controllers/';
		App::import('vendor', 'md5Crypt', array('file' => 'md5Crypt.php'));
		$this->objMd5 = new Md5Crypt;
		
		
		$this->set('objGeneral',$this->General);
		$this->set('objMd5',$this->objMd5);
	
		
		if (!$this->RequestHandler->isAjax()){
			$this->Category->recursive = 1;
			$this->Category->filterBindings(array('Message','Package'));
			$jokecatData = $this->Category->find('all', array('conditions' => array('Category.id in (' . FUN_CAT . ')', 'Category.toshow' => '1'),'cache' => 'jokecatData'));
			//$jokecatData = $this->Category->find('all', array('conditions' => array('Category.parent' => FUN_ID, 'Category.toshow' => '1')));
			$this->set('catData', $jokecatData); //all SMS Jokes categories
		}
	
		if(($this->params['controller'] == 'apps' && $this->params['action'] == 'view') || ($this->params['controller'] == 'messages' && $this->params['action'] == 'facebook') || ($this->params['controller'] == 'tags' && $this->params['action'] == 'view') || ($this->params['controller'] == 'users' && $this->params['action'] == 'newDashboard') || ($this->params['controller'] == 'categories' && $this->params['action'] == 'view') || ($this->params['controller'] == 'messages' && $this->params['action'] == 'socialshare') || ($this->params['controller'] == 'messages' && $this->params['action'] == 'view') || ($this->params['controller'] == 'packages' && $this->params['action'] == 'view') || ($this->params['controller'] == 'retailers' && $this->params['action'] == 'all') || ($this->params['controller'] == 'retailers' && $this->params['action'] == 'locator') || ($this->params['controller'] == 'users' && $this->params['action'] == 'login')){
			$oCatData = $this->Category->find('all', array('conditions' => array('Category.parent' => '0','Category.toshow' => '1'),
			'order' => 'FIELD(id,'.CATEGORIES_IN_ORDER.')',
			'cache' => 'oCatData'
			));
			$this->set('ocatData', $oCatData); //other main categories
			//$this->packageCategories = $oCatData;
		}

		if(($this->params['controller'] == 'sitemaps') || ($this->params['controller'] == 'packages' && $this->params['action'] == 'view') || ($this->params['controller'] == 'groups' && $this->params['action'] == 'dataEntry')){
			$oCatData = $this->Category->find('all', array('conditions' => array('Category.parent' => '0','Category.toshow' => '1'),
			'order' => 'FIELD(id,'.CATEGORIES_IN_ORDER.')',
			'cache' => 'oCatData'
			));
			foreach($oCatData as $category){
				$categoryMapping[$category['Category']['id']] = $category['Category']['name'];
				$i = 0;
				while(!empty($category['Category'][$i])) {
					$categoryMapping[$category['Category'][$i]['id']] = $category['Category'][$i]['name'];
					$i++;
				}
			}		
			
			//$this->categoryMapping = $categoryMapping;
			$this->set('categoryMapping',$categoryMapping);
		}			
			
			$this->Package->recursive = 0;
			$packData = $this->Package->find('all',array('conditions' => array('Package.toshow' => 1),
			'cache' => 'packData'
			));
			$this->set('packData', $packData);

			if(($this->params['controller'] == 'tags' && $this->params['action'] == 'view') || ($this->params['controller'] == 'apps' && $this->params['action'] == 'view') || ($this->params['controller'] == 'apps' && $this->params['action'] == 'allApps') || ($this->params['controller'] == 'apps' && $this->params['action'] == 'getApps') || ($this->params['controller'] == 'users' && $this->params['action'] == 'newDashboard') || ($this->params['controller'] == 'categories' && $this->params['action'] == 'view') || ($this->params['controller'] == 'messages' && $this->params['action'] == 'socialshare') || ($this->params['controller'] == 'messages' && $this->params['action'] == 'view') || ($this->params['controller'] == 'packages' && $this->params['action'] == 'view') || ($this->params['controller'] == 'retailers' && $this->params['action'] == 'all') || ($this->params['controller'] == 'retailers' && $this->params['action'] == 'locator') || ($this->params['controller'] == 'users' && $this->params['action'] == 'login')){
				$this->SMSApp->recursive = -1;
				$apps = $this->SMSApp->find('all',array('order' => 'FIELD(id,'.APPS_IN_ORDER.')',
				'cache' => 'appsData'
				));
				$this->set('allApps',$apps);
			}
			
			
			if (!$this->RequestHandler->isAjax()){
				$this->popularPacks = $this->getDisplayPackages(POPULAR_ARRAY);
				$this->set('popularPacks',$this->popularPacks);
			}
			//$this->set('pickingPacks',$this->getDisplayPackages(PICKING_ARRAY));
			//$this->set('latestPacks',$this->getDisplayPackages(LATEST_ARRAY));
			
			if($this->params['controller'] == 'users' && $this->params['action'] == 'newDashboard')
			$this->set('recommendedPacks',$this->getDisplayPackages(RECOM_ARRAY));
			
			//$this->set('moneyBack',explode(',',MONEY_BACK_PACKS));
			//$this->Session->write('categoryMapping',$categoryMapping);
	}
	
	function printArray($txt){
		echo  '<pre>';
		print_r($txt);
		echo '</pre>';		
	}
	
	function getDisplayPackages($arr,$limit=null,$page=null){
		$this->Package->recursive = -1;
		
		$str = "1000";
		if($limit != null){
			$str = ($page-1)*$limit . "," . $limit;
		}
		
		$packs = $this->Package->find('all',array('conditions' => array('Package.id in('.$arr.')','Package.toshow' => 1),
		 'order' => 'FIELD(id,'.$arr.')',
		 'limit' => $str));
		
		return $packs;
	}
	
	function lock(){
		$name = $this->params['controller'] . "/" . $this->params['action'];
		$check = $this->Package->query("SELECT ready_flag FROM crontabs WHERE name='$name'");
		if(count($check) == 1){
			if($check['0']['crontabs']['ready_flag'] == 0){
				echo "This cron is locked now";
				exit;
			}
			else {
				$this->Package->query("UPDATE crontabs SET ready_flag = 0 WHERE name='$name'");
			}
		}
		else {
			$this->Package->query("INSERT into crontabs (name) VALUES ('$name')");
		}
	}
	
	function releaseLock(){
		$name = $this->params['controller'] . "/" . $this->params['action'];
		$this->Package->query("UPDATE crontabs SET ready_flag = 1 WHERE name='$name'");	
	}
}
?>