--
-- Table structure for table `recharge_log`
--

CREATE TABLE IF NOT EXISTS `recharge_log` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `trans_id` int(20) NOT NULL,
  `parent_trans_id` int(20) NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `mob_dth_no` varchar(50) NOT NULL COMMENT 'common field to contain mob no or DTH no',
  `type` int(2) NOT NULL COMMENT 'Mob->1 , DTH->2',
  `contact_no` int(20) NOT NULL,
  `err_code` varchar(5) DEFAULT NULL,
  `description` text,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `trans_id` (`trans_id`,`user_id`,`mob_dth_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
