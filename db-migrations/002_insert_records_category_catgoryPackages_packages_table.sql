

/* 

Note :-  change id according to the max id in category table 

*/

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Airtel', 'airtel-recharge', '10', 0, 'Prepaid Airtel Mobile Recharge ', 'Prepaid Airtel Mobile Recharge ', '2', 'Airtel-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'BSNL', 'bsnl-recharge', '10', 0, 'Prepaid BSNL Mobile Recharge ', 'Prepaid BSNL Mobile Recharge ', '2', 'BSNL-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Docomo', 'docomo-recharge', '10', 0, 'Prepaid Docomo Mobile Recharge ', 'Prepaid Docomo Mobile Recharge ', '2', 'Docomo-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Idea', 'idea-recharge', '10', 0, 'Prepaid Idea Mobile Recharge ', 'Prepaid Idea Mobile Recharge ', '2', 'Idea-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Loop', 'loop-recharge', '10', 0, 'Prepaid Loop Mobile Recharge ', 'Prepaid Loop Mobile Recharge ', '2', 'Loop-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'MTNL', 'mtnl-recharge', '10', 0, 'Prepaid MTNL Mobile Recharge ', 'Prepaid MTNL Mobile Recharge ', '2', 'MTNL-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Reliance-CDMA', 'reliance-cdma-recharge', '10', 0, 'Prepaid Reliance-CDMA Mobile Recharge ', 'Prepaid Reliance-CDMA Mobile Recharge ', '2', 'Rel-CDMA-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Reliance-GSM', 'reliance-gsm-recharge', '10', 0, 'Prepaid Reliance-GSM Mobile Recharge ', 'Prepaid Reliance-GSM Mobile Recharge ', '2', 'Rel-GSM-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Tata Indicom', 'tata-indicom-recharge', '10', 0, 'Prepaid Tata Indicom Mobile Recharge ', 'Prepaid Tata Indicom Mobile Recharge ', '2', 'Tata-Indicom-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Uninor', 'uninor-recharge', '10', 0, 'Prepaid Uninor Mobile Recharge ', 'Prepaid Uninor Mobile Recharge ', '2', 'Uninor-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Vodafone', 'vodafone-recharge', '10', 0, 'Prepaid Vodafone Mobile Recharge ', 'Prepaid Vodafone Mobile Recharge ', '2', 'Vodafone-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');


INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Aircel', 'aircel-recharge', '10', 0, 'Prepaid Aircel Mobile Recharge ', 'Prepaid Aircel Mobile Recharge ', '2', 'Aircel-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Uninor', 'mts-recharge', '10', 0, 'Prepaid MTS Mobile Recharge ', 'Prepaid MTS Mobile Recharge ', '2', 'MTS-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');

INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Videocon', 'videocon-recharge', '10', 0, 'Prepaid Videocon Mobile Recharge ', 'Prepaid Videocon Mobile Recharge ', '2', 'Videocon-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');



INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Dish TV', 'dish-tv-recharge', '10', 0, 'Prepaid Dish TV DTH Recharge ', 'Prepaid Dish TV DTH Recharge ', '2', 'DISH-TV-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Tata Sky', 'tata-sky-recharge', '10', 0, 'Prepaid Tata Sky DTH Recharge ', 'Prepaid Tata Sky DTH Recharge ', '2', 'Tata-Sky-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'SUN Direct', 'sun-direct-tv-recharge', '10', 0, 'Prepaid SUN Direct DTH Recharge ', 'Prepaid SUN Direct DTH Recharge ', '2', 'SUN-DIRECT-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'BIG TV', 'big-tv-recharge', '10', 0, 'Prepaid BIG TV DTH Recharge ', 'Prepaid BIG TV DTH Recharge ', '2', 'BIG-TV-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Airtel DTH', 'airtel-dth-recharge', '10', 0, 'Prepaid Airtel DTH Recharge ', 'Prepaid Airtel DTH Recharge ', '2', 'AirtelDTH-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`packages` (`id`, `name`, `url`, `price`, `validity`, `shortDesc`, `description`, `frequency`, `code`, `toshow`, `cycle_ids`, `trial_days`, `created`, `modified`) 
       VALUES (NULL, 'Videocon', 'videocon-recharge', '10', 0, 'Prepaid Videocon DTH Recharge ', 'Prepaid Videocon DTH Recharge ', '2', 'Videocon-RECHARGE', '0', NULL, '0', '2013-04-23 00:00:00', '2013-04-23 00:00:00');



INSERT INTO `smstadka`.`categories` (`id`, `name`, `keyword`, `parent`, `description`, `layout`, `toshow`, `created`, `modified`) VALUES 
                                    (NULL, 'Prepaid Recharge', 'Prepaid Recharge', '0', 'Prepaid Recharge Root Category for mobile / DTH', 'L', '0', NULL, NULL);
INSERT INTO `smstadka`.`categories` (`id`, `name`, `keyword`, `parent`, `description`, `layout`, `toshow`, `created`, `modified`) VALUES 
                                    (NULL, 'Mobile Prepaid Recharge', 'Mobile Prepaid Recharge', '63', 'Prepaid Recharge Category for mobile', 'L', '0', NULL, NULL);
INSERT INTO `smstadka`.`categories` (`id`, `name`, `keyword`, `parent`, `description`, `layout`, `toshow`, `created`, `modified`) VALUES 
                                    (NULL, 'DTH Prepaid Recharge', 'DTH Prepaid Recharge', '63', 'Prepaid Recharge Category for DTH', 'L', '0', NULL, NULL);

INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '155', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '156', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '157', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '158', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '159', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '160', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '161', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '162', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '163', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '164', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '165', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '172', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '173', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '64', '174', '2013-04-23 00:00:00', '2013-04-23 00:00:00');



INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '65', '166', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '65', '167', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '65', '168', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '65', '169', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '65', '170', '2013-04-23 00:00:00', '2013-04-23 00:00:00');
INSERT INTO `smstadka`.`categories_packages` (`id`, `category_id`, `package_id`, `created`, `modified`) 
                                      VALUES (NULL, '65', '171', '2013-04-23 00:00:00', '2013-04-23 00:00:00');


UPDATE  `smstadka`.`packages` SET  `code` =  '2' WHERE  `packages`.`id` =155;
UPDATE  `smstadka`.`packages` SET  `code` =  '3' WHERE  `packages`.`id` =156;

UPDATE  `smstadka`.`packages` SET  `name` =  'Tata Docomo',`code` =  '9' WHERE  `packages`.`id` =157;

UPDATE  `smstadka`.`packages` SET  `code` =  '4' WHERE  `packages`.`id` =158;

UPDATE  `smstadka`.`packages` SET  `code` =  '5' WHERE  `packages`.`id` =159;

UPDATE  `smstadka`.`packages` SET  `code` =  '30' WHERE  `packages`.`id` =160;

UPDATE  `smstadka`.`packages` SET  `code` =  '7' WHERE  `packages`.`id` =161;

UPDATE  `smstadka`.`packages` SET  `code` =  '8' WHERE  `packages`.`id` =162;

UPDATE  `smstadka`.`packages` SET  `code` =  '10' WHERE  `packages`.`id` =163;

UPDATE  `smstadka`.`packages` SET  `code` =  '11' WHERE  `packages`.`id` =164;

UPDATE  `smstadka`.`packages` SET  `code` =  '15' WHERE  `packages`.`id` =165;

UPDATE  `smstadka`.`packages` SET  `code` =  '18' WHERE  `packages`.`id` =166;

UPDATE  `smstadka`.`packages` SET  `code` =  '20' WHERE  `packages`.`id` =167;

UPDATE  `smstadka`.`packages` SET  `code` =  '19' WHERE  `packages`.`id` =168;

UPDATE  `smstadka`.`packages` SET  `code` =  '17' WHERE  `packages`.`id` =169;

UPDATE  `smstadka`.`packages` SET  `code` =  '16' WHERE  `packages`.`id` =170;

UPDATE  `smstadka`.`packages` SET  `code` =  '21' WHERE  `packages`.`id` =171;

UPDATE  `smstadka`.`packages` SET  `code` =  '1' WHERE  `packages`.`id` =172;

UPDATE  `smstadka`.`packages` SET  `code` =  '12' WHERE  `packages`.`id` =174;


