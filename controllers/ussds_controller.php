<?php
class UssdsController extends AppController {
	
	var $name = 'Ussds';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler','Shop');
	var $uses = array('Ussd','UssdCall');
	
	function beforeFilter() {
		//parent::beforeFilter();
		$this->Auth->allow('*');
		//$this->Auth->allowedActions = array();
	}

	/*function test($sessionId,$method,$mobile=null,$option=null){
        $ch = curl_init('http://ussd.signal7.in/ussds/session/'.$sessionId.'/'.$method.'/'.$mobile.'/'.$option);
		echo $output = curl_exec($ch);
		curl_close($ch);
		exit;
    }*/	
	
	function session($sessionId=null,$method=null){		
		if($method == 'status'){
			echo '{"sessionActive":true,"responseExitCode":200,"responseMessage":""}';	
		}
		
		if($method == 'start'){
			$handle = fopen('php://input','r');
			$jsonData = fgets($handle);
			$recArr = json_decode($jsonData);
			$mobile = $recArr->{'msisdn'};
			if(strlen($mobile) > 10)
			$mobile = substr($mobile, -10);
			
			//$this->test($sessionId,$method,$mobile,$recArr->{'optional'});
			
			if($recArr->{'optional'} == 'new'){
				echo $this->toOss($mobile,$sessionId);
			}else if($recArr->{'optional'} == 'lokpal'){
				echo $this->toLokpal($mobile,$sessionId);				
			}else if($recArr->{'optional'} == 'fc'){
				echo $this->toFCharge($mobile,$sessionId);				
			}else if($recArr->{'optional'} == 'marsh'){
				$url = SITE_NAME . "apis/marshAPI.php?first=1&mobile=$mobile&tid=$sessionId";
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($ch, CURLOPT_TIMEOUT, 100);
				echo $msg = curl_exec($ch);
				curl_close ($ch);
			}
			else if(in_array($recArr->{'optional'},array('playwin','signal7','marsh'))){
				$msg = $this->toUSSD($mobile,$sessionId,3,1);
				echo '{"shouldClose":"false","ussdMenu":"'.$msg.'","responseExitCode":200,"responseMessage":""}';			
			}
			else if($recArr->{'optional'} == 'cab'){
				echo $this->toCab($mobile,$sessionId);				
			}else{
				$this->Ussd->query("update ussd_calls set session_id = '".$sessionId."' where id=".$recArr->{'optional'});
				$getCnfMsg = $this->Ussd->query("select message_sent from ussd_calls where id = ".$recArr->{'optional'});

				
			
				echo $cnfMsg = stripslashes($getCnfMsg['0']['ussd_calls']['message_sent']);	
			}
		}
		
		if($method == 'response'){
			$handle = fopen('php://input','r');
			$jsonData = fgets($handle);	
			$recArr = json_decode($jsonData);
			$mobile = $recArr->{'msisdn'};
			if(strlen($mobile) > 10)
			$mobile = substr($mobile, -10);
			
			//$this->test($sessionId,$method,$mobile,$recArr->{'text'});
			
			$this->ussdReceive($mobile,$recArr->{'text'},$sessionId,$code=null,$tid=null,$dcs=null);
		}
		
		if($method == 'end'){
			echo '{"responseExitCode":200,"responseMessage":""}'; exit;
		}
		
		$this->layout = 'ajax'; 
		$this->autoRender = false;
	}
	
	function releaseSession($session_id){
		$post =  json_encode(array('username'=>'mindsarray','authKey'=>'WtzR!bG?hxQHQc','sessionId'=>$session_id, 'reason' => 'Test'));		
		$arr = array();
		array_push($arr, 'Content-Type: application/json; charset=utf-8');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $arr);
		curl_setopt($ch, CURLOPT_URL, 'http://ussd1.infobip.com:8080/appcore/access/rest/v2/application/mindsarray/session/release');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_exec($ch);
		curl_close($ch);
	}
	 
	function recieveRequest($client=null,$mobile=null){
		header("Location:http://ussd.signal7.in/ussds/receiveRequest/$client/$mobile");exit;
		$vendor = $this->decideVendor($mobile);
		if($client == '1'){
			$requestXML = '<VerifyMobile><MobileNo>'.$mobile.'</MobileNo></VerifyMobile>';
			$response = $this->ossApi('VerifyMobileNo',$requestXML,$mobile);
			if($response['VerifyMobileNo']['VerifyDetails']['IsCustomer'] == 'True' || $response['VerifyMobileNo']['VerifyDetails']['IsMerchant'] == 'True'){
				$this->handshake($mobile,$vendor,'1');
			}
		}
		if($client == '2'){ $this->handshake($mobile,$vendor,'2'); }		
		if($client == '3'){ $this->handshake($mobile,$vendor,'3'); }
		if($client == '4'){ $this->handshake($mobile,$vendor,'4'); }
		if($client == '5'){ $this->handshake($mobile,$vendor,'5'); }
		if($client == '6'){ $this->handshake($mobile,$vendor,'6'); }
		if($client == '7'){ $this->handshake($mobile,$vendor,'7'); }
		$this->autoRender = false;		
	}
	
	function handshake($mobile,$vendor,$appid,$extra=null){
		if(is_null($extra))
		$extra = '';
								
		if($vendor == '1'){//messagin365 					
			$url = "http://messaging365.com:8080/el/invokeMenuApi?username=anurag&password=a1anurag&msg=anurag&to=91".$mobile."&gate=353871423333";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			$data = curl_exec ($ch);
			curl_close ($ch);
			$this->Ussd->query("INSERT INTO ussd_mob_ven_app (mobile_no,vendor_id,app_id,extra) VALUES ('".$mobile."','".$vendor."','".$appid."','".$extra."')");
		}else if($vendor == '2'){//imei
			
			if($appid == '1'){
				if($extra != ''){					
					$getCnfMsg = $this->Ussd->query("select message_sent from ussd_calls where id = ".$extra);
					$flash = stripslashes($getCnfMsg['0']['ussd_calls']['message_sent']);	
				}else{
					$flash = $this->toOss($mobile,$vendor,$appid,$sessionId);
				}				
			}else if($appid == '2'){
				$flash = $this->toLokpal($mobile,$vendor,$appid,$sessionId);				
			}else if($appid == '3'){
				$flash = $this->toFCharge($mobile,$vendor,$appid,$sessionId);				
			}else if($appid == '6'){
				$flash = $this->toCab($mobile,$vendor,$appid,$sessionId);				
			}else if(in_array($appid,array('4','5','7'))){
				$flash = $this->toUSSD($mobile,$sessionId,1,1);			
			}

			$flash = str_replace("\\n","\n",$flash);
			
			$url = "http://203.199.114.231/ussdproxy/ussdpush.aspx?msisdn=$mobile&msg=".urlencode($flash)."&src=56263&tid=123&session=0&uid=anurag&keyword=Mindsarray";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			$data = curl_exec ($ch);
			if(curl_errno($ch)){//globalUSSD
				curl_close($ch);
				$url = "http://ec2.globalussd.mobi/demo/push?service=mindsarray.demo&subscriber=91".$mobile;
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($ch, CURLOPT_TIMEOUT, 100);
				$data = curl_exec ($ch);
				curl_close ($ch);
				$vendor = 3;
				$this->Ussd->query("INSERT INTO ussd_mob_ven_app (mobile_no,vendor_id,app_id,extra) VALUES ('".$mobile."','".$vendor."','".$appid."','".$extra."')");
			}else{
				curl_close ($ch);
				$this->Ussd->query("INSERT INTO ussd_mob_ven_app (mobile_no,vendor_id,app_id,extra) VALUES ('".$mobile."','".$vendor."','".$appid."','".$extra."')");
			}			
		}else if($vendor == '3'){//globalUSSD		
			$url = "http://ec2.globalussd.mobi/demo/push?service=mindsarray.demo&subscriber=91".$mobile;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			$data = curl_exec ($ch);
			curl_close ($ch);
			$this->Ussd->query("INSERT INTO ussd_mob_ven_app (mobile_no,vendor_id,app_id,extra) VALUES ('".$mobile."','".$vendor."','".$appid."','".$extra."')");					
		}
	}
	
	function toUSSD($mobile,$sessionId,$route,$first=null){
		$url = SITE_NAME . "apis/ussd.php?msisdn=$mobile&tid=$sessionId&route=$route";
		if($first != null){
			$url .="&first=1";
		}
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$flash = curl_exec($ch);
		curl_close ($ch);
		return $flash;
	}
	
	function decideVendor($mobile){
		$res = $this->Ussd->query("select id from mobile_numbering WHERE number = ". substr($mobile,0,4) . " AND (operator = 'AT' || operator = 'AC')");
		if(isset($res['0']['mobile_numbering']['id']) && $res['0']['mobile_numbering']['id'] != ''){//messagin365
			return 1;
		}else{
			return 3;
		}
	}
	
	function globalUssd(){
		header("Location:http://ussd.signal7.in/ussds/globalUssd/?subscriber=".$_REQUEST['subscriber']."&reply=".urlencode($_REQUEST['reply']));exit;
		
		$fh = fopen('/tmp/globalUSSD.txt','a+');
		fwrite($fh,"\n".$data);
		$data = $_REQUEST;		
		fwrite($fh,"\njson:".json_encode($data));
		
		$mobile = $_REQUEST['subscriber'];
		if(strlen($mobile) > 10)
		$mobile = substr($mobile, -10);

		
		
		$result = $this->Ussd->query("select id,app_id,extra,session_id from ussd_mob_ven_app where mobile_no = '".$mobile."' and vendor_id = '3' order by id desc limit 1");
		if(is_null($result['0']['ussd_mob_ven_app']['session_id'])){
			$sessionId = strtotime(date('Y-m-d H:i:s'));
			$this->Ussd->query("update ussd_mob_ven_app set session_id = '".$sessionId."' where id=".$result['0']['ussd_mob_ven_app']['id']);
		}else{
			$sessionId = $result['0']['ussd_mob_ven_app']['session_id'];
		}

		
		$appid = $result['0']['ussd_mob_ven_app']['app_id'];
		$extra = $result['0']['ussd_mob_ven_app']['extra'];
		
		if(!isset($_REQUEST['reply'])){//first time
			header('Content-type: text/xml');
			if($appid == '1'){
				if($extra != ''){
					$this->Ussd->query("update ussd_calls set session_id = '".$sessionId."' where id=".$extra);
					$getCnfMsg = $this->Ussd->query("select message_sent from ussd_calls where id = ".$extra);
					$flash = stripslashes($getCnfMsg['0']['ussd_calls']['message_sent']);	
				}else{
					$flash = $this->toOss($mobile,'3',$appid,$sessionId);
				}
			}else if($appid == '2'){
					$flash = $this->toLokpal($mobile,'3',$appid,$sessionId);				
			}else if($appid == '3'){
					$flash = $this->toFCharge($mobile,'3',$appid,$sessionId);				
			}else if($appid == '6'){
					$flash = $this->toCab($mobile,'3',$appid,$sessionId);				
			}else if($appid == '4' || $appid == '5' || $appid == '7'){
				$url = "Location:http://www.smstadka.com/apis/ussd.php?msisdn=$mobile&tid=$sessionId&route=4&first=1&msg=";			
				header($url); exit;
			}
						
			echo $flash; exit;			
		}else{
			$reply = $_REQUEST['reply'];
			if($appid == '4' || $appid == '5' || $appid == '7'){
				$url = "Location:http://www.smstadka.com/apis/ussd.php?msisdn=$mobile&msg=".urlencode($reply)."&tid=$sessionId&route=4";			
				header($url); exit;
			}else{
				$this->ussdReceive($mobile,$reply,$sessionId,'3');	
			}
		}
				
		$this->autoRender = false;
	}
	
	function m365Rec($sessionId,$page_id,$data,$appid,$mobile,$extra){
		
		if($page_id == '0'){// first time
			header('Content-type: text/xml');
			if($appid == '1'){
				if($extra != ''){
					$this->Ussd->query("update ussd_calls set session_id = '".$sessionId."' where id=".$extra);
					$getCnfMsg = $this->Ussd->query("select message_sent from ussd_calls where id = ".$extra);
					$flash = stripslashes($getCnfMsg['0']['ussd_calls']['message_sent']);	
				}else{
					$flash = $this->toOss($mobile,'1',$appid,$sessionId);
				}
			}else if($appid == '2'){
					$flash = $this->toLokpal($mobile,'1',$appid,$sessionId);				
			}else if($appid == '3'){
					$flash = $this->toFCharge($mobile,'1',$appid,$sessionId);				
			}else if($appid == '6'){
					$flash = $this->toCab($mobile,'1',$appid,$sessionId);				
			}
						
			echo $flash; exit;
		}else{
			$this->ussdReceive($mobile,$data,$sessionId,'1',$code=null,$tid=null,$dcs=null);
		}		
	}
	
	function imei($sessionId,$data,$appid,$mobile,$extra){
		if($extra != ''){
			$this->Ussd->query("update ussd_calls set session_id = '".$sessionId."' where id=".$extra);
		}else{
			$this->Ussd->query("update ussd_calls set session_id = '".$sessionId."' where mobile='".$mobile."' and vendor_id = '2' and app_id = '".$appid."'");
			
		}
		$this->ussdReceive($mobile,$data,$sessionId,'2',$code=null,$tid=null,$dcs=null);		
	}
	
	function ussdCalls($mobile,$vendor_id,$appid,$session_id=null,$screenSent=null,$flashMsg=null,$user=null,$extra=null){
		$this->UssdCall->create();
		$this->data['UssdCall']['mobile'] = $mobile;
		if(!is_null($session_id))$this->data['UssdCall']['session_id'] = $session_id;
		if(!is_null($screenSent))$this->data['UssdCall']['screen_sent'] = $screenSent;
		if(!is_null($flashMsg))$this->data['UssdCall']['message_sent'] = addslashes($flashMsg);
		if(!is_null($user))$this->data['UssdCall']['ussd_user'] = $user;
		if(!is_null($extra))$this->data['UssdCall']['extra'] = $extra;
		$this->data['UssdCall']['created'] = date("Y-m-d H:i:s");
		if(!is_null($vendor_id))$this->data['UssdCall']['vendor_id'] = $vendor_id;
		if(!is_null($appid))$this->data['UssdCall']['app_id'] = $appid;
		
		$this->UssdCall->save($this->data);
		return $this->UssdCall->id;
	}
	
	function toCab($mobile,$vendor_id,$appid,$sessionId=null){
		$flashMsg = $this->createMenu(51,$vendor_id,$sessionId);
 		$xnId = $this->ussdCalls($mobile,$vendor_id,$appid,$sessionId,'51',$flashMsg,'8'); 
 		return $flashMsg;		
	}
	
	function toFCharge($mobile,$vendor_id,$appid,$sessionId=null){						 		
 		$flashMsg = $this->createMenu(26,$vendor_id,$sessionId);
 		$xnId = $this->ussdCalls($mobile,$vendor_id,$appid,$sessionId,'26',$flashMsg,'5'); 
 		return $flashMsg;
	}
	
	function toLokpal($mobile,$vendor_id,$appid,$sessionId=null){				 		
 		$flashMsg = $this->createMenu(19,$vendor_id,$sessionId);
 		$xnId = $this->ussdCalls($mobile,$vendor_id,$appid,$sessionId,'19',$flashMsg,'4');
 		return $flashMsg;
	}	

	function toOss($mobile,$vendor_id,$appid,$sessionId=null){ 		 			 	
			$requestXML = '<VerifyMobile><MobileNo>'.$mobile.'</MobileNo></VerifyMobile>';
			$response = $this->ossApi('VerifyMobileNo',$requestXML,$mobile);
			//print_r($response); exit;
		 	if($response){		 		
			 	if($response['VerifyMobileNo']['VerifyDetails']['IsCustomer'] == 'True' && $response['VerifyMobileNo']['VerifyDetails']['IsMerchant'] == 'True'){
			 		$flashMsg = $this->createMenu(1,$vendor_id,$sessionId); 
			 		$xnId = $this->ussdCalls($mobile,$vendor_id,$appid,$sessionId,'1',$flashMsg,'1');
			 		return $flashMsg;
			 	}else if($response['VerifyMobileNo']['VerifyDetails']['IsCustomer'] == 'False' && $response['VerifyMobileNo']['VerifyDetails']['IsMerchant'] == 'True'){			 		
			 		$flashMsg = $this->createMenu(3,$vendor_id,$sessionId);
			 		$xnId = $this->ussdCalls($mobile,$vendor_id,$appid,$sessionId,'3',$flashMsg,'3');
			 		return $flashMsg;			 		
			 	}else if($response['VerifyMobileNo']['VerifyDetails']['IsCustomer'] == 'True' && $response['VerifyMobileNo']['VerifyDetails']['IsMerchant'] == 'False'){			 					 		
			 		$flashMsg = $this->createMenu(2,$vendor_id,$sessionId);
			 		$xnId = $this->ussdCalls($mobile,$vendor_id,$appid,$sessionId,'2',$flashMsg,'2');
			 		return $flashMsg;		 					 		
			 	}				 		
		 	}					
	}
	
	function ossApi($method,$requestXML,$mobile=null){		
		if($mobile == '9004387418' || $mobile == '9892471157'){
			$url = 'http://demo.osscard.com/OSSCardUSSDService.asmx';
			$accessId = '1';
			$authKey = 'MSA_VF_Access';
			$requestXML = str_replace($mobile,"9892471157",$requestXML);
		}else{
			$url = 'http://www.osscard.com/OSSCardUSSDService.asmx';
			$accessId = '2';
			$authKey = 'MSA_USSD_Access';
		}
		
		App::import('vendor', 'soap', array('file' => 'soaplib/nusoap.php'));		
		$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
		$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
		$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
		$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
		$useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
		$namespace  = "http://tempuri.org/";
		$client = new nusoap_client($url, false,
								$proxyhost, $proxyport, $proxyusername, $proxypassword);
		
		$client->setUseCurl($useCURL);
		$params = "<strRequestXML>".htmlspecialchars($requestXML)."</strRequestXML>";
		//$params = "<strRequestXML>&lt;VerifyMobile&gt;&lt;MobileNo&gt;9099959156&lt;/MobileNo&gt;&lt;/VerifyMobile&gt;</strRequestXML>";
		$headers = "<MobileServiceAccessAuthentication xmlns='http://tempuri.org/'><MSAccessID>".$accessId."</MSAccessID><AuthKey>".$authKey."</AuthKey></MobileServiceAccessAuthentication>";
		
		$res =  $client->call($method, $params,'http://tempuri.org/',$namespace.''.$method,$headers);
		return $this->General->xml2array($res);
	}

	function ussdReceive($mobile,$msg,$sessionId,$vendor,$code=null,$tid=null,$dcs=null){
		$mobile = trim($mobile);
		$msg = trim($msg);
		$sessionId = $sessionId;
		$code = trim($code);
		$tid = trim($tid);
		$dcs = trim($dcs);
		

		$lastTran = $this->Ussd->query("select id from ussd_calls where session_id = '".$sessionId."' and mobile = '".$mobile."' order by id desc limit 1");	
		$tid = $lastTran['0']['ussd_calls']['id'];
		if(empty($tid)){
			$msg = $this->toUSSD($mobile,$sessionId,3);
			echo $msg;
			exit;
		}
		//user data
		$data = $this->Ussd->query("select ussd_calls.screen_sent,ussd_calls.session_id,ussd_calls.mobile,ussd_calls.ussd_user,ussd_users.ussd_client from ussd_calls,ussd_users where ussd_calls.ussd_user = ussd_users.id and ussd_calls.id = ".$tid);

		$optionChosen = $this->Ussd->query("select option_id from ussd_operations where screen_no = ".$data['0']['ussd_calls']['screen_sent']." and option_order = ".$msg);
		if($optionChosen['0']['ussd_operations']['option_id'] != '')
			$option = $optionChosen['0']['ussd_operations']['option_id'];
		else
			$option = $msg;
			
		$this->Ussd->query("update ussd_calls set msisdn = '".$mobile."',response='".$option."',code='".$code."',dcs='".$dcs."' where id=".$tid);		
		
		//echo $data[0]['ussd_users']['ussd_client']; exit;
		if($data[0]['ussd_users']['ussd_client'] == '1'){// to oss	
			$this->interactionOSS($tid,$data['0']['ussd_calls']['screen_sent'], $data['0']['ussd_calls']['session_id'],$data['0']['ussd_calls']['mobile'],$data['0']['ussd_calls']['ussd_user'],$data['0']['ussd_users']['ussd_client'],$option,$vendor);
		}

		if($data[0]['ussd_users']['ussd_client'] == '2'){// to lokpal				
			$this->interactionLokpal($tid,$data['0']['ussd_calls']['screen_sent'], $data['0']['ussd_calls']['session_id'],$data['0']['ussd_calls']['mobile'],$data['0']['ussd_calls']['ussd_user'],$data['0']['ussd_users']['ussd_client'],$option,$vendor);
		}
		
		if($data[0]['ussd_users']['ussd_client'] == '3'){// to freecharge
			$this->interactionFCharge($tid,$data['0']['ussd_calls']['screen_sent'], $data['0']['ussd_calls']['session_id'],$data['0']['ussd_calls']['mobile'],$data['0']['ussd_calls']['ussd_user'],$data['0']['ussd_users']['ussd_client'],$option,$vendor);
		}
		
		if($data[0]['ussd_users']['ussd_client'] == '6'){// cab booking
			$this->interactionCab($tid,$data['0']['ussd_calls']['screen_sent'], $data['0']['ussd_calls']['session_id'],$data['0']['ussd_calls']['mobile'],$data['0']['ussd_calls']['ussd_user'],$data['0']['ussd_users']['ussd_client'],$option,$vendor);
		}
		
		$this->autoRender = false;
	}
	
	function interactionCab($tid,$screen,$session_id,$mobile,$ussd_user,$appid,$response,$vendor){
		if($vendor == '1')header('Content-type: text/xml');
		
		if($screen == '51' && $response == '1'){			
			$flashMsg = $this->createMenu(52,$vendor,$session_id);			
 			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'52',$flashMsg,'8'); 
 			echo $flashMsg;	exit;
		}
		
		if($screen == '52'&& ($response == '1' || $response == '2')){
			$flashMsg = $this->createMenu(53,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'53',$flashMsg,'8');
			echo $flashMsg;	exit;			
		}
		
		if($screen == '53' && $response != ''){
			$flashMsg = $this->createMenu(59,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'59',$flashMsg,'8');
			echo $flashMsg;	exit;			
		}
		
		if($screen == '59' && $response != ''){
			$flashMsg = $this->createMenu(54,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'54',$flashMsg,'8');
			echo $flashMsg;	exit;
		}

		if($screen == '54' && $response != ''){
			$flashMsg = $this->createMenu(55,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'55',$flashMsg,'8');
			echo $flashMsg;	exit;			
		}
		
		
		if($screen == '55' && $response != ''){
			$flashMsg = $this->createMenu(56,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'56',$flashMsg,'8');
			echo $flashMsg;	exit;
		}
		
		if($screen == '56' && $response != ''){
			$flashMsg = $this->createMenu(58,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'58',$flashMsg,'8');
			echo $flashMsg;	exit;
		}
		
		if($screen == '51' && $response == '2'){			
			$flashMsg = $this->createMenu(60,$vendor,$session_id);			
 			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'60',$flashMsg,'8'); 
 			echo $flashMsg;	exit;
		}
		
		if($screen == '60' && $response != ''){			
			$flashMsg = $this->createMenu(61,$vendor,$session_id);			
 			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'61',$flashMsg,'8'); 
 			echo $flashMsg;	exit;
		}
		
		if($screen == '51' && $response == '3'){			
			$flashMsg = $this->createMenu(62,$vendor,$session_id);			
 			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'62',$flashMsg,'8'); 
 			echo $flashMsg;	exit;
		}
		
		if(($screen == '58' || $screen == '61' || $screen == '62') && $response == '0'){
			$this->releaseSession($session_id);
		}
		
		if(($screen == '58' || $screen == '61' || $screen == '62') && $response == '9'){
			$flashMsg = $this->createMenu(51,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'51',$flashMsg,'8');
			echo $flashMsg;	exit;
		}
						
		$this->autoRender = false;
	}
	
	function interactionFCharge($tid,$screen,$session_id,$mobile,$ussd_user,$appid,$response,$vendor){
		if($vendor == '1')header('Content-type: text/xml');
		if($screen == '26' && $response == '1'){			
			$flashMsg = $this->createMenu(29,$vendor,$session_id);			
 			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'29',$flashMsg,'5'); 
 			echo $flashMsg;	exit;
		}
		
		if(($screen == '29' || $screen == '32') && $response != ''){
			$mobErr = $this->General->mobileValidate($response);
			
			if($mobErr == '1'){ //ERROR: invalid mobile no				
				$flashMsg = $this->createMenu(32,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'32',$flashMsg,'5');
				echo $flashMsg;	exit;			
			}else{				
				$flashMsg = $this->createMenu(30,$vendor,$session_id);				
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'30',$flashMsg,'5');
				echo $flashMsg;	exit;	
			}			
		}
		
		if(($screen == '30' || $screen == '33') && $response != ''){
			$amt = $this->General->priceValidate($response);
			
			if($amt == ''){ //ERROR: invalid amount
				$flashMsg = $this->createMenu(33,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'33',$flashMsg,'5');
				echo $flashMsg;	exit;			
			}else{				
				$flashMsg = $this->createMenu(31,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'31',$flashMsg,'5');
				echo $flashMsg;	exit;	
			}			
		}
		
		if(($screen == '31' || $screen == '38') && $response == '1'){
			$flashMsg = $this->createMenu(26,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'26',$flashMsg,'5');
			echo $flashMsg;	exit;
		}
		
		if(($screen == '31' || $screen == '38') && $response == '2'){
			$this->releaseSession($session_id);
		}
		
		if($screen == '26' && $response == '2'){
			$flashMsg = $this->createMenu(35,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'35',$flashMsg,'5');
			echo $flashMsg;	exit;
		}
		
		if($screen == '35' && $response != ''){
			$flashMsg = $this->createMenu(36,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'36',$flashMsg,'5');
			echo $flashMsg;	exit;
		}
		
		if(($screen == '36' || $screen == '37') && $response != ''){
			$amt = $this->General->priceValidate($response);
			
			if($amt == ''){ //ERROR: invalid amount
				$flashMsg = $this->createMenu(37,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'37',$flashMsg,'5');
				echo $flashMsg;	exit;			
			}else{
				$flashMsg = $this->createMenu(38,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'38',$flashMsg,'5');
				echo $flashMsg;	exit;	
			}			
		}
				
		$this->autoRender = false;
	}
	
	function interactionLokpal($tid,$screen,$session_id,$mobile,$ussd_user,$appid,$response,$vendor){
		if($vendor == '1')header('Content-type: text/xml');
		if($screen == '19' && $response != ''){
			$flashMsg = $this->createMenu(20,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'20',$flashMsg,'4');
			echo $flashMsg;	exit;
		}
		
		if($screen == '20' && $response != ''){
			$flashMsg = $this->createMenu(21,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'21',$flashMsg,'4');
			echo $flashMsg;	exit;
		}
		
		if($screen == '21' && $response != ''){
			$flashMsg = $this->createMenu(22,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'22',$flashMsg,'4');
			echo $flashMsg;	exit;		
		}
		
		if($screen == '22' && $response != ''){
			$flashMsg = $this->createMenu(23,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'23',$flashMsg,'4');
			echo $flashMsg;	exit;		
		}
		
		if($screen == '23' && $response != ''){
			$flashMsg = $this->createMenu(24,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'24',$flashMsg,'4');
			echo $flashMsg;	exit;		
		}
		
		if($screen == '24' && $response == '1'){		
			$flashMsg = $this->createMenu(25,$vendor,$session_id,'true');
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'25',$flashMsg,'4');
			echo $flashMsg;	exit;		
		}
		
		if($screen == '24' && $response == '2'){
			$this->releaseSession($session_id);		
		}
		
		$this->autoRender = false;
	}
	
	
	function interactionOSS($tid,$screen,$session_id,$mobile,$ussd_user,$appid,$response,$vendor){
		if($vendor == '1')header('Content-type: text/xml');
		if($screen == '1' && $response == '1'){ //user is a customer
			$flashMsg = $this->createMenu(2,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'2',$flashMsg,'2');
			echo $flashMsg;	exit;
		}
		
		if($screen == '1' && $response == '2'){ //user is a merchant
			$flashMsg = $this->createMenu(3,$vendor,$session_id);			
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'3',$flashMsg,'3');
			echo $flashMsg;	exit;
		}
		
		if($screen == '1' && $response != ''){ //Invalid option
			$flashMsg = $this->createMenu(1,$vendor,$session_id,'false',1);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'1',$flashMsg,'1');
			echo $flashMsg;	exit;			
			//$this->ussdRequest($mobile,$flashMsg,$xnId);
		}
		
		if($screen == '2' && $response == '1'){ //customer->balance
		 	$requestXML = '<GetBalance><MobileNo>'.$mobile.'</MobileNo></GetBalance>';
			$response = $this->ossApi('GetBalance',$requestXML,$mobile);

			if($response['GetBalance']['OSSCardBal']['BAL']){
				$flashMsg = $this->createMenu(15,$vendor,$session_id,'true');
				$flashMsg = str_replace("_BAL_",$response['GetBalance']['OSSCardBal']['BAL'],$flashMsg);
				$flashMsg = str_replace("_CHARGE_",$response['GetBalance']['OSSCardBal']['RequestCharges'],$flashMsg);

				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'15',$flashMsg,'2');
				echo $flashMsg;	exit;			
			}else{
				echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
			}
		}
		if($screen == '2' && $response == '2'){ //cust->mini stmt
		 	$requestXML = '<GetLastTransactionDetails><MobileNo>'.$mobile.'</MobileNo></GetLastTransactionDetails>';
			$response = $this->ossApi('GetLastTransactionDetails',$requestXML,$mobile);

		 	if($response['GetLastTransactionDetails']){
			 	$flashMsg =  "Last ".count($response['GetLastTransactionDetails']['TransactionDetail'])." Trns- ".$response['GetLastTransactionDetails']['TransactionDetail']['0']['TranDate']." ".$response['GetLastTransactionDetails']['TransactionDetail']['0']['TranAmount']." ".$response['GetLastTransactionDetails']['TransactionDetail']['0']['CreditDebit']."/Tr ID ".$response['GetLastTransactionDetails']['TransactionDetail']['0']['TransactionNo'].". ";
				if($response['GetLastTransactionDetails']['TransactionDetail']['1'])
				$flashMsg .= $response['GetLastTransactionDetails']['TransactionDetail']['1']['TranDate']." ".$response['GetLastTransactionDetails']['TransactionDetail']['1']['TranAmount']." ".$response['GetLastTransactionDetails']['TransactionDetail']['1']['CreditDebit']."/Tr ID ".$response['GetLastTransactionDetails']['TransactionDetail']['1']['TransactionNo']."; "; 
				if($response['GetLastTransactionDetails']['TransactionDetail']['2'])
				$flashMsg .= $response['GetLastTransactionDetails']['TransactionDetail']['2']['TranDate']." ".$response['GetLastTransactionDetails']['TransactionDetail']['2']['TranAmount']." ".$response['GetLastTransactionDetails']['TransactionDetail']['2']['CreditDebit']."/Tr ID ".$response['GetLastTransactionDetails']['TransactionDetail']['2']['TransactionNo']."; ";
				
				$flashMsg .= "Charges ".($response['GetLastTransactionDetails']['TransactionDetail']['0']['RequestCharges']*100)." paisa/request.";
	
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'',$flashMsg,'2');
				echo $this->msg($flashMsg,$vendor,$session_id);	exit;		
		 	}else{
				echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
			}
		}
		
		if($screen == '2' && $response == '3'){ //cust->order status		 	
			$flashMsg = $this->createMenu(16,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'16',$flashMsg,'2');
			echo $flashMsg; exit;	
		}

		if($screen == '16' && $response != ''  && $ussd_user == '2'){ //cust->order status
			$requestXML = '<GetCustomerOrderStatus><CustomerMobileNo>'.$mobile.'</CustomerMobileNo><TransactionID>'.trim($response).'</TransactionID></GetCustomerOrderStatus>';
			$response = $this->ossApi('GetCustomerOrderStatus',$requestXML,$mobile);
			//echo "<pre>";print_r($response);echo "</pre>";
			
			if($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'] != ''){
				$statusId = trim($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['StatusID']);
				if($statusId == '1'){ // order request expired
					$flashMsg = "Tran ID:".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'].": Order request expired. Charges ".($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['RequestCharges']*100)." paisa/request.";	
				}else if($statusId == '2'){//order confirmed
					$flashMsg = "Tran ID:".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'].": Payment Successfully debited from your A/C towards payment to Merchant ".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['MerchantMobileNo'].". Current Bal is Rs. ".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['CardBalance'].". Charges ".($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '3'){//order cancelled
					$flashMsg = "Tran ID:".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'].": Order cancelled. Charges ".($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '4'){//Order Cancel Request UnderProcess
					$flashMsg = "Tran ID:".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'].": Cancellation request Successfully submitted by merchant ".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['MerchantMobileNo'].". Request Amt Rs. ".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['CancelOrderAmount']." will be credited in your OSS A/C shortly. Charges ".($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '5'){//Order Cancel Request Approved 
					$flashMsg = "Tran ID:".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'].": Cancellation Successfully done by OSS on request of merchant ".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['MerchantMobileNo'].". Request Amt Rs. ".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['CancelOrderAmount']." has been credited in your OSS A/C. Charges ".($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '6'){//Order Cancel Request Rejected
					$flashMsg = "Tran ID:".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'].": Order cancel request rejected. Charges ".($response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else{
					$flashMsg = "Tran ID:".$response['GetCustomerOrderRequestStatus']['CustomerOrderRequestDetails']['TransactionID'].": ".$response['GetCustomerOrderRequestDetails']['CustomerOrderRequestDetails']['StatusRemark'].". Charges ".($response['GetCustomerOrderStatus']['CustomerOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}
				
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'',$flashMsg,'2');
				echo $this->msg($flashMsg,$vendor,$session_id);	exit;
			}else{
				echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
			}					 				
		}
		
		if($screen == '2' && $response == '4'){ //cust->payment request		 	
			$flashMsg = $this->createMenu(17,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'17',$flashMsg,'2');
			echo $flashMsg; exit;
		}
		
		if(($screen == '17' || $screen == '39' || $screen == '40' || $screen == '18') && $response != ''){ //cust->payment process
			 //received mobile no and amount
			$arg = explode("*",$response);
			$merNo = trim($arg[0]);
			$mobErr = $this->General->mobileValidate($merNo);			
			$amt = $this->General->priceValidate(trim($arg[1]));
			//cust validation 
			$requestXML = '<VerifyMobile><MobileNo>'.$merNo.'</MobileNo></VerifyMobile>';
			$response = $this->ossApi('VerifyMobileNo',$requestXML,$mobile);
			//echo "<pre>"; print_r($response); echo "</pre>";exit; 
			if($mobErr == '1'){ //ERROR: invalid mobile no
				$flashMsg = $this->createMenu(39,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'39',$flashMsg,'2');
				echo $flashMsg; exit;			
			}else if($amt == ''){ //ERROR: invalid amount
				$flashMsg = $this->createMenu(40,$vendor,$session_id);				
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'40',$flashMsg,'2');
				echo $flashMsg; exit;			
			}else if($response['VerifyMobileNo']['VerifyDetails']['IsMerchant'] == 'False'){
				$flashMsg = $this->createMenu(18,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'18',$flashMsg,'2');
				echo $flashMsg; exit;
			}else{
				$requestXML = '<MerchantPaymentByCustomer><CustMobileNo>'.$mobile.'</CustMobileNo><MerchantMobileNo>'.$merNo.'</MerchantMobileNo><PaidAmount>'.$amt.'</PaidAmount></MerchantPaymentByCustomer>';
				$response = $this->ossApi('MerchantPaymentByCustomer',$requestXML,$mobile);
				//echo "<pre>"; print_r($response); echo "</pre>"; exit;
				if(isset($response['MerchantPaymentByCustomer']['MerchantPayment']['RequestID'])){
					$ossTransId = $response['MerchantPaymentByCustomer']['MerchantPayment']['RequestID'];					
					//intimate customer
					$flashMsg = $this->createMenu(49,$vendor,$session_id,'true');
					$flashMsg = str_replace('_AMT_',$response['MerchantPaymentByCustomer']['MerchantPayment']['TranAmount'],$flashMsg);
					$flashMsg = str_replace('_MERNO_',$response['MerchantPaymentByCustomer']['MerchantPayment']['MerchantMobileNo'],$flashMsg);
					$flashMsg = str_replace('_TRANID_',$response['MerchantPaymentByCustomer']['MerchantPayment']['RequestID'],$flashMsg);
					$flashMsg = str_replace('_BAL_',$response['MerchantPaymentByCustomer']['MerchantPayment']['OSSCardBalance'],$flashMsg);

					$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'49',$flashMsg,'2');
					echo $flashMsg; exit;
				}else{
					echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
				}			
			}		
		}
			
		if($screen == '2' && $response == '5'){ //cust->money transfer request		 	
			$flashMsg = $this->createMenu(41,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'41',$flashMsg,'2');
			echo $flashMsg; exit;
		}
		
		if(($screen == '41' || $screen == '42' || $screen == '43' || $screen == '44') && $response != ''){ //cust->money transfer process
			 //received mobile no and amount
			$arg = explode("*",$response);
			$custNo = trim($arg[0]);
			$mobErr = $this->General->mobileValidate($custNo);			
			$amt = $this->General->priceValidate(trim($arg[1]));
			//cust validation 
			$requestXML = '<VerifyMobile><MobileNo>'.$custNo.'</MobileNo></VerifyMobile>';
			$response = $this->ossApi('VerifyMobileNo',$requestXML,$mobile);
			//echo "<pre>"; print_r($response); echo "</pre>";exit; 
			if($mobErr == '1'){ //ERROR: invalid mobile no
				$flashMsg = $this->createMenu(42,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'42',$flashMsg,'2');
				echo $flashMsg; exit;			
			}else if($amt == ''){ //ERROR: invalid amount
				$flashMsg = $this->createMenu(43,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'43',$flashMsg,'2');
				echo $flashMsg; exit;			
			}else if($response['VerifyMobileNo']['VerifyDetails']['IsCustomer'] == 'False'){
				$flashMsg = $this->createMenu(44,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'44',$flashMsg,'2');
				echo $flashMsg; exit;
			}else{
				$requestXML = '<CustomerOSSCardMoneyTransfer><CustMobileNo>'.$mobile.'</CustMobileNo><ReceiverMobileNo>'.$custNo.'</ReceiverMobileNo><PaidAmount>'.$amt.'</PaidAmount></CustomerOSSCardMoneyTransfer>';
				$response = $this->ossApi('CustomerOSSCardMoneyTransfer',$requestXML,$mobile);
				//echo "<pre>"; print_r($response); echo "</pre>"; exit;
				if(isset($response['CustomerMoneyTransfer']['MoneyTransfer']['RequestID'])){
					$ossTransId = $response['CustomerMoneyTransfer']['MoneyTransfer']['RequestID'];					
					//intimate transferer
					$flashMsg = $this->createMenu(47,$vendor,$session_id,'true');
					$flashMsg = str_replace('_AMT_',$response['CustomerMoneyTransfer']['MoneyTransfer']['PaidAmount'],$flashMsg);
					$flashMsg = str_replace('_MERNO_',$response['CustomerMoneyTransfer']['MoneyTransfer']['ReceiverMobileNo'],$flashMsg);
					$flashMsg = str_replace('_TRANID_',$response['CustomerMoneyTransfer']['MoneyTransfer']['RequestID'],$flashMsg);
					$flashMsg = str_replace('_BAL_',$response['CustomerMoneyTransfer']['MoneyTransfer']['OSSCardBalance'],$flashMsg);
					
					$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'47',$flashMsg,'2');
					echo $flashMsg; exit;
				}else{
					echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
				}			
			}		
		}
		
		if($screen == '2' && $response != ''){ //Invalid option
			$flashMsg = $this->createMenu(2,$vendor,$session_id,'false',1);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'2',$flashMsg,'2');
			echo $flashMsg;	exit;
		}
		
		if($screen == '3' && $response == '2'){ //mer->bal
			$requestXML = '<GetBalance><MobileNo>'.$mobile.'</MobileNo></GetBalance>';
			$response = $this->ossApi('GetMerchantBalance',$requestXML,$mobile);
			//echo "<pri>";print_r($response);echo "</pri>";exit;			
			if(isset($response['GetBalance']['MerchantBalance']['BAL'])){
				$flashMsg = $this->createMenu(15,$vendor,$session_id,'true');
				$flashMsg = str_replace("_BAL_",$response['GetBalance']['MerchantBalance']['BAL'],$flashMsg);
				$flashMsg = str_replace("_CHARGE_",$response['GetBalance']['MerchantBalance']['RequestCharges'],$flashMsg);

				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'15',$flashMsg,'3');
				echo $flashMsg;	exit;
			}else{
				echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
			}		
		}
		
		if($screen == '3' && $response == '3'){ //mer->mini stmt
			$requestXML = '<GetMerchantMiniStatement><MobileNo>'.$mobile.'</MobileNo></GetMerchantMiniStatement>';
			$response = $this->ossApi('GetMerchantLastTransactions',$requestXML,$mobile);
			//print_r($response);
		 	if(isset($response['GetMerchantLastTransactions'])){
			 	$flashMsg =  "Last ".count($response['GetMerchantLastTransactions']['TransactionDetail'])." Transactions- ".$response['GetMerchantLastTransactions']['TransactionDetail']['0']['TranDate']." ".$response['GetMerchantLastTransactions']['TransactionDetail']['0']['TranAmount']." ".$response['GetMerchantLastTransactions']['TransactionDetail']['0']['CreditDebit']."/Tr ID ".$response['GetMerchantLastTransactions']['TransactionDetail']['0']['TransactionNo'].". ";
				if($response['GetMerchantLastTransactions']['TransactionDetail']['1'])
				$flashMsg .= $response['GetMerchantLastTransactions']['TransactionDetail']['1']['TranDate']." ".$response['GetMerchantLastTransactions']['TransactionDetail']['1']['TranAmount']." ".$response['GetMerchantLastTransactions']['TransactionDetail']['1']['CreditDebit']."/Tr ID ".$response['GetMerchantLastTransactions']['TransactionDetail']['1']['TransactionNo'].". "; 
				if($response['GetMerchantLastTransactions']['TransactionDetail']['2'])
				$flashMsg .= $response['GetMerchantLastTransactions']['TransactionDetail']['2']['TranDate']." ".$response['GetMerchantLastTransactions']['TransactionDetail']['2']['TranAmount']." ".$response['GetMerchantLastTransactions']['TransactionDetail']['2']['CreditDebit']."/Tr ID ".$response['GetMerchantLastTransactions']['TransactionDetail']['2']['TransactionNo'].". ";
				
				$flashMsg .= "Charges ".($response['GetMerchantLastTransactions']['TransactionDetail']['0']['RequestCharges']*100)." paisa/request.";

				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'',$flashMsg,'3');
				echo $this->msg($flashMsg,$vendor,$session_id);	exit;	
		 	}else{
				echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
			}	
		}
		
		if($screen == '3' && $response == '1'){ //mer->payment
			$flashMsg = $this->createMenu(4,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'4',$flashMsg,'3');
			echo $flashMsg; exit;
		}
		
		if(($screen == '4' || $screen == '45' || $screen == '46' || $screen == '7') && $response != ''){ //received mobile no and amount
			$arg = explode("*",$response);
			$custNo = trim($arg[0]);
			$mobErr = $this->General->mobileValidate($custNo);			
			$amt = $this->General->priceValidate(trim($arg[1]));
			//cust validation 
			$requestXML = '<VerifyMobile><MobileNo>'.$custNo.'</MobileNo></VerifyMobile>';
			$response = $this->ossApi('VerifyMobileNo',$requestXML,$mobile);
			//echo "<pre>"; print_r($response); echo "</pre>";exit; 
			if($mobErr == '1'){ //ERROR: invalid mobile no
				$flashMsg = $this->createMenu(45,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'45',$flashMsg,'3');
				echo $flashMsg; exit;			
			}else if($amt == ''){ //ERROR: invalid amount
				$flashMsg = $this->createMenu(46,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'46',$flashMsg,'3');
				echo $flashMsg; exit;			
			}else if($response['VerifyMobileNo']['VerifyDetails']['IsCustomer'] == 'False'){
				$flashMsg = $this->createMenu(7,$vendor,$session_id);
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'7',$flashMsg,'3');
				echo $flashMsg; exit;
			}else{
				$requestXML = '<MakeOrderRequest><CustMobileNo>'.$custNo.'</CustMobileNo><MerchantMobileNo>'.$mobile.'</MerchantMobileNo><OrderAmount>'.$amt.'</OrderAmount></MakeOrderRequest>';
				$response = $this->ossApi('MakeOrderRequest',$requestXML,$mobile);
				if(isset($response['MakeOrderRequest']['OrderRequest']['RequestID'])){
					$ossTransId = $response['MakeOrderRequest']['OrderRequest']['RequestID'];
					$flashMsg = $this->createMenu(5,$vendor,$session_id,'true');
					$flashMsg = str_replace('_CUSTNO_',$custNo,$flashMsg);
					$flashMsg = str_replace('_AMT_',$amt,$flashMsg);
					$flashMsg = str_replace('_TRANS_',$ossTransId,$flashMsg);
					
					$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'5',$flashMsg,'3');
					$tmp = $flashMsg;
					
							
					//confirm request to customer
					$vendor = $this->decideVendor($custNo);
					$flashMsg = $this->createMenu(6,$vendor,$session_id);
					$flashMsg = str_replace('_MERNO_',$mobile,$flashMsg);
					$flashMsg = str_replace('_AMT_',$amt,$flashMsg);

					$extra = $mobile."*".$ossTransId."*".$amt;
					$xnId = $this->ussdCalls($custNo,$vendor,$appid,'','6',$flashMsg,'2',$extra);
										
					echo $tmp;
					$this->handshake($custNo,$vendor,'1',$xnId);
					
					exit;
				}else{
					echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
				}			
			}
			
			
		}
		
		if($screen == '6' && $response == '1'){ //process payment
		 	$requestXML = '<ConfirmOrderRequestTran><CustMobileNo>'.$mobile.'</CustMobileNo></ConfirmOrderRequestTran>';
			$response = $this->ossApi('ConfirmOrderRequestTransaction',$requestXML,$mobile);
			//echo "<pre>";print_r($response);echo "<pre>"; exit; 
		 	if(isset($response['ConfirmOrderRequestTran']['TransactionDetails']['RequestID'])){		 		
				//intimate customer
				$flashMsg = $this->createMenu(8,$vendor,$session_id,'true');
				$flashMsg = str_replace('_AMT_',$response['ConfirmOrderRequestTran']['TransactionDetails']['TranAmount'],$flashMsg);
				$flashMsg = str_replace('_MERNO_',$response['ConfirmOrderRequestTran']['TransactionDetails']['MerchantMobileNo'],$flashMsg);
				$flashMsg = str_replace('_TRANID_',$response['ConfirmOrderRequestTran']['TransactionDetails']['RequestID'],$flashMsg);
				$flashMsg = str_replace('_BAL_',$response['ConfirmOrderRequestTran']['TransactionDetails']['CustomerCardBalance'],$flashMsg);
				
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'8',$flashMsg,'2');
				echo $flashMsg; exit;			
		 	}else{
				echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
			}			
		}
		
		if($screen == '6' && $response == '2'){ //ignore payment
		 	$this->releaseSession($session_id);			
		}
		
		
		if($screen == '3' && $response == '4'){ //mer->xn cancel
			$flashMsg = $this->createMenu(10,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'10',$flashMsg,'3');
			echo $flashMsg; exit;
		}
		
		if($screen == '10' && $response != ''){ //receive tran cancel req
			$arg = explode("*",$response);
			$tranNo = trim($arg[0]);
			$cancelAmt = trim($arg[1]);
			//validate amt
			$amt = $this->General->priceValidate($cancelAmt);
				 		
	 		if($amt != ''){ //valid amt format
		 		$requestXML = '<MerchantOrderCancelRequest><MerchantMobileNo>'.$mobile.'</MerchantMobileNo><TransactionID>'.$tranNo.'</TransactionID><CancelOrderAmount>'.$cancelAmt.'</CancelOrderAmount></MerchantOrderCancelRequest>';
				$response = $this->ossApi('MerchantOrderCancelRequest',$requestXML,$mobile);
		 		if(isset($response['OrderCancelRequest']['MerchantCancelOrder']['RequestID'])){
		 			//intimate merchant
					$flashMsg = $this->createMenu(13,$vendor,$session_id,'true');
					$flashMsg = str_replace('_AMT_',$response['OrderCancelRequest']['MerchantCancelOrder']['CancelOrderAmount'],$flashMsg);
					$flashMsg = str_replace('_TRANID_',$response['OrderCancelRequest']['MerchantCancelOrder']['RequestID'],$flashMsg);
					$flashMsg = str_replace('_CUSTNO_',$response['OrderCancelRequest']['MerchantCancelOrder']['CustomerMobileNo'],$flashMsg);
					
					$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'13',$flashMsg,'3');
					echo $flashMsg; exit;															
		 		}else{
					echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
				}		 						
	 		}else{
	 			echo $this->msg('Invalid amount. Try again.',$vendor,$session_id,'false');	exit;
	 		}
		}
		

		if($screen == '3' && $response == '5'){ //mer->order status	 	
			$flashMsg = $this->createMenu(16,$vendor,$session_id);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'16',$flashMsg,'3');
			echo $flashMsg; exit;	
		}
		
				
		if($screen == '16' && $response != '' && $ussd_user == '3'){ //merchant->order status
			$requestXML = '<GetMerchantOrderStatus><MerchantMobileNo>'.$mobile.'</MerchantMobileNo><TransactionID>'.trim($response).'</TransactionID></GetMerchantOrderStatus>';
			$response = $this->ossApi('GetMerchantOrderStatus',$requestXML,$mobile);
			//echo "<pre>";print_r($response); echo "</pre>";exit;
			if($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'] != ''){
				$statusId = trim($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['StatusID']);
				if($statusId == '1'){ // order request expired
					$flashMsg = "Tran ID:".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'].": Order request expired. Charges ".($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['RequestCharges']*100)." paisa/request.";	
				}else if($statusId == '2'){//order confirmed
					$flashMsg = "Tran ID:".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'].": Payment Successfully credited in your A/C towards payment made by customer ".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['CustomerMobileNo'].". Current Bal is Rs. ".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['MerchantAccountBalance'].". Charges ".($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '3'){//order cancelled
					$flashMsg = "Tran ID:".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'].": Order cancelled. Charges ".($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '4'){//Order Cancel Request UnderProcess
					$flashMsg = "Tran ID:".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'].": Cancellation request Successfully submitted to OSS. Request amt. Rs. ".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['CancelOrderAmount']." will be credited in customer ".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['CustomerMobileNo']." OSS A/C shortly. Charges ".($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '5'){//Order Cancel Request Approved 
					$flashMsg = "Tran ID:".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'].": Your cancellation request successfully done by OSS. Request amt Rs. ".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['CancelOrderAmount']." has been credited in customer ".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['CustomerMobileNo']." OSS A/C. Charges ".($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else if($statusId == '6'){//Order Cancel Request Rejected
					$flashMsg = "Tran ID:".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'].": Order cancel request rejected. Charges ".($response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}else{
					$flashMsg = "Tran ID:".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['TransactionID'].": ".$response['GetMerchantOrderRequestStatus']['MerchantOrderRequestDetails']['StatusRemark'].". Charges ".($response['GetCustomerOrderStatus']['MerchantOrderRequestDetails']['RequestCharges']*100)." paisa/request.";
				}
				
				$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'',$flashMsg,'3');
				echo $this->msg($flashMsg,$vendor,$session_id);	exit;
			}else{
				echo $this->msg($response['Error']['ErrorDesc'],$vendor,$session_id);	exit;
			}					 				
		}
		
		if($screen == '3' && $response != ''){ //Invalid option
			$flashMsg = $this->createMenu(3,$vendor,$session_id,'false',1);
			$xnId = $this->ussdCalls($mobile,$vendor,$appid,$session_id,'3',$flashMsg,'3');
			echo $flashMsg;	exit;
		}
		
		$this->autoRender = false;
	}
	
	function createMenu($screen,$vendor,$sessionId=null,$lastMsg=null,$invOpt=null){
		if(!isset($lastMsg))$lastMsg = 'false';
		
		$msg = '';
 		
 		if($invOpt == '1'){
 			$msg .= "Error: Invalid option chosen. Try again.\\n";
 		}else{
 			$scrTitle = $this->Ussd->query("select title from ussd_screens where id = ".$screen);
 			$msg .= $scrTitle[0]['ussd_screens']['title']."\\n";
 		}
 		
 		$scrOptions = $this->Ussd->query("select option_order,option_label from ussd_operations where toShow = 1 and screen_no = ".$screen." order by option_order asc");
 		foreach($scrOptions as $so){
 			$msg .= $so['ussd_operations']['option_order'].">".stripslashes($so['ussd_operations']['option_label'])."\\n";			 			
 		}
 		
 		if($vendor == '1'){//m365
 			$msg = str_replace("\\n","<br/>",$msg);
 			if($lastMsg == 'false'){
	 			$str = '<?xml version="1.0" encoding="UTF-8"?>
				<page version="2.">
				<session_id>'.$sessionId.'</session_id>
				<div>'.$msg.'</div>
				<navigation>
				<link accesskey="*" pageId="1"/>
				</navigation>
				</page>';
 			}else{
 				$str = '<?xml version="1.0" encoding="UTF-8"?>
				<page version="2.0">
				  <session_id>'.$sessionId.'</session_id>
				  <div>'.$msg.'</div>
				</page>';
 			}
 		}else if($vendor == '2'){ //imei
 			$str = str_replace("\\n","\n",$msg);  			
 		}else if($vendor == '3'){//globalUSSD
 			$msg = str_replace("\\n","<br/>",$msg);
 			if($lastMsg == 'false'){
	 			$str = '<?xml version="1.0" encoding="UTF-8"?>
				<page version="1.0">
				  <div protocol="ussd">
				 '.$msg.'
				    <input navigationId="form" name="reply"/>
				  </div>				  
				</page>';
 			}else{
 				$str = '<?xml version="1.0" encoding="UTF-8"?>
				<page version="1.0">
				  <div protocol="ussd">
				  '.$msg.'
				  </div>				  
				</page>';
 			}
 		}else{//infobip
 			$str =  '{"shouldClose":"'.$lastMsg.'","ussdMenu":"'.$msg.'","responseExitCode":200,"responseMessage":""}';	
 		}
 		
 		return $str;
	}
	
	function msg($msg,$vendor,$sessionId,$lastMsg=null){
		if(!isset($lastMsg))
		$lastMsg = 'true';
		
		if($vendor == '1'){//m365
	 			$msg = str_replace("\\n","<br/>",$msg);
	 			if($lastMsg == 'false'){
		 			$str = '<?xml version="1.0" encoding="UTF-8"?>
					<page version="2.">
					<session_id>'.$sessionId.'</session_id>
					<div>'.$msg.'</div>
					<navigation>
					<link accesskey="*" pageId="1"/>
					</navigation>
					</page>';
	 			}else{
	 				$str = '<?xml version="1.0" encoding="UTF-8"?>
					<page version="2.0">
					  <session_id>'.$sessionId.'</session_id>
					  <div>'.$msg.'</div>
					</page>';
	 			}
	 		}else if($vendor == '2'){ //imei
	 			$str = str_replace("\\n","\n",$msg);
	 		}else if($vendor == '3'){//globalUSSD
	 			$msg = str_replace("\\n","<br/>",$msg);
	 			if($lastMsg == 'false'){
		 			$str = '<?xml version="1.0" encoding="UTF-8"?>
					<page version="1.0">
					  <div protocol="ussd">
					 '.$msg.'
					    <input navigationId="form" name="reply"/>
					  </div>				  
					</page>';
	 			}else{
	 				$str = '<?xml version="1.0" encoding="UTF-8"?>
					<page version="1.0">
					  <div protocol="ussd">
					  '.$msg.'
					  </div>				  
					</page>';
	 			}
 			}else{//infobip
	 			$str =  '{"shouldClose":"'.$lastMsg.'","ussdMenu":"'.$msg.'","responseExitCode":200,"responseMessage":""}';	
	 		}
 		return $str;	
	}
}
?>