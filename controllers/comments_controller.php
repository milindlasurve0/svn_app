<?php
class CommentsController extends AppController {

	var $name = 'Comments';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $uses = array('User','Comment');
	var $components = array('RequestHandler');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
	}

	function index() {
		$this->Comment->recursive = 0;
		$this->set('comments', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid comment', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('comment', $this->Comment->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Comment->create();
			if ($this->Comment->save($this->data)) {
				$this->Session->setFlash(__('The comment has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The comment could not be saved. Please, try again.', true));
			}
		}
		$users = $this->Comment->User->find('list');
		$this->set(compact('users'));
	}
	
	function addComment(){
		$_REQUEST['text'] = urldecode($_REQUEST['text']);
		$email = $_REQUEST['email'];
		//$retCode = $_REQUEST['retCode'];
		

		$this->User->query("UPDATE users SET followup='".$_REQUEST['followupdate']."' WHERE id=". $_REQUEST['id']);
		if(trim($email)!= ""){
			$this->User->query("UPDATE users SET email='$email' WHERE id=" . $_REQUEST['id']);
			$this->General->tagUser('EMAIL',$_REQUEST['id']);
		}
			
		if($_REQUEST['type'] == 0){
			preg_match_all("/#[a-zA-Z0-9]+/", $_REQUEST['text'], $matches);
			$_REQUEST['text'] = trim(preg_replace("/#[a-zA-Z0-9]+/", "",$_REQUEST['text']));
			foreach($matches[0] as $match){
				$this->General->tagUser(substr($match,1),$_REQUEST['id'],$email);
			}
			$this->General->tagUser(date('Y-m-d'),$_REQUEST['id']);
		}
		if(!empty($_REQUEST['text'])){
			$this->data['Comment']['comment'] = addslashes($_REQUEST['text']);
			$this->data['Comment']['itemid'] = $_REQUEST['id'];
			$this->data['Comment']['itemtype']  = $_REQUEST['type']; //0 for admincomment & 1 for retailerComment
			$this->data['Comment']['user_id'] = $_SESSION['Auth']['User']['id'];
			$this->data['Comment']['created']  = date('Y-m-d H:i:s');
			$this->data['Comment']['modified']  = date('Y-m-d H:i:s');
			//$this->data['Comment']['mobile'] = $_REQUEST['mobile'];
			$this->Comment->create();
			if ($this->Comment->save($this->data)) {
				$comment['comments']['comment'] = $_REQUEST['text'];
				$comment['comments']['created'] = date('Y-m-d H:i:s');
				$comment['users']['name'] = $_SESSION['Auth']['User']['name'];
				$this->set('comment',$comment);
				$this->User->query("UPDATE comments SET mobile='".$_REQUEST['mobile']."' WHERE id=" . $this->Comment->id);
				$this->render('/elements/commentElement');
			}
		}
		else {
			$this->autoRender = false;
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid comment', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Comment->save($this->data)) {
				$this->Session->setFlash(__('The comment has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The comment could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Comment->read(null, $id);
		}
		$users = $this->Comment->User->find('list');
		$this->set(compact('users'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for comment', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Comment->delete($id)) {
			$this->Session->setFlash(__('Comment deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Comment was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>