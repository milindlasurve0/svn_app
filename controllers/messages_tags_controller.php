<?php
class MessagesTagsController extends AppController {

	var $name = 'MessagesTags';

	function index() {
		$this->MessagesTag->recursive = 0;
		$this->set('messagesTags', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid messages tag', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('messagesTag', $this->MessagesTag->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MessagesTag->create();
			if ($this->MessagesTag->save($this->data)) {
				$this->Session->setFlash(__('The messages tag has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The messages tag could not be saved. Please, try again.', true));
			}
		}
		$tags = $this->MessagesTag->Tag->find('list');
		$messages = $this->MessagesTag->Message->find('list');
		$this->set(compact('tags', 'messages'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid messages tag', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MessagesTag->save($this->data)) {
				$this->Session->setFlash(__('The messages tag has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The messages tag could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MessagesTag->read(null, $id);
		}
		$tags = $this->MessagesTag->Tag->find('list');
		$messages = $this->MessagesTag->Message->find('list');
		$this->set(compact('tags', 'messages'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for messages tag', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MessagesTag->delete($id)) {
			$this->Session->setFlash(__('Messages tag deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Messages tag was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>