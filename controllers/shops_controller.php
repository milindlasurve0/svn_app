<?php
class ShopsController extends AppController {

	var $name = 'Shops';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler','Shop');
	var $uses = array('Message','data_fun','Retailer','Distributor','SuperDistributor','User','SlabsUser','Product','Coupon','Invoice','RetailersCoupon','ShopTransaction','Receipt','Pnr');
	
	 
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allowedActions = array('cronUpdateBalanceLFRS','printCreditDebitNote','backCreditDebit','createNote','getCreditDebitNotes','createCreditDebitNotes','retailerProdActivation','products','index','initializeOpeningBalance','generateInvoice','logout','test','setSignature','saveSignature','topupReceipts','printRequest','script','issue','backReceipt','issueReceipt','printReceipt','printInvoice','retailerListing','PNRListing','smsOutgoingMonitoring');
		
		$states = $this->Retailer->query("SELECT id,name FROM locator_state ORDER BY name asc");
		$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $states['0']['locator_state']['id']." ORDER BY name asc");
		$this->Product->recursive = -1;
		$products = $this->Product->find('all',array('fields' => array('Product.name','Product.id','Product.outside_flag','Product.price','Product.validity','Product.code'), 'conditions' => array('Product.toshow' => 1), 'order' => 'Product.name asc'));
		
		$this->set('objShop',$this->Shop);
		if($this->Session->check('Auth.User')){
			$info = $this->Shop->getShopData($this->Session->read('Auth.User.id'),$this->Session->read('Auth.User.group_id'));
			$this->info = $info;
			$this->set('info',$info);
		}
		
		if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
			//$distributors = $this->Distributor->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));
			$this->Distributor->recursive = -1;
			$distributors = $this->Distributor->find('all', array(
			'fields' => array('Distributor.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount) as xfer'),
			'conditions' => array('Distributor.parent_id' => $this->info['id']),
			'joins' => array(
				array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Distributor.slab_id = slabs.id')
				),
				array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Distributor.user_id = users.id')
				),
				array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('Distributor.id = shop_transactions.ref2_id','Date(shop_transactions.timestamp) = "'.date('Y-m-d').'"', 'shop_transactions.type = 1')
				)
		
				),
			'order' => 'Distributor.company asc',
			'group'	=> 'Distributor.id'
				)
			);
					
		
			$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE group_id = " . DISTRIBUTOR);
			$this->set('slabs',$slabs);
			$this->set('distributors',$distributors);
			$this->set('records',$distributors);
			$this->set('modelName','Distributor');
		}
		else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			//$retailers = $this->Retailer->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));
			$this->Retailer->recursive = -1;
			$retailers = $this->Retailer->find('all', array(
			'fields' => array('Retailer.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount) as xfer'),
			'conditions' => array('Retailer.parent_id' => $this->info['id']),
			'joins' => array(
				array(
							'table' => 'slabs',
							'type' => 'left',
							'conditions'=> array('Retailer.slab_id = slabs.id')
				),
				array(
							'table' => 'users',
							'type' => 'left',
							'conditions'=> array('Retailer.user_id = users.id')
				),
				array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('Retailer.id = shop_transactions.ref2_id','Date(shop_transactions.timestamp) = "'.date('Y-m-d').'"', 'shop_transactions.type = 2')
				)		
				),
			'order' => 'Retailer.shopname asc',
			'group'	=> 'Retailer.id'
			)
			);
			
			$slabs = $this->Retailer->query("SELECT * FROM slabs as Slab WHERE group_id = " . RETAILER);
			$this->set('slabs',$slabs);
			$this->set('retailers',$retailers);
			$this->set('records',$retailers);
			$this->set('modelName','Retailer');
			
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $cities['0']['locator_city']['id']." ORDER BY name asc");
			$this->set('areas',$areas);
		}
		
		$this->set('states',$states);
		$this->set('cities',$cities);
		$this->set('products',$products);
		$this->layout = 'shop';
		
		$this->Auth->loginAction = array('controller' => 'shops', 'action' => 'index');
		$this->Auth->logoutRedirect = array('controller' => 'shops', 'action' => 'index');
		$this->Auth->loginRedirect = array('controller' => 'shops', 'action' => 'view');
	}
	
	
	
	function index(){
		if($this->Session->check('Auth.User'))$this->redirect(array('action' => 'view'));
		$this->render('index');
	}
	
	function view(){		
		if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR)
		$this->render('transfer');
		else{
			$this->Product->recursive = -1;
			$pnr_sign = $this->Product->findbyId(PNR_SIGNATURE);
			$this->set('pnr_sign',$pnr_sign);
			$this->render('view');
		}
	}
	
	function formDistributor(){
		$this->render('form_distributor');
	}
	
	function backDistributor(){
		$this->set('data',$this->data);
		$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
		$this->set('cities',$cities);
		$this->render('/elements/shop_form_distributor');
	}
	
	function backDistEdit($type){
		/*echo "<pre>";
		print_r($this->data);
		echo "</pre>";**/
		if($type == 'r'){
			$modName = 'Retailer';
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
			$this->set('cities',$cities);			
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
			$this->set('areas',$areas);
			
			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Retailer']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Retailer']['city']);			
			$areaName = $this->Retailer->query("SELECT name FROM locator_area WHERE id = ". $this->data['Retailer']['area_id']);
			
			$this->data['Retailer']['state'] = $stateName['0']['locator_state']['name'];
			$this->data['Retailer']['city'] = $cityName['0']['locator_city']['name'];
			$this->data['Retailer']['area'] = $areaName['0']['locator_area']['name'];
		}		
		if($type == 'd'){
			$modName = 'Distributor';
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Distributor']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Distributor']['city']);
						
			$this->data['Distributor']['state'] = $stateName['0']['locator_state']['name'];
			$this->data['Distributor']['city'] = $cityName['0']['locator_city']['name'];;
		}
		$tmparr[0] = $this->data;
		$this->set('editData',$tmparr);
		$this->set('type',$type);
		$this->render('/elements/edit_form_ele_retailer');
	}
	
	
	function createDistributor(){
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['Distributor']['tds_flag']) && $this->data['Distributor']['tds_flag'] == 'on') 
			$this->data['Distributor']['tds_flag'] = 1;
		else 	
			$this->data['Distributor']['tds_flag'] = 0;
			
		if(isset($this->data['confirm']))
			$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);	
		if(empty($this->data['Distributor']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['company'])){
			$empty[] = 'Company Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Distributor']['mobile'] = trim($this->data['Distributor']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Distributor']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}
		if($this->data['Distributor']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Distributor']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['area_range'])){
			$empty[] = 'Area Range';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['pan_number'])){
			$empty[] = 'PAN Number';
			$empty_flag = true;
			$to_save = false;
		}
		
		if($to_save){
			$exists = $this->General->checkIfUserExists($this->data['Distributor']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Distributor']['mobile']);
				if($user['group_id'] == SUPER_DISTRIBUTOR || $user['group_id'] == DISTRIBUTOR || $user['group_id'] == RETAILER){
					$to_save = false;
					$msg = "You cannot make this mobile as your distributor";
				}
			}
		}
		
		if(!$to_save){
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			if($empty_flag){				
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';				
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_distributor','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.' </div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_distributor','ajax');
			}			
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);	
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);	
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Distributor']['slab_id']);
			
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			$this->render('confirm_distributor','ajax');
		}
		else {
				
			$this->data['Distributor']['created'] = date('Y-m-d H:i:s');
			$this->data['Distributor']['modified'] = date('Y-m-d H:i:s');
			$this->data['Distributor']['balance'] = 0;
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);	
			$this->data['Distributor']['state'] = $state['0']['locator_state']['name'];
			
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);	
			$this->data['Distributor']['city'] = $city['0']['locator_city']['name'];
			
			if(!$exists){						
				$user = $this->General->registerUser($this->data['Distributor']['mobile'],RETAILER_REG,DISTRIBUTOR);
				$user = $user['User'];
				$new_user = 1;
			}
			else if($user['group_id'] == MEMBER){
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = DISTRIBUTOR;
				$this->User->save($userData);//make already user to a distributor
			}
			$this->data['Distributor']['user_id'] = $user['id'];
			$this->data['Distributor']['parent_id'] = $this->info['id'];
			
			$this->Distributor->create();
			if ($this->Distributor->save($this->data)) {
				if($this->data['login'] == 'on'){
					if($user['login_count'] == 0){
						$sms = $this->General->createMessage("SMS_NEW_RETAILER_DISTRIBUTOR",array('distributor',$user['mobile'],$user['syspass']));
					}
					else {
						$sms = $this->General->createMessage("SMS_RETAILER_DISTRIBUTOR",array('distributor'));
					}
					$this->General->sendMessage(SMS_SENDER,$user['mobile'],$sms,'template');
				}
				$mail_subject = "New Distributor Created";
				$mail_body = "SuperDistributor: " . $this->info['company'] . "<br/>";
				$mail_body .= "Distributor: " . $this->data['Distributor']['company'];
				$mail_body .= "Address: " . $this->data['Distributor']['address'];
				//$this->General->mailToAdmins($mail_subject, $mail_body);
				$this->Shop->updateSlab($this->data['Distributor']['slab_id'],$this->Distributor->id,DISTRIBUTOR);
				
				$this->data = null;
				$this->data['Retailer']['parent_id'] = $this->Distributor->id;
				$this->data['Retailer']['toshow'] = 0;
				$this->data['Retailer']['name'] = 'Cash Bill';
				$this->data['Retailer']['slab_id'] = 3;
				$this->data['Retailer']['shopname'] = 'Cash Bill';
				$this->data['Retailer']['created'] = date('Y-m-d H:i:s');
				$this->data['Retailer']['modified'] = date('Y-m-d H:i:s');
				$this->Retailer->create();
				$this->Retailer->save($this->data);
				$this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);
				$this->set('data',null);
				$this->render('/elements/shop_form_distributor','ajax');
			} else {
				$err_msg = '<div class="error_class">The Distributor could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_distributor','ajax');
			}
		}
	} 
	
	function allotCards(){
		$this->render('allotcards');
	}
	
	function backAllotment(){
		$this->set('data',$this->data);
		$this->render('/elements/shop_allot_cards');
	}
	
	function allotRetailCards(){
		$distributor = $this->data['Distributor']['id'];
		$empty_flag = true;
		$error_flag = false;
		$confirm = 0;
		$err_msg = "";
		if(isset($this->data['confirm'])){
			$confirm = $this->data['confirm'];
		}
		$coupons = array();
		$i = 0;
		if($this->data['Distributor']['id'] == 0){
			$err_msg = "Please select distributor";
			$error_flag = true;
		}
		else {
			while(!empty($this->data['Product']['id'][$i])){
				$product = $this->data['Product']['id'][$i];
				$start = $this->data['Product']['serialStart'][$i];
				$end = $this->data['Product']['serialEnd'][$i];
				if(!empty($start) || !empty($end)){
					$empty_flag = false;
					if(empty($end)){
						$error_flag = true;
						$err_msg = "End Serial Number cannot be empty in Row ".($i+1);
						break;
					}
					else if(empty($start)){
						$error_flag = true;
						$err_msg = "Start Serial Number cannot be empty in Row ".($i+1);
						break;
					}
					else {
						if($start > $end){
							$error_flag = true;
							$err_msg = "Start Serial Number cannot be greater than End Serial Number in Row ".($i+1);
							break;
						}
						else {
							$couponData = $this->Coupon->find('all',array('fields' => array('Coupon.product_id','Product.name','Product.price'),'conditions' => array('Coupon.serialNumber in ('.$start.','.$end.')'),
							'joins' => array(
												array(
													'table' => 'products',
													'type' => 'inner',
													'alias' => 'Product',
													'conditions' => array('Product.id = Coupon.product_id')
												)
										)
							));
							if($start == $end && count($couponData) < 1) {
								$error_flag = true;
								$err_msg = "Serial numbers provided are not valid in Row ".($i+1);
								break;
							}
							else if($start != $end && count($couponData) < 2){
								$error_flag = true;
								$err_msg = "Serial numbers provided are not valid in Row ".($i+1);
								break;
							}
							else {
								$prod1 = $couponData['0']['Coupon']['product_id'];
								if(count($couponData) == 2 && $prod1 != $couponData['1']['Coupon']['product_id']){
									$error_flag = true;
									$err_msg = "Serial numbers provided are not valid in Row ".($i+1);
									break;
								}
								else if($prod1 != $this->data['Product']['id'][$i]){
									$error_flag = true;
									$err_msg = "Product selected in Row ".($i+1) . " is not correct";
									break;
								}
								else {
									$this->data['Product']['name'][$i] =  $couponData['0']['Product']['name'];
									$this->data['Product']['price'][$i] =  $couponData['0']['Product']['price'];
									$couponData = $this->Coupon->find('all',array('fields' => array('Coupon.id','Coupon.serialNumber','Coupon.product_id','RetailersCoupon.superdistributor_id','RetailersCoupon.distributor_id'),
										'conditions' => array('Coupon.serialNumber >= '.$start, 'Coupon.serialNumber <='.$end),
										'joins' => array(
												array(
													'table' => 'retailers_coupons',
													'type' => 'inner',
													'alias' => 'RetailersCoupon',
													'conditions' => array('Coupon.id = RetailersCoupon.coupon_id','RetailersCoupon.distributor_id is null', 'RetailersCoupon.retailer_id is null', 'RetailersCoupon.superdistributor_id = ' . $this->info['id'])
												)
										)
									));
									$count = $end - $start + 1;
									if($count != count($couponData)){
										$err_msg = "Some of serial numbers in the range in Row ".($i+1) ." has been already alloted or not valid";
										$error_flag = true;
										break;
									}
									else {
										foreach($couponData as $coupon){
											if(!in_array($coupon['Coupon']['id'],$coupons)){
												$coupons[] = $coupon['Coupon']['id'];
											}
											else {
												$err_msg = "Overlapping in serial numbers before Row ".($i+1);
												$error_flag = true;
												break;
											}
										}
										if($error_flag)break;
									}
								}
							}
						}
					}
				}
				else {
					$this->data['Product']['empty'][$i] = true;
				}
				$i++;
			}
		}
		$this->set('data',$this->data);
		if($error_flag){
			$err_msg = '<div class="error_class">' . $err_msg . '</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_allot_cards','ajax');
		}
		else if($empty_flag){
			$this->Session->setFlash(__('<div class="error_class">Atleast one row should be filled</div>', true));
			$this->render('/elements/shop_allot_cards','ajax');
		}
		else if($confirm == 0){
			$distributor = $this->Distributor->find('first',array('conditions' => array('id' => $this->data['Distributor']['id'])));
			$this->set('distributor',$distributor);
			$this->render('confirm_allot','ajax');
		}
		else {
			$distributorData = $this->Distributor->find('first',array('conditions' => array('id' => $this->data['Distributor']['id'])));
			
			$mail_subject = "Cards Alloted";
			$mail_body = "SuperDistributor: " . $this->info['company'] . " allotted " . count($coupons) . " cards to Distributor: " . $distributorData['Distributor']['company'];
			//$this->General->mailToAdmins($mail_subject, $mail_body);
					
			$this->Retailer->query("UPDATE retailers_coupons set distributor_id = $distributor, allot_time='".date("Y-m-d H:i:s")."' where coupon_id in (" . implode(",",$coupons) .")");
			$this->set('data',null);
			$this->render('/elements/shop_allot_cards','ajax');
		}
		
	}
	
	function activateCards(){
		$this->render('activatecards');
	}
	
	function backActivation(){
		$this->set('data',$this->data);
		$this->render('/elements/shop_activate_cards');
	}
	
	function activateRetailCards(){
		$retailer = $this->data['Retailer']['id'];
		$empty_flag = true;
		$error_flag = false;
		$confirm = 0;
		$err_msg = "";
		if(isset($this->data['confirm'])){
			$confirm = $this->data['confirm'];
		}
		$coupons = array();
		$products = array();
		$i = 0;
		$amount = 0;
		if($this->data['Retailer']['id'] == 0){
			$err_msg = "Please select retailer";
			$error_flag = true;
		}
		else {
			while(!empty($this->data['Product']['id'][$i])){
				$product = $this->data['Product']['id'][$i];
				$start = $this->data['Product']['serialStart'][$i];
				$end = $this->data['Product']['serialEnd'][$i];
				if(!empty($start) || !empty($end)){
					$empty_flag = false;
					if(empty($end)){
						$error_flag = true;
						$err_msg = "End Serial Number cannot be empty in Row ".($i+1);
						break;
					}
					else if(empty($start)){
						$error_flag = true;
						$err_msg = "Start Serial Number cannot be empty in Row ".($i+1);
						break;
					}
					else {
						if($start > $end){
							$error_flag = true;
							$err_msg = "Start Serial Number cannot be greater than End Serial Number in Row ".($i+1);
							break;
						}
						else {
							$couponData = $this->Coupon->find('all',array('fields' => array('Coupon.product_id','Product.name','Product.price'),'conditions' => array('Coupon.serialNumber in ('.$start.','.$end.')'),
							'joins' => array(
												array(
													'table' => 'products',
													'type' => 'inner',
													'alias' => 'Product',
													'conditions' => array('Product.id = Coupon.product_id')
												)
										)
							));
							if($start == $end && count($couponData) < 1) {
								$error_flag = true;
								$err_msg = "Serial numbers provided are not valid in Row ".($i+1);
								break;
							}
							else if($start != $end && count($couponData) < 2){
								$error_flag = true;
								$err_msg = "Serial numbers provided are not valid in Row ".($i+1);
								break;
							}
							else {
								$prod1 = $couponData['0']['Coupon']['product_id'];
								if(count($couponData) == 2 && $prod1 != $couponData['1']['Coupon']['product_id']){
									$error_flag = true;
									$err_msg = "Serial numbers provided are not valid in Row ".($i+1);
									break;
								}
								else if($prod1 != $this->data['Product']['id'][$i]){
									$error_flag = true;
									$err_msg = "Product selected in Row ".($i+1) . " is not correct";
									break;
								}
								else {
									$this->data['Product']['name'][$i] =  $couponData['0']['Product']['name'];
									$this->data['Product']['price'][$i] =  $couponData['0']['Product']['price'];
									$couponData = $this->Coupon->find('all',array('fields' => array('Coupon.id','Coupon.serialNumber','Coupon.product_id','RetailersCoupon.superdistributor_id','RetailersCoupon.distributor_id'),
										'conditions' => array('Coupon.serialNumber >= '.$start, 'Coupon.serialNumber <='.$end),
										'joins' => array(
												array(
													'table' => 'retailers_coupons',
													'type' => 'inner',
													'alias' => 'RetailersCoupon',
													'conditions' => array('Coupon.id = RetailersCoupon.coupon_id','RetailersCoupon.distributor_id = ' . $this->info['id'], 'RetailersCoupon.retailer_id is null', 'RetailersCoupon.superdistributor_id = ' . $this->info['parent_id'])
												)
										)
									));
									$count = $end - $start + 1;
									if($count != count($couponData)){
										$err_msg = "Some of serial number(s) in Row ".($i+1) ." has been already activated or not valid for you";
										$error_flag = true;
										break;
									}
									else {
										if(!isset($products[$prod1]))
										$products[$prod1] = 0;
										
										$products[$prod1] = $products[$prod1] +  count($couponData);
										$amount += count($couponData) * $this->data['Product']['price'][$i];
										foreach($couponData as $coupon){
											if(!in_array($coupon['Coupon']['id'],$coupons)){
												$coupons[] = $coupon['Coupon']['id'];
											}
											else {
												$err_msg = "Overlapping in serial numbers before Row ".($i+1);
												$error_flag = true;
												break;
											}
										}
										if($error_flag)break;
									}
								}
							}
						}
					}
				}
				else {
					$this->data['Product']['empty'][$i] = true;
				}
				$i++;
			}
			if($amount > $this->info['balance']){
				$err_msg = "You don't have enough balance to activate all above cards";
				$error_flag = true;
			}
		}
		
		$this->set('data',$this->data);
		if($error_flag){
			$err_msg = '<div class="error_class">' . $err_msg . '</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_activate_cards','ajax');
		}
		else if($empty_flag){
			$this->Session->setFlash(__('<div class="error_class">Atleast one row should be filled</div>', true));
			$this->render('/elements/shop_activate_cards','ajax');
		}
		else if($confirm == 0){
			$retailer = $this->Retailer->find('first',array('conditions' => array('id' => $this->data['Retailer']['id'])));
			$this->set('retailer',$retailer);
			$this->render('confirm_activate','ajax');
		}
		else {
			//whole logic of activation and money transferring & comission exchange
			$trans_id = $this->Shop->transactionsOnDistributorActivation($this->info['id'],$this->info['parent_id'],$coupons,$products);
			
			//create invoice of retailer
			$invoice = $this->Shop->getNewInvoice($retailer,$this->info['id'],RETAILER,DISTRIBUTOR_ACTIVATION);
			//add trans_id in invoice
			$this->Shop->addToInvoice($invoice,$trans_id);
			
			$amount = $this->Shop->getInvoiceAmount($invoice,date('Y-m-d'));
			$number = $this->Shop->getInvoiceNumber($invoice,date('Y-m-d'));
			$this->Invoice->query("UPDATE invoices SET amount = $amount,invoice_number='$number' WHERE id = $invoice");
			
			$retailerData = $this->Retailer->find('first',array('conditions' => array('id' => $this->data['Retailer']['id'])));
			
			$mail_subject = "Cards Activated";
			$mail_body = "Distributor: " . $this->info['company'] . " activated " . count($coupons) . " cards for Retailer: " . $retailerData['Retailer']['shopname'];
			//$this->General->mailToAdmins($mail_subject, $mail_body);
			
			$this->Retailer->query("UPDATE retailers_coupons set retailer_id = $retailer, activate_time='".date("Y-m-d H:i:s")."' where coupon_id in (" . implode(",",$coupons) . ")");
			$this->set('data',null);
			$this->render('/elements/shop_activate_cards','ajax');
		}
		
	}
	
	function allRetailer(){
		$this->render('allretailer');
	}
	
	function retailerListing($id = null){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		$retailers = array();
		if($id == null)$this->set('empty',1);
		else {
			$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
			$this->set('distributor',$shop['company']);
			$this->set('distributor_id',$id);
			if($shop['parent_id'] == $this->info['id']){
				$this->Retailer->recursive = -1;
				$retailers = $this->Retailer->find('all', array(
				'fields' => array('Retailer.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount) as xfer'),
				'conditions' => array('Retailer.parent_id' => $id),
				'joins' => array(
					array(
								'table' => 'slabs',
								'type' => 'left',
								'conditions'=> array('Retailer.slab_id = slabs.id')
					),
					array(
								'table' => 'users',
								'type' => 'left',
								'conditions'=> array('Retailer.user_id = users.id')
					),
					array(
								'table' => 'shop_transactions',
								'type' => 'left',
								'conditions'=> array('Retailer.id = shop_transactions.ref2_id','Date(shop_transactions.timestamp) = "'.date('Y-m-d').'"', 'shop_transactions.type = 2')
					)		
					),
				'order' => 'Retailer.shopname asc',
				'group'	=> 'Retailer.id'
				)
				);
			}
			else {
				$retailers = array();
			}
		}
		$this->set('retailers',$retailers);
		$this->render('retailer_listing');
	}
	
	function editRetailer($type,$id){
		if(!in_array($type, array('d','r')))
		$this->redirect(array('action' => 'allRetailer'));

		if($type == 'r'){
		$tableName = 'retailers';
		$modName = 'Retailer';
		$editData = $this->Retailer->find('all', array(
			'fields' => array('Retailer.*', 'slabs.name','users.mobile'),
			'conditions' => array('Retailer.id' => $id),
			'joins' => array(
				array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Retailer.slab_id = slabs.id')
				),
				array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Retailer.user_id = users.id')
				)		
				))
			);
			$stateName = trim($editData['0']['Retailer']['state']);
			$cityName = trim($editData['0']['Retailer']['city']);
		}
		
		if($type == 'd'){
		$tableName = 'distributors';
		$modName = 'Distributor';
		$editData = $this->Distributor->find('all', array(
			'fields' => array('Distributor.*', 'slabs.name','users.mobile'),
			'conditions' => array('Distributor.id' => $id),
			'joins' => array(
				array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Distributor.slab_id = slabs.id')
				),
				array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Distributor.user_id = users.id')
				)
		
				))
			);
			$stateName = trim($editData['0']['Distributor']['state']);
		}

		if(!$this->Retailer->query("SELECT id FROM ".$tableName." WHERE parent_id = ".$this->info['id']." and id=".$id))
		$this->redirect(array('action' => 'allRetailer'));
		
		
		$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = (select id from locator_state where name='".$stateName."') ORDER BY name asc");
		$this->set('cities',$cities);
		
		$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = (select id from locator_city where name='".$cityName."') ORDER BY name asc");
		$this->set('areas',$areas);



		/*echo $this->info['id'];
		echo "<pre>";
		print_r($editData);
		echo "</pre>";*/
		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('editData',$editData);
		$this->set('modName',$modName);
		
		$this->render('edit_form_retailer');
	}
	
	function showDetails($type,$id,$dist=null){
		
		if(!in_array($type, array('d','r')) || empty($id)){
			if($dist == null)
				$this->redirect(array('action' => 'allRetailer'));
			else 
				$this->redirect(array('action' => 'retailerListing'));	
		}
		
		if($type == 'r'){
			$tableName = 'retailers';
			$modName = 'Retailer';
			$this->Retailer->recursive = -1;
			$editData = $this->Retailer->find('all', array(
			'fields' => array('Retailer.*', 'slabs.name','users.mobile'),
			'conditions' => array('Retailer.id' => $id),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Retailer.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Retailer.user_id = users.id')
			)
			))
			);
			
			if($dist != null){
				$parent = $editData['0']['Retailer']['parent_id'];
				$shop = $this->Shop->getShopDataById($parent,DISTRIBUTOR);
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR && ($this->$parent != $dist || $shop['parent_id'] != $this->info['id']))
					$this->redirect(array('action' => 'retailerListing'));
				else 
					$this->redirect(array('action' => 'allRetailer'));	
				$this->set('dist',$dist);	
			}
			
			$state = $editData['0']['Retailer']['state'];	
			$city = $editData['0']['Retailer']['city'];	
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $editData['0']['Retailer']['slab_id']);
			$area = $this->Retailer->query("SELECT name FROM locator_area WHERE id = " . $editData['0']['Retailer']['area_id']);	
			
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city);
			$this->set('state',$state);
			$this->set('area',$area['0']['locator_area']['name']);
		}

		if($type == 'd'){
			$tableName = 'distributors';
			$modName = 'Distributor';
			$this->Distributor->recursive = -1;
			$editData = $this->Distributor->find('all', array(
				'fields' => array('Distributor.*', 'slabs.name','users.mobile'),
				'conditions' => array('Distributor.id' => $id),
				'joins' => array(
				array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Distributor.slab_id = slabs.id')
				),
				array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Distributor.user_id = users.id')
				)
	
				))
			);
			$state = $editData['0']['Distributor']['state'];
			$city = $editData['0']['Distributor']['city'];	
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $editData['0']['Distributor']['slab_id']);
				
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city);
			$this->set('state',$state);			
		}

		if(!$this->Retailer->query("SELECT id FROM ".$tableName." WHERE parent_id = ".$this->info['id']." and id=".$id) && $dist == null)
			$this->redirect(array('action' => 'allRetailer'));

		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('editData',$editData);
		$this->set('modName',$modName);
		//$this->render('showDetails');			
	}
	
	function editRetValidation(){				
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
	if(isset($this->data['confirm']))
			$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);	
		if(empty($this->data['Retailer']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['pan_number'])){
			$empty[] = 'Pan Number';
			$empty_flag = true;
			$to_save = false;
		}
		
		if($this->data['Retailer']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['area_id'] == 0){
			$empty[] = 'Area';
			$empty_flag = true;
			$to_save = false;
		}
		
		if(empty($this->data['Retailer']['pin'])){
			$empty[] = 'Pin Code';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['shopname'])){
			$empty[] = 'Shop Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
				
		if(!$to_save){
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
			$this->set('areas',$areas);
			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Retailer']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Retailer']['city']);			
			$areaName = $this->Retailer->query("SELECT name FROM locator_area WHERE id = ". $this->data['Retailer']['area_id']);
			if($empty_flag){
				$this->data['Retailer']['state'] = $stateName['0']['locator_state']['name'];
				$this->data['Retailer']['city'] = $cityName['0']['locator_city']['name'];
				$this->data['Retailer']['area'] = $areaName['0']['locator_area']['name'];
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','r');
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';				
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/edit_form_ele_retailer','ajax');
				
			}
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);	
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);	
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Retailer']['slab_id']);
			$area = $this->Retailer->query("SELECT name FROM locator_area WHERE id = " . $this->data['Retailer']['area_id']);	
			
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			$this->set('area',$area['0']['locator_area']['name']);
			$this->render('confirm_ret_edit','ajax');
		}
		else {
			$id = $this->data['Retailer']['id'];
			$name = $this->data['Retailer']['name'];
			$panNumber = $this->data['Retailer']['pan_number'];
			
			$email = $this->data['Retailer']['email'];
							
			
			$stateQ = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);	
			$state = $stateQ['0']['locator_state']['name'];
			
			$cityQ = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);	
			$city = $cityQ['0']['locator_city']['name'];
			
			$areaId = $this->data['Retailer']['area_id'];
			$pin = $this->data['Retailer']['pin'];
			$shopName = $this->data['Retailer']['shopname'];
			$Address = $this->data['Retailer']['address'];
			$slab = $this->data['Retailer']['slab_id'];
			//old slab id 
			$oldSlabQ = $this->Distributor->query("select slab_id from retailers where id=".$id);
			$oldSlabId = $oldSlabQ['0']['retailers']['slab_id'];
			//
								
			$modified = date('Y-m-d H:i:s');
			
			$go = 0;
			if($this->Retailer->query("SELECT id FROM retailers WHERE parent_id = ".$this->info['id']." and id=".$id)){
				if ($this->Distributor->query("update retailers set name='".$name."',pan_number='".$panNumber."', email='".$email."', state='".$state."', city='".$city."', area_id='".$areaId."', pin='".$pin."', shopname='".$shopName."', address='".$Address."', slab_id='".$slab."', modified='".$modified."' where id=".$id)) {
					if($oldSlabId != $slab)
					$this->Shop->updateSlab($slab,$id,RETAILER);
					$go = 1;
				}else{
					$err = 'The Retailer could not be saved. Please, try again.';
				}
			}else{
				$err = "You don't have permission to edit this retailer.";
			}
			
			if ($go == 1) {				
				$this->set('data',null);
				$retailer = $this->Retailer->find('all', array(
				'fields' => array('Retailer.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount)'),
				'conditions' => array('Retailer.parent_id' => $this->info['id']),
				'joins' => array(
					array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Retailer.slab_id = slabs.id')
					),
					array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Retailer.user_id = users.id')
					),
					array(
								'table' => 'shop_transactions',
								'type' => 'left',
								'conditions'=> array('Retailer.id = shop_transactions.ref2_id','datediff(now(),shop_transactions.timestamp) < 1 ', 'shop_transactions.type = 2')
					)
			
					),
				'order' => 'Retailer.name asc',
				'group'	=> 'Retailer.id')
				);
				
				$this->set('records',$retailer);
				$this->render('/elements/allRetailers','ajax');
			} else {
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','r');				
				$err_msg = '<div class="error_class">'.$err.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}	
	}
	
	function editDistValidation(){		
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
			$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);	
		if(empty($this->data['Distributor']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		
		if(empty($this->data['Distributor']['company'])){
			$empty[] = 'Company Name';
			$empty_flag = true;
			$to_save = false;
		}
		
		
		
		if($this->data['Distributor']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Distributor']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		
		

		if(empty($this->data['Distributor']['area_range'])){
			$empty[] = 'Area Range';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['pan_number'])){
			$empty[] = 'PAN Number';
			$empty_flag = true;
			$to_save = false;
		}		
		if(!$to_save){			
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Distributor']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Distributor']['city']);
			//$data['Retailer']['state'] = $stateName['0']['locator_state']['name'];
			
			if($empty_flag){
				$this->data['Distributor']['state'] = $stateName['0']['locator_state']['name'];
				$this->data['Distributor']['city'] = $cityName['0']['locator_city']['name'];
				$tArr[0] = $this->data;				
				$this->set('editData',$tArr);
				$this->set('type','d');
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';				
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);	
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);	
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Distributor']['slab_id']);
			
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			$this->render('confirm_dist_edit','ajax');
		}
		else {
			$id = $this->data['Distributor']['id'];
			$name = $this->data['Distributor']['name'];
			$panNumber = $this->data['Distributor']['pan_number'];
			$companyName = $this->data['Distributor']['company'];
			$email = $this->data['Distributor']['email'];
							
			
			$stateQ = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);	
			$state = $stateQ['0']['locator_state']['name'];
			
			$cityQ = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);	
			$city = $cityQ['0']['locator_city']['name'];
			
			$areaRange = $this->data['Distributor']['area_range'];
			$compAddress = $this->data['Distributor']['address'];
			$slab = $this->data['Distributor']['slab_id'];
			
			//old slab id 
			$oldSlabQ = $this->Distributor->query("select slab_id from distributors where id=".$id);
			$oldSlabId = $oldSlabQ['0']['distributors']['slab_id'];
			//
			$modified = date('Y-m-d H:i:s');			
			$go = 0;
			if($this->Retailer->query("SELECT id FROM distributors WHERE parent_id = ".$this->info['id']." and id=".$id)){
				if ($this->Distributor->query("update distributors set name='".$name."',pan_number='".$panNumber."', company='".$companyName."', email='".$email."', state='".$state."', city='".$city."', area_range='".$areaRange."', address='".$compAddress."', slab_id='".$slab."', modified='".$modified."' where id=".$id)) {
					if($oldSlabId != $slab)
					$this->Shop->updateSlab($slab,$id,DISTRIBUTOR);
					$go = 1;
				}else{
					$err = 'The Distributor could not be saved. Please, try again.';
				}
			}else{
				$err = "You don't have permission to edit this distributor.";
			}
			
			if ($go == 1) {				
				$this->set('data',null);
				$distributors = $this->Distributor->find('all', array(
				'fields' => array('Distributor.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount) as xfer'),
				'conditions' => array('Distributor.parent_id' => $this->info['id']),
				'joins' => array(
					array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Distributor.slab_id = slabs.id')
					),
					array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Distributor.user_id = users.id')
					),
					array(
								'table' => 'shop_transactions',
								'type' => 'left',
								'conditions'=> array('Distributor.id = shop_transactions.ref2_id','datediff(now(),shop_transactions.timestamp) < 1 ', 'shop_transactions.type = 1')
					)
			
					),
				'order' => 'Distributor.name asc',
				'group'	=> 'Distributor.id'
					)
				);
				
				$this->set('records',$distributors);
				$this->render('/elements/allRetailers','ajax');
			} else {
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','d');
				$err_msg = '<div class="error_class">'.$err.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}			
	}
	
	function formRetailer(){		
		$this->render('form_retailer');
	}
	
	function backRetailer(){
		$this->set('data',$this->data);
		$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
		$this->set('cities',$cities);
		$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
		$this->set('areas',$areas);
		$this->render('/elements/shop_form_retailer');
	}
	
	function createRetailer(){
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
			$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);	
		if(empty($this->data['Retailer']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Retailer']['mobile'] = trim($this->data['Retailer']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Retailer']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}
		if($this->data['Retailer']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['area_id'] == 0){
			$empty[] = 'Area';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['pin'])){
			$empty[] = 'Pin Code';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['shopname'])){
			$empty[] = 'Shop Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		
		if($to_save){
			$exists = $this->General->checkIfUserExists($this->data['Retailer']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Retailer']['mobile']);
				if($user['group_id'] == SUPER_DISTRIBUTOR || $user['group_id'] == DISTRIBUTOR || $user['group_id'] == RETAILER){
					$to_save = false;
					$msg = "You cannot make this mobile as your retailer";
				}
			}
		}
		
		if(!$to_save){
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
			$this->set('areas',$areas);
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';				
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_retailer','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_retailer','ajax');
			}
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);	
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);	
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Retailer']['slab_id']);
			$area = $this->Retailer->query("SELECT name FROM locator_area WHERE id = " . $this->data['Retailer']['area_id']);	
			
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			$this->set('area',$area['0']['locator_area']['name']);
			
			$this->render('/shops/confirm_retailer','ajax');
		}
		else {
				
			$this->data['Retailer']['created'] = date('Y-m-d H:i:s');
			$this->data['Retailer']['modified'] = date('Y-m-d H:i:s');
			$this->data['Retailer']['balance'] = 0;
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);	
			$this->data['Retailer']['state'] = $state['0']['locator_state']['name'];
			
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);	
			$this->data['Retailer']['city'] = $city['0']['locator_city']['name'];
			if(!$exists){						
				$user = $this->General->registerUser($this->data['Retailer']['mobile'],RETAILER_REG,RETAILER);
				$user = $user['User'];
				$new_user = 1;
			}
			else if($user['group_id'] == MEMBER){
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = RETAILER;
				$this->User->save($userData);//make already user to a retailer
			}
			$this->data['Retailer']['user_id'] = $user['id'];
			$this->data['Retailer']['parent_id'] = $this->info['id'];
			
			$this->Retailer->create();
			if ($this->Retailer->save($this->data)) {
				$this->General->updateLocation($this->data['Retailer']['area_id']);
				if($this->data['login'] == 'on'){
					if($user['login_count'] == 0){
						$sms = $this->General->createMessage("SMS_NEW_RETAILER_DISTRIBUTOR",array('retailer',$user['mobile'],$user['syspass']));
					}
					else {
						$sms = $this->General->createMessage("SMS_RETAILER_DISTRIBUTOR",array('retailer'));
					}
					$this->General->sendMessage(SMS_SENDER,$user['mobile'],$sms,'template');	
				}
				$mail_subject = "New Retailer Created";
				$mail_body = "Distributor: " . $this->info['company'] . "<br/>";
				$mail_body .= "Retailer: " . $this->data['Retailer']['shopname'] . "<br/>";
				$mail_body .= "Address: " . $this->data['Retailer']['address'];
				//$this->General->mailToAdmins($mail_subject, $mail_body);
				
				$this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);
					
				$this->set('data',null);
				$this->render('/elements/shop_form_retailer','ajax');
			} else {
				$err_msg = '<div class="error_class">The Retailer could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_retailer','ajax');
			}
		}
	}
	
	
	function changePassword(){
		$this->render('setting');
	}
	
	function setSignature(){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		if($this->info['signature_flag'] == 1){
			$data['signature'] = 'on';
		}
		$data['text'] = $this->info['signature'];
		$this->set('data',$data);
		$this->render('signature');
	}
	
	function saveSignature(){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		$signature = 0;
		if(isset($this->data['signature']) && $this->data['signature'] == 'on'){
			$signature = 1;
		}
		$this->set('data',$this->data);
		if($signature == 1 && empty($this->data['text'])){
			$err_msg = '<div class="error_class">Please enter your 60 character signature.</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_signature','ajax');
		}
		else {
			$this->Retailer->updateAll(array('Retailer.signature_flag' => $signature, 'Retailer.signature' => '"'.$this->data['text'].'"'),array('Retailer.id' => $this->info['id']));
			if($signature == 0) $msg = 'Signature Removed Successfully';
			else $msg = 'Signature Saved Successfully';
			$err_msg = '<div class="success">'.$msg.'</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_signature','ajax');
		}
	}
	
	function accountHistory($date=null){
		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
				
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
					$transactions = $this->Retailer->query("SELECT * FROM (
						(SELECT shop_transactions.id, shop_transactions.amount,'SMSTadka Credit' as name,'' as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref2_id = ".$this->info['id'] . " AND type = " . ADMIN_TRANSFER . ") 
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,distributors.company as name,distributors.id as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN distributors ON (distributors.id = ref2_id) WHERE ref1_id = ".$this->info['id'] . " AND type = " . SDIST_DIST_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,st1.type as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$this->info['id'] . " AND shop_transactions.type in (" . TDS_SUPERDISTRIBUTOR . "," . COMMISSION_SUPERDISTRIBUTOR . "))
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,distributors.company as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) INNER JOIN distributors ON (distributors.id = shop_transactions.ref1_id AND distributors.parent_id = ".$this->info['id'].") WHERE shop_transactions.type in (" . TDS_DISTRIBUTOR . "," . COMMISSION_DISTRIBUTOR . "))
						UNION
						(SELECT 'TOCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,'' as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit WHERE shop_creditdebit.to_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".SUPER_DISTRIBUTOR." AND shop_creditdebit.from_id is null)		
						UNION
						(SELECT 'FROMCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,distributors.company as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit INNER JOIN distributors ON (distributors.id = shop_creditdebit.to_id) WHERE shop_creditdebit.api_flag = 0 AND shop_creditdebit.from_id =  ".$this->info['id'] . "  AND shop_creditdebit.to_groupid = ".DISTRIBUTOR.")		
					) as transactions where Date(transactions.timestamp) >= '$date_from' AND  Date(transactions.timestamp) <= '$date_to' order by transactions.timestamp desc,transactions.id desc");
				}
				else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
					$transactions = $this->Retailer->query("SELECT * FROM (
						(SELECT shop_transactions.id,shop_transactions.amount,retailers.shopname as name,retailers.id as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN retailers ON (retailers.id = ref2_id) WHERE ref1_id = ".$this->info['id'] . " AND type = " . DIST_RETL_BALANCE_TRANSFER . ") 
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,super_distributors.company as name,super_distributors.id as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN super_distributors ON (super_distributors.id = ref1_id) WHERE ref2_id = ".$this->info['id'] . " AND type = " . SDIST_DIST_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,retailers.shopname as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN retailers_coupons ON (retailers_coupons.coupon_id in (ref2_id)) INNER JOIN retailers ON (retailers.id = retailers_coupons.retailer_id) WHERE ref1_id = ".$this->info['id'] . " AND type = " . DISTRIBUTOR_ACTIVATION . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,st1.type as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$this->info['id'] . " AND shop_transactions.type in (" . TDS_DISTRIBUTOR . "," . COMMISSION_DISTRIBUTOR . "))
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,retailers.shopname as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) INNER JOIN retailers ON (retailers.id = shop_transactions.ref1_id AND retailers.parent_id = ".$this->info['id'].") WHERE shop_transactions.type in (" . TDS_RETAILER . "," . COMMISSION_RETAILER . "))
						UNION
						(SELECT 'TOCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,super_distributors.company as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit LEFT JOIN super_distributors ON (super_distributors.id = shop_creditdebit.from_id) WHERE shop_creditdebit.to_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".DISTRIBUTOR.")		
						UNION
						(SELECT 'FROMCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,retailers.shopname as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit INNER JOIN retailers ON (retailers.id = shop_creditdebit.to_id) WHERE shop_creditdebit.from_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".RETAILER.")
					) as transactions where Date(transactions.timestamp) >= '$date_from' AND  Date(transactions.timestamp) <= '$date_to' order by transactions.timestamp desc,transactions.id desc");
				}
				else if($this->Session->read('Auth.User.group_id') == RETAILER){
					$transactions = $this->Retailer->query("SELECT * FROM (
						(SELECT shop_transactions.id,shop_transactions.amount,distributors.company as name,distributors.id as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN distributors ON (distributors.id = ref1_id) WHERE ref2_id = ".$this->info['id'] . " AND type = " . DIST_RETL_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,products.name as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN products ON (ref2_id = products.id) WHERE ref1_id = ".$this->info['id'] . " AND type = " . RETAILER_ACTIVATION . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,st1.type as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$this->info['id'] . " AND shop_transactions.type in (" . TDS_RETAILER . "," . COMMISSION_RETAILER . "))
						UNION
						(SELECT 'TOCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,distributors.company as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit INNER JOIN distributors ON (distributors.id = shop_creditdebit.from_id) WHERE shop_creditdebit.to_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".RETAILER.")
					) as transactions where Date(transactions.timestamp) >= '$date_from' AND  Date(transactions.timestamp) <= '$date_to' order by transactions.timestamp desc,transactions.id desc");
				}
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			
			}
			else {
				$transactions = array();
			}
			
		}
		else {
			$transactions = array();
		}
		//$this->printArray($transactions);
		if($date == null) $this->set('empty',0);
		$this->set('transactions',$transactions);
		$this->render('account_history');
	}
	
	function cardsSold(){
		$this->render('cards_sold');
	}
	
	function cardsAllotted($date=null,$distributor=null){
		$this->RetailersCoupon->recursive = -1;
		
		if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
			$error = false;
			if($date == null)$error = true;
			$cond1 = '';
			$cond2 = '';
			if(!$error && $date != -1){	
				$dates = explode("-",$date);
				$date_from = $dates[0];
				$date_to = $dates[1];
				if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
					$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
					$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
					$cond1 = "Date(RetailersCoupon.allot_time) >= '$date_from' AND Date(RetailersCoupon.allot_time) <= '$date_to'";
				}
				else $error = true;
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			}
			if(!$error && $distributor !=null){
				$cond2 = "Distributor.id = $distributor";
				$this->set('distributor_id',$distributor);
			}
			
			if(!$error) {
				$alloted = $this->RetailersCoupon->find('all',array('fields' => array('Distributor.company','Date(RetailersCoupon.allot_time) as date','Product.name','Product.id','Distributor.id'), 'conditions' => array('RetailersCoupon.superdistributor_id' => $this->info['id'], 'RetailersCoupon.distributor_id is not null',$cond1,$cond2),
				'joins' => array(
						array(
							'table' => 'distributors',
							'alias' => 'Distributor',
							'type' => 'inner',
							'conditions' => array('Distributor.id = RetailersCoupon.distributor_id')	
						),
						array(
							'table' => 'coupons',
							'alias' => 'Coupon',
							'type' => 'inner',
							'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
						),
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Product.id = Coupon.product_id')	
						)
					),
				'group' => array('RetailersCoupon.distributor_id','Date(RetailersCoupon.allot_time)','Product.id'),
				'order' => array('Date(RetailersCoupon.allot_time) desc')	
				));
				$i=0;
				foreach($alloted as $allot){
					$serials = $this->RetailersCoupon->find('all',array('fields' => array('Coupon.serialNumber'), 'conditions' => array('RetailersCoupon.superdistributor_id' => $this->info['id'], 'Distributor.id' =>$allot['Distributor']['id'], 'Date(RetailersCoupon.allot_time)' =>$allot['0']['date'], 'Product.id' => $allot['Product']['id']),
					'joins' => array(
							array(
								'table' => 'distributors',
								'alias' => 'Distributor',
								'type' => 'inner',
								'conditions' => array('Distributor.id = RetailersCoupon.distributor_id')	
							),
							array(
								'table' => 'coupons',
								'alias' => 'Coupon',
								'type' => 'inner',
								'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
							),
							array(
								'table' => 'products',
								'alias' => 'Product',
								'type' => 'inner',
								'conditions' => array('Product.id = Coupon.product_id')	
							)
						)
					));
					$ser_array = array();
					foreach($serials as $serial){
						$ser_array[] = $serial['Coupon']['serialNumber'];
					}
					$alloted[$i]['0']['serials'] = implode(",",$ser_array);
					$i++;
				}
				
			}
			else {
				$alloted = array();
			}
		}
		else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			$error = true;
			$cond1 = '';
			$cond2 = '';
			if($date != null){	
				$dates = explode("-",$date);
				$date_from = $dates[0];
				$date_to = $dates[1];
				if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
					$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
					$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
					$cond1 = "Date(RetailersCoupon.allot_time) >= '$date_from' AND Date(RetailersCoupon.allot_time) <= '$date_to'";
					$error = false;
				}
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			}
			if(!$error){
				$alloted = $this->RetailersCoupon->find('all',array('fields' => array('Date(RetailersCoupon.allot_time) as date','Product.name','Product.id'), 'conditions' => array('RetailersCoupon.distributor_id' => $this->info['id'],$cond1),
				'joins' => array(
						array(
							'table' => 'coupons',
							'alias' => 'Coupon',
							'type' => 'inner',
							'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
						),
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Product.id = Coupon.product_id')	
						)
					),
				'group' => array('Date(RetailersCoupon.allot_time)','Product.id'),
				'order' => array('Date(RetailersCoupon.allot_time) desc')	
				));
				$i=0;
				foreach($alloted as $allot){
					$serials = $this->RetailersCoupon->find('all',array('fields' => array('Coupon.serialNumber'), 'conditions' => array('RetailersCoupon.distributor_id' =>$this->info['id'], 'Date(RetailersCoupon.allot_time)' =>$allot['0']['date'], 'Product.id' => $allot['Product']['id']),
					'joins' => array(
							array(
								'table' => 'coupons',
								'alias' => 'Coupon',
								'type' => 'inner',
								'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
							),
							array(
								'table' => 'products',
								'alias' => 'Product',
								'type' => 'inner',
								'conditions' => array('Product.id = Coupon.product_id')	
							)
						)
					));
					$ser_array = array();
					foreach($serials as $serial){
						$ser_array[] = $serial['Coupon']['serialNumber'];
					}
					$alloted[$i]['0']['serials'] = implode(",",$ser_array);
					$i++;
				}
			}
			else {
				$alloted = array();
			}
		}
		if($error) $this->set('empty',0);
		$this->set('alloted',$alloted);
		$this->render('cards_allotted');
	}
	
	function cardsActivated($date=null,$retailer=null){
		$this->RetailersCoupon->recursive = -1;
			
		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			$error = false;
			if($date == null)$error = true;
			$cond1 = '';
			$cond2 = '';
			if(!$error && $date != -1){	
				$dates = explode("-",$date);
				$date_from = $dates[0];
				$date_to = $dates[1];
				if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
					$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
					$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
					$cond1 = "Date(RetailersCoupon.activate_time) >= '$date_from' AND Date(RetailersCoupon.activate_time) <= '$date_to'";
				}
				else $error = true;
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			}
			if(!$error && $retailer !=null){
				$cond2 = "Retailer.id = $retailer";
				$this->set('retailer_id',$retailer);
			}
			if(!$error) {
				$activated = $this->RetailersCoupon->find('all',array('fields' => array('Retailer.shopname','Date(RetailersCoupon.activate_time) as date','Product.name','Retailer.id','Product.id'), 'conditions' => array('RetailersCoupon.distributor_id' => $this->info['id'], 'RetailersCoupon.retailer_id is not null',$cond1,$cond2),
				'joins' => array(
						array(
							'table' => 'retailers',
							'alias' => 'Retailer',
							'type' => 'inner',
							'conditions' => array('Retailer.id = RetailersCoupon.retailer_id')	
						),
						array(
							'table' => 'coupons',
							'alias' => 'Coupon',
							'type' => 'inner',
							'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
						),
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Product.id = Coupon.product_id')	
						)
					),
				'group' => array('RetailersCoupon.retailer_id','Date(RetailersCoupon.activate_time)','Product.id'),
				'order' => array('Date(RetailersCoupon.activate_time) desc')	
				));
				
				$i=0;
				foreach($activated as $active){
					$serials = $this->RetailersCoupon->find('all',array('fields' => array('Coupon.serialNumber'), 'conditions' => array('Retailer.id' =>$active['Retailer']['id'], 'Date(RetailersCoupon.activate_time)' =>$active['0']['date'], 'Product.id' => $active['Product']['id']),
					'joins' => array(
							array(
								'table' => 'retailers',
								'alias' => 'Retailer',
								'type' => 'inner',
								'conditions' => array('Retailer.id = RetailersCoupon.retailer_id')	
							),
							array(
								'table' => 'coupons',
								'alias' => 'Coupon',
								'type' => 'inner',
								'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
							),
							array(
								'table' => 'products',
								'alias' => 'Product',
								'type' => 'inner',
								'conditions' => array('Product.id = Coupon.product_id')	
							)
						)
					));
					$ser_array = array();
					foreach($serials as $serial){
						$ser_array[] = $serial['Coupon']['serialNumber'];
					}
					$activated[$i]['0']['serials'] = implode(",",$ser_array);
					$i++;
				}
			}
			else {
				$activated = array();
			}
			
		}
		else if($this->Session->read('Auth.User.group_id') == RETAILER){
			$error = true;
			$cond1 = '';
			$cond2 = '';
			if($date != null){	
				$dates = explode("-",$date);
				$date_from = $dates[0];
				$date_to = $dates[1];
				if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
					$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
					$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
					$cond1 = "Date(RetailersCoupon.activate_time) >= '$date_from' AND Date(RetailersCoupon.activate_time) <= '$date_to'";
					$error = false;
				}
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			}
			if(!$error){
				$activated = $this->RetailersCoupon->find('all',array('fields' => array('Date(RetailersCoupon.activate_time) as date','Product.name','Product.id'), 'conditions' => array('RetailersCoupon.retailer_id' => $this->info['id'], 'RetailersCoupon.distributor_id is not null',$cond1),
				'joins' => array(
						array(
							'table' => 'coupons',
							'alias' => 'Coupon',
							'type' => 'inner',
							'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
						),
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Product.id = Coupon.product_id')	
						)
					),
				'group' => array('Date(RetailersCoupon.activate_time)','Product.id'),
				'order' => array('Date(RetailersCoupon.activate_time) desc')	
				));
				
				$i=0;
				foreach($activated as $active){
					$serials = $this->RetailersCoupon->find('all',array('fields' => array('Coupon.serialNumber'), 'conditions' => array('RetailersCoupon.retailer_id' => $this->info['id'], 'Date(RetailersCoupon.activate_time)' =>$active['0']['date'], 'Product.id' => $active['Product']['id']),
					'joins' => array(
							array(
								'table' => 'coupons',
								'alias' => 'Coupon',
								'type' => 'inner',
								'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')	
							),
							array(
								'table' => 'products',
								'alias' => 'Product',
								'type' => 'inner',
								'conditions' => array('Product.id = Coupon.product_id')	
							)
						)
					));
					$ser_array = array();
					foreach($serials as $serial){
						$ser_array[] = $serial['Coupon']['serialNumber'];
					}
					$activated[$i]['0']['serials'] = implode(",",$ser_array);
					$i++;
				}
			}
			else {
				$activated = array();
			}
		}
		if($error) $this->set('empty',0);
		$this->set('activated',$activated);
		$this->render('cards_activated');
	}
	
	function invoices($date = null){
		$show = false;
		$show_invoice = false;
		$show_sales = false;
		$cond = '';
		$cond_invoice = '';
		
		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
				$show = true;
				$show_invoice = true;
				$show_sales = true;
				$cond_invoice = '(Date(Invoice.timestamp) >= "'.$date_from.'" AND Date(Invoice.timestamp) <= "' .$date_to . '")';
				$cond = '(Date(ShopTransaction.timestamp) >= "'.$date_from.'" AND Date(ShopTransaction.timestamp) <= "' .$date_to . '")';
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			}
		}
		else {
			$show = true;
			$this->set('start',0);
			if($this->Session->read('Auth.User.group_id') == RETAILER){
				$show_invoice = true;
				$this->set('start_inv',0);
			}
		}
		
		if($show_invoice){
			$invoices = $this->Invoice->find('all',array('fields' => array('Invoice.id','Invoice.group_id','Invoice.invoice_type','Invoice.amount','Invoice.invoice_number','Date(Invoice.timestamp) as date','GROUP_CONCAT(Receipt.receipt_number) as numbers','GROUP_CONCAT(Receipt.id) as recids','min(Receipt.os_amount) as os_amount'), 'conditions' => array('Invoice.ref_id' => $this->info['id'], 'Invoice.group_id' => $this->Session->read('Auth.User.group_id'), $cond_invoice), 
								'joins' => array(
									array(
										'table' => 'receipts',
										'alias' => 'Receipt',
										'type' => 'left',
										'conditions' => array('Receipt.receipt_type = ' . RECEIPT_INVOICE. ' AND Receipt.receipt_ref_id = Invoice.id AND Receipt.shop_to_id = ' . $this->info['id'] . ' AND Receipt.group_id = ' . $this->Session->read('Auth.User.group_id'))
									)
								),			
								'order' => 'Invoice.timestamp desc',
								'group' => array('Invoice.id')
						));
		}
		else {
			$invoices = array();
		}
		
		if($show){
			if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
				$data_parent = $this->ShopTransaction->find('all',array('fields' => array('ShopTransaction.id','ShopTransaction.amount','ShopTransaction.timestamp','GROUP_CONCAT(Receipt.receipt_number) as numbers','GROUP_CONCAT(Receipt.id) as recids','min(Receipt.os_amount) as os_amount'), 'conditions' => array('ShopTransaction.ref2_id' => $this->info['id'], 'ShopTransaction.type' => ADMIN_TRANSFER, $cond),
						'joins' => array(
									array(
										'table' => 'receipts',
										'alias' => 'Receipt',
										'type' => 'left',
										'conditions' => array('Receipt.receipt_type = ' . RECEIPT_TOPUP. ' AND Receipt.receipt_ref_id = ShopTransaction.id AND Receipt.shop_to_id = ' . $this->info['id'] . ' AND Receipt.group_id = ' . $this->Session->read('Auth.User.group_id'))
									)
								),
						'order' => 'ShopTransaction.id desc',
						'group' => array('ShopTransaction.id')
						));	
			}
			else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
				$data_parent = $this->ShopTransaction->find('all',array('fields' => array('ShopTransaction.id','ShopTransaction.amount','ShopTransaction.timestamp','GROUP_CONCAT(Receipt.receipt_number) as numbers','GROUP_CONCAT(Receipt.id) as recids','min(Receipt.os_amount) as os_amount'), 'conditions' => array('ShopTransaction.ref2_id' => $this->info['id'], 'ShopTransaction.type' => SDIST_DIST_BALANCE_TRANSFER, $cond),
					'joins' => array(
									array(
										'table' => 'receipts',
										'alias' => 'Receipt',
										'type' => 'left',
										'conditions' => array('Receipt.receipt_type = ' . RECEIPT_TOPUP. ' AND Receipt.receipt_ref_id = ShopTransaction.id AND Receipt.shop_to_id = ' . $this->info['id'] . ' AND Receipt.group_id = ' . $this->Session->read('Auth.User.group_id'))
									)
								),
					'order' => 'ShopTransaction.id desc',
					'group' => array('ShopTransaction.id')
					));	
			}
			else if($this->Session->read('Auth.User.group_id') == RETAILER){
				$data_parent = $this->ShopTransaction->find('all',array('fields' => array('ShopTransaction.id','ShopTransaction.amount','ShopTransaction.timestamp','GROUP_CONCAT(Receipt.receipt_number) as numbers','GROUP_CONCAT(Receipt.id) as recids','min(Receipt.os_amount) as os_amount'), 'conditions' => array('ShopTransaction.ref2_id' => $this->info['id'], 'ShopTransaction.type' => DIST_RETL_BALANCE_TRANSFER, $cond),
					'joins' => array(
									array(
										'table' => 'receipts',
										'alias' => 'Receipt',
										'type' => 'left',
										'conditions' => array('Receipt.receipt_type = ' . RECEIPT_TOPUP. ' AND Receipt.receipt_ref_id = ShopTransaction.id AND Receipt.shop_to_id = ' . $this->info['id'] . ' AND Receipt.group_id = ' . $this->Session->read('Auth.User.group_id'))
									)
								),
					'order' => 'ShopTransaction.id desc',
					'group' => array('ShopTransaction.id')
					));
			}
		}
		else {
			$data_parent = array();
		}
		
		if($show_sales){
			$cond = 'AND Date(st1.timestamp) >= "'.$date_from.'" AND Date(st1.timestamp) <= "' .$date_to . '"';
			$cond_ret = 'AND Date(retailers_coupons.activate_time) >= "'.$date_from.'" AND Date(retailers_coupons.activate_time) <= "' .$date_to . '"';
				
			if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
				$query = "SELECT products.name,products.id,products.price,count(transactions.id) as counts FROM (
						(SELECT st1.ref2_id as product_id, st1.id as id FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_SUPERDISTRIBUTOR." AND st2.ref1_id = ".$this->info['id'].") WHERE st1.type = " .RETAILER_ACTIVATION. " $cond )
						UNION
						(SELECT coupons.product_id as product_id,coupons.id as id  FROM retailers_coupons INNER JOIN coupons ON (coupons.id = retailers_coupons.coupon_id) WHERE retailers_coupons.superdistributor_id = " .$this->info['id']. " AND retailers_coupons.distributor_id IS NOT NULL AND retailers_coupons.retailer_id IS NOT NULL $cond_ret )
					) as transactions INNER JOIN products ON (products.id = transactions.product_id) group by transactions.product_id";
			}
			else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
				$query = "SELECT products.name,products.id,products.price,count(transactions.id) as counts FROM (
						(SELECT st1.ref2_id as product_id, st1.id as id FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_DISTRIBUTOR." AND st2.ref1_id = ".$this->info['id'].") WHERE st1.type = " .RETAILER_ACTIVATION. " $cond )
						UNION
						(SELECT coupons.product_id as product_id,coupons.id as id  FROM retailers_coupons INNER JOIN coupons ON (coupons.id = retailers_coupons.coupon_id) WHERE retailers_coupons.distributor_id = " .$this->info['id']. " AND retailers_coupons.superdistributor_id IS NOT NULL AND retailers_coupons.retailer_id IS NOT NULL $cond_ret )
					) as transactions INNER JOIN products ON (products.id = transactions.product_id) group by transactions.product_id";
			}
			else if($this->Session->read('Auth.User.group_id') == RETAILER){
				$query = "SELECT products.name,products.id,products.price,count(transactions.id) as counts FROM (
						(SELECT st1.ref2_id as product_id, st1.id as id FROM shop_transactions as st1 WHERE st1.ref1_id = ".$this->info['id']." AND st1.type = " .RETAILER_ACTIVATION. " $cond )
						UNION
						(SELECT coupons.product_id as product_id,coupons.id as id  FROM retailers_coupons INNER JOIN coupons ON (coupons.id = retailers_coupons.coupon_id) WHERE retailers_coupons.retailer_id = " .$this->info['id']. " AND retailers_coupons.superdistributor_id IS NOT NULL AND retailers_coupons.distributor_id IS NOT NULL $cond_ret )
					) as transactions INNER JOIN products ON (products.id = transactions.product_id) group by transactions.product_id";
			}
			$products = $this->Retailer->query($query);
			$i = 0;
			foreach($products as $product){
				$prod = array();
				$prod[$product['products']['id']] = 1;
				$commission = $this->Shop->calculateCommission($this->info['id'],$this->Session->read('Auth.User.group_id'),$prod,date('Y-m-d'));
				$commission = $commission*$product['0']['counts'];
				if(isset($this->info['parent_id'])) $parent = $this->info['parent_id'];
				if(empty($parent)) $tds_flag = 1;
				else {
					$parent_shop = $this->Shop->getShopDataById($parent,$this->Session->read('Auth.User.group_id') - 1);
					$tds_flag = $parent_shop['tds_flag'];
				}
				if($tds_flag == 1)$tds = $this->Shop->calculateTDS($commission);
				else $tds = 0;
				$products[$i]['0']['commission'] = $commission;
				$products[$i]['0']['tds'] = $tds;
				$i++;
			}
		}
		else {
			$products = array();
		}
		$this->set('products',$products);
		
		if($date == null) $this->set('empty',0);
		$this->set('invoices',$invoices);
		$this->set('data_parent',$data_parent);
		$this->render('invoices');
	}
	
	function topupReceipts($date=null,$id=null){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		if($this->Session->read('Auth.User.group_id') != RETAILER){
			$error = false;
			$show_sales = false;
			if($date == null)$error = true;
			$cond0 = '';
			$cond1 = '';
			$cond2 = '';
			if(!$error && $date != -1){	
				$dates = explode("-",$date);
				$date_from = $dates[0];
				$date_to = $dates[1];
				if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
					$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
					$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
					$cond1 = "Date(Invoice.timestamp) >= '$date_from' AND Date(Invoice.timestamp) <= '$date_to'";
					$cond0 = "Date(ShopTransaction.timestamp) >= '$date_from' AND Date(ShopTransaction.timestamp) <= '$date_to'";
				}
				else $error = true;
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			}
			if(!$error && $id !=null){
				$cond2 = "Child.id = $id";
				$show_sales = true;
				$this->set('id',$id);
			}
			$show = false;
			$show_invoice = false;
			
			if($date == null){
				$show = true;
				$this->set('start',0);
				if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
					$show_invoice = true;
					$this->set('start_inv',0);
				}
			}
			
			$child = $this->Session->read('Auth.User.group_id') + 1;
			if($child == DISTRIBUTOR){
				$table = "distributors";
				$company = "company";
			}
			else {
				$table = "retailers";
				$company = "shopname";
			}
			
			if(!$error || ($date == null && $show_invoice)){
				$invoices_child = $this->Invoice->find('all',array('fields' => array('Invoice.id','Invoice.group_id','Invoice.invoice_type','Invoice.amount','Invoice.invoice_number','Date(Invoice.timestamp) as date',"Child.id","Child.$company as company",'GROUP_CONCAT(Receipt.receipt_number) as numbers','GROUP_CONCAT(Receipt.id) as recids','min(Receipt.os_amount) as os_amount'), 'conditions' => array('Invoice.group_id' => $child,$cond1,$cond2), 
					'joins' => array(
						array(
							'table' => $table,
							'alias' => 'Child',
							'type' => 'inner',
							'conditions' => array('Invoice.ref_id = Child.id AND Child.parent_id = ' . $this->info['id'])
						),
						array(
							'table' => 'receipts',
							'alias' => 'Receipt',
							'type' => 'left',
							'conditions' => array('Receipt.receipt_type = ' . RECEIPT_INVOICE. ' AND Receipt.receipt_ref_id = Invoice.id AND Receipt.shop_from_id = ' . $this->info['id'] . ' AND Receipt.group_id = ' . ($this->Session->read('Auth.User.group_id') + 1))
						)
					),
				
					'order' => 'Invoice.timestamp desc',
					'group' => array('Invoice.id')
					));
			}
			else {
				$invoices_child = array();
			}
			$this->ShopTransaction->recursive = -1;
			
			if(!$error || ($date == null && $show)){
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
					if($id !=null)$cond2 = "Distributor.id = $id";
					$data = $this->ShopTransaction->find('all',array('fields' => array('ShopTransaction.id','ShopTransaction.amount','ShopTransaction.timestamp','Distributor.id','Distributor.company','GROUP_CONCAT(Receipt.receipt_number) as numbers','GROUP_CONCAT(Receipt.id) as recids','min(Receipt.os_amount) as os_amount'), 'conditions' => array('ShopTransaction.ref1_id' => $this->info['id'], 'ShopTransaction.type' => SDIST_DIST_BALANCE_TRANSFER, $cond0,$cond2),
						'joins' => array(
							array(
								'table' => 'distributors',
								'type' => 'inner',
								'alias' => 'Distributor',
								'conditions' => array('Distributor.id = ShopTransaction.ref2_id')
							),
							array(
								'table' => 'receipts',
								'alias' => 'Receipt',
								'type' => 'left',
								'conditions' => array('Receipt.receipt_type = ' . RECEIPT_TOPUP. ' AND Receipt.receipt_ref_id = ShopTransaction.id AND Receipt.shop_from_id = ' . $this->info['id'] . ' AND Receipt.group_id = ' . ($this->Session->read('Auth.User.group_id') + 1))
							)
						),
					'order' => 'ShopTransaction.id desc',
					'group' => array('ShopTransaction.id')	
					));
				}
				else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
					if($id !=null)$cond2 = "Retailer.id = $id";
					$data = $this->ShopTransaction->find('all',array('fields' => array('ShopTransaction.id','ShopTransaction.amount','ShopTransaction.timestamp','Retailer.id','Retailer.shopname','GROUP_CONCAT(Receipt.receipt_number) as numbers','GROUP_CONCAT(Receipt.id) as recids','min(Receipt.os_amount) as os_amount'), 'conditions' => array('ShopTransaction.ref1_id' => $this->info['id'], 'ShopTransaction.type' => DIST_RETL_BALANCE_TRANSFER, $cond0,$cond2),
						'joins' => array(
							array(
								'table' => 'retailers',
								'type' => 'inner',
								'alias' => 'Retailer',
								'conditions' => array('Retailer.id = ShopTransaction.ref2_id')
							),
							array(
								'table' => 'receipts',
								'alias' => 'Receipt',
								'type' => 'left',
								'conditions' => array('Receipt.receipt_type = ' . RECEIPT_TOPUP. ' AND Receipt.receipt_ref_id = ShopTransaction.id AND Receipt.shop_from_id = ' . $this->info['id'] . ' AND Receipt.group_id = ' . ($this->Session->read('Auth.User.group_id') + 1))
							)
						),
					'order' => 'ShopTransaction.id desc',
					'group' => array('ShopTransaction.id')	
					));
				}
			}
			else {
				$data = array();
			}
			if($show_sales){
				if($date != -1){
					$cond = 'AND Date(st1.timestamp) >= "'.$date_from.'" AND Date(st1.timestamp) <= "' .$date_to . '"';
					$cond_ret = 'AND Date(retailers_coupons.activate_time) >= "'.$date_from.'" AND Date(retailers_coupons.activate_time) <= "' .$date_to . '"';
				}
				else {
					$cond = '';
					$cond_ret = '';
				}
				$shop = $this->Shop->getShopDataById($id,$this->Session->read('Auth.User.group_id')+1);
					
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
					$query = "SELECT products.name,products.id,products.price,count(transactions.id) as counts FROM (
							(SELECT st1.ref2_id as product_id, st1.id as id FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_DISTRIBUTOR." AND st2.ref1_id = ".$id.") WHERE st1.type = " .RETAILER_ACTIVATION. " $cond )
							UNION
							(SELECT coupons.product_id as product_id,coupons.id as id  FROM retailers_coupons INNER JOIN coupons ON (coupons.id = retailers_coupons.coupon_id) WHERE retailers_coupons.superdistributor_id = " .$this->info['id']. " AND retailers_coupons.distributor_id = $id AND retailers_coupons.retailer_id IS NOT NULL $cond_ret )
						) as transactions INNER JOIN products ON (products.id = transactions.product_id) group by transactions.product_id";
					$company = $shop['company'];
				}
				else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
					$query = "SELECT products.name,products.id,products.price,count(transactions.id) as counts FROM (
							(SELECT st1.ref2_id as product_id, st1.id as id FROM shop_transactions as st1 WHERE st1.ref1_id= $id AND st1.type = " .RETAILER_ACTIVATION. " $cond )
							UNION
							(SELECT coupons.product_id as product_id,coupons.id as id  FROM retailers_coupons INNER JOIN coupons ON (coupons.id = retailers_coupons.coupon_id) WHERE retailers_coupons.distributor_id = " .$this->info['id']. " AND retailers_coupons.superdistributor_id IS NOT NULL AND retailers_coupons.retailer_id =$id $cond_ret )
						) as transactions INNER JOIN products ON (products.id = transactions.product_id) group by transactions.product_id";
					$company = $shop['shopname'];
				}
				
				$products = $this->Retailer->query($query);
				$i = 0;
				foreach($products as $product){
					$prod = array();
					$prod[$product['products']['id']] = 1;
					$commission = $this->Shop->calculateCommission($id,$this->Session->read('Auth.User.group_id')+1,$prod,date('Y-m-d'));
					$commission = $commission*$product['0']['counts'];
					
					if($this->info['tds_flag'] == 1)$tds = $this->Shop->calculateTDS($commission);
					else $tds = 0;
					$products[$i]['0']['commission'] = $commission;
					$products[$i]['0']['tds'] = $tds;
					$i++;
				}
				$this->set('company',$company);
			}
			else {
				$products = array();
			}
			$this->set('products',$products);
		
			if($error) $this->set('empty',0);
			$this->set('data',$data);
			$this->set('childs',$invoices_child);
			$this->render('topup_receipts');
		}
		
	}
	
	function transfer(){
		$this->render('transfer');
	}
	
	function amountTransfer(){
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
			$confirm = $this->data['confirm'];
		preg_match('/^[0-9]/',$this->data['amount'],$matches,0);
		if($this->data['shop'] == 0){
			if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR)
			$msg = "Please select distributor";
			else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR)
			$msg = "Please select retailer";
			$to_save = false;
		}
		else if(empty($this->data['amount'])){
			$msg = "Please enter some amount";
			$to_save = false;
		}
		else if($this->data['amount'] <= 0 || empty($matches)){
			$msg = "Amount entered is not valid";
			$to_save = false;
		}
		else {
			$shop = $this->Shop->getShopData($this->Session->read('Auth.User.id'),$this->Session->read('Auth.User.group_id'));
			$bal = $shop['balance'];
			if($this->data['amount'] > $bal){
				$msg = "Your amount cannot be greater than your account balance";
				$to_save = false;
			}
			else {
				$shop = $this->Shop->getShopDataById($this->data['shop'],$this->Session->read('Auth.User.group_id') + 1);
				$this->data['shopData'] = $shop;
				if($confirm == 0){
					$this->set('balance',$bal - $this->data['amount']);
					$this->set('data',$this->data);
					$this->render('confirm_transfer','ajax');
				}
				else {
					$mail_subject = "Retail Panel: Amount Transferred";
					
					if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
						$this->Shop->shopTransactionUpdate(SDIST_DIST_BALANCE_TRANSFER,$this->data['amount'],$this->info['id'],$shop['id']);
						$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$this->info['id'],SUPER_DISTRIBUTOR);
						$this->Shop->shopBalanceUpdate($this->data['amount'],'add',$shop['id'],DISTRIBUTOR);
						$mail_body = "SuperDistributor: " . $this->info['company'] . " transferred Rs. " . $this->data['amount'] . " to Distributor: " . $shop['company'];
					}
					else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
						$this->Shop->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$this->data['amount'],$this->info['id'],$shop['id']);
						$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$this->info['id'],DISTRIBUTOR);
						$this->Shop->shopBalanceUpdate($this->data['amount'],'add',$shop['id'],RETAILER);
						$mail_body = "Distributor: " . $this->info['company'] . " transferred Rs. " . $this->data['amount'] . " to Retailer: " . $shop['shopname'];
					}
					//$this->General->mailToAdmins($mail_subject, $mail_body);
					echo "<script> reloadShopBalance(".$bal.");  </script>";
					$this->render('/elements/shop_transfer','ajax');
				}
			}
		}
		
		if(!$to_save){
			$this->set('data',$this->data);
			$msg = '<div class="error_class">'.$msg.'</div>';
			$this->Session->setFlash(__($msg, true));
			$this->render('/elements/shop_transfer','ajax');
		}
	}
	
	
	function backTransfer(){
		$this->set('data',$this->data);
		$this->render('/elements/shop_transfer');
	}
	
	function printRequest($request_id){
		if(!$this->Session->check('Auth.User')) $this->logout();
		$request_id = $this->objMd5->Decrypt($request_id,encKey);
		$this->ShopTransaction->recursive = -1;
		$receipt = $this->ShopTransaction->findById($request_id);
		if($receipt['ShopTransaction']['type'] == ADMIN_TRANSFER){
			$to = $this->Shop->getShopDataById($receipt['ShopTransaction']['ref2_id'],SUPER_DISTRIBUTOR);
			$company = $to['company'];
			$receipt['from_addr'] = SMSTADKA_ADDRESS;
			$receipt['from_comp'] = SMSTADKA_COMPANY;
			$receipt['mobile'] = SMSTADKA_CONTACT;
		}
		else if($receipt['ShopTransaction']['type'] == SDIST_DIST_BALANCE_TRANSFER){
			$from = $this->Shop->getShopDataById($receipt['ShopTransaction']['ref1_id'],SUPER_DISTRIBUTOR);
			$to = $this->Shop->getShopDataById($receipt['ShopTransaction']['ref2_id'],DISTRIBUTOR);
			$company = $to['company'];
			$receipt['from_addr'] = $from['address'];
			$receipt['from_comp'] = $from['company'];
			$parentData = $this->General->getUserDataFromId($from['user_id']);
			$receipt['mobile'] = "+91" . $parentData['mobile'];
		}
		else if($receipt['ShopTransaction']['type'] == DIST_RETL_BALANCE_TRANSFER){
			$from = $this->Shop->getShopDataById($receipt['ShopTransaction']['ref1_id'],DISTRIBUTOR);
			$to = $this->Shop->getShopDataById($receipt['ShopTransaction']['ref2_id'],RETAILER);
			$company = $to['shopname'];
			$receipt['from_addr'] = $from['address'];
			$receipt['from_comp'] = $from['company'];
			$parentData = $this->General->getUserDataFromId($from['user_id']);
			$receipt['mobile'] = "+91" . $parentData['mobile'];
		}
		$receipt['to_addr'] = $to['address'];
		$receipt['to_comp'] = $company;
		$this->set('data',$receipt);
		$this->render('/elements/request','print');
	}
	
	function printInvoice($invoice_id=null){
		if($invoice_id ==null){
			$invoice_id = $_REQUEST['invoice_id'];
		}
		$invoice_id = $this->objMd5->Decrypt($invoice_id,encKey);
		$this->Invoice->recursive = -1;
		$invoice = $this->Invoice->findById($invoice_id);
		$id = $invoice['Invoice']['ref_id'];
		$group_id = $invoice['Invoice']['group_id'];
		$invoice_type = $invoice['Invoice']['invoice_type'];
		$data['date'] = date('Y-m-d', strtotime($invoice['Invoice']['timestamp']));
		
		$data['invoice_id'] = $invoice_id;
		$data['invoice_number'] = $invoice['Invoice']['invoice_number'];
		$data['tds'] = 1;
		$shop = $this->Shop->getShopDataById($id,$group_id);
		if($group_id != SUPER_DISTRIBUTOR){
			$parent = $shop['parent_id'];
			if($group_id == DISTRIBUTOR){
				$parentShop = $this->Shop->getShopDataById($parent,SUPER_DISTRIBUTOR);
			}
			else if($group_id == RETAILER){
				$parentShop = $this->Shop->getShopDataById($parent,DISTRIBUTOR);
			}
			$data['tds'] = $parentShop['tds_flag'];
			$data['company'] = $parentShop['company'];
			$data['address'] = $parentShop['address'];
			$parentData = $this->General->getUserDataFromId($parentShop['user_id']);
			$data['mobile'] = "+91" . $parentData['mobile'];
		}
		else {
			$data['company'] = SMSTADKA_COMPANY;
			$data['address'] = SMSTADKA_ADDRESS;
			$data['mobile'] = SMSTADKA_CONTACT;
		}
		
		$data['name'] = $shop['company'];
		$data['addr'] = $shop['address'];
		if($group_id == RETAILER)
			$data['name'] = $shop['shopname'];
			
		$transactions = $this->Invoice->query("SELECT ref2_id,shop_transactions.id FROM invoices_transactions,shop_transactions WHERE invoice_id = $invoice_id AND shop_transactions.id = invoices_transactions.shoptransaction_id");	
		if($invoice_type == DISTRIBUTOR_ACTIVATION){
			$coupons = array();
			foreach($transactions as $transaction){
				$coupons = array_merge($coupons,explode(",",$transaction['shop_transactions']['ref2_id']));
			}
			$this->Coupon->recursive = -1;
			$prodData = $this->Coupon->find('all',array('fields' => array('GROUP_CONCAT(Coupon.serialNumber) as serials','COUNT(Coupon.id) as quantity','Product.id','Product.name','Product.price'), 'conditions' => array('Coupon.id in (' .implode(",",$coupons). ')' ),
					'joins' => array(
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Coupon.product_id = Product.id')
						)
					),
					'group' => array('Coupon.product_id')
			));
			foreach($prodData as $prod){
				$data['Product'][] = $prod['Product']['name'];
				$data['quantity'][] = $prod['0']['quantity'];
				$data['rate'][] = $prod['Product']['price'];
				$data['serials'][] = $prod['0']['serials'];
				$product = array();
				$product[$prod['Product']['id']] = $prod['0']['quantity'];
				$data['percent'][] = $this->Shop->getCommissionPercent($id,$group_id,$prod['Product']['id'],$data['date']);
				$data['commission'][] = $this->Shop->calculateCommission($id,$group_id,$product,$data['date']);
			}
		}
		else {
			$products = array();
			$prodids = array();
			foreach($transactions as $transaction){
				$prod_id = $transaction['shop_transactions']['ref2_id'];
				$this->ShopTransaction->recursive = -1;
				
				if($group_id == RETAILER){
					$commission = $this->ShopTransaction->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_RETAILER)));
				}
				else if($group_id == DISTRIBUTOR){
					$commission = $this->ShopTransaction->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_DISTRIBUTOR)));
				}
				else if($group_id == SUPER_DISTRIBUTOR){
					$commission = $this->ShopTransaction->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_SUPERDISTRIBUTOR)));
				} 
				$commission = $commission['ShopTransaction']['amount'];
				
				if(!isset($products[$prod_id])){
					$products[$prod_id]['quantity'] = 1;
					$products[$prod_id]['commission'] = $commission;
					$prodids[] = $prod_id;
				}
				else {
					$products[$prod_id]['quantity'] = $products[$prod_id]['quantity'] + 1;
					$products[$prod_id]['commission'] = $products[$prod_id]['commission'] + $commission;
				}
			}
			$this->Product->recursive = -1;
			$prodData = $this->Product->find('all',array('fields' => array('Product.id','Product.name','Product.price'), 'conditions' => array('Product.id in (' .implode(",",$prodids). ')' )));	
			foreach($prodData as $prod){
				$data['Product'][] = $prod['Product']['name'];
				$data['quantity'][] = $products[$prod['Product']['id']]['quantity'];
				$data['rate'][] = $prod['Product']['price'];
				$data['commission'][] = $products[$prod['Product']['id']]['commission'];
				$data['percent'][] = $this->Shop->getCommissionPercent($id,$group_id,$prod['Product']['id'],$data['date']);
			}
		}
		$this->set('group_id',$group_id);
		$this->set('invoice_type',$invoice_type);
		$this->set('data',$data);
		$this->render('/elements/invoice','print');
	}
		
	function logout(){
		$this->Auth->logout();
		$this->redirect('/shops');
	}
	
	function retailerProdActivation(){
		$go = true;
		if(isset($_REQUEST['mobNo'])){
			if(!$this->Session->check('Auth.User'))$this->logout();
			$mobile_number = $_REQUEST['mobNo'];
			$product_id = $_REQUEST['prodId'];
			$pnrNo = $_REQUEST['pnrNo'];
			$param = $pnrNo;
			$vendor = false;
		}
		else {
			$product_id = $this->objMd5->Decrypt(trim(urldecode($_REQUEST['product_id'])),encKey);
			$param = trim(urldecode($_REQUEST['param']));
			$mobile_number = trim(urldecode($_REQUEST['mobile_number']));
			$trans_id = trim(urldecode($_REQUEST['trans_id']));
			$vendor_id = trim(urldecode($_REQUEST['vendor_id']));
			$retailer_code = trim(urldecode($_REQUEST['retailer_code']));
			$vendor = true;
			$checkProd = $this->Product->query("SELECT * FROM vendors_commissions WHERE vendor_id = $vendor_id AND product_id = $product_id");
			if(empty($checkProd))$go = false;
		}
		$success = 1;
		$prodInfo = $this->General->getProductInfo($product_id);
		if(!empty($prodInfo) && $go){	
			if($vendor || ($this->info['balance'] >= $prodInfo['Product']['price'] && !$vendor)){
				
				$exists = $this->General->checkIfUserExists($mobile_number);
				if(!$exists){
					$new_user = 1;
					$user = $this->General->registerUser($mobile_number,ONLINE_RETAILER_REG);
					$user = $user['User'];
				}
				else {
					$new_user = 0;
					$user = $this->General->getUserDataFromMobile($mobile_number);
				}
				
				$user_id = $user['id'];			
				$to_send = true;
										
				
				$true_flag = true;
				if($prodInfo['Product']['outside_flag'] == 1){
					$true_flag = false;
					$to_send = false;
					if($product_id == PNR_PRODUCT || $product_id == PNR_HAPPY_JOURNEY || $product_id == PNR_SIGNATURE){					
						$random['random'] = mt_rand();
						$params['coupon_id'] = -1;
						$params['user_id'] = $user_id;
						$params['product_id'] = $product_id;
						$params['serial_number'] = -1;
						if(!$vendor){// REtailers via panel
							$params['retailer_id'] = $this->info['id'];
							$params['mobile'] = $this->info['mobile'];
						}
						else { // vendors
							$params['vendor_id'] = $vendor_id;
						}
						if($product_id == PNR_HAPPY_JOURNEY){
							$sign = $this->General->getRetailerSignature($params['retailer_id']);
							if(empty($sign) || $sign['Retailer']['signature_flag'] == 0){
								$return['success'] = 0;
								$return['msg'] = "Please create your signature first to use this. Go to my profile section to create your signature";			
								$return['prod_userid'] = -1;
							}
							else {
								$this->General->addAsynchronousCall($random['random'],'pnr','retailTrainJourney',$params);
								$return = $this->General->retailTrainJourney($param,$mobile_number,$random['random']);
							}
						}
						else {
							$this->General->addAsynchronousCall($random['random'],'pnr','retailPNR',$params);
							$return = $this->General->retailPNR($param,$mobile_number,$random['random']);
						}
						
						$success = $return['success'];
						$ret = $return['msg'];
						$prod_userid = $return['prod_userid'];
					}
					else if($product_id == RECHARGE_CARD_50){
						$true_flag = true;
						$balance = $this->General->balanceUpdate(50,'add',$user_id);
						$this->General->transactionUpdate(TRANS_RETAIL,50,null,null,$user_id);
						if($new_user == 1 || ($new_user == 0 && $user['login_count'] == 0)){
							$sms = $this->General->createMessage("SMSTADKA_RECHARGE",array($mobile_number,$user['syspass'],50,$balance));
						}
						else {
							$sms = $this->General->createMessage("SMSTADKA_RECHARGE_ONLINE",array(50,$balance));
						}
						$this->General->sendMessage(SMS_SENDER,$mobile_number,$sms,'template');
					}
				}
				if($true_flag){
					$prod_userid = $this->General->executeCouponEnteries('-1',$user_id,$mobile_number,$product_id,'-1',$prodInfo['Product']['outside_flag']);
					
					$data = $this->Product->query("SELECT id,end,trial,count FROM products_users WHERE product_id = $product_id and user_id = $user_id and active = 1");
					if(empty($data)){					
						$end = date('Y-m-d H:i:s', strtotime( '+ '.$prodInfo['Product']['validity'].' days'));
						if($prodInfo['Product']['outside_flag'] == 1) $end = null;					
					}
					else {
						if($data['0']['products_users']['trial'] == 1 && $data['0']['products_users']['count'] == 1){
							$end = date('Y-m-d H:i:s', strtotime( '+ '.$prodInfo['Product']['validity'].' days'));
						}
						else {
							$end = date('Y-m-d H:i:s', strtotime( $data['0']['products_users']['end'] . '+ '.$prodInfo['Product']['validity'].' days'));
						}
						if($prodInfo['Product']['outside_flag'] == 1) $end = null;					
					}
					
					$displayMsg = "Product <b>".$prodInfo['Product']['name']."</b> has been activated successfully on <b>".$mobile_number."</b>. <br>";
					
					if($prodInfo['Product']['outside_flag'] != 1)
					$displayMsg .= "The product will expire on " . date('d-M-Y',strtotime($end)).".";
					
					$mail['subject'] = "Retail Product - via panel " . $prodInfo['Product']['name'] . " subscribed";				
					if(!$vendor){	//via hiren panel
						$mail['body'] = "User ".$mobile_number." subscribed it. Retailer: " . $this->info['name'] . " <br>";
						$mail['body'] .= $this->info['shopname'] . "<br>";
						$mail['body'] .= $this->info['address'];
						$this->Shop->transactionsOnRetailerActivation($this->info['id'],$product_id,$prod_userid);
					}
					else { //via vendor panel
						$vendor = $this->Shop->getVendor($vendor_id);
						$mail['body'] = "Retailer ID: " . $retailer_code."<br/>";
						$mail['body'] .= "User ".$mobile_number." subscribed it <br/>";
						$mail['body'] .= "Vendor: " . $vendor;
					}
					$ret = $displayMsg;
				}
				
				if($to_send){
					$params = array();
					$params['receiver'] = $mobile_number;
					$params['product'] = $product_id;
					$this->General->addAsynchronousCall('-1','retailer','msgAfterSubscription',$params);
				}
				
			}else{// no sufficient balance
				$ret = "1";
				$success = 0;
			}
		}
		else {
			$ret = "-1";
			$success = 0;
		}
		
		/*if(DND_FLAG && !TRANS_FLAG && $success == 1 && $user['dnd_flag'] == 1){
			$this->General->tagUser('DNDUSER',$user['id']);
			$this->General->mailToAdmins("User Registered in DND, NCPR: " . $user['ncpr_pref'], "NCPR Preference: ".$user['ncpr_pref'].". Call, Mobile Number: ".$user['mobile']);
			$message = $user['mobile'] . " 1";
			$this->General->sendMessage(SMS_SENDER,array('9833032643'),$message,'sms');
		}*/
		
		$url = SERVER_BACKUP . 'users/sendMsgMails';
		$data = array();
		if(isset($mail['subject'])){
			//$data['mail_subject'] = $mail['subject'];
			$data['mail_body'] = $mail['body'];
		}
		if(!$vendor){
			$data['retailer_id'] = $this->info['id'];
		}
		else {
			$data['vendor_id'] = $vendor_id;
		}
		if(!empty($data))
		$this->General->curl_post_async($url,$data);
		//echo $products['0']['Product']['price'].$product_id; echo $mobile_number;
		if(!$vendor){
			echo $ret;
		}
		else {
			if($success == 1) $result = 'SUCCESS';
			else $result = 'FAILURE';
			
			$msg = "$trans_id#*#$result#*#".strip_tags($ret);
			if($success == 1){
				//entry in vendor_activations
				$ref_id = $this->Shop->updateVendorActivation($vendor_id,$prod_userid,$trans_id,$retailer_code);
				$msg .= "#*#$ref_id";
			}
			else {
				$msg .= "#*#0";
			}
			if(in_array($vendor_id,explode(',',SCHEME_VENDORS)) && SCHEME_FLAG == 1){
				if($success == 1){
					$return = $this->General->checkRetailerPrizeScheme($vendor_id,$retailer_code,$prodInfo['Product']['price']);
				}
				else {
					$return = "Do Transactions of SMSTadka & Keep Winning !!";
				}
				$msg .= "#*#$return";
			}
			echo $msg;
		}
		$this->autoRender = false;
	}
	
	function initializeOpeningBalance(){//initialize balance once everyday at 11:30PM
		$this->lock();
		$this->Distributor->updateAll(array('Distributor.opening_balance' => 'Distributor.balance'));
		$this->Retailer->updateAll(array('Retailer.opening_balance' => 'Retailer.balance'));
		$this->releaseLock();
		$this->autoRender = false;
	}
	
	function generateInvoice(){ //generate all invoices at day end
		$this->lock();
		$this->SuperDistributor->recursive = -1;
		$super_distributors = $this->SuperDistributor->find('all');
		foreach($super_distributors as $super){
			$this->Shop->generateInvoice($super['SuperDistributor']['id'],SUPER_DISTRIBUTOR,DISTRIBUTOR_ACTIVATION);
			$this->Shop->generateInvoice($super['SuperDistributor']['id'],SUPER_DISTRIBUTOR,RETAILER_ACTIVATION);
		}
		$this->Distributor->recursive = -1;
		$distributors = $this->Distributor->find('all');
		foreach($distributors as $dist){
			$this->Shop->generateInvoice($dist['Distributor']['id'],DISTRIBUTOR,DISTRIBUTOR_ACTIVATION);
			$this->Shop->generateInvoice($dist['Distributor']['id'],DISTRIBUTOR,RETAILER_ACTIVATION);
		}
		$this->Retailer->recursive = -1;
		$retailers = $this->Retailer->find('all',array('conditions' => array('Retailer.toshow' => 1,'Retailer.parent_id is not null')));
		foreach($retailers as $ret){
			$this->Shop->generateInvoice($ret['Retailer']['id'],RETAILER,RETAILER_ACTIVATION);
		}
		$this->releaseLock();
		$this->autoRender = false;
	}
	
	function products(){
		$this->render('products','xml/default');
	}
	
	function test($var=null){
		//$this->General->sendMessage("",'9819032643,1212121212','hello','payone');
		exit;
		$msg = "Amount Rs 2000 transferred to retailer srikanth-warang.. successfully.
Your balance now: 2500 (2000)
Your today's topups: 2000.000";
		$this->General->sendMessageViaRouteSMS('9819032643',$msg,'payone');exit;
		$this->General->sendMessageViaInfobip('',array('9819032643','9833032643'),'hello');exit;
		//$this->General->mailToUsers('test','test',array('ashish@mindsarray.com'));
		exit;
		$this->General->sendMessage('','9892609560,9819852204,9004387418','hello');
		exit;
		
		$query1 = "SELECT mobile FROM `products_users` inner join users on (users.id = products_users.user_id) WHERE product_id in (12,13) and users.dnd_flag = 1 group by user_id";
		$query2 = "SELECT mobile FROM `products_users` inner join users on (users.id = products_users.user_id) WHERE product_id in (12,13) and users.dnd_flag = 0 group by user_id";
		$query3 = "SELECT * FROM ((SELECT mobile FROM users where balance > 0 and dnd_flag = 0) UNION (SELECT mobile FROM `products_users` inner join users on (users.id = products_users.user_id) WHERE product_id not in (12,13) and products_users.active = 1 and users.dnd_flag = 0 group by user_id) UNION (SELECT mobile FROM `packages_users` inner join users on (users.id = packages_users.user_id) WHERE packages_users.active = 1 and users.dnd_flag = 0 group by user_id)) as table1";
		$query4 = "SELECT * FROM ((SELECT mobile FROM users where balance > 0 and dnd_flag = 1) UNION (SELECT mobile FROM `products_users` inner join users on (users.id = products_users.user_id) WHERE product_id not in (12,13) and products_users.active = 1 and users.dnd_flag = 1 group by user_id) UNION (SELECT mobile FROM `packages_users` inner join users on (users.id = packages_users.user_id) WHERE packages_users.active = 1 and users.dnd_flag = 1 group by user_id)) as table1";
		
		$data = $this->Retailer->query($query1);
		$mobiles = array();
		foreach($data as $dt){
			$mobiles[] = $dt['users']['mobile'];
		}
		$message = "Dear User,
Your number is registered with Do Not Disturb Registry. To continue receiving Playwin results, you must deregister by calling 1909 or sending message STOP DND to 1909. 
As per new TRAI rules, we can only send SMSes to Non-DND users between 9AM to 9PM, so you will receive Playwin results at morning 9 AM";
		$this->General->sendMessage(SMS_SENDER,$mobiles,$message,'sms');
		exit;
		
		$file_handle = fopen("smstadka-DND.txt", "r");
		while (!feof($file_handle)) {
		   $line = trim(fgets($file_handle));
		   if(!empty($line)) {
		   	  $this->Retailer->query("UPDATE users SET dnd_flag = 1 WHERE mobile='$line'");
		   }
		}
		fclose($file_handle);exit;
		$this->printArray($this->Session->read('Auth.User'));exit;
		$query = "SELECT packages.name,logs.content FROM packages,logs WHERE logs.package_id = packages.id AND packages.toshow = 1 group by package_id order by logs.package_id,logs.id desc";
		$data = $this->Retailer->query($query);
		
		foreach($data as $dt){
			$package= $dt['packages']['name'];
			$text1 = addslashes("*$package*
<Variable 1>");
			$text2 = addslashes("e.g - SMS Subscription of $package subscribed by a user for a month. User pays for this service");
			$text3 = addslashes(strip_tags($dt['logs']['content']));
			$this->Retailer->query("INSERT INTO test VALUES ('$text1','$text2','$text3')");
		}
		exit;
		$query = "SELECT mobile FROM `vendors_retailers` where mobile is not null AND vendor_id = " .VENDOR_OSS;
		$data = $this->Retailer->query($query);
		$mobiles = array();
		foreach($data as $dt){
			$message = "New offer week has started!!
25th raat 12 baje tak sirf Rs. 700 ka transaction karo aur paao Rs. 2499 ki Reebok Watch 100% sure.
Check last week's winners, at http://www.smstadka.com/offers.
Keep selling SMSTadka VAS packs to win daily lucky & weekly confirmed prizes.
For more info: SMS: OSSBP to 09223178889";
			$this->General->sendMessage(SMS_SENDER,array($dt['vendors_retailers']['mobile']),$message,'sms');
		}
		exit;
		
		$data = $this->Retailer->query("SELECT retailer_code,user_id,id FROM vendors_retailers");
		foreach($data as $dt){
			if(!empty($dt['vendors_retailers']['user_id'])){
				$query = "SELECT user_id,users.mobile FROM user_taggings,users WHERE users.id = user_id and tagging_id = 10 AND user_id in (SELECT user_id FROM user_taggings,taggings WHERE taggings.id = user_taggings.tagging_id AND taggings.name='".$dt['vendors_retailers']['retailer_code']."')";
				$data1 = $this->Retailer->query($query);
				if(!empty($data1)){
					$this->Retailer->query("UPDATE vendors_retailers SET user_id = " . $data1['0']['user_taggings']['user_id'] . ", mobile = '" . $data1['0']['users']['mobile'] . "' WHERE id = " . $dt['vendors_retailers']['id']);
				}
			}
		}
		
		
		exit;
		$query = "SELECT users.id FROM `user_taggings`,users where users.id = user_taggings.user_id and tagging_id = 22 AND user_id in (SELECT user_id FROM `user_taggings` where tagging_id = 10)";
		$data = $this->Retailer->query($query);
		
		//$this->General->mailToUsers("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code<br/>Please send us the mobile number and address of this retailer.", array('ashisharya.iitb@gmail.com'));
		exit;
		$query = "SELECT users.id FROM `user_taggings`,users where users.id = user_taggings.user_id and tagging_id = 22 AND user_id in (SELECT user_id FROM `user_taggings` where tagging_id = 10)";
		$data = $this->Retailer->query($query);
		$mobiles = array();
		foreach($data as $dt){
			$mobiles[] = $dt['users']['mobile'];
		}
		$message = "Special Offer for OSS BP! Already 3 prizes worth Rs 7500 are given away.
You can also win prizes, so sell SMSTadka VAS packs from your mobileseva account.
WIN Mobiles, Digital Cameras, Watches & a lot more.
Also win weekly assured prizes. For more info: SMS: OSSBP to 09223178889 or log on to MobileSeva.in";
		$this->General->sendMessage(SMS_SENDER,$mobiles,$message,'sms');
		//$this->General->br2newline(
		exit;
		
		$data = $this->Retailer->query("SELECT vendor_id,vendor_retail_code,sum(products.price) as totalSale FROM vendors_activations INNER JOIN products_users ON (products_users.id = productuser_id) INNER JOIN products ON (products.id = products_users.product_id) INNER JOIN vendors ON (vendors.id = vendor_id) WHERE vendors.active = 1 group by vendor_retail_code");
		foreach($data as $dt){
			$this->Retailer->query("INSERT INTO vendors_retailers (vendor_id,retailer_code) VALUES (".$dt['vendors_activations']['vendor_id'].",'".$dt['vendors_activations']['vendor_retail_code']."')");
		}
		exit;
		echo date_diff(date('Y-m-d H:i:s'), date('Y-m-d H:i:s',strtotime('-4 days'))); exit;
		$data = $this->Retailer->query("SELECT data FROM jokes1");
		foreach($data as $dt){
			$content = $dt['jokes1']['data'];
			$this->Message->create();
			$this->data['Message']['content'] = nl2br($content);
			$this->data['Message']['category_id'] = 0; 
			$this->data['Message']['toshow'] = 0; 
			$this->data['Message']['created'] = date('Y-m-d H:i:s'); 
			$this->Message->save($this->data);
			$table = 'data_fun';
			
			$this->data[$table]['package_id'] = 35;
			$this->data[$table]['message_id'] = $this->Message->id;
			$this->$table->create();
			$this->$table->save($this->data);
		}
		exit;
		$data = $this->Retailer->query("SELECT products_users.user_id,vendor_retail_code from vendors_activations inner join products_users on (products_users.id = vendors_activations.productuser_id) where vendor_id = 3");
		foreach($data as $dt){
			if(trim($dt['vendors_activations']['vendor_retail_code']) != 'Test123')
			$this->General->tagUser($dt['vendors_activations']['vendor_retail_code'],$dt['products_users']['user_id']);
		}
		
		exit;
		set_time_limit(0);
		ini_set("memory_limit","-1");
		App::import('Vendor','xtcpdf'); 
		$tcpdf = new XTCPDF();
		$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
		
		$tcpdf->SetAuthor(COMPANY);
		$tcpdf->SetAutoPageBreak( false );
		$tcpdf->xheadercolor = array(150,0,0);
		
		// add a page (required with recent versions of tcpdf)
		$tcpdf->AddPage();
		
		// Now you position and print your page content
		// example: 
		$tcpdf->SetTextColor(0, 0, 0);
		$tcpdf->SetFont($textfont,'B',10);
		$buffer = file_get_contents(SITE_NAME . "shops/printInvoice/8");
		$tcpdf->WriteHTML($buffer, true, 0, true, 0);
		$tcpdf->Cell(0,14, "Hello World", 0,1,'L');
		// ...
		// etc.
		// see the TCPDF examples 
		
		echo $tcpdf->Output('filename.pdf', 'D');
	}
	
	function script(){
		$this->Invoice->recursive = -1;
		$invoices = $this->Invoice->find('all');
		foreach($invoices as $invoice){
			if($invoice['Invoice']['group_id'] == SUPER_DISTRIBUTOR){
				$parent = null;
			}
			else if($invoice['Invoice']['group_id'] == DISTRIBUTOR || $invoice['Invoice']['group_id'] == RETAILER){
				$shop = $this->Shop->getShopDataById($invoice['Invoice']['ref_id'],$invoice['Invoice']['group_id']);
				$parent = $shop['parent_id'];
			}
			$this->Invoice->updateAll(array('Invoice.from_id' => $parent),array('Invoice.id' => $invoice['Invoice']['id']));
		}
		$this->autoRender = false;
	}
	
	function issue(){
		$id = $_REQUEST['id'];
		$type = $_REQUEST['type'];
		$child = $_REQUEST['child'];
		
		if($type == RECEIPT_TOPUP){
			$number = $this->Shop->getTopUpReceiptNumber($id);
		}
		else if($type == RECEIPT_INVOICE){
			$invoice = $this->Invoice->find('first',array('fields' => array('Invoice.timestamp','Invoice.invoice_number'), 'conditions' => array('Invoice.id' => $id)));
			$number = $invoice['Invoice']['invoice_number'];
		}
		$this->set('number',$number);
		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('child',$child);
		$this->render('receipt_form','ajax');
	}
	
	function backReceipt(){
		$this->set('data',$this->data);
		$this->render('receipt_form','ajax');
	}
	
	function issueReceipt(){
		$to_save = true;
		$confirm = 0;
		$empty = true;
		$amount = 0;
		if(isset($this->data['confirm']))
			$confirm = $this->data['confirm'];
		
		if(!empty($this->data['Receipt']['cash_amount'])){
			$empty = false;
			$this->data['Receipt']['cash_amount'] = trim($this->data['Receipt']['cash_amount']);
			preg_match('/^[0-9]/',$this->data['Receipt']['cash_amount'],$matches,0);
			if($this->data['Receipt']['cash_amount'] <= 0 || empty($matches)){
				$to_save = false;
				$err_msg = "Cash amount is not valid";
			}
			else {
				$amount += $this->data['Receipt']['cash_amount'];
			}
		}
		else {
			$this->data['Receipt']['cash_amount'] = 0;
		}
		
		if(!empty($this->data['Receipt']['cheque_amount'])){
			$empty = false;
			$this->data['Receipt']['cheque_amount'] = trim($this->data['Receipt']['cheque_amount']);
			preg_match('/^[0-9]/',$this->data['Receipt']['cheque_amount'],$matches,0);
			if($this->data['Receipt']['cheque_amount'] <= 0 || empty($matches)){
				$to_save = false;
				$err_msg = "Cheque amount is not valid";
			}
			else {
				$amount += $this->data['Receipt']['cheque_amount'];
				$this->data['Receipt']['cheque_number'] = trim($this->data['Receipt']['cheque_number']);
				preg_match('/^[0-9]/',$this->data['Receipt']['cheque_number'],$matches,0);
				if(empty($this->data['Receipt']['cheque_number']) || empty($matches)){
					$to_save = false;
					$err_msg = "Please enter the valid cheque number";
				}
				else {
					$this->data['Receipt']['cheque_date'] = trim($this->data['Receipt']['cheque_date']);
					$this->data['Receipt']['cheque_bank'] = trim($this->data['Receipt']['cheque_bank']);
					$this->data['Receipt']['cheque_branch'] = trim($this->data['Receipt']['cheque_branch']);
					$dates = explode("-",$this->data['Receipt']['cheque_date']);
					if(empty($this->data['Receipt']['cheque_date']) || !checkdate($dates[1],$dates[0],$dates[2])){
						$to_save = false;
						$err_msg = "Please enter the valid cheque date in dd-mm-yyyy format";
					}
					else if(empty($this->data['Receipt']['cheque_bank'])){
						$to_save = false;
						$err_msg = "Please enter the bank name of the cheque";
					}
					else if(empty($this->data['Receipt']['cheque_branch'])){
						$to_save = false;
						$err_msg = "Please enter the branch of the bank mentioned";
					}
				}
			}
		}
		else {
			$this->data['Receipt']['cheque_amount'] = 0;
		}
		
		if(!empty($this->data['Receipt']['transfer_amount'])){
			$empty = false;
			$this->data['Receipt']['transfer_amount'] = trim($this->data['Receipt']['transfer_amount']);
			preg_match('/^[0-9]/',$this->data['Receipt']['transfer_amount'],$matches,0);
			if($this->data['Receipt']['transfer_amount'] <= 0 || empty($matches)){
				$to_save = false;
				$err_msg = "Transfer amount is not valid";
			}
			else {
				$amount += $this->data['Receipt']['transfer_amount'];
				$this->data['Receipt']['transfer_ref_id'] = trim($this->data['Receipt']['transfer_ref_id']);
				if(empty($this->data['Receipt']['transfer_ref_id'])){
					$to_save = false;
					$err_msg = "Please enter the transfer reference id";
				}
			}
		}
		else {
			$this->data['Receipt']['transfer_amount'] = 0;
		}
		
		if($empty){
			$to_save = false;
			$err_msg = "Please enter some amount";
		}
		else {
			$outstanding = $this->Shop->getOutstandingBalance($this->data['id'],$this->data['type']);
			if($outstanding < $amount) {
				$to_save = false;
				$err_msg = "Entered amount cannot be greater than the outstanding balance";
			}
		}
		
		if(!$to_save){
			$this->set('data',$this->data);
			$msg = '<div class="error_class">'.$err_msg.'</div>';
			$this->Session->setFlash(__($msg, true));
			$this->render('receipt_form','ajax');
		}
		else if($confirm == 0){
			$this->set('total',$amount);
			$this->set('outstanding',($outstanding - $amount));
			$this->set('data',$this->data);
			$this->render('confirm_receipt','ajax');
		}
		else {
			$this->Receipt->recursive = -1;
			$this->Receipt->create();
			
			$this->data['Receipt']['shop_from_id'] = $this->info['id'];
			$this->data['Receipt']['shop_to_id'] = $this->data['child'];
			$this->data['Receipt']['group_id'] = $this->Session->read('Auth.User.group_id') + 1;
			$this->data['Receipt']['receipt_type'] = $this->data['type'];
			$this->data['Receipt']['receipt_ref_id'] = $this->data['id'];
			$this->data['Receipt']['total_amount'] = $amount;
			$this->data['Receipt']['os_amount'] = $outstanding - $amount;
			$this->data['Receipt']['timestamp'] = date("Y-m-d H:i:s");
			$this->data['Receipt']['cheque_date'] = $dates[2]."-".$dates[1]."-".$dates[0];
			
			if($this->Receipt->save($this->data)){
				$rec_num = $this->Shop->getReceiptNumber($this->Receipt->id);
				$this->Receipt->query("UPDATE receipts SET receipt_number = '$rec_num' WHERE id = " . $this->Receipt->id);
				echo "<script>$('bg').hide(); $('popUpDiv').hide();window.location = '".SITE_NAME."shops/topupReceipts'</script>";
				$this->autoRender = false;
			}
		}
	}
	
	function printReceipt($receipt_id){
		if(!$this->Session->check('Auth.User')) $this->logout();
		$this->Receipt->recursive = -1;
		$data = $this->Receipt->findById($receipt_id);
		
		$shop = $this->Shop->getShopDataById($data['Receipt']['shop_to_id'],$data['Receipt']['group_id']);
		if($data['Receipt']['group_id'] != SUPER_DISTRIBUTOR){
			$parentShop = $this->Shop->getShopDataById($data['Receipt']['shop_from_id'],$data['Receipt']['group_id'] - 1);
			$data['company'] = $parentShop['company'];
			$data['address'] = $parentShop['address'];
			$parentData = $this->General->getUserDataFromId($parentShop['user_id']);
			$data['mobile'] = "+91" . $parentData['mobile'];
		}
		else {
			$data['company'] = SMSTADKA_COMPANY;
			$data['address'] = SMSTADKA_ADDRESS;
			$data['mobile'] = SMSTADKA_CONTACT;
		}
		
		if($data['Receipt']['group_id'] != RETAILER){
			$data['name'] = $shop['company'];	
		}
		else {
			$data['name'] = $shop['shopname'];
		}
		
		if($data['Receipt']['receipt_type'] == RECEIPT_TOPUP){
			$number = $this->Shop->getTopUpReceiptNumber($data['Receipt']['receipt_ref_id']);
		}
		else if($data['Receipt']['receipt_type'] == RECEIPT_INVOICE){
			$invoice = $this->Invoice->find('first',array('fields' => array('Invoice.timestamp','Invoice.invoice_number'), 'conditions' => array('Invoice.id' => $data['Receipt']['receipt_ref_id'])));
			$number = $invoice['Invoice']['invoice_number'];
		}
		
		$data['number'] = $number;
		$data['addr'] = $shop['address'];
		$this->set('data',$data);
		$this->render('/elements/receipt','print');
	}
	
	function PNRListing($date=null){
		if(!$this->Session->check('Auth.User')) $this->logout();
		$to_show = false;
		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
				$to_show = true;
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			}
		}
		else {
			$this->set('empty',1);
		}
		if($to_show && $this->Session->read('Auth.User.group_id') == RETAILER){
			$this->Pnr->recursive = -1;
			$pnrs = $this->Pnr->find('all',array('fields' => array('Pnr.mobile','Pnr.pnr_number','Pnr.start','Product.name'),
					'conditions' => array('Pnr.retailer_id' => $this->info['id'], 'Pnr.product_id IN ('.PNR_PRODUCTS.')','Date(Pnr.start) >= "' . $date_from. '"','Date(Pnr.start) <= "' . $date_to . '"'),
					'joins' => array(
						array(
							'table' => 'products',
							'type' => 'inner',
							'alias' => 'Product',
							'conditions' => array('Pnr.product_id = Product.id')
						)
					),
					'order' => 'Pnr.start desc'
			));
		}
		else {
			$pnrs = array();
		}
		$this->set('pnrs',$pnrs);
		$this->render('pnr_listing');
	}
	
	function getCreditDebitNotes($date=null){
		$transactions = array();
		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
				
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
					$data_to = $this->Retailer->query("SELECT shop_creditdebit.*,'MindsArray Technologies' as shop FROM shop_creditdebit WHERE shop_creditdebit.to_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".SUPER_DISTRIBUTOR." AND shop_creditdebit.from_id is null AND Date(shop_creditdebit.timestamp) >= '$date_from' AND  Date(shop_creditdebit.timestamp) <= '$date_to' order by shop_creditdebit.timestamp desc,shop_creditdebit.id desc");
					$data_from = $this->Retailer->query("SELECT shop_creditdebit.*,SUBSTR(distributors.company,1) as shop FROM shop_creditdebit INNER JOIN distributors ON (distributors.id = shop_creditdebit.to_id) WHERE shop_creditdebit.from_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".DISTRIBUTOR." AND Date(shop_creditdebit.timestamp) >= '$date_from' AND  Date(shop_creditdebit.timestamp) <= '$date_to' order by shop_creditdebit.timestamp desc,shop_creditdebit.id desc");
					$transactions['to'] = $data_to;
					$transactions['from'] = $data_from;
				}
				else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
					$data_to = $this->Retailer->query("SELECT shop_creditdebit.*,SUBSTR(super_distributors.company,1) as shop FROM shop_creditdebit LEFT JOIN super_distributors ON (super_distributors.id = shop_creditdebit.from_id) WHERE shop_creditdebit.to_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".DISTRIBUTOR." AND Date(shop_creditdebit.timestamp) >= '$date_from' AND  Date(shop_creditdebit.timestamp) <= '$date_to' order by shop_creditdebit.timestamp desc,shop_creditdebit.id desc");
					$data_from = $this->Retailer->query("SELECT shop_creditdebit.*,SUBSTR(retailers.shopname,1) as shop FROM shop_creditdebit INNER JOIN retailers ON (retailers.id = shop_creditdebit.to_id) WHERE shop_creditdebit.from_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".RETAILER." AND Date(shop_creditdebit.timestamp) >= '$date_from' AND  Date(shop_creditdebit.timestamp) <= '$date_to' order by shop_creditdebit.timestamp desc,shop_creditdebit.id desc");
					$transactions['to'] = $data_to;
					$transactions['from'] = $data_from;
				}
				else if($this->Session->read('Auth.User.group_id') == RETAILER){
					$transactions['to'] = $this->Retailer->query("SELECT shop_creditdebit.*,SUBSTR(distributors.company,1) as shop FROM shop_creditdebit INNER JOIN distributors ON (distributors.id = shop_creditdebit.from_id) WHERE shop_creditdebit.to_id =  ".$this->info['id'] . " AND shop_creditdebit.to_groupid = ".RETAILER . " AND Date(shop_creditdebit.timestamp) >= '$date_from' AND  Date(shop_creditdebit.timestamp) <= '$date_to' order by shop_creditdebit.timestamp desc,shop_creditdebit.id desc");
				}
		
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
			
			}
		}
		
		//$this->printArray($transactions);
		if($date == null) $this->set('empty',0);
		$this->set('creditDebits',$transactions);
		$this->render('get_credit_debits');
	}
	
	function createCreditDebitNotes(){
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR)$this->redirect('/shops/view');
		$this->render('credit_debit');
	}
	
	function createNote(){
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR)$this->redirect('/shops/view');
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
			$confirm = $this->data['confirm'];
		preg_match('/^[0-9]/',$this->data['amount'],$matches,0);
		if($this->data['shop'] == 0){
			if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR)
			$msg = "Please select distributor";
			else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR)
			$msg = "Please select retailer";
			$to_save = false;
		}
		else if(empty($this->data['amount'])){
			$msg = "Please enter some amount";
			$to_save = false;
		}
		else if($this->data['amount'] <= 0 || empty($matches)){
			$msg = "Amount entered is not valid";
			$to_save = false;
		}
		else {
			if($this->data['note'] == 0){//Credit Note
				$shop = $this->Shop->getShopData($this->Session->read('Auth.User.id'),$this->Session->read('Auth.User.group_id'));
				$bal = $shop['balance'];
				$msg = "Credit amount cannot be greater than your account balance";
			}
			else {
				$shop = $this->Shop->getShopDataById($this->data['shop'],$this->Session->read('Auth.User.group_id') + 1);
				$bal = $shop['balance'];
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR)
					$msg = "Debit amount cannot be greater account balance of selected distributor";
				else $msg = "Debit amount cannot be greater account balance of selected retailer";
			}
			if($this->data['amount'] > $bal){
				$to_save = false;
			}
			else {
				$shop = $this->Shop->getShopDataById($this->data['shop'],$this->Session->read('Auth.User.group_id') + 1);
				$this->data['shopData'] = $shop;
				if($confirm == 0){
					$this->set('data',$this->data);
					$this->render('confirm_credit_debit','ajax');
				}
				else {
					if($this->data['note'] == 0)
						$mail_subject = "Credit Note: Amount Credited";
					else 
						$mail_subject = "Debit Note: Amount Debited";	
					
					$numbering = $this->Shop->getCreditDebitNumber($shop['id'],$this->info['id'],$this->Session->read('Auth.User.group_id') + 1,$this->data['note']);
					if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
						$this->Shop->shopCreditDebitUpdate($this->data['note'],$this->data['amount'],$this->info['id'],$shop['id'],DISTRIBUTOR,$this->data['description'],$numbering);
						if($this->data['note'] == 0){
							$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$this->info['id'],SUPER_DISTRIBUTOR);
							$this->Shop->shopBalanceUpdate($this->data['amount'],'add',$shop['id'],DISTRIBUTOR);
							$mail_body = "SuperDistributor: " . $this->info['company'] . " make a Credit Note of Rs. " . $this->data['amount'] . " for Distributor: " . $shop['company'];
							echo "<script> reloadShopBalance(".$bal.");  </script>";
						}
						else {
							$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'add',$this->info['id'],SUPER_DISTRIBUTOR);
							$this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$shop['id'],DISTRIBUTOR);
							$mail_body = "SuperDistributor: " . $this->info['company'] . " make a Debit Note of Rs. " . $this->data['amount'] . " for Distributor: " . $shop['company'];
							echo "<script> reloadShopBalance(".$bal.");  </script>";
						}
					}
					else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
						$this->Shop->shopCreditDebitUpdate($this->data['note'],$this->data['amount'],$this->info['id'],$shop['id'],RETAILER,$this->data['description'],$numbering);
						if($this->data['note'] == 0){
							$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$this->info['id'],DISTRIBUTOR);
							$this->Shop->shopBalanceUpdate($this->data['amount'],'add',$shop['id'],RETAILER);
							$mail_body = "Distributor: " . $this->info['company'] . " make a Credit Note of Rs. " . $this->data['amount'] . " for Retailer: " . $shop['shopname'];
							echo "<script> reloadShopBalance(".$bal.");  </script>";
						}
						else {
							$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'add',$this->info['id'],DISTRIBUTOR);
							$this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$shop['id'],RETAILER);
							$mail_body = "Distributor: " . $this->info['company'] . " make a Debit Note of Rs. " . $this->data['amount'] . " for Retailer: " . $shop['shopname'];
							echo "<script> reloadShopBalance(".$bal.");  </script>";
						}
					}
					//$this->General->mailToAdmins($mail_subject, $mail_body);
					$this->render('/elements/shop_creditDebit','ajax');
				}
			}
		}
		
		if(!$to_save){
			$this->set('data',$this->data);
			$msg = '<div class="error_class">'.$msg.'</div>';
			$this->Session->setFlash(__($msg, true));
			$this->render('/elements/shop_creditDebit','ajax');
		}
	}
	
	function backCreditDebit(){
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR)$this->redirect('/shops/view');
		$this->set('data',$this->data);
		$this->render('/elements/shop_creditDebit');
	}
	
	function printCreditDebitNote($id){
		if(!$this->Session->check('Auth.User')) $this->logout();
		$id = $this->objMd5->Decrypt($id,encKey);
		$note = $this->Retailer->query("SELECT * FROM shop_creditdebit WHERE id = $id");
		
		if(empty($note['0']['shop_creditdebit']['from_id'])){
			$receipt['from_addr'] = SMSTADKA_ADDRESS;
			$receipt['from_comp'] = SMSTADKA_COMPANY;
			$receipt['mobile'] = SMSTADKA_CONTACT;
		}
		else {
			$from = $this->Shop->getShopDataById($note['0']['shop_creditdebit']['from_id'],$note['0']['shop_creditdebit']['to_groupid'] - 1);
			$receipt['from_addr'] = $from['address'];
			$receipt['from_comp'] = $from['company'];
			$parentData = $this->General->getUserDataFromId($from['user_id']);
			$receipt['mobile'] = "+91" . $parentData['mobile'];
		}
		
		$to = $this->Shop->getShopDataById($note['0']['shop_creditdebit']['to_id'],$note['0']['shop_creditdebit']['to_groupid']);
		if(isset($to['company']))
			$company = $to['company'];
		else 
			$company = $to['shopname'];
		$receipt['to_addr'] = $to['address'];
		$receipt['to_comp'] = $company;
		$this->set('data',$receipt);
		$this->set('note',$note);
		$this->render('/elements/credit_debit','print');
	}
	
	function cronUpdateBalanceLFRS(){//every night 2 AM
		//$this->lock();
		$date = date('Y-m-d',strtotime('-1 day'));
		$lfr_id = VENDOR_TSS; $distributor_id = 9; $superdistributor_id = 1;
		$data = $this->Retailer->query("SELECT products_users.product_id,count(vendors_activations.id) as counts,products.price FROM vendors_activations INNER JOIN products_users ON (vendors_activations.productuser_id = products_users.id) INNER JOIN products ON (products.id = products_users.product_id) WHERE vendor_id = $lfr_id AND Date(vendors_activations.timestamp) = '$date' group by product_id");
		$products = array();
		$amount = 0;
		$v_commission = 0;
		$products = array();
		if(!empty($data)){
			foreach($data as $dt){
				$product = $dt['products_users']['product_id'];
				$price = $dt['products']['price'];
				
				$amount += $price*$dt['0']['counts'];
				
				$vendor_commission = $this->Retailer->query("SELECT commission_percent FROM vendors_commissions WHERE vendor_id = $lfr_id");
				$vendor_commission = $vendor_commission['0']['vendors_commissions']['commission_percent'];
				
				$v_commission = $v_commission + ($price*$dt['0']['counts']*($vendor_commission/100));
				$products[$product] = $dt['0']['counts'];
			}
			$commission_superdistributor = $this->Shop->calculateCommission($superdistributor_id,SUPER_DISTRIBUTOR,$products,$date);
			$tds_superdistributor = $this->Shop->calculateTDS($commission_superdistributor);
			$commission_superdistributor -= $tds_superdistributor;
			
			$shop_superdist = $this->Shop->getShopDataById($superdistributor_id,SUPER_DISTRIBUTOR);
			if($shop_superdist['tds_flag'] == '1'){
				$v_tds = $this->calculateTDS($v_commission);
				$v_commission -= $v_tds;
			}
			
			$debit = $amount - $v_commission;
			$credit = $commission_superdistributor - $v_commission;
			
			$credit_number = $this->Shop->getCreditDebitNumber($superdistributor_id,null,SUPER_DISTRIBUTOR,0);
			$this->Shop->shopCreditDebitUpdate(0,$credit,null,$superdistributor_id,SUPER_DISTRIBUTOR,'Commission added due to SMARTSHOP sale on ' . date('s-m-Y') ,$credit_number,1);
			
			$dedit_number = $this->Shop->getCreditDebitNumber($distributor_id,$superdistributor_id,DISTRIBUTOR,1);
			$this->Shop->shopCreditDebitUpdate(1,$debit,$superdistributor_id,$distributor_id,DISTRIBUTOR,'Amount debited due to SMSTadka sale on ' . date('s-m-Y') ,$dedit_number,1);
			
			$this->Shop->shopBalanceUpdate($debit,'subtract',$distributor_id,DISTRIBUTOR);
			$this->Shop->shopBalanceUpdate($credit,'add',$superdistributor_id,SUPER_DISTRIBUTOR);
		
		}
		$this->releaseLock();
		$this->autoRender = false;
	}
        
}

?>