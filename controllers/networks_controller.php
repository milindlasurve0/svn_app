<?php
class NetworksController extends AppController {

	var $name = 'Networks';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler','Shop');
	var $uses = array('Network');
	public $maxRec = 10;
	 
	function beforeFilter() {
		session_start();
		parent::beforeFilter();
		$this->Auth->allowedActions = array('*');

		$totCnt = $this->Network->query("select count(user_id) as cnt from products_users where product_id in (select product_id from partners_products where partner_id = ".$_SESSION['partnerId'].")");
		$this->set('totSub',$totCnt[0][0]['cnt']);
		
		$totActive = $this->Network->query("select count(user_id) as cnt from products_users where product_id in (select product_id from partners_products where partner_id = ".$_SESSION['partnerId'].") and active = 1");
		$this->set('totActive',$totActive[0][0]['cnt']);
		
		$this->layout = 'network';
	}
	
	function sessionChk(){
		if(!$_SESSION['username'])$this->redirect(array('action' => 'index'));
	}
	
	function logout(){
		session_destroy();
		$this->redirect(array('action' => 'index'));	
	}
	
	function index(){		
		if($this->Session->check('username'))$this->redirect(array('action' => 'subscriptions'));
		$this->render('index');
	}
	
	function afterLogin(){		
		$username = $_POST['username'];
		$password = $_POST['password'];
		
		$usrData = $this->Network->query("select id,username from partners where username='".$username."' and password = '".$password."'");
		if(empty($usrData)){			
			echo '0';
		}else{
			$_SESSION['username'] = $usrData['0']['partners']['username'];
			$_SESSION['partnerId'] = $usrData['0']['partners']['id'];
			echo '1';
		}
		$this->autoRender = false;
	}
	
	function formatDate($date){
		$tmp = explode('-',$date);							
		return $tmp[2]."-".$tmp[1]."-".$tmp[0];			
	} 
	
	function refine($type = null){
		//Array ( [fromDate] => 01-10-2011 [toDate] => 14-10-2011 [product] => 17 )
		if(trim($_POST['fromDate']) == '')
		$frmDate = date("d-m-Y");
		else
		$frmDate = $_POST['fromDate'];
		
		if(trim($_POST['toDate']) == '')
		$toDate = date("d-m-Y");
		else
		$toDate = $_POST['toDate'];
		
		if(is_null($toDate))
		$toDate = date("d-m-Y");
		
		if($type == 'swi')
		$this->redirect(array('controller' => 'networks','action' => 'switchings',$frmDate,$toDate,$_POST['product']));
		else
		$this->redirect(array('controller' => 'networks','action' => 'subscriptions',$frmDate,$toDate,$_POST['product'])); 
		$this->autoRender = false;
	}
	
	function subscriptions($frmDate=null,$toDate=null,$prodId=null,$page=null){		
		$this->sessionChk();
		
		if(is_null($frmDate))
		$frmDate = date("d-m-Y");
		$this->set('frmDate',$frmDate);
		
		if(is_null($toDate))
		$toDate = date("d-m-Y");
		$this->set('toDate',$toDate);
		
		if(is_null($prodId))
		$prodId = 0;
		$this->set('prodId',$prodId);

		$switch = '';
		if($prodId == '0'){
			$switch = '0';
		}else{			
			$prdSub = $this->Network->query("select products.switching from partners_products,products where partners_products.partner_id=".$_SESSION['partnerId']." and partners_products.product_id = products.id and products.id = ".$prodId);
			$switch = $prdSub['0']['products']['switching'];
		}

		if($switch == '')
		$this->redirect(array('controller' => 'networks','action' => 'index'));

		$this->set('switch',$switch);
		
		$prdData = $this->Network->query("select products.name,partners_products.product_id from partners_products,products where partners_products.partner_id=".$_SESSION['partnerId']." and partners_products.product_id = products.id");
		$this->set('prdData',$prdData);
		
		//pagination 
		$recordsPerPage = $this->maxRec;
		if(is_null($page))
			$page = 1;
		
		$limit = ($page-1)*$recordsPerPage;	
		
		if($prodId == '0'){
			$prdSub = $this->Network->query("select SUM(count-trial) as count,products_users.user_id,users.mobile,users.id,products.name,products.price from products_users,users,products where products_users.user_id=users.id and products_users.product_id in (select product_id from partners_products where partner_id = ".$_SESSION['partnerId'].") and CAST(products_users.start AS DATE) between '".$this->formatDate($frmDate)."' and  '".$this->formatDate($toDate)."' and products.id=products_users.product_id group by products_users.user_id,products_users.product_id limit ".$limit.",".$recordsPerPage);
			$prdSubCnt = $this->Network->query("select SUM(count-trial) as count,products.price from products_users,users,products where products_users.user_id=users.id and products_users.product_id in (select product_id from partners_products where partner_id = ".$_SESSION['partnerId'].") and CAST(products_users.start AS DATE) between '".$this->formatDate($frmDate)."' and  '".$this->formatDate($toDate)."' and products.id=products_users.product_id group by products_users.user_id,products_users.product_id");
			$switch = 0;
		}else{
			$prdSub = $this->Network->query("select SUM(count-trial) as count,products_users.user_id,users.mobile,users.id,products.name from products_users,users,products where products_users.user_id=users.id and products_users.product_id = ".$prodId." and CAST(products_users.start AS DATE) between '".$this->formatDate($frmDate)."' and '".$this->formatDate($toDate)."' and products.id=products_users.product_id group by products_users.user_id,products_users.product_id limit ".$limit.",".$recordsPerPage);
			$prdSubCnt = $this->Network->query("select SUM(count-trial) as count, products.price from products_users,users,products where products_users.user_id=users.id and products_users.product_id = ".$prodId." and CAST(products_users.start AS DATE) between '".$this->formatDate($frmDate)."' and '".$this->formatDate($toDate)."' and products.id=products_users.product_id group by products_users.user_id,products_users.product_id");
			$switch = $prdSub['0']['products']['switching'];
		}
		
		
		$sale = 0;
		$count = 0;
		foreach($prdSubCnt as $psb){
			$sale = $sale + $psb['0']['count']*$psb['products']['price'];
			$count = $count +  $psb['0']['count'];
		}
		$this->set('totSale',$sale);
		
		$this->set('totSubscription',$count);
		$pages = ceil(count($prdSubCnt)/$recordsPerPage);
			$paginationStr = '';
			if($pages > 1){
				//if($pages > 3 && $page != 1)
				$paginationStr .= "<li><span class='lightText'><a href='/networks/subscriptions/".$frmDate."/".$toDate."/".$prodId."/1'> &lt;&lt; First</a> </span>";
				$paginationStr .= '<li class="paginationNo">';				
				for($i=0;$i<$pages;$i++){
					$pageNo = $i+1;
					if(($page-2) == $pageNo || ($page-1) == $pageNo || $page == $pageNo || ($page+1) == $pageNo || ($page+2) == $pageNo || ($page+3) == $pageNo || ($page+4) == $pageNo){						
						if($page == $pageNo)
							$paginationStr .= "<span class='current'>".$pageNo."</span>";
						else
							$paginationStr .= "<span><a href='/networks/subscriptions/".$frmDate."/".$toDate."/".$prodId."/".$pageNo."'>".$pageNo."</a></span>";							
					}
				}
				$paginationStr .= '</li>';
				//if($pages > 3 && $page != $pageNo)
				$paginationStr .= "<li class='lastElement'><span><a class='noAffect' href='/networks/subscriptions/".$frmDate."/".$toDate."/".$prodId."/".$pageNo."'>Last &gt;&gt;</a> </span>";
			}
		
		$this->set('paginationStr',$paginationStr);		
		$this->set('prdSub',$prdSub);
	}
	
	function switchings($frmDate=null,$toDate=null,$prodId=null,$page=null){
		$this->sessionChk();
		
		if(is_null($frmDate))
		$frmDate = date("d-m-Y");
		$this->set('frmDate',$frmDate);
		
		if(is_null($toDate))
		$toDate = date("d-m-Y");
		$this->set('toDate',$toDate);
		
		$this->set('prodId',$prodId);
		
		$prdData = $this->Network->query("select products.name,partners_products.product_id from partners_products,products where partners_products.partner_id=".$_SESSION['partnerId']." and partners_products.product_id = products.id and products.switching = 1");
		$this->set('prdData',$prdData);
		
		$switch = '';	
		$prdSub = $this->Network->query("select products.switching from partners_products,products where partners_products.partner_id=".$_SESSION['partnerId']." and partners_products.product_id = products.id and products.id = ".$prodId);
		$switch = $prdSub['0']['products']['switching'];

		if($switch == '')
		$this->redirect(array('controller' => 'networks','action' => 'index'));
		
		$this->set('switch',$switch);
		
		$subProd = $this->Network->query("select products.name,products.id from products where products.parent_id=".$prodId);
		
		//pagination 
		$recordsPerPage = $this->maxRec;
		if(is_null($page))
			$page = 1;
		
		$limit = ($page-1)*$recordsPerPage;	
		
		$k = 0;
		foreach($subProd as $sbd){
			$users = $this->Network->query("select users.id,mobile from users join products_users_switch on (users.id = products_users_switch.user_id) where products_users_switch.product_id='".$sbd['products']['id']."' and CAST(products_users_switch.timestamp AS DATE) between '".$this->formatDate($frmDate)."' and '".$this->formatDate($toDate)."' limit ".$limit.",".$recordsPerPage);
			$totUsers = $this->Network->query("select mobile from users join products_users_switch on (users.id = products_users_switch.user_id) where products_users_switch.product_id='".$sbd['products']['id']."' and CAST(products_users_switch.timestamp AS DATE) between '".$this->formatDate($frmDate)."' and '".$this->formatDate($toDate)."'");			
			$subProd[$k]['users'] = $users;
			$subProd[$k]['totusers'] = $totUsers;
			$k++;			
		}
		/*echo "<pre>";
		print_r($subProd);
		echo "</pre>";*/
		
		//max records
		$count = 0;
		foreach($subProd as $sp){
			if(count($sp['users']) > $count)
			$count = count($sp['users']);
		}
		
		$this->set('count',$count);
		$this->set('subProd',$subProd);
				

		//total count 
		$totcount = 0;
		foreach($subProd as $sp){
			if(count($sp['totusers']) > $totcount)
			$totcount = count($sp['totusers']);
		}
		
		$pages = ceil($totcount/$recordsPerPage);
			$paginationStr = '';
			if($pages > 1){
				//if($pages > 3 && $page != 1)
				$paginationStr .= "<li><span class='lightText'><a href='/networks/switchings/".$frmDate."/".$toDate."/".$prodId."/1'> &lt;&lt; First</a> </span>";
				$paginationStr .= '<li class="paginationNo">';				
				for($i=0;$i<$pages;$i++){
					$pageNo = $i+1;
					if(($page-2) == $pageNo || ($page-1) == $pageNo || $page == $pageNo || ($page+1) == $pageNo || ($page+2) == $pageNo || ($page+3) == $pageNo || ($page+4) == $pageNo){						
						if($page == $pageNo)
							$paginationStr .= "<span class='current'>".$pageNo."</span>";
						else
							$paginationStr .= "<span><a href='/networks/switchings/".$frmDate."/".$toDate."/".$prodId."/".$pageNo."'>".$pageNo."</a></span>";							
					}
				}
				$paginationStr .= '</li>';
				//if($pages > 3 && $page != $pageNo)
				$paginationStr .= "<li class='lastElement'><span><a class='noAffect' href='/networks/switchings/".$frmDate."/".$toDate."/".$prodId."/".$pageNo."'>Last &gt;&gt;</a> </span>";
			}
		
		$this->set('paginationStr',$paginationStr);	
	}
	
	function userSwitch(){		
		$users = $this->Network->query("select mobile from users where id=".$_POST['id']);
		$this->set('mobile',$users['0']['users']['mobile']);
		
		$usersUniqueProd = $this->Network->query("select distinct products_users_switch.product_parent_id,products.name from products_users_switch,products where products_users_switch.user_id=".$_POST['id']." and products_users_switch.product_parent_id= products.id");
		
		$l = 0;
		foreach($usersUniqueProd as $uup){
			$usersUniqueProd[$l]['switches'] = $this->Network->query("select products_users_switch.product_id,products_users_switch.timestamp, products.name from products_users_switch,products where products_users_switch.user_id=".$_POST['id']." and products_users_switch.product_parent_id=".$uup['products_users_switch']['product_parent_id']." and products_users_switch.product_id= products.id order by timestamp asc");
			
			$l++;
		}
		/*echo "<pre>";
		print_r($usersUniqueProd);
		echo "</pre>";*/
		$this->set('usersUniqueProd',$usersUniqueProd);
		$this->render('/networks/user_switch','ajax');
		//$this->autoRender = false;
	}
	
	function updateMarshData(){
      	set_time_limit(0);
		ini_set("memory_limit","-1");
		
		App::import('Vendor', 'excel_reader2');
		$data = new Spreadsheet_Excel_Reader('Mobile App - Marsh data.xls', true);
		//employee table
		$i = 0;
		foreach($data->sheets[0]['cells'] as $user){
			if($i == 0){$i = 1; continue;
			}
			$user[8] = date('Y-m-d',strtotime(trim($user[8])));
			if(trim($user[13]) == 'Activated'){
				$user[13] = 1;
			}
			else $user[13] = 0;
			if(!isset($user[12]))$user[12] = '';
			if(!isset($user[10]))$user[10] = '';
			$data1 = $this->Network->query("SELECT * from msh_employee where employee_id = '".trim($user[1])."' AND company_code = '".trim($user[2])."' AND tpa_card = '" . trim($user[9])."'");
			if(empty($data1)){
				$this->Network->query("INSERT INTO msh_employee (employee_id,company_code,tpa_id,policy_num,member_name,relationship,dob,tpa_card,mobile,email_id,city,active,timestamp) VALUES ('".trim($user[1])."','".trim($user[2])."','".trim($user[3])."','".trim($user[5])."','".addslashes(trim($user[6]))."','".trim($user[7])."','".$user[8]."','".trim($user[9])."','".trim($user[10])."','".trim($user[11])."','".$user[12]."',".$user[13].",'".date('Y-m-d H:i:s')."')");
			}
			else {
				$this->Network->query("UPDATE msh_employee SET tpa_id='".trim($user[3])."',policy_num='".trim($user[5])."',member_name='".addslashes(trim($user[6]))."',relationship='".trim($user[7])."',dob='".$user[8]."',mobile='".trim($user[10])."',email_id='".trim($user[11])."',city='".trim($user[12])."',active=".$user[13] . " WHERE id = " . $data1['0']['msh_employee']['id']);
			}
			$i++;
		}
		//claims
		$i = 0;
		foreach($data->sheets[1]['cells'] as $user){
			if($i == 0){
				$i = 1; continue;
			}
			$user[8] = date('Y-m-d',strtotime(trim($user[8])));
			if(!empty($user[10]))
			$user[10] = date('Y-m-d',strtotime(trim($user[10])));
			else $user[10] = '';
			
			if(isset($user[13]) && !empty($user[13]))
			$user[13] = date('Y-m-d',strtotime(trim($user[13])));
			else $user[13] = '';
			
			if(!isset($user[17]))$user[17] = '';
			if(!isset($user[14]) || empty($user[14]))$user[14] = 0;
			$data1 = $this->Network->query("SELECT * from msh_claims where employee_id = '".trim($user[1])."' AND company_code = '".trim($user[2])."' AND tpa_card = '" . trim($user[5])."'");
			if(empty($data1)){
				$this->Network->query("INSERT INTO msh_claims (employee_id,company_code,tpa_card,claim_number,member_name,claim_date,claim_amount,claim_logged_date,claim_status,paid_amount,claim_settled_date,balance_sum_insured,hospital_name,city,notes,timestamp) VALUES ('".trim($user[1])."','".trim($user[2])."','".trim($user[5])."','".trim($user[6])."','".addslashes(trim($user[7]))."','".$user[8]."','".trim($user[9])."','".$user[10]."','".trim($user[11])."','".trim($user[12])."','".$user[13]."','".trim($user[14])."','".trim($user[15])."','".trim($user[16])."','".trim($user[17])."','".date('Y-m-d H:i:s')."')");
			}
			else {
				$this->Network->query("UPDATE msh_claims SET claim_number='".trim($user[6])."',member_name='".addslashes(trim($user[7]))."',claim_date='".$user[8]."',claim_amount='".trim($user[9])."',claim_logged_date='".$user[10]."',claim_status='".trim($user[11])."',paid_amount='".trim($user[12])."',claim_settled_date='".$user[13] . "',balance_sum_insured='".trim($user[14])."',hospital_name='".trim($user[15])."',city='".trim($user[16])."',notes='".trim($user[17])."' WHERE id = " . $data1['0']['msh_claims']['id']);
			}
			$i++;
		}
		
		//TPA data
		$i = 0;
		foreach($data->sheets[2]['cells'] as $user){
			if($i == 0){
				$i = 1; continue;
			}
			$data1 = $this->Network->query("SELECT * from msh_tpa where tpa_id = '".trim($user[1]) . "'");
			if(empty($data1)){
				$this->Network->query("INSERT INTO msh_tpa (tpa_id,tpa_name,toll_free,emergency_no) VALUES ('".trim($user[1])."','".trim($user[2])."','".trim($user[3])."','".trim($user[4])."')");
			}
			else {
				$this->Network->query("UPDATE msh_tpa SET tpa_name='".addslashes(trim($user[2]))."',toll_free='".addslashes(trim($user[3]))."',emergency_no='".trim($user[4])."' WHERE id = " . $data1['0']['msh_tpa']['id']);
			}
			$i++;
		}
		
		//Insurer data
		$i = 0;
		foreach($data->sheets[3]['cells'] as $user){
			if($i == 0){
				$i = 1; continue;
			}
			$data1 = $this->Network->query("SELECT * from msh_insurer where insurer_id = '".trim($user[1]) . "'");
			if(empty($data1)){
				$this->Network->query("INSERT INTO msh_insurer (insurer_id,insurer_name,toll_free,emergency_no) VALUES ('".trim($user[1])."','".trim($user[2])."','".trim($user[3])."','".trim($user[4])."')");
			}
			else {
				$this->Network->query("UPDATE msh_insurer SET insurer_name='".addslashes(trim($user[2]))."',toll_free='".addslashes(trim($user[3]))."',emergency_no='".trim($user[4])."' WHERE id = " . $data1['0']['msh_insurer']['id']);
			}
			$i++;
		}
		
		//Policy
		$i = 0;
		foreach($data->sheets[4]['cells'] as $user){
			if($i == 0){
				$i = 1; continue;
			}
			$user[5] = date('Y-m-d',strtotime(trim($user[5])));
			$user[6] = date('Y-m-d',strtotime(trim($user[6])));
			if(!isset($user[11]))$user[11] = '';
			$data1 = $this->Network->query("SELECT * from msh_policy where policy_no = '".trim($user[1]) . "'");
			if(empty($data1)){
				$this->Network->query("INSERT INTO msh_policy (company_code,tpa_id,insurer_id,policy_no,policy_start,policy_end,sum_insured,maternity_limit,dependents,restrictions,notes,timestamp) VALUES ('".trim($user[2])."','".trim($user[3])."','".trim($user[4])."','".trim($user[1])."','".$user[5]."','".$user[6]."','".trim($user[7])."','".trim($user[8])."','".trim($user[9])."','".trim($user[10])."','".trim($user[11])."','".date('Y-m-d H:i:s')."')");
			}
			else {
				$this->Network->query("UPDATE msh_policy SET company_code='".trim($user[2])."',tpa_id='".addslashes(trim($user[3]))."',insurer_id='".trim($user[4])."',policy_start='".$user[5]."',policy_end='".$user[6]."',sum_insured='".trim($user[7])."',maternity_limit='".trim($user[8])."',dependents='".trim($user[9])."',restrictions='".trim($user[10])."',notes='".trim($user[11])."' WHERE id = " . $data1['0']['msh_policy']['id']);
			}
			$i++;
		}
		
		//Hospital
		$i = 0;
		$this->Network->query("TRUNCATE msh_hospital");
		foreach($data->sheets[5]['cells'] as $user){
			if($i == 0){
				$i = 1; continue;
			}
			if(!isset($user[3]))$user[3] = '';
			if(!isset($user[5]))$user[5] = '';
			if(!isset($user[7]))$user[7] = '';
			
			$this->Network->query("INSERT INTO msh_hospital (hospital_id,tpa_id,tpa_hospital_id,hospital_name,hospital_location,hospital_city,hospital_type,hospital_phone,hospital_address) VALUES ('".trim($user[3])."','".trim($user[1])."','".trim($user[2])."','".addslashes(trim($user[4]))."','".trim($user[5])."','".addslashes(trim($user[6]))."','".trim($user[7])."','".trim($user[8])."','".addslashes(trim($user[9]))."')");
			
			$i++;
		}
		$this->autoRender = false;
	}
}
?>