<?php
class PartnersController extends AppController {
	
	var $name = 'Partners';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler');
	var $uses = array('Retailer');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		//$this->Auth->allowedActions = array('switchProduct','contentPartnerSubscription','pushPartnerData');
	}
	
	function contentPartnerSubscription(){
		$product = $_REQUEST['product'];
		$productuser_id = $_REQUEST['prod_userid'];
		$user_id = $_REQUEST['user'];
		$mobile = $_REQUEST['mobile'];
		
		$prod_data = $this->Retailer->query("SELECT id,name FROM products WHERE parent_id = $product ORDER BY id asc");
		
		if(isset($_REQUEST['param']) && !empty($_REQUEST['param'])){
			$limit = $_REQUEST['param'] - 1;
			$prod_sub = $this->Retailer->query("SELECT id,name FROM products WHERE parent_id = $product ORDER BY id asc limit $limit,1");
			$prod_id = $this->General->updateProductSwitch($product,$user_id,$prod_sub['0']['products']['id'],'change');
		}
		else {
			$prod_id = $this->General->updateProductSwitch($product,$user_id,$prod_data['0']['products']['id'],'sub');
		}
		/*$message = $this->General->getLastMessageSent($prod_id);
		if(!empty($message)){
			$this->General->sendMessage(SMS_SENDER,array($mobile),strip_tags($message),'pac'); 
		}*/
		
		$i = 1;
		$msg = "";
		foreach($prod_data as $prod){
			$msg .= "$i - " . $prod['products']['name']."\n";
			if($prod_id == $prod['products']['id']) $prod_name = $prod['products']['name'];
			$i++;
		}
		$message = "You are subscribed to $prod_name\n";
		$message .= "Switch to any of below options anytime\n";
		$message .= $msg;
		$message .= "E.g. For English Movies send SMS: ACT TV 2 to 0".VIRTUAL_NUMBER;
		$message .= "\nSend SMS: OPT TV to 0".VIRTUAL_NUMBER . " to get above options";
		$this->General->sendMessage(SMS_SENDER,array($mobile),$message,'user');
		$this->autoRender = false;
	}
	
	function switchProduct(){
		$type = $_REQUEST['type'];
		$option = $_REQUEST['option'];
		$user_id = $_REQUEST['user_id'];
		$mobile = $_REQUEST['mobile'];		
		if(strtoupper($type) == 'TV'){
			$product_id = WOI_PRODUCT;
			$limit = $option - 1;
			$prod = $this->Retailer->query("SELECT id,name FROM products WHERE parent_id = $product_id LIMIT $limit,1");
			if(empty($prod)){
				$prod_data = $this->Retailer->query("SELECT id,name FROM products WHERE parent_id = $product_id ORDER BY id asc");
				$i = 1;
				foreach($prod_data as $prod){
					$msg .= "$i - " . $prod['products']['name']."\n";
					$i++;
				}
				$sms = "Sorry, You have sent a wrong option to us.\n".$msg."To switch to 2nd option, send SMS: ACT $type 2 to 0".VIRTUAL_NUMBER.				
"\nFor more help, Send HELP to 0".VIRTUAL_NUMBER.".";
				$mail['subject'] = "User Trying to Switch: Wrong Code Sent";
				$mail['body'] = "Message sent ACT $type $option";
			}
			else {
				$check = $this->Retailer->query("SELECT id FROM products_users WHERE product_id = $product_id AND user_id = $user_id AND active = 1");
				if(empty($check)){
					$sms = $this->General->createMessage("PRODUCT_NOT_SUBSCRIBED",array("0".VIRTUAL_NUMBER));
					$root = 'template';
					$mail['subject'] = "User Trying to Switch: Not Subscribed";
					$mail['body'] = "Message sent ACT $type $option";
				}
				else {
					$this->General->updateProductSwitch($product_id,$user_id,$prod['0']['products']['id'],'change');	
					$sms = $this->General->createMessage("PRODUCT_WOI_SWITCH",array($prod['0']['products']['name']));
					$root = 'template';	
					$mail['subject'] = "User Switched product";
					$mail['body'] = "Mobile: $mobile, Switched to " . $prod['0']['products']['name'];
				}
			}
				
		}
		else {
			$sms = $this->General->createMessage("INVALID_CODE",array("0".VIRTUAL_NUMBER));
			$mail['subject'] = "User Trying to Switch: Wrong Code Sent";
			$mail['body'] = "Mobile: $mobile_number. Message sent '$msg'\n";
		}
		
		$url = SERVER_BACKUP . 'users/sendMsgMails';
		$data = array();
		
		//$data['mail_subject'] = $mail['subject'];
		$data['mail_body'] = $mail['body'];
		$data['sms'] =  $sms;
		if(isset($root)) $data['root'] = $root;
		else $data['root'] =  'retail';
		$data['mobile'] = $mobile;
		$this->General->curl_post_async($url,$data);
		$this->autoRender = false;				
	}
	
	function sendReminder($product_id,$type){
		$query = "SELECT contact_emails,contact_mobiles,products.name FROM products_timings,products WHERE products.id = product_id AND product_id = $product_id";	
		$data = $this->Retailer->query($query);
		$mails = explode(",",$data['0']['products_timings']['contact_emails']);
		$mobiles = explode(",",$data['0']['products_timings']['contact_mobiles']);
		
		if($type == 0){
			$sub = "SMS Data of " . $data['0']['products']['name'] . " is not available";
			$body = $sub . ".<br/>Please submit it asap";
			$msg = $this->General->br2newline($body);
		}
		else if($type == 1){
			$sub = "SMS Data of " . $data['0']['products']['name'] . " is > 280 chars";
			$body = $sub . ".<br/>Please check & re-submit it";
			$msg = $this->General->br2newline($body);
		}
		else if($type == 2){
			$sub = "SMS Data of " . $data['0']['products']['name'] . " cannot be sent now";
			$body = $sub . ".<br/>Buffer time exhausted";
			$msg = $this->General->br2newline($body);
		}
		
		//$this->General->authenticatedMailToUsers($mails,$sub,$body,'SMSTadka');
		//$this->General->sendMessage(SMS_SENDER,$mobiles,$msg,'user');
	}
	
	function pushPartnerData($par1,$par2){
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			if(date('H:i') == '12:00'){
				//send message to users every 7 days
				$prod_data = $this->Retailer->query("SELECT id,name,parent_id FROM products WHERE parent_id is not null");
				
				foreach($prod_data as $prod){
					$prod_name = $prod['products']['name'];
					$message = "You are subscribed to $prod_name\n";
					$message .= "You can switch to any of below options anytime\n";
				
					$data = $this->Retailer->query("SELECT distinct mobile FROM products_users,products_users_switch,users WHERE products_users.product_id = ".$prod['products']['parent_id']." AND products_users_switch.product_id = " . $prod['products']['id'] . " AND products_users.user_id = products_users_switch.user_id AND products_users.product_id = products_users_switch.product_parent_id AND products_users.active = 1 AND products_users_switch.active = 1 AND users.id = products_users.user_id AND MOD(DATEDIFF('".date('Y-m-d')."',Date(start)),7) = 0");
					$mobiles = array();
					foreach($data as $dt){
						$mobiles[] = $dt['users']['mobile'];
					}
					$prods = $this->Retailer->query("SELECT id,name FROM products WHERE parent_id = " . $prod['products']['parent_id']);
					$i = 1;
					foreach($prods as $pds){
						$message .= "$i - " . $pds['products']['name']."\n";
						$i++;
					}
					$message .= "For 2nd option, send SMS: ACT TV 2 to 0".VIRTUAL_NUMBER;
					$this->General->sendMessage(SMS_SENDER,$mobiles,$message,'user');
				}
			}
			
			if(date('H:i') == '02:00'){
				$this->Retailer->query("UPDATE products_timings SET sent_flag = 0, cron_time = time");
				$this->releaseLock();
			}
			else if(date('H:i') >= '08:00'){
				$query = "SELECT id,product_id,slot,time,cron_time FROM products_timings WHERE sent_flag = 0 AND SUBTIME(time,buffer_time) <= '" . date('H:i:s') . "'";	
				$data = $this->Retailer->query($query);
				foreach($data as $dt){
					$check = $this->Retailer->query("SELECT partners_data.*,products.parent_id FROM partners_data,products WHERE products.id = partners_data.product_id AND product_id = " . $dt['products_timings']['product_id'] . " AND slot = " . $dt['products_timings']['slot'] . " AND date = '".date('Y-m-d')."' AND sent_flag = 0 order by id desc");
						
					if($dt['products_timings']['cron_time'] > date('H:i:s')){//reminder
						if(empty($check)){
							$this->sendReminder($dt['products_timings']['product_id'],0);
						}
						else if(strlen($check['0']['partners_data']['content']) > 305){// or data is greater than 305 chars
							$this->sendReminder($dt['products_timings']['product_id'],1);
						}
					}
					else {
						if($dt['products_timings']['cron_time'] > date('H:i:s',strtotime($dt['products_timings']['time'] . " + 3 hours"))){
							// cannot send now
							$this->sendReminder($dt['products_timings']['product_id'],2);
							$this->Retailer->query("UPDATE products_timings SET sent_flag = 1 WHERE id = " . $dt['products_timings']['id']);
						}
						else if(empty($check)){
							// give a reminder again
							$this->sendReminder($dt['products_timings']['product_id'],0);
							$this->Retailer->query("UPDATE products_timings SET cron_time = ADDTIME(cron_time, '00:30:00') WHERE id = " . $dt['products_timings']['id']);
						}
						else if(strlen($check['0']['partners_data']['content']) > 305){// or data is greater than 305 chars
							$this->sendReminder($dt['products_timings']['product_id'],1);
							$this->Retailer->query("UPDATE products_timings SET cron_time = ADDTIME(cron_time, '00:30:00') WHERE id = " . $dt['products_timings']['id']);
						}
						else {
							//push data
							$data1 = $this->Retailer->query("SELECT distinct mobile FROM products_users,products_users_switch,users WHERE products_users.product_id = ".$check['0']['products']['parent_id']." AND products_users_switch.product_id = " . $check['0']['partners_data']['product_id'] . " AND products_users.user_id = products_users_switch.user_id AND products_users.product_id = products_users_switch.product_parent_id AND products_users.active = 1 AND products_users_switch.active = 1 AND users.id = products_users.user_id");
							$mobiles = array();
							
							foreach($data1 as $dt1){
								$mobiles[] = $dt1['users']['mobile'];
							}
							$this->General->sendMessage(SMS_SENDER,$mobiles,$check['0']['partners_data']['content'],'pac');
							
							$data2 = $this->Retailer->query("SELECT distinct mobile FROM products_users,users WHERE products_users.product_id = " . $check['0']['partners_data']['product_id'] . " AND products_users.active = 1 AND users.id = products_users.user_id");
							$mobiles = array();
							
							foreach($data2 as $dt2){
								$mobiles[] = $dt2['users']['mobile'];
							}
							$this->General->sendMessage(SMS_SENDER,$mobiles,$check['0']['partners_data']['content'],'pac');
							
							$this->Retailer->query("UPDATE products_timings SET sent_flag = 1 WHERE id = " . $dt['products_timings']['id']);
							$this->Retailer->query("UPDATE partners_data SET sent_flag = 1,modified='".date('Y-m-d H:i:s')."' WHERE id = " . $check['0']['partners_data']['id']);
						}
					}
				}
			}
			
			$this->releaseLock();
		}
		$this->autoRender = false;
	}
	
	function playwinMisscall($mobile){
		$mobile = substr($mobile,-10);
		$msg = "Register on www.rummyplaywin.com. You will receive a FREE entry to WIN Rs.1 Crore in the World Rummy Tournament!!!";
		$check = $this->Retailer->query("SELECT sender FROM misscall_logs WHERE sender ='$mobile' AND misscall_number='02261512275'");
		
		if(empty($check)){
			$this->General->makeOptIn247SMS($mobile,2);
		}
		
		$this->Retailer->query("INSERT INTO misscall_logs (misscall_number,sender,timestamp,sms_sent) VALUES ('02261512275','$mobile','".date('Y-m-d H:i:s')."','".addslashes($msg)."')");
							
		$this->General->sendMessageVia247SMS('',$mobile,$msg,'playwin');
		$this->autoRender = false;
	}
	
	function delivery(){
		$this->General->sendMails("DELIVERY TEST", json_encode($_REQUEST),array('ashish@mindsarray.com'));
		$this->autoRender = false;
	}
		
}