<?php
class AppsController extends AppController {

	var $name = 'Apps';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler');
	var $uses = array('SMSApp','User','Metadata');
	/*ODI & T20
		//1st inning 
		Aus elected to bat
		Aus: 220/3 (50 Ovs)
		Sachin: 99
		Zaheer: 10
		
		//2nd inning
		 Aus elected to bat
		Aus: 220/3 (50 Ovs)
		Ind: 189/8 (43 Ovs)
		Need 31 from 42balls
		Sachin: 99
		Zaheer: 10
		
		//win
		Ind won 
		Aus: 220/3 (50 Ovs)
		Ind: 221/8 (43 Ovs)
		
		Test
		//1st inning
		Aus elected to bat
		Aus: 520/9 (43 ovs)
		Sachin: 99
		Zaheer: 10
		
		or
		
		Aus elected to bat
		Aus: 520/9 (90 ovs)
		Ind: 320(43 Ovs)
		Sachin: 99
		Zaheer: 10
		
		//2nd inning
		 Aus elected to bat
		Aus: 520/9 & 160/5 (43ovs)
		Ind: 320 (43 Ovs)
		Sachin: 99
		Zaheer: 10
		
		*/
	function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('*');
		$this->Auth->allowedActions = array('test','cronScore','getMisscall','curlCall','scorePause','clearScore','updateScrPlus','updateMatch','updateScore','setMatchStatus','createMatch','getLiveScore','playwinDemo','marshAPI','marshAPIDemo','marshInteract','marshInteract1','DBbackup','view','showApp','getApps','euroDemo','euroLogin');
	}

	function test(){
		
			$matches = $this->SMSApp->query("select * from cricketMatches where status = '0' order by id desc");
			foreach($matches as $m){
				$scr = stripslashes(trim($m['cricketMatches']['score']));
				$scrPls = stripslashes(trim($m['cricketMatches']['scorePlus']));
				if($scr != '' || $scrPls != ''){
					$scr = json_decode($scr,true);
					$ret = $m['cricketMatches']['matchTitle'];
					
					if($scr['0']['rn'] != '')
					$ret .= "\n".$scr['0']['tm']." ".$scr['0']['rn']."/".$scr['0']['wt']." (".$scr['0']['ov']." ovs)\nBatsmen:\n".$scr['0']['b1']."\n".$scr['0']['b2'];
					
					if($scr['1']['rn'] != '')
					$ret .= "\nLast inning: ".$scr['1']['tm']." ".$scr['1']['rn']."/".$scr['1']['wt'];
					
					if($scr['1']['ov'] != '')
					$ret .= " (".$scr['1']['ov']." ovs)";

					$ret .= " ".$scrPls;
				}else{
					$ret = "Content is not available. Please try after some time";		
				}
				echo $ret;
			}
			
			$this->autoRender = false;
	}
	
	function cronScore(){
		$matches = $this->SMSApp->query("select * from cricketMatches where status = '0' order by id desc limit 1");
		foreach($matches as $m){
			$scr = stripslashes(trim($m['cricketMatches']['score']));
			$scrPls = stripslashes(trim($m['cricketMatches']['scorePlus']));
			if($scr != '' || $scrPls != ''){
				$scr = json_decode($scr,true);
				$ret = $m['cricketMatches']['matchTitle'];
				
				if($scr['0']['rn'] != '')
				$ret .= "\n".$scr['0']['tm']." ".$scr['0']['rn']."/".$scr['0']['wt']." (".$scr['0']['ov']." ovs)\nBatsmen:\n".$scr['0']['b1']."\n".$scr['0']['b2'];
				
				if($scr['1']['rn'] != '')
				$ret .= "\nLast inning: ".$scr['1']['tm']." ".$scr['1']['rn']."/".$scr['1']['wt'];
				
				if($scr['1']['ov'] != '')
				$ret .= " (".$scr['1']['ov']." ovs)";

				$ret .= " ".$scrPls;
			}
		}
		
		if(!empty($ret)){
			$lastScore = $matches['0']['cricketMatches']['lastScore'];
			if($ret != $lastScore){
				$this->SMSApp->query("update cricketMatches SET lastScore = '".addslashes($ret)."' WHERE id = " . $matches['0']['cricketMatches']['id']);
				
				$users_prods = $this->SMSApp->query("SELECT users.mobile FROM products_users,users WHERE products_users.product_id in (3,13) AND products_users.active = 1 AND products_users.user_id = users.id");
				$users = array();
				foreach($users_prods as $user){
					$users[] = $user['users']['mobile'];
				}
				$users_packs = $this->SMSApp->query("SELECT users.mobile FROM packages_users,users WHERE packages_users.package_id in (20,149) AND packages_users.active = 1 AND packages_users.user_id = users.id");
				foreach($users_packs as $user){
					$users[] = $user['users']['mobile'];
				}
				array_unique($users);
				
				$ret = "*Cricket Score*\n" . $ret;
				
				$this->General->sendMessage('',$users,$ret,'cricket');
			}
		}
		
		$this->autoRender = false;
	}
	
	function clearScore(){
		$id = $_REQUEST['id'];
		$this->User->query("UPDATE cricketmatches SET score='' WHERE id=".$id);	
		$this->autoRender = false;			
	}
	
	function updateScrPlus(){
		$scrPlus = urldecode($_REQUEST['text']);
		$id = $_REQUEST['id'];
		$this->User->query("UPDATE cricketmatches SET scorePlus='".addslashes($scrPlus)."' WHERE id=".$id);
		$this->autoRender = false;		
	}
	
	function setMatchStatus($id){
		$statusQry = $this->SMSApp->query("select status from cricketMatches where id =".$id);
		$status = 0;
		if($statusQry['0']['cricketMatches']['status'] == '0')
		$status = 1;
		
		$this->SMSApp->query("update cricketMatches set status = '".$status."' where id =".$id);
		
		header("Location: /apps/createMatch");
		$this->autoRender = false;	
	}
	
	function scorePause($id){
		$statusQry = $this->SMSApp->query("select paused from cricketMatches where id =".$id);
		$status = 0;
		if($statusQry['0']['cricketMatches']['paused'] == '0')
		$status = 1;
		
		$this->SMSApp->query("update cricketMatches set paused = '".$status."' where id =".$id);
		
		header("Location: /apps/createMatch");
		$this->autoRender = false;	
	}
	
	function createMatch(){
		$msg = '';
		$matchTitle = '';
		$matchUrl = '';
		$matchBakUrl = '';
		$matchType = '';
		$matchDate = '';
		$matchHr = '';
		$matchMin = '';
		$matchDur = '';
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/index.php');
		if(isset($_REQUEST['matchTitle'])){
			$matchTitle = trim($_REQUEST['matchTitle']);
			$matchUrl = trim($_REQUEST['matchUrl']);
			$matchBakUrl = trim($_REQUEST['matchBakUrl']);
			$matchType = trim($_REQUEST['matchType']);			
			$matchDate = trim($_REQUEST['matchDate']);
			$matchHr = trim($_REQUEST['matchHr']);
			$matchMin = trim($_REQUEST['matchMin']);
			$matchDur = trim($_REQUEST['matchDur']);
			
			$properDateArr = explode("-",$matchDate);
			$matchTiming = implode("-",array_reverse($properDateArr))." ".$matchHr.":".$matchMin.":00";
			
			if(trim($matchTitle) == ''){
				$msg .= 'Enter Match title</br>';
			}
			
			if(trim($matchUrl) == ''){
				$msg .= 'Enter Match URL</br>';
			}else if (!preg_match("#^http(s)?://[a-z0-9-_.]+\.[a-z]{2,4}#i",$matchUrl)) {
				$msg .= "Enter valid Match URL</br>";
			} 
			
			if(trim($matchBakUrl) != ''){
				if (!preg_match("#^http(s)?://[a-z0-9-_.]+\.[a-z]{2,4}#i",$matchBakUrl))
				$msg .= 'Enter valid back-up Match URL</br>';
			}
			
			if(trim($matchDate) == ''){
				$msg .= 'Enter Match Date</br>';
			}
			
			if($msg == ''){
				$this->SMSApp->query("INSERT INTO cricketMatches (matchTitle,matchUrl,matchBakUrl,type,timing,duration,created,status) VALUES ('".addslashes($matchTitle)."','".addslashes($matchUrl)."','".addslashes($matchBakUrl)."','".$matchType."','".$matchTiming."','".$matchDur."','".date('Y-m-d H:i:s')."','0')");
				header("Location: /apps/createMatch");
			}
		}
		
		$matches = $this->SMSApp->query("select * from cricketMatches order by id desc limit 10");
		$this->set('matches',$matches);
		$this->set('message',$msg);
		$this->set('matchTitle',$matchTitle);
		$this->set('matchUrl',$matchUrl);
		$this->set('matchBakUrl',$matchBakUrl);
		$this->set('matchType',$matchType);
		$this->set('matchDate',$matchDate);
		$this->set('matchHr',$matchHr);
		$this->set('matchMin',$matchMin);
		$this->set('matchDur',$matchDur);
		$this->layout = 'admin';
	}
	
	function updateMatch($id){
		$msg = '';
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/index.php');
		if(isset($_REQUEST['matchTitle'])){
			$matchTitle = trim($_REQUEST['matchTitle']);
			$matchUrl = trim($_REQUEST['matchUrl']);
			$matchBakUrl = trim($_REQUEST['matchBakUrl']);
			$matchType = trim($_REQUEST['matchType']);			
			$matchDate = trim($_REQUEST['matchDate']);
			$matchHr = trim($_REQUEST['matchHr']);
			$matchMin = trim($_REQUEST['matchMin']);
			$matchDur = trim($_REQUEST['matchDur']);
			
			$properDateArr = explode("-",$matchDate);
			$matchTiming = implode("-",array_reverse($properDateArr))." ".$matchHr.":".$matchMin.":00";
			
			if(trim($matchTitle) == ''){
				$msg .= 'Enter Match title</br>';
			}
			
			if(trim($matchUrl) == ''){
				$msg .= 'Enter Match URL</br>';
			}else if (!preg_match("#^http(s)?://[a-z0-9-_.]+\.[a-z]{2,4}#i",$matchUrl)) {
				$msg .= "Enter valid Match URL</br>";
			} 
			
			if(trim($matchBakUrl) != ''){
				if (!preg_match("#^http(s)?://[a-z0-9-_.]+\.[a-z]{2,4}#i",$matchBakUrl))
				$msg .= 'Enter valid back-up Match URL</br>';
			}
			
			if(trim($matchDate) == ''){
				$msg .= 'Enter Match Date</br>';
			}
			
			if($msg == ''){
				$this->SMSApp->query("update cricketMatches set matchTitle='".addslashes($matchTitle)."', matchUrl = '".addslashes($matchUrl)."',matchBakUrl = '".addslashes($matchBakUrl)."',type='".$matchType."',timing='".$matchTiming."',duration='".$matchDur."',created='".date('Y-m-d H:i:s')."' where id=".$id);
				header("Location: /apps/createMatch");
			}
		}else{
			$matches = $this->SMSApp->query("select * from cricketMatches where id =".$id);		
			$matchTitle = stripslashes($matches['0']['cricketMatches']['matchTitle']);
			$matchUrl = stripslashes($matches['0']['cricketMatches']['matchUrl']);
			$matchBakUrl = stripslashes($matches['0']['cricketMatches']['matchBakUrl']);
			$matchType = $matches['0']['cricketMatches']['type'];
			
			$matchTiming = explode(" ",$matches['0']['cricketMatches']['timing']);
			$tmp = explode("-",$matchTiming[0]);
			$matchDate = implode("-",array_reverse($tmp));
			$tmp = explode(":",$matchTiming[1]);
			$matchHr = $tmp[0];
			$matchMin = $tmp[1];
			$matchDur = $matches['0']['cricketMatches']['duration'];
		}
		
		$this->set('matches',$matches);
		$this->set('message',$msg);
		$this->set('matchTitle',$matchTitle);
		$this->set('matchUrl',$matchUrl);
		$this->set('matchBakUrl',$matchBakUrl);
		$this->set('matchType',$matchType);
		$this->set('matchDate',$matchDate);
		$this->set('matchHr',$matchHr);
		$this->set('matchMin',$matchMin);
		$this->set('matchDur',$matchDur);
		$this->set('id',$id);
		$this->layout = 'admin';
		$this->render('/apps/create_match');	
	}
	
	function updateScore(){
		$matches = $this->SMSApp->query("select * from cricketMatches where status = '0' or status = 2");
		foreach($matches as $m){
			$time = date("Y-m-d H:i:s");
			//echo strtotime($time)."timing:".strtotime($m['cricketMatches']['timing'])."after duration: ".(strtotime($m['cricketMatches']['timing'])+($m['cricketMatches']['duration']*60*60)); exit;
			if((strtotime($time) > strtotime($m['cricketMatches']['timing'])) && (strtotime($time) < (strtotime($m['cricketMatches']['timing'])+($m['cricketMatches']['duration']*60*60)))){
				if($m['cricketMatches']['status'] == 2){
					$this->SMSApp->query("update cricketMatches set status = 0 where id =".$m['cricketMatches']['id']);
				}
				if(trim($m['cricketMatches']['paused']) == '0'){//score update paused				
					$score = $this->getLiveScore(stripslashes(trim($m['cricketMatches']['matchUrl'])),stripslashes(trim($m['cricketMatches']['matchBakUrl'])));
					if(trim($score[0]['ov']) != ''){ // if u get runs only the // fix: cricinfo changes its dom structure
						$ovr = $score[0]['ov'];
						if(strstr($ovr, '.6') != '')
						$ovr = floor($ovr + 1);					
						$testStr = $score[0]['rn'].$score[0]['wt'].$ovr;
						
						$score[0]['ov'] = $ovr;
						$scoreJ = json_encode($score);						
						if($testStr != trim($m['cricketMatches']['testStr'])){							
							//as second inning starts
							if($ovr < trim($m['cricketMatches']['currOvr']) && isset($score[1]['rn']) && $score[1]['rn'] != '')
							$this->SMSApp->query("update cricketMatches set currOvr='".$ovr."' where id =".$m['cricketMatches']['id']);
							
							if($ovr >= trim($m['cricketMatches']['currOvr']))
							$this->SMSApp->query("update cricketMatches set score = '".addslashes($scoreJ)."',scorePlus='',testStr='".$testStr."',currOvr='".$ovr."' where id =".$m['cricketMatches']['id']);
						}
					}
				}
			}
			else if(strtotime($time) > (strtotime($m['cricketMatches']['timing'])+($m['cricketMatches']['duration']*60*60))){
				$this->SMSApp->query("update cricketMatches set status = 1 where id =".$m['cricketMatches']['id']);
			}
			else {
				$this->SMSApp->query("update cricketMatches set status = 2 where id =".$m['cricketMatches']['id']);
			}
		}
		$this->autoRender = false;	
	}
	
	function curlCall($url){		
			$user_agent = '$user_agent = "Opera/9.60 (J2ME/MIDP; Opera Mini/4.2.14912/812; U; ru) Presto/2.4.15";';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			$url=curl_exec ($ch);
			return $url;		
	}
	
	function getLiveScore($url,$bakUrl){
		if(strstr($url, 'cricbuzz') != ''){
			$arr = $this->getLiveScoreCB($url);
			if(trim($arr[0]['ov']) == ''){
				$arr = $this->getLiveScoreCI($bakUrl);
			}
		}
		if(strstr($url, 'espncricinfo') != ''){
			
			$arr = $this->getLiveScoreCI($url);		
		
			if(trim($arr[0]['ov']) == ''){
				$arr = $this->getLiveScoreCB($bakUrl);
			}
		}
		
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
		return $arr;
		$this->autoRender = false;
	}
	
	function getLiveScoreCB($url){
		echo "CRICBUZZ";

	 	$page = $this->curlCall($url);
		$domDocument = new DOMDocument();
		$domDocument->loadHTML($page);
		$xpath = new DOMXPath($domDocument);
		
		$arr = array();			
		//get current and past inning score
		$nodelist = $xpath->query("//div[contains(concat(' ',normalize-space(@class),' '),'news_row')]");
		foreach($nodelist as $k=>$v){
			$nodearr[$k] = $v;						
		}
		
		for($i=0;$i<2;$i++){
			$tmp = $nodearr[$i]->nodeValue;
			$str = explode(":",$tmp);
			$arr[$i]['tm'] = trim($str[0]);			
			$str1 = explode("(",trim($str[1]));
			$runWktStr = explode("/",$str1[0]);
			$arr[$i]['rn'] = trim($runWktStr[0]);
			$arr[$i]['wt'] = trim($runWktStr[1]);
			$ovrsStr = explode(" ",$str1[1]);
			$arr[$i]['ov'] = trim($ovrsStr[0]);				
		}			
		
		if($arr[1]['rn'] == '')unset($arr[1]);
		
		//get batsmen
		$nodelist = $xpath->query("//table[contains(concat(' ',normalize-space(@class),' '),'commMiniscorePanel')]/tr/td");			
		foreach($nodelist as $k=>$v){
			$nodearr[$k] = $v;						
		}
		
		//$arr[0]['b1'] = ereg_replace("[^A-Za-z0-9]", "", $nodearr[0]->nodeValue)." ".$nodearr[1]->nodeValue."(".$nodearr[2]->nodeValue.")";
		$arr[0]['b1'] = ereg_replace("[^A-Za-z0-9 ]", "", $nodearr[3]->nodeValue)." ".$nodearr[4]->nodeValue."(".$nodearr[5]->nodeValue.")";
		if(trim($nodearr[7]->nodeValue) != ''){
			$b2 = ereg_replace("[^A-Za-z0-9 ]", "", $nodearr[6]->nodeValue)." ".$nodearr[7]->nodeValue."(".$nodearr[8]->nodeValue.")";
		}else{
			$b2 = '';
		}
		
		$arr[0]['b2'] = $b2;
		//$this->autoRender = false;		
		return $arr;
	}
		
	function getLiveScoreCI($url){
		echo "CRICINFO</br>";
		$url = "http://m.espncricinfo.com/s/5643/MatchCenter?matchId=548310&live=true";
	 	$page = $this->curlCall($url);
		$domDocument = new DOMDocument();
		$domDocument->loadHTML($page);
		$xpath = new DOMXPath($domDocument);
		
		$arr = array();
		
		//get current and past inning score
		$nodelist = $xpath->query("//table[4]/tr/td");
		$l = 0;
		foreach($nodelist as $nodes){
			if(strstr($nodes->nodeValue, '/') != '')
				$l++;					
		}
		$bat = 6;
		if($l == '0'){//cricinfi changes its html
			$nodelist = $xpath->query("//table[5]/tr/td");
			$l = 0;
			foreach($nodelist as $nodes){
				if(strstr($nodes->nodeValue, '/') != '')
					$l++;					
			}
			$bat = 7;			
		}
		
		foreach($nodelist as $k=>$v){
			$nodearr[$k] = $v;						
		}

		/*for($i=0;$i<=5;$i++){
			echo $i.trim($nodearr[$i]->nodeValue)."</br>";
		}
		//exit;*/
		$j = 0;
		for($i=$l;$i>0;$i--){
			$tmp = ereg_replace("[^A-Za-z0-9.()/ ]", "", trim($nodearr[$i]->nodeValue));
			$str = explode(" ",trim($tmp));
			$arr[$j]['tm'] = trim($str[0]);	
			$runWktStr = explode("/",$str[1]);
			$arr[$j]['rn'] = trim($runWktStr[0]);
			$arr[$j]['wt'] = trim($runWktStr[1]);
			$ovrsStr = explode("ov",$str[2]);
			$arr[$j]['ov'] = trim(str_replace("(","",$ovrsStr[0]));
			$j++;
		}			

		//cricinfo structure change
	
		//get batsmen
		$nodelist = $xpath->query("//table[".$bat."]/tr/td");
		foreach($nodelist as $k=>$v){
			$nodearr[$k] = $v;						
		}
		
		//get batsmen
		$arr[0]['b1'] = ereg_replace("[^A-Za-z0-9 ]", "", trim($nodearr[6]->nodeValue))." ".trim($nodearr[7]->nodeValue)."(".trim($nodearr[8]->nodeValue).")";
		if(trim($nodearr[13]->nodeValue) != ''){
			$b2 = ereg_replace("[^A-Za-z0-9 ]", "", trim($nodearr[12]->nodeValue))." ".trim($nodearr[13]->nodeValue)."(".trim($nodearr[14]->nodeValue).")";
		}else{
			$b2 = '';
		}
		$arr[0]['b2'] = $b2;
		return $arr;
		//$this->autoRender = false;	
	}
		
	function view($url){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('url' => $url)));
		if(empty($data) || !$url){
			$this->redirect(array('controller' => 'users','action' => 'er404','404'));
		}
		
		if($this->Session->read('Auth.User')) {
			$this->Session->write('param','app');
			$this->Session->write('app',$data['SMSApp']['controller_name']);
			$this->Session->write('about',1);
			$this->redirect(array('controller' => 'users','action' => 'view'));
		}
		else {
			$this->set('data',$data);
			$this->set('controller',$data['SMSApp']['controller_name']);
			$this->set('name',$data['SMSApp']['name']);
			$metaData = $this->Metadata->find('first', array('fields' => array('Metadata.title', 'Metadata.desc'),'conditions' => array('Metadata.controller' => 'apps','Metadata.action' => 'view','Metadata.ref_id' => $data['SMSApp']['id'])));
			$this->set('pageTitle',$metaData['Metadata']['title']);
			$this->set('pageDesc',$metaData['Metadata']['desc']);
			$this->render('view');
			//$this->render('/'.$data['SMSApp']['controller_name'].'s/view');
		}
	}
	
	function showApp(){
		$controller = $_REQUEST['controller'];
		
		if($this->Session->read('Auth.User')) {
			$this->Session->write('param','app');
			$this->Session->write('app',$controller);
			$this->Session->write('about',0);
			echo "0";
			$this->autoRender = false;
		}
		else {
			$this->render('/elements/login_def','ajax');
			$pckPars = array();
			$pckPars['par'] = 'app';
			$pckPars['controller'] = $controller;
			$pckPars['about'] = 0;
			$this->Session->write('usrParams',$pckPars);
		}
	}

	function getApps(){
		$this->SMSApp->recursive = -1;
		$apps = $this->SMSApp->find('all',array('joins' => array(
				array(
							'table' => 'apps_users',
							'type' => 'inner',
							'conditions'=> array('SMSApp.id = apps_users.app_id and apps_users.user_id = '. $this->Session->read('Auth.User.id'))
				)
		)));
		$this->set('apps', $apps);
	}

	function allApps(){
		$this->render('/elements/all_apps','ajax');
	}

	function myApps(){
		/*$this->SMSApp->recursive = -1;
		$apps = $this->SMSApp->find('all',array('joins' => array(
				array(
							'table' => 'apps_users',
							'type' => 'inner',
							'conditions'=> array('SMSApp.id = apps_users.app_id and apps_users.user_id = '. $this->Session->read('Auth.User.id'))
				)
		)));
		$this->set('apps', $apps); */
		$this->render('/elements/my_apps','ajax');
	}
	
	function defineAlerts(){
		$this->render('define_alerts','ajax');
	}
	
	function faqs(){
		$this->render('faqs','ajax');
	}

	function DBbackup($par1,$par2){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			set_time_limit(0);
			ini_set("memory_limit","-1");
			$host = DB_HOST;
			$user = DB_USER;
			$pass = DB_PASS;
			$name = DB_DB;
			$tables = '*';
			$link = mysql_connect($host,$user,$pass);
			mysql_select_db($name,$link);
			
			//get all of the tables
			if($tables == '*')
			{
				$tables = array();
				$result = mysql_query('SHOW TABLES');
				while($row = mysql_fetch_row($result))
				{
					$tables[] = $row[0];
				}
			}
			else
			{
				$tables = is_array($tables) ? $tables : explode(',',$tables);
			}
			
			//cycle through
			foreach($tables as $table)
			{
				$result = mysql_query('SELECT * FROM '.$table);
				$num_fields = mysql_num_fields($result);
				
				$return.= 'DROP TABLE IF EXISTS '.$table.';';
				$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
				$return.= "\n\n".$row2[1].";\n\n";
				
				for ($i = 0; $i < $num_fields; $i++) 
				{
					while($row = mysql_fetch_row($result))
					{
						$return.= 'INSERT INTO '.$table.' VALUES(';
						for($j=0; $j<$num_fields; $j++) 
						{
							$row[$j] = addslashes($row[$j]);
							$row[$j] = ereg_replace("\n","\\n",$row[$j]);
							if (isset($row[$j]))
							{ 
								$return .= '"'.$row[$j].'"' ; 
							} else {
								$return .= '""'; 
							}
							
							if ($j <($num_fields-1)) { $return.= ','; }
						}
						$return.= ");\n";
					}
				}
				$return.="\n\n\n";
			}
			
			//save file
			//$handle = fopen('/tmp/db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
			$handle = fopen('/mnt/db/db-backup.sql','w+');
			fwrite($handle,$return);
			fclose($handle);
			exec('tar -zcf /mnt/db/db-backup.tar.gz /mnt/db/db-backup.sql');
			$this->releaseLock();
		}
		$this->autoRender = false;
	}
	
	function euroDemo(){
		
		$this->layout = 'mobile';
	}
	
	function euroLogin(){
		
		$this->layout = 'mobile';
	}
	
	function marshAPI($mobile,$session_id=null,$type=null){
		$link = $this->General->getBitlyUrl(SITE_NAME."apps/marshAPIDemo/".$mobile);
		$message = "Welcome!
Click below link to access the Employee Benefit System
$link

Thank you";
		$this->General->sendMessage('',array($mobile),$message,'priority',3);
		$this->autoRender = false;
	}
	
	function playwinDemo($mobile){
		$message = "Ten2Win Result:
1a, 2b, 3a, 4c, 5b, 6a, 7c, 8a, 9a, 10c";
		$this->General->sendMessage('',array($mobile),$message,'priority',3);
		$this->autoRender = false;
	}
	
	function marshAPIDemo($mobile,$session_id=null,$type=null){
		//$mobile = $this->objMd5->decrypt(urldecode($mobile),encKey);
		//$session_id = $this->objMd5->decrypt(urldecode($session_id),encKey);
		$this->set('session_id',$session_id);
		$this->set('mobile',$mobile);
		
		/*$url = SITE_NAME."apis/ussd.php?msisdn=$mobile&tid=$session_id&msg=0";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
		$message = curl_exec ($ch);
		curl_close ($ch);*/
		
		//$message = str_replace("\n","<br/>",$message);
		
		//$this->set('message',$message);
		
		if($type == 1)$this->render('marsh_api1');
		else $this->render('marsh_api','marsh');
	}
	
	function marshInteract($page,$num,$mobile){
		
		if($page == 1){
			if($num == 1){
				$message = "Sum Insured: 500000<br/>
Maternity cover: 50000<br/>
Eligible Dependants: Spouse, Children, and Parents (optional)<br/>
Balance Sum Insured:453479<script>$('goback').show();</script>
";
			}
			else if($num == 2){
				$html = "";
				$message = "<h4>Initimate Claim</h4>1.Employee: Mohit Agarwal<br/>
2.Wife: Meghna Agarwal<br/>
3.Son: Shrey Agarwal<br/>
4.Son: Shaurya Agarwal<br/>
5.Father: Satendra Kumar Agarwal<br/><br/>
<form>
Select Claimant: <input style='margin-left:105px;' type = 'text'><br/>
Enter reason for hospitalization: <input type = 'text'><br/>
Enter Date of hospitalization: <input style='margin-left:20px;' type = 'text'><br/>
Enter Hospital name: <input style='margin-left:75px;' type = 'text'><br/>
Enter Hospital - City: <input style='margin-left:75px;' type = 'text'><br/>
Enter Estimated claim amount: <input style='margin-left:10px;' type = 'text'><br/>
<input type='button' value='Submit' onclick='$(\"innerDiv\").innerHTML=\"Your Claim has been Submitted Successfully\";$(\"goback\").show();'>
</form>
";
			}
			else if($num == 3){
				$message = "<h4>Track claim status</h4>No Claims found !!<br/><br/><script>$('goback').show();</script>";
			}
			else if($num == 4){
				$message = "<h4>Find Network Hospitals</h4>
<form>
Enter Area Pincode: <input style='margin-left:10px;' type = 'text'><br/>
Enter City: <input style='margin-left:70px;' type = 'text'><br/>
Enter Area: <input style='margin-left:65px;' type = 'text'><br/>
<input type='button' value='Submit' onclick='$(\"innerDiv\").innerHTML=\"1)Agrawal Hospital<br/>Kilachand Centre Station Road, Patan 384265 Dist Patan Gujarat<br/>Ahmedabad<br/><br/>2)Mehta Hospital<br/>Kilachand Centre Station Road, Patan 384265 Dist Patan Gujarat<br/>Ahmedabad<br/>An email is sent with all the search results\";$(\"goback\").show();'>
</form>";
			}
			else if($num == 5){
				$message = "<h4>Imp contact details</h4>TPA Toll Free Number: 1-800-425-4033<br/>
Emergency Number: 9223719195<br/><script>$('goback').show();</script>";
			}
			else if($num == 6){
				$message = "<h4>Claims related docs</h4>An email containing Pre-authorization and Claim Form is sent to your email id<script>$('goback').show();</script>";
			}
		}
		echo $message;
		$this->autoRender = false;
	}
	
	function marshInteract1(){
		$mobile = $this->data['mobile'];
		$session_id = $this->data['session'];
		$msg = $this->data['inp'];
		
		$url = SITE_NAME."apis/ussd.php?msisdn=$mobile&tid=$session_id&msg=".urlencode($msg);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$message = curl_exec ($ch);
		curl_close ($ch);
		
		$message = str_replace("\n","<br/>",$message);
		echo $message;
		$this->autoRender = false;
	}
	
	function getMisscall($mobile,$misscall){
		$mobile = substr($mobile,-10);
		$this->General->logMisscalls($mobile,$misscall);
		$data = $this->SMSApp->query("SELECT * FROM products_packages WHERE misscall = '$misscall'");
		if(!empty($data)){
			$this->productPush($data['0']['products_packages']['product_id'],$data['0']['products_packages']['package_id'],$data['0']['products_packages']['limit'],$mobile);	
		}
		else {
			$data = $this->SMSApp->query("SELECT group_concat(package_id) as packs,sum(products_packages.limit) as limits,products.id FROM products,products_packages WHERE products.misscall = '$misscall' AND products.id=products_packages.product_id AND products_packages.toshow = 1 group by product_id");
		
			if(!empty($data)){
				$this->productPush($data['0']['products']['id'],explode(",",$data['0']['0']['packs']),$data['0']['0']['limits'],$mobile);	
			}
		}
		$this->autoRender = false;
	}
	
	function productPush($product_id,$package_id,$limit,$mobile){
		$data = $this->General->getUserDataFromMobile($mobile);
		if(empty($data)){//not a smstadka user yet
			$user = $this->General->registerUser($mobile,RETAILER_REG);
			$user_id = $user['User']['id'];
		}
		else {
			$user_id = $data['id'];
		}
		$prodData = $this->SMSApp->query("SELECT SUM(active) as actives, SUM(count) as counts,SUM(trial) as trials FROM products_users WHERE product_id = $product_id AND user_id = $user_id");
		$active = false;
		$trial = false;
		if($prodData['0']['0']['counts'] == 0){//trial case
			$this->SMSApp->query("INSERT INTO products_users (product_id,user_id,active,count,trial,start,end) VALUES ($product_id,$user_id,1,1,1,'".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s',strtotime('+ 1 day'))."')");
			$active = true;
			$trial = true;
		}
		else if($prodData['0']['0']['actives'] == 1){
			$active = true;
			if($prodData['0']['0']['trials'] == $prodData['0']['0']['counts']){//right now in trial period
				$trial = true;
			}
		}
		
		$msgs = $this->SMSApp->query("SELECT product_expiry, trial_expiry,products.name FROM products_copies,products WHERE product_id = $product_id AND products.id = product_id");
			
		if($active){
			$tot = $prodData['0']['0']['counts'] - $prodData['0']['0']['trials'];
			if(is_array($package_id)){
				shuffle($package_id);
				foreach($package_id as $pack){
					$lt = $this->SMSApp->query("SELECT products_packages.limit,packages.name,products_packages.misscall FROM products_packages,packages WHERE product_id = $product_id AND package_id = $pack AND packages.id = package_id");
					$message = $this->getMessage($pack,$user_id,$lt['0']['products_packages']['limit']*$tot,$trial,1);
					if(!empty($message)){
						$extra_msg = "\nYou can get only " . $lt['0']['packages']['name'] . " by giving misscall to " . $lt['0']['products_packages']['misscall'];
						
						$message = str_replace("__MORE__",$extra_msg,$message);
						break;
					}
				}
			}
			else $message = $this->getMessage($package_id,$user_id,$limit*$tot,$trial,0);
			
			if(empty($message)){//Exhausted
				if($trial){
					$message = $msgs['0']['products_copies']['trial_expiry'];
				}
				else {
					if(is_array($package_id)){
						$message = "Dear User\nYour max limit exhausted for this product";
						$message .= "\nTo get more, buy one more SMSTadka ".$msgs['0']['products']['name']." card. To locate a nearby shop, send SMS: SHOP <pincode> to 0".VIRTUAL_NUMBER;
						$this->General->mailToAdmins('Misscall Packs: User limit Exhausted', "Mobile: $mobile, Product: ".$msgs['0']['products']['name']);
					}
					else {
						$message = "Dear User\nYour max limit exhausted for this number";
					
						$packs = $this->SMSApp->query("SELECT products_packages.misscall,packages.name FROM products_packages,packages WHERE product_id = $product_id AND package_id != $package_id AND packages.id = package_id");
						if(!empty($packs)){
							$message .= "\nTry below content also";
							foreach($packs as $pack){
								$message .= "\n".$pack['packages']['name'] . " - " . $pack['products_packages']['misscall'];
							}
						}
						$pack = $this->SMSApp->query("SELECT packages.name FROM packages WHERE packages.id = $package_id");
						
						$message .= "\nTo get more ".$pack['packages']['name'].", buy one more SMSTadka ".$msgs['0']['products']['name']." card. To locate a nearby shop, send SMS: SHOP <pincode> to 0".VIRTUAL_NUMBER;
						$this->General->mailToAdmins('Misscall Packs: User limit Exhausted', "Mobile: $mobile, Package: ".$pack['packages']['name']);
					}
				}
			}
		}
		else {//Deactivated
			$message = $msgs['0']['products_copies']['product_expiry'];
		}
		
		$this->General->sendMessage('',array($mobile),$message,'retail');
	}

	function getMessage($package_id,$user_id,$limit,$trial,$prod){
		$data = $this->SMSApp->query("SELECT count,ids FROM misscall_user_packages WHERE package_id = $package_id AND user_id = $user_id");
		$update = true;
		if(empty($data)){
			$this->SMSApp->query("INSERT INTO misscall_user_packages (package_id,user_id) VALUES ($package_id,$user_id)");
			$data['0']['misscall_user_packages']['count'] = 0;
			$data['0']['misscall_user_packages']['ids'] = 0;
		}
		if($trial){
			$limit = 2;
			if($prod == 1)$limit = 1;
		}
		
		if($limit > 0 && $limit != null && $data['0']['misscall_user_packages']['count'] >= $limit){
			return null;
		}
		
		if($package_id == 20){//live score
			$matches = $this->SMSApp->query("select * from cricketMatches where status = '0' order by id desc");
			if(!empty($matches)){
				foreach($matches as $m){
					$scr = stripslashes(trim($m['cricketMatches']['score']));
					$scrPls = stripslashes(trim($m['cricketMatches']['scorePlus']));
					if($scr != '' || $scrPls != ''){
						$scr = json_decode($scr,true);
						$ret = $m['cricketMatches']['matchTitle'];
						
						if($scr['0']['rn'] != '')
						$ret .= "\n".$scr['0']['tm']." ".$scr['0']['rn']."/".$scr['0']['wt']." (".$scr['0']['ov']." ovs)\nBatsmen:\n".$scr['0']['b1']."\n".$scr['0']['b2'];
						
						if($scr['1']['rn'] != '')
						$ret .= "\nLast inning: ".$scr['1']['tm']." ".$scr['1']['rn']."/".$scr['1']['wt'];
						
						if($scr['1']['ov'] != '')
						$ret .= " (".$scr['1']['ov']." ovs)";
	
						$ret .= "\n".$scrPls;
					}else{
						$ret = "Content is not available. Please try after some time";		
					}
				}
			}
			else {
				$ret = "There is no match going on right now. Please try after some time";
			}
		}
		else {
			$ids = $data['0']['misscall_user_packages']['ids'];
			if(empty($ids))$ids = 0;
			$table = $this->SMSApp->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $package_id AND categories_packages.category_id = data_tables.category_id");
			$table_name = $table['0']['data_tables']['table_name'];
			if($table_name == 'data_funs')$table_name = 'messages';

			$contentData = $this->SMSApp->query("SELECT content.id,content.cyclicData FROM misscall_refined,$table_name as content WHERE content.id = misscall_refined.message_id AND misscall_refined.package_id = $package_id AND misscall_refined.message_id NOT IN ($ids) ORDER BY RAND() LIMIT 0,1");
			if(empty($contentData)){
				$ret = "Content is not available right now. Please try after some time";
				$this->General->mailToAdmins('Misscall Packs: Content not there in database',"UserId: $user_id, Package_id: $package_id<br/>Message sent to user: $ret");
				$update = false;
			}
			else {
				$new_id = $contentData['0']['content']['id'];
				$ret = $contentData['0']['content']['cyclicData'];
				if($ids == 0)$ids = $new_id;
				else $ids = $ids . ",". $new_id;
				if($prod == 1 && !$trial){
					if(count(explode(",",$ids))%10 == 1){
						$ret .= "\n__MORE__";
					}
				}
			}
		}
		if($update)
			$this->SMSApp->query("UPDATE misscall_user_packages SET ids='$ids',count=count+1 WHERE package_id=$package_id AND user_id=$user_id");
		return $ret;
	}	
	
	
}
?>