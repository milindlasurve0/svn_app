<?php 
/**
 * This is a component to send email from CakePHP using PHPMailer
 * @link http://bakery.cakephp.org/articles/view/94
 * @see http://bakery.cakephp.org/articles/view/94
 */

class EmailComponent
{
  /**
   * Send email using SMTP Auth by default.
   */
    var $from         = null;
    var $fromName     = null;//FROM_NAME;
    var $smtpUserName = null;  // SMTP username
    var $smtpPassword = null; // SMTP password
    var $smtpHostNames= "smtp.gmail.com:587";  // specify main and backup server
    var $text_body = null;
    var $html_body = null;
    var $to = null;
    var $toName = null;
    var $subject = null;
    var $cc = null;
    var $bcc = null;
    var $template = 'email/default';
    var $attachments = null;
    var $params = null;

    var $controller;

    function startup( &$controller ) {
      $this->controller = &$controller;
    }

    function bodyText() {
    /** This is the body in plain text for non-HTML mail clients
     */
      ob_start();
      $temp_layout = $this->controller->layout;
      $this->controller->layout = '';  // Turn off the layout wrapping
      $mail = $this->controller->render($this->template . '_text'); 
      ob_get_clean();
      $this->controller->layout = $temp_layout; // Turn on layout wrapping again
      return $mail;
    }

    function bodyHTML() {
    /** This is HTML body text for HTML-enabled mail clients
     */
      ob_start();
      $temp_layout = $this->controller->layout;
      $this->controller->layout = 'mailer';  //  HTML wrapper for my html email in /app/views/layouts
      if(!empty($this->params)){
      	 $this->controller->set('params',$this->params);
      }
      $mail = $this->controller->render($this->template . '_html');
      ob_get_clean();
      $this->controller->layout = $temp_layout; // Turn on layout wrapping again
      return $mail;
    }

    function attach($filename, $asfile = '') {
      if (empty($this->attachments)) {
        $this->attachments = array();
        $this->attachments[0]['filename'] = $filename;
        $this->attachments[0]['asfile'] = $asfile;
      } else {
        $count = count($this->attachments);
        $this->attachments[$count+1]['filename'] = $filename;
        $this->attachments[$count+1]['asfile'] = $asfile;
      }
    }


    function send()
    {
    //App::import('Vendor', 'phpmailer/class.phpmailer.php');
    require_once('../vendors/phpmailer/class.phpmailer.php');
  
    $mail = new PHPMailer();
    $mail->IsSMTP();            // set mailer to use SMTP
    $mail->SMTPAuth = true;     // turn on SMTP authentication
    $mail->SMTPSecure = 'tls';
    $mail->Host   = $this->smtpHostNames;
    $mail->Username = $this->smtpUserName;
    $mail->Password = $this->smtpPassword;

    $mail->From     = $this->from;
    $mail->FromName = $this->fromName;
    foreach($this->to as $to){
   	 	$mail->AddAddress($to);
    	//$mail->AddBCC($to);
    }
    $mail->AddReplyTo($this->from, $this->fromName );

    $mail->CharSet  = 'UTF-8';
    $mail->WordWrap = 50;  // set word wrap to 50 characters
	if (!empty($this->attachments)) {
      foreach ($this->attachments as $attachment) {
        if (empty($attachment['asfile'])) {
          $mail->AddAttachment($attachment['filename']);
        } else {
          $mail->AddAttachment($attachment['filename'], $attachment['asfile']);
        }
      }
    }
	
    $mail->IsHTML(true);  // set email format to HTML
	
    $mail->Subject = $this->subject;
    
    if(empty($this->html_body))$mail->Body = $this->bodyHTML();
    else $mail->Body = $this->html_body;
    
    if(empty($this->html_text))$mail->AltBody = $this->bodyText();
    else $mail->AltBody = $this->html_text;
    
    $result = $mail->Send();
   
    if($result == false ) $result = $mail->ErrorInfo;
	return $result;
    }
}
?>