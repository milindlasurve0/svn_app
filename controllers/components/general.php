<?php

class GeneralComponent extends Object {
	var $components = array('Auth','Shop');


	function redis_connect(){
		try {
			App::import('Vendor', 'Predis',array('file'=>'Autoloader.php'));
			Predis\Autoloader::register();
			$this->redis = new Predis\Client(array(
                   'host' => REDIS_HOST,
                   'port' => REDIS_PORT 
			));
//                   'password' => REDIS_PASSWORD,

		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->redis = false;
		}
		return $this->redis;
	}

	function dumpLog($loggername,$loggerfilename){
		try {
			App::import('Vendor', 'logger/main/php',array('file'=>'Logger.php'));
			$this->logger = Logger::getLogger($loggername);
			Logger::configure(array(
	            'rootLogger' => array(
	                'appenders' => array('default'),
			),
	            'appenders' => array(
	                'default' => array(
	                    'class' => 'LoggerAppenderFile',
	                    'layout' => array(
	                        'class' => 'LoggerLayoutPattern'
	                        ),
	                    'params' => array(
	                        'file' => '/var/log/apps/'.$loggerfilename.'_'.date('Ymd').'.log',
	                        'append' => true
	                        )
	                        )
	                        )
	                        ));
		}
		catch (Exception $e) {
			echo "Couldn't connected to Redis";
			echo $e->getMessage();
			$this->logger = false;
		}
		return $this->logger;
	}


	function enCrypt($data = null) {
		if ($data != null) {
			// Make an encryption resource using a cipher
			$td = mcrypt_module_open('cast-256', '', 'ecb', '');
			// Create and encryption vector based on the $td size and random
			$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
			// Initialize the module using the resource, my key and the string vector
			mcrypt_generic_init($td, encKey, $iv);
			// Encrypt the data using the $td resource
			$encrypted_data = mcrypt_generic($td, $data);
			// Encode in base64 for DB storage
			$encoded = base64_encode($encrypted_data);
			// Make sure the encryption modules get un-loaded
			if (!mcrypt_generic_deinit($td) || !mcrypt_module_close($td)) {
				$encoded = false;
			}
		} else {
			$encoded = false;
		}
		return $encoded;
	}
	/**
	 * This function will de-crypt the string that is passed to it
	 *
	 * @param String $data The string to be encrypted.
	 * @return String Returns the encrypted string or false
	 */
	function deCrypt($data = null) {
		if ($data != null) {
			// The reverse of encrypt.  See that function for details
			$data = (string) base64_decode(trim($data));
			$td = mcrypt_module_open('cast-256', '', 'ecb', '');
			$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
			mcrypt_generic_init($td, encKey, $iv);
			$data = (string) trim(mdecrypt_generic($td, $data));
			// Make sure the encryption modules get un-loaded
			if (!mcrypt_generic_deinit($td) || !mcrypt_module_close($td)) {
				$data = false;
			}
		} else {
			$data = false;
		}
		return $data;
	}

	function makeUrl($text){
		$text = preg_replace('/[^a-zA-Z0-9 -]/s', '', $text);
		$text = str_replace('  ', ' ', $text);
		$text = str_replace(' ','-',strtolower($text));
		return $text;
	}

	function makeCamelcase($str)
	{
		$str = trim($str);
		$str = ucwords(strtolower($str));

		return $str;
	}
	function dateFormat($date){
		return date('jS M, Y',strtotime($date));
	}

	function dateTimeFormat($date){
		return date('jS M, Y g:i A',strtotime($date));
	}

	function nameToUrl($name) {
		return $this->makeUrl($name);
	}

	function urlToName($url){
		$name = str_replace('-',' ',$url);
		return $this->makeCamelcase($name);
	}

	function generatePassword($characters,$mobile=null){
		$code = '';

		if($mobile == null){
			$possible = '0123456789';
			$i = 0;
			while ($i < $characters) {
				$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
				$i++;
			}
		}
		else {
			$code =  substr($mobile, -2, 1) .  substr($mobile, -4, 1) . substr($mobile, -6, 1) . substr($mobile, -8, 1);
		}

		if(SENDFLAG == '0')
		$code = "1234";

		return $code;
	}

	function sendPassword($mobile,$password,$flag,$missCall=null){
		if($flag == '1') {
			$alias = "SUBSCRIBE_PASSWORD";
			$vars[] = $password;
		}
		else if($flag == '0'){
			$alias = "FORGOT_PASSWORD";
			$vars[] = $password;
		}
		$message = $this->createMessage($alias,$vars);
		if(SENDFLAG == '1' && $missCall == null) {
			$this->sendMessage('',$mobile,$message,'template');
		}
		else if($missCall != null){
			return $message;
		}
	}


	function balanceUpdate($price,$type,$userId=null){
		if($userId == null){
			$userId = $_SESSION['Auth']['User']['id'];
		}
		$bal = $this->getBalance($userId);
		$userObj = ClassRegistry::init('User');

		if($type == 'subtract'){
			$userObj->query("UPDATE users set balance = balance - $price where id = $userId");
			$balance = $bal - $price;
		}
		else if($type == 'add'){
			$userObj->query("UPDATE users set balance = balance + $price where id = $userId");
			$balance = $bal + $price;
		}

		return $balance;
	}

	function getBalance($userId){
		$userObj = ClassRegistry::init('User');
		$userObj->recursive = -1;
		$bal = $userObj->findById($userId);
		return $bal['User']['balance'];
	}

	function checkIfUserExists($mobile){
		$userObj = ClassRegistry::init('User');

		$count = $userObj->find('count',array('conditions' => array('User.mobile' => $mobile)));

		if($count > 0) return true;
		return false;
	}

	function getCouponIfExists($number){
		$number = strtoupper($number);
		if(in_array($number,explode(",",TRIAL_CODES))){
			$prodObj = ClassRegistry::init('Product');
			$prodObj->recursive = -1;
			$find = $prodObj->find('first',array('fields' => array('Product.id'),'conditions' => array('Product.code' => $number)));

			$ret['product_id'] = $find['Product']['id'];
			$ret['trial_flag'] = 1;
			return $ret;
		}
		else {

			$couponObj = ClassRegistry::init('Coupon');
			//$couponObj->recursive = -1;
			$data = $couponObj->find('first',array('fields' => array('Coupon.id','Coupon.serialNumber','Coupon.product_id','Coupon.dry_flag','RetailersCoupon.user_id','RetailersCoupon.retailer_id'),'conditions' => array('Coupon.code' => $number),
				'joins' => array(
			array(
								'table' => 'retailers_coupons',
								'alias' => 'RetailersCoupon',
								'type' => 'left',
								'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')
			))
			)
			);

			if(!empty($data)){
				$ret['product_id'] = $data['Coupon']['product_id'];
				$ret['coupon_id'] = $data['Coupon']['id'];
				$ret['serial_number'] = $data['Coupon']['serialNumber'];
				$ret['user_id'] = $data['RetailersCoupon']['user_id'];
				$ret['retailer_id'] = $data['RetailersCoupon']['retailer_id'];
				$ret['trial_flag'] = 0;
				$ret['dry_flag'] = $data['Coupon']['dry_flag'];

				return $ret;
			}
			else return null;
		}
	}

	function retailerCouponSold($coupon_id,$user_id){
		$retcouponObj = ClassRegistry::init('RetailersCoupon');
		$data = $retcouponObj->find('first',array('conditions' => array('RetailersCoupon.coupon_id' => $coupon_id)));
		if(empty($data)){
			$this->data['RetailersCoupon']['coupon_id'] = $coupon_id;
			$this->data['RetailersCoupon']['user_id'] = $user_id;
			$this->data['RetailersCoupon']['created'] = date('Y-m-d H:i:s');
			$this->data['RetailersCoupon']['modified'] = date('Y-m-d H:i:s');

			$retcouponObj->create();
			if($retcouponObj->save($this->data)){

			}
		}
		else {
			$retcouponObj->query("UPDATE retailers_coupons set user_id = $user_id,modified = '".date('Y-m-d H:i:s')."' where coupon_id = $coupon_id");
		}
	}

	function getNewCycle($package_id,$user_id){
		$productUserObj = ClassRegistry::init('ProductsUser');
		$productUserObj->recursive = -1;

		$query = "SELECT GROUP_CONCAT(distinct cyclic_user_packages.cycle) as cycles FROM cyclic_user_packages WHERE package_id = $package_id AND user_id = $user_id AND cycle != 0";
		$data = $productUserObj->query($query);
		$cycles = explode(",",$data['0']['0']['cycles']);

		$query = "SELECT GROUP_CONCAT(distinct cyclic_refined.cycle) AS cycles FROM cyclic_refined WHERE cyclic_refined.package_id = $package_id AND cycle != 0";
		$data = $productUserObj->query($query);
		$allcycles = explode(",",$data['0']['0']['cycles']);
		$result = array_diff($allcycles, $cycles);
		if(empty($result)){
			$max = max($allcycles);

			$query = "SELECT MAX(cyclic_refined.cycle) AS cycle FROM cyclic_refined WHERE cyclic_refined.package_id = $package_id";
			$data2 = $productUserObj->query($query);
			$maxCycle = $data2['0']['0']['cycle'];

			if($max == $maxCycle){//recycle
				$cycle = $cycles['0'];
				//$this->mailToUsers("Recycled user $user_id, package $package_id","Product cycle $cycle",array('ashish@mindsarray.com','chirutha@mindsarray.com'));
				$productUserObj->query("DELETE from cyclic_user_packages WHERE package_id = $package_id AND user_id = $user_id");

				return $cycle;
			}
			else {
				return ($max + 1);
			}
		}
		else {
			shuffle($result);
			return $result['0'];
		}
	}

	function userCyclicPackageInsert($product_id,$package_id,$user_id,$trial=null){
		$productUserObj = ClassRegistry::init('ProductsUser');
		$productUserObj->recursive = -1;
		$packages = array();

		if($product_id == null){
			$query = "SELECT * FROM packages WHERE id = $package_id AND cycle_ids IS NOT NULL";
			$data = $productUserObj->query($query);
			if(!empty($data))
			$packages[] = $package_id;
		}
		else {
			$query = "SELECT GROUP_CONCAT(products_packages.package_id) as packages FROM products_packages WHERE product_id = $product_id AND products_packages.cycle_ids IS NOT NULL";
			$data = $productUserObj->query($query);
			$packages = explode(",",$data['0']['0']['packages']);
			array_unique($packages);
		}

		foreach($packages as $package){
			if(empty($package))continue;
			$query1 = "SELECT * FROM products_users INNER JOIN products_packages ON (products_packages.product_id = products_users.product_id) WHERE products_packages.package_id = $package AND products_users.user_id = $user_id AND products_users.active = 1";
			$data1 = $productUserObj->query($query1);

			$query2 = "SELECT * FROM packages_users WHERE packages_users.package_id = $package AND packages_users.user_id = $user_id AND packages_users.active = 1";
			$data2 = $productUserObj->query($query2);
			$insert_flag = false;
			if(empty($data1) && empty($data2)){//no product or package is active
				//check for trial
				if(!empty($trial) && $trial == 1){
					$cycle = 0;
				}
				else {
					$cycle = $this->getNewCycle($package,$user_id);
				}
				$insert_flag = true;
			}
			else if((empty($trial) || $trial == 0) && ($data1['0']['products_users']['trial'] == 1 || $data2['0']['packages_users']['trial_flag'] == 1)){
				$cycle = $this->getNewCycle($package,$user_id);
				$insert_flag = true;
			}

			/*$query = "SELECT MAX(cyclic_refined.cycle) AS cycle FROM cyclic_refined WHERE cyclic_refined.package_id = $package";
			 $data2 = $productUserObj->query($query);
			 $maxCycle = $data2['0']['0']['cycle'];
			 	
			 if($cycle > $maxCycle){
				$query1 = "SELECT frequency FROM cyclic_packages WHERE package_id = $package";
				$freq = $productUserObj->query($query1);
				$freq = $freq['0']['cyclic_packages']['frequency'];
					
				$productUserObj->query("INSERT INTO cyclic_packages (package_id,cycle,msg_num,frequency) VALUES ($package,$cycle,1,$freq)");
				$this->mailToUsers("Offline Product Cycle of user $user_id, package ". $package ." is $cycle","Offline Product Cycle is $cycle",array('dinesh@mindsarray.com','ashish@mindsarray.com','chirutha@mindsarray.com'));
				}*/
			if($insert_flag)
			$productUserObj->query("INSERT INTO cyclic_user_packages (package_id,user_id,cycle,count) VALUES (".$package.",$user_id,$cycle,0)");
		}
	}

	function getProductInfo($product_id){
		$prodObj = ClassRegistry::init('Product');
		$prodObj->recursive = -1;
		return $prodObj->find('first',array('conditions' => array('id' => $product_id)));
	}

	function draftUpdate($type,$desc,$refId,$amount,$friendIds,$mobile=null, $user_id=null){

		if($user_id == null)
		$user_id = $_SESSION['Auth']['User']['id'];

		$draftObj = ClassRegistry::init('Draft');

		$find = $draftObj->find('first',array('fields' => array('Draft.id'),
			'conditions' => array('Draft.user_id' => $user_id, 'Draft.type' => $type, 'Draft.refid' => $refId, 'Draft.content' => $desc, 'Draft.friendIds' => "$friendIds",'Draft.mobile' => $mobile)));
		if(!empty($find)){
			$this->data['Draft']['id'] = $find['Draft']['id'];
			$this->data['Draft']['draftedOn']= date('Y-m-d H:i:s');
		}
		else {
			$this->data['Draft']['user_id']= $user_id;
			$this->data['Draft']['type']= $type;
			$this->data['Draft']['refid']= $refId;

			$this->data['Draft']['content']= $desc;
			$this->data['Draft']['friendIds']= "$friendIds";
			if(!empty($mobile))
			$this->data['Draft']['mobile']= $mobile;
			$this->data['Draft']['amount']= $amount;
			$this->data['Draft']['draftedOn']= date('Y-m-d H:i:s');
			$draftObj->create();
		}

		$draftObj->save($this->data);
	}

	function transactionUpdate($type,$amount,$pack_id,$msg_id,$user_id = null){

		if($user_id == null){
			$user_id = $_SESSION['Auth']['User']['id'];
		}

		$transObj = ClassRegistry::init('Transaction');

		$this->data['Transaction']['user_id'] = $user_id;
		$this->data['Transaction']['package_id'] =  $pack_id;
		$this->data['Transaction']['message_id'] =  $msg_id;
		$this->data['Transaction']['amount'] =  $amount;
		$this->data['Transaction']['type'] =  $type;
		$this->data['Transaction']['timestamp'] =  date('Y-m-d H:i:s');
		$transObj->create();
		$transObj->save($this->data);

		return $transObj->id;
	}

	function rechargeLogUpdate($id=null , $trans_id,$parent_trans_id,$amount,$user_id,$mob_dth_no,$type,$contact_no,$err_code,$description,$created=null,$rechargeBy=1){
		$rechargeLog = ClassRegistry::init('RechargeLog');

		if($user_id == null){
			$user_id = empty($_SESSION['Auth']['User']['id']) ? 0 : $_SESSION['Auth']['User']['id']  ;
		}

		if(!empty($id)){
			$this->data['RechargeLog']['id'] = $id;
		}else{
			$rechargeLog->create();
		}

		//$trans_id,$amount,$user_id,$mob_dth_no,$type,$contact_no,$err_code,$description,$created
		$this->data['RechargeLog']['trans_id'] = $trans_id;
		$this->data['RechargeLog']['parent_trans_id'] = $parent_trans_id;
		$this->data['RechargeLog']['amount'] =  $amount;
		$this->data['RechargeLog']['user_id'] =  $user_id;
		$this->data['RechargeLog']['mob_dth_no'] =  $mob_dth_no;
		$this->data['RechargeLog']['type'] =  $type;
		$this->data['RechargeLog']['contact_no'] =  $contact_no;
		$this->data['RechargeLog']['err_code'] =  $err_code;
		$this->data['RechargeLog']['description'] =  $description;
		$this->data['RechargeLog']['recharge_by'] =  $rechargeBy;
			
		if($created == null){
			$this->data['RechargeLog']['created'] =  date('Y-m-d H:i:s');
		}else{
			$this->data['RechargeLog']['created'] =  $created;
		}
		$this->data['RechargeLog']['modified'] =  date('Y-m-d H:i:s');


		$id = 0;

		try{
			$rechargeLog->save($this->data);
			$id = $rechargeLog->id;
		}catch(Exception $e){
			$id = 0;
		}
		return $id;
	}

	function smsCountUpdate($type,$count,$user_id = null){
		if($user_id == null)
		$user_id = $_SESSION['Auth']['User']['id'];

		$userObj = ClassRegistry::init('User');
		if($type=="free"){
			$userObj->query("UPDATE users SET smsfree_count = smsfree_count + $count WHERE id = $user_id");
		}
		else if($type=="fwd"){
			$userObj->query("UPDATE users SET smsfwd_count = smsfwd_count + $count WHERE id = $user_id");
		}
	}

	function appTransactionUpdate($type,$amount,$app_id,$ref_id,$user_id = null){

		if($user_id == null)
		$user_id = $_SESSION['Auth']['User']['id'];

		$transObj = ClassRegistry::init('AppsTransaction');

		$this->data['AppsTransaction']['user_id'] = $user_id;
		$this->data['AppsTransaction']['app_id'] =  $app_id;
		$this->data['AppsTransaction']['ref_id'] =  $ref_id;
		$this->data['AppsTransaction']['amount'] =  $amount;
		$this->data['AppsTransaction']['type'] =  $type;
		$this->data['AppsTransaction']['timestamp'] =  date('Y-m-d H:i:s');

		$transObj->create();
		$transObj->save($this->data);

		return $transObj->id;
	}

	function logUpdate($pack_id,$msg_id,$trans_id,$content,$mobile=null,$user_id=null){

		$logObj = ClassRegistry::init('log');
		if($user_id == null)
		$user_id = $_SESSION['Auth']['User']['id'];

		$this->data['Log']['user_id'] = $user_id;
		$this->data['Log']['package_id'] =  $pack_id;
		if($msg_id != null)
		$this->data['Log']['message_id'] =  $msg_id;
		$this->data['Log']['transaction_id'] =  $trans_id;
		$this->data['Log']['mobile'] =  $mobile;
		$this->data['Log']['content'] =  $content;
		$this->data['Log']['report'] =  '0';
		$this->data['Log']['timestamp'] =  date('Y-m-d H:i:s');

		$logObj->create();
		$logObj->save($this->data);
		return $logObj->id;
	}

	function appLogUpdate($app_id,$ref_id,$trans_id,$content,$mobile=null,$user_id=null){

		$logObj = ClassRegistry::init('AppsLog');
		if($user_id == null)
		$user_id = $_SESSION['Auth']['User']['id'];

		$this->data['AppsLog']['user_id'] = $user_id;
		$this->data['AppsLog']['app_id'] =  $app_id;
		$this->data['AppsLog']['ref_id'] =  $ref_id;
		$this->data['AppsLog']['transaction_id'] =  $trans_id;
		$this->data['AppsLog']['mobile'] =  $mobile;
		$this->data['AppsLog']['content'] =  $content;
		$this->data['AppsLog']['timestamp'] =  date('Y-m-d H:i:s');

		$logObj->create();
		$logObj->save($this->data);
		return $logObj->id;
	}

	function appUsersUpdate($app_id,$user_id=null){
		$appsUserObj = ClassRegistry::init('AppsUser');
		if($user_id == null)
		$user_id = $_SESSION['Auth']['User']['id'];

		$count = $appsUserObj->find('count',array('conditions' => array('user_id' => $user_id,'app_id' => $app_id)));

		if($count == 0){
			$this->data['AppsUser']['user_id'] = $user_id;
			$this->data['AppsUser']['app_id'] =  $app_id;
			$this->data['AppsUser']['active'] =  1;
			$this->data['AppsUser']['start'] =  date('Y-m-d H:i:s');

			$appsUserObj->create();
			$appsUserObj->save($this->data);
		}
	}

	function getAllChildPackages($parentCat){
		$catObj = ClassRegistry::init('Category');

		$arrPackData = $catObj->find('all',
		array('fields' => array( 'Package.*'),
				'joins' => array(
		array(
						'table' => 'categories_packages',
						'alias' => 'CategoryPackages',
						'type' => 'inner',
						'conditions'=> array(
							'AND' => array(
								'Category.id = CategoryPackages.category_id',
								'Category.parent = '.$parentCat,
								'Category.toshow = 1' 
								))
								),
								array(
						'table' => 'packages',
						'alias' => 'Package',
						'type' => 'inner',
						'conditions' => array( 
							'Package.id = CategoryPackages.package_id','Package.toshow = 1'
							)
							)
							),
				'group' => 'Package.id'
				)
				);

				return $arrPackData;
	}

	function getAllChildPackagesAdmin($parentCat){
		$bigArr = array(
			'32' => array('36' => 'Fun Double Dose', '38' => 'Occasional Wishes', '141' => 'RajaniKanth Jokes'),
			'34' => array('70' => 'Daily Market Summary', '71' => 'Business News', '72' => 'Market Alerts', '74' => 'World Market Summary', '144' => 'World Market Alerts', '148' => 'Top Gainers Top Loosers'),
			'3' => array('18' => 'Breaking News', '75' => 'Banking News', '76' => 'Info-tech News', '140' => 'Daily Top Stories'),
			'61' => array('1' => 'Leo Daily Forecast', '2' => 'Aquarius Daily Forecast', '3' => 'Gemini Daily Forecast', '4' => 'Virgo Daily Forecast', '5' => 'Taurus Daily Forecast', '7' => 'Libra Daily Forecast', '8' => 'Aries Daily Forecast', '9' => 'Scorpio Daily Forecast', '10' => 'Cancer Daily Forecast', '11' => 'Pisces Daily Forecast', '12' => 'Sagittarius Daily Forecast', '14' => 'Capricorn Daily Forecast', '161' => 'Gita Quotes'),
			'36' => array('22' => 'Bollywood Tadka', '23' => 'Movie Reviews', '26' => 'Katrina Kaif', '30' => 'Salmaan Khan'),
			'35' => array('19' => 'Cricket News', '20' => 'Cricket Live Score', '146' => 'Cricket Trivia', '149' => 'Cricket Live Score - India'),
			'38' => array()
		);


		return $bigArr[$parentCat];
	}

	function getAllPackages($catarr,$limit=null,$page=null){

		$catids = implode(",",$catarr);

		$catObj = ClassRegistry::init('Category');
		$str = "1000";
		if($limit != null){
			$str = ($page-1)*$limit . "," . $limit;
		}

		$arrPackData = $catObj->find('all',
		array('fields' => array( 'Package.*'),
				'joins' => array(
		array(
						'table' => 'categories_packages',
						'alias' => 'CategoryPackages',
						'type' => 'inner',
						'conditions'=> array(
							'AND' => array(
								'Category.id = CategoryPackages.category_id',
								'Category.id in ('.$catids.')',
								'Category.toshow = 1' 
								))
								),
								array(
						'table' => 'packages',
						'alias' => 'Package',
						'type' => 'inner',
						'conditions' => array( 
							'Package.id = CategoryPackages.package_id','Package.toshow = 1'
							)
							)
							),
				'group' => 'Package.id',
				'limit' => $str,
				'cache'	=> $catids."-".$limit."-".$page
							)
							);
							return $arrPackData;
	}

	function getCountPackages($catarr){
		$catids = implode(",",$catarr);

		$catObj = ClassRegistry::init('Category');

		$count = $catObj->find('count',
		array('fields' => array('count(distinct Package.id) as count'),
				'joins' => array(
		array(
						'table' => 'categories_packages',
						'alias' => 'CategoryPackages',
						'type' => 'inner',
						'conditions'=> array(
							'AND' => array(
								'Category.id = CategoryPackages.category_id',
								'Category.id in ('.$catids.')',
								'Category.toshow = 1'
								))
								),
								array(
						'table' => 'packages',
						'alias' => 'Package',
						'type' => 'inner',
						'conditions' => array( 
							'Package.id = CategoryPackages.package_id','Package.toshow = 1'
							)
							)
							),
				'group' => 'Category.id',
				'cache'	=> 'counts_'.$catids			
							)
							);

							return $count;
	}

	function getRecommendedPacks($user_id,$limit){
		$arr = RECOMMENDED;
		$pckObj = ClassRegistry::init('Package');
		$pckObj->recursive = -1;
		$packs = $pckObj->find('all',array('fields' => array('distinct Package.*','PackagesUser.package_id'),'conditions' => array('Package.id in('.$arr.')','Package.toshow' => 1,'package_id is null'),
		 	'joins' => array (
		array(
							'table' => 'packages_users',
							'alias' => 'PackagesUser',
							'type' => 'left',
							'conditions' => array('PackagesUser.package_id = Package.id','PackagesUser.user_id = '.$user_id)
		)
		),
		 	'order' => 'FIELD(Package.id,'.$arr.')',
		 	'limit' => $limit
		)
		);

		return $packs;
	}

	function getRecentMessages($packs,$limit,$flag=null){
		$logObj = ClassRegistry::init('log');
		$logObj->recursive = -1;
		if($flag == null){
			if($limit == 'no')
			{
				$logData = $logObj->find('all', array(
					'fields' => array('Log.content','Log.timestamp'),
					'conditions' => array('Log.package_id in ('.$packs.')'),
					'order' => 'Log.timestamp desc',
					'group' => 'Log.content,Date(Log.timestamp)'
					));
			}
			else
			{
				$logData = $logObj->find('all', array(
					'fields' => array('Log.content','Log.timestamp'),
					'conditions' => array('Log.package_id in ('.$packs.')'),
					'order' => 'Log.timestamp desc',
					'group' => 'Log.content,Date(Log.timestamp)',
					'limit' => $limit
				));
			}
		}
		else {
			//$package_cond = "1";
			if(!empty($packs)){
				$package_cond = "Log.package_id in ($packs)";
				$logData = $logObj->find('all', array(
				'fields' => array('Log.id','Log.content','Log.timestamp','Log.package_id','packages.name','packages.url'),
				'conditions' => array('Log.package_id is not null and Log.timestamp > "'.date('Y-m-d H:i:s', strtotime( ' - 2048 hours')).'"',"$package_cond"),
				'joins' => array(
				array(
							'table' => 'packages',
							'type' => 'inner',
							'conditions'=> array('Log.package_id = packages.id')
				)
				),
				'order' => 'Log.timestamp desc',
				'group' => 'Log.content',
				'limit' => $limit
				));
			}
			else {
				$package_cond = "Log.package_id not in (".DONT_SHOW_RECENT.")";
				$logData = $logObj->find('all', array(
				'fields' => array('Log.id','Log.content','Log.timestamp','Log.package_id','packages.name','packages.url'),
				'conditions' => array('Log.package_id is not null and Log.timestamp > "'.date('Y-m-d H:i:s', strtotime( ' - 2048 hours')).'"',"$package_cond"),
				'joins' => array(
				array(
							'table' => 'packages',
							'type' => 'inner',
							'conditions'=> array('Log.package_id = packages.id')
				)
				),
				'order' => 'Log.timestamp desc',
				'group' => 'Log.content',
				'limit' => $limit,
				'cache' => array('recentMsg', '+1 hours')
				));
			}
		}
		return $logData;
	}

	function getGroupId($name,$mobile=null){
		$groupObj = ClassRegistry::init('Group');
		$groupObj->recursive = -1;
		$groupId = $groupObj->find('first', array('fields' => array('Group.id'),'conditions' => array('Group.name' => $name)));

		$mobileNums = array('9892471157','9892609560','9819852204','9820595052','9004387418','9819032643');
		if($name != 'admin' && in_array($mobile,$mobileNums)){
			return $this->getGroupId('admin');
		}
		return $groupId['Group']['id'];
	}

	function getFrequency($freq){
		$str = '';
		if(empty($freq))
		$str = FREQUENCY_DEFAULT;
		else if($freq == 1)
		$str = 'Once a day';
		else if($freq == 2)
		$str = 'Twice a day';
		else if($freq == 4)
		$str = 'Four Times a day';
		return $str;
	}

	function getPrice($price){
		$str = '';
		if($price >= 1)
		$str = '<span><img class="rupee1" src="/img/rs.gif"></span>'.$price;
		else
		$str = 100*$price . ' paise';
		return $str;
	}

	function getCategoryLayout($cat_id){
		$catObj = ClassRegistry::init('Category');
		$catObj->recursive = -1;
		$layout = $catObj->find('first',array(
			'fields' => array('Category.layout'),
			'conditions' => array('Category.id' => $cat_id)
		));
		return $layout['Category']['layout'];
	}

	function countChars($message){
		return strlen($message);
	}

	function getSMSNums($charcount){
		//$charcount = $charcount - DEFAULT_MESSAGE_LENGTH + ADSPACE;
		$num = ceil($charcount/DEFAULT_MESSAGE_LENGTH);
		return $num;
	}

	function getCharge($num_messages){
		return (EACH_MESSAGE_COST/100)*$num_messages;
	}


	function getTotalMessageAmount($message,$num){
		$chars = $this->countChars($message);
		$num_messages = $this->getSMSNums($chars);
		$amount = $this->getCharge($num_messages)*$num;
		return $amount;
	}

	function getMessageCharge($id){
		$msgObj = ClassRegistry::init('Message');
		$msgObj->recursive = -1;
		$msgData = $msgObj->find('first', array('fields' => array('Message.charCount','Message.content'),'conditions' => array('Message.id' => $id)));

		if($msgData['Message']['charCount'] == null){
			$chars = $this->countChars($msgData['Message']['content']);
		}
		else {
			$chars = $msgData['Message']['charCount'];
		}

		$num_messages = $this->getSMSNums($chars);
		$amount = $this->getCharge($num_messages);

		return $amount;
	}

	function getDefaultRechargeMessage($type,$amount,$pck_id = null){
		$msg = "<div class='field'>";
		if($type == "message"){
			$msg .= "You don't have sufficient balance for sending this message. <br/>";
			$msg .= "Do you wish to Recharge your account?<br> If yes, Click 'Recharge' to proceed. (Minimum Balance required is <span><img class='rupee1' src='/img/rs.gif'/></span>".$amount.")</div> <a class='buttSprite leftFloat' href='/users/paynow'><img class='butRecharge' src='/img/spacer.gif'></a><br class='clearLeft' />";
		}
		else if($type == "package"){
			/*$msg .= "You don't have sufficient balance to subscribe this package.<br/><br/> ";
			 $msg .= "Click here for a Free trial <br/> <a class='buttSprite leftFloat' href='javascript:void(0);' onclick='subPackageTrial(\"".$pck_id."\",\"sub\",this);'><img class='butTrial' src='/img/spacer.gif'></a><br class='clearLeft' /> <br/>";
			 $msg .= "Or <br/><br/>For a complete 30 days subscription you need a minimum balance of <span><img class='rupee1' src='/img/rs.gif'/></span>".$amount.". Click here to Recharge your account</div> <a class='buttSprite leftFloat' href='/users/paynow'><img class='butRecharge' src='/img/spacer.gif'></a><br class='clearLeft' />";*/
			$msg .= "For subscription you need a minimum balance of <span><img class='rupee1' src='/img/rs.gif'/></span>".$amount.". Click here to Recharge your account</div> <a class='buttSprite leftFloat' href='/users/paynow'><img class='butRecharge' src='/img/spacer.gif'></a><br class='clearLeft' />";
		}
		else if($type == "app"){
			$msg .= "You don't have sufficient balance to create this alert.";
			$msg .= " Do you wish to Recharge your account?<br> If yes, Click 'Recharge' to proceed. (Minimum Balance required is <span><img class='rupee1' src='/img/rs.gif'/></span>".$amount.")</div> <a class='buttSprite leftFloat' href='/users/paynow'><img class='butRecharge' src='/img/spacer.gif'></a><br class='clearLeft' />";
		}
		return $msg;
	}

	function getPackageCharge($id){
		$pckObj = ClassRegistry::init('Package');
		$pckObj->recursive = -1;
		$pckData = $pckObj->find('first', array('fields' => array('Package.price'),'conditions' => array('Package.id' => $id)));
		return $pckData['Package']['price'];
	}


	function addAsynchronousCall($random,$controller,$action,$params){
		$userObj = ClassRegistry::init('User');
		$userObj->query("INSERT INTO asynchronous_calls (random_id,controller,action,params) VALUES ($random,'".$controller."','".$action."','".addslashes(json_encode($params))."')");
	}


	function registerUser($mobile_number,$reg_type,$group=null){
		$userObj = ClassRegistry::init('User');

		$this->data['User']['mobile'] = $mobile_number;
		$password = $this->generatePassword(4); //generate 4 character password
		$this->data['User']['password'] = $this->Auth->password($password); //encrypted password using hash salt

		$this->data['User']['balance'] = FREE_LOGIN_CREDIT;
		if($group == null)
		$this->data['User']['group_id'] = $this->getGroupId('member',$this->data['User']['mobile']);
		else
		$this->data['User']['group_id'] = $group;
		$this->data['User']['dob'] = '0000-00-00';
		$this->data['User']['gender'] = 0; //0 means male
		$this->data['User']['passflag'] = 0;
		$this->data['User']['login_count'] = 0;

		//$dnd = $this->checkDND($mobile_number);
		//$dnd_flag = $dnd['dnd'];
		$dnd_flag = 0;
		if($reg_type == ONLINE_REG)
		$this->data['User']['verify'] = 0;
		else if($reg_type == MISSCALL_REG)
		$this->data['User']['verify'] = -1;
		else if($reg_type == RETAILER_REG)
		$this->data['User']['verify'] = -2;
		else if($reg_type == REF_CODE_REG)
		$this->data['User']['verify'] = -3;
		else if($reg_type == ONLINE_RETAILER_REG){
			$this->data['User']['verify'] = -4;
			/*if($dnd_flag == 1){
				$this->data['User']['opt_flag'] = 0;
				$this->mailToUsers("Setting Opt-flag of User as NOT-OPTED", "Mobile: $mobile_number",array('ashish@mindsarray.com','chirutha@mindsarray.com','nausheen@mindsarray.com'));
				}*/
		}

		/*if($dnd_flag == 1 && $reg_type != FORCE_RETAILER_REG){
			$msg = "Dear User\nAs your number is in DND you cannot receive SMSes from SMSTadka.\nTo receive SMS from SMSTadka please send SMS: START SMSTDK to 09004190190
			For any further queries SMS: HELP to 09223178889";
			$this->sendMessage('',$mobile_number,$msg,'modem');//send message to users to start getting smses .. start
			}*/

		/*$det = $this->getMobileDetails($mobile_number);
		 if($det['3'] == "TI" || $det['3'] == "T" || $det['3'] == "UN" || $det['3'] == "RC" || $det['3'] == "RG"){
			$this->data['User']['tata_flag'] = 1;
			$this->mailToUsers("TATA/Uninor/Reliance Mobile Number", "Mobile Number: $mobile_number",array('chirutha@mindsarray.com','nausheen@mindsarray.com'));
			}*/
		$this->data['User']['dnd_flag'] = $dnd_flag;
		$this->data['User']['syspass'] = $password;
		$this->data['User']['created'] = date("Y-m-d H:i:s");
		$this->data['User']['modified'] = date("Y-m-d H:i:s");
		if(isset($dnd['preference']))
		$this->data['User']['ncpr_pref'] =  $dnd['preference'];
		//$this->data['User']['email'] = 'a@b.com';

		$userObj->create();
		if($userObj->save($this->data)) {
			//if($reg_type != RETAILER_REG)
			//$this->transactionUpdate(TRANS_ADMIN_FREE_CREDIT, FREE_LOGIN_CREDIT,null,null,$userObj->id);
			//$this->makeOptIn247SMS($mobile_number,1);
			$this->data['User']['id'] = $userObj->id;
			return $this->data;
		}
	}

	function isDND($mobile){
		if(true){
			//if(DND_FLAG){ //TRAI Changes
			/*$url = "http://www.24x7sms.com/checkdnd.aspx?mobile=$mobile";
			 $status_str = file_get_contents($url);
			 $status = trim(strtolower(strip_tags($status_str)));
			 if($status == 'active') return 1;
			 else if ($status == 'inactive') return 0;
			 else return -1;*/
			$ret = $this->checkDND($mobile);
			return $ret['dnd'];
		}
		else{
			return 0;
		}
	}

	function updateLogCC($user_id,$desc){
		$loggedInUser= $_SESSION['Auth']['User']['id'];
		$userObj = ClassRegistry::init('User');
		$userObj->query("insert into  logs_cc(user_id,description,parent_user_id) values ($user_id,'".addslashes($desc)."',$loggedInUser)");
	}

	function segregateMobileNumbers($mobiles){
		if(is_array($mobiles)) {
			$mobiles = implode(",",$mobiles);
		}
		$mobis = explode(",",$mobiles);
		$userObj = ClassRegistry::init('User');
		$data = $userObj->query("SELECT * from users where mobile in (".$mobiles.")");
		$users_dnd = array();
		$users_nondnd = array();
		$users = array();
		$users_opt = array();
		$users_others = array();
		foreach($data as $user){
			if($user['users']['dnd_flag'] == 0 && $user['users']['opt_flag'] != 0){
				$users_nondnd[] = $user['users']['mobile'];
			}
			else if($user['users']['opt_flag'] != 0){
				//$users_dnd[] = $user['users']['mobile'];
				if($user['users']['opt_flag'] == 1){
					$users_opt[] = $user['users']['mobile'];
				}
				else {
					$users_others[] = $user['users']['mobile'];
				}
			}

			$users[] = $user['users']['mobile'];
		}
		$array = array_diff($mobis, $users);
		$users_others = array_merge($users_others,$array);
		//$return['dnd'] = $users_dnd;
		$return['nondnd'] = $users_nondnd;
		$return['opt'] = $users_opt;
		$return['others'] =  $users_others;
		return $return;
	}

	function getTata_Numbers($mobiles){
		$arr_flag = false;
		if(is_array($mobiles)) {
			$mobiles = implode(",",$mobiles);
			$arr_flag = true;
		}
		$mobis = explode(",",$mobiles);
		$userObj = ClassRegistry::init('User');
		$data = $userObj->query("SELECT * from users where tata_flag = 1 AND mobile in (".$mobiles.")");
		$users = array();
		$email_users = array();
		foreach($data as $user){
			$users[] = $user['users']['mobile'];
		}
		$array = array_diff($mobis, $users);
		$return['other'] = $array;
		if($arr_flag) {
			$return['tata'] =  $users;
		}
		else {
			$return['tata'] = implode(",",$users);
		}
		return $return;
	}

	function checkTimeSlot($par = null){
		if(DND_FLAG){ //TRAI Changes
			if($par){
				if((intval($par) < (TIME_SLOT_START+30)) || (intval($par) > (TIME_SLOT_END))){
					return false;
				}else{
					return true;
				}
			}else{
				$current = date('Hi');
				if(intval($current) < TIME_SLOT_START || intval($current) > TIME_SLOT_END){
					return false;
				}else{
					return true;
				}
			}
		}
		else {
			return true;
		}
	}

	function addNonSentMessages($sender,$receivers,$message,$type,$app_name){
		$array_flag = 0;
		if(is_array($receivers)) {
			$receivers = implode(",",$receivers);
			$array_flag = 1;
		}
		if(!empty($receivers)){
			$userObj = ClassRegistry::init('User');
			$userObj->query("INSERT INTO log_notsent (sender,receivers,message,type,app_name,array_flag,timestamp) VALUES ('".$sender."','".$receivers."','".addslashes($message)."','".$type."','$app_name',$array_flag,'".date('Y-m-d H:i:s')."')");
		}
	}

	function sendMessageVia247SMS_Promotional($sender,$receivers,$message,$type=null,$switch=null){ //receivers is in comma seperated format

		if(!is_array($receivers)) {
			$receivers = explode(",",$receivers);
		}
		$message = trim($message);
		if(empty($receivers) || empty($message))return;
		preg_match('/[0-9]+/',$sender,$matches);
		if(empty($sender) || !empty($matches))
		$sender = SMS_SENDER;

		if(SENDFLAG == '1' && ($_SESSION['Auth']['User']['mobile'] != DUMMY_USER)){
			$mobiles = array_unique($receivers);
			$logger = $this->dumpLog('sendMessageVia247Promo', '247SmsRequestsPromo');

			$num = intval(count($mobiles)/50);
			if(count($mobiles)%50 !=0) $num = $num + 1;
			for($i = 0; $i < $num; $i++){
				$arr = array_slice($mobiles,$i*50,50);
				$receivers = "";
				foreach($arr as $mobi){
					if(strlen($mobi) == 10)
					$mobi = "91" . $mobi;
					$receivers .= $mobi . ",";
				}
				$receivers = substr($receivers, 0, -1);
				$logger->info("Message: $message");
				$logger->info("Receivers: $receivers");
				//$ch = curl_init('https://opt.smsapi.org/SendSMS.aspx');
				$serviceName = "PROMOTIONAL_HIGH";//@TODO make an entry in bootstrap and change the value

				$adm = "EmailID=".PAYONE_SMS_USER_24X7SMS."&Password=".PAYONE_SMS_PASS_24X7SMS."&MobileNo=".$receivers."&SenderID=CAMPGN&Message=".urlencode($message)."&ServiceName=".$serviceName;//@TODO change UserName and Password
				$type1 = 2;

				$url = 'http://smsapi.24x7sms.com/api_1.0/SendSMS.aspx?'.$adm;
				$ch = curl_init($url);
				//$Rec_Data = file_get_contents('http://smsapi.24x7sms.com/api_1.0/SendSMS.aspx?'.$adm);
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
				curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
				curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($ch, CURLOPT_TIMEOUT, 50);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				//echo $adm;
				$Rec_Data = curl_exec($ch);
				curl_close($ch);

				if(empty($Rec_Data) || $Rec_Data == "INVALID username or password" || $Rec_Data == "INVALID PARAMETERS" || $Rec_Data == "INVALID SenderID" || $Rec_Data == "INSUFFICIENT_CREDIT" ){
					if($Rec_Data == "INSUFFICIENT_CREDIT"){
						$this->mailToUsers("(SOS) NO SMS Credits Left in PAYONE PROMOTIONAL ACCOUNT", "(SOS) NO SMS Credits Left in PAYONE", array('ashish@mindsarray.com','chirutha@mindsarray.com'));
					}
					else {
						$msg = "Got error as: " .$Rec_Data ."<br>";
						$msg .= "Sender: " . $sender . "<br>";
						$msg .= "Receivers: ". implode(",",$arr) . "<br>";
						$msg .= $message;
						$this->mailToUsers("Problem in 24X7SMS Service",  "Got error as: " .$Rec_Data, array('24x7smshelpdesk@gmail.com'));
						//$this->mailToUsers("Problem in 24X7SMS Service",  "Got error as: " .$Rec_Data, array('ashish@mindsarray.com'));
					}
				}
				else{
					$abc = explode('<br>',$Rec_Data);

					//$ret_val = $abc[1];
					$recurr_time = 10800;
					$usrObj = ClassRegistry::init('User');
					//foreach($arr as $mobi){
					$vals = array();
                                        $redis = $this->redis_connect();
                                        $queryQ = "SMS_LOG_QUERY";
					foreach($abc as $str){
						if(empty($str)){
							continue;
						}
						$strArr = explode(":", $str);
						$mob = "";
						if($strArr[0]=="BLOCKED"){
							$mob = substr($strArr[1],-10);
							$ret_val = "0";

						}elseif($strArr[0]=="MsgID"){
							$mob = substr($strArr[2],-10);
							$ret_val = $strArr[1];
							//$vals[] = "('".$ret_val."','".addslashes($str)."',$type1,'".addslashes($message)."','".$mob."',$recurr_time,'".date('Y-m-d')."','".date("Y-m-d H:i:s")."','".(time()+$recurr_time)."')";
							$in_qry = "insert into msg247smsdellog(ret_val,reply,type,message,mobile,recurr_time,date,created,timestamp) values ('".$ret_val."','".addslashes($str)."',$type1,'".addslashes($message)."','".$mob."',$recurr_time,'".date('Y-m-d')."','".date("Y-m-d H:i:s")."','".(time()+$recurr_time)."')";
                                                        
                                                        if(!$usrObj->query($in_qry)){
                                                                $logger->info(" query_failed ".$usrObj->getLog());
                                                                $redis->lpush($queryQ,$in_qry);
                                                        }

						}
					}

					//$usrObj->query("insert into msg247smsdellog(ret_val,reply,type,message,mobile,recurr_time,date,created,timestamp) values ".implode(",",$vals));

				}
			}
		}
	}

	function makeSpamFree($msg){
		$keywords = array('G.L.C.online-mobile-Award','Loomis','Euromillions09','live(dot)com','Dr.Niss','live.com',
		'chevron','chvlondon','Mr.Mark','+448712347121','+ 448712347121','44701428261','chvlondon1@gmail.com','chvlondon1(at)gmail.com',
		'LOTTERY','lottery','GBP','UK $','UK$','Lund','Pussy','Fuck','Naked','Breast','Choot','Gaand','Loda','Chod','Dick','Vagina','Cock','Bhosra');

		$replaced = array('G.L.C.0NLine-m0bile-Award','L00Mis','Eur0milli0ns09','live(d0t)c0m','Dr.N1ss','live.c0m',
		'chevr0n','chvl0nd0n','Mr.Mark','+4487 1 2347121','+ 4487 1 2347121','44701 428261','chvl0nd0n1@gmail.com','chvl0nd0n1(at)gmail.com',
		'L0TTERY','l0ttery','G(B)P','UK D0llar','UKD0llar','L**d','Pu**y','F**k','Nak*d','Br**st','Ch00t','G@@nd','L0da','Ch0d','D**k','Vag!na','C*ck','Bh*sra');

		return str_ireplace($keywords, $replaced, $msg);
	}

	function sendMessageGupshup($numArr,$msg,$priority,$toresend){
		$msg = trim($msg);
		if(empty($numArr) || empty($msg)) return;

		if($priority == 'low' && !$this->checkTimeSlot()){
			$this->sendMessage('',$numArr,$msg,'fail-gupshup');
			//$this->addNonSentMessages(SMS_SENDER,$numArr,$msg,'sms');
		}
		else if(SENDFLAG == '1' && ($_SESSION['Auth']['User']['mobile'] != DUMMY_USER)){
			$message = $msg;
			$mobiles = $numArr;
			//gupshup fixed paras
			if($priority == 'high'){
				$gs_url = GS_API;
				$param[userid] = GS_UID;
				$param[password] = GS_PASSWD;
			}
			else if($priority == 'tadka'){
				$gs_url = GS_API;
				$param[userid] = GS_UID_TADKA;
				$param[password] = GS_PASSWD_TADKA;
			}
			else if($priority == 'playwin'){
				$gs_url = GS_API;
				$param[userid] = '2000099453';
				$param[password] = 'playwin';
			}
			else{
				$gs_url = GS_API_LOW;
				$param[userid] = GS_UID_LOW;
				$param[password] = GS_PASSWD_LOW;
			}
			$param[method]= GS_METHOD;
			$param[msg] = $message;
			$param[v] = GS_VER;
			$param[msg_type] = GS_MSG_TYP; //Can be "FLASHï¿½/"UNICODE_TEXT"/ï¿½BINARYï¿½
			$param[auth_scheme] = GS_AUTH_SCM;
			$param[mask] = GS_MASK;
			$param[format] = 'json';
			//ends

			$num = intval(count($mobiles)/GS_NO_LIMIT);
			if(count($mobiles)%GS_NO_LIMIT !=0) $num = $num + 1;
			for($j = 0; $j < $num; $j++){
				$arr = array_slice($mobiles,$j*GS_NO_LIMIT,GS_NO_LIMIT);
				$receivers = "";
				foreach($arr as $mobi){
					if(strlen($mobi) == 10)
					$mobi = "91" . $mobi;
					$receivers .= $mobi . "|";
				}
				$receivers = substr($receivers, 0, -1);

				//gupshup requests
				$request =""; //initialise the request variable
				$param[send_to] = $receivers;
				//Have to URL encode the values
				foreach($param as $key=>$val) {
					$request.= $key."=".urlencode($val);
					//we have to urlencode the values
					$request.= "&";
					//append the ampersand (&) sign after each parameter/value pair
				}
				$request = substr($request, 0, strlen($request)-1);
				//remove final (&) sign from the request
				$url = $gs_url.$request;
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$curl_scraped_page = curl_exec($ch);
				curl_close($ch);
				$str  = $curl_scraped_page;
				$str = json_decode($str,true);
				if(empty($str) || $str['response']['status'] == 'error'){
					if(isset($str['response']) && $str['response']['id'] == '253'){
						$msg = "Got error " . $str['response']['details'] . "<br/>";
						$msg .= "Receivers: ". $receivers . "<br>";
						$msg .= "Message: " . $message;
						//$this->mailToUsers("Gupshup Templates Not Matching", $msg,array('dinesh@mindsarray.com','ashish@mindsarray.com'));
					}
					else {
						$msg = "Got error " . $str['response']['details'] . "<br/>";
						$msg .= "Receivers: ". $receivers . "<br>";
						$msg .= $message;
						$this->mailToAdmins("GupShup Service Not Working", $msg);
					}
					if($priority == 'high'){
						$this->sendMessage("",$arr,$message,'fail-gupshup-payone');
					}
					else if($priority == 'tadka'){
						$this->sendMessage("",$arr,$message,'fail-gupshup-tadka');
					}
					else {
						$this->sendMessage("",$arr,$message,'fail-gupshup');
					}
				}
				$usrObj = ClassRegistry::init('User');
				if($toresend != 0){
					$toresend = 1;
				}
				if(!isset($str['data'])){
					$usrObj->query("insert into gupshup_del_log(reply,mobile_no,unique_id,err_no,priority_root,resend_flag,message,created) values ('".trim($str['response']['status'])."','".trim($str['response']['phone'])."','".trim($str['response']['id'])."','".trim($str['response']['details'])."','$priority',$toresend,'".addslashes($message)."','".date('Y-m-d H:i:s')."')");
				}
				else foreach($str['data']['response_messages'] as $resp){
					$usrObj->query("insert into gupshup_del_log(reply,mobile_no,unique_id,err_no,priority_root,resend_flag,message,created) values ('".trim($resp['status'])."','".trim($resp['phone'])."','".trim($resp['id'])."','".trim($resp['details'])."','$priority',$toresend,'".addslashes($message)."','".date('Y-m-d H:i:s')."')");
				}
				//ends
			}
		}
		//return $ret;
	}

	function sendMessageViaAchariyaAPI($sender,$receivers,$message,$priority=null,$unicode=null){ //receivers is in comma seperated format
		//$priority = 2;
		$domain="api.smstadka.com";
		$method="POST";
		//$sender = SMS_SENDER;
		$username=urlencode(SMS_USER);
		$password=urlencode(SMS_PASS);
		$sender=urlencode($sender);
		$message=urlencode(stripslashes($message));
		$content = "username=$username&password=$password&sender=$sender&to=$receivers&message=$message";
		$priority = 1;
		if($priority != null){
			$content .= "&priority=$priority";
		}
		if($unicode != null){
			$content .= "&unicode=$unicode";
		}

		$opts = array(
			'http'=>array(
				'method'=>"$method",
				'content' => $content,
				'header'=>"Accept-language: en\r\n" .
				"Cookie: foo=bar\r\n"
				)
				);

				$context = stream_context_create($opts);

				$fp = fopen("http://$domain/pushsms.php", "r", false, $context);
				$response = @stream_get_contents($fp);
				fpassthru($fp);
				fclose($fp);
				if(!(strpos(strtolower($response),'sorry') === false) || empty($response)){
					//$this->sendMessageViaFreeSMSAPI($sender,$receivers,$message,1);
					$this->sendMessageGupshup(explode(",",$receivers),$message,'low',0);

					$msg = "Got error as: " .$response ."<br>";
					$msg .= "Sender: " . $sender . "<br>";
					$msg .= "Receivers: ". $receivers . "<br>";
					$msg .= "Below message has been sent via gupshup power pack<br>";
					$msg .= $message;
					$this->mailToAdmins("Achariya Service Not Working", $msg);
				}
				return $response;
	}

	function sendMessageViaFreeSMSAPI($sender,$receivers,$message,$prio){ //receivers is in comma seperated format
		if(SENDFLAG == '1' && $_SESSION['Auth']['User']['mobile'] != DUMMY_USER){
			$ch = curl_init('http://s1.freesmsapi.com/bulksms/send');
			curl_setopt($ch, CURLOPT_POST,1);

			//curl_setopt($ch, CURLOPT_POSTFIELDS,POSTVARS.$Email);
			if($prio == '1'){
				curl_setopt($ch, CURLOPT_POSTFIELDS,"skey=".FREESMSAPI_KEY_ASHISH."&message=".urlencode(stripslashes($message))."&senderid=".$sender."&mobile=".$receivers."&tag=General");
				//file_get_contents("http://s1.freesmsapi.com/bulksms/send?skey=7f23bf3a8ce657ab9c24959b17b6b6a3&message=".urlencode(stripslashes($message))."&senderid=".$sender."&mobile=".$receivers."&tag=General");
			}
			else if($prio == '2'){
				curl_setopt($ch, CURLOPT_POSTFIELDS,"skey=".FREESMSAPI_KEY_ANURAG."&message=".urlencode(stripslashes($message))."&senderid=".$sender."&mobile=".$receivers."&tag=General");
				//file_get_contents("http://s1.freesmsapi.com/bulksms/send?skey=5364d954a33628cb983425c3c2173a89&message=".urlencode(stripslashes($message))."&senderid=".$sender."&mobile=".$receivers."&tag=General");
			}

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
			curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
			curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$array = $this->xml2array($Rec_Data);
			if(isset($array['response']['error']['message']) || empty($array)){
				if(isset($array['response']['error']['message'])){
					$msg = "Got error as: " . $array['response']['error']['message'] . "<br/>";
					$msg .= "Sender: " . $sender . "<br/>";
					$msg .= "Receivers: " . $receivers . "<br/>";
					$msg .= "Message: " . $message;
				}
				else $msg = "API is not reachable";
				$this->mailToAdmins("Mosh API Not Working", $msg);
			}

			return $Rec_Data;
		}
		return null;
	}

	function makeOptIn247SMS($mobile,$type){
		if(strlen($mobile) == 10) $mobile = "91$mobile";

		if($type == 4){
			$adm = "EmailID=".PAYONE_SMS_USER_24X7SMS."&Password=".PAYONE_SMS_PASS_24X7SMS;
		}
		else if($type == 1){
			$adm = "EmailID=".SMS_USER_24X7SMS."&Password=".SMS_PASS_24X7SMS;
		}
		$adm .= "&opt_Numbers=$mobile&opt_Status=2&opt_Date=".urlencode(date('m-d-Y h:i:s'))."&opt_Unique_ID=".time();

		$ch = curl_init('http://optapi.24x7sms.com/api_1.0/bulk_reg_process.aspx?'.$adm);
		curl_setopt($ch, CURLOPT_POST,0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$Rec_Data = curl_exec($ch);//$this->xml2array();
		//$this->printArray($Rec_Data);
	}

	function makeOptOut247SMS($mobile,$type){
		if(strlen($mobile) == 10) $mobile = "91$mobile";

		if($type == 4){
			$adm = "EmailID=".PAYONE_SMS_USER_24X7SMS."&Password=".PAYONE_SMS_PASS_24X7SMS;
		}
		else if($type == 1){
			$adm = "EmailID=".SMS_USER_24X7SMS."&Password=".SMS_PASS_24X7SMS;
		}
		$adm .= "&opt_Numbers=$mobile&opt_Status=3&opt_Date=".urlencode(date('m-d-Y h:i:s'))."&opt_Unique_ID=".time();

		$ch = curl_init('http://optapi.24x7sms.com/api_1.0/bulk_reg_process.aspx?'.$adm);
		curl_setopt($ch, CURLOPT_POST,0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$Rec_Data = curl_exec($ch);//$this->xml2array();
		//$this->printArray($Rec_Data);
	}

	function optinCheck($mobile,$type){
		$url = 'http://optapi.24x7sms.com/api_1.0/Check_Number_Status.aspx?';
		if($type == 1){
			//$adm = "username=".SMS_USER_24X7SMS."&password=".SMS_PASS_24X7SMS."&numbers=$mobile&response_type=1&separator=,";
			$adm = "EmailID=".SMS_USER_24X7SMS."&Password=".SMS_PASS_24X7SMS."&Numbers=".$mobile;
		}
		else if($type == 4){
			$adm = "EmailID=".PAYONE_SMS_USER_24X7SMS."&Password=".PAYONE_SMS_PASS_24X7SMS."&Numbers=".$mobile;
			//$adm = "username=optPAYONE&password=optPAYONE&numbers=$mobile&response_type=1&separator=,";
		}

		$url = $url.$adm;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST,0);

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$Rec_Data = trim(curl_exec($ch));
		$ret = explode(":",$Rec_Data);
		return trim($ret[1]);
	}

	function sendMessageVia247SMSNew($sender,$receivers,$message,$type=null,$switch=null){ //receivers is in comma seperated format
		if(!is_array($receivers)) {
			$receivers = explode(",",$receivers);
		}
		if(empty($message))return;
        if(count($receivers)==1){
            $bulkmessages = $sender."|<>|".$receivers[0]."|<>|".$message."|<>|".$type."|<>|".$switch;
            $this->redis = $this->redis_connect();
			$this->redis->lpush("singlefinalmessages", $bulkmessages);
			$logger = $this->dumpLog('sendMessageVia247SMSNew', '247SmsRequests');
			$logger->info($bulkmessages);
        }else{
            $arr = array_chunk($receivers,2,true);
            foreach($arr as $receivers_chunk){
                $bulkmessages = $sender."|<>|".implode(",",$receivers_chunk)."|<>|".$message."|<>|".$type."|<>|".$switch;
                $this->redis = $this->redis_connect();
                $this->redis->lpush("finalmessages", $bulkmessages);
                $logger = $this->dumpLog('sendMessageVia247SMSNew', '247BulkSmsRequests');
                $logger->info($bulkmessages);
            }
        }
	}


	function sendMessageViaTata($to, $message, $type=null,$switch=null) {
		if(is_array($to)){
			$to = implode(",",$to);
		}
        
       	$bulkmessages = $to."|<>|".$message."|<>|".$type."|<>|".$switch;
        $this->redis = $this->redis_connect();
		$this->redis->lpush("tatamessages", $bulkmessages);
		$logger = $this->dumpLog('sendMessageViaTataNew', 'TataSmsRequests');
		$logger->info($bulkmessages);
	}
	
	function sendMessageViaTataNew($to, $message, $type=null,$switch=null) {
		// file_put_contents('/tmp/tataapi_incomming.txt', Date("Y-m-d H:i:s  -- ")."Inside send msg tata -- ".json_encode($to)."  $sender,$message \n", FILE_APPEND | LOCK_EX);
		$sender = 'PAYONE';

		/*$fhtt = fopen('/tmp/tataapi_incomming.txt','a+');
		 fwrite($fhtt, "Inside TATA\n");
		 fwrite($fhtt, "Receiver ".json_encode($to)." \n");
		 fclose($fhtt);*/
		//--It will prefix 91 to mobile number if it is 10 digit number
		if(!is_array($to)){
			$to = explode(",",$to);
		}
		if(is_array($to)) {
			$to = array_map(function($msisdn) { if(strlen($msisdn)==10) return "91".$msisdn; }, $to);
			$to = implode(",",$to); //explode(",",$to);
		}
		//------------------------
		//$intype = ($type=='payone')?4:1;
		$message = trim($message);
		if(empty($to) || empty($message))return;
		/*preg_match('/[0-9]+/',$sender,$matches);
		 if(empty($sender) || !empty($matches))
		 $sender = SMS_SENDER;*/
		if(SENDFLAG == '1'){
            $uniq_log_id = (int)(microtime(true) * 100000);
			$logger = $this->dumpLog('TATA_API_HIT', 'TATA_apihit');
			try {

				//$url = "http://182.156.191.54:8080/EnterpriseUi/pushAction/NzQ%3D.html?username=payone&password=payone123&from=".$sender."&to=".$to."&text=".urlencode($message)."&notification=true";
                                $url = "http://182.156.191.54:8080/EnterpriseUi/pushasynchAction/MTM1.html?username=paynew&password=p@yN3w@31&from=".$sender."&to=".$to."&text=".urlencode($message)."&notification=true";
				try{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30);
					//curl_setopt($ch, CURLOPT_TIMEOUT_MS, 5000);
					$output = curl_exec($ch);					
                                        $errno = curl_errno($ch);
                                        curl_close($ch);
                                        
                                        $logger->info($uniq_log_id." : calling tataurl => ".$url);
                                        $formated_data = $this->format_tata_res($output);
                                        
                                        $logger->info($uniq_log_id." : output received => ".  json_encode($output));
                                        //$formated_data = $this->format_tata_res($output);
                                        $logger->info($uniq_log_id." : output received => ".  json_encode($formated_data));
                                        $time = time();
                                        $date = date("Y-m-d");
                                        $datetime = date("Y-m-d H:i:s");
                                        $usrObj = ClassRegistry::init('User');
                                        $messageInsert = addslashes($message);//$this->checkIfResetPassMsg($message);
                                        $redis = $this->redis_connect();
                                        $queryQ = "SMS_LOG_QUERY";
                                        if (is_array($formated_data)) {
                                                foreach ($formated_data as $trnx_id => $val) {
                                                        foreach ($val as $msisdn => $msgid) {
                                                                $qry = "INSERT INTO `smstadka`.`msgTataSmsDelLog` (`tx_id`, `ret_val`, `type`, `message`, `mobile`, `sender`, `reply`, `status_flag`, `status`, `resent`, `recurr_time`, `created`, `timestamp`, `date`) VALUES ( '$trnx_id', '$msgid', 1, '$messageInsert', '$msisdn', '$switch', '$output', '0', '', '0', 10800, '$datetime', '$time', '$date')";
                                                                if(!$usrObj->query($qry)){
                                                                    $logger->warn($uniq_log_id." | ".$qry." | query_failed ".json_encode($usrObj->getLog()));
                                                                    $redis->lpush($queryQ,$qry);
                                                                }
                                                                //$logger->info($qry);
                                                        }
                                                }
                                        }else{
                                                $logger->warn($uniq_log_id." : api response not in format");
                                                $qry = "INSERT INTO `smstadka`.`msgTataSmsDelLog` (`tx_id`, `ret_val`, `type`, `message`, `mobile`, `sender`, `reply`, `status_flag`, `status`, `resent`, `recurr_time`, `created`, `timestamp`, `date`) VALUES ( NULL, NULL, 1, '$messageInsert', '$to', '$switch', '$output', '0', NULL, '0', NULL, '$datetime', '$time', '$date')";
                                                if(!$usrObj->query($qry)){
                                                    $logger->info(" query_failed ".$usrObj->getLog());
                                                    $redis->lpush($queryQ,$qry);
                                                }
                                        }
                                        
					if(empty($output) || empty($formated_data)){
						//$errno = curl_errno($ch);
                                                if(empty($formated_data)){
                                                            $this->mailToUsers("issue from tata sms operator", " $uniq_log_id Blank output received from TATA, errno received: $errno | ".json_encode($output), "nandan@mindsarray.com");
                                                }		
						$logger->debug($uniq_log_id." : Blank output received from TATA, errno received:".$errno);
                                                $logger->info("Moving route to fail-tatasms-payone ");
						return $this->sendMessage('',$to,$message,'fail-tatasms-payone',null,$switch);
					}
                                        
                                        if(empty($formated_data)){
                                            $logger->info(" Moving route to fail-tatasms-payone ");
                                            $this->mailToUsers("issue from tata sms operator", json_encode($output), "nandan@mindsarray.com");
                                        }
                                        
					//curl_close($ch);
				}catch(Exception $ex){
					$logger->debug($ex->getTraceAsString());
					return $this->sendMessage('',$to,$message,'fail-tatasms-payone',null,$switch);
				}
				
			} catch (Exception $ex) {
				$logger->debug($uniq_log_id." : ".$ex->getTraceAsString());
			}
		}else{
			$logger->debug($uniq_log_id." : SEND_FLAG is OFF in bootstrap .");
		}
	}

	function format_tata_res($response) {
		file_put_contents('/tmp/tataapi_incomming.txt', Date("Y-m-d H:i:s  -- ")."format_tata_res  -- $response \n", FILE_APPEND | LOCK_EX);
		if (preg_match('#^Wrong Credentials#i', $response)) {
			return $response;
		}
		if (preg_match('#^obile number is missing#i', $trimmed_res)) {
			return $trimmed_res;
		}
		$trimmed_res = trim(trim(trim($response, "<html><div>"), "</div></html>"),"+");
		$res_array = explode("<br/>", trim($trimmed_res));
		$reslen = count($res_array);
		$txn = explode(" ", $res_array[$reslen - 1]);
		$txnid = substr($txn[2], 1, -1);

                $final_response = array();
		foreach ($res_array as $resp) {
			$outer_res = explode("=>", $resp);
			$inner_res = explode(":", $outer_res[1]);
			if (isset($inner_res[1])) {
				//$final_response[$txnid][$outer_res[0]] = isset($inner_res[1]) ? (strpos($inner_res[1], "=") ? substr(trim($inner_res[1]), 3, -1) : substr(trim($inner_res[1]), 0, -1)) : "";
				$final_response[$txnid][trim($outer_res[0])] = isset($inner_res[1]) ? (strpos($inner_res[1], "=") ? substr(trim(preg_replace('/\s\s+/', '',$inner_res[1])), 3, -1) : substr(trim(preg_replace('/\s\s+/', '',$inner_res[1])), 0, -1)) : "";
			}
		}
		return $final_response;
	}
	
	function getSendMessageViaTata($flag=0){ //receivers is in comma seperated format
		$this->redis = $this->redis_connect();
		
		$fname = ($flag == 0) ? "Tataapihit" : "Tataapihitb2c";
        $queue = ($flag == 0) ? "tatamessages" : "tatamessages_b2c";
        
        $logger = $this->dumpLog('TataQueue', $fname);        
        
		while(true){
			if($this->redis->llen($queue)==0 ){
				sleep(2);
				continue;
			}
            
			$one_msg = $this->redis->rpop($queue);
			
			$logger->info("TataSMS===".$one_msg);
                    
			$one_by_one_msg_arr = explode("|<>|",$one_msg);
			$receivers = explode(",",$one_by_one_msg_arr[0]);
			$receivers = array_unique($receivers);
			$message = $one_by_one_msg_arr[1];
			$type = $one_by_one_msg_arr[2];
			$switch = $one_by_one_msg_arr[3];
			
			$logger->info("Tata Request===".implode(",",$receivers)."|".$message."|".$type."|".$switch);

			$this->sendMessageViaTataNew($receivers,$message,$type,$switch);
                
		}
	}

	function getSendMessageVia247SMS($msgopt = 1){ //receivers is in comma seperated format
		$this->redis = $this->redis_connect();

		//$serviceName = "OPTIN_OPTOUT";//@TODO make an entry in bootstrap and change the value
        $serviceName = "TEMPLATE_BASED";
		$xml_smstdk =  "<Data><userdetails><EmailID>".SMS_USER_24X7SMS."</EmailID><Password>".SMS_PASS_24X7SMS."</Password><SenderID>SMSTDK</SenderID><ServiceName>".$serviceName."</ServiceName></userdetails>";
		$xml_payone =  "<Data><userdetails><EmailID>".PAYONE_SMS_USER_24X7SMS."</EmailID><Password>".PAYONE_SMS_PASS_24X7SMS."</Password><SenderID>PAYONE</SenderID><ServiceName>".$serviceName."</ServiceName></userdetails>";
        
        $logger = ($msgopt == 0)?$this->dumpLog('XMLAPIHIT', '247apihit'):(($msgopt == 1)?$this->dumpLog('XMLAPIHIT', '247bulkapihit') : $this->dumpLog('XMLAPIHIT', '247apihitb2c'));        
        $queue = ($msgopt == 0)?"singlefinalmessages":(($msgopt == 1) ? "finalmessages" : "247messages_b2c");
        $logger->info($msgopt."    ".$queue);
		while(true){
			$q_length = $this->redis->llen($queue);
			if($q_length==0 ){
				sleep(2);
				continue;
			}
            
			
			//$all_msg = $this->redis->lrange("finalmessages",0,50);
			$xml_1 = "";
			$xml_2 = "";
			//$logger->info("Lrange===".count($all_msg));
			//static $payoneMsgCount = 0;
			//static $smsTdkCount = 0;
			$data = array();
			for($i = 0; $i < 50; $i++){
				//foreach($all_msg as $one_msg){
                $one_msg = $this->redis->rpop($queue);
                
				if(!empty($one_msg)){
					//$logger->info("247SMS===".$one_msg);
                    
					$one_by_one_msg_arr = explode("|<>|",$one_msg);
					$receivers = explode(",",$one_by_one_msg_arr[1]);
					$receivers = array_unique($receivers);
					$message = $one_by_one_msg_arr[2];
					$sender = $one_by_one_msg_arr[0];
					$type = $one_by_one_msg_arr[3];
					$switch = $one_by_one_msg_arr[4];
					$logger->info("SingleRequest===".$sender."|".implode(",",$receivers)."|".$message."|".$type."|".$switch);
					
					/*try{
						$logger->info($sender."|".implode(",",$receivers)."|".$message."|".$type);
					} catch (Exception $ex) {
						print $ex->getTraceAsString();
						$logger->dedug("Exception eccured : ".$ex->getTraceAsString());
					}*/

					$message = trim($message);
					if(empty($receivers) || empty($message))continue;
					preg_match('/[0-9]+/',$sender,$matches);
					if(empty($sender) || !empty($matches))
					$sender = SMS_SENDER;

					foreach ($receivers as $rec){

						$rec = trim(substr($rec,-10));
						$rec = "91" . $rec;
						if(empty($rec) || empty($message))continue;
						preg_match('/[0-9]+/',$sender,$matches);
						$data[$rec]['sms'] = $message;
						$data[$rec]['switch'] = $switch;
						if($type=='payone'){
							$xml_1 .= "<Messages><To>".$rec."</To><SMSText>".htmlspecialchars($message)."</SMSText></Messages>";
						}
						else{
							$xml_2 .= "<Messages><To>".$rec."</To><SMSText>".htmlspecialchars($message)."</SMSText></Messages>";
						}
					}
				}
				else break;
			}

			if(SENDFLAG == '1'){
				if(!empty($xml_1))$xml_1 = $xml_payone.$xml_1."</Data>";
				if(!empty($xml_2))$xml_2 = $xml_smstdk.$xml_2."</Data>";
				$logger->info("XML_PAYONE::".$xml_1);
				$logger->info("XML_SMSTDK::".$xml_2);
				$logger->info("data to be passed ".json_encode($data));
				if(!empty($xml_1))$this->sendFor247Response($xml_1,4,$data,$logger);
				if(!empty($xml_2))$this->sendFor247Response($xml_2,1,$data,$logger);
				sleep(1);
				continue;
			}
		}
	}

	function sendFor247Response($xml,$type1,$data,$loggerObj){
		$url = "http://smsapi.24x7sms.com/api_1.0/SendSMSXML.aspx";
		$adm = "XML=".urlencode($xml);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);
			
		/*	$adm = "XML=".urlencode($xml);
		 $ch = curl_init($url."?".$adm);
		 curl_setopt($ch, CURLOPT_POST,0); */
			
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$Rec_Data = trim(curl_exec($ch));
		curl_close($ch);
        
		//$logger = $this->dumpLog('XMLAPIHIT', '247apihit');
        $logger = $loggerObj;
		$logger->info($url." |  Response=  ".$Rec_Data);
		//$logger->info("data messaged ".json_encode($data));
		$sender = "";
        
                $failure_error_array = array(
                  'INVALID USERNAME OR PASSWORD','INVALID PARAMETERS','INVALID SENDERID','INSUFFICIENT_CREDIT','ABSENT_SUBSCRIBER','UNKNOWN_SUBSCRIBER','SYSTEM_FAILURE','INVALID_SERIES','NETWORK_FAILURE','OTHERS'
                );
                
                //if(empty($Rec_Data) || $Rec_Data == "INVALID username or password" || strtoupper($Rec_Data) == "INVALID PARAMETERS" || strtoupper($Rec_Data) == "INVALID SENDERID" || strtoupper($Rec_Data) == "INSUFFICIENT_CREDIT" ) {
                if(empty($Rec_Data) || in_array(strtoupper(trim($Rec_Data)),$failure_error_array)){		
			$logger->info("Response Failed=  ".$Rec_Data." Shifted to other route.");
            
			foreach ($data as $key=>$val){
				if($type1 == '4'){
					$this->sendMessage($sender,$key,$val['sms'],'fail-24x7sms-payone',null,$val['switch']);
				}
				else {
					$this->sendMessage($sender,$key,$val['sms'],'fail-24x7sms',null,$val['switch']);
				}
			}
            
			if($Rec_Data == "INSUFFICIENT_CREDIT"){
				if($type1 == '4'){//payone
					$this->mailToUsers("(SOS) NO SMS Credits Left in PAYONE", "(SOS) NO SMS Credits Left in PAYONE", array('ashish@mindsarray.com','chirutha@mindsarray.com','sarfaraz@iglobesolutions.com'));
				}
				else {//smstadka
					$this->mailToUsers("(SOS) NO SMS Credits Left in SMSTadka", "(SOS) NO SMS Credits Left in SMSTadka", array('ashish@mindsarray.com','chirutha@mindsarray.com','sarfaraz@iglobesolutions.com'));
				}
			}
			else {
				$msg = "Got error as: " .$Rec_Data ."<br>";
				$msg .= "Receivers: ". json_encode($data) . "<br>";
					
				$this->mailToUsers("Problem in 24X7SMS Service",  "Got error as: " .$Rec_Data, array('24x7smshelpdesk@gmail.com'));
				//$this->mailToUsers("Problem in 24X7SMS Service",  "Got error as: " .$Rec_Data, array('ashish@mindsarray.com'));
			}
		}
		else{
			$logger->info("Response successful");
			//error response
			//BLOCKED:9199672649<br>BLOCKED:9199672642<br>
			//success response
			//MsgID:7abcb08d81234e04a4efb4289ebe2aec:919967264985<br>MsgID:2683e684bd5f4105ad53d0c421e6f89a:919967264285<br>
			$abc = explode('<br>',$Rec_Data);

			//$ret_val = $abc[1];
			$recurr_time = 10800;
			$usrObj = ClassRegistry::init('User');
			//foreach($arr as $mobi){
			$vals = array();
			foreach($abc as $str){
				if(empty($str) || strlen(trim($str)) < 1){
					continue;
				}
				$strArr = explode(":", $str);
				$mob = "";
				if($strArr[0]=="BLOCKED"){
					$mob = substr($strArr[1],-10);
					$message = $data['91'.$mob]['sms'];
					$switch = $data['91'.$mob]['switch'];
					
					$logger->info("Optin issues: " . $mob);
					$ret_val = "0";
					if($type1 == 4){
                        //--- commented after 
						//$this->makeOptIn247SMS($mob,4);
						$this->sendMessage($sender,$mob,$message,'fail-24x7sms-payone',null,$switch);
					}
					else {
						$this->sendMessage($sender,$mob,$message,'fail-24x7sms',null,$switch);
					}
				}elseif($strArr[0]=="MsgID"){
					$mob = substr($strArr[2],-10);
					$ret_val = $strArr[1];
					$logger->info("message =============== ".json_encode($data["91".$mob]));
					$vals[] = "('".$ret_val."','$switch',$type1,'".addslashes($data["91".$mob]['sms'])."','".$mob."',$recurr_time,'".date('Y-m-d')."','".date("Y-m-d H:i:s")."','".(time()+$recurr_time)."')";
					//$usrObj->query("insert into msg247smsdellog(ret_val,reply,type,message,mobile,recurr_time,date,created,timestamp) values ('".$ret_val."','".addslashes($str)."',$type1,'".addslashes($message)."','".$mob."',$recurr_time,'".date('Y-m-d')."','".date("Y-m-d H:i:s")."','".(time()+$recurr_time)."')");
					unset($data["91".$mob]);
				}
			}
			//$logger->info("data array".json_encode($vals));
                        $redis = $this->redis_connect();
                        $queryQ = "SMS_LOG_QUERY";
                        $final_val = array_chunk($vals, 20);
                        foreach($final_val as $small_vals){
                            $qry_res = "insert into msg247smsdellog(`ret_val`,`reply`,`type`,`message`,`mobile`,`recurr_time`,`date`,`created`,`timestamp`) values ".implode(",",$vals);
                            if($usrObj->query($qry_res)){
                                $querystatus = "yes";
                            }else{
                                $usrnewObj = ClassRegistry::init('Log');
                                if(!$usrnewObj->query($qry_res)){
                                    $logger->info(" query_failed ".$usrObj->getLog());
                                    $redis->lpush($queryQ,$qry_res);
                                }else{
                                    $querystatus = "yes";
                                }
                            }
                            $logger->info("qry result =>".$qry_res." | qrystatus : ".$querystatus);
                        }
			//$logger->info("Query===insert into msg247smsdellog(ret_val,reply,type,message,mobile,recurr_time,date,created,timestamp) values ".implode(",",$vals));
		}
	}

	function sendMessageVia247SMS($sender,$receivers,$message,$type=null,$switch=null){ //receivers is in comma seperated format
		return $this->sendMessageVia247SMSNew($sender,$receivers,$message,$type,$switch);		
	}

	function sendMessageViaVfirst($receivers,$message,$switch=null){
		$sender = "PAYONE";
		$message = trim($message);
		if(empty($receivers) || empty($message))return;
		preg_match('/[0-9]+/',$sender,$matches);

		$ch = curl_init('http://www.myvaluefirst.com/smpp/sendsms');
		curl_setopt($ch, CURLOPT_POST,1);
		$message = str_replace("\n","\r\n",$message);
        
		$logger = $this->dumpLog('sendMessage', 'Vfirst');
		
		$adm = "username=mindsarray&password=minds2ay&from=$sender&to=".implode(",",$receivers)."&text=".urlencode($message)."&dlr-mask=19&dlr-url";
		curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$Rec_Data = curl_exec($ch);
		curl_close($ch);
		
		
		$logger->info("Sent: $adm");
		$logger->info("Output: $Rec_Data");
				
		if(empty($Rec_Data) || strpos($Rec_Data,'Sent.') === false){
			$logger->info("Output: Problem in valuefirst so resending message via fail-vfirst");
			
			$msg = "Got error as: " .$Rec_Data ."<br>";
			$msg .= "Sender: " . $sender . "<br>";
			$msg .= "Receivers: ". implode(",",$receivers) . "<br>";
			$msg .= nl2br($message);
			$this->mailToUsers("Problem in ValueFirst Service", $msg,array('ashish@mindsarray.com'));
			$this->sendMessage($sender,$receivers,$message,'fail-vfirst',null,$switch);
			
			
		}
		else {
			$usrObj = ClassRegistry::init('User');
			$messageInsert = $message;//$this->checkIfResetPassMsg($message);
			$usrObj->query("insert into delivery_vfirst(message,mobile,status,created,date) values ('".addslashes($messageInsert)."','".implode(",",$receivers)."','".addslashes($Rec_Data)."','".date("Y-m-d H:i:s")."','".date('Y-m-d')."')");
		}
	}

	function sendMessageViaSMSLane($sender,$receivers,$message,$trans){ //receivers are in array format
		if(empty($sender)) $sender = SMS_SENDER;
		$message = trim($message);
		if(empty($receivers) || empty($message))return;
		if(SENDFLAG == '1' && ($_SESSION['Auth']['User']['mobile'] != DUMMY_USER)){
			$mobiles = $receivers;
			$mobiles = array_unique($mobiles);

			$num = intval(count($mobiles)/50);
			if(count($mobiles)%50 !=0) $num = $num + 1;
			for($i = 0; $i < $num; $i++){
				$arr = array_slice($mobiles,$i*50,50);
				$receivers = "";
				foreach($arr as $mobi){
					if(strlen($mobi) == 10)
					$mobi = "91" . $mobi;
					$receivers .= $mobi . ",";
				}
				$receivers = substr($receivers, 0, -1);
				$ch = curl_init('http://smslane.com/vendorsms/pushsms.aspx');
				curl_setopt($ch, CURLOPT_POST,1);
				$adm = "user=".USER_SMSLANE."&password=".PASS_SMSLANE."&msg=".urlencode($message)."&sid=".$sender."&msisdn=".$receivers."&fl=0&gwid=$trans";
				//exit;
				curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);

				curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
				curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
				curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($ch, CURLOPT_TIMEOUT, 100);
				$Rec_Data = curl_exec($ch);
				curl_close($ch);
				if(empty($Rec_Data)){
					$msg = "Got error as: " .$Rec_Data ."<br>";
					$msg .= "Receivers: ". implode(",",$arr) . "<br>";
					$msg .= "Below message has been sent via smslane transactional root<br>";
					$msg .= $message;
					$this->mailToAdmins("Problem in SMSLane Service", $msg);
				}
				$response_arr = explode("<br />",$Rec_Data);
				$j = 0;
				$usrObj = ClassRegistry::init('User');
				$messageInsert = $message;//$this->checkIfResetPassMsg($message);

				foreach($response_arr as $resp){
					if(!empty($resp)){
						$ret_vals = explode(":",$resp);
						$resp = explode(",",$ret_vals[1]);
						$usrObj->query("insert into smslanedellog(ret_val,message,mobile,created) values ('".trim($resp[0])."','".addslashes($messageInsert)."','".$arr[$j]."','".date("Y-m-d H:i:s")."')");
					}
					$j++;
				}
			}
		}
	}

	function sendMessageViaInfobip($sender,$receivers,$message){ //receivers are in array format
		$sender = SMS_SENDER;
		$message = trim($message);
		if(empty($receivers) || empty($message))return;
		if(!is_array($receivers)) {
			$receivers = explode(",",$receivers);
		}
		$usrObj = ClassRegistry::init('User');
		if(SENDFLAG == '1' && ($_SESSION['Auth']['User']['mobile'] != DUMMY_USER)){
			$mobiles = $receivers;
			$mobiles = array_unique($mobiles);

			$error = array();
			$error['-2'] = "Not enough credits";
			$error['-3'] = "Network Not Covered";
			$error['-4'] = "Socket Exception";
			$error['-13'] = "Invalid destination address";

			$num = intval(count($mobiles)/50);
			if(count($mobiles)%50 !=0) $num = $num + 1;
			for($i = 0; $i < $num; $i++){
				$arr = array_slice($mobiles,$i*50,50);
				$receivers = "";
				foreach($arr as $mobi){
					if(strlen($mobi) == 10)
					$mobi = "91" . $mobi;
					$receivers .= $mobi . ",";
				}
				$receivers = substr($receivers, 0, -1);
				if(empty($receivers))continue;
				$ch = curl_init('http://api2.infobip.com/api/sendsms/plain');
				curl_setopt($ch, CURLOPT_POST,1);
				$adm = "user=mindsarr&password=WtzR!bG?&SMSText=".urlencode($message)."&sender=".$sender."&GSM=".$receivers."&type=LongSMS";
				//exit;
				curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);

				curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
				curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
				curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($ch, CURLOPT_TIMEOUT, 100);
				$Rec_Data = curl_exec($ch);
				$Rec_Data = $this->br2newline($Rec_Data);
				$outputs = explode("\n",$Rec_Data);
				curl_close($ch);
				$j = 0;
				$errors = array();
				$error_nums = array();
				foreach($arr as $mobi){
					$ret = trim($outputs[$j]);
					if(empty($ret))continue;
					$messageInsert = $message;//$this->checkIfResetPassMsg($message);
					if($ret < 0){
						$errors[] = $error[$ret];
						$error_nums[] = $mobi;
						$usrObj->query("insert into smslanedellog(ret_val,message,mobile,status,created) values ('".$ret."','".addslashes($messageInsert)."','".$mobi."','".$error[$ret]."','".date("Y-m-d H:i:s")."')");
					}
					else {
						$usrObj->query("insert into smslanedellog(ret_val,message,mobile,created) values ('".$ret."','".addslashes($messageInsert)."','".$mobi."','".date("Y-m-d H:i:s")."')");
					}
					$j++;
				}

				if(!empty($errors)){
					$msg = "Got errors as: " .implode(",",$errors) ."<br>";
					$msg .= "Receivers: ".implode(",",$error_nums)." <br>";
					$msg .= $message;
					if(!$this->checkTimeSlot()) {
						$msg .= "<br/>Message routed via gupshup in morning";
						$this->addNonSentMessages($sender,$error_nums,$message,'sms');
					}
					else {
						$msg .= "<br/>Cannot be sent by any root";
					}
					$this->mailToAdmins("Problem in infobip Service", $msg);
				}
			}
		}
	}

	function sendMessageViaMOS($receivers,$message,$type=null){ //receivers are in array format
		$sender = SMS_SENDER;
		$message = trim($message);
		if(!is_array($receivers)) {
			$receivers = explode(",",$receivers);
		}
		if(empty($receivers) || empty($message)) {
			return;
		}

		if($type != 'temp' && !$this->checkTimeSlot()){
			$this->sendMessage($sender,$receivers,$message,'fail-mos-nontemp');
			return;
		}

		$usrObj = ClassRegistry::init('User');
		if(SENDFLAG == '1' && ($_SESSION['Auth']['User']['mobile'] != DUMMY_USER)){
			$mobiles = $receivers;
			$mobiles = array_unique($mobiles);

			$error = array();
			$error['-2'] = "Not enough credits";
			$error['-3'] = "Network Not Covered";
			$error['-4'] = "Socket Exception";
			$error['-13'] = "Invalid destination address";

			$num = intval(count($mobiles)/50);
			if(count($mobiles)%50 !=0) $num = $num + 1;
			for($i = 0; $i < $num; $i++){
				$arr = array_slice($mobiles,$i*50,50);
				$receivers = "";
				foreach($arr as $mobi){
					if(strlen($mobi) == 10)
					$mobi = "91" . $mobi;
					$receivers .= $mobi . ",";
				}
				$receivers = substr($receivers, 0, -1);
				if(empty($receivers))continue;
				if($type == 'temp'){
					$url = "http://dndopen.dove-sms.com/SMSAPI.jsp";
					$adm = "username=smstdk&password=guest&sendername=SMSTDK&mobileno=$receivers&message=".urlencode($message);
				}
				else {
					$url = "http://dndopen.dove-sms.com/SMSAPI.jsp";
					$adm = "username=smstdk1&password=guest&sendername=SMSTDK&mobileno=$receivers&message=".urlencode($message);
				}
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST,1);
				//exit;
				curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);

				curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
				curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
				curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($ch, CURLOPT_TIMEOUT, 100);
				$Rec_Data = curl_exec($ch);
				curl_close($ch);
				$messageInsert = $message;//$this->checkIfResetPassMsg($message);
				foreach($arr as $mobi){
					$usrObj->query("insert into delivery_mos(message,mobile,status,type,created) values ('".addslashes($messageInsert)."','".$mobi."','SENT','$type','".date("Y-m-d H:i:s")."')");
				}
				if(empty($Rec_Data)){
					$msg .= "Receivers: ".implode(",",$arr)." <br>";
					$msg .= $message;

					$this->mailToAdmins("Problem in MOS Service", $msg);
					if($type == 'temp'){
						$this->sendMessage($sender,$arr,$message,'fail-mos-temp');
					}
					else {
						$this->sendMessage($sender,$arr,$message,'fail-mos-nontemp');
					}
				}
			}
		}
	}

	function createMessage($alias,$vars){
		$seperator = "@__123__@";
		$fname = $_SERVER['DOCUMENT_ROOT'] . "/templates.txt";
		$fh = fopen($fname,'r');
		$contents = fread($fh, filesize($fname));
		fclose($fh);
		$templates = json_decode($contents,true);
		//$this->printArray($templates);
		$message = $templates[$alias];

		foreach($vars as $var){
			$message = preg_replace('/@__123__@/', $var, $message, 1);
		}
		$message = preg_replace('/@__123__@/', 'N/A', $message);
		return $message;
	}

	function emailSMSToUsers($sms, $emails){
		$message = nl2br($sms);
		$message .= "<br/><br/><br/>Dear Customer, As your mobile number is in DND registry, You are receiving SMSTadka messages on email. If you wish to receive messages on your mobile, either opt out of DND registry or provide alternate non-DND mobile number.<br/>To know if your number is in DND, visit http://nccptrai.gov.in/nccpregistry/search.misc<br/>For any help, sms HELP to 09223178889.";
		$userObj = ClassRegistry::init('User');
		$str = array();
		foreach($emails as $email){
			$str[] = "('$email','".addslashes($message)."','".date('Y-m-d H:i:s')."')";
		}
		$data = $userObj->query("INSERT INTO log_mails (emailid,body,timestamp) VALUES " . implode(",",$str));
		//$this->mailToUsers('Message From SMSTadka - '.date('jS M, Y'),$message,$emails);
	}

	function getLastMessageSent($product_id,$package_id=null){
		$prodObj = ClassRegistry::init('ProductsPackage');
		if(empty($package_id))
		$data = $prodObj->query("SELECT content FROM partners_data WHERE product_id=$product_id AND (date='".date('Y-m-d')."' OR date = '".date('Y-m-d',strtotime('-1 day'))."') AND sent_flag = 1 ORDER BY modified desc LIMIT 1");

		if(!empty($data)){
			return $data['0']['partners_data']['content'];
		}
		else {
			if(empty($package_id)){
				$prodObj->recursive = -1;
				$package = $prodObj->find('all',array('fields' => array('GROUP_CONCAT(ProductsPackage.package_id) as packages'),'conditions' => array('ProductsPackage.product_id' => $product_id,'ProductsPackage.toshow' => 1), 'group' => 'ProductsPackage.product_id'));
				$package_id = $package['0']['0']['packages'];
			}
			$data = $prodObj->query("SELECT package_id,time FROM cyclic_crons WHERE package_id IN ($package_id) ORDER by time desc");
			if(empty($data)){
				$time = date('Y-m-d',strtotime('-1 days')) . ' 00:00:00';
				$logObj = ClassRegistry::init('Log');

				$logObj->recursive = -1;
				$lastMsgArr = $logObj->find('all', array(
					'fields' => array('Log.content','Log.message_id','Log.package_id'),
					'conditions' => array('Log.package_id in (' . $package_id . ')', "Log.timestamp > '$time'"),
					'order' => 'Log.timestamp desc',
					'limit' => 1
				));
				if(!empty($lastMsgArr)){
					$lastMsg = $lastMsgArr['0']['Log']['content'];
					return $lastMsg;
				}
			}
			else {
				$logObj = ClassRegistry::init('Log');

				$logObj->recursive = -1;
				$num = 0;
				foreach($data as $pck){
					if($pck['cyclic_crons']['time'] < date('H:i:s')){
						break;
					}
					else {
						$num++;
					}
				}
				if($num == count($data)) $num--;
				$pck_id = $data[$num]['cyclic_crons']['package_id'];
				$msg_num_data = $logObj->query("SELECT msg_num,frequency FROM cyclic_packages WHERE package_id = $pck_id AND cycle = 1");
				$frequency = $msg_num_data['0']['cyclic_packages']['frequency'];
				$msg_num = $msg_num_data['0']['cyclic_packages']['msg_num'];
				if($msg_num == 1){
					$msg_num = CYCLIC_LENGTH * $frequency;
				}
				else $msg_num = $msg_num - 1;

				$table = $logObj->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $pck_id AND categories_packages.category_id = data_tables.category_id");
				$table_name = $table['0']['data_tables']['table_name'];
				if($table_name == 'data_funs')$table_name = 'messages';

				$contentData = $logObj->query("SELECT cyclicData FROM cyclic_refined INNER JOIN $table_name ON ($table_name.id = cyclic_refined.message_id) WHERE cyclic_refined.msg_num = $msg_num AND cyclic_refined.package_id = $pck_id AND cyclic_refined.cycle=1 AND cyclic_refined.table = '$table_name'");
				$content = $contentData['0'][$table_name]['cyclicData'];
				return $content;
			}
		}
	}

	function sendMessageViaGSMModem($receivers,$message,$priority){
		if(!is_array($receivers)) {
			$receivers = explode(",",$receivers);
		}
		if(empty($receivers) || empty($message)) {
			return;
		}
		$usrObj = ClassRegistry::init('User');
		if(SENDFLAG == '1' && ($_SESSION['Auth']['User']['mobile'] != DUMMY_USER)){
			$mobiles = $receivers;
			$mobiles = array_unique($mobiles);

			$num = intval(count($mobiles)/50);
			if(count($mobiles)%50 !=0) $num = $num + 1;
			for($i = 0; $i < $num; $i++){
				$arr = array_slice($mobiles,$i*50,50);
				$receivers = "";
				foreach($arr as $mobi){
					if(strlen($mobi) == 10)
					$mobi = "91" . $mobi;
					$receivers .= $mobi . ",";
				}
				$receivers = substr($receivers, 0, -1);
				if(empty($receivers))continue;

				$ipArr = array('122.170.110.229','183.87.34.26');
				foreach($ipArr as $ip){
					$url = "http://$ip:2022/sender.php";
					$adm = "query=sms&mobile=$receivers&message=".urlencode($message)."&priority=$priority";
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_POST,1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);

					curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
					curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
					curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
					curl_setopt($ch, CURLOPT_TIMEOUT, 50);
					$Rec_Data = curl_exec($ch);
					curl_close($ch);
					if(!empty($Rec_Data))break;
				}
				$j = 0;
				foreach($arr as $mobi){
					$ids = explode(",",$Rec_Data);
					$messageInsert = $message;//$this->checkIfResetPassMsg($message);
					$usrObj->query("insert into delivery_modem(ret_val,message,mobile,status,priority,created,date) values (".$ids[$j].",'".addslashes($messageInsert)."','".$mobi."','SENT','$priority','".date("Y-m-d H:i:s")."','".date('Y-m-d')."')");
					$j++;
				}
				if(empty($Rec_Data)){
					$msg .= "Receivers: ".implode(",",$arr)." <br>";
					$msg .= $message;

					$this->mailToAdmins("Problem in GSM Modem", $msg);
					$this->sendMessage('',$arr,$message,'fail-modem');
				}
			}
		}
	}
	function checkIfResetPassMsg($message) {
        $message = trim($message);
        $templates = array(
                "Dear Sir,\nYour password has been reset to @__password__@",
                "Congrats!!\nYou have become Distributor of Pay1. Your login details are below\nOnline Url: http://panel.pay1.in\nUserName: @__mobile__@\nPassword: @__password__@",
                "Welcome to Pay1!\nUserName: @__mobile__@\nPassword: @__password__@\nDownload@__extra__@",
                "Dear Pay1 User, Your One Time Password (OTP) for @__abc__@ app registration is : @__password__@",
                "Dear User, Your One Time Password (OTP) to Change Number is @__password__@",
                "Dear User, Your One Time Password(OTP) to reset your password is @__password__@",
                "You can login to Pay1 Channel Partner Android App with pin: @__password__@. Kindly, change your pin from the app.",
                "Your One Time Password (OTP) for Pay1 app registration is : @__password__@.",
                "You have registered as a Retailer with Pay1. Use OTP @__password__@ to verify your mobile number. Do not share it with anyone",
                "Use OTP @__password__@ to reset pin.\n    \t\t\t\t\t\tDo not share it with anyone",
                "Your PIN was reset from Pay1 Channel Partner app. Your new PIN is @__password__@",
				"You are trying to login through a new device or browser. To login type OTP (One Time Password) @__password__@ to verify your mobile number. DO NOT SHARE IT WITH ANYONE",
				"You password has been changed for security reasons.Your new password is @__password__@",
				"Use OTP @__password__@ to reset pin. Do not share it with anyone",
				"Congratulations!\nYou have become a Salesman at Pay1\nKindly, note your pin for login\nPin: @__password__@",
				"You are trying to login through a new location or place. To login type OTP (One Time Password) @__password__@ to verify your mobile number. Do not share it with anyone.",
				"OTP for Pay1 login is @__password__@.",
				"You are trying to login through a new device or browser. To login type OTP (One Time Password) @__password__@ to verify your mobile number. Do not share it with anyone.",
				"One Time Passowrd(OTP) to create a new Distributor is @__password__@. This is valid for next 30 mins. Do not share it with anyone.",
				"One Time Passowrd(OTP) to change Distributor mobile number is @__password__@.This is valid for next 30 mins. Do not share it with anyone.",
				"One Time Passowrd(OTP) to create a new Retailer is @__password__@.This is valid for next 30 mins. Do not share it with anyone.",
				"One Time Passowrd(OTP) to create a new Salesman is @__password__@.This is valid for next 30 mins. Do not share it with anyone."
        );

        foreach ($templates as $temp) {

            $ret = $this->matchTemplate($message, $temp);
            if ($ret['status'] == 'success') {
                $ret['vars']['password'] = 'xxxx';
                foreach ($ret['vars'] as $key => $val) {
                    $temp = preg_replace('/@__' . $key . '__@/', $val, $temp, 1);
                }
                $message = $temp;
                break;
            }
        }

        return $message;
        //"Dear Sir,\nYour password has been reset to $password"
        /* if (strpos($message,"Dear Sir,\nYour password has been reset to ") !== false) {
          $messageArr = explode("Dear Sir,\nYour password has been reset to ", $message);
          $messageInsert = $messageArr[1];
          $messageInsert = "Dear Sir,\nYour password has been reset to "."XXXXX";
          }else{
          $messageInsert = $message;
          }
          return $messageInsert; */
    }

    function sendMessageViaRouteSMS($receivers,$message,$route,$switch=null){
		if(!is_array($receivers)) {
			$receivers = explode(",",$receivers);
		}
		if(empty($receivers) || empty($message)) {
			return;
		}
		$logger = $this->dumpLog('sendMessage', 'RouteSMS');
		
		$usrObj = ClassRegistry::init('User');
		if(SENDFLAG == '1'){
			$mobiles = $receivers;
			$mobiles = array_unique($mobiles);

			$num = intval(count($mobiles)/50);
			if(count($mobiles)%50 !=0) $num = $num + 1;
			for($i = 0; $i < $num; $i++){
				$arr = array_slice($mobiles,$i*50,50);
				$receivers = "";
				foreach($arr as $mobi){
					if(strlen($mobi) == 10)
					$mobi = "91" . $mobi;
					$receivers .= $mobi . ",";
				}
				$receivers = substr($receivers, 0, -1);
				if(empty($receivers))continue;

				$url = "http://sms6.routesms.com:8080/bulksms/bulksms";
				if($route == 'payone'){
					$adm = "username=payone&password=pay9876&type=0&dlr=1&destination=$receivers&source=PAYONE&message=".urlencode($message);
				}
				else if($route == 'smstdk'){
					$adm = "username=payone1&password=nbjght7&type=0&dlr=1&destination=$receivers&source=SMSTDK&message=".urlencode($message);
				}
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST,1);
				//exit;
				curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);

				curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
				curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
				curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				curl_setopt($ch, CURLOPT_TIMEOUT, 50);
				$Rec_Data = curl_exec($ch);
				curl_close($ch);

				$logger->info("Sent: $adm");
				$logger->info("Output: $Rec_Data");
				
				if(empty($Rec_Data)){
					$this->mailToUsers("Empty return in Routesms - $route", "Error: ".$Rec_Data,array('ashish@mindsarray.com'));
				}
				else {
					$ids = explode(",",$Rec_Data);
					$messageInsert = $message;//$this->checkIfResetPassMsg($message);

					foreach($ids as $id){
						$msgid = explode(":",$id);
						$mobs = explode("|",$msgid[0]);
							
						$mob = $mobs[1];
						$msgid = $msgid[1];

							
						if($mobs[0] == '1701'){//success
							$usrObj->query("insert into delivery_routesms(msg_id,message,mobile,status,code,route,created,date) values ('$msgid','".addslashes($messageInsert)."','$mob','SENT','".$mobs[0]."','$route','".date("Y-m-d H:i:s")."','".date('Y-m-d')."')");
							$logger->info("Output: Success");
						}
						else if($mobs[0] == '1025'){//insufficient credits
							$msg .= "Receivers: $mob<br/>";
							$msg .= $message;
							$mob = substr($mob,-10);

							$this->mailToUsers("Insufficient credits in Routesms - $route", $msg,array('chirutha@mindsarray.com','ashish@mindsarray.com'));
							$this->sendMessage('',$mob,$message,'fail-routesms-'.$route,null,$switch);
							$logger->info("Output: Failed due to insufficient credits, sending message again to ".'fail-routesms-'.$route);
						}
						else {//failed
							$msg .= "Receivers: $mob<br/>";
							$msg .= $message;
							$mob = substr($mob,-10);

							$this->mailToUsers("Problem in Routesms - $route, Error: ".$mobs[0], $msg,array('ashish@mindsarray.com'));
							$this->sendMessage('',$mob,$message,'fail-routesms-'.$route,null,$switch);
							$logger->info("Output: Failed due to code".$mobs[0].", sending message again to ".'fail-routesms-'.$route);
						}
					}
				}
			}
		}
	}

	function sendMessage($sender,$receivers,$message,$type=null,$app_name=null,$switch=null){
		/*if($_SERVER['SERVER_NAME'] == 'www.smstadka.com'){
			$this->sendMessageViaOtherServer($sender,$receivers,$message,$type,$app_name);
			return;
			}*/
		if(!is_array($receivers)) {
			$receivers = explode(",",$receivers);
		}

		$logger = $this->dumpLog('sendMessage', 'OutgoingSMSes');
		//$logger->info("$message :: " . implode(",",$receivers). " :: $type");
        $logger->info("$message :: " . implode(",",$receivers). " | type :: $type". "| switch :: $switch");

			
		$array = array();
		foreach($receivers as $rec){
			$rec = trim(substr($rec,-10));
			if(!empty($rec) && in_array(substr($rec,0,1),array("9","8","7")) && strlen($rec) == 10){
				$array[] = $rec;
			}
			else {
				/*$fh = fopen('/var/www/html/smstadka/smslog.txt',"a+");
				 fwrite($fh,date('Y-m-d H:i:s')."::$rec::$message::$type\n");
				 fclose($fh);*/
			}
		}
		$receivers = $array;
		$receivers = array_diff($receivers, array(DUMMY_USER));

		if(empty($receivers))return;
		$message = strip_tags(html_entity_decode($message,ENT_QUOTES));
		$message = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $message);
		$message = $this->makeSpamFree($message);
		$message = str_replace("\r\n","\n",$message);

		if(empty($switch) || !in_array($switch,array('247','tata','route','vfirst'))){
			if(count($receivers) > 1) $switch = '247';
			else {
				$router = intval(substr($receivers[0],-2))%5;
				if($router != 4){
					//$switch = '247';
                                        $switch = 'tata';
				}else{
                                        //$switch = 'tata';
					$switch = '247';
				}
			}
			
                    $this->redis = $this->redis_connect();
                    $switchType = $this->redis->get("smstadka_switch_type");
                    //$orginalType = $type;            
                    //$type = ($switchType == false || $switchType == "all" || empty($switchType)) ? $orginalType : $switchType;
                    $orginalType = $switch;
                    
                    //$type = 'tata';
                    if($type === '247')$switch = '247';
                    else if($type === 'tata')$switch = 'tata';
                    $switch = ($switchType == false || $switchType == "all" || empty($switchType)) ? $orginalType : $switchType;
		}

        $logger->info("$message :: " . implode(",",$array). " | type :: $type". "| switch :: $switch");
            
		
		if($type === 'shops' || $type === 'payone'){
			if($switch == '247')$this->sendMessageVia247SMS($sender,$receivers,$message,'payone',$switch);
			else if($switch == 'route')$this->sendMessageViaRouteSMS($receivers,$message,'payone',$switch);
			else if($switch == 'vfirst')$this->sendMessageViaVfirst($receivers,$message,$switch);
			else if($switch == 'tata')$this->sendMessageViaTata($receivers,$message,null,$switch);
		}
		else if($type === '247'){
			$this->sendMessageVia247SMS($sender,$receivers,$message,'payone',$switch);
		}
		else if($type === 'tata'){
			$this->sendMessageViaTata($receivers,$message,null,$switch);
		}
		else if($type === 'promo'){
			$this->sendMessageVia247SMS_Promotional($sender,$receivers,$message,$type);
		}
		else if($type == 'playwin' || $type == 'cricket' || $type == 'pac' || $type == 'pacM' || $type == 'retail' || $type == 'template'){
			//$this->sendMessageViaRouteSMS($receivers,$message,'smstdk');
			if($type == 'template'){
				$pckObj = ClassRegistry::init('Package');
				$vn = $pckObj->query("SELECT * FROM `virtual_number` WHERE mobile = '".$receivers[0]."' AND virtual_num in ('919821232431','919223178889')  AND timestamp > '".date("Y-m-d H:i:s",time()-(24*60*60))."' order by id desc LIMIT 0 , 1");
				if(!empty($vn)){
					$message = preg_replace(array('/7666888676/','/9223178889/','/Send SMS: HELP to 09223178889/'),array('9223178889','9223178889','Give a missed call to 02267242270'), $message);
					return $this->sendMessage($sender,$receivers,$message,'tata',null,$switch);
				}
			}

			$this->sendMessageVia247SMS($sender,$receivers,$message,$switch);
			//$this->sendMessageGupshup($receivers,$message,'tadka',1);
		}
		else if($type === 'modem'){
			//$this->sendMessageViaGSMModem($receivers,$message,2,$switch);
		}
		else if($type === 'fail-modem'){
			//$this->sendMessageGupshup($receivers,$message,'low',0);
		}
		else if($type === 'fail-gupshup'){
			//$this->sendMessageViaGSMModem($receivers,$message,3);
		}
		else if($type === 'fail-gupshup-payone'){
			//$this->sendMessageVia247SMS($sender,$receivers,$message,'payone');
			//$this->sendMessageViaGSMModem($receivers,$message,3);
		}
		else if($type === 'fail-gupshup-tadka'){
			//$this->sendMessageVia247SMS($sender,$receivers,$message);
		}
		else if($type === 'fail-routesms-payone'){
			/*if($switch == '247')$this->sendMessageViaVfirst($receivers,$message);
			 else if($switch == 'route')$this->sendMessageVia247SMS($receivers,$message,'payone');
			 else if($switch == 'vfirst')$this->sendMessageViaGSMModem($receivers,$message,3);*/
			
			/*if($switch == 'tata')$this->sendMessageViaVfirst($receivers,$message);
			else if($switch == '247')$this->sendMessageViaVfirst($receivers,$message);
			else if($switch == 'route')$this->sendMessageViaTata ($receivers,$message);
			else if($switch == 'vfirst')$this->sendMessageViaGSMModem($receivers,$message,3);*/
		}
		else if($type === 'fail-routesms-smstdk'){
			//$this->sendMessageViaGSMModem($receivers,$message,3);
		}
		else if($type === 'fail-vfirst'){
			/*if($switch == 'tata')$this->sendMessageViaGSMModem($receivers,$message,3,$switch);
			else if($switch == '247')$this->sendMessageViaGSMModem($receivers,$message,3,$switch);
			else if($switch == 'route')$this->sendMessageViaGSMModem($receivers,$message,3,$switch);
			else*/ if($switch == 'vfirst')$this->sendMessageViaTata($receivers,$message,null,$switch);
		}
		else if($type === 'fail-24x7sms'){
			$this->sendMessageViaRouteSMS($receivers,$message,'smstdk',$switch);
		}
		else if($type === 'fail-tatasms'){
			$this->sendMessageVia247SMS($receivers,$message,'smstdk',$switch);
		}
		else if($type === 'fail-24x7sms-payone'){
			if($switch == 'tata')$this->sendMessageViaRouteSMS($receivers,$message,'payone',$switch);
			else if($switch == '247')$this->sendMessageViaTata($receivers,$message,null,$switch);
			else if($switch == 'route')$this->sendMessageViaVfirst($receivers,$message,$switch);
			else if($switch == 'vfirst')$this->sendMessageViaRouteSMS($receivers,$message,'payone',$switch);
		}
		else if($type === 'fail-tatasms-payone'){
			if($switch == 'tata')$this->sendMessageVia247SMS($sender,$receivers,$message,'payone',$switch);
			else if($switch == '247')$this->sendMessageViaRouteSMS($receivers,$message,'payone',$switch);
			else if($switch == 'route')$this->sendMessageVia247SMS($sender,$receivers,$message,'payone',$switch);
			else if($switch == 'vfirst')$this->sendMessageVia247SMS($sender,$receivers,$message,'payone',$switch);
		}
		else {
			//$this->sendMessageViaGSMModem($receivers,$message,2,$switch);
			//$this->sendMessageNONTemplate($sender,$receivers,$message,$type,$app_name);
			//$this->sendMessageVia247SMS($sender,$receivers,$message);
		}
	}


	function sendMessageNONTemplate($sender,$receivers,$message,$type=null,$app_name=null){
		$receivers = $this->segregateMobileNumbers($receivers);

		$mobiles_nondnd = $receivers['nondnd'];
		$mobiles_opt = 	$receivers['opt'];
		$mobiles_others = 	$receivers['others'];

		$priority = 0;
		if($type === 'grp' || ($type === 'app' && $app_name === 'reminder')){
			$message = "From: " . $sender ."\n" . $message;
			$priority = 2;
		}
		else if($type === 'user' || $type === 'template'){
			$priority = 2;
		}
		else if($type === 'priority'){
			$priority = 3;
			if(!empty($app_name))$priority = $app_name;
		}
		else if($type === 'pacM' || $type === 'playwin')
		{
			$priority = 1;
		}
		$this->sendMessageVia247SMS($sender,$mobiles_nondnd,$message);
		$this->sendMessageVia247SMS($sender,$mobiles_opt,$message);

		$this->sendMessageViaGSMModem($mobiles_others,$message,$priority);

		//$this->sendMessageGupshup($mobiles_nondnd,$message,'low',1);
		//$this->sendMessageViaMOS($mobiles_nondnd,$message,'non-temp');
	}

	function sendMessageViaOtherServer($sender,$receivers,$message,$type=null,$app_name=null){
		$url = SERVER_BACKUP . 'users/sendMsgMails';
		$data['sender'] = $sender;
		if(is_array($receivers))
		$data['mobile'] = implode(",",$receivers);
		else $data['mobile'] = $receivers;
		$data['sms'] = $message;
		$data['root'] = $type;
		$data['app_name'] = $app_name;
		if(!empty($data)){
			$this->curl_post_async($url,$data);
		}
	}

	function sendMailsViaOtherServer($subject,$mail_body,$emails=null){
		$url = SERVER_BACKUP . 'users/sendMsgMails';
		$data['mail_subject'] = $subject;
		$data['mail_body'] = $mail_body;
		if(!empty($emails)){
			$data['emails'] = implode(",",$emails);
		}
		if(!empty($data)){
			$this->curl_post_async($url,$data);
		}
	}

	function initCronsPackage ($pack_id) {
		$pckObj = ClassRegistry::init('Package');
		$pckObj->recursive = -1;
		$pckData = $pckObj->find('first', array('fields' => array('Package.frequency','Package.name'),'conditions' => array('Package.id' => $pack_id)));
		$freq = $pckData['Package']['frequency'];

		$starttime = '';

		if(empty($freq)){ //As it happens
			$freq = 'NULL';
			$starttime = 'NULL';
		}
		else {
			$starttime = date('Y-m-d');
			$time = $pckObj->query("select time from packages_times where package_id=$pack_id and number=1");
			$time = $time['0']['packages_times']['time'];
			$str =  "$starttime" . " " . $time;
			$starttime = $str;
		}

		$data = $pckObj->query("select * from crons_packages where package_id = $pack_id");
		if(empty($data)){
			$pckObj->query("insert into crons_packages(package_id,lefttogo,flag,starttime) values ($pack_id,$freq,0,'$starttime')");
		}
		else {
			$pckObj->query("update crons_packages set lefttogo=$freq,flag=0,starttime='$starttime' where package_id = $pack_id");
		}
	}

	function cronsPackageUpdate($pack_id){
		$pckObj = ClassRegistry::init('Package');
		$pckObj->recursive = -1;
		$data = $pckObj->query("select * from crons_packages where package_id = ".$pack_id);

		$flag = 0;

		if(!empty($data['0']['crons_packages']['lefttogo'])){
			$lefttogo = $data['0']['crons_packages']['lefttogo'] - 1;
			$starttime = 'NULL';

			if($lefttogo > 0) {
				$pckData = $pckObj->find('first', array('fields' => array('Package.frequency'),'conditions' => array('Package.id' => $pack_id)));
				$num = $pckData['Package']['frequency'] - $lefttogo + 1;

				$starttime = date('Y-m-d');
				$time = $pckObj->query("select time from packages_times where package_id=$pack_id and number=$num");
				$ntime = $time['0']['packages_times']['time'];
				if($ntime != "00:00:00"){
					$str =  "$starttime" . " " . $ntime;
					$starttime = $str;
				}
			}
			$pckObj->query("update crons_packages set lefttogo=$lefttogo,flag=$flag,starttime='$starttime' where package_id = $pack_id");
		}
		else {
			$flag = 1;
		}

		return $flag;
	}

	function printArray($txt){
		echo  '<pre>';
		print_r($txt);
		echo '</pre>';
	}

	function getUserDataFromMobile($mobile)
	{
		$userObj = ClassRegistry::init('User');
		$query = "SELECT * FROM users WHERE mobile = $mobile";
		$res_arr = $userObj->query($query);

		return $res_arr['0']['users'];
	}

	function getUserDataFromId($id)
	{
		$userObj = ClassRegistry::init('User');
		$query = "SELECT * FROM users WHERE id = $id";
		$res_arr = $userObj->query($query);

		return $res_arr['0']['users'];
	}

	function getMobileDetails($mobileNumber)
	{
		$mobNum = substr($mobileNumber,0,4);
		$query = "select mna.area_name, mns.opr_name, mn.area, mns.opr_code from mobile_numbering AS mn LEFT JOIN mobile_numbering_area as mna ON mn.area = mna.area_code LEFT JOIN mobile_numbering_service AS mns ON mn.operator = mns.opr_code WHERE mn.number = ".$mobNum;
		$userObj = ClassRegistry::init('User');

		$data = $userObj->query($query);
		$area_name = $data['0']['mna']['area_name'];
		$opr_name = $data['0']['mns']['opr_name'];

		$ret_arr = array($area_name, $opr_name, $data['0']['mn']['area'], $data['0']['mns']['opr_code']);
		return $ret_arr;
	}

	function xml2array($contents, $get_attributes=1, $priority = 'tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;//Hmm...

		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();

		$current = &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble

			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.

			$result = array();
			$attributes_data = array();

			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}

			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}

			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;

					$current = &$current[$tag];

				} else { //There was another element with the same tag name

					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;

						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}

							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}

		return($xml_array);
	}

	function sendTweet($twitter_id,$message){
		$pckUserObj = ClassRegistry::init('PackagesUser');
		$pckUserObj->recursive = -1;

		$users = $pckUserObj->find('all', array(
			'fields' => array('User.mobile','User.id','packages_twitter.name','PackagesUser.package_id'),
			'conditions' => array('PackagesUser.active' => '1'),
			'joins' => array(
		array(
					'table' => 'packages_twitter',
					'type' => 'inner',
					'conditions'=> array('packages_twitter.package_id = PackagesUser.package_id','packages_twitter.twitter_id = '.$twitter_id)
		),
		array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'inner',
					'conditions'=> array('PackagesUser.user_id = User.id')
		),
		array(
					'table' => 'packages',
					'alias' => 'Package',
					'type' => 'inner',
					'conditions'=> array('PackagesUser.package_id = Package.id','Package.toshow = 1')
		)
		)));

		$mobiles = array();
		$content = '';

		foreach($users as $user){
			$name = $user['packages_twitter']['name'];
			$content = "*Tweets*\n" . $name . ": " . $message;

			$insID = $this->logUpdate($user['PackagesUser']['package_id'],null,null,nl2br($content),$user['User']['mobile'],$user['User']['id']);
			if($user['User']['mobile'] != DUMMY_USER)
			$mobiles[$insID] = $user['User']['mobile'];
		}

		if(!empty($mobiles))
		$this->sendMessage('',$mobiles,$content,'pacM');

	}

	function mailToAdmins($subject, $mail_body){
		/*if($_SERVER['SERVER_NAME'] == 'www.smstadka.com'){
			$this->sendMailsViaOtherServer($subject,$mail_body);
			return;
			}*/
		//$userObj = ClassRegistry::init('User');
		//$data = $userObj->find('all', array('fields' => array('User.mobile','User.email'),'conditions' => array('User.group_id' => $this->getGroupId('admin'))));
		$emails = array('ashish@mindsarray.com','chirutha@mindsarray.com','anurag@mindsarray.com','vinit@mindsarray.com');
		if(SENDFLAG == '1'){
			foreach($emails as $email){
				//$this->sendMessage(SMS_SENDER,$user['User']['mobile'],$mail_body,'dnd');
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				$headers .= "From: admin@smstadka.com\r\n";
				$headers .= "Reply-To: admin@smstadka.com\r\n";

				$m_result = mail($email,$subject,$mail_body,$headers);
				$content = "\n".date('Y-m-d H:i:s')." - mailtouser: Emails -".$email." sub-".$subject." msg-".$mail_body." header-".$headers." response- ".json_encode($m_result);
				file_put_contents("/var/log/apps/maillog", $content, FILE_APPEND | LOCK_EX);
			}
		}
	}

	function smsToAdmins($message,$mobile){
		$nums = array('9819032643','9892471157','9004387418','9820595052','9833770118');
		$nums = array_diff($nums, array($mobile));
		if(SENDFLAG == '1'){
			$message = "Message from $mobile\n" . $message;
			$this->sendMessage('',$nums,$message,'priority',3);
		}
	}
        
        function mail_from_other_app($subject, $mail_body, $emails, $sender){
            if(!is_array($emails)) {
                    $emails = explode(",",$emails);
            }
            foreach($emails as $email){	
                    //$this->sendMessage(SMS_SENDER,$user['User']['mobile'],$mail_body,'dnd');
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                    $headers .= "From: $sender\r\n";
                    $headers .= "Reply-To: $sender\r\n";

                    $m_result = mail(trim($email),$subject,$mail_body,$headers);
                    $content = "\n".date('Y-m-d H:i:s')." - mailtouser: Emails -".$email." sub-".$subject." msg-".$mail_body." header-".$headers." response- ".json_encode($m_result);
                    file_put_contents("/var/log/apps/maillog_other_app", $content, FILE_APPEND | LOCK_EX);
            }
        }

	function mailToUsers($subject, $mail_body, $emails, $notif=null){
		/*if($_SERVER['SERVER_NAME'] == 'www.smstadka.com'){
			$this->sendMailsViaOtherServer($subject,$mail_body,$emails);
			return;
			}*/
		if(SENDFLAG == '1' && empty($notif)){
			if(!is_array($emails)) {
				$emails = explode(",",$emails);
			}
			
			$val = array_search('tadka@mindsarray.com', $emails);
			if($val !== false){
				unset($emails[$val]);
				$emails[] = 'ashish@mindsarray.com';
				$emails[] = 'chirutha@mindsarray.com';
			}
			
			foreach($emails as $email){	
				//$this->sendMessage(SMS_SENDER,$user['User']['mobile'],$mail_body,'dnd');
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				$headers .= "From: admin@smstadka.com\r\n";
				$headers .= "Reply-To: admin@smstadka.com\r\n";

				$m_result = mail(trim($email),$subject,$mail_body,$headers);
				$content = "\n".date('Y-m-d H:i:s')." - mailtouser: Emails -".$email." sub-".$subject." msg-".$mail_body." header-".$headers." response- ".json_encode($m_result);
				file_put_contents("/var/log/apps/maillog", $content, FILE_APPEND | LOCK_EX);
			}
		}
		else {
			$url = "http://ec2-54-224-133-255.compute-1.amazonaws.com:8000/fire-event/";
			$url .= urlencode("<b>$subject</b><br/>$mail_body");
			$this->curl_post_async($url,null,'GET');
		}
	}

	function authenticatedMailToUsers($emails, $subject, $mail_body, $from, $attachments = null){
		$url = SITE_NAME . 'groups/shootMail';
		$params['sub'] = $subject;
		$params['body'] = $mail_body;
		$params['from'] = $from;

		if(!empty($attachments)){
			$params['path'] = implode(",",$attachments);
		}
		foreach($emails as $email){
			$params['email'] = trim($email);
			$this->curl_post_async($url,$params);
		}
	}

	function br2newline( $input ) {
		$out = str_replace( "<br>", "\n", $input );
		$out = str_replace( "<br/>", "\n", $out );
		$out = str_replace( "<br />", "\n", $out );
		$out = str_replace( "<BR>", "\n", $out );
		$out = str_replace( "<BR/>", "\n", $out );
		$out = str_replace( "<BR />", "\n", $out );
		return $out;
	}

	function getHTMLFromNode($node){
		$domDocument1 = new DOMDocument();
			
		foreach($node->childNodes as $childNode){
			$domDocument1->appendChild($domDocument1->importNode($childNode, true));
		}

		return trim(strip_tags($domDocument1->saveHTML()));
	}

	function RSS_Tags($item, $type)
	{
		$y = array();
		$tnl = $item->getElementsByTagName("title");
		$tnl = $tnl->item(0);
		$title = $tnl->firstChild->textContent;

		$tnl = $item->getElementsByTagName("link");
		$tnl = $tnl->item(0);
		$link = $tnl->firstChild->textContent;

		$tnl = $item->getElementsByTagName("pubDate");
		$tnl = $tnl->item(0);
		$date = $tnl->firstChild->textContent;

		$tnl = $item->getElementsByTagName("description");
		$tnl = $tnl->item(0);
		$description = $tnl->firstChild->textContent;

		$y["title"] = html_entity_decode($title,ENT_QUOTES);
		$y["link"] = $link;
		$y["date"] = date('Y-m-d H:i:s', strtotime($date));
		$y["description"] = html_entity_decode($description,ENT_QUOTES);
		$y["type"] = $type;

		return $y;
	}


	function RSS_Channel($channel)
	{
		$items = $channel->getElementsByTagName("item");

		// Processing channel

		$y = $this->RSS_Tags($channel, 0);		// get description of channel, type 0
		array_push($this->RSS_Content, $y);

		// Processing articles

		foreach($items as $item)
		{
			$y = $this->RSS_Tags($item, 1);	// get description of article, type 1
			array_push($this->RSS_Content, $y);
		}
	}

	function RSS_Retrieve($url)
	{
		$doc  = new DOMDocument();
		$doc->load($url);

		$channels = $doc->getElementsByTagName("channel");

		$this->RSS_Content = array();

		foreach($channels as $channel)
		{
			$this->RSS_Channel($channel);
		}

	}


	function RSS_RetrieveLinks($url)
	{
		$doc  = new DOMDocument();
		$doc->load($url);

		$channels = $doc->getElementsByTagName("channel");

		$this->RSS_Content = array();

		foreach($channels as $channel)
		{
			$items = $channel->getElementsByTagName("item");
			foreach($items as $item)
			{
				$y = $this->RSS_Tags($item, 1);	// get description of article, type 1
				array_push($this->RSS_Content, $y);
			}

		}

	}


	function RSS_Links($url, $size = 15,$mode)
	{
		$page = "";

		$this->RSS_RetrieveLinks($url);
		if($size > 0)
		$recents = array_slice($this->RSS_Content, 0, $size + 1);

		foreach($recents as $article)
		{
			$type = $article["type"];
			if($type == 0) continue;
			$title = $article["title"];
			$link = $article["link"];
			if($mode)
			{
				$page .= "<li><a href=\"$link\">$title</a></li>\n";
			}
			else
			{

				$page .= "# ".$title."<br>\n";
			}
		}

		$page .="\n";

		return $page;

	}

	function RSS_Display($url, $size = 15, $site = 0, $withdate = 0)
	{
		$opened = false;
		$page = "";
		$site = (intval($site) == 0) ? 1 : 0;

		$this->RSS_Retrieve($url);
		if($size > 0)
		$recents = array_slice($this->RSS_Content, $site, $size + 1 - $site);

		return $recents;
	}

	function extractPassword($mobile)
	{
		$userObj = ClassRegistry::init('User');
		$sysPass = $userObj->find('first', array('fields' => array('User.syspass'),'conditions' => array('User.mobile' => $mobile)));
		//$count = $userObj->find('count',array('conditions' => array('User.mobile' => $mobile)));

		if(!empty($sysPass)) return $sysPass['User']['syspass'];
		return false;

	}

	function getBitlyUrl($url){
		$link = 'http://api.bit.ly/v3/shorten?login='.BITLY_USER.'&apiKey='.BITLY_KEY.'&longUrl='.$url.'&format=txt';
		//echo $link; exit;
		$ch = curl_init($link);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$data = curl_exec ($ch);
		curl_close ($ch);
		return $data;
	}


	function executeCouponEnteries($coupon_id,$user_id,$mobile_number,$product_id,$serialNum,$outside_flag,$param = null){
		//echo "<br/>Enters in $coupon_id  -- $user_id --  $mobile_number  -- $product_id  -- $serialNum -- $outside_flag  -- $param";
		if($coupon_id != -1)
		$this->retailerCouponSold($coupon_id,$user_id);

		$userObj = ClassRegistry::init('User');
		$prodInfo = $this->getProductInfo($product_id);

		$this->userCyclicPackageInsert($product_id,null,$user_id);
		$data = $userObj->query("SELECT id,end,trial,count FROM products_users WHERE product_id = $product_id and user_id = $user_id and active = 1 and end is not null and end != '0000-00-00 00:00:00'");
		if(empty($data)){
			$start = date('Y-m-d H:i:s');
			$end = date('Y-m-d H:i:s', strtotime( '+ '.$prodInfo['Product']['validity'].' days'));
			if($outside_flag == 1) $end = null;

			$retailer_flag = 0;
			if($coupon_id == -1)
			$retailer_flag = 1;

			$productUserObj = ClassRegistry::init('ProductsUser');
			$productUserObj->recursive = -1;
			$productUserObj->create();
			$ProdData['ProductsUser']['product_id'] = $product_id;
			$ProdData['ProductsUser']['user_id'] = $user_id;
			$ProdData['ProductsUser']['active'] = 1;
			$ProdData['ProductsUser']['start'] = $start;
			$ProdData['ProductsUser']['end'] = $end;
			$ProdData['ProductsUser']['retailer_flag'] = $retailer_flag;
			$productUserObj->save($ProdData);
			//$this->userCyclicPackageInsert($product_id,null,$user_id);
			$prod_userid = $productUserObj->id;
			//$this->makeOptIn247SMS($mobile_number,1);
		}
		else {
			if($data['0']['products_users']['trial'] == 1 && $data['0']['products_users']['count'] == 1){
				$end = date('Y-m-d H:i:s', strtotime( '+ '.$prodInfo['Product']['validity'].' days'));
				//$this->userCyclicPackageInsert($product_id,null,$user_id);
			}
			else {
				$end = date('Y-m-d H:i:s', strtotime( $data['0']['products_users']['end'] . '+ '.$prodInfo['Product']['validity'].' days'));
			}
			if($outside_flag == 1) $end = null;

			$retailer_flag = '';
			if($coupon_id == -1)
			$retailer_flag = ', retailer_flag = (retailer_flag + 1)';

			$userObj->query("UPDATE products_users SET end = '$end', count = (count + 1) $retailer_flag WHERE id = " . $data['0']['products_users']['id']);
			$prod_userid = $data['0']['products_users']['id'];
		}

		$sms = $this->createMessage("PRODUCT_SUBSCRIPTION",array($prodInfo['Product']['name'],date('d-M-Y',strtotime($end)),"0".VIRTUAL_NUMBER,$prodInfo['Product']['code'],"0".VIRTUAL_NUMBER));

		if($coupon_id != -1){
			$RetailersCouponObj = ClassRegistry::init('RetailersCoupon');
			$RetailersCouponObj->recursive = -1;
			$retailer = $RetailersCouponObj->find('first',array('fields' => array('Retailer.*','Salesman.mobile','coupons.expiry_date','coupons.serialNumber'), 'conditions' => array('coupon_id' => $coupon_id),
				'joins' => array(array(
						'table' => 'retailers',
						'alias' => 'Retailer',
						'type' => 'inner',
						'conditions' => array('RetailersCoupon.retailer_id = Retailer.id') 		
			),
			array(
						'table' => 'salesmans',
						'alias' => 'Salesman',
						'type' => 'left',
						'conditions' => array('Salesman.id = Retailer.salesman_id') 		
			),
			array(
						'table' => 'coupons',
						'alias' => 'coupons',
						'type' => 'left',
						'conditions' => array('coupons.id = RetailersCoupon.coupon_id') 		
			)
			)
			));

			$mail['subject'] = "Retail Product " . $prodInfo['Product']['name'] . " subscribed";
			if(empty($retailer)) {
				$mail['body'] = "User $mobile_number subscribed it. Serial Number: ".$serialNum.". Retailer: Unknown";
			}
			else {
				$mail['body'] = "User $mobile_number subscribed it. Retailer: " . $retailer['Retailer']['name'] . " <br>";
				$mail['body'] .= $retailer['Retailer']['shopname'] . "<br>";
				$mail['body'] .= $retailer['Retailer']['address'] . "<br>";
				if($retailer['Retailer']['toshow'] == 0){
					$dist = $this->Shop->getShopDataById($retailer['Retailer']['parent_id'],DISTRIBUTOR);
					if(empty($dist['company']))$dist['company'] = 'SMSTadka';
					$mail['body'] .= "Distributor: ".$dist['company'];
				}
			}
			/*if(!empty($retailer['Salesman']['mobile'])){
				$sms_salesman = $mail['subject'] . "\n" . $mail['body'];
				$this->sendMessage(SMS_SENDER,$retailer['Salesman']['mobile'],$sms_salesman,'market');
				}*/
		}
		$data = array();
		if($outside_flag == 0){
			$data['sms'] = $sms;
			$data['mobile'] = $mobile_number;
			$data['root'] = 'template';
		}

		$url = SERVER_BACKUP . 'users/sendMsgMails';
		if(isset($mail['subject'])){
			//$data['mail_subject'] = $mail['subject'];
			$data['mail_body'] = $mail['body'];
			if(!empty($retailer['Retailer']['id'])){
				$data['retailer_id'] = $retailer['Retailer']['id'];
			}
		}
		if(!empty($data)){
			$this->curl_post_async($url,$data);
		}

		if($product_id == WOI_PRODUCT){
			$params = array();
			$url = SERVER_BACKUP . 'partners/contentPartnerSubscription';
			$params['user'] = $user_id;
			$params['mobile'] = $mobile_number;
			$params['product'] = $product_id;
			$params['prod_userid'] = $prod_userid;
			$params['param'] = $param;
			$this->curl_post_async($url,$params);
		}
		
		// if coupon is pay1 recharge coupon then
		if(in_array($product_id,explode(",",PAY1_RECHARGE_CARDS))){
		//if($product_id == PAY1_RECHARGE_CARD_30 || $product_id == PAY1_RECHARGE_CARD_50 || $product_id == PAY1_RECHARGE_CARD_125 || $product_id == PAY1_RECHARGE_CARD_150){
			$arg = array();
			$params = explode("*",$param);
			$arg['operator'] = $params[0] ;
			$arg['amount'] = empty($params[1])?"":$params[1];

			$arg['coupon_id'] = $coupon_id  ;//$this->General->transactionUpdate(TRANS_ADMIN_DEBIT,$amount,$operator_id,null);
			$arg['mobile_no'] = $mobile_number;
			$arg['cust_id'] = $mobile_number ;
			$arg['type'] = "MOB";
			$arg['product_id'] = $product_id ;
			$arg['serialNumber'] = $retailer['coupons']['serialNumber'];
			$arg['coupon_amt'] = $prodInfo['Product']['price'];
			$arg['card_expiry_date'] = $retailer['coupons']['expiry_date'];//Static @TODO Change & fetch from DB
			//echo "<pre>".print_r($arg)."</pre>";
			$res = $this->rechargeByCoupon($arg);
		}

		return $prod_userid;
	}


	function rechargeByCoupon($params){//couponId,opreator,amt,mobileNo,recharge_type

		$response = array();
		$transId = $params['coupon_id'];//$this->General->transactionUpdate(TRANS_ADMIN_DEBIT,$amount,$operator_id,null);
		$operator_id = $params['operator'];
		$mob_no = $params['mobile_no'];
		$amount = $params['amount'];
		$cust_id = $params['cust_id'];
		$type = $params['type'];
		$product_id = $params['product_id'];
		$card_expiry_date = $params['card_expiry_date'];
		$card_serial_number = $params['serialNumber'];
		if($product_id == 40)$card_serial_number = "";
		
		$coupon_amt = $params['coupon_amt'];
		$fname = $_SERVER['DOCUMENT_ROOT'] . "/coupon_recharge_log.txt";
		$fh = fopen($fname,'a+');
			
		$operators = array('1' => '15','2' => '2', '3' => '7','4'=>'8','5'=>'4','6'=>'5','7'=>'3','8'=>'31','9'=>'9','10'=>'10','11'=>'1','12'=>'11');

		/*if($product_id == PAY1_RECHARGE_CARD_30 ){
			$coupon_amt = 30;
		}elseif($product_id == PAY1_RECHARGE_CARD_50){
			$coupon_amt = 50;
		}
		elseif($product_id == PAY1_RECHARGE_CARD_125){
			$coupon_amt = 125;
		}
		elseif($product_id == PAY1_RECHARGE_CARD_150){
			$coupon_amt = 150;
		}*/
		
		$err = "";
		//$exDate = new Date($card_expiry_date);
		$exDate  = strtotime($card_expiry_date);
		$today    = time();
		
		if(empty($amount)) $amount = $coupon_amt;
		//check wrong params
		if(empty($amount) || empty($operator_id)){
			$err = "Wrong SMS: Please send operator with the recharge code.\nE.g To recharge for vodafone, send SMS as code*1\n For help, give misscall to 02267242270";
		}
		else if($amount > $coupon_amt){//amount is wrong
			$err = "Wrong amount transaction. Amount should be less than $coupon_amt";
		}elseif($today > ($exDate+24*60*60) ){//card expiry
			$err = "Card ($card_serial_number) expired. Contact customer care for help. Give misscall to 02267242270";
		}
		else if(!isset($operators[$operator_id])){
			$err = "Wrong operator code. For help, give misscall to 02267242270";
		}

		if($err != ""){
			$response['status'] = 'failure';
			$response['description'] = $err;

			$rechargeLog = ClassRegistry::init('RechargeLog');
			$rechargeLog->query("update retailers_coupons set user_id = NULL where coupon_id = $transId ");// open coupon after reverse

			fwrite($fh, date("Y-m-d H:i:s")."::$card_serial_number::$err\n");

			fclose($fh);
			$this->sendMessage(null,array($mob_no),$err,'tata');
			return $response;
		}


		$rechargeLogId = $this->rechargeLogUpdate( null,$transId,0,$amount,null,$cust_id,$type,$mob_no,"0","Successful transaction .",null,2 );//by = byCoupon(1)/online(2)
		$operator = $operators[$operator_id];
		//call Pay1 Recharge Api
		$url = PAY1_API_URL;//@TODO change
		$partnerId = PAY1_API_PARTNER_ID;//@TODO change
		$operation_type = PAY1_API_METHOD_ID_RECHARGE;//@TODO change
			
		$hash = sha1($partnerId.$rechargeLogId.PAY1_API_ACCESS_PASS);//@TODO change static pass
		$params = "operator=$operator&mobile=".$mob_no."&amount=".$amount."&operation_type=".$operation_type."&partner_id=".$partnerId."&trans_id=".$rechargeLogId."&hash_code=".urlencode($hash);

		$pars = array('operator'=>$operator,'mobile'=>$mob_no,'amount'=>$amount,'operation_type'=>$operation_type,'partner_id'=>$partnerId,'trans_id'=>$rechargeLogId,'hash_code'=>$hash);

		$out = $this->curl_post($url,$pars);
		if(!$out['success']){
			if($out['timeout']){
				$res = "Recharge for coupon ($card_serial_number) is not successful due to some error. Please try again after some time.\nFor help, give misscall to 02267242270";
					
				//$res = "Recharge unsuccessful due to some error. Please try again after some time.";
				$response['status'] = 'failure';
				$response['description'] = $res;

				fwrite($fh, date("Y-m-d H:i:s")."::$card_serial_number::$res\n");

				// reverse if instant failure
				$rechargeLog = ClassRegistry::init('RechargeLog');
				$rechargeLog->query("update retailers_coupons set user_id = NULL where coupon_id = $transId");// open coupon after reverse
				$rechargeLog->query("delete from recharge_log where trans_id = $transId and recharge_by = 2");

				$this->sendMessage(null,array($mob_no),$res,'tata');
				return $response;
			}
		}
		fwrite($fh, date("Y-m-d H:i:s")."::$card_serial_number::".$out['output']."\n");
			
		$Rec_Data = $out['output'];
		$jsonData = json_decode($Rec_Data);
		//fwrite($fh, date("Y-m-d H:i:s")."::$card_serial_number::".$out['output']."\n");
		if(!empty($jsonData) && $jsonData->status == 'failure'){
			$descr = "";

			if( in_array($jsonData->errCode , array('E008','E009','E010','E011','E012','E013','E014'))){
				$descr = $jsonData->description;
				$res = "Recharge for coupon ($card_serial_number) is not successful.\nError : ".$descr;
					
			}else{
				//Email to smstadka admin
				$emailSub = "Pay1 Recharge Cards Error !!!";
				$emailBody = "Error Code : ".$jsonData->errCode."<br>"."Description : ".$Rec_Data;
					
				//$this->mailToAdmins($emailSub, $emailBody);
				$res = "Recharge for coupon ($card_serial_number) is not successful. Please try again after some time.\nFor help, give misscall to 02267242270";
			}


			$response['status'] = 'failure';
			$response['description'] = $res;

			fwrite($fh, date("Y-m-d H:i:s")."::$card_serial_number::$res\n");
			// reverse if instant failure
			$rechargeLog = ClassRegistry::init('RechargeLog');
			$rechargeLog->query("update retailers_coupons set user_id = NULL where coupon_id = $transId");// open coupon after reverse
			$rechargeLog->query("delete from recharge_log where trans_id = $transId and recharge_by = 2");

			$this->sendMessage(null,array($mob_no),$res,'tata');

		} else {//
			$pay1_id = $jsonData->ref_code;
			$rechargeLog = ClassRegistry::init('RechargeLog');
			fwrite($fh, date("Y-m-d H:i:s")."::$card_serial_number::$pay1_id\n");

			$rechargeLog->query("update recharge_log set pay1_transid = $pay1_id where trans_id = $transId");// open coupon after reverse

			$msg = "You have successfully done a recharge on mobile $mob_no of Rs $amount by your coupon no. $card_serial_number\nYou will get this recharge soon.";
			$this->sendMessage(null,array($mob_no),$msg,'tata');
			//email to admin recharge successful
			if(empty($jsonData)){
				$emailSub = "Pay1 recharge card activated with empty response from server!!";
			}
			else $emailSub = "Pay1 recharge card activated !!";
			$emailBody = "Coupon SerialNumber $card_serial_number successfully used to recharge on mobile $mob_no of Rs $amount .";
			$this->mailToAdmins($emailSub, $emailBody);
			$response['status'] = 'success';
		}

		fclose($fh);
		//print_r($response);
		return $response;
	}


	function matchTemplate($sms, $template,$varStart="@__",$varEnd="__@"){
		$sms = trim($sms);
		$template = trim($template);
		$template = str_replace($varStart,"|~|",$template);
		$template = str_replace($varEnd,"|~|",$template);

		$t=explode("|~|",$template);

		$vars = array();
		$ret = true;
		$i = 0;
		$start = 0;
		$log = "";

		$out['sms'] = $sms;
		for($i=0;$i<=count($t);$i=$i+2){
			if($t[$i] == null){
				if($i != 0){
					$vars[$start] = $sms;
					$vars[$t[$i-1]] = $sms;
				}
			}
			else {
				$log .= "Checking ".$t[$i];
				$index = strpos($sms,$t[$i]);
				$log .= ": $index\n";
				if($index === false){
					$ret = false;
					break;
				}
				else {
					$var = substr($sms,0,$index);
					if($i != 0){
						$vars[$start] = trim($var);
						$vars[$t[$i-1]] = trim($var);
						$start++;
					}
					$sms = substr($sms,$index+strlen($t[$i]));
				}
			}
		}

		if(count($t) == 1 && $out['sms'] != $template){
			$ret = false;
		}

		if($ret){
			$out['status'] = 'success';
			$out['vars'] = $vars;
		}
		else {
			$out['status'] = 'failure';
			$out['vars'] = $vars;
		}
		$out['logs'] = $log;
		return $out;
	}

	function logData($file,$data){
		$fh = fopen($file,'a+');
		fwrite($fh,"$data\n");
		fclose($fh);
	}

	function curl_post($url, $params=null,$type='POST',$timeout=30,$connect_timeout=10)
	{
		if(empty($params)){
			$post_string = "";
		}
		else if(is_array($params)){
			foreach ($params as $key => &$val) {
				if (is_array($val)) $val = implode(',', $val);
				if($key != 'uva')$post_params[] = $key.'='.urlencode($val);
				else $post_params[] = $val;
			}
			$post_string = implode('&', $post_params);
		}
		else {
			$post_string = $params;
		}

		$ch = curl_init($url);
		if($type == 'POST'){
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		}
		else {
			curl_setopt($ch, CURLOPT_POST,0);
		}

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		$out = trim(curl_exec($ch));

		$info = curl_getinfo($ch);

		if($info['connect_time'] > 10 || $info['total_time'] > 10){
			$this->logData('/var/www/html/smstadka/curl_log.txt',"[".date('Y-m-d H:i:s')."] Took ".$info['total_time']." seconds to send request of $post_string to $url & took ".$info['connect_time']." seconds to connect");
		}
		if(!curl_errno($ch)){
			curl_close($ch);
			return array('output'=>$out,'success'=>true,'timeout'=>false);
		}
		else {
			$errno = curl_errno($ch);
			curl_close($ch);
			$this->logData('/var/www/html/smstadka/curl_log_error.txt',"[".date('Y-m-d H:i:s')."] Curl Error $errno: Took ".$info['total_time']." seconds to send request of $post_string to $url & took ".$info['connect_time']." seconds to connect");
			if(in_array($errno,array(6,7)) || $info['connect_time'] > $connect_timeout){
				return array('output'=>$out,'success'=>false,'timeout'=>true);//connection timeout
			}
			else {
				return array('output'=>$out,'success'=>false,'timeout'=>false);//timeout
			}
		}
	}

	function curl_post_async($url, $params=null,$type='POST')
	{
		foreach ($params as $key => &$val) {
			if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}
		$post_string = implode('&', $post_params);

		$parts=parse_url($url);

		$fp = pfsockopen($parts['host'],
		isset($parts['port'])?$parts['port']:80,
		$errno, $errstr, 30);

		if(!$fp){
			$this->sendMails("Couldn't open a socket to ".$url." (".$errstr.")","Couldn't open a socket to ".$url." (".$errstr.")",array('tadka@mindsarray.com'));
			return array('status'=>'failure','errno'=>$errno,'error'=>$errstr);
		}
		else {
			if($type == "GET" && !empty($post_string))
			$parts['path'] .= '?'.$post_string;
			$out = "$type ".$parts['path']." HTTP/1.1\r\n";
			$out.= "Host: ".$parts['host']."\r\n";
			$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
			$out.= "Content-Length: ".strlen($post_string)."\r\n";
			$out.= "Connection: Close\r\n\r\n";
			if ($type == 'POST' && isset($post_string)) $out.= $post_string;

			fwrite($fp, $out);
			fclose($fp);
			return array('status'=>'success');
		}
	}

	function mobileValidate($phone){
		$err = '';
		if(!ereg("^[7-9]{1}[0-9]{9}$", $phone)) {
			$err = 1;
		}
		return $err;
	}

	function priceValidate($price)
	{
		if(is_numeric($price))
		return sprintf('%01.2f', round($price, 2));
		else
		return '';
	}

	function getRetailerSignature($retailer_id){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->recursive = -1;
		$sign = $retailObj->find('first',array('fields' => array('Retailer.signature', 'Retailer.signature_flag'), 'conditions' => array('Retailer.id' => $retailer_id)));
		return $sign;
	}


	function getPNRInfo($pnr){
		//Configure::write('debug', 2);

		$url = "http://pnrapi.appspot.com/$pnr";
		$user_agent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$str = trim(curl_exec($ch));
		$data = str_replace('\'','"',$str);
		$data=json_decode($data,true);
		if($data['status'] == "OK"){
			$data = $data['data'];
			$data['boarding'] = $data['board'];
			unset($data['board']);
			$data['journey_date'] = date('Y-m-d', strtotime($data['travel_date']['date']));
			unset($data['travel_date']);
			unset($data['alight']);
			$data['train_info'] = $str;

			$data['num_passengers'] = count($data['passenger']);
			$data['train_number'] = substr($data['train_number'],1);

			$cancelFlag = true;
			$cnfFlag = true;
			$val_cnfFlag = true;
			foreach($data['passenger'] as $psnger){
				$passengers = array();
				$passengers['booking_status'] = $psnger['seat_number'];
				$passengers['current_status'] = $psnger['status'];
				if($passengers['current_status'] != "Can/Mod"){
					$cancelFlag = false;
					if(preg_match("/(W\/L|CNF|Confirmed|RAC)/i", $passengers['current_status'])){
						$cnfFlag = false;
					}
					if(strtoupper($passengers['current_status']) != "CNF" && strtoupper($passengers['current_status']) != 'CONFIRMED'){
						$val_cnfFlag = false;
					}
				}
				$data['passengers'][] = $passengers;
			}
			unset($data['passenger']);
			//$data['tkt_status'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item($num*$num_psngrs));
			$data['cleared'] = "0";
			if($data['tkt_status']  == "CHART PREPARED"){
				$data['error'] = "5";
				if(!$cancelFlag && $cnfFlag) $data['cleared'] = "1";
			}
			else {
				if($cancelFlag) { //ticket cancelled
					$data['error'] = "4";
				}
				else if($cnfFlag) { //ticket cleared
					$data['error'] = "3";
					$data['cleared'] = "1";
				}
				else if($val_cnfFlag) { //ticket cleared but seat number not appeared yet
					$data['error'] = "6";
					$data['cleared'] = "1";
				}
			}

			if($data['journey_date'] < date('Y-m-d')){
				$data['error'] = "5";
			}
		}
		else {
			//$data['error'] = 1;
			//$this->mailToUsers('Problem in fetching PNR Data', 'Problem in fetching PNR Data: ' . $pnr, array('ashish@mindsarray.com'));
			$data = $this->getPNRInfo_backup($pnr);
		}
		return $data;
	}

	function getPNRInfo_backup($pnr){
		$url = "http://www.mmtstraintimings.in/pnrstatus/$pnr";
		//$url = "http://www.pnrstatuscheck.in/proxyt.php?url=/pnrstatus/$pnr";
		$user_agent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$page = curl_exec ($ch);
		$page=strip_tags(stripslashes($page));
		$outputs = explode("u000a",$page);
		//$this->printArray($outputs);
		if(trim($outputs[3]) == 'Train Number'){
			$data['boarding'] = trim($outputs[19]);
			$data['to'] = trim($outputs[17]);
			$data['from'] = trim($outputs[16]);
			$outputs[15] = str_replace(' ','',trim($outputs[15]));
			$data['journey_date'] = date('Y-m-d', strtotime($outputs[15]));
			$data['train_info'] = str_replace("u000a","",$page);

			$count = 0;
			$data['train_number'] = substr(trim($outputs[13]),1);
			$data['train_name'] = trim($outputs[14]);
			$data['pnr_number'] = $pnr;
			$data['class'] = trim($outputs[20]);

			$cancelFlag = true;
			$cnfFlag = true;
			$val_cnfFlag = true;

			$idx = 29;
			while(strstr(trim($outputs[$idx]),"Charting Status") === FALSE){
				if(strstr(trim($outputs[$idx]),"Passenger") !== FALSE){
					$count++;
					$passengers = array();
					$passengers['booking_status'] = trim($outputs[$idx+1]);
					$passengers['current_status'] = trim($outputs[$idx+2]);
					if($passengers['current_status'] != "Can/Mod"){
						$cancelFlag = false;
						if(preg_match("/(W\/L|CNF|Confirmed|RAC)/i", $passengers['current_status'])){
							$cnfFlag = false;
						}
						if(strtoupper($passengers['current_status']) != "CNF" && strtoupper($passengers['current_status']) != 'CONFIRMED'){
							$val_cnfFlag = false;
						}
					}
					$idx += 3;
					$data['passengers'][] = $passengers;
				}
				$idx++;
			}

			$data['num_passengers'] = $count;

			$data['tkt_status'] = trim($outputs[$idx+1]);
			$data['cleared'] = "0";
			if($data['tkt_status']  == "CHART PREPARED"){
				$data['error'] = "5";
				if(!$cancelFlag && $cnfFlag) $data['cleared'] = "1";
			}
			else {
				if($cancelFlag) { //ticket cancelled
					$data['error'] = "4";
				}
				else if($cnfFlag) { //ticket cleared
					$data['error'] = "3";
					$data['cleared'] = "1";
				}
				else if($val_cnfFlag) { //ticket cleared but seat number not appeared yet
					$data['error'] = "6";
					$data['cleared'] = "1";
				}
			}

			if($data['journey_date'] < date('Y-m-d')){
				$data['error'] = "5";
			}
		}
		else {
			$data['error'] = 1;
			//$this->mailToUsers('Backup: Problem in fetching PNR Data', 'Problem in fetching PNR Data: ' . $pnr, array('ashish@mindsarray.com'));
		}
		return $data;
	}

	function getPNRInfo_indianrail($pnr){
		//Configure::write('debug', 0);
		$param1 = substr($pnr,0,3);
		$param2 = substr($pnr,3);
		$url = 'http://www.indianrail.gov.in/cgi_bin/inet_pnrstat_cgi.cgi';
		//$params = "lccp_pnrno1=".$param1."&lccp_pnrno2=".$param2; //you must know what you want to post
		$params = "lccp_pnrno1=".$pnr."&lccp_pnrno2=".$param2;
		//$user_agent = "Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)";
		$user_agent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);

		$page=curl_exec ($ch);
		if($page)
		{
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);

			$content = $this->getPNRData($domDocument,$pnr);
			curl_close ($ch);
			return $content;
		}
		else
		{
			curl_close ($ch);
			$data = array();
			$data['error'] = "1"; //Site is down
			return $data;
		}

	}

	function checkDND($mobile){
		$url = 'http://nccptrai.gov.in/nccpregistry/saveSearchSub.misc';
		$params = "phoneno=$mobile"; //you must know what you want to post
		$user_agent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);

		$page=curl_exec ($ch);
		if($page)
		{
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);
			$xpath = "//td[contains(concat(' ',normalize-space(@class),' '),' GridHeader ')]";

			$domXPath = new DOMXPath($domDocument);
			$domNodeList = $domXPath->query($xpath);
			$ret = -1;
			foreach($domNodeList as $node){
				$domDocument1 = new DOMDocument();
				foreach($node->childNodes as $childNode){
					$domDocument1->appendChild($domDocument1->importNode($childNode, true));
					$html = trim(strip_tags($domDocument1->saveHTML()));
					if($html == 'The number is not registered in NCPR'){
						$ret = 0;
					}
					else {
						$ret = 1;
					}
				}
			}
			$return['dnd'] = $ret;
			if($ret == 1){
				$xpath = "//tr[15]/td";

				$domXPath = new DOMXPath($domDocument);
				$domNodeList = $domXPath->query($xpath);
				foreach($domNodeList as $node){
					$domDocument1 = new DOMDocument();
					foreach($node->childNodes as $childNode){
						$domDocument1->appendChild($domDocument1->importNode($childNode, true));
					}
					$html = substr(trim(strip_tags($domDocument1->saveHTML())),-1,1);
					$return['preference'] = $html;
				}
			}
			return $return;
			//return $content;
		}
		else {
			$return['dnd'] = -1;
			return $return;
		}
		curl_close ($ch);
	}

	function getPNRData($domDocument,$pnr)
	{
		//create an XPath object out of the document and query it for the supplied xpath
		$xpath = "//table[contains(concat(' ', normalize-space(@class), ' '), 'table_border')]";
		$domXPath = new DOMXPath($domDocument);
		$domNodeList = $domXPath->query($xpath);
		$data = array();
		$i = 0;
		foreach($domNodeList as $node){
			if($i == 0){ //train info
				$domDocument1 = new DOMDocument();
				foreach($node->childNodes as $childNode){
					$domDocument1->appendChild($domDocument1->importNode($childNode, true));
				}
				$domXPath1 = new DOMXPath($domDocument1);
				$xpath1 = "//td[contains(concat(' ', normalize-space(@class), ' '), 'table_border_both')]";
				$data['train_number'] = substr($this->getHTMLFromNode($domXPath1->query($xpath1)->item(0)),1);
				$data['train_name'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item(1));
				$data['boarding'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item(6));
				$date_str = $this->getHTMLFromNode($domXPath1->query($xpath1)->item(2));
				$date_str = str_replace(" ", "", $date_str);
				$data['journey_date'] = date('Y-m-d', strtotime($date_str));

				$data['from'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item(3));
				$data['to'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item(4));
				$data['class'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item(7));
			}
			else if($i == 1) { //reservation status
				$domDocument1 = new DOMDocument();
				foreach($node->childNodes as $childNode){
					$domDocument1->appendChild($domDocument1->importNode($childNode, true));
				}
				$data['train_info'] = strip_tags($domDocument1->saveHTML());
				$domXPath1 = new DOMXPath($domDocument1);
				$xpath1 = "//td[contains(concat(' ', normalize-space(@class), ' '), 'table_border_both')]";
				$val = $this->getHTMLFromNode($domXPath1->query($xpath1)->item(4));
				$num = 3;
				if($val == "Passenger 2" || $val == "CHART PREPARED" || $val == "CHART NOT PREPARED"){
					$num = 4;
				}
				$num_psngrs =  intval(($domXPath1->query($xpath1)->length)/$num);
				$data['num_passengers'] = $num_psngrs;
				$cancelFlag = true;
				$cnfFlag = true;
				$val_cnfFlag = true;
				for($j = 0;$j < $num_psngrs ; $j++){
					$passengers = array();
					$passengers['booking_status'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item($num*$j + 1));
					$passengers['current_status'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item($num*$j + 2));
					if($passengers['current_status'] != "Can/Mod"){
						$cancelFlag = false;
						if(preg_match("/(W\/L|CNF|Confirmed|RAC)/i", $passengers['current_status'])){
							$cnfFlag = false;
						}
						if(strtoupper($passengers['current_status']) != "CNF" && strtoupper($passengers['current_status']) != 'CONFIRMED'){
							$val_cnfFlag = false;
						}
					}
					$data['passengers'][$j] = $passengers;
				}
				$data['tkt_status'] = $this->getHTMLFromNode($domXPath1->query($xpath1)->item($num*$num_psngrs));
				$data['pnr_number'] = $pnr;
				$data['cleared'] = "0";
				if($data['tkt_status']  == "CHART PREPARED"){
					$data['error'] = "5";
					if(!$cancelFlag && $cnfFlag) $data['cleared'] = "1";
				}
				else {
					if($cancelFlag) { //ticket cancelled
						$data['error'] = "4";
					}
					else if($cnfFlag) { //ticket cleared
						$data['error'] = "3";
						$data['cleared'] = "1";
					}
					else if($val_cnfFlag) { //ticket cleared but seat number not appeared yet
						$data['error'] = "6";
						$data['cleared'] = "1";
					}
				}
			}
			$i++;
		}
		if($i == 0){
			$data['error'] = "0"; //Invalid PNR Number
		}
		return $data;
	}

	function retailPNR($pnr,$mobile,$random){
		$pnr = trim($pnr);
		$PnrObj = ClassRegistry::init('Pnr');
		$getInfo = $this->getPNRInfo($pnr);
		$PnrObj->recursive = -1;
		$data = $PnrObj->query("SELECT params FROM asynchronous_calls WHERE random_id = " . $random . " AND controller = 'pnr' AND action = 'retailPNR'");
		$params = json_decode($data['0']['asynchronous_calls']['params'],true);
		$success = 1;

		if(!isset($params['retailer_id']) && !isset($params['vendor_id'])) {
			$alias_suffix = "_OFFLINE";
		}
		else {
			if($params['product_id'] == PNR_SIGNATURE){
				$mail_subject = "Retail Product PNR Alert With Signature subscribed via retail panel";
			}
			else {
				$mail_subject = "Retail Product PNR Alert subscribed via retail panel";
			}
		}
			
		if(isset($getInfo['error'])){
			if($getInfo['error'] == '0' || $getInfo['error'] == '1'){
				//PNR Status is not available from the Railway Server. It might be a case that railway servers are not responding at this moment.
				//$sms = "Pnr number not valid or railway servers are down this time. Please try again later";
				$sms = $this->createMessage("PNR_NUMBER_NOT_CORRECT",array($pnr));
				$sms1 = "Ask for correct PNR number. If the number is correct, please try again later as Railway servers are overloaded at this time.";
			}
			else if($getInfo['error'] == '3' || $getInfo['error'] == '6'){
				//Ticket is already cleared
				$sms = $this->createMessage("PNR_TICKET_CONFIRMED".$alias_suffix,array());
				$sms1 = "Ticket is already confirmed so PNR alert cannot be created.";
			}
			else if($getInfo['error'] == '4'){
				//Ticket is cancelled
				$sms = $this->createMessage("PNR_TICKET_CANCELLED".$alias_suffix,array());
				$sms1 = "Ticket is cancelled so PNR alert cannot be created.";
			}
			else if($getInfo['error'] == '5'){
				//Chart has been prepared already
				$sms = $this->createMessage("PNR_CHART_PREPARED".$alias_suffix,array());
				$sms1 = "Chart has already been prepared for this PNR number so PNR alert cannot be created.";
			}
			/*if(isset($params['retailer_id'])){
				$this->sendMessage(SMS_SENDER,$params['mobile'],$sms1,'market');
				}*/
			$success = 0;
		}
		else {
			$getInfo['user_id'] = $params['user_id'];
			$getInfo['mobile'] = $mobile;
			if(isset($params['retailer_id'])){
				$getInfo['retailer_id'] = $params['retailer_id'];
				$getInfo['product_id'] = $params['product_id'];
			}
			$data = $PnrObj->find('first',array('conditions' => array('pnr_number' => $pnr,'mobile' => $mobile,'user_id' => $params['user_id'],'status_flag' => 1)));
			if(empty($data)){
				$url = SERVER_BACKUP . 'pnrs/createPNRAlert/-1/1';
				$this->curl_post_async($url,$getInfo);

				$prod_userid = $this->executeCouponEnteries($params['coupon_id'],$params['user_id'],$mobile,$params['product_id'],$params['serial_number'],1);
				if(isset($params['retailer_id']))$this->Shop->transactionsOnRetailerActivation($params['retailer_id'],$params['product_id'],$prod_userid);
				$title = substr(ucwords(strtolower($getInfo['train_name'])) . " - " . date("dMy",strtotime($getInfo['journey_date'])),0,20);
				$sms = $this->createMessage("PNR_SUBSCRIBE",array($title,$pnr));
				$sms1 = $sms;
			}
			else {
				$alias = "PNR_ALREADY_ACTIVATED" . $alias_suffix;
				$sms = $this->createMessage($alias,array($pnr));
				$sms1 = "PNR Number " . $pnr . " is already activated on mobile $mobile";
				$success = 0;
			}
		}

		$mail_body = "PNR No: ".$pnr."<br>";
		$mail_body .= "Mobile No: ".$mobile."<br>";
			
		if(isset($params['retailer_id'])){
			$shop = $this->Shop->getShopDataById($params['retailer_id'],RETAILER);
			$mail_body .= "Retailer: " . $shop['shopname'];
		}
		else if(isset($params['vendor_id'])){
			$vendor = $this->Shop->getVendor($params['vendor_id']);
			$mail_body .= "Vendor: " . $vendor;
		}
		$PnrObj->query("DELETE FROM asynchronous_calls WHERE random_id = " . $random . " AND controller = 'pnr' AND action = 'retailPNR'");

		$url = SERVER_BACKUP . 'users/sendMsgMails';
		$data['sms'] = $sms;
		$data['root'] = 'template';
		$data['mobile'] = $mobile;
		if($success == 1 && !empty($mail_subject)){
			//$data['mail_subject'] = $mail_subject;
			$data['mail_body'] = $mail_body;
		}
		if(isset($params['retailer_id'])){
			$data['retailer_id'] = $params['retailer_id'];
		}
		$this->curl_post_async($url,$data);

		$ret['success'] = $success;
		$ret['msg'] = $sms1;
		if(empty($prod_userid))$prod_userid = -1;
		$ret['prod_userid'] = $prod_userid;
		return $ret;
	}

	function retailTrainJourney($pnr,$mobile,$random){
		$pnr = trim($pnr);
		$PnrObj = ClassRegistry::init('Pnr');
		$getInfo = $this->getPNRInfo($pnr);
		$PnrObj->recursive = -1;
		$data = $PnrObj->query("SELECT params FROM asynchronous_calls WHERE random_id = " . $random . " AND controller = 'pnr' AND action = 'retailTrainJourney'");
		$params = json_decode($data['0']['asynchronous_calls']['params'],true);
		$success = 0;

		$mail_subject = "Retail Product Train Happy Journey subscribed via retail panel";
			
		if(isset($getInfo['error'])){
			if($getInfo['error'] == '0' || $getInfo['error'] == '1'){
				//PNR Status is not available from the Railway Server. It might be a case that railway servers are not responding at this moment.
				//$sms = "Pnr number not valid or railway servers are down this time. Please try again later";
				$sms1 = "Ask for correct PNR number. If the number is correct, please try again later as Railway servers are overloaded at this time.";
			}
			else if($getInfo['error'] == '4'){
				//Ticket is cancelled
				$sms1 = "Ticket is cancelled so PNR alert cannot be created.";
			}
			else if($getInfo['journey_date'] < date('Y-m-d')){
				//After journey date
				$sms1 = "Sorry, you cannot create this alert after journey date";
			}
			else if($getInfo['cleared'] == '1'){
				$success = 1;
				$getInfo['user_id'] = $params['user_id'];
				$getInfo['mobile'] = $mobile;
				$getInfo['retailer_id'] = $params['retailer_id'];
				$getInfo['product_id'] = $params['product_id'];

				$data = $PnrObj->find('first',array('conditions' => array('pnr_number' => $pnr,'mobile' => $mobile,'user_id' => $params['user_id'])));
				if(empty($data)){
					$url = SERVER_BACKUP . 'pnrs/createPNRAlert/-1/1';
					$this->curl_post_async($url,$getInfo);

					$prod_userid = $this->executeCouponEnteries($params['coupon_id'],$params['user_id'],$mobile,$params['product_id'],$params['serial_number'],1);
					if(isset($params['retailer_id']))$this->Shop->transactionsOnRetailerActivation($params['retailer_id'],$params['product_id'],$prod_userid);
					$sms1 = "Happy Journey for train '" . $getInfo['train_name'] . "' has been successfully created. PNR Number is " . $pnr;
				}
				else {
					$sms1 = "PNR Number " . $pnr . " is already activated on mobile $mobile";
					$success = 0;
				}
			}
			else {
				$sms1 = "Sorry, this ticket is not confirmed.";
			}
		}
		else {
			$sms1 = "Sorry, this ticket is not confirmed.";
		}

		$mail_body = "PNR No: ".$pnr."<br>";
		$mail_body .= "Mobile No: ".$mobile."<br>";
			
		if(isset($params['retailer_id'])){
			$shop = $this->Shop->getShopDataById($params['retailer_id'],RETAILER);
			$mail_body .= "Retailer: " . $shop['shopname'];
		}
		else if(isset($params['vendor_id'])){
			$vendor = $this->Shop->getVendor($params['vendor_id']);
			$mail_body .= "Vendor: " . $vendor;
		}
		$PnrObj->query("DELETE FROM asynchronous_calls WHERE random_id = " . $random . " AND controller = 'pnr' AND action = 'retailTrainJourney'");

		$url = SERVER_BACKUP . 'users/sendMsgMails';
		if($success == 1 && !empty($mail_subject)){
			//$data['mail_subject'] = $mail_subject;
			$data['mail_body'] = $mail_body;
		}
		if(isset($params['retailer_id'])){
			$data['retailer_id'] = $params['retailer_id'];
		}
		$this->curl_post_async($url,$data);

		$ret['success'] = $success;
		$ret['msg'] = $sms1;
		if(empty($prod_userid))$prod_userid = -1;
		$ret['prod_userid'] = $prod_userid;
		return $ret;
	}

	function createTagIfNotExists($tag){
		$tagObj = ClassRegistry::init('Tagging');
		$tagObj->recursive = -1;
		$tagData = $tagObj->find('first',array('conditions' => array('name' => strtoupper($tag))));
		if(empty($tagData)){
			$this->data['Tagging']['name'] = strtoupper($tag);
			$tagObj->create();
			$tagObj->save($this->data);
			return $tagObj->id;
		}
		else return $tagData['Tagging']['id'];
	}

	function tagUser($tag,$user_id,$email = null){
		$tag_id = $this->createTagIfNotExists($tag);

		if($tag_id == RETAILER_TAG){
			$this->linkRetailerUser($user_id,$email);
		}
		$tagObj = ClassRegistry::init('UserTagging');
		//echo "In function tagUser";
		$this->data['UserTagging']['tagging_id'] = $tag_id;
		$this->data['UserTagging']['user_id'] = $user_id;
		$this->data['UserTagging']['timestamp'] = date('Y-m-d H:i:s');
		$tagObj->create();
		if($tagObj->save($this->data)){
			return true;
		}
		else return false;
	}

	function updateGroupIdForRetailer($userId)
	{
		$data = $this->getUserDataFromId($userId);
		if($data['group_id'] == MEMBER){
			$userObj = ClassRegistry::init('User');

			$this->data['User']['id'] = $userId;
			$this->data['User']['group_id'] = RETAILER;

			$userObj->create();
			$userObj->save($this->data);
		}
	}

	function linkRetailerUser($user_id,$email=null){
		$tagObj = ClassRegistry::init('UserTagging');
		$data = $tagObj->query("SELECT taggings.name,vendors_retailers.id FROM user_taggings,taggings,vendors_retailers WHERE UPPER(vendors_retailers.retailer_code) = taggings.name AND taggings.id = user_taggings.tagging_id AND user_taggings.user_id = $user_id ");
		if(!empty($data)){
			$userData = $this->getUserDataFromId($user_id);
			$mobile = $userData['mobile'];

			$eText = "";
			if(trim($email) != '')
			$eText = ",email='$email'";

			$tagObj->query("UPDATE vendors_retailers SET user_id = $user_id,mobile='$mobile' $eText WHERE id=" . $data['0']['vendors_retailers']['id']);
		}
	}

	function getTaggedUsers($tag){
		$tagObj = ClassRegistry::init('Tagging');
		$tagObj->recursive = -1;
		$data = $tagObj->find('all',array('fields' => array('mobile_numbering_area.area_name','User.mobile','User.id'),'conditions' => array('Tagging.name like "%' . strtoupper($tag) . '%"'),
			'joins' => array(
		array(
					'type' => 'inner',
					'table' => 'user_taggings',
					'alias' => 'UserTagging',
					'conditions' => array('UserTagging.tagging_id = Tagging.id')
		),
		array(
					'type' => 'inner',
					'table' => 'users',
					'alias' => 'User',
					'conditions' => array('User.id = UserTagging.user_id')
		),
		array(
					'type' => 'left',
					'table' => 'mobile_numbering',
					'conditions' => array('mobile_numbering.number = SUBSTR(User.mobile,1,4)')
		),
		array(
					'type' => 'left',
					'table' => 'mobile_numbering_area',
					'conditions' => array('mobile_numbering.area = mobile_numbering_area.area_code')
		)
		),
			'group' => 'User_id',
			'order' => 'UserTagging.id desc'
			));
			$i = 0;
			foreach($data as $dt){
				$cmntObj = ClassRegistry::init('Comment');
				$cmntObj->recursive = -1;
				$comment = $cmntObj->find('all', array('fields' => array('Comment.comment','Comment.created'),
				'conditions' => array('Comment.itemid' => $dt['User']['id'],'Comment.itemtype' => 0),
				'order' => 'Comment.created desc',
				'limit' => 1
				));
				if(!empty($comment)){
					$data[$i]['Comment']['comment'] = $comment['0']['Comment']['comment'];
					$data[$i]['Comment']['created'] = $comment['0']['Comment']['created'];
				}
				$data[$i]['Tags'] = $this->getUserTags($dt['User']['id']);
				$i++;
			}
			return $data;
	}

	function getUserTags($user_id){
		$tagObj = ClassRegistry::init('UserTagging');
		$tagObj->recursive = 1;
		$data = $tagObj->find('all',array('fields' => array('Tagging.name','Tagging.id'),
			'conditions' => array('UserTagging.user_id' => $user_id)));
		return $data;
	}

	function updateLocation($area_id){
		$retailObj = ClassRegistry::init('Retailer');

		$areaArr = $retailObj->query("SELECT area_id FROM retailers where id = ".$area_id);
		foreach($areaArr as $a){
			$retailObj->query("update locator_area set toShow = 1 where id =".$a['retailers']['area_id']);
			$cityArr = 	$retailObj->query("SELECT city_id FROM locator_area where id = ".$a['retailers']['area_id']);
			foreach($cityArr as $c){
				$retailObj->query("update locator_city set toShow = 1 where id =".$c['locator_area']['city_id']);
				$stateArr = $retailObj->query("SELECT state_id FROM locator_city where id = ".$c['locator_area']['city_id']);
				foreach($stateArr as $s){
					$retailObj->query("update locator_state set toShow = 1 where id =".$s['locator_city']['state_id']);
				}
			}
		}

		$this->autoRender = false;
	}

	function checkRetailerPrizeScheme($vendor_id,$retailer_code,$prod_price){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->query("UPDATE scheme_sales SET totalSale= totalSale+$prod_price, dailySale= dailySale+$prod_price");

		$retailObj->query("UPDATE vendors_retailers SET totalSale= totalSale+$prod_price, weeklySale= weeklySale+$prod_price WHERE vendor_id = $vendor_id AND retailer_code = '$retailer_code'");

		$weeklySale_data = $retailObj->query("SELECT vendors_retailers.weeklySale FROM vendors_retailers WHERE vendors_retailers.vendor_id = $vendor_id AND vendors_retailers.retailer_code = '$retailer_code'");
		$weeklySale = $weeklySale_data['0']['vendors_retailers']['weeklySale'];

		$dailySale = $retailObj->query("SELECT scheme_sales.dailySale FROM scheme_sales");
		$dailySale = $dailySale['0']['scheme_sales']['dailySale'];

		$weeklyOffer = false;
		$goaOffer = true;
		$daily = false;
		$message = "";
		if($dailySale >= DAILY_PRIZE_SALE_LIMIT){
			$retailerCheck = $retailObj->query("SELECT max(timestamp) as lastDate FROM retailers_winners WHERE vendor_id = $vendor_id AND retailer_code = '$retailer_code' AND type = 'daily'");
			if(empty($retailerCheck) || $retailerCheck['0']['0']['lastDate'] < date('Y-m-d H:i:s',strtotime('- ' . DAILY_PRIZE_DAYS_WINDOW .' days'))){
				$retailWinObj = ClassRegistry::init('RetailersWinner');
				$this->data['RetailersWinner']['vendor_id'] = $vendor_id;
				$this->data['RetailersWinner']['retailer_code'] = $retailer_code;
				$this->data['RetailersWinner']['prize'] = 1;
				$this->data['RetailersWinner']['type'] = 'daily';
				$this->data['RetailersWinner']['timestamp'] = date('Y-m-d H:i:s');

				$retailWinObj->create();
				$retailWinObj->save($this->data);
				$last_id = $retailWinObj->id;

				App::import('vendor', 'md5Crypt', array('file' => 'md5Crypt.php'));
				$objMd5 = new Md5Crypt;
				$retailer_winner_id = $objMd5->Encrypt($last_id,encKey);

				$retailObj->query("UPDATE scheme_sales SET dailySale=0");
				//$this->mailToAdmins("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code");
				/*if($vendor_id == VENDOR_OSS){
					$this->mailToUsers("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code<br/>Please send us the mobile number and address of this retailer to dinesh@mindsarray.com.", array('ashisharya.iitb@gmail.com'));
					$this->mailToUsers("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code<br/>Please send us the mobile number and address of this retailer to dinesh@mindsarray.com.", array('navin.shrivastav@onestopshop.in'));
					$this->mailToUsers("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code<br/>Please send us the mobile number and address of this retailer to dinesh@mindsarray.com.", array('khushboo.prajapati@onestopshop.in'));
					}*/
				$message = "Congratulations, You have just won a Lucky Prize from SMSTadka.<br/>
Your prize is on its way to your home<br/>";
				$daily = true;
			}
		}

		if($goaOffer){
			$retail = $retailObj->query("SELECT SUM(products.price) as totalSaleTill FROM vendors_activations INNER JOIN products_users ON (vendors_activations.productuser_id = products_users.id) INNER JOIN products ON (products.id = products_users.product_id) WHERE vendors_activations.vendor_id = $vendor_id AND vendors_activations.vendor_retail_code = '$retailer_code'");

			$message .= "Keep doing transactions. Your total SMSTadka sale till now is Rs " . $retail['0']['0']['totalSaleTill'];
		}

		if($weeklyOffer && !$daily && $weeklySale >= WEEKLY_PRIZE_SALE_LIMIT){
			$message = "Congratulations !!! You have already won your weekly assured prize.<br/>
Your prize is on its way to your home.<br/>
You can keep trying for more lucky prizes on every transactions.";
		}
		else if($weeklyOffer && !$daily){
			$weekly = WEEKLY_PRIZE_SALE_LIMIT - $weeklySale;
			$message = "Keep activating SMSTadka products for your customers and get a chance to win exciting prizes from SMSTadka.<br/>
For an assured prize worth Rs 2499/-, You are just Rs $weekly away.<br/>
<a href='".SITE_NAME."offers'>To know more Click here.</a>";
		}
		return $message;
	}

	function updateProductSwitch($parent_product,$user_id,$product_id,$type){
		$retailObj = ClassRegistry::init('Retailer');
		$data = $retailObj->query("SELECT product_id FROM products_users_switch WHERE product_parent_id = $parent_product AND user_id = $user_id AND active = 1");
		$save = false;
		$product = $product_id;
		if(empty($data)){
			$save = true;
		}
		else if($type == 'sub'){
			$product = $data['0']['products_users_switch']['product_id'];
		}
		else if($type == 'change' && $product_id != $data['0']['products_users_switch']['product_id']){
			$save = true;
		}
		if($save){
			$retailObj->query("UPDATE products_users_switch SET active = 0 WHERE active = 1 AND product_parent_id = $parent_product AND user_id = $user_id");
			$retailObj->query("INSERT INTO products_users_switch (product_id,product_parent_id,user_id,timestamp) VALUES ($product,$parent_product,$user_id,'".date('Y-m-d H:i:s')."')");
		}
		return $product;
	}

	function maskNumber($n){
		return substr($n,0,6)."XXXX";
	}

	function tagCloud(){
		$retailObj = ClassRegistry::init('Retailer');
		$tags = $retailObj->query("select tag_id,count(messages_tags.tag_id) as counts from messages_tags group by messages_tags.tag_id having counts > 200 limit 40");
		$my_array=array();
		foreach($tags as $t){
			$my_array[]=$t['messages_tags']['tag_id'];
		}
		$comma_string=implode(",",$my_array);
		$tagsinfo = $retailObj->query("SELECT tags.url,tags.name from tags where tags.id in(".$comma_string.")");
		$i = 0;
		foreach($tags as $t){
			$tagsinfo[$i]['0']['counts']=$t['0']['counts'];
			$i++;
		}
		return $tagsinfo;
	}

	function logMisscalls($mobile,$misscall){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->query("INSERT INTO log_misscall (mobile,misscall,timestamp) VALUES ('$mobile','$misscall','".date('Y-m-d H:i:s')."')");
	}
	function apiAccessHashCheck($params){

		$hashGen = sha1(PAY1_API_PARTNER_ID.$params['client_req_id'].PAY1_API_ACCESS_PASS); // order -> acc_id + transId + password
		if( !empty( $params['hash_code'] ) && $params['hash_code'] == $hashGen){
			return true;
		}else{
			return false;
		}

	}
}
?>