<?php

class ShopComponent extends Object {
	var $components = array('General');

	function shopTransactionUpdate($type,$amount,$ref1_id,$ref2_id,$prodUserId=null){

		$transObj = ClassRegistry::init('ShopTransaction');

		$this->data['ShopTransaction']['ref1_id'] = $ref1_id;
		$this->data['ShopTransaction']['ref2_id'] =  $ref2_id;
		$this->data['ShopTransaction']['amount'] =  $amount;
		$this->data['ShopTransaction']['type'] =  $type;
		$this->data['ShopTransaction']['productuser_id'] =  $prodUserId;
		$this->data['ShopTransaction']['timestamp'] =  date('Y-m-d H:i:s');

		$transObj->create();
		$transObj->save($this->data);

		return $transObj->id;
	}

	function shopCreditDebitUpdate($type,$amount,$from,$to,$to_groupId,$desc,$numbering,$api_flag=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		if($api_flag == null)$api_flag = 0;
		if(!empty($from))
		$transObj->query("INSERT INTO shop_creditdebit (from_id,to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($from,$to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");
		else
		$transObj->query("INSERT INTO shop_creditdebit (to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");
	}

	function transactionsOnDistributorActivation($distributor_id,$parent_id,$coupons,$products){
		$prodObj = ClassRegistry::init('Product');
		$prodids = array();
		foreach($products as $key=>$value){
			$prodids[] = $key;
		}
		$prodObj->recursive = -1;
		$prices = $prodObj->find("all",array('fields' => array('id,price'), 'conditions' => array('Product.id in (' . implode(",",$prodids) . ')')));
		$amount = 0;
		foreach($prices as $price){
			$amount += $price['Product']['price']*$products[$price['Product']['id']];
		}
		//transaction entries
		$trans_id = $this->shopTransactionUpdate(DISTRIBUTOR_ACTIVATION,$amount,$distributor_id,implode(",",$coupons));

		$commission_superdistributor = $this->calculateCommission($parent_id,SUPER_DISTRIBUTOR,$products,date('Y-m-d'));
		$tds_superdistributor = $this->calculateTDS($commission_superdistributor);
		$this->shopTransactionUpdate(COMMISSION_SUPERDISTRIBUTOR,$commission_superdistributor,$parent_id,$trans_id);
		$this->shopTransactionUpdate(TDS_SUPERDISTRIBUTOR,$tds_superdistributor,$parent_id,$trans_id);
		$commission_superdistributor -= $tds_superdistributor;

		$commission_distributor = $this->calculateCommission($distributor_id,DISTRIBUTOR,$products,date('Y-m-d'));
		$this->shopTransactionUpdate(COMMISSION_DISTRIBUTOR,$commission_distributor,$distributor_id,$trans_id);

		$shop_superdist = $this->getShopDataById($parent_id,SUPER_DISTRIBUTOR);
		if($shop_superdist['tds_flag'] == '1'){
			$tds_distributor = $this->calculateTDS($commission_distributor);
			$this->shopTransactionUpdate(TDS_DISTRIBUTOR,$tds_distributor,$distributor_id,$trans_id);
			$commission_distributor -= $tds_distributor;
		}

		//balance update
		$amount_distributor = $amount - $commission_distributor;
		$amount_superdistributor = $commission_superdistributor - $commission_distributor;
		$bal = $this->shopBalanceUpdate($amount_distributor,'subtract',$distributor_id,DISTRIBUTOR);
		$this->shopBalanceUpdate($amount_superdistributor,'add',$parent_id,SUPER_DISTRIBUTOR);
		echo "<script> reloadShopBalance(".$bal.");  </script>";
		return $trans_id;
	}

	function transactionsOnRetailerActivation($retailer_id,$product,$prodUserId=null){
		$prodObj = ClassRegistry::init('Product');
		$prodObj->recursive = -1;
		$price = $prodObj->find("first",array('fields' => array('id,price'), 'conditions' => array('Product.id' => $product)));
		$amount = $price['Product']['price'];

		//transaction entries
		$trans_id = $this->shopTransactionUpdate(RETAILER_ACTIVATION,$amount,$retailer_id,$product,$prodUserId);
		$prod[$product] = 1;
		$commission_retailer = $this->calculateCommission($retailer_id,RETAILER,$prod,date('Y-m-d'));

		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$distributor_id = $ret_shop['parent_id'];

		$dist_shop = $this->getShopDataById($distributor_id,DISTRIBUTOR);
		$superdistributor_id = $dist_shop['parent_id'];

		$commission_distributor = $this->calculateCommission($distributor_id,DISTRIBUTOR,$prod,date('Y-m-d'));

		$commission_superdistributor = $this->calculateCommission($superdistributor_id,SUPER_DISTRIBUTOR,$prod,date('Y-m-d'));
		$tds_superdistributor = $this->calculateTDS($commission_superdistributor);

		$this->shopTransactionUpdate(COMMISSION_SUPERDISTRIBUTOR,$commission_superdistributor,$superdistributor_id,$trans_id);
		$this->shopTransactionUpdate(TDS_SUPERDISTRIBUTOR,$tds_superdistributor,$superdistributor_id,$trans_id);
		$commission_superdistributor -= $tds_superdistributor;

		$this->shopTransactionUpdate(COMMISSION_DISTRIBUTOR,$commission_distributor,$distributor_id,$trans_id);
		$superdist_shop = $this->getShopDataById($superdistributor_id,SUPER_DISTRIBUTOR);
		if($superdist_shop['tds_flag'] == 1){
			$tds_distributor = $this->calculateTDS($commission_distributor);
			$this->shopTransactionUpdate(TDS_DISTRIBUTOR,$tds_distributor,$distributor_id,$trans_id);
			$commission_distributor -= $tds_distributor;
		}

		$this->shopTransactionUpdate(COMMISSION_RETAILER,$commission_retailer,$retailer_id,$trans_id);
		if($dist_shop['tds_flag'] == 1){
			$tds_retailer = $this->calculateTDS($commission_retailer);
			$this->shopTransactionUpdate(TDS_RETAILER,$tds_retailer,$retailer_id,$trans_id);
			$commission_retailer -= $tds_retailer;
		}

		//balance update
		$amount_retailer = $amount - $commission_retailer;
		$amount_distributor = $commission_distributor - $commission_retailer;
		$amount_superdistributor = $commission_superdistributor - $commission_distributor;
		$bal = $this->shopBalanceUpdate($amount_retailer,'subtract',$retailer_id,RETAILER);
		$this->shopBalanceUpdate($amount_distributor,'add',$distributor_id,DISTRIBUTOR);
		$this->shopBalanceUpdate($amount_superdistributor,'add',$superdistributor_id,SUPER_DISTRIBUTOR);
		echo "<script> reloadShopBalance(".$bal.");  </script>";
	}

	function updateVendorActivation($vendor_id,$prod_userid,$trans_id,$retailer_code){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;

		$this->data['VendorsActivation']['vendor_id'] = $vendor_id;
		$this->data['VendorsActivation']['productuser_id'] =  $prod_userid;
		$this->data['VendorsActivation']['vendor_trans_id'] =  $trans_id;
		$this->data['VendorsActivation']['vendor_retail_code'] =  $retailer_code;
		$this->data['VendorsActivation']['timestamp'] =  date('Y-m-d H:i:s');

		$vendorActObj->create();
		$vendorActObj->save($this->data);
		$id = $vendorActObj->id;

		$ref_code =  "20010434" . sprintf('%08d', $id);
		$vendorActObj->updateAll(array('VendorsActivation.ref_code' => $ref_code), array('VendorsActivation.id' => $id));

		$userObj = ClassRegistry::init('ProductsUser');
		$prod = $userObj->findById($prod_userid);
		$vendor = $userObj->query("SELECT username FROM vendors WHERE id = $vendor_id");
		$this->General->tagUser($vendor['0']['vendors']['username'],$prod['ProductsUser']['user_id']);
		$this->General->tagUser($retailer_code,$prod['ProductsUser']['user_id']);

		$this->addNewVendorRetailer($vendor_id,$retailer_code);
		return $ref_code;
	}

	function mailToSuperDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		$superdist_shop = $this->getShopDataById($dist_shop['parent_id'],SUPER_DISTRIBUTOR);
		//$this->General->mailToUsers($subject,$mail_body,array($superdist_shop['email']));
	}

	function mailToDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		//$this->General->mailToUsers($subject,$mail_body,array($dist_shop['email']));
	}

	function calculateCommission($id,$group_id,$products,$date){
		$slabObj = ClassRegistry::init('SlabsUser');
		$shop = $slabObj->query("SELECT slab_id FROM slabs_users WHERE shop_id = $id AND group_id = $group_id AND Date(timestamp) <= '$date' ORDER by id desc LIMIT 1");
		$slab = $shop['0']['slabs_users']['slab_id'];
		//$shop = $this->getShopDataById($id,$group_id);
		//$slab = $shop['slab_id'];

		$prodids = array();
		foreach($products as $key=>$value){
			$prodids[] = $key;
		}

		$prodData =  $slabObj->query("SELECT product_id, percent, products.price FROM slabs_products INNER JOIN products ON (product_id = products.id) WHERE slab_id = $slab AND product_id in (" . implode(",",$prodids). ")");
		$commission = 0;
		foreach($prodData as $prod){
			//$price = (100*$prod['products']['price'])/(100 + SERVICE_TAX_PERCENT);
			$price = $prod['products']['price'];
			$commission = $commission + ($price*$products[$prod['slabs_products']['product_id']]*($prod['slabs_products']['percent']/100));
		}
		return $commission;
	}

	function getCommissionPercent($id,$group_id,$product,$date){
		$slabObj = ClassRegistry::init('SlabsUser');
		$shop = $slabObj->query("SELECT slab_id FROM slabs_users WHERE shop_id = $id AND group_id = $group_id AND Date(timestamp) <= '$date' ORDER by id desc LIMIT 1");
		$slab = $shop['0']['slabs_users']['slab_id'];

		//$shop = $this->getShopDataById($id,$group_id);
		//$slab = $shop['slab_id'];

		$prodData =  $slabObj->query("SELECT percent FROM slabs_products WHERE slab_id = $slab AND product_id = $product");
		return $prodData['0']['slabs_products']['percent'];
	}

	function calculateTDS($commission){
		return ($commission*TDS_PERCENT/100);
	}

	function shopBalanceUpdate($price,$type,$id,$group_id){
		$bal = $this->getShopDataById($id,$group_id);
		$bal = $bal['balance'];
		if($type == 'subtract')
		$balance = $bal - $price;
		else if($type == 'add')
		$balance = $bal + $price;

		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			$userObj->updateAll(array('SuperDistributor.balance' => $balance), array('SuperDistributor.id' => $id));
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			$userObj->updateAll(array('Distributor.balance' => $balance), array('Distributor.id' => $id));
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->updateAll(array('Retailer.balance' => $balance), array('Retailer.id' => $id));
		}
		return sprintf('%.2f', $balance);
	}

	function getNewInvoice($shop_id,$from_id,$group_id,$invoice_type){
		$invoiceObj = ClassRegistry::init('Invoice');
		$this->data['Invoice']['ref_id'] = $shop_id;
		$this->data['Invoice']['from_id'] = $from_id;
		$this->data['Invoice']['group_id'] = $group_id;
		$this->data['Invoice']['invoice_type'] =  $invoice_type;
		$this->data['Invoice']['timestamp'] =  date('Y-m-d H:i:s');

		$invoiceObj->create();
		$invoiceObj->save($this->data);

		return $invoiceObj->id;
	}

	function addToInvoice($invoice_id,$trans_id){
		$invoiceObj = ClassRegistry::init('Invoice');
		$invoiceObj->query("INSERT INTO invoices_transactions (invoice_id,shoptransaction_id,timestamp) VALUES ($invoice_id,$trans_id,'".date('Y-m-d H:i:s')."')");
	}

	function getInvoiceAmount($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');

		$invoiceObj->recursive = -1;
		$invoice = $invoiceObj->findById($invoice_id);
		$id = $invoice['Invoice']['ref_id'];
		$group_id = $invoice['Invoice']['group_id'];
		$invoice_type = $invoice['Invoice']['invoice_type'];
		$data['date'] = date('Y-m-d', strtotime($invoice['Invoice']['timestamp']));

		$shop = $this->getShopDataById($id,$group_id);
		if($group_id != SUPER_DISTRIBUTOR){
			$parent = $shop['parent_id'];
			if($group_id == DISTRIBUTOR){
				$parentShop = $this->getShopDataById($parent,SUPER_DISTRIBUTOR);
			}
			else if($group_id == RETAILER){
				$parentShop = $this->getShopDataById($parent,DISTRIBUTOR);
			}
			$tds = $parentShop['tds_flag'];
		}
		else {
			$tds = 1;
		}

		$Totcommission = 0;
		$Totamount = 0;

		$transactions = $invoiceObj->query("SELECT ref2_id,shop_transactions.id FROM invoices_transactions,shop_transactions WHERE invoice_id = $invoice_id AND shop_transactions.id = invoices_transactions.shoptransaction_id");
		if($invoice_type == DISTRIBUTOR_ACTIVATION){
			$coupons = array();
			foreach($transactions as $transaction){
				$coupons = array_merge($coupons,explode(",",$transaction['shop_transactions']['ref2_id']));
			}
			$couponObj = ClassRegistry::init('Coupon');
			$couponObj->recursive = -1;
			$prodData = $couponObj->find('all',array('fields' => array('GROUP_CONCAT(Coupon.serialNumber) as serials','COUNT(Coupon.id) as quantity','Product.id','Product.name','Product.price'), 'conditions' => array('Coupon.id in (' .implode(",",$coupons). ')' ),
					'joins' => array(
			array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Coupon.product_id = Product.id')
			)
			),
					'group' => array('Coupon.product_id')
			));
			foreach($prodData as $prod){
				$Totamount += $prod['0']['quantity']*$prod['Product']['price'];
				$product = array();
				$product[$prod['Product']['id']] = $prod['0']['quantity'];
				$Totcommission += $this->calculateCommission($id,$group_id,$product,$date);
			}
		}
		else {
			$products = array();
			$prodids = array();
			foreach($transactions as $transaction){
				$prod_id = $transaction['shop_transactions']['ref2_id'];
				$transObj = ClassRegistry::init('ShopTransaction');
				$transObj->recursive = -1;
					
				if($group_id == RETAILER){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_RETAILER)));
				}
				else if($group_id == DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_DISTRIBUTOR)));
				}
				else if($group_id == SUPER_DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_SUPERDISTRIBUTOR)));
				}
				$commission = $commission['ShopTransaction']['amount'];

				if(!isset($products[$prod_id])){
					$products[$prod_id]['quantity'] = 1;
					$products[$prod_id]['commission'] = $commission;
					$prodids[] = $prod_id;
				}
				else {
					$products[$prod_id]['quantity'] = $products[$prod_id]['quantity'] + 1;
					$products[$prod_id]['commission'] = $products[$prod_id]['commission'] + $commission;
				}
			}
				
			$prodObj = ClassRegistry::init('Product');
			$prodObj->recursive = -1;
			$prodData = $prodObj->find('all',array('fields' => array('Product.id','Product.name','Product.price'), 'conditions' => array('Product.id in (' .implode(",",$prodids). ')' )));
			foreach($prodData as $prod){
				$Totamount += $products[$prod['Product']['id']]['quantity'] * $prod['Product']['price'];
				$Totcommission += $products[$prod['Product']['id']]['commission'];
			}
		}
		if($tds == 1){
			$tds = $this->calculateTDS($Totcommission);
			$Totcommission -= $tds;
		}

		$net = sprintf('%.2f',$Totamount - $Totcommission);
		return round($net);
	}

	function getOutstandingBalance($id,$type){
		$rcptObj = ClassRegistry::init('Receipt');
		$data = $rcptObj->find('all',array('fields' => array('min(Receipt.os_amount) as os'), 'conditions' => array('Receipt.receipt_ref_id' => $id,'Receipt.receipt_type' => $type)));
			
		if(empty($data['0']['0']['os'])){
			if($type == RECEIPT_INVOICE){
				$invObj = ClassRegistry::init('Invoice');
				$amount = $invObj->findById($id);
				$amount = $amount['Invoice']['amount'];
			}
			else if($type == RECEIPT_TOPUP){
				$transObj = ClassRegistry::init('ShopTransaction');
				$amount = $transObj->findById($id);
				$amount = $amount['ShopTransaction']['amount'];
			}
		}
		else {
			$amount = $data['0']['0']['os'];
		}
		return $amount;
	}

	function generateInvoice($id,$group_id,$type,$date=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		$last_trans = $transObj->query("SELECT max(shoptransaction_id) as trans_id FROM invoices_transactions WHERE invoice_id IN (SELECT max(id) FROM invoices WHERE ref_id=$id AND group_id = $group_id AND invoice_type = $type)");
		$trans_id = $last_trans['0']['0']['trans_id'];

		if($group_id == RETAILER && $type == RETAILER_ACTIVATION)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == DISTRIBUTOR && $type == RETAILER_ACTIVATION)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_DISTRIBUTOR;
		}
		else if($group_id == DISTRIBUTOR && $type == DISTRIBUTOR_ACTIVATION){
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == SUPER_DISTRIBUTOR)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_SUPERDISTRIBUTOR;
		}

		if(!empty($trans_id)){
			$query .= " AND st1.id > $trans_id";
		}
		if($date != null){
			$query .= " AND Date(st1.timestamp) = '$date'";
		}
		$data = $transObj->query($query);
		if(!empty($data)){
			if($group_id == SUPER_DISTRIBUTOR){
				$parent = null;
			}
			else if($group_id == DISTRIBUTOR || $group_id == RETAILER){
				$sData = $this->getShopDataById($id,$group_id);
				$parent = $sData['parent_id'];
			}
			$new_invoice = $this->getNewInvoice($id,$parent,$group_id,$type);
			foreach($data as $trans){
				$this->addToInvoice($new_invoice,$trans['st1']['id']);
			}
			$amount = $this->getInvoiceAmount($new_invoice,date('Y-m-d'));
			$number = $this->getInvoiceNumber($new_invoice,date('Y-m-d'));
			$transObj->query("UPDATE invoices SET amount = $amount,invoice_number='$number' WHERE id = $new_invoice");
		}
	}


	function getShopDataById($id,$group_id){

		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['SuperDistributor'];
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Distributor'];
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Retailer'];
		}
	}

	function shortSerials($serials){
		$serials = explode(",",$serials);
		sort($serials);
		$serial_array = array();

		$min = $serials[0];
		$last = $serials[0];
		$serial_array[$min] = 1;
		foreach($serials as $serial){
			if($serial == $last + 1){
				$serial_array[$min] = $serial_array[$min] + 1;
				$last = $serial;
			}
			else if($serial != $min){
				$min = $serial;
				$last = $serial;
				$serial_array[$min] = 1;
			}
		}
		$strings = array();
		foreach($serial_array as $key=>$value){
			$string = $key;
			if($value > 1){
				$string .= "-" . ($key + $value - 1);
			}
			$strings[] = $string;
		}
		return implode(", ", $strings);
	}

	function getInvoiceNumber($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');
		$data = $invoiceObj->findById($invoice_id);
		if($data['Invoice']['group_id'] == SUPER_DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == RETAILER){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		$number1 = $ret_to['0']['0']['counts'] + 1;
		return "INV-" . $suffix1 . "/" . $suffix2 . "/" . date('Y',strtotime($date)). "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}

	function getTopUpReceiptNumber($transaction_id){
		$transObj = ClassRegistry::init('ShopTransaction');
		$data = $transObj->findById($transaction_id);
		if($data['ShopTransaction']['type'] == ADMIN_TRANSFER){
			$suffix1 = "ST";
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],SUPER_DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);
		}
		else if($data['ShopTransaction']['type'] == SDIST_DIST_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['ShopTransaction']['type'] == DIST_RETL_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],RETAILER);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		$ret = $transObj->query("SELECT count(*) as counts FROM shop_transactions WHERE type = ".$data['ShopTransaction']['type']." and ref1_id = ".$data['ShopTransaction']['ref1_id']." and id < $transaction_id");
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "TP-". $suffix1 . "/". $suffix2 . "/" . sprintf('%06d', $number);
	}

	function getCreditDebitNumber($to_id,$from_id,$to_groupid,$type,$id=null){
		$invoiceObj = ClassRegistry::init('Invoice');
		$query = "";
		if($id != null){
			$query = " AND id < $id";
		}
		if(empty($from_id)) $from = " is null";
		else $from = " = $from_id";

		$ret = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE to_groupid = $to_groupid AND from_id $from AND to_id = $to_id AND type=$type $query");
		$retAll = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE from_id $from AND type=$type $query");

		$shop = $this->getShopDataById($to_id,$to_groupid);
		if(!empty($from_id)){
			$parent_shop = $this->getShopDataById($from_id,$to_groupid-1);
			$parent_comp = explode(" ",$parent_shop['company']);
		}

		if(isset($shop['company']))
		$comp = explode(" ",$shop['company']);
		else
		$comp = explode(" ",$shop['shopname']);

		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $retAll['0']['0']['counts'] + 1;
		$number1 = $ret['0']['0']['counts'] + 1;
		if($type == 0) $suffix0 = "CN";
		else if($type == 1) $suffix0 = "DN";
		return "$suffix0-" . $suffix1 . "/" . $suffix2 . "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}

	function getReceiptNumber($id){
		$recObj = ClassRegistry::init('Receipt');
		$data = $recObj->findById($id);
		if($data['Receipt']['receipt_type'] == RECEIPT_INVOICE){
			$suffix1 = "INV";
		}
		else if($data['Receipt']['receipt_type'] == RECEIPT_TOPUP){
			$suffix1 = "TP";
		}

		if($data['Receipt']['group_id'] == SUPER_DISTRIBUTOR){
			$suffix2 = "ST";
		}
		else if($data['Receipt']['group_id'] == DISTRIBUTOR){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['Receipt']['group_id'] == RETAILER){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}

		$ret = $recObj->query("SELECT count(*) as counts FROM receipts WHERE shop_from_id = ".$data['Receipt']['shop_from_id']." and group_id = " . $data['Receipt']['group_id'] . " and id < $id");
		if(isset($parent_comp)){
			$suffix2 = "";
			foreach($parent_comp as $str){
				$suffix2 .= substr($str,0,1);
			}
			$suffix2 = strtoupper($suffix2);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "R/". $suffix1 . "/". $suffix2 . "/" . sprintf('%03d', $number);
	}

	function getShopData($user_id,$group_id=null){
		if($group_id == null){
			$userData = $this->General->getUserDataFromId($user_id);
			$group_id = $userData['group_id'];
		}
		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['SuperDistributor'];
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['Distributor'];
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['Retailer'];
		}
	}

	function getVendor($id){
		$retailObj = ClassRegistry::init('Retailer');
		$ret = $retailObj->query("SELECT company FROM vendors where id = $id");
		return $ret['0']['vendors']['company'];
	}

	function updateSlab($slab_id,$shop_id,$group_id){
		$slabObj = ClassRegistry::init('SlabsUser');
		$slabObj->create();
		$slabData['SlabsUser']['slab_id'] = $slab_id;
		$slabData['SlabsUser']['shop_id'] = $shop_id;
		$slabData['SlabsUser']['group_id'] = $group_id;
		$slabData['SlabsUser']['timestamp'] = date('Y-m-d H:i:s');
		$slabObj->save($slabData);
	}

	function addNewVendorRetailer($vendor_id,$retailer_code){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->query("INSERT INTO vendors_retailers (vendor_id,retailer_code) VALUES ($vendor_id,'$retailer_code')");
	}
}
?>