<?php
class PackagesUsersController extends AppController {

	var $name = 'PackagesUsers';

	function index() {
		$this->PackagesUser->recursive = 0;
		$this->set('packagesUsers', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid packages user', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('packagesUser', $this->PackagesUser->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->PackagesUser->create();
			if ($this->PackagesUser->save($this->data)) {
				$this->Session->setFlash(__('The packages user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The packages user could not be saved. Please, try again.', true));
			}
		}
		$packages = $this->PackagesUser->Package->find('list');
		$users = $this->PackagesUser->User->find('list');
		$slots = $this->PackagesUser->Slot->find('list');
		$this->set(compact('packages', 'users', 'slots'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid packages user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->PackagesUser->save($this->data)) {
				$this->Session->setFlash(__('The packages user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The packages user could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->PackagesUser->read(null, $id);
		}
		$packages = $this->PackagesUser->Package->find('list');
		$users = $this->PackagesUser->User->find('list');
		$slots = $this->PackagesUser->Slot->find('list');
		$this->set(compact('packages', 'users', 'slots'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for packages user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->PackagesUser->delete($id)) {
			$this->Session->setFlash(__('Packages user deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Packages user was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>