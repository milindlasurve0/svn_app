<?php
class UsersController extends AppController {

	var $name = 'Users';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $uses = array('User','Package','PackagesUser','Transaction','Category','Friendlist','Draft','Message','Slot','billing_user','Log');
        //var $uses = array();
	var $components = array('RequestHandler','Ssl','Shop','General');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		//$this->Auth->allowedActions = array('addNewArea','addNewCity','submitPayment','showRetailerForm','setOptFlag','submitRetailer','sendPassword1','addNewNumber','checkDND','getRecentMsgs','sendMsgMails','paymentResponseOSS','prepaymentPayOSS','iccPaymentDone','paymentResponseICC','prepaymentPayICC','initDB','loginWin','redeemCredits','login','paymentResponse','afterLogin','beforeSend','beforeSubscribe','captcha','captchaValidate','register','forgotPassword','registerCheck','forgotPasswordCheck','updatePassword','twitt','dnd','resenddndcode','senddndcode','dndverification','dndblock','fetchMorePackages','er404','resendPassword','restoreReg','changePassword','getMobDthRecharge','getMobRecharge','getDthRecharge');
                //$this->autoLayout = $this->autoRender = FALSE;
                $this->redis = $this->General->redis_connect();
	}

	function addNewCity()
	{
		$newCity=$_REQUEST['newCity'];
		$state=$_REQUEST['stateValue'];
		$stateIdResult=$this->User->query("select id from locator_state where name like '$state' ");
		$stateId=$stateIdResult['0']['locator_state']['id'];
		//echo $stateId;
		$this->User->query("insert into locator_city (state_id,name,toShow) values($stateId,'$newCity',0)");
		$cityIdResult=$this->User->query("select id from locator_city where name like '$newCity' ");
		echo $cityIdResult['0']['locator_city']['id'];

		/* $newCityResult=mysql_query("select name from locator_city where state_id=$stateId");
		 while ($row=mysql_fetch_assoc($newCityResult))
		 {
		 echo $row['name'];
		 }*/
		//echo "Inserted successfully. ";
		$this->autoRender=false;
	}

	function addNewArea()
	{
		$newArea=$_REQUEST['newArea'];
		$city=$_REQUEST['cityValue'];
		//echo "New City Name".$city;
		$state=$_REQUEST['stateValue'];

		$cityIdResult=$this->User->query("select id from locator_city where name like '$city' ");
		$cityId=$cityIdResult['0']['locator_city']['id'];
		//echo $stateId;
		$this->User->query("insert into locator_area (city_id,name,toShow) values($cityId,'$newArea',1)");
		$areaIdResult=$this->User->query("select id from locator_area where name like '$newArea' ");
		echo $areaIdResult['0']['locator_area']['id'];
		/* $newAreaResult=mysql_query("select name from locator_area where city_id=$cityId");
		 while ($row=mysql_fetch_assoc($newAreaResult))
		 {
		 echo $row['name'];
		 }
		 //echo "Inserted successfully. ";*/
		$this->autoRender=false;
	}

	function submitPayment()
	{
		$userid=$_REQUEST['userId'];
		$details=$_REQUEST['details'];
		$amt=$_REQUEST['amount'];
		$payMode=$_REQUEST['type'];
		$today=date('Y-m-d H:i:s');
		$tableMsgForLogs="User made payment of Rs. ".$amt. ". Received by ".$payMode. ", Details: $details";
		$loggedInUser= $_SESSION['Auth']['User']['id'];
		$usersMobileResult=$this->User->query("select mobile from users where id=$userid");
		$mobile=$usersMobileResult['0']['users']['mobile'];

		$updatedBalance = $this->General->balanceUpdate($amt,'add',$userid);
		if($payMode!='FREE')
		{
			$this->User->query("insert into payment(user_id,flag,other_details,amount,start_time) values($userid,'SUCCESS','$details',$amt,'$today') ");
			$transactionResult=$this->General->transactionUpdate(TRANS_USER_CREDIT,$amt,null,null,$userid);
			//$this->General->mailToAdmins("Payment received offline by ".$mobile,"Payment of Rs.".$amt. " received from user ".$mobile." by ".$payMode . " Mode");
			$msg="Dear user, thank you for payment of Rs. ".$amt." by ".$payMode. "\nYour SMSTadka account balance is ".$updatedBalance;
		}
		else {
			$tableMsgForLogs="Free credit of Rs. ".$amt. " added to account";
			$transactionResult=$this->General->transactionUpdate(TRANS_ADMIN_FREE_CREDIT,$amt,null,null,$userid);
			$msg="Dear user, your account has been credited with Rs. ".$amt. "\nYour SMSTadka account balance is ".$updatedBalance;
		}
		echo "Payment updated successfully";
		//For logs updation in userInfo
		$this->User->query("insert into  logs_cc(user_id,description,parent_user_id) values ($userid,'$tableMsgForLogs',$loggedInUser)");
		//Send SMS to user.
		$this->General->sendMessage('',$mobile,$msg);
		$this->autoRender=false;
	}

	function submitRetailer()
	{

		$retailerNameValue=$_REQUEST['retailerNameValue'];
		$userId=$_REQUEST['userId'];//$_SESSION['Auth']['User']['id'];
		if($_REQUEST['retailerNameValue'] != '0' && $this->General->tagUser($retailerNameValue,$userId)){
			echo "1";
		}
		else {
			echo "0";
		}

		$this->autoRender=false;
	}


	function sendPassword1()
	{
			
		$password = $this->General->generatePassword(4);
		$this->General->sendPassword($_REQUEST['mobile'],$password,0);
		echo $password;
		$this->autoRender=false;

	}
	function addNewNumber(){

		$loggedInUser= $_SESSION['Auth']['User']['id'];
		$oldNum=$_REQUEST['oldNumber'];
		$newNum=$_REQUEST['newNumber'];
		$oldIDForLogsResult=$this->User->query("select id from users where mobile='$oldNum'");
		$oldIDForLogs=$oldIDForLogsResult['0']['users']['id'];

		$result=$this->User->query("Select id from users where mobile='$newNum'");
		$newNumCheck=$this->User->query("Select mobile from users where mobile='$newNum'");
		if(count($newNumCheck) > 0) // checking if new number already present in system.
		{
				
			$msg = "Your new number exists in the system. ";
		}


		$successs = 0;

		if(count($result) > 0)  // old number and new number get swapped.
		{
			$newId = $result['0']['users']['id'];
			$oldQuery=$this->User->query("select id from users WHERE mobile='$oldNum'");//get oldNum id.
			$oldId=$oldQuery['0']['users']['id'];// OLD NUMBER ID
			$this->User->query("update users set mobile='temp_str' where id=$newId");//relacing new num by 'temp_str'.
			$this->User->query("update users set mobile='".$newNum."' where id=$oldId");
			$this->User->query("update users set mobile='".$oldNum."' where id=$newId");
			$successs = 1;
		}
		else {
			$result=$this->User->query("UPDATE users SET mobile=$newNum WHERE mobile=$oldNum");
			$successs = 1;
		}
		$tableMsg="Number updated from ".$oldNum." to new number ".$newNum;
		$msg1= "Dear User,\nYour SMSTadka Profile has been shifted from $oldNum to your new number ".$newNum;
		echo $successs."^^^".$msg1;

		if($successs == 1)
		{
			$this->General->sendMessage('',$newNum,$msg1);
			$this->General->updateLogCC($oldIDForLogs,$tableMsg);
			if(count($result) > 0){
				$tableMsg="Number swapped from ".$newNum." to new number ".$oldNum;
				$this->General->updateLogCC($newId,$tableMsg);
			}
		}
		$this->autoRender=false;
	}



	function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	function paynow(){
		$id = $_SESSION['Auth']['User']['id'];
		if (!$id) {
			$this->redirect(array('action' => 'login'));
		}
		$this->Ssl->force();
		$data = $this->billing_user->find('first',array('conditions' => array('billing_user.user_id' => $this->Session->read('Auth.User.id'))));
		if(empty($data)){
			$data['billing_user']['name'] = $this->Session->read('Auth.User.name');
			$data['billing_user']['city'] = $this->Session->read('Auth.User.city');
			$data['billing_user']['state'] = $this->Session->read('Auth.User.state');
			$data['billing_user']['email'] = $this->Session->read('Auth.User.email');
		}
		$this->set('data',$data);
			
		$this->layout = 'payment';
	}

	function view() {
		$id = $_SESSION['Auth']['User']['id'];
		if (!$id) {
			$this->redirect(array('action' => 'login'));
		}

		$this->User->recursive = -1;
		$this->set('user', $this->User->read(null, $id));

		if($this->Session->check('param')){
			if($this->Session->read('ccCode')){
				$this->set('ccCode',$this->Session->read('ccCode'));
				//$this->Session->destroy('ccCode');
			}
				
			$this->set('pageType',$this->Session->read('param'));
			if($this->Session->read('param') == 'app'){
				$this->set('controller_name',$this->Session->read('app'));
				$this->set('about',$this->Session->read('about'));
			}
		}
		else if($this->Session->check('category')){
			$this->set('pageType','package');
			$this->set('category',$this->Session->read('category'));
			$this->set('package',$this->Session->read('package'));
		}
		else {
			/*$this->Category->recursive = 1;
			 $this->Category->filterBindings(array('Message','Package'));
			 $resultCategories= $this->Category->find('all', array(
				'fields' => array('Category.name', 'Category.id'),
				'conditions' => array('Category.parent' =>'0','Category.toshow' => '1')
				));
					
				$category = 'pop';

				if($this->Session->check('category')){
				$category = $this->Session->read('category');
				}

				if($category == 'pop'){
				$limit = PACK_LIMIT_L;
				$layout = 'L';
				$resultPack= $this->getDisplayPackages(POPULAR_ARRAY,$limit,1);
				$count = count(explode(',',POPULAR_ARRAY));
				} else {

				$layout = $this->General->getCategoryLayout($category);
				if($layout == 'L'){
				$limit = PACK_LIMIT_L;
				}
				else if($layout == 'G'){
				$limit = PACK_LIMIT_G;
				}
				$resultPack= $this->General->getAllPackages(array($category),$limit,1);
				$count = $this->General->getCountPackages(array($category));
				}

				$this->set('resultCategories',$resultCategories);
				$this->set('packageData',$resultPack);
				$this->set('count',$count);
				$this->set('limit',$limit);
				$this->set('layout',$layout);
				$this->set('page',1);
				$this->set('category',$category);
				$this->set('package',$this->Session->read('package'));
				//$this->set('pageType','package');
				*/
			$this->set('pageType','dashboard');
		}

		$this->Session->delete('param');
		$this->Session->delete('category');
		$this->Session->delete('package');
		$this->Session->delete('app');
		$this->Session->delete('about');
	}


	function add() {
		if (!empty($this->data)) {
			$this->User->create();
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
		$groups = $this->User->Group->find('list');
		$packages = $this->User->Package->find('list');
		$this->set(compact('groups', 'packages'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		$groups = $this->User->Group->find('list');
		$packages = $this->User->Package->find('list');
		$this->set(compact('groups', 'packages'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}


	function addFriend(){

		$this->data['Friendlist']['nickname'] = $_REQUEST['nickname'];
		$this->data['Friendlist']['mobile'] = $_REQUEST['mobile'];
		$this->data['Friendlist']['user_id'] = $_SESSION['Auth']['User']['id'];

		$this->Friendlist->create();
		if($this->Friendlist->save($this->data)){
			$id = $this->Friendlist->id;
			echo $id;
		}
		else{
			//echo "<script>alert('Either nickname or mobile number is duplicate');</script>";
			echo 'null';
		}
		$this->autoRender = false;
	}

	function deleteFriend(){
		$id = $_POST['id'];
		if($this->Friendlist->delete($id)){
			$data = $this->Friendlist->find('first', array('fields' => array('Friendlist.id'), 'conditions' => array('Friendlist.user_id' => $_SESSION['Auth']['User']['id'])));
				
			if($data['Friendlist']['id'] == ''){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo 'null';
		}
		$this->autoRender = false;
	}

	/*function mobileCheck() {

	if (!empty($this->data))
	{
	$mobile = $this->data['User']['mobile'];
	$this->set('mobile', $mobile);

	if($this->General->checkIfUserExists($mobile)){
	$this->set('forgot', 1);
	$this->render('/elements/login_user','ajax');
	}
	else {
	$this->render('/elements/captcha_check','ajax');
	}
	}
	}*/

	function captcha($val=null)	{
		$width = 59;
		$height = 28;
		$characters = 4;

		App::import('Component','Captcha');
		$this->Captcha = new CaptchaComponent($this);
		$this->Captcha->create($width, $height, $characters);
		$this->autoRender = false;
	}


	function captchaValidate()	{
		$this->data['User']['mobile'] = $_POST['mobile'];
		$this->data['User']['captchaText'] = $_POST['captcha'];
		$resend_stmt = "";

		$mobileDetails = $this->General->getMobileDetails($this->data['User']['mobile']);
		if($mobileDetails['0'] != '' || $mobileDetails['1'] != ''){
			$exists = $this->General->checkIfUserExists($this->data['User']['mobile']);
			$this->Session->write('displayDiv','loginDiv');
				
			$this->set('mobile',$this->data['User']['mobile']);
			if($exists){
				$this->User->recursive = -1;
				$verify = $this->User->find('first', array('fields' => array('User.verify'), 'conditions' => array('User.mobile' => $this->data['User']['mobile'])));

				$resend_stmt = "You are already registered. Login from top.";
				if($verify['User']['verify'] > 0){
					$this->set('mobile',$this->data['User']['mobile']);
					$this->render('/elements/reg_not_logged','ajax');
				}
				else {
					$this->set('mobile',$this->data['User']['mobile']);
					$this->set('page','index');
					$this->render('/elements/reg_logged','ajax');
				}
			}
			else if(isset($this->data['User']['captchaText']) && strtolower($this->data['User']['captchaText']) == $this->Session->read('security_code')){
				$user = $this->General->registerUser($this->data['User']['mobile'],ONLINE_REG);
				$this->set('mobile',$this->data['User']['mobile']);
				if(($user['User']['dnd_flag'] == 0 &&  $this->General->checkTimeSlot()) || TRANSFLAG){
					$this->General->sendPassword($this->data['User']['mobile'],$user['User']['syspass'],1);
					$this->render('/elements/after_reg','ajax');
				}
				else {
					$this->render('/elements/dnd_time_reg','ajax');
				}
			}
			else {
				$this->Session->setFlash(__('<span class="errMessage1">Please enter correct verification code.</span>', true));
				$this->render('/elements/captcha_check','ajax');
			}
		}
		else{
			$msg = "Your number seems to be from a new series of mobile numbers.
This might be the reason you are getting the error while registration.

To register you can give a missed call to 7666888676, you'll get the password instantly.";
			$this->General->sendMessage(SMS_SENDER,'91' . $this->data['User']['mobile'],$msg,'market');
			//$this->General->mailToAdmins("Registration Failed", $this->data['User']['mobile'] . " is not a valid number");
			$this->Session->setFlash(__('<span class="errMessage1">Please enter a valid mobile number.</span>', true));
			$this->render('/elements/captcha_check','ajax');
		}
	}

	function restoreReg()
	{
		$page = $_REQUEST['page'];
		if($page == 'index'){
			$this->render('/elements/captcha_check','ajax');
		}
		else{
			$this->render('/elements/login_def','ajax');
		}
	}

	function forgotPasswordCheck($direct=null,$mobile=null){
		if($mobile != null)$this->data['User']['mobile'] = $mobile;
		$exists = $this->General->checkIfUserExists($this->data['User']['mobile']);
		$this->Session->write('displayDiv','forgotPassword');
		if($exists){
			if($direct == '1' || $strtolower($this->data['User']['captchaText']) == $this->Session->read('security_code'))	{
				$this->data['User']['mobile'] = $this->data['User']['mobile'];
				$check = $this->User->find('first',array('fields' => array('syspass','dnd_flag'),'conditions' => array('mobile' => $this->data['User']['mobile'])));
				if(empty($check['User']['syspass']) || $check['User']['syspass'] == 'NULL' || SENDFLAG == '0'){
					$password = $this->General->generatePassword(4); //generate 4 character password
				}
				else {
					$password = $check['User']['syspass'];
				}

				$this->data['User']['password'] = $this->Auth->password($password); //encrypted password using hash salt
				//echo "<script>showLoader2('messagePopUpDiv');</script>";
				if($this->updatePassword($this->data['User']['mobile'],$password,'change')){
					if($direct == null){
						if(($check['User']['dnd_flag'] == 0 &&  $this->General->checkTimeSlot()) || TRANS_FLAG){
							$this->Session->setFlash(__('The password has been sent to your mobile. <br>Enter your password to sign in<br><br>', true));
							$this->set('mobile',$this->data['User']['mobile']);
							//					 /$this->set('forgot', 0);
							$this->render('/elements/login_user','ajax');
						}
						else if($check['User']['dnd_flag'] == 1){
							echo "Your number is registered in DND. Due to new Telecom regulations we cannot send you SMS. Kindly de-register by calling 1909";
							$this->autoRender = false;
						}
						else {
							echo "Due to New Telecom regulations we cannot send you SMS this time. Kindly re-try " . TIME_SLOT_PERIOD . " only ";
							$this->autoRender = false;
						}
					}
						
				}
			}
			else {
				$this->Session->setFlash(__('You have entered wrong code. Please try again<br><br>', true));
				echo "<script>$('popUpDiv').hide();forgetPassword();</script>";
				$this->autoRender = false;
			}

		}
		else {
			echo 'You are not registered. <a href="javascript:void(0);" onclick="register();">Click here to register.</a>';
			$this->autoRender = false;
		}

		//echo "<script> centerPos('popUpDiv');</script>";
	}

	function newDashboard(){
		$recommended = $this->General->getRecommendedPacks($this->Session->read('Auth.User.id'),4);
		//$recentMessages = $this->General->getRecentMessages('',20,1);
		$this->set(compact('recommended'));
		$this->render('/elements/new_dashboard','ajax');
	}

	function getRecentMsgs(){
		$recentMessages = $this->General->getRecentMessages('',20,1);
		$this->set(compact('recentMessages'));
		$this->render('/elements/rm','ajax');
	}

	function dashboard() {
		/*$news = $this->General->getRecentMessages(DASH_NEWS,5,1);
		 $bolly = $this->General->getRecentMessages(DASH_BOLLY,5,1);
		 $tweet = $this->General->getRecentMessages(DASH_TWEET,5,1);
		 $cricket = $this->General->getRecentMessages(DASH_CRICKET,5,1);
		 $market = $this->General->getRecentMessages(DASH_MARKET,5,1);
		 $tips = $this->General->getRecentMessages(DASH_TIPS,5,1);

		 $this->set(compact('news','bolly','tweet','cricket','market','tips'));
		 */

		if(isset($_POST['upper']))
		$upper = $_POST['upper']+DASH_QRY_LMT;
		else
		$upper = 0;

		$limitStr = $upper.','.DASH_QRY_LMT;
		$data = $this->General->getRecentMessages('',$limitStr,1);
		$this->set(compact('data','upper'));

		if(isset($_POST['upper']))
		$this->render('/elements/dashboard_box');
		else
		$this->render('/elements/dashboard');


	}

	function refreshDashBox(){
		$par = $_POST['par'];
		$cat_map = array('news' => DASH_NEWS,'bolly' => DASH_BOLLY,'tweet' => DASH_TWEET,'cricket' => DASH_CRICKET,'market' => DASH_MARKET,'tips' => DASH_TIPS);
		$this->set('logs',$this->General->getRecentMessages($cat_map[$par],5,1));
		$this->set('num',$_POST['num']);
		$this->render('/elements/insideDashElem','ajax');
	}

	function activePackages(){
		//active packages
		$resultActive = $this->PackagesUser->find('all', array(
			'fields' => array('PackagesUser.*', 'packages.name','packages.url', 'packages.id','packages.price','packages.created','packages.validity'),
			'conditions' => array('PackagesUser.user_id' => $this->Session->read('Auth.User.id'),'PackagesUser.active' => '1'),
			'joins' => array(
		array(
					'table' => 'packages',
					'type' => 'inner',
					'conditions'=> array('PackagesUser.package_id = packages.id')
		)

		),
			'order' => 'PackagesUser.start desc'));
		$this->set('resultActive',$resultActive);
		$this->render('/elements/active_packages');

	}

	function expiredPackages()
	{
		//Used packages
		$resultUsed = $this->PackagesUser->find('all', array(
			'fields' => array( 'PackagesUser.package_id','PackagesUser.trial_flag','max(PackagesUser.start) as start', 'max(PackagesUser.end) as end','packages.name','packages.url','packages.price','packages.created','packages.validity'),
			'conditions' => array('PackagesUser.user_id' => $this->Session->read('Auth.User.id'),'PackagesUser.active' => '0',
				'PackagesUser.package_id not in (Select package_id from packages_users where user_id= "'.$this->Session->read('Auth.User.id').'" and active=1)'),
				'group' => 'PackagesUser.package_id',
				'joins' => array(
		array(
						'table' => 'packages',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.package_id = packages.id')
		)
			
		),
				'order' => 'PackagesUser.end desc'));

		$this->set('resultExpired',$resultUsed);
		$this->render('/elements/expired_packages');
	}

	function transactions()
	{
		$this->Transaction->recursive = -1;
		$subQuery1 = "SELECT Transaction.user_id as user_id, Transaction.package_id as package_id, Transaction.message_id as message_id,Transaction.amount as amount, Transaction.type as type, '' as app_id, Transaction.timestamp as timestamp, packages.name as name, categories.name as category_name FROM transactions AS `Transaction`
                                                                left JOIN packages  ON (`package_id` = `packages`.`id`)
                                                                left JOIN categories_packages  ON (`categories_packages`.`package_id` = `packages`.`id`)
                                                                left JOIN categories  ON ( `categories`.`id` = `categories_packages`.`category_id` )
                                                                ";
		$subQuery2 = "SELECT AppsTransaction.user_id as user_id, '' as package_id, '' as message_id, AppsTransaction.amount as amount, AppsTransaction.type as type, AppsTransaction.app_id as app_id, AppsTransaction.timestamp as timestamp, apps.name as name , '' as category_name  FROM apps_transactions AS `AppsTransaction` left JOIN s_m_s_apps as apps  ON (`app_id` = `apps`.`id`)";
		$subQuery  = "SELECT * FROM ((".$subQuery1.") UNION (".$subQuery2.")) as transactions WHERE user_id = ".$this->Session->read('Auth.User.id')."  ORDER BY timestamp DESC LIMIT 10";
		$resultTransaction = $this->Transaction->query($subQuery);
			
		$this->set('resultTransaction',$resultTransaction);
		$this->render('/elements/transaction');
	}

	function account() {

		//active packages
		$resultActive = $this->PackagesUser->find('all', array(
			'fields' => array('PackagesUser.*', 'packages.name','packages.url', 'packages.id','packages.price','packages.created','packages.validity'),
			'conditions' => array('PackagesUser.user_id' => $this->Session->read('Auth.User.id'),'PackagesUser.active' => '1'),
			'joins' => array(
		array(
					'table' => 'packages',
					'type' => 'inner',
					'conditions'=> array('PackagesUser.package_id = packages.id')
		)

		),
			'limit' => '5'));

		//Used packages
		$resultUsed = $this->PackagesUser->find('all', array(
			'fields' => array( 'PackagesUser.*','packages.name','packages.url','packages.price','packages.created','packages.validity'),
			'conditions' => array('PackagesUser.user_id' => $this->Session->read('Auth.User.id'),'PackagesUser.active' => '0',
				'PackagesUser.package_id not in (Select package_id from packages_users where user_id= "'.$this->Session->read('Auth.User.id').'" and active=1)'),
				'group' => 'PackagesUser.package_id',
				'joins' => array(
		array(
						'table' => 'packages',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.package_id = packages.id')
		)
			
		),
				'limit' => '5'));


		$this->Transaction->recursive = -1;
		$resultTransaction = $this->Transaction->find('all', array(
			'fields' => array( 'Transaction.*','packages.name'),
			'conditions' => array('Transaction.user_id' => $this->Session->read('Auth.User.id')),
			'joins' => array(
		array(
						'table' => 'packages',
						'type' => 'left',
						'conditions'=> array('Transaction.package_id = packages.id')
		)
			
		),
			'order' => array('Transaction.id DESC'),
			'limit' => '5'
			));

			$this->set('resultActive',$resultActive);
			$this->set('resultExpired',$resultUsed);
			$this->set('resultTransaction',$resultTransaction);

			$this->render('/elements/account');

	}

	function draft() {

		$this->Draft->recursive = -1;
		$resultTransaction = $this->Draft->find('all', array(
			'fields' => array('Draft.*'),
			'conditions' => array('Draft.user_id' => $this->Session->read('Auth.User.id')),
			'order' => array('Draft.draftedOn DESC')
		));
		$i = 0;
		foreach($resultTransaction as $draft){
			if($draft['Draft']['type'] == "frd"){
				$this->Friendlist->recursive = -1;
				$nicknames = $this->Friendlist->find('all',array('fields' => array('Friendlist.nickname'),'conditions' => array(
					'Friendlist.id in ('.$draft['Draft']['friendIds'].')')));
				$str = '';
				$j = 0;
				if(!empty($draft['Draft']['mobile']))
				$str = $draft['Draft']['mobile'];
					
				foreach($nicknames as $nickname){
					if($j > 0)
					$str .= ", ";

					$str .= $nickname['Friendlist']['nickname'];
					$j++;
				}
				$resultTransaction[$i]['Draft']['nicknames'] = $str;
			}
			$i++;
		}
		//$this->printArray($resultTransaction);

		$this->set('resultTransaction',$resultTransaction);

		$this->render('/elements/draft','ajax');

	}

	function wSMS() {
		$friendList = $this->Friendlist->find('all',array('conditions' => array('Friendlist.user_id' => $this->Session->read('Auth.User.id'))));
		$this->set('friendList',$friendList);
		$this->render('/elements/wSMS');
	}

	function deleteDraft(){
		$id = $_POST['id'];
		$this->Draft->delete($id);
		echo "Draft deleted successfully";
		$this->autoRender = false;
	}

	function setting() {
		$resultActive = $this->PackagesUser->find('all', array(
			'fields' => array('PackagesUser.*', 'packages.name','packages.url', 'packages.id','packages.price','packages.created','packages.validity'),
			'conditions' => array('PackagesUser.user_id' => $this->Session->read('Auth.User.id'),'PackagesUser.active' => '1'),
			'joins' => array(
		array(
					'table' => 'packages',
					'type' => 'inner',
					'conditions'=> array('PackagesUser.package_id = packages.id')
		)

		),
			'limit' => '10',
			'order' => 'PackagesUser.start desc'));
		$this->set('resultActive',$resultActive);
		$this->render('/elements/setting');
	}
	function getMobDthRecharge() {
		$this->Category->recursive = -1;
		$category = $this->Category->query("SELECT Package.id,Package.name,Package.code FROM categories as Category,packages as Package,categories_packages WHERE Category.id = categories_packages.category_id AND Package.id = categories_packages.package_id AND Category.name = 'Mobile Prepaid Recharge' ORDER BY Package.name");
		//$this->printArray($category);
		$mob_recharge_operators = $category;

		$this->set( 'operators' , $mob_recharge_operators );
		$this->render('/elements/mob_dth_recharge');
	}
	function getMobRecharge() {
		$this->Category->recursive = -1;
		$category = $this->Category->query("SELECT Package.id,Package.name,Package.code FROM categories as Category,packages as Package,categories_packages WHERE Category.id = categories_packages.category_id AND Package.id = categories_packages.package_id AND Category.name = 'Mobile Prepaid Recharge' ORDER BY Package.name");
		//$this->printArray($category);
		$mob_recharge_operators = $category;

		$this->set( 'operators' , $mob_recharge_operators );

		$this->render('/elements/mob_recharge');
	}
	function getDthRecharge() {
		$this->Category->recursive = -1;
		$category = $this->Category->query("SELECT Package.id,Package.name,Package.code FROM categories as Category,packages as Package,categories_packages WHERE Category.id = categories_packages.category_id AND Package.id = categories_packages.package_id AND Category.name = 'DTH Prepaid Recharge' ORDER BY Package.name");
		//$this->printArray($category);
		$mob_recharge_operators = $category;

		$this->set( 'operators' , $mob_recharge_operators );

		$this->render('/elements/dth_recharge');
	}

	function details() {
		$this->render('/elements/personal_details','ajax');
	}


	function changeDetails(){

		$this->data['User']['id'] = $this->Session->read('Auth.User.id');
		if ($this->User->save($this->data)) {
			$_SESSION['Auth']['User']['name'] = $this->data['User']['name'];
			$_SESSION['Auth']['User']['email'] = $this->data['User']['email'];
			$_SESSION['Auth']['User']['gender'] = $this->data['User']['gender'];
			$_SESSION['Auth']['User']['city'] = $this->data['User']['city'];
			$_SESSION['Auth']['User']['dob'] = $this->data['User']['dob']['year'].'-'.$this->data['User']['dob']['month'].'-'.$this->data['User']['dob']['day'];
			$this->Session->setFlash(__('Details Saved Successfully', true));
			$this->render('/elements/personal_details','ajax');
		}
		else {
			$this->Session->setFlash(__('Please enter proper email id', true));
			$this->render('/elements/personal_details','ajax');
		}

	}

	function billDetails(){

		$data = $this->billing_user->find('first',array('conditions' => array('billing_user.user_id' => $this->Session->read('Auth.User.id'))));

		if(empty($data)){
			$data['billing_user']['name'] = $this->Session->read('Auth.User.name');
			$data['billing_user']['city'] = $this->Session->read('Auth.User.city');
			$data['billing_user']['state'] = $this->Session->read('Auth.User.state');
			$data['billing_user']['email'] = $this->Session->read('Auth.User.email');
		}

		$this->set('data',$data);
		$this->render('/elements/billing_details');
	}

	function changeBillDetails(){

		$data = $this->billing_user->find('first',array('conditions' => array('billing_user.user_id' => $this->Session->read('Auth.User.id'))));
		if(empty($data)){
			$this->billing_user->create();
		}
		else {
			$this->data['billing_user']['id'] = $data['billing_user']['id'];
		}

		$this->data['billing_user']['user_id'] = $this->Session->read('Auth.User.id');
		$this->data['billing_user']['name'] = $_POST['custName'];
		$this->data['billing_user']['address'] = $_POST['custAddress'];
		$this->data['billing_user']['city'] = $_POST['custCity'];
		$this->data['billing_user']['state'] = $_POST['custState'];
		$this->data['billing_user']['zip'] = $_POST['custPinCode'];
		$this->data['billing_user']['email'] = $_POST['custEmailId'];
		$this->data['billing_user']['other'] = $_POST['otherNotes'];

		$this->set('data',$this->data);

		if ($this->billing_user->save($this->data)) {
			$this->Session->setFlash(__('Details Saved Successfully', true));
			$this->render('/elements/billing_details','ajax');
		}
		else {
			$this->Session->setFlash(__('Entered some wrong data !!', true));
			$this->render('/elements/billing_details','ajax');
		}
	}

	function payment() {
		$data = $this->billing_user->find('first',array('conditions' => array('billing_user.user_id' => $this->Session->read('Auth.User.id'))));
		if(empty($data)){
			$data['billing_user']['name'] = $this->Session->read('Auth.User.name');
			$data['billing_user']['city'] = $this->Session->read('Auth.User.city');
			$data['billing_user']['state'] = $this->Session->read('Auth.User.state');
			$data['billing_user']['email'] = $this->Session->read('Auth.User.email');
		}
		$this->set('data',$data);
		$this->render('/elements/payment');
	}

	function recharge() {
		$data = $this->billing_user->find('first',array('conditions' => array('billing_user.user_id' => $this->Session->read('Auth.User.id'))));
		if(empty($data)){
			$data['billing_user']['name'] = $this->Session->read('Auth.User.name');
			$data['billing_user']['city'] = $this->Session->read('Auth.User.city');
			$data['billing_user']['state'] = $this->Session->read('Auth.User.state');
			$data['billing_user']['email'] = $this->Session->read('Auth.User.email');
		}
		$this->set('data',$data);
		$this->render('/elements/recharge');
	}

	function cheque() {
		$this->render('/elements/cheque');
	}

	function custSupport() {
		$this->render('/elements/custSupport');
	}
	function rechargeBalance(){
		$this->Session->write('param','recharge');
		$this->redirect("/users/view/");
	}

	function userPasswordChange(){
		$this->Session->write('param','pass');
		$this->redirect("/users/view/");
	}

	function redeemCredits($ccCode){
		$this->Session->write('ccCode',$ccCode);
		$this->Session->write('param','freeCredits');
		$this->redirect("/users/view/");
	}

	function packageGallery(){
		$this->Session->write('param','package');
		$this->redirect("/users/view/");
	}

	function package($category = null, $package = null) {
		//all major categories
		if(isset($_REQUEST['category'])){
			$category = $_REQUEST['category'];
			$package = $_REQUEST['package'];
		}

		$this->Category->recursive = 1;
		$this->Category->filterBindings(array('Message','Package'));
		$resultCategories= $this->Category->find('all', array(
			'fields' => array('Category.name', 'Category.id'),
			'conditions' => array('Category.parent' =>'0','Category.toshow' => '1'),
			'order' => 'FIELD(id,'.CATEGORIES_IN_ORDER.')',
		));

		$page = 1;
		if($category == null){
			$resultPack= $this->getDisplayPackages(POPULAR_ARRAY,PACK_LIMIT_L,$page);
			$count = count(explode(',',POPULAR_ARRAY));

			$this->set('packData',$resultPack);
			$this->set('limit',PACK_LIMIT_L);
			$this->set('layout','L');
			$this->set('category','pop');
		}
		else {
				
			if(empty($package)){
				$layout = $this->General->getCategoryLayout($category);
				if($layout == 'L'){
					$limit = PACK_LIMIT_L;
				}
				else if($layout == 'G'){
					$limit = PACK_LIMIT_G;
				}
				$resultPack = $this->General->getAllPackages(array($category),$limit,$page);
				$this->set('packData',$resultPack);
				$this->set('limit',$limit);
				$this->set('layout',$layout);
			}
			else {
				$this->set('package',$package);
			}
				
			$count = $this->General->getCountPackages(array($category));
			$this->set('category',$category);
		}

		$this->set('page',$page);
		$this->set('count',$count);
		$this->set('resultCategories',$resultCategories);
		if(isset($_REQUEST['category'])){
			$this->render('/elements/package','ajax');
		}
		else $this->render('/elements/package');
	}

	function forgotPassword(){
		//		/$this->Session->setFlash(__('', true));
	}

	function resendPassword() {
		$mobile_num = $_POST['mob'];
		$password = $this->General->extractPassword($mobile_num);
		echo "Your password has been sent to <strong>".$mobile_num."</strong>";
		$this->General->sendMessage('',$mobile_num,'Your password is '.$password.'
Login to smstadka.com & Subscribe to your favorite SMS packages','priority',3);
		$this->autoRender = false;
		//sending SMS to be done here
		//$this->set('messagePopUpDiv','Password sent to '.$mobile_num);
		//$this->render('/users/forgot_password','ajax');
	}

	function register() {
		$this->render('/users/register','ajax');
	}

	function registerCheck(){

		$exists = $this->General->checkIfUserExists($this->data['User']['mobile']);
		if(!$exists){
			if(isset($this->data['User']['captchaText']) && strtolower($this->data['User']['captchaText']) == $this->Session->read('security_code')){
				$mobileDetails = $this->General->getMobileDetails($this->data['User']['mobile']);
				if($mobileDetails['0'] != '' || $mobileDetails['1'] != ''){
					$user = $this->General->registerUser($this->data['User']['mobile'],ONLINE_REG);
					$this->set('mobile',$this->data['User']['mobile']);
					if(($user['User']['dnd_flag'] == 0 &&  $this->General->checkTimeSlot()) || TRANS_FLAG){
						$this->General->sendPassword($this->data['User']['mobile'],$user['User']['syspass'],1);
						$this->render('/elements/after_reg','ajax');
					}
					else {
						$this->render('/elements/dnd_time_reg','ajax');
					}
				}
				else
				{
					$msg = "Your number seems to be from a new series of mobile numbers.
This might be the reason you are getting the error while registration.

To register you can give a missed call to 7666888676, you'll get the password instantly.";
					$this->General->sendMessage(SMS_SENDER,'91' . $this->data['User']['mobile'],$msg,'market');
					//$this->General->mailToAdmins("Registration Failed", $this->data['User']['mobile'] . " is not a valid number");
					$this->Session->setFlash(__('Please enter a valid mobile number.', true));
					$this->render('/elements/captcha_check','ajax');
				}
			}
			else {
				$this->Session->setFlash(__('You have entered wrong code. Please try again<br><br>', true));
				echo "<script>register();</script>";
				$this->autoRender = false;
			}
		}
		else {
			$verify = $this->User->find('first', array('fields' => array('User.verify'), 'conditions' => array('User.mobile' => $this->data['User']['mobile'])));
			$this->set('mobile',$this->data['User']['mobile']);
			if($verify['User']['verify'] > 0){
				//$this->Session->setFlash(__('You are already registered with us, If you have not received the password yet, just <b>give a miss call on 07666888676</b> to get it.', true));
				$this->render('/elements/reg_not_logged','ajax');
			}
			else
			{
				//$this->Session->setFlash(__('You are already registered with us. To login, please enter the password below for', true));
				$this->set('page','popup');
				$this->render('/elements/reg_logged','ajax');
			}
			//$this->render('/elements/login_user','ajax');
		}
	}

	function fetchPackages(){
		$catid = $_POST['data'];
		$page = 1;

		if($catid == 'pop'){
			$layout = 'L';
			$limit = PACK_LIMIT_L;
			$resultPack= $this->getDisplayPackages(POPULAR_ARRAY,$limit,$page);
			$count = count(explode(',',POPULAR_ARRAY));
		}
		else {
			$layout = $this->General->getCategoryLayout($catid);
			if($layout == 'L'){
				$limit = PACK_LIMIT_L;
			}
			else if($layout == 'G'){
				$limit = PACK_LIMIT_G;
			}
			$resultPack = $this->General->getAllPackages(array($catid),$limit,$page);
			$count = $this->General->getCountPackages(array($catid));
		}

		$this->set('packData',$resultPack);
		$this->set('category',$catid);
		$this->set('page',$page);
		$this->set('count',$count);
		$this->set('limit',$limit);
		$this->set('layout',$layout);
		$this->render('/elements/packages','ajax');
	}

	function fetchMorePackages(){
		$catid = $_POST['id'];
		$page = $_POST['page'];
		$count = $_POST['count'];

		if($catid == 'pop'){
			$layout = 'L';
			$limit = PACK_LIMIT_L;
			$resultPack= $this->getDisplayPackages(POPULAR_ARRAY,$limit,$page);
		}
		else {
			$layout = $this->General->getCategoryLayout($catid);
			if($layout == 'L'){
				$limit = PACK_LIMIT_L;
			}
			else if($layout == 'G'){
				$limit = PACK_LIMIT_G;
			}
			$resultPack = $this->General->getAllPackages(array($catid),$limit,$page);
		}
		$this->set('packData',$resultPack);
		//$this->printArray($resultPack);
		$this->set('limit',$limit);
		$this->set('page',$page);
		$this->set('count',$count);
		$this->set('layout',$layout);

		if(!$this->Session->read('Auth.User')){
			$this->set('bcrumb',1);
		}
		$this->render('/elements/packages','ajax');
	}

	function passwordChange(){
		$this->render('/elements/change_password','ajax');
	}

	function changePassword($par=null){
		//all other checkings should be from javascript side

		//here i am assuming all other data is correct
		$this->User->recursive = -1;
		$password = $this->User->find('first', array('fields' => array('User.password'), 'conditions' => array('User.mobile' => $this->Session->read('Auth.User.mobile'))));

		if($password['User']['password'] != $this->Auth->password($this->data['User']['pass1']))	{
			$this->set('errFlag','1');
			if($par != null){
				$this->set('par',$par);
			}
			$this->render('/elements/change_password','ajax');
		}
		else{
			if($this->updatePassword($this->Session->read('Auth.User.mobile'),$this->data['User']['pass2'],'change','in')){
				echo "Password changed successfully";
				echo "<script>$('notice').innerHTML = '';</script>";
				echo "<script>$('tabsBgMainNotice').removeClassName('tabsBgMainNotice');</script>";
				echo "<script>$('tabsBgMainNotice').addClassName('tabsBgMain');</script>";
			}
			else {
				echo "Password can not be changed";
			}

			$this->autoRender = false;
		}
	}

	function itsfine(){
		$this->User->updateAll(array('User.passflag' => '1'), array('User.id' => $this->Session->read('Auth.User.id')));
		$_SESSION['Auth']['User']['passflag'] = 1;
		$this->autoRender = false;
	}

	function updatePassword($mobile,$password,$type,$changeP=null){


		if($type == "new" || $changeP == null){
			$passFlag = '0';
			$sysPass = $password;
		}
		else if($type == "change"){
			$passFlag = '1';
			$sysPass = 'NULL';
		}

		if($this->User->updateAll(array('User.password' => "'".$this->Auth->password($password)."'", 'User.passflag' => $passFlag, 'User.syspass' => "$sysPass"), array('User.mobile' => $mobile))){
			if($changeP == null)
			$this->General->sendPassword($mobile,$password,0);

			if($this->Session->read('Auth.User')){
				$_SESSION['Auth']['User']['passflag'] = $passFlag;
			}

			return true;
		}

		return false;
	}



	/*function userLogin($div){
	 $this->set('forgot', 1);
	 $this->set('toshow',1);
	 $this->set('displayDiv',$div);
	 $this->render('/elements/login_user','ajax');
	 } */

	function login() {

		if($this->Session->read('Auth.User')) {
			if($this->Session->read('Auth.redirect')){
				if($this->Session->read('Auth.redirect') == "/")
				$this->redirect('/users/view/');
				else {
					$this->redirect($this->Session->read('Auth.redirect'));
				}
			}
			else {
				$this->redirect('/users/view/');
			}
		}

		$this->Category->recursive = 1;
		$this->Category->filterBindings(array('Message'));

		$mainData = $this->Category->find('all', array('conditions' => array('Category.parent' => '0','Category.toshow' => '1', 'Category.name not like "SMS%"')));

		$this->set('mainData', $mainData);
	}


	function logout() {
		//$this->Session->setFlash('Good-Bye');
		$this->redirect($this->Auth->logout());
	}

	function afterLogin(){
		$this->data['User']['mobile'] = $_POST['mobile'];
		$this->data['User']['password'] = $this->Auth->password($_POST['password']);
		$param = $_POST['param'];

		$this->User->recursive = -1;
		$usrData = $this->User->find('first',array('conditions' => array('User.mobile' => $this->data['User']['mobile'],'User.password' => $this->data['User']['password'])));
		if(empty($usrData)){
			$user = $this->User->find('first',array('conditions' => array('User.mobile' => $this->data['User']['mobile'])));
			$password = $this->General->generatePassword(4);
			if(empty($user) && $_POST['password'] == $password){
				//This means it is coming thru misscall, to set verify = -1
				$user = $this->General->registerUser($this->data['User']['mobile'],MISSCALL_REG);
				$this->Session->write('Auth',$user);
				echo '1';
			}else {
				echo '0';
			}
			$this->autoRender = false;
		}
		else {
			if($usrData['User']['verify'] > 0){
				$this->User->query("UPDATE users SET verify=0,login_count = login_count + 1, modified = '" .date('Y-m-d H:i:s'). "' WHERE mobile=".$this->data['User']['mobile']);
				//$this->User->updateAll(array('User.verify' => 0,'User.modified = "'.date('Y-m-d H:i:s'). '"'), array('User.mobile' => $this->data['User']['mobile']));
			}
			else {
				$this->User->query("UPDATE users SET login_count = login_count + 1, modified = '" .date('Y-m-d H:i:s'). "' WHERE mobile=".$this->data['User']['mobile']);
				//$this->User->updateAll(array('User.modified = "'.date('Y-m-d H:i:s'). '"'), array('User.mobile' => $this->data['User']['mobile']));
			}
			if($param != 'retail')
			$this->Session->write('Auth',$usrData);
			if($this->Session->check('usrParams') && $param == 'in') {
				$par = $this->Session->read('usrParams.par');

				if($par == 'msg'){
					$this->beforeSend($this->Session->read('usrParams.id'),$this->Session->read('usrParams.to'));
				}
				else if($par == 'pck'){
					$this->beforeSubscribe($this->Session->read('usrParams.id'),$this->Session->read('usrParams.type'),$this->Session->read('usrParams.trial'));
				}
				else if($par == 'app'){
					$this->Session->write('param','app');
					$this->Session->write('app',$this->Session->read('usrParams.controller'));
					$this->Session->write('about',$this->Session->read('usrParams.about'));
					echo '1';
					$this->autoRender = false;
				}else if($par == 'redeem'){
					echo '4';
					$this->autoRender = false;
				}
				$this->Session->delete('usrParams');
			}
			else if($param == 'top' && $_SERVER['HTTP_REFERER'] != SITE_NAME."index.php") {
				$this->autoRender = false;
			}
			else if($param == '') {
				echo '2';
				$this->autoRender = false;
			}
			else if($param == 'retail') {
				if($usrData['User']['group_id'] == SUPER_DISTRIBUTOR || $usrData['User']['group_id'] == DISTRIBUTOR || $usrData['User']['group_id'] == RETAILER || $usrData['User']['group_id'] == ADMIN){
					$this->Session->write('Auth',$usrData);
					echo '3';
				}
				else {
					echo '0';
				}
				$this->autoRender = false;
			}else if($param == 'redeem') {
				echo '4';
				$this->autoRender = false;
			}
			else {
				echo '1'; //if no paramters are set in session
				$this->autoRender = false;
			}
		}


	}

	function rightHeader(){
		$this->render('/elements/right_header','ajax');
	}

	function beforeSend($id = null,$who=null, $from=null){
		if($id == null)
		$id = $_REQUEST['id'];
		if($who == null)
		$who = $_REQUEST['to'];
		if ($this->Session->read('Auth.User')) { //if user is logged in
			if(empty($from)){
				$from = $_REQUEST['from'];
			}
			if(empty($from) || $from == 'msg'){
				$this->Message->recursive = -1;
				$msgData = $this->Message->find('first',array('conditions' => array('Message.id' => $id)));
			}
			else if($from == 'log'){
				$this->Log->recursive = -1;
				$msg = $this->Log->find('first',array(
						'fields' => array('Log.*','packages.name'),
						'conditions' => array('Log.id' => $id),
						'joins' => array(
				array(
								'table' => 'packages',
								'type' => 'left',
								'conditions'=> array('Log.package_id = packages.id')
				)
				))
				);
				$msgData['Message'] = $msg['Log'];
				$msgData['Message']['id'] = '';
				$msgData['Message']['title'] = $msg['packages']['name'];

			}
				
			if($who == "me"){
				$this->set('msgId',$id);
				$this->render('/elements/sendsms','ajax');
			}
			else if($who == "frnds"){
				$this->Friendlist->recursive = -1;
				$friendList = $this->Friendlist->find('all',array('conditions' => array('Friendlist.user_id' => $this->Session->read('Auth.User.id'))));
					
				$this->set('msgData',$msgData);
				$this->set('friendList',$friendList);
				$this->render('/elements/friendlist_popup','ajax');
			}

			//$this->render('/elements/recharge','ajax');
		}
		else {
			$this->render('/elements/login_def','ajax');
			$msgPars = array();
			$msgPars['par'] = 'msg';
			$msgPars['to'] = $who;
			$msgPars['id'] = $id;
			$this->Session->write('usrParams',$msgPars);
		}
	}

	function loginWin(){
		$msgPars['par'] = 'redeem';
		$this->Session->write('usrParams',$msgPars);
		$this->set('flag','redeem');
		$this->render('/elements/login_user','ajax');
	}

	function beforeSubscribe($id=null,$type=null,$trial=null){
		if($id == null)
		$id = $_REQUEST['id'];
		if($type == null)
		$type = $_REQUEST['type'];
		if($trial == null){
			if(isset($_REQUEST['trial']))
			$trial = $_REQUEST['trial'];
			else $trial = 0;
		}
		if ($this->Session->read('Auth.User')){ //if user is logged in

			$id = $this->objMd5->decrypt($id,encKey);
			$this->Package->recursive = -1;
			$pack_name = $this->Package->find('first',array('fields' => array('Package.name,Package.url,Package.trial_days'),'conditions' => array('Package.id' => $id,'Package.toshow' => 1)));
				
			$userData = $this->PackagesUser->find('first', array('fields' => array('SUM(PackagesUser.trial_flag) as trial','SUM(PackagesUser.active) as active','COUNT(PackagesUser.id) as ids'),'conditions' => array('PackagesUser.package_id' => $id,'PackagesUser.user_id' => $this->Session->read('Auth.User.id'))));
				
			if(empty($pack_name)){
				echo "Sorry, this package is not active now.";
				$this->autoRender = false;
			}
			else {
				$name = $pack_name['Package']['name'];
				$url =  $pack_name['Package']['url'];
				if($type == "sub"){
					$amount = $this->General->getPackageCharge($id);
					$price = $this->General->getBalance($this->Session->read('Auth.User.id')) - $amount;

					if(empty($userData) || $userData['0']['active'] == 0 || ($userData['0']['trial']*$userData['0']['ids']*$userData['0']['active'] == 1 && $trial == 0)){
						if($trial == 1 && $userData['0']['trial'] != 0){
							echo "You have already tried this package before. You can susbcribe to the monthly package";
							$this->autoRender = false;
						}
						else if($price >= 0 || $trial == 1){
							$this->set('balance', $price);
							$this->set('packageName', $name);
							$this->set('packageUrl', $url);
							$this->set('trial', $trial);
							$this->set('trial_days',$pack_name['Package']['trial_days']);

							$this->render('/elements/subscribe','ajax');
							$draft_id = $this->Draft->find('first',array('fields' => array('Draft.id'),'conditions' => array('Draft.user_id' => $this->Session->read('Auth.User.id'), 'Draft.refid' =>$id,'Draft.type' => 'pck')));
							$this->Draft->delete($draft_id['Draft']['id']);
						}
						else {
							//$this->General->draftUpdate('pck',$name,$id,$this->General->getPackageCharge($id),'');
							echo $this->General->getDefaultRechargeMessage("package",$amount,$this->objMd5->Encrypt($id, encKey));
							$this->autoRender = false;
						}
					}
					else {
						echo "You are subscribed to this package already.";
						$this->autoRender = false;
					}
				}
				else if($type == "unsub"){
					if(!empty($userData) && $userData['0']['active'] == 1){
						$this->set('packageName', $name);
						$this->set('packageUrl', $url);
						$this->render('/elements/unsubscribe','ajax');
					}
					else {
						echo "This package is not active";
						$this->autoRender = false;
					}
				}
			}


		}
		else {

			$this->render('/elements/login_def','ajax');
			$pckPars = array();
			$pckPars['par'] = 'pck';
			$pckPars['type'] = $type;
			$pckPars['id'] = $id;
			$pckPars['trial'] = $trial;
			$this->Session->write('usrParams',$pckPars);

		}

	}

	function getAlltransactions() {
		$this->Transaction->recursive = -1;
		$resultTransaction = $this->Transaction->find('all', array(
			'conditions' => array('Transaction.user_id' => $this->Session->read('Auth.User.id')),
			'order' => array('Transaction.id DESC')
		));
		$this->set('resultTransaction',$resultTransaction);
		//$this->autoRender = false;
		$this->render('/elements/transactions');
	}



	function twitt($url) {

		$content = $this->Message->find('first',array('fields' => array('Message.content'),'conditions' => array('Message.url' => $url)));
		$url = urlencode(SITE_NAME.'messages/view/'.$url);
		/*$ch = curl_init();
		 curl_setopt($ch, CURLOPT_URL, 'http://api.bit.ly/v3/shorten');
		 curl_setopt($ch, CURLOPT_POST      ,1);
		 curl_setopt($ch, CURLOPT_POSTFIELDS, "login=smstadka&apiKey=R_1740de7d233b01239b119b0f3891d521&longUrl=$url&format=txt");
		 curl_setopt($ch, CURLOPT_HEADER, 1);
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		 $data = curl_exec($ch);
		 curl_close($ch);
		 */
		$data= $this->General->getBitlyUrl($url);
		if(strlen($content['Message']['content']) > 105)
		$text = substr($content['Message']['content'],0,105);
		else
		$text = $content['Message']['content'];


		$data = urlencode(str_replace('<br />','',$text))."... more @ ".$data;
		header("Location: http://twitter.com/home?status=".$data);exit;

		$this->autoRender = false;
	}

	function dnd(){
		if(!empty($_SESSION['Auth']['User']['mobile']))
		$this->set('mobile',$_SESSION['Auth']['User']['mobile']);
		else
		$this->set('mobile','');
	}

	function resenddndcode(){
		$this->senddndcode();
		$this->autoRender = false;
	}

	function senddndcode(){
		$code = $this->General->generatePassword('4');
		$number = $this->Session->read('receiver');
		$this->Session->write('dndcode',$code);
		$message = "Your verification code is ".$code." - smstadka.com";
		$this->General->sendMessage('',$number,$message,'dnd');
	}

	function dndverification(){
		if(trim($_REQUEST['code']) == $this->Session->read('security_code')){
			$number = $_REQUEST['number'];
			$tobeblocked = $_REQUEST['tobeblocked'];
			$this->Session->write('tobeblocked',$tobeblocked);
			$this->Session->write('receiver',$number);
			$this->senddndcode();
			/*with resend
			 * echo '<table><tbody><tr>
			 <td  style="padding-top: 0px;" width="350px;" valign="top">
			 <h3>Mobile Number Verification :</h3></td>
			 <td style="padding-top: 10px;">We have sent you a verification code on the number mentioned. Please type the code below and continue. If you do not get the code within 1 minute, <a href="javascript:void(0)" onclick="sendVerCode()">click here</a> to get another code.
			 <br><br><input type="text"  name="mobileVerCode" id="mobileVerCode" /><a href="javascript:void(0)" onclick="dndblock()">Block</a><br><br><div id="dnderrordiv" name="dnderrordiv" style="display:none;">
			 </td>
		  </tr>
		  </tbody>
		  </table>^^^^1' ;
		  */
			echo '<table><tbody><tr>
		  	<td  style="padding-top: 0px;" width="350px;" valign="top">
		  		<h3>Mobile Number Verification:</h3></td>
		  		<td style="padding-top: 10px;">We have sent you a verification code on the number mentioned. Please type the code below and continue.  		  		
		  		<br><br><input type="text"  name="mobileVerCode" id="mobileVerCode" /><br><br><a href="javascript:void(0)" onclick="dndblock()"><img src="/img/butBlock.png"></a><br><br><div id="dnderrordiv" name="dnderrordiv" style="display:none;">		  		
		  	</td>		  	
		  </tr>
		</tbody>
		</table>^^^^1' ;
		}else{
			echo "Verification code did not match. Please try again or reload the code and try again^^^^0";
		}


		$this->autoRender = false;

	}

	function dndblock(){
		if(trim($_REQUEST['code']) == $this->Session->read('dndcode')){

			///enter in the database
			$tobeblockedArr = explode(",",$this->Session->read('tobeblocked'));
			$arrCnt = count($tobeblockedArr);
			$numStrArr = array();
			$i = 0;
			for($k=0;$k<$arrCnt;$k++){
				$number2bblocked = trim($tobeblockedArr[$k]);
				if($number2bblocked != "" && $this->Session->read('receiver') != $number2bblocked){
					$numStrArr[$i] = $number2bblocked;
					$this->User->query("insert into dnd(receiver,sender) values ('".$this->Session->read('receiver')."','".$number2bblocked."')");
					$i++;
				}
			}
			//ends

			//kill sessions
			$this->Session->delete('receiver');
			$this->Session->delete('tobeblocked');
			$this->Session->delete('security_code');
			$this->Session->delete('dndcode');
			//ends
			echo 'You have successfully blocked following number(s)<br>'.implode(",",$numStrArr).'.<br> <a href="/users/dnd">Click here</a> to block more numbers. ^^^^1' ;
		}else{
			echo "Verification code did not match. Please try again.^^^^0";
		}


		$this->autoRender = false;

	}



	function paymentResponse($response){
		$this->Ssl->force();
		/*echo "<pre>";
		 print_r($_SERVER);
		 echo "----------------------";
		 print_r($_POST);
		 echo "----------------------";
		 print_r($_GET);
		 echo "----------------------";
		 print_r($_REQUEST);exit;

		 echo "</pre>";
		 exit;*/

		if(trim($response) != ''){
			if(trim($_SERVER['SERVER_NAME']) == 'www.smstadka.com' && $_POST['responseparams']){
				///updating database
				$flag = $_POST['flag'];
				$responseparams = $_POST['responseparams'];
				$responseparamsArr = explode("|",$responseparams);
				$dPayRefID = $responseparamsArr[0];
				$dPayFlag = $responseparamsArr[1];
				$dPayCountry = $responseparamsArr[2];
				$dPayCurrency = $responseparamsArr[3];
				$dPayOther = $responseparamsArr[4];
				$dPayOrderId = $responseparamsArr[5];
				$dPayAmt = $responseparamsArr[6];
				$dPayOtherDec = $this->objMd5->Decrypt($dPayOther, encKey);
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
				mysql_select_db(DB_DB);

				$res = mysql_query("select user_id,other_details from payment where id = '".$dPayOrderId."'");
					
				while ($row = mysql_fetch_assoc($res)) {
					$userid = $row['user_id'];
					$other_details = $row['other_details'];
				}

				if(trim($other_details) == trim($dPayOtherDec)){
					mysql_query("update  payment set directpay_ref_id = '".$dPayRefID."',flag='".$dPayFlag."',other_details='".$dPayOther."',mer_order_no='".$dPayOrderId."',amount='".$dPayAmt."',response_txt='".$responseparams."',end_time='".date('Y-m-d H:i:s')."' where id = '".$dPayOrderId."'");

					//testing

					/*$res = mysql_query("select user_id from payment where id = '".$dPayOrderId."'");

					while ($row = mysql_fetch_assoc($res)) {
					$userid = $row['user_id'];
					}
					if(trim($userid) == '8'){
					mysql_query("insert into transactions (user_id,amount,type,timestamp) values ('".$userid."','".$dPayAmt."','".TRANS_USER_CREDIT."','".date('Y-m-d H:i:s')."')");
					echo "Payment Successful!";
					}
					$this->General->balanceUpdate($dPayAmt,'add');
					*/
					//ends
						
					$mailBody = "Payment: Direcpay<br/>";
					if($response == 1 && strtolower($dPayFlag) == 'success'){
						mysql_query("insert into transactions (user_id,amount,type,timestamp) values ('".$userid."','".$dPayAmt."','".TRANS_USER_CREDIT."','".date('Y-m-d H:i:s')."')");
						mysql_query("Update users set balance = balance + ".$dPayAmt." where id = ".$userid);
						$bal = mysql_query("select balance,mobile from users where id = '".$userid."'");
						while ($row1 = mysql_fetch_assoc($bal)) {
							$balance = $row1['balance'];
							$mobileNo = $row1['mobile'];
						}
						$mailBody .= "Amount: ".$dPayAmt;
						$mailBody .= "<br/>Mobile no: ".$mobileNo;
						$mailBody .= "<br/>Status: Successful";
						/*if($_SESSION['Auth']['User']['id'] != "")
							$_SESSION['Auth']['User']['balance'] = $balance;
							*/
						$this->set('status','S');
						$this->set('balance',$balance);
						$this->set('tid',$dPayOrderId);
						$this->layout = 'normal';

					}else{

						$bal = mysql_query("select balance,mobile from users where id = '".$userid."'");
						while ($row1 = mysql_fetch_assoc($bal)) {
							$balance = $row1['balance'];
							$mobileNo = $row1['mobile'];
						}

						$mailBody .= "Amount: ".$dPayAmt;
						$mailBody .= "<br/>Mobile no: ".$mobileNo;
						$mailBody .= "<br/>Status: Failed";

						$this->set('status','F');
						$this->set('balance',$balance);
						$this->set('tid',$dPayOrderId);
						$this->layout = 'normal';
					}
						
					$this->General->mailToAdmins("SMSTadka online payment", $mailBody);
				}else{
					$this->redirect(SITE_NAME.'users/view');
					$this->autoRender = false;
				}
			}else{
				$this->redirect(SITE_NAME.'users/view');
				$this->autoRender = false;
			}
		}else{

			$this->redirect(SITE_NAME.'users/view');
			$this->autoRender = false;
		}
	}



	function prepayment(){
		$id = $_SESSION['Auth']['User']['id'];
		if (!$id) {
			$this->redirect(array('action' => 'login'));
			exit;
		}
		try {
			$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
			if (!$link) {
				echo 'fail';
			}else{
				$rand = rand();

					
				mysql_select_db(DB_DB);
				if(mysql_query("insert into payment(user_id,other_details,start_time) values ('".$_SESSION['Auth']['User']['id']."','".$rand."','".date("Y-m-d H:i:s")."')")){
					$insertid = mysql_insert_id();
					$encVar = $this->objMd5->Encrypt($rand, encKey);

					$data = $this->billing_user->find('first',array('conditions' => array('billing_user.user_id' => $this->Session->read('Auth.User.id'))));
					if(empty($data)){
						$data['billing_user']['name'] = $this->Session->read('Auth.User.name');
						$data['billing_user']['city'] = $this->Session->read('Auth.User.city');
						$data['billing_user']['state'] = $this->Session->read('Auth.User.state');
						$data['billing_user']['email'] = $this->Session->read('Auth.User.email');
					}
						
					$billingDtls = $data['billing_user']['name']."|".$data['billing_user']['address']."|".$data['billing_user']['city']."|".$data['billing_user']['state']."|".$data['billing_user']['zip']."|IN|00|00|00|".$_SESSION['Auth']['User']['mobile']."|".$data['billing_user']['email']."|".$data['billing_user']['other'];
					$delDtls = $data['billing_user']['name']."|".$data['billing_user']['address']."|".$data['billing_user']['city']."|".$data['billing_user']['state']."|".$data['billing_user']['zip']."|IN|00|00|00|".$_SESSION['Auth']['User']['mobile'];
					$reqParams = "201008181000020|DOM|IND|INR|".$_REQUEST['amount']."|".$insertid."|".$encVar."|https://www.smstadka.com/users/paymentResponse/1|https://www.smstadka.com/users/paymentResponse/0|DirecPay";
					//live merchant id = 201008181000020
						
					App::import('vendor', 'CryptAES', array('file'=>'CryptAES.php'));

					//$key = "glBv+xevOenDM5ydcqb9pA==";
					//ini_set("display_errors","on");
					$key = "qcAHa6tt8s0l5NN7UWPVAQ==";
					$aes = new CryptAES();
					$aes->set_key(base64_decode($key));
					$aes->require_pkcs5();

					$reqParams = $aes->encrypt($reqParams);
					$billingDtls = $aes->encrypt($billingDtls);
					$delDtls = $aes->encrypt($delDtls);

					/*$reqParams = $aes->decrypt($reqParams);
					 $billingDtls = $aes->decrypt($billingDtls);
					 $delDtls = $aes->decrypt($delDtls);
					 */
					echo $reqParams."*^!!".$billingDtls."*^!!".$delDtls;
				}else{
					echo "fail";
				}
			}
		}catch(Exception $e){
			echo "fail";
		}
		$this->autoRender = false;
	}

	function prepaymentPayICC(){
		$id = $_SESSION['Auth']['User']['id'];
		if (!$id) {
			$this->redirect(array('action' => 'login'));
			exit;
		}
		try {
			$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
			if (!$link) {
				echo 'fail';
			}else{
				$rand = rand();
				mysql_select_db(DB_DB);
				if(mysql_query("insert into payment(user_id,other_details,merchant_flag,start_time) values ('".$_SESSION['Auth']['User']['id']."','".$rand."',1,'".date("Y-m-d H:i:s")."')")){
					$orderID = mysql_insert_id();
					$mId = ICC_MER_KEY;
					$cCode = CLIENT_CODE;
					$Customparam = $this->objMd5->Encrypt($rand, encKey);;
					$res =  "<input type='text' name='Orderid' id='Orderid' value='".$orderID."' />";
					$res .=  "<input type='text' name='Merchantkey' id='Merchantkey' value='".$mId."' />";
					$res .=  "<input type='text' name='Amount' id='Amount' value='".$_POST['payAmt']."' />";
					$res .=  "<input type='text' name='client_code' id='client_code' value='".$cCode."' />";
					$res .=  "<input type='text' name='Customparam' id='Customparam' value='".$Customparam."' />";
					$res .=  "<input type='text' name='RU' id='RU' value='".SITE_NAME_SEC."users/paymentResponseICC' />";
					echo $res;
				}else{
					echo "fail";
				}
			}
		}catch(Exception $e){
			echo "fail";
		}
		$this->autoRender = false;
	}


	function prepaymentPayOSS(){
		$id = $_SESSION['Auth']['User']['id'];
		if (!$id) {
			$this->redirect(array('action' => 'login'));
			exit;
		}
		try {
			$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
			if (!$link) {
				echo 'fail';
			}else{
				$rand = rand();
				mysql_select_db(DB_DB);
				if(mysql_query("insert into payment(user_id,other_details,merchant_flag,start_time) values ('".$_SESSION['Auth']['User']['id']."','".$rand."',2,'".date("Y-m-d H:i:s")."')")){
					$orderID = mysql_insert_id();
					$Customparam = $this->objMd5->Encrypt($rand, encKey);
					$res = '';
					$res .=  "<input type='text' name='MerchantID' id='MerchantID' value='".OSS_MER_ID."' />";
					$res .=  "<input type='text' name='MerchantAuthKey' id='MerchantAuthKey' value='".OSS_MER_AUTH_KEY."' />";
					$res .=  "<input type='text' name='TranTypeID' id='TranTypeID' value='".OSS_TRAN_TYPE_ID."' />";
					$res .=  "<input type='text' name='TranAuthKey' id='TranAuthKey' value='".OSS_TRAN_AUTH_KEY."' />";
					$res .=  "<input type='text' name='ProductID' id='ProductID' value='".OSS_PROD_ID."' />";
					$res .=  "<input type='text' name='MerchantTranNo' id='MerchantTranNo' value='".$orderID."' />";
					$res .=  "<input type='text' name='OrderQty' id='OrderQty' value='1' />";
					$res .=  "<input type='text' name='Remark' id='Remark' value='remark' />";
					$res .=  "<input type='text' name='ReturnURL' id='ReturnURL' value='".SITE_NAME_SEC."users/paymentResponseOSS' />";
					$res .=  "<input type='text' name='Amount' id='Amount' value='".$_POST['payAmt']."' />";
					$res .=  "<input type='text' name='POSTPARAM1' id='POSTPARAM1' value='".$Customparam."' />";
					echo $res;
				}else{
					echo "fail";
				}
			}
		}catch(Exception $e){
			echo "fail";
		}
		$this->autoRender = false;
	}

	function paymentResponseICC(){
		$this->Ssl->force();
		$response = $_GET;
		//print_r($response);exit;
		//echo implode('|',$response);
		///Array ( [url] => users/paymentResponseICC [orderid] => 89 [TransactionID] => 4350B2582E235953 [amount] => 100 [client_code] => ST9632A [status] => 0 [customparam] => test|test )

		//http://www.dtadka.com/users/paymentResponseICC?orderid=89&TransactionID=4350B2582E235953&amount=100&client_code=ST9632A&status=0&customparam=test|test


		//$this->Ssl->force();
		if($_GET['orderid'] != ''){
			if(trim($_SERVER['SERVER_NAME']) == 'www.smstadka.com' && $_GET['TransactionID']){
				///updating database
				$orderid =  $_GET['orderid'];
				$TransactionID = $_GET['TransactionID'];
				$amount = $_GET['amount'];
				$status = $_GET['status'];
				$customparam = $_GET['customparam'];
				$res_txt = implode('|',$response);
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
				mysql_select_db(DB_DB);

				$used = mysql_query("select id from payment where mer_order_no is not null and id = '".$orderid."'");
				while ($row1 = mysql_fetch_assoc($used)) {
					$userid = $row1['id'];
				}

				if(isset($userid)){
					$this->redirect(SITE_NAME.'users/view');
					$this->autoRender = false;
				}

				$res = mysql_query("select user_id,other_details from payment where id = '".$orderid."'");
					
				while ($row = mysql_fetch_assoc($res)) {
					$userid = $row['user_id'];
					$other_details = $row['other_details'];
				}

				if(trim($other_details) == trim($this->objMd5->Decrypt($customparam, encKey))){
					//status: 0=>success, 1=>failure
					if($status == 0) $updateFlag = 'SUCCESS';
					else $updateFlag = 'FAIL';
					mysql_query("update  payment set directpay_ref_id = '".$TransactionID."',flag='".$updateFlag."',other_details='".$customparam."',mer_order_no='".$orderid."',amount='".$amount."',response_txt='".$res_txt."',end_time='".date('Y-m-d H:i:s')."' where id = '".$orderid."'");

					//testing

					/*$res = mysql_query("select user_id from payment where id = '".$dPayOrderId."'");

					while ($row = mysql_fetch_assoc($res)) {
					$userid = $row['user_id'];
					}
					if(trim($userid) == '8'){
					mysql_query("insert into transactions (user_id,amount,type,timestamp) values ('".$userid."','".$dPayAmt."','".TRANS_USER_CREDIT."','".date('Y-m-d H:i:s')."')");
					echo "Payment Successful!";
					}
					$this->General->balanceUpdate($dPayAmt,'add');
					*/
					//ends
					$statusFlag = '';
					$mailBody = "Payment: I cash card<br/>";
					if($status == 0){
						mysql_query("insert into transactions (user_id,amount,type,timestamp) values ('".$userid."','".$amount."','".TRANS_USER_CREDIT."','".date('Y-m-d H:i:s')."')");
						mysql_query("Update users set balance = balance + ".$amount." where id = ".$userid);
						$bal = mysql_query("select balance,mobile from users where id = '".$userid."'");
						while ($row1 = mysql_fetch_assoc($bal)) {
							$balance = $row1['balance'];
							$mobileNo = $row1['mobile'];
						}

						$mailBody .= "Amount: ".$amount;
						$mailBody .= "<br/>Mobile no: ".$mobileNo;
						$mailBody .= "<br/>Status: Successful";
						/*if($_SESSION['Auth']['User']['id'] != "")
							$_SESSION['Auth']['User']['balance'] = $balance;
							*/
						$statusFlag = 'S';
						/*$this->set('status','S');
						 $this->set('balance',$balance);
						 $this->set('tid',$orderid);
						 $this->layout = 'normal';*/

					}else{

						$bal = mysql_query("select balance,mobile from users where id = '".$userid."'");
						while ($row1 = mysql_fetch_assoc($bal)) {
							$balance = $row1['balance'];
							$mobileNo = $row1['mobile'];
						}
						$statusFlag = 'F';
						$mailBody .= "Amount: ".$amount;
						$mailBody .= "<br/>Mobile no: ".$mobileNo;
						$mailBody .= "<br/>Status: Failed";

						/*$this->set('status','F');
						 $this->set('balance',$balance);
						 $this->set('tid',$orderid);
						 $this->layout = 'normal';*/
					}
					$this->General->mailToAdmins("SMSTadka online payment", $mailBody);
				}else{
					$this->redirect(SITE_NAME.'users/view');
					$this->autoRender = false;
				}
			}else{
				$this->redirect(SITE_NAME.'users/view');
				$this->autoRender = false;
			}
		}else{

			$this->redirect(SITE_NAME.'users/view');
			$this->autoRender = false;
		}
		$this->redirect('/users/iccPaymentDone/'.$this->objMd5->Encrypt($orderid, encKey).'/'.$this->objMd5->Encrypt($balance, encKey).'/'.$this->objMd5->Encrypt($statusFlag, encKey));
	}

	function paymentResponseOSS(){
		$this->Ssl->force();
		$response = $_REQUEST;
		/*Array ( [url] => users/paymentResponseOSS [MERCHANTID] => 2470 [MERCHANTAUTHKEY] => OSS_MR00002470_Minds77 [TRANTYPEID] => 2491 [TRANAUTHKEY] => MindsArray_123_DR [PRODUCTID] => 296 [MERCHANTTRANNO] => 122 [ORDERQTY] => 1 [AMOUNT] => 10 [BASICAMOUNT] => 10 [REMARK] => remark [STATUS] => SUCCESS [CARDTRANID] => 348 [POSTPARAM1] => V2ZVYwQzBjFTZw== [POSTPARAM2] => [POSTPARAM3] => [POSTPARAM4] => [POSTPARAM5] => [POSTPARAM6] => [POSTPARAM7] => [POSTPARAM8] => [POSTPARAM9] => [POSTPARAM10] => )*/
		//print_r($response);exit;
		if(trim($response['MERCHANTAUTHKEY']) == OSS_MER_AUTH_KEY){
			if(trim($_SERVER['SERVER_NAME']) == 'www.smstadka.com' && $_REQUEST['CARDTRANID']){
				///updating database
				$orderid =  $_REQUEST['MERCHANTTRANNO'];
				$TransactionID = $_REQUEST['CARDTRANID'];
				$amount = $_REQUEST['AMOUNT'];
				$status = $_REQUEST['STATUS'];
				$customparam = $_REQUEST['POSTPARAM1'];
				$res_txt = implode('|',$response);
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
				mysql_select_db(DB_DB);

				$used = mysql_query("select id from payment where mer_order_no is not null and id = '".$orderid."'");
				while ($row1 = mysql_fetch_assoc($used)) {
					$recid = $row1['id'];
				}

				if(isset($recid)){
					$this->redirect(SITE_NAME.'users/view');
					$this->autoRender = false;
				}

				$res = mysql_query("select user_id,other_details from payment where id = '".$orderid."'");
					
				while ($row = mysql_fetch_assoc($res)) {
					$userid = $row['user_id'];
					$other_details = $row['other_details'];
				}

				if(trim($other_details) == trim($this->objMd5->Decrypt($customparam, encKey))){
					//status: 0=>success, 1=>failure
					mysql_query("update  payment set directpay_ref_id = '".$TransactionID."',flag='".strtoupper($status)."',other_details='".$customparam."',mer_order_no='".$orderid."',amount='".$amount."',response_txt='".$res_txt."',end_time='".date('Y-m-d H:i:s')."' where id = '".$orderid."'");
					$statusFlag = '';
					$mailBody = "Payment: OSS<br/>";
					if(strtolower($status) == 'success'){
						mysql_query("insert into transactions (user_id,amount,type,timestamp) values ('".$userid."','".$amount."','".TRANS_USER_CREDIT."','".date('Y-m-d H:i:s')."')");
						mysql_query("Update users set balance = balance + ".$amount." where id = ".$userid);
						$bal = mysql_query("select balance,mobile from users where id = '".$userid."'");
						while ($row1 = mysql_fetch_assoc($bal)) {
							$balance = $row1['balance'];
							$mobileNo = $row1['mobile'];
						}

						$mailBody .= "Amount: ".$amount;
						$mailBody .= "<br/>Mobile no: ".$mobileNo;
						$mailBody .= "<br/>Status: Successful";
						$statusFlag = 'S';
					}else{
						$bal = mysql_query("select balance,mobile from users where id = '".$userid."'");
						while ($row1 = mysql_fetch_assoc($bal)) {
							$balance = $row1['balance'];
							$mobileNo = $row1['mobile'];
						}

						$mailBody .= "Amount: ".$amount;
						$mailBody .= "<br/>Mobile no: ".$mobileNo;
						$mailBody .= "<br/>Status: Failed";
						$statusFlag = 'F';
					}
						
					$this->General->mailToAdmins("SMSTadka online payment", $mailBody);
				}else{
					$this->redirect(SITE_NAME.'users/view');
					$this->autoRender = false;
				}
			}else{
				$this->redirect(SITE_NAME.'users/view');
				$this->autoRender = false;
			}
		}else{

			$this->redirect(SITE_NAME.'users/view');
			$this->autoRender = false;
		}
		$this->redirect('/users/iccPaymentDone/'.$this->objMd5->Encrypt($orderid, encKey).'/'.$this->objMd5->Encrypt($balance, encKey).'/'.$this->objMd5->Encrypt($statusFlag, encKey));
	}

	function iccPaymentDone($oId,$bal,$status){
		$this->set('balance',$this->objMd5->Decrypt($bal, encKey));
		$this->set('tid',$this->objMd5->Decrypt($oId, encKey));
		$this->set('status',$this->objMd5->Decrypt($status, encKey));
		$this->layout = 'normal';
		$this->render('/users/payment_response');
	}
	function er404($response){
		//$this->set('response',$response);

		$this->set('response',$response);
		$this->layout = 'er404';
	}

	function setOptFlag($mobile,$type){
		$mobile = substr($mobile, -10);
		$opt_flag = -1;
		if(strtoupper($type) === "START"){
			$opt_flag = 1;
		}
		else if(strtoupper($type) === "STOP"){
			$opt_flag = 0;
		}
		$this->User->query("INSERT INTO log_opt (mobile,type,timestamp) VALUES ('$mobile','$type','".date('Y-m-d H:i:s')."')");

		$this->User->query("UPDATE users SET opt_flag = $opt_flag WHERE mobile = '$mobile'");
		//$this->General->mailToAdmins("START/STOP sent by User", "Mobile : $mobile<br/>Message Sent: $type");
		echo "SUCCESS";
		$this->autoRender = false;
	}
        
        function resultSendMessageVia247SMS($flag=1){
            $this->autoLayout = $this->autoRender = FALSE;
            $this->General->getSendMessageVia247SMS($flag);
        }
        
		function resultSendMessageViaTata($flag=0){//0 means b2b & 1 means b2c
            $this->autoLayout = $this->autoRender = FALSE;
            $this->General->getSendMessageViaTata($flag);
        }
        
        /*function sendMsgMailsNew(){
            $len = $this->redis->llen("smstadka");
        	
        	$len = ($len >= 50) ? 50 : $len;
        	$smstadka_data = $this->redis->lRange('smstadka', 0, $len);
        	$this->redis->lTrim('smstadka', 0, $len);
                            
        	$smstadka_data = $this->redis->lRange('smstadka', 0, -1);
                    $this->redis->del("smstadka");
                    $messages = array();
                    $mails = array();
                    foreach($smstadka_data as $keys){
                        $keys_single = json_decode($keys);

                        if(isset($keys_single->sms)){
                            $messages[] = $keys;
                        }
                        
                        if (isset($keys_single->mail_subject)) {
                            $mails[] = $keys;
                        }
                    }
                    
                    file_put_contents(LOG_PATH.date('Ymd').".txt","Fetching all requests from Queue together |Time=".time()."|Messages=".json_encode($messages)."|Mails=".json_encode($mails)."\n\n",FILE_APPEND);
                    
                    //echo "<pre>"; print_r($messages); echo "</pre>"; exit;
                    if(count($messages)>0){
                        foreach($messages as $single_message){
                            $single_message = json_decode($single_message);
                            if(isset($single_message->root) && !empty($single_message->root)){$root = $single_message->root;}
                            else $root = 'sms';
                            $this->General->sendMessage($single_message->sender,$single_message->mobile,$single_message->sms,$root,$single_message->app_name);
                        }
                    }
                    if(count($mails)>0){
                        foreach ($mails as $single_mail){
                            if(!isset($single_mail->emails)){
				$this->General->mailToAdmins($single_mail->mail_subject, $single_mail->mail_body);
                            }
                            else {
                                    $this->General->mailToUsers($single_mail->mail_subject, $single_mail->mail_body, $single_mail->emails);
                            }
                            if(isset($single_mail->retailer_id)){
                                    $this->Shop->mailToSuperDistributor($single_mail->retailer_id,$single_mail->mail_subject,$single_mail->mail_body);
                            }
                            else if(isset($single_mail->vendor_id) && $single_mail->vendor_id == 5){
                                    $this->General->mailToUsers($single_mail->mail_subject, $single_mail->mail_body,array('smartsolutions@nootan.net'));
                            }
                            
                        }
                       
                }
                
	}*/
        
	
        function sendMsgMailsNew1(){
        	while(true){
        		$dt = $this->redis->rpop('smstadka');
                        
        		if($dt == null){
        			sleep(2);
        			continue;
        		}
        		$logger = $this->General->dumpLog('sendMsgMailsNew', 'incomingProcessRequests');
                        $logger->info($dt);
                        //echo $dt;
        		$dt = json_decode($dt,true);
        		if(isset($dt['sms'])){
        			if(isset($dt['root']) && !empty($dt['root']))$root = $dt['root'];
        			else $root = 'sms';
        			$this->General->sendMessage($dt['sender'],$dt['mobile'],$dt['sms'],$root,$dt['app_name']);
        		}
        		if(isset($dt['mail_subject']) && !empty($dt['mail_body'])){
        			if(!isset($dt['emails'])){
        				$this->General->mailToAdmins($dt['mail_subject'], $dt['mail_body']);
        			}elseif( isset($dt['sender_id']) && !empty($dt['sender_id']) ){
                                        $this->General->mail_from_other_app($dt['mail_subject'], $dt['mail_body'], $dt['emails'],$dt['sender_id']);
                                }else {
        				$this->General->mailToUsers($dt['mail_subject'], $dt['mail_body'], $dt['emails']);
        			}
        			if(isset($dt['retailer_id'])){
        				$this->Shop->mailToSuperDistributor($dt['retailer_id'],$dt['mail_subject'],$dt['mail_body']);
        			}
        			/*else if(isset($_REQUEST['vendor_id']) && $_REQUEST['vendor_id'] == 5){
        			 $this->General->mailToUsers($_REQUEST['mail_subject'], $_REQUEST['mail_body'],array('smartsolutions@nootan.net'));
        			 }*/
        		}
        		
        	}
        	$this->autoRender = false;
        }
        
	function sendMails(){
        	while(true){
        		$dt = $this->redis->rpop('smstadka-mail');
                        
        		if($dt == null){
        			sleep(2);
        			continue;
        		}
        		$logger = $this->General->dumpLog('sendMails', 'incomingMailRequests');
                        $logger->info($dt);
                        //echo $dt;
        		$dt = json_decode($dt,true);
        		
        		if(isset($dt['mail_subject']) && !empty($dt['mail_body'])){
        			if(!isset($dt['emails'])){
        				$this->General->mailToAdmins($dt['mail_subject'], $dt['mail_body']);
        			}elseif( isset($dt['sender_id']) && !empty($dt['sender_id']) ){
                                        $this->General->mail_from_other_app($dt['mail_subject'], $dt['mail_body'], $dt['emails'],$dt['sender_id']);
                                }else {
        				$this->General->mailToUsers($dt['mail_subject'], $dt['mail_body'], $dt['emails']);
        			}
        			if(isset($dt['retailer_id'])){
        				$this->Shop->mailToSuperDistributor($dt['retailer_id'],$dt['mail_subject'],$dt['mail_body']);
        			}
        			/*else if(isset($_REQUEST['vendor_id']) && $_REQUEST['vendor_id'] == 5){
        			 $this->General->mailToUsers($_REQUEST['mail_subject'], $_REQUEST['mail_body'],array('smartsolutions@nootan.net'));
        			 }*/
        		}
        		
        	}
        	$this->autoRender = false;
        }

	function sendMsgMailsNew(){
        	while(true){
        		$dt = $this->redis->rpop('smstadkaredis');
                        
        		if($dt == null){
        			sleep(2);
        			continue;
        		}
        		$logger = $this->General->dumpLog('sendMsgMailsNew', 'incomingProcessRequests');
                        $logger->info($dt);
                        //echo $dt;
        		$dt = json_decode($dt,true);
        		if(isset($dt['sms'])){
        			if(isset($dt['root']) && !empty($dt['root']))$root = $dt['root'];
        			else $root = 'sms';
        			$this->General->sendMessage($dt['sender'],$dt['mobile'],$dt['sms'],$root,$dt['app_name']);
        		}
        		if(isset($dt['mail_subject']) && !empty($dt['mail_body'])){
        			if(!isset($dt['emails'])){
        				$this->General->mailToAdmins($dt['mail_subject'], $dt['mail_body']);
        			}
        			else {
        				$this->General->mailToUsers($dt['mail_subject'], $dt['mail_body'], $dt['emails']);
        			}
        			if(isset($dt['retailer_id'])){
        				$this->Shop->mailToSuperDistributor($dt['retailer_id'],$dt['mail_subject'],$dt['mail_body']);
        			}
        			/*else if(isset($_REQUEST['vendor_id']) && $_REQUEST['vendor_id'] == 5){
        			 $this->General->mailToUsers($_REQUEST['mail_subject'], $_REQUEST['mail_body'],array('smartsolutions@nootan.net'));
        			 }*/
        		}
        		$logger = $this->General->dumpLog('sendMsgMails', 'incomingSMSMailProcessing');
        		$logger->info(json_encode($dt));
        	}
        	$this->autoRender = false;
        }
            
	function sendMsgMails(){
		if(isset($_REQUEST['sms'])){
			if(isset($_REQUEST['root']) && !empty($_REQUEST['root']))$root = $_REQUEST['root'];
			else $root = 'sms';
			$this->General->sendMessage($_REQUEST['sender'],$_REQUEST['mobile'],$_REQUEST['sms'],$root,$_REQUEST['app_name']);
		}
		if(isset($_REQUEST['mail_subject']) && !empty($_REQUEST['mail_body'])){
			if(!isset($_REQUEST['emails'])){
				$this->General->mailToAdmins($_REQUEST['mail_subject'], $_REQUEST['mail_body']);
			}
			else {
				$this->General->mailToUsers($_REQUEST['mail_subject'], $_REQUEST['mail_body'], $_REQUEST['emails']);
			}
			if(isset($_REQUEST['retailer_id'])){
				$this->Shop->mailToSuperDistributor($_REQUEST['retailer_id'],$_REQUEST['mail_subject'],$_REQUEST['mail_body']);
			}
			/*else if(isset($_REQUEST['vendor_id']) && $_REQUEST['vendor_id'] == 5){
				$this->General->mailToUsers($_REQUEST['mail_subject'], $_REQUEST['mail_body'],array('smartsolutions@nootan.net'));
			}*/
		}
		$this->autoRender = false;
	}

	function checkDND($mobile,$type=null){
		exit;
		$mobs = explode(",",$mobile);
		array_unique($mobs);
		foreach($mobs as $mob){
			$arr[$mob] = $this->General->checkDND($mob);
		}
		if($type == null){
			if($arr[$mobile]['dnd'] == 0) echo "User is not in DND";
			else if($arr[$mobile]['dnd'] == 1){
				echo "User is in DND<br/>";
				echo "Preference Category: " . $arr[$mobile]['preference'];
			}
			else {
				echo "Not able to fetch data";
			}
		}
		else {
			//$this->General->mailToUsers("DND Check by Infobip","Mobiles: $mobile",array('ashish@mindsarray.com'));

			echo json_encode($arr);
		}
		$this->autoRender = false;
	}

	function initDB() {
		set_time_limit(0);
		ini_set("memory_limit","-1");

	 $group =&$this->User->Group;
	 //Allow admins to everything
	 $group->id = 2;
	 $this->Acl->allow($group, 'controllers');
	  
	 //author permissions
	 $group->id = 3;
	 $this->Acl->deny($group, 'controllers');

	 //user permissions
	 $group->id = 1;
	 $this->Acl->deny($group, 'controllers');

	 $member_acl['Users'] = array('index','add','edit','delete');
	 $member_acl['Logs'] = array('index','add','edit','delete','view');
	 $member_acl['Categories'] = array('index','add','edit','delete','view');
	 $member_acl['Transactions'] = array('index','add','edit','delete','view');
	 $member_acl['Tags'] = array('index','add','edit','delete','addTags','findTag','submitTags','getTags');
	 $member_acl['Stocks'] = array('curl_getStockCompanies','getCompanies','saveCompany','getAllCompanies');
	 $member_acl['Reminders'] = array();
	 $member_acl['Pnrs'] = array();
	 $member_acl['Apps'] = array();
	 $member_acl['Packages'] = array('index','add','edit','delete');
	 $member_acl['Messages'] = array('index','add','edit','delete','addTags','refineMessages','submitMessage','editMessage','deleteMessage','addMessages');

	 foreach($member_acl as $controller => $actions){
	 	$this->Acl->allow($group, 'controllers/'.$controller);
	 	foreach($actions as $action){
	 		$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
	 	}
	 }

	 $superdistributor_acl['Shops'] = array('topupReceipts','printRequest','issue','issueReceipt','backReceipt','printReceipt','printInvoice', 'script','view','formDistributor','backDistributor','backDistEdit','createDistributor','allotCards','backAllotment','allotRetailCards','allRetailer','editRetailer','showDetails','editDistValidation','changePassword','accountHistory','cardsAllotted','invoices','transfer','amountTransfer','backTransfer');
	 $distributor_acl['Shops'] = array('topupReceipts','printRequest','issue','issueReceipt','backReceipt','printReceipt','printInvoice', 'script', 'view','activateCards','backActivation','activateRetailCards','allRetailer','editRetailer','showDetails','editRetValidation','formRetailer','backRetailer','createRetailer','changePassword','accountHistory','cardsAllotted','cardsActivated','invoices','transfer','amountTransfer','backTransfer');
	 $retailer_acl['Shops'] = array('topupReceipts','printRequest','issue','issueReceipt','backReceipt','printReceipt','setSignature','saveSignature','printInvoice', 'script', 'view','changePassword','accountHistory','cardsSold','cardsActivated','invoices','retailerProdActivation');

	 //super distributor permissions
	 $group->id = 4;
	 $this->Acl->deny($group, 'controllers');

	 foreach($member_acl as $controller => $actions){
		 $this->Acl->allow($group, 'controllers/'.$controller);
		 foreach($actions as $action){
		 	$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 foreach($superdistributor_acl as $controller => $actions){
		 foreach($actions as $action){
		 	$this->Acl->allow($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }

	 //distributor permissions
	 $group->id = 5;
	 $this->Acl->deny($group, 'controllers');

	 foreach($member_acl as $controller => $actions){
		 $this->Acl->allow($group, 'controllers/'.$controller);
		 foreach($actions as $action){
		 	$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 foreach($distributor_acl as $controller => $actions){
		 foreach($actions as $action){
		 	$this->Acl->allow($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }

	 //retailer permissions
	 $group->id = 6;
	 $this->Acl->deny($group, 'controllers');

	 foreach($member_acl as $controller => $actions){
		 $this->Acl->allow($group, 'controllers/'.$controller);
		 foreach($actions as $action){
		 	$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 foreach($retailer_acl as $controller => $actions){
		 foreach($actions as $action){
		 	$this->Acl->allow($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }

	 $this->autoRender = false;
	}
	
	
	function b2cMarketingMsg($date=null,$date_to=null){
		if(empty($date))$date = date('Y-m-d');
		$to_date = true;	
		if(empty($date_to)){
			$date_to = $date;
			$to_date = false;	
		}
		$data1= $this->User->query("SELECT count(*) as counts,date FROM `msg247smsdellog` where date >= '$date' AND date <= '$date_to' AND message like 'Thank you for recharging at a PAY1%' group by date");
		$data2= $this->User->query("SELECT count(*) as counts,date FROM `msgTataSmsDelLog` where date >= '$date' AND date <= '$date_to' AND message like 'Thank you for recharging at a PAY1%' group by date");
		
		$data = array();
		foreach($data1 as $dt){
			$date_key = $dt['msg247smsdellog']['date'];
			$data[$date_key] = $dt['0']['counts'];
		}
		
		foreach($data2 as $dt){
			$date_key = $dt['msgTataSmsDelLog']['date'];
			$data[$date_key] = isset($data[$date_key]) ? ($data[$date_key] + $dt['0']['counts']) : $dt['0']['counts'];
		}
		
		if($to_date){
			echo json_encode($data);
		}
		else {
			$arr = array_values($data);
			echo $arr[0];
		}
		$this->autoRender = false;
	}


    function daily_sms_consumption($date=null){
        if(empty($date))$date = date('Y-m-d',strtotime("-1 days"));
        $tata_sms_detail = "SELECT * FROM (select status, count(1) as counts from msgTataSmsDelLog where date='$date' group by 1) as t";
        $m247_sms_detail = "SELECT * FROM (select status, count(1) as counts from msg247smsdellog where date='$date' group by 1) as t";
        $m247_sms_detail_data = $this->User->query($m247_sms_detail);
        $tata_sms_detail_data = $this->User->query($tata_sms_detail);
        $tata_data = array();
        $m247_data = array();

        foreach($m247_sms_detail_data as $k=>$v):
            $v['t']['status'] = empty($v['t']['status'])?"unknown":$v['t']['status'];
            $m247_data[$v['t']['status']] = $v['t']['counts'];
            $mtotal += $v['t']['counts'];
        endforeach;
        $m247_data['Total'] = $mtotal;
        
        foreach($tata_sms_detail_data as $k=>$v):
            $v['t']['status'] = empty($v['t']['status'])?"unknown":$v['t']['status'];
            $tata_data[$v['t']['status']] = $v['t']['counts'];
            $tata_total += $v['t']['counts'];
        endforeach;
        $tata_data['Total'] = $tata_total;
        
        $mail_msg = " SMS CONSUMPTION FOR << $date >> <br>";
        $mail_msg .= "TATA MSG : ".  json_encode($tata_data)." <br> ";
        $mail_msg .= "24X7 MSG : ".  json_encode($m247_data)."  ";
        $this->General->mailToUsers("<< Daily SMS Consumption >>", $mail_msg, "ashish@mindsarray.com,nandan@mindsarray.com");
        $this->autoRender = false;
    }

    }
