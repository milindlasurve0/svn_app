<?php
class CategoriesPackagesController extends AppController {

	var $name = 'CategoriesPackages';

	function index() {
		$this->CategoriesPackage->recursive = 0;
		$this->set('categoriesPackages', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid categories package', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('categoriesPackage', $this->CategoriesPackage->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->CategoriesPackage->create();
			if ($this->CategoriesPackage->save($this->data)) {
				$this->Session->setFlash(__('The categories package has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The categories package could not be saved. Please, try again.', true));
			}
		}
		$categories = $this->CategoriesPackage->Category->find('list');
		$packages = $this->CategoriesPackage->Package->find('list');
		$this->set(compact('categories', 'packages'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid categories package', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->CategoriesPackage->save($this->data)) {
				$this->Session->setFlash(__('The categories package has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The categories package could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->CategoriesPackage->read(null, $id);
		}
		$categories = $this->CategoriesPackage->Category->find('list');
		$packages = $this->CategoriesPackage->Package->find('list');
		$this->set(compact('categories', 'packages'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for categories package', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->CategoriesPackage->delete($id)) {
			$this->Session->setFlash(__('Categories package deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Categories package was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>