<?php
class PromotionsController extends AppController {
	
	var $name = 'Promotions';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $uses = array('Group');
	var $components = array('RequestHandler','Email');
	
	function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('*');
		$this->Auth->allowedActions = array('test','promoPanel','promoShoot','sendPromoMsg','misscall','mnit','followup','curl_post_async','retMail','retOffer','mixdata','addfundata','addintomsgs','userDelivery','index','setDataAstro','creditDetails','HIW','fdesign','CSupport','custSupport','freeCredits','shareCredits','redeem','redeemCredits','smsOutgoingMonitoring');		
	}
	
	function test() {
		$this->autoRender = false;
		echo "1";
	}

	function followup(){
		if($this->Session->read('Auth.User.group_id') == ADMIN){
			$query = "SELECT users.id,users.mobile,users.followup, GROUP_CONCAT(comments.comment,'***') as comment,comments.created FROM users left join comments on (comments.itemid= users.id) where  users.followup is not null and users.followup <> '' group by users.id order by users.followup asc,comments.created desc";			
	        $data = $this->Group->query($query);
	        $this->set('data',$data);
		}else{
			$this->redirect('/users/view');
		}
	}
	
	function curl_post_async($url, $params=null)
	{
	    foreach ($params as $key => &$val) {
	      if (is_array($val)) $val = implode(',', $val);
	        $post_params[] = $key.'='.urlencode($val);
	    }
	    $post_string = implode('&', $post_params);
	
	    $parts=parse_url($url);
	
	    $fp = fsockopen($parts['host'],
	        isset($parts['port'])?$parts['port']:80,
	        $errno, $errstr, 30);
	
	    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
	    $out.= "Host: ".$parts['host']."\r\n";
	    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	    $out.= "Content-Length: ".strlen($post_string)."\r\n";
	    $out.= "Connection: Close\r\n\r\n";
	    if (isset($post_string)) $out.= $post_string;
	
	    fwrite($fp, $out);
	    fclose($fp);
	}

	function retMail(){
		//mail params 
		ini_set("memory_limit","-1");
		$url = SERVER_BACKUP . 'groups/shootMail';
		$params['sub'] = "SMSTadka Go-Goa offer!";		
				
		$params['type'] = 1;
		$params['from'] = 'SMSTadka.com';			
		//$params['path'][0] = $_SERVER['DOCUMENT_ROOT'] . '/Onlinepackages.jpg';
		
		
		
		$query = "SELECT mobile,email,vendor_id FROM vendors_retailers where mobile is not null and email is not null";
		//$query = "SELECT mobile,email,vendor_id FROM vendors_retailers where id = 1";
        $data = $this->Group->query($query);
        //echo "<pre>";print_r($data); echo "</pre>";exit;
		//$i = 1;        
        foreach($data as $dt){
        	echo $i."=".$dt['vendors_retailers']['mobile']."=".$dt['vendors_retailers']['email']."<br/>";
        	$i++;
        	//$message = "Dear BP, Now offer new SMS packs to your customers. SMSTadka has introduced Gita quotes, Motivational quotes, beauty tips and more. Sell SMSTadka packs and win assured prizes every day. For details Check your email ".$dt['vendors_retailers']['email'].". Thank you.";
        	$message = "Dear BP, Sell SMSTadka packs & win exciting prizes! Sell packs more than Rs.1000 and win an assured prize and get a chance to win a Goa trip. To know more check your email ".$dt['vendors_retailers']['email'].". To know your total sale, sms SALE to 09223178889.";
        	//send msg
            $this->General->sendMessage(SMS_SENDER,array($dt['vendors_retailers']['mobile']),$message,'sms');
            //send mail
            
            
            
            if($dt['vendors_retailers']['vendor_id'] == '3'){
            $params['body'] = "<b>Go-Goa Offer!!</b><br/><br/>
Have a party at Goa for FREE! Sell SMSTadka packs and win a trip for two at Goa worth Rs. 20000<br/><br/>
On every sell of SMSTadka packs worth Rs. 1000 win an assured gift and an entry coupon for Goa trip.<br/><br/>
Sell more to collect multiple coupons and increase your chances to be the winner.<br/><br/>
For eg. If you sell packs of Rs. 5000, you get 5 assured gifts and 5 Goa coupons. The more the merrier.<br/><br/>
A lucky draw will be held on <b>1st January 2012</b> and winners will be announced. Lots of other prizes to be won.Offer valid till <b>31st December</b>.<br/><br/>
To know more about offers, visit http://smstadka.com/offers<br/><br/>
SMSTadka has also introduced new products.<br/><br/>
New products include Gita quotes, Motivational quotes, Beauty  Tips, Health Tips and more. Visit Mobileseva VAS option to find out more.<br/><br/>
You can call me on +919892471157 for any queries and assistance. <br/><br/>
Regards,<br/>
Dinesh Amle<br/>
SMSTadka.com<br/>
+91 9892471157<br/>
dinesh@mindsarray.com";	
            //$params['path'][1] = $_SERVER['DOCUMENT_ROOT'] . '/vidupload/marketing_oss.zip';
            }else{
            	$params['body'] = "<b>Go-Goa Offer!!</b><br/><br/>
Have a party at Goa for FREE! Sell SMSTadka packs and win a trip for two at Goa worth Rs. 20000<br/><br/>
On every sell of SMSTadka packs worth Rs. 1000 win an assured gift and an entry coupon for Goa trip.<br/><br/>
Sell more to collect multiple coupons and increase your chances to be the winner.<br/><br/>
For eg. If you sell packs of Rs. 5000, you get 5 assured gifts and 5 Goa coupons. The more the merrier.<br/><br/>
A lucky draw will be held on <b>1st January 2012</b> and winners will be announced. Lots of other prizes to be won.Offer valid till <b>31st December</b>.<br/><br/>
To know more about offers, visit http://smstadka.com/tss<br/><br/>
SMSTadka has also introduced new products.<br/><br/>
New products include Gita quotes, Motivational quotes, Beauty  Tips, Health Tips and more. Visit Mobileseva VAS option to find out more.<br/><br/>
You can call me on +919892471157 for any queries and assistance. <br/><br/>
Regards,<br/>
Dinesh Amle<br/>
SMSTadka.com<br/>
+91 9892471157<br/>
dinesh@mindsarray.com";
			///$params['path'][1] = $_SERVER['DOCUMENT_ROOT'] . '/vidupload/marketing_smartshop.zip';
            }
			
            $params['email'] = $dt['vendors_retailers']['email'];
            $this->curl_post_async($url,$params);
        }
        echo "done"; 
        $this->autoRender = false;
	}
	
	function retOffer(){
		$query = "SELECT mobile FROM vendors_retailers where mobile is not null";
        $data = $this->Group->query($query);
        
        $mobiles = array();
        foreach($data as $dt){
        	$message = "Dear BP, Sell SMSTadka packs & win prizes! Already many retailers have won exciting prizes. Now you can also win a FREE trip to Goa. Sell packs more than Rs.1000 and win an assured prize and an entry coupon for Goa trip lucky draw. To know your total sale, sms SALE to 09223178889.";
            $this->General->sendMessage(SMS_SENDER,array($dt['vendors_retailers']['mobile']),$message,'sms');
        }
        $this->autoRender = false;
	}

	function mixdata(){
		/*$data1 = $this->Group->query("SELECT data from santabanta1");
		//$data = $this->Group->query("SELECT data from jokes1");
		foreach($data as $d){
			$this->Group->query("insert into mixdata(data) 
			values ('".addslashes($d['jokes1']['data'])."')");
		}
		foreach($data1 as $d1){
			$this->Group->query("insert into mixdata1(data) 
			values ('".addslashes($d1['santabanta1']['data'])."')");
		}
		*/
		set_time_limit(0);
		$i=1;
		$j=1;
		for($k=0;$k<500;$k++){
			$data = $this->Group->query("SELECT data from mixdata where id=".$i);
			foreach($data as $d){
				if(trim($d['mixdata']['data']) != ''){
					$this->Group->query("insert into mixdata2(data) 
					values ('".addslashes($d['mixdata']['data'])."')");
					$this->Group->query("delete from mixdata where id=".$i);
				}
			}
			$i++;
			$data1 = $this->Group->query("SELECT data from mixdata where id=".$i);
			foreach($data1 as $d1){
				if(trim($d1['mixdata']['data']) != ''){
					$this->Group->query("insert into mixdata2(data) 
					values ('".addslashes($d1['mixdata']['data'])."')");
					$this->Group->query("delete from mixdata where id=".$i);
				}
			}
			$i++;
			$data2 = $this->Group->query("SELECT data from mixdata1 where id=".$j);
			foreach($data2 as $d2){
				if(trim($d2['mixdata1']['data']) != ''){
					$this->Group->query("insert into mixdata2(data) 
					values ('".addslashes($d2['mixdata1']['data'])."')");
					$this->Group->query("delete from mixdata1 where id=".$j);
				}
			}
			$j++;
		}
	}
	
	
	function addfundata(){
		$mStart = '9872';
		$mEnd = '10049';
		$p = '36';
		
		for($i=$mStart;$i<=$mEnd;$i++){
				$this->Group->query("insert into data_funs(message_id,package_id,flag,resent) 
			values (".$i.",".$p.",0,0)");
		}
		    
		$this->autoRender = false;
	}
	
	function addintomsgs(){
		$data = $this->Group->query("SELECT data from mixdata2");
		foreach($data as $d){
			//echo $d['santabanta1']['data']."<br/>";
			$this->Group->query("insert into messages(category_id,title,url,content,cyclicData,charCount,rating,author_id,toshow,created,modified) 
			values (0,null,null,'".addslashes($d['mixdata2']['data'])."',null,null,0,0,0,'".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')");
		}
		$this->autoRender = false;
	} 
	
	function index(){
		define("FACEBOOK_APP_ID", '151742211559792');
		define("FACEBOOK_API_KEY", '27f7a7821b8e75b1a1ea0ceb85b57c91');
		define("FACEBOOK_SECRET_KEY", 'a7b2bf898579f3d1d4767029a30b77d6');
		define("FACEBOOK_CANVAS_URL", 'http://apps.facebook.com/smstadkaquotes/');
		App::import('vendor', 'facebook', array('file' => 'facebook.php'));
		$facebook = new Facebook(array(
								'appId'  => FACEBOOK_APP_ID,
								'secret' => FACEBOOK_SECRET_KEY,
								'cookie' => true,
								'domain' => 'smstadka.com'
								));
								
		$session = $facebook->getSession();
 
		if (!$session) {

       			$url = $facebook->getLoginUrl(array(
                'canvas' => 1,
                'fbconnect' => 0
                ));
       
                echo "<script type='text/javascript'>top.location.href = '$url';</script>";
                $this->autoRender = false;
        }else{                
                try {		 
					$uid = $facebook->getUser();
					$me = $facebook->api('/me');				 										
			 		$this->set('me',$me);
					
			 		//$maxMin = $this->Group->query("SELECT MAX(`id`) AS max_id , MIN(`id`) AS min_id FROM facebook_app_data");
			 		//$randNo = rand($maxMin[0]['facebook_app_data']['min_id'],$maxMin[0]['facebook_app_data']['max_id']); 
			 		$randNo = rand(1,2158);
			 		$res = $this->Group->query("select content from facebook_app_data where id=".$randNo);
			 		$this->set('quote',$res[0]['facebook_app_data']['content']);
			 		$this->set('session',$session);
					$this->set('appId','151742211559792');			 												 
				}catch(FacebookApiException $e) {		 
					echo "Error:" . print_r($e, true);		 
				}	
				$this->layout = 'faceapp';	
        }
					
	}
	function creditDetails(){
		$iRedeem = $this->Group->query("SELECT users_redeemed.*, users.mobile from users_redeemed join users on (users_redeemed.ref_user_id = users.id) where user_id =".$_SESSION['Auth']['User']['id']);
		$othersRedeem = $this->Group->query("SELECT users_redeemed.*, users.mobile from users_redeemed join users on (users_redeemed.user_id = users.id) where ref_user_id =".$_SESSION['Auth']['User']['id']);
			
		$this->set('resultTransaction',$iRedeem);
		$this->set('resultTransaction1',$othersRedeem);
		$this->render('/elements/credit_details');
	}
	
	function freeCredits($cc=null){
		$iRedeem = $this->Group->query("SELECT users_redeemed.*, users.mobile from users_redeemed join users on (users_redeemed.ref_user_id = users.id) where user_id =".$_SESSION['Auth']['User']['id']);
		if(count($iRedeem)>0)
		$this->set('redeemed',1);
		
		$this->set('cc',$cc);
		$this->render('/promotions/free_credits');
	}
	
	function CSupport(){
		$this->Session->write('param','custSupport');
		$this->redirect("/users/view/");
	}
	function custSupport(){		
		$this->render('/promotions/cust_support');
	}
	
	function HIW(){
		$iRedeem = $this->Group->query("SELECT users_redeemed.*, users.mobile from users_redeemed join users on (users_redeemed.ref_user_id = users.id) where user_id =".$_SESSION['Auth']['User']['id']);
		if(count($iRedeem)>0)
		$this->set('redeemed',1);
		$this->render('/elements/couponCodeHIW');
	}
	
	function redeem(){
		$this->render('/elements/redeem');
	}
	
	function redeemCredits(){
		$couponCode = trim($_POST['data']['Promotion']['freeCreditCode']);
		if($couponCode != ''){//!empty coupon code			
			$res = $this->Group->query("select id from users_redeemed where user_id = ".$_SESSION['Auth']['User']['id']." and type='o'");
			if(count($res)<1){//hasn't redeemed yet
				$codeValid = $this->Group->query("select id,user_id from coupon_codes where coupon_code = '".$couponCode."'");
				if(count($codeValid)>0){//valid coupon code
					if($codeValid['0']['coupon_codes']['user_id'] == $_SESSION['Auth']['User']['id']){//self-generated code
						$this->Session->setFlash(__('<div class="errMessage1">You cannot redeem self-generated coupon code.</div>', true));
					}else{//shared code
						if($this->Group->query("insert into users_redeemed(user_id,ref_user_id,coupon_code,type,created) values (".$_SESSION['Auth']['User']['id'].",".$codeValid['0']['coupon_codes']['user_id'].",'".$couponCode."','o','".date('Y-m-d H:i:s')."')")){
							//one who redeems gets 5 rs.
							$this->Group->query("insert into transactions (user_id,amount,type,timestamp) values ('".$_SESSION['Auth']['User']['id']."','".REDEEM_AMT."','".TRANS_ADMIN_FREE_CREDIT."','".date('Y-m-d H:i:s')."')");
							$this->General->balanceUpdate(REDEEM_AMT,'add',$_SESSION['Auth']['User']['id']);
							//$this->Group->query("update users set balance = balance + ".REDEEM_AMT." where id = ".$_SESSION['Auth']['User']['id']);
							$redeemer = $this->General->getUserDataFromId($_SESSION['Auth']['User']['id']);
							echo "<script> reloadBalance(".	$redeemer['balance']."); </script>"; 
							// one whose code is redeemed gets 2 rs.
							$this->Group->query("insert into transactions (user_id,amount,type,timestamp) values ('".$codeValid['0']['coupon_codes']['user_id']."','".SHARER_CREDIT."','".TRANS_ADMIN_FREE_CREDIT."','".date('Y-m-d H:i:s')."')");
							$this->General->balanceUpdate(SHARER_CREDIT,'add',$codeValid['0']['coupon_codes']['user_id']);
							//$this->Group->query("update users set balance = balance + ".SHARER_CREDIT." where id = ".$codeValid['0']['coupon_codes']['user_id']);
							$redeeme = $this->General->getUserDataFromId($codeValid['0']['coupon_codes']['user_id']);
																				
							//mail to admins
							$mailSubject = "Balance update using ref code";
							$mailBody = "User ".$redeemer['mobile']." redeemed ref code of ".$redeeme['mobile'].". Ref code is ".$couponCode;
							//$this->General->mailToAdmins($mailSubject,$mailBody);
							//ends
							$this->Session->setFlash(__('<div class="strng">You have redeemed Rs.'.REDEEM_AMT.' successfully! Your current balance is Rs.'.$redeemer['balance'].'</div>', true));
						}else{
							$this->Session->setFlash(__('<div class="errMessage1">Please try again.</div>', true));
						}
					}					
				}else{
					$this->Session->setFlash(__('<div class="errMessage1">Invalid coupon code.</div>', true));
				}								
			}else{
				$this->Session->setFlash(__('<div class="errMessage1">Sorry, you have already redeemed coupon code before.</div>', true));
			}
		}else{
			$this->Session->setFlash(__('<div class="errMessage1">Please enter proper coupon code.</div>', true));
		}
		if($_POST['data']['Promotion']['freeCreditCode'])$this->set('cc',$_POST['data']['Promotion']['freeCreditCode']);
		$this->render('/elements/redeem');
	}
	
	function fdesign(){
		
			 		$this->set('quote',"Change is a nature of life. but challenge is a aim of life so you have to challenge the changes but not to change challenge.");
	}	
	
	function setDataAstro($pass,$date=null){
		if(!isset($date)){
			$entryDate = date('Y-m-d');
		}else{
			$entryDate = $date;
		}
		if($pass == 's1tadka'){
			$signIdArr = array(1,2,3,4,5,7,8,9,10,11,12,14);
			for($i=0;$i<count($signIdArr);$i++){				
				$records = $this->Group->query("select id from data_astrologies where package_id=".$signIdArr[$i]." and already_sent = 0 and ref_id = 0");
				if(count($records) < 1){
					$this->Group->query("update data_astrologies set already_sent = 0 where ref_id = 0 and package_id=".$signIdArr[$i]);
					$records = $this->Group->query("select id from data_astrologies where package_id=".$signIdArr[$i]." and already_sent = 0 and ref_id = 0");
				}
				$idArr = array();
				$j = 0;
				foreach($records as $rec){
					$idArr[$j] = $rec['data_astrologies']['id'];
					$j++;
				}
				$randomIdKey = array_rand($idArr,1);
				$randomId = $idArr[$randomIdKey];
				//echo count($idArr)."-".$randomId."<br>";
				$insRec = $this->Group->query("select content from data_astrologies where id=".$randomId);
				foreach($insRec as $ir){
					//echo $ir['data_astrologies']['content']; "</br></br>";
					$this->Group->query("insert into data_astrologies(package_id,content,date,flag,already_sent,repeated,ref_id) values (".$signIdArr[$i].",'".addslashes($ir['data_astrologies']['content'])."','".$entryDate."',0,0,0,".$randomId.")");
					$this->Group->query("update data_astrologies set already_sent = 1, repeated = repeated+1 where id = ".$randomId);	
				}
			}
		}else{
			echo "get lost";
		}
		 $this->autoRender = false;
	}
	
	function userDelivery($num = null){
		if($num == null)$num = $_POST['mobno'];
		if($num){
			$mainArr = array();

			$dRoute = $this->Group->query("SELECT * FROM delivery_routesms WHERE mobile = '91".$num."' ORDER BY created DESC");			
			$resArr = array();			
			$i = 0;
			foreach($dRoute as $data){
				$resArr[$i]['message'] = $this->General->checkIfResetPassMsg($data['delivery_routesms']['message']);//$data['delivery_routesms']['message'];
				$resArr[$i]['status'] = $data['delivery_routesms']['status_flag'];
				$resArr[$i]['cause'] = $data['delivery_routesms']['status'];
				$resArr[$i]['priority'] = '';
				$resArr[$i]['resend'] = '';
				$resArr[$i]['created'] = $data['delivery_routesms']['created'];
				$resArr[$i]['resendTime'] = '';
				$i++;
			}
						
			$mainArr['RouteSMS'] = $resArr;
			
			$dVfirst = $this->Group->query("select * from delivery_vfirst where mobile = '".$num."' order by created desc");			
			$resArr = array();			
			$i = 0;
			foreach($dVfirst as $data){
				$resArr[$i]['message'] = $this->General->checkIfResetPassMsg($data['delivery_vfirst']['message']);
				$resArr[$i]['status'] = $data['delivery_vfirst']['status_flag'];
				$resArr[$i]['cause'] = $data['delivery_vfirst']['status'];
				$resArr[$i]['priority'] = '';
				$resArr[$i]['resend'] = '';
				$resArr[$i]['created'] = $data['delivery_vfirst']['created'];
				$resArr[$i]['resendTime'] = '';
				$i++;
			}
						
			$mainArr['VFirst'] = $resArr;
			//24x7
			$d24x7 = $this->Group->query("select * from msg247smsdellog where mobile = '".$num."' order by created desc");			
			$resArr = array();			
			$i = 0;
			foreach($d24x7 as $data){
				$resArr[$i]['message'] = $this->General->checkIfResetPassMsg($data['msg247smsdellog']['message']);
				$resArr[$i]['status'] = $data['msg247smsdellog']['status_flag'];
				$resArr[$i]['cause'] = $data['msg247smsdellog']['status'];
				$resArr[$i]['priority'] = $data['msg247smsdellog']['type'];
				$resArr[$i]['resend'] = $data['msg247smsdellog']['resent'];
				$resArr[$i]['created'] = $data['msg247smsdellog']['created'];
				$resArr[$i]['resendTime'] = $data['msg247smsdellog']['recurr_time'];
				$i++;
			}
						
			$mainArr['24X7'] = $resArr;
                        
                        
                        //tata
			$dtata = $this->Group->query("select * from msgTataSmsDelLog where mobile = '91".$num."' order by created desc");			
			$resArr = array();			
			$i = 0;
			foreach($dtata as $data){
				$resArr[$i]['message'] = $this->General->checkIfResetPassMsg($data['msgTataSmsDelLog']['message']);
				$resArr[$i]['status'] = $data['msgTataSmsDelLog']['status_flag'];
				$resArr[$i]['cause'] = $data['msgTataSmsDelLog']['status'];
				$resArr[$i]['priority'] = $data['msgTataSmsDelLog']['type'];
				$resArr[$i]['resend'] = $data['msgTataSmsDelLog']['resent'];
				$resArr[$i]['created'] = $data['msgTataSmsDelLog']['created'];
				$resArr[$i]['resendTime'] = $data['msgTataSmsDelLog']['recurr_time'];
				$i++;
			}
						
			$mainArr['tata'] = $resArr;
			
			/*$mos = $this->Group->query("select * from delivery_mos where mobile = '".$num."' order by created desc");
			$resArr = array();
			$i = 0;
			foreach($mos as $data){
				$resArr[$i]['message'] = $data['delivery_mos']['message'];
				$resArr[$i]['status'] = $data['delivery_mos']['status_flag'];
				$resArr[$i]['cause'] = $data['delivery_mos']['status'];
				$resArr[$i]['priority'] = 0;
				$resArr[$i]['resend'] = 0;
				$resArr[$i]['created'] = $data['delivery_mos']['created'];
				$resArr[$i]['resendTime'] = 0;
				$i++;
			}
			
			$mainArr['Multilink'] = $resArr;*/
			
			//gsm modem
			$mos = $this->Group->query("select * from delivery_modem where mobile = '".$num."' order by created desc");
			$resArr = array();
			$i = 0;
			foreach($mos as $data){
				$resArr[$i]['message'] = $this->General->checkIfResetPassMsg($data['delivery_modem']['message']);
				$resArr[$i]['status'] = $data['delivery_modem']['status_flag'];
				$resArr[$i]['cause'] = $data['delivery_modem']['status'];
				$resArr[$i]['priority'] = $data['delivery_modem']['priority'];
				$resArr[$i]['resend'] = 0;
				$resArr[$i]['created'] = $data['delivery_modem']['created'];
				$resArr[$i]['resendTime'] = 0;
				$i++;
			}
			
			$mainArr['GSM Modem'] = $resArr;
			
			
			//gupshup
			/*$gupshup = $this->Group->query("select * from gupshup_del_log where mobile_no = '91".$num."' order by created desc");
			$resArr = array();
			$i = 0;
			foreach($gupshup as $data){
				$resArr[$i]['message'] = $data['gupshup_del_log']['message'];
				$resArr[$i]['status'] = $data['gupshup_del_log']['status'];
				$resArr[$i]['cause'] = $data['gupshup_del_log']['cause'];
				$resArr[$i]['priority'] = $data['gupshup_del_log']['priority_root'];
				$resArr[$i]['resend'] = $data['gupshup_del_log']['resend_flag'];
				$resArr[$i]['created'] = $data['gupshup_del_log']['created'];
				$resArr[$i]['resendTime'] = 10800;
				$i++;
			}
			
			$mainArr['Gupshup'] = $resArr;*/
			/*
			$smslane = $this->Group->query("select * from smslanedellog where mobile = '".$num."' order by created desc");
			$resArr = array();
			$i = 0;
			foreach($smslane as $data){
				$resArr[$i]['message'] = $data['smslanedellog']['message'];
				$resArr[$i]['status'] = $data['smslanedellog']['status_flag'];
				$resArr[$i]['cause'] = $data['smslanedellog']['status'];
				$resArr[$i]['priority'] = 0;
				$resArr[$i]['resend'] = 0;
				$resArr[$i]['created'] = $data['smslanedellog']['created'];
				$resArr[$i]['resendTime'] = 0;
				$i++;
			}
			
			$mainArr['SmsLane'] = $resArr;
			*/
			/*echo "<pre>";
			print_r($mainArr);
			echo "</pre>";
			*/
			$this->set('mob',$num);
			$this->set('data',$mainArr);
		}
		$this->layout = 'admin';	
	}
	
	/*function mnit(){
		$message = "PRIMAVERA'12 Prsents workshops
Hacking
Robotics
Android
Surface computing
3D Animation
For Reg. Contact 9461970268
or www.primavera12.in
-
smstadka.com";
		$j = 0;
		while($j <= 40){
 			$num = $j*250;
	 		$data = $this->Group->query("SELECT mobile,id FROM market_data WHERE dnd_flag = 0 AND type not in ('help','fest') order by id LIMIT $num,250");
			if(empty($data))break;
	 		$mobiles = array();
	 		$ids = array();
	 		foreach($data as $mobi){
				$mobiles[] = $mobi['market_data']['mobile'];
				$ids[] = $mobi['market_data']['id'];
			}
			//$this->printArray($mobiles);exit;
			$this->General->sendMessageVia247SMS('pvMNIT',$mobiles,$message);
			$data = $this->Group->query("update market_data set count = count + 1 where id in (".implode(",",$ids).")");
			
	 		sleep(300);
	 		$j++;
		}
	}
	
	function promoPanel(){
		if(!$this->Session->check('Auth.User.group_id')){
			$this->redirect('/users/view');
		}
		$data = $this->Group->query("SELECT mobile,id FROM log_misscall WHERE misscall = '02261512288' group by mobile");
		$logs = $this->Group->query("SELECT content,timestamp FROM logs as Log WHERE package_id = -1 order by id desc");
		$this->set('count',count($data));
		$this->set('logs',$logs);
	}
	
	function misscall(){
		$number = $_POST['number'];
		if($number != null){
			$data = $this->Group->query("SELECT distinct mobile FROM log_misscall WHERE misscall = '$number'");
			$this->set('number',$number);
			$this->set('count',count($data));
		}
		$this->layout = 'ajax';
	}
	
	function promoShoot(){
		$message = $this->data['content'];
		$logs = $this->Group->query("SELECT content,timestamp FROM logs as Log WHERE package_id = -1 order by id desc");
		
		if(strlen($message) <= 140 && count($logs) < 10){
			$this->Group->query("INSERT INTO logs (user_id,package_id,content,timestamp) VALUES (".$_SESSION['Auth']['User']['id'].",-1,'".addslashes($message)."','".date("Y-m-d H:i:s")."')");
			
			$url = SERVER_BACKUP."promotions/sendPromoMsg";
			$data = array();
			$data['message'] = $message;
			$this->General->curl_post_async($url,$data);
		}
		$this->autoRender = false;
	}
	
	function sendPromoMsg(){
		$message = $_REQUEST['message'];
		$message .= "\nvia smstadka.com";
		$j = 0;
		while($j <= 40){
 			$num = $j*250;
	 		$data = $this->Group->query("SELECT market_data.mobile,market_data.id FROM log_misscall,market_data WHERE market_data.mobile = log_misscall.mobile AND dnd_flag = 0 AND misscall='02261512288' group by market_data.mobile order by market_data.id LIMIT $num,250");
			if(empty($data))break;
	 		$mobiles = array();
	 		$ids = array();
	 		foreach($data as $mobi){
				$mobiles[] = $mobi['market_data']['mobile'];
				$ids[] = $mobi['market_data']['id'];
			}
			//$this->printArray($mobiles);exit;
			$this->General->sendMessageVia247SMS('pvMNIT',$mobiles,$message);
			$data = $this->Group->query("update market_data set count = count + 1 where id in (".implode(",",$ids).")");
			
	 		sleep(300);
	 		$j++;
		}
		$this->autoRender = false;
	}*/
	
        function smsOutgoingMonitoring($vendorsStr = null){
            $this->autoRender = false;
            //echo date("Y-m-d H:i:s",time()-900);return;
                 
                            
            $v1 = "routeSMS";// delivery_routesms
            $v2 = "sms24x7"; // msg247smsdellog
            $v3 = "vfirst";    // delivery_vfirst
            $v4 = "modem";   // delivery_modem
            $v5 = "tata";   // Tata
            $vendors = array();
            if($vendorsStr == null){                
                $vendors = array("tata","sms24x7","routeSMS","vfirst","modem");
            }else{
                $vendors = explode(",", $vendors);
            }
           $vendorsOutGoing = array();
            foreach($vendors as $v){
                if($v == $v1){// delivery_routesms
                    /*
                    DELIVRD 	386273
                    EXPIRED 	10215
                    FAILED 	12
                    REJECTD 	25
                    SENT 	19446
                    UNDELIV 	1014
                    */
                    
                   $qStr = "SELECT `status` , count( * ) as cnt 
                            FROM `delivery_routesms`
                            WHERE      `created` >= '".date("Y-m-d H:i:s",time()-1800)."'
                                   AND `created` <= '".date("Y-m-d H:i:s",time()-180)."'
                                   AND `date` = '".date("Y-m-d")."'
                            GROUP BY `status`";
                   
                   $data = $this->Group->query($qStr);
                   $arr  = array();
                   $total = 0;
                  $arr['UNDELIVERED']['count']  = 0;
                  $arr['DELIVERED']['count']    = 0;
                  $arr['UNRESOLVED']['count']   = 0;

                  $arr['UNDELIVERED']['status']  = "UNDELIVERED";
                  $arr['DELIVERED']['status']    = "DELIVERED";
                  $arr['UNRESOLVED']['status']   = "UNRESOLVED";
                  
                  $arr['UNDELIVERED']['per']  = "-";
                  $arr['DELIVERED']['per']    = "-";
                  $arr['UNRESOLVED']['per']   = "-";
                   if( count($data) != 0 ){//
                      foreach($data as $d){
                          
                          /*DELIVRD 	386273
                            EXPIRED 	10215
                            FAILED 	12
                            REJECTD 	25
                            SENT 	19446
                            UNDELIV 	1014*/
                          if($d['delivery_routesms']['status'] == "EXPIRED" || $d['delivery_routesms']['status'] == "FAILED" || $d['delivery_routesms']['status'] == "REJECTD" || $d['delivery_routesms']['status'] == "UNDELIV"){ // failed condition
                              //$arr['UNDELIVERED'] = $temp;
                              $arr['UNDELIVERED']['status'] = 'UNDELIVERED';
                              $arr['UNDELIVERED']['count']  = $arr['UNDELIVERED']['count'] + $d[0]['cnt'];
                          }else if( $d['delivery_routesms']['status'] == "DELIVRD" || $d['delivery_routesms']['status'] == "DELIVERED" || $d['delivery_routesms']['status'] == "DELIVRED" || $d['delivery_routesms']['status'] == "DELIVERD"){//Success condition
                              $arr['DELIVERED']['status'] = 'DELIVERED';
                              $arr['DELIVERED']['count']  = $arr['DELIVERED']['count'] + $d[0]['cnt'];
                          }else {// Inprocess condition //if($temp['status'] == "SENT" || $temp['status'] == null )
                              $arr['UNRESOLVED']['status'] = $d['delivery_routesms']['status'];
                              $arr['UNRESOLVED']['count']  = $arr['UNRESOLVED']['count'] + $d[0]['cnt'];
                          }                          
                          
                          $total = $total + $d[0]['cnt'];                          
                      
                      } 
                      foreach($arr as $key => $d){
                           //$total += $d[0]['cnt'];
                           $arr[$key]['per']  = $d['count'] / $total * 100;
                          // $arr[$key]['per']  = 100 - $arr[$key]['per'] ;
                      }
                      
                   }else{
                     //problem   
                     $arr['MSG']  =  "Not any entry found for RouteSMS ";
                   }
                   $vendorsOutGoing['RouteSMS']=$arr;
                   
                }else if($v == $v2){// msg247smsdellog
                    /*
                    NULL                1451
                    DELIVERED           5174
                    Undeliverable 	9
                     */
                    $qStr = "SELECT `status` , count( * ) as cnt 
                            FROM `msg247smsdellog`
                            WHERE      `created` >= '".date("Y-m-d H:i:s",time()-1800)."'
                                   AND `created` <= '".date("Y-m-d H:i:s",time()-180)."'
                                   AND `date` = '".date("Y-m-d")."'
                            GROUP BY `status`";
                   
                   
                   $data = $this->Group->query($qStr);
                   $arr  = array();
                   $total = 0;
                  $arr['UNDELIVERED']['count']  = 0;
                  $arr['DELIVERED']['count']    = 0;
                  $arr['UNRESOLVED']['count']   = 0;

                  $arr['UNDELIVERED']['status']  = "UNDELIVERED";
                  $arr['DELIVERED']['status']    = "DELIVERED";
                  $arr['UNRESOLVED']['status']   = "UNRESOLVED";
                  
                  $arr['UNDELIVERED']['per']  = "-";
                  $arr['DELIVERED']['per']    = "-";
                  $arr['UNRESOLVED']['per']   = "-";
                   if( count($data) != 0 ){//
                      
                      
                      foreach($data as $d){
                          
                          if($d['msg247smsdellog']['status'] == "Undeliverable"){ // failed condition
                              //$arr['UNDELIVERED'] = $temp;
                              $arr['UNDELIVERED']['status'] = 'UNDELIVERED';
                              $arr['UNDELIVERED']['count']  = $arr['UNDELIVERED']['count'] + $d[0]['cnt'];
                          }else if( $d['msg247smsdellog']['status'] == "DELIVERED" ){//Success condition
                              $arr['DELIVERED']['status'] = 'DELIVERED';
                              $arr['DELIVERED']['count']  = $arr['DELIVERED']['count'] + $d[0]['cnt'];
                          }else {// Inprocess condition //if($temp['status'] == "SENT" || $temp['status'] == null )
                              $arr['UNRESOLVED']['status'] = $d['msg247smsdellog']['status'];
                              $arr['UNRESOLVED']['count']  = $arr['UNRESOLVED']['count'] + $d[0]['cnt'];
                          }                          
                          
                          $total = $total + $d[0]['cnt'];                          
                      
                      } 
                      foreach($arr as $key => $d){
                           //$total += $d[0]['cnt'];
                           $arr[$key]['per']  = $d['count'] / $total * 100;
                          // $arr[$key]['per']  = 100 - $arr[$key]['per'] ;
                      }
                      
                   }else{
                     //problem   
                      $arr['MSG']  =  "Not any entry found for SMS-24X7 ";
                   }
                   $vendorsOutGoing['SMS24x7']=$arr;
                   
                }else if($v == $v5){// tata delivery
                   $qStr = "SELECT `status` , count( * ) as cnt 
                            FROM `msgTataSmsDelLog`
                            WHERE      `created` >= '".date("Y-m-d H:i:s",time()-1800)."'
                                   AND `created` <= '".date("Y-m-d H:i:s",time()-180)."'
                                   AND `date` = '".date("Y-m-d")."'
                            GROUP BY `status`";
                   
                   
                   $data = $this->Group->query($qStr);
                   $arr  = array();
                   $total = 0;
                  $arr['UNDELIVERED']['count']  = 0;
                  $arr['DELIVERED']['count']    = 0;
                  $arr['SENT']['count']   = 0;

                  $arr['UNDELIVERED']['status']  = "UNDELIVERED";
                  $arr['DELIVERED']['status']    = "DELIVERED";
                  $arr['SENT']['status']   = "SENT";
                  
                  $arr['UNDELIVERED']['per']  = "-";
                  $arr['DELIVERED']['per']    = "-";
                  $arr['SENT']['per']   = "-";
                   if( count($data) != 0 ){//
                      
                      
                      foreach($data as $d){
                          
                          if($d['msgTataSmsDelLog']['status'] == "Failed"){ // failed condition
                              $arr['UNDELIVERED']['status'] = 'UNDELIVERED';
                              $arr['UNDELIVERED']['count']  = $arr['UNDELIVERED']['count'] + $d[0]['cnt'];
                          }else if( $d['msgTataSmsDelLog']['status'] == "Delivered" ){//Success condition
                              $arr['DELIVERED']['status'] = 'DELIVERED';
                              $arr['DELIVERED']['count']  = $arr['DELIVERED']['count'] + $d[0]['cnt'];
                          }else {// Inprocess condition //if($temp['status'] == "SENT" || $temp['status'] == null )
                              $arr['SENT']['status'] = 'SENT';
                              $arr['SENT']['count']  = $arr['SENT']['count'] + $d[0]['cnt'];
                          }                          
                          
                          $total = $total + $d[0]['cnt'];                          
                      
                      } 
                      foreach($arr as $key => $d){
                           //$total += $d[0]['cnt'];
                           $arr[$key]['per']  = $d['count'] / $total * 100;
                          // $arr[$key]['per']  = 100 - $arr[$key]['per'] ;
                      }
                      
                   }else{
                     //problem   
                      $arr['MSG']  =  "Not any entry found for SMS-Tata ";
                   }
                   $vendorsOutGoing['SMSTata']=$arr;
                   
                }else if($v == $v3){// delivery_vfirst
                    /*
                    Sent.                1451
                     */
                    $qStr = "SELECT `status` , count( * ) as cnt 
                            FROM `delivery_vfirst`
                            WHERE      `created` >= '".date("Y-m-d H:i:s",time()-1800)."'
                                   AND `created` <= '".date("Y-m-d H:i:s",time()-180)."'
                                   AND `date` = '".date("Y-m-d")."'
                            GROUP BY `status`";
                   
                   
                   $data = $this->Group->query($qStr);
                   $arr  = array();
                   $total = 0;
                  $arr['UNDELIVERED']['count']  = 0;
                  $arr['DELIVERED']['count']    = 0;
                  $arr['UNRESOLVED']['count']   = 0;

                  $arr['UNDELIVERED']['status']  = "UNDELIVERED";
                  $arr['DELIVERED']['status']    = "DELIVERED";
                  $arr['UNRESOLVED']['status']   = "SENT";
                  
                  $arr['UNDELIVERED']['per']  = "-";
                  $arr['DELIVERED']['per']    = "-";
                  $arr['UNRESOLVED']['per']   = "-";
                   if( count($data) != 0 ){//
                      
                      
                      foreach($data as $d){
                          
                          if($d['delivery_vfirst']['status'] == "Undeliverable"){ // failed condition
                              //$arr['UNDELIVERED'] = $temp;
                              $arr['UNDELIVERED']['status'] = 'UNDELIVERED';
                              $arr['UNDELIVERED']['count']  = $arr['UNDELIVERED']['count'] + $d[0]['cnt'];
                          }else if( $d['delivery_vfirst']['status'] == "DELIVERED" ){//Success condition
                              $arr['DELIVERED']['status'] = 'DELIVERED';
                              $arr['DELIVERED']['count']  = $arr['DELIVERED']['count'] + $d[0]['cnt'];
                          }else {// Inprocess condition //if($temp['status'] == "SENT" || $temp['status'] == null )
                              $arr['UNRESOLVED']['status'] = $d['delivery_vfirst']['status'];
                              $arr['UNRESOLVED']['count']  = $arr['UNRESOLVED']['count'] + $d[0]['cnt'];
                          }                          
                          
                          $total = $total + $d[0]['cnt'];                          
                      
                      } 
                      foreach($arr as $key => $d){
                           //$total += $d[0]['cnt'];
                           $arr[$key]['per']  = $total==0 ? 0 : round($d['count'] / $total * 100 , 0);
                          // $arr[$key]['per']  = 100 - $arr[$key]['per'] ;
                      }
                      
                   }else{
                     //problem   
                      $arr['MSG']  =  "Not any entry found for VFirst ";
                   }
                   $vendorsOutGoing['VFirst']=$arr;
                }else if($v == $v4){// delivery_modem
                    
                   $qStr = "SELECT `status` , count( * ) as cnt 
                            FROM `delivery_modem`
                            WHERE      `created` >= '".date("Y-m-d H:i:s",time()-1800)."'
                                   AND `created` <= '".date("Y-m-d H:i:s",time()-180)."'
                                   AND `date` = '".date("Y-m-d")."'
                            GROUP BY `status`";
                   
                   $data = $this->Group->query($qStr);
                   $arr  = array();
                   $total = 0;
                  $arr['UNDELIVERED']['count']  = 0;
                  $arr['DELIVERED']['count']    = 0;
                  $arr['UNRESOLVED']['count']   = 0;

                  $arr['UNDELIVERED']['status']  = "UNDELIVERED";
                  $arr['DELIVERED']['status']    = "DELIVERED";
                  $arr['UNRESOLVED']['status']   = "UNRESOLVED";
                  
                  $arr['UNDELIVERED']['per']  = "-";
                  $arr['DELIVERED']['per']    = "-";
                  $arr['UNRESOLVED']['per']   = "-";
                   if( count($data) != 0 ){//
                      
                      foreach($data as $d){
                          /*
                            Delivered<br/>From mobile: 8879201468 	130
                            Delivered<br/>From mobile: 8879201470 	127
                            Delivered<br/>From mobile: 8879202881 	61
                            Delivered<br/>From mobile: 8879256827 	169
                            Not delivered<br/>From mobile: 8879201470 	1
                            Not delivered<br/>From mobile: 8879202881 	2
                            SENT 	2
                             */
                          $sarr = explode("<br/>",$d['delivery_modem']['status']);
                          if(trim($sarr[0]) == "Delivered" ){ // failed condition
                              //$arr['UNDELIVERED'] = $temp;
                              $arr['UNDELIVERED']['status'] = 'UNDELIVERED';
                              $arr['UNDELIVERED']['count']  = $arr['UNDELIVERED']['count'] + $d[0]['cnt'];
                          }else if( trim($sarr[0]) == "Not delivered" ){//Success condition
                              $arr['DELIVERED']['status'] = 'DELIVERED';
                              $arr['DELIVERED']['count']  = $arr['DELIVERED']['count'] + $d[0]['cnt'];
                          }else {// Inprocess condition //if($temp['status'] == "SENT" || $temp['status'] == null )
                              $arr['UNRESOLVED']['status'] = $d['delivery_modem']['status'];
                              $arr['UNRESOLVED']['count']  = $arr['UNRESOLVED']['count'] + $d[0]['cnt'];
                          }                          
                          
                          $total = $total + $d[0]['cnt'];                          
                      
                      } 
                      foreach($arr as $key => $d){
                           //$total += $d[0]['cnt'];
                           $arr[$key]['per']  = $d['count'] / $total * 100;
                          // $arr[$key]['per']  = 100 - $arr[$key]['per'] ;
                      }
                      
                   }else{
                     //problem   
                         $arr['MSG']  =  "Not any entry found for Modem ";
                   }
                   $vendorsOutGoing['Modem']=$arr;
                   
                }
                
                
            }
            
            $this->set('vendorsOutGoing',$vendorsOutGoing);
            //print_r($vendorsOutGoing);
           echo json_encode($vendorsOutGoing);
            //$this->render('sms_outgoing_monitoring');
        }
}
?>