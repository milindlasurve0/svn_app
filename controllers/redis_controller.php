<?php

class RedisController extends AppController {

	var $name = 'Redis';
	var $components = array('General');
	var $uses = array('SMSApp','User','Metadata');
        var $redis;
        
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
                
                $this->autoLayout = $this->autoRender = FALSE;
                $this->redis = $this->General->redis_connect();
		
	}
        
        function insertInQ() {
            $this->autoLayout = $this->autoRender = FALSE;
            $val = json_encode($_REQUEST);
            $this->redis->lpush("smstadkaredis", $val);
            
            $logger = $this->General->dumpLog('Redis-insertInQ', 'incomingRequests_other');
			$logger->info($val);  
        }
        
		function insertInQsms() {
            $this->autoLayout = $this->autoRender = FALSE;
            $val = json_encode($_REQUEST);
            $this->redis->lpush("smstadka", $val);
            
            $logger = $this->General->dumpLog('Redis-insertInQ', 'incomingRequests');
			$logger->info($val);  
        }
        
        function SendSMSXML($xml){
            file_put_contents(LOG_PATH.date('Ymd').".txt", "Requests parameters from SMSAPI |".urldecode($xml)."/n/n",FILE_APPEND);
            
        }
        
        
        function test(){
        	$this->redis->lpush("smstadka", json_encode(array('name'=>'ashish')));
        	$len = $this->redis->llen("smstadka");
        	
        	echo $len = ($len >= 50) ? 50 : $len;
        	$data = $this->redis->lrange("smstadka",0,2);
        	print_r($data);
        	//echo "1";
        	$this->autoRender = FALSE;
        }



}
