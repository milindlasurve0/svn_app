<?php
class RssController extends AppController {

	var $name = 'Rss';
	var $helpers = array('Html','Ajax','Javascript');
	var $components = array('RequestHandler');
	var $RSS_Content = array();
	var $package_id;

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
	}
	
	function getFeeds($par1,$par2,$pck_id=null){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			if($pck_id == null)
				$this->package_id = $_POST['pck_id'];
			else 
				$this->package_id = $pck_id;	
			
			$this->Rss->recursive = -1;
			$data = $this->Rss->find('first',array('conditions' => array('Rss.package_id' => $this->package_id)));
			$rssData = $this->General->RSS_Display($data['Rss']['feed_url'],20);
			
			foreach($rssData as $article)
			{
				$type = $article["type"];
				$title = $article["title"];
				$link = $article["link"];
				$date = $article["date"];
				$description = $article["description"];
				
				//inserting the values into database
				$this->Rss->query("insert into rss_feeds (rss_id,title,link,description) values (".$data['Rss']['id'].",'".addslashes($title)."','".addslashes($link)."','".addslashes($description)."')");
			}
			$this->releaseLock();
			$this->autoRender = false;
		}
	}
	
	function readUnreadFeed(){
		$feedid = $_POST['feedid'];
		$read = $_POST['read'];
		
		$this->Rss->query("update rss_feeds set flag = $read where id = $feedid");
		$this->autoRender = false;
	}
	
	function refreshFeeds(){
		$pack_id = $_POST['packid'];
		$arr = explode(",",AUTOMATED_PACKS);
		if(in_array($pack_id,$arr)) {
			$feedData = $this->Rss->query("select * from rss_feeds, rsses where rsses.id = rss_feeds.rss_id and rsses.package_id = " .$pack_id. " and flag = 0 order by rss_feeds.id desc limit 10");
		}
		
		$this->set('packid',$pack_id);
		$this->set('feedData',$feedData);
		$this->render('/elements/feed_data','ajax');
	}
}

	?>