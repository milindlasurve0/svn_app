<?php
class TagsController extends AppController {

	var $name = 'Tags';
	var $helpers = array('Html','Ajax','Javascript','Paginator','Minify');
	var $components = array('General');
	var $uses = array('Tag','MessagesTag','Message','Category');


	function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('*');
		$this->Auth->allowedActions = array('view','getTaggedUsers');
		$this->dateMsg = MSG_DATE;
	}
	
	var $paginate = array(	
		'MessagesTag' => array('limit' => 10,
								'fields' => array('Message.id','Message.content','Message.title','Message.url','Category.name'),
								 'order' => array('rating' => 'desc','Message.id' => 'desc'),
								 'joins' => array(
							          array(
							            'table' => 'messages'
							            , 'type' => 'INNER'
							            , 'alias' => 'Message'
							          
							            , 'conditions' => array(
							                'Message.id = MessagesTag.message_id',
							                'Message.toshow = 1',
							            	'DATE(Message.created) < "2011-06-30"'
							            )),
							            array(
							            'table' => 'categories'
							            , 'type' => 'INNER'
							            , 'alias' => 'Category'
							          
							            , 'conditions' => array(
							                'Category.id = Message.category_id',
							                'Category.toshow = 1'
							            ))
								
								))
	);

	function index() {
		$this->Tag->recursive = 0;
		$this->set('tags', $this->paginate());
	}

	function view($tag) {
		if (!$tag) {
			//$this->Session->setFlash(__('Invalid tag', true));
			$this->redirect(array('controller' => 'users','action' => 'er404','404'));
		}
		
		$this->Session->write('Auth.redirect',null);
		//$tag_name = $this->General->urlToName($tag);
		
		$tagData = $this->Tag->find('first',array('conditions' => array('Tag.url' => $tag)));
		
		if(empty($tagData)){
			//$this->Session->setFlash(__('Invalid tag', true));
			$this->redirect(array('controller' => 'users','action' => 'er404','404'));
		}
		
		$msgData = $this->paginate('MessagesTag',array('MessagesTag.tag_id' => $tagData['Tag']['id']));
		
		   
		$i = 0;
		$msgIds = array();
		foreach($msgData as $msg){
			$this->Message->filterBindings(array('Category','Author'));
			$data = $this->Message->find('first',array('conditions' => array('Message.id' => $msg['Message']['id'])));
			
			$msgData[$i]['Tag'] = $data['Tag'];
			$msgIds[] = $msg['Message']['id'];
			$i++;
		}
		
		$this->Tag->recursive = -1;
		$simTags = $this->Tag->find('all', array(
						'fields' => array('Tag.name','count(messages_tags.tag_id) as counts'),
						'joins' => array(
						    array(
						        'table' => 'messages_tags',
						        'type' => 'inner',
						        'conditions'=> array('messages_tags.tag_id = Tag.id')
						    )
					    
						),
						'conditions' => array('Tag.id != '. $tagData['Tag']['id'], 'messages_tags.message_id in ('.implode(",",$msgIds).')'),
						'group' => 'Tag.id',
						'order'=> array('counts DESC'),
						'limit' => '5'));
		$tagTitles = '';
		foreach ($simTags as $tags) {
			$tagTitles .= $tags['Tag']['name'] . ", ";
		}				
						
		if($this->Session->read('Message')){
			$this->Session->write('Auth.redirect','/');
			$this->set('msgPars',$this->Session->read('Message'));
			$this->Session->delete('Message');
		}
		
		$this->set('tagData', $tagData);
		$this->set('msgData', $msgData);
		$popular = POPULAR_ARRAY;
		if($tagData['Tag']['id'] == '60') $popular = '142,'.$popular; 
		$packs = $this->getDisplayPackages($popular);
		$this->set('packData',$packs);
		$pageTitle = $tagData['Tag']['name'] . " - " . $tagTitles . " SMS Alerts";
		$pageDesc = $pageTitle . ". Get SMS Alert on SMS Wishes, SMS Quotes, SMS Shayari, SMS Jokes";
		$this->set('pageTitle',$pageTitle);
		$this->set('pageDesc',$pageDesc);
		
		//$query = "SELECT tags.url,tags.name,count(`messages_tags`.tag_id) as counts FROM `messages_tags`,tags where tags.id  = `messages_tags`.tag_id group by `messages_tags`.tag_id having count(`messages_tags`.tag_id) > 200";
		$this->set('tagCloud',$this->General->tagCloud());
		
		//$this->set('pageH1',$tagData['Tag']['name']);
		//$this->set('pageFooter',$tagData['Tag']['name']);
	}

	function add() {
		if (!empty($this->data)) {
			$this->data['Tag']['url'] = $this->General->makeUrl($this->data['Tag']['name']);
			$this->Tag->create();
			if ($this->Tag->save($this->data)) {
				$this->Session->setFlash(__('The tag has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tag could not be saved. Please, try again.', true));
			}
		}
		$messages = $this->Tag->Message->find('list');
		$this->set(compact('messages'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid tag', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Tag']['url'] = $this->General->makeUrl($this->data['Tag']['name']);
			if ($this->Tag->save($this->data)) {
				$this->Session->setFlash(__('The tag has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tag could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Tag->read(null, $id);
		}
		$messages = $this->Tag->Message->find('list');
		$this->set(compact('messages'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for tag', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Tag->delete($id)) {
			$this->Session->setFlash(__('Tag deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Tag was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

	function addTags(){
		$this->render('/elements/add_tags');
	}

	function findTag(){

		$tag = $_REQUEST['tag'];
		$tag = trim(strtolower($tag));

		if($tag != ''){
			$names = $this->Tag->query("select name from tags where lower(name) like '". $tag . "%'");
			$html = "<ul>";

			//print_r($names);
			foreach($names as $name){
				$html .= "<li>" . $name['tags']['name'] . "</li>";
			}

			$html .= "</ul>";
			echo $html;
		}
		//$this->set('names',$names);
		$this->autoRender = false;
	}
	
	function submitTags(){
		$tags = explode(",",$this->data['Tag']['names']);

		foreach($tags as $tag){
			$tag = trim(strtolower($tag));
			$tag = ucfirst($tag);
			$this->data['Tag']['name'] = $tag;
			$this->data['Tag']['url'] = $this->General->makeUrl($tag);
			$this->Tag->create();
			$this->Tag->save($this->data);
		}
		
		$this->redirect(array('action' => 'addTags'));
		
	}
	
	function getTags(){
		$cat_id = $_REQUEST['catid'];
		//$msg_id = $_REQUEST['msgid'];
		$this->Category->recursive = -1;
		
		$catName = $this->Category->find('first', array('fields' => array('Category.name'), 'conditions' => array('Category.id' => 	$cat_id)));
		
		$name = $catName['Category']['name'];
		
		$oCats = $this->Category->find('all', array('fields' => array('Category.name'), 'conditions' => array('Category.name !=' => 	$name,'Category.parent' => FUN_ID,'Category.name like "SMS%"')));
		
		//$this->printArray($oCats);
		$query = "select name,id from tags where 1";
		foreach($oCats as $cat){
			$arr = explode(' ',$cat['Category']['name']);
			$query .= " and name not like '%". $arr[1] . "%' ";
		}
		//echo $query;
		$query .= "order by name asc";
		$tags = $this->Category->nativeQuery($query,true,'tag'.$cat_id);
		
		$str = 'Choose tags : <br><table><tr>';
		$i = 0;
		foreach($tags as $tag){
			$str .= 	'<td><input type="checkbox" name="data[Tag]['.$tag['tags']['id'].']" value="'.$tag['tags']['id'].'">' . $tag['tags']['name'] . "</td>";
			$i++;
			if($i%6 == 0)
				$str .= "</tr><tr>";
		}
		
		echo $str ."</tr></table><br>";
		
		$this->autoRender = false;
	}
	
	function getTaggedUsers($tag){
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/users/view');
		$data = $this->General->getTaggedUsers($tag);
		$this->set('data',$data);
	}
	/*function toUrl(){ //script which makes url of all the tags names
		$msgData = $this->Tag->find('all');
		
		foreach($msgData as $msg){
			$this->data['Tag']['id'] = $msg['Tag']['id'];			
			$this->data['Tag']['url']  = $this->General->makeUrl($msg['Tag']['name']);
			$this->Tag->save($this->data);
		}
		$this->autoRender = false;		
	}*/
	

}
?>