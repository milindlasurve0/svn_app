<?php
class TransactionsController extends AppController {

	var $name = 'Transactions';
	var $helpers = array('Html','Ajax','Javascript');
	var $uses = array('Transaction','User');
	var $components = array('RequestHandler');

	function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('*');
	}
	
	function index() {
		$this->Transaction->recursive = 0;
		$this->set('transactions', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid transaction', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('transaction', $this->Transaction->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Transaction->create();
			if ($this->Transaction->save($this->data)) {
				$this->Session->setFlash(__('The transaction has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.', true));
			}
		}
		$users = $this->Transaction->User->find('list');
		$packages = $this->Transaction->Package->find('list');
		$messages = $this->Transaction->Message->find('list');
		$this->set(compact('users', 'packages', 'messages'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid transaction', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Transaction->save($this->data)) {
				$this->Session->setFlash(__('The transaction has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Transaction->read(null, $id);
		}
		$users = $this->Transaction->User->find('list');
		$packages = $this->Transaction->Package->find('list');
		$messages = $this->Transaction->Message->find('list');
		$this->set(compact('users', 'packages', 'messages'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for transaction', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Transaction->delete($id)) {
			$this->Session->setFlash(__('Transaction deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Transaction was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	function getAlltransactions() {
		$this->Transaction->recursive = -1;
		/*$subQuery1 = "SELECT Transaction.user_id as user_id, Transaction.package_id as package_id, Transaction.message_id as message_id,Transaction.amount as amount, Transaction.type as type, '' as app_id, Transaction.timestamp as timestamp, packages.name as name FROM transactions AS `Transaction` left JOIN packages  ON (`package_id` = `packages`.`id`)";
		$subQuery2 = "SELECT AppsTransaction.user_id as user_id,  '' as package_id, '' as message_id, AppsTransaction.amount as amount, AppsTransaction.type as type, AppsTransaction.app_id as app_id, AppsTransaction.timestamp as timestamp, apps.name as name FROM apps_transactions AS `AppsTransaction` left JOIN s_m_s_apps as apps  ON (`app_id` = `apps`.`id`)";
		$subQuery = "SELECT * FROM ((".$subQuery1.") UNION (".$subQuery2.")) as transactions WHERE user_id = ".$this->Session->read('Auth.User.id')."  ORDER BY timestamp DESC";
		$resultTransaction = $this->Transaction->query($subQuery);
		*/
        $subQuery1 = "SELECT Transaction.user_id as user_id, Transaction.package_id as package_id, Transaction.message_id as message_id,Transaction.amount as amount, Transaction.type as type, '' as app_id, Transaction.timestamp as timestamp, packages.name as name, categories.name as category_name FROM transactions AS `Transaction` 
                                                                left JOIN packages  ON (`package_id` = `packages`.`id`)
                                                                left JOIN categories_packages  ON (`categories_packages`.`package_id` = `packages`.`id`)
                                                                left JOIN categories  ON ( `categories`.`id` = `categories_packages`.`category_id` )
                                                                ";
		$subQuery2 = "SELECT AppsTransaction.user_id as user_id, '' as package_id, '' as message_id, AppsTransaction.amount as amount, AppsTransaction.type as type, AppsTransaction.app_id as app_id, AppsTransaction.timestamp as timestamp, apps.name as name , '' as category_name  FROM apps_transactions AS `AppsTransaction` left JOIN s_m_s_apps as apps  ON (`app_id` = `apps`.`id`)";
		$subQuery  = "SELECT * FROM ((".$subQuery1.") UNION (".$subQuery2.")) as transactions WHERE user_id = ".$this->Session->read('Auth.User.id')."  ORDER BY timestamp DESC ";//LIMIT 10
		$resultTransaction = $this->Transaction->query($subQuery);
		//print_r($resultTransaction);
		$this->set('resultTransaction',$resultTransaction);
		$this->render('/elements/transactions');
	}
}
?>