<?php

class RemindersController extends AppController {

	var $name = 'Reminders';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler');
	var $uses = array('Reminder','SMSApp','Remcron','Friendlist','AppsLog','Grouplist');
	var $controller_name = 'reminder';

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allowedActions = array('uploadExcel','about','sendRemCron','createRemCronEntry','validateRem');
	}
	
	function uploadExcel(){
		if(strtolower(substr(strrchr($_FILES['excelfile']['name'],"."),1)) != 'csv'){
			echo "<span class='inlineErr1'>Only CSV file can be imported.<span>";
		}else{
			$fp = fopen($_FILES['excelfile']['tmp_name'],'r') or die("can't open file");
			$invalidNos = 0;
			$invalidNames = 0;
			$totalRecs = 0;	
			$validRecs = 0;
			$recImp = 0;		
			while($csv_line = fgetcsv($fp,1024)){
				$totalRecs++;
				$csvLine = count($csv_line);
				//echo $csvLine."<br>";
				$invFlag = 0;	   
			    for ($i = 0, $j = $csvLine; $i < $j; $i++) {
			        //print $csv_line[$i]."<br>";
			    	if($i == 0){
			        	if(trim($csv_line[$i]) == ''){
			        		$invFlag = 1;
			        		$invalidNames++;	
			        	}
			        }
			        if($i == 1){
			        	if($this->General->mobileValidate($csv_line[$i]) == 1){
			        		$invFlag = 1;
							$invalidNos++;        		
			        	}
			        }
			    }
			    
			    if($invFlag == 0){
			    	$validRecs++;
					$this->data['Friendlist']['nickname'] = $csv_line[0];
					$this->data['Friendlist']['mobile'] = $csv_line[1];					
					$this->data['Friendlist']['user_id'] = $_SESSION['Auth']['User']['id'];
			
					$this->Friendlist->create();
					if($this->Friendlist->save($this->data)){
						$recImp++;
					}					
			    }
			}
			
			$invR = $totalRecs - $validRecs;
			//echo "<br>Total Records = ".$totalRecs;
			//echo "<br>Total Invalid Records = ".$invR;
			//echo "<br>Total Invalid Names = ".$invalidNames;
			//echo "<br>Total Invalid Numbers = ".$invalidNos;
			echo "<br><b>Successfully imported ".$recImp. " out of " .$totalRecs. ".</b>" ;
			//echo "<br>Total Records Imported = ".$recImp;
		
			echo '<br><a class="sel" onclick="simContact();" >View All Contacts</a>';
			
			fclose($fp) or die("can't close file");
		}
		/*echo $_FILES['excelfile']['tmp_name'];
		echo strtolower(substr(strrchr($_FILES['excelfile']['name'],"."),1));
		print_r($_POST);
		print_r($_FILES);
		print_r($this->data);
		echo "dinesh";*/
		$this->autoRender = false;
	}
	
	function initial($about = null){
		if($about == null)
			$about = $_REQUEST['about'];
		$this->set('about',$about);
		$this->render('initial');
	}
	
	function about(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		$this->render('about_reminder');
	}
	
	function faqs(){
		$this->render('faqs');
	}
	
	function autoCompleteFrnd() {
		//Partial strings will come from the autocomplete field as
		//$this->data['Post']['subject']
		$this->Friendlist->recursive= -1;
		$friendList = $this->Friendlist->find('all',array(
							'fields' => array('Friendlist.nickname','Friendlist.mobile'),
							'conditions' => array(
								'AND' => array('Friendlist.user_id = '.$this->Session->read('Auth.User.id'),
									'OR'=> array(
										 'Friendlist.nickname LIKE ' => $this->data['appRem']['For'].'%',
									 	 'Friendlist.mobile LIKE ' => $this->data['appRem']['For'].'%'
									 )
								)	
							)
		)); 
		$this->set('friendList',$friendList);
		$this->layout = 'ajax';
	}
	
	function sendRemCron() {
		$this->lock();
		$this->SMSApp->recursive= -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$nw = date('H:i').":00";
		$nwPlusThirty = date('H:i',strtotime($nw.' + 30 minutes')).":00";
		if($nwPlusThirty < $nw){
			$nw = '00:00:00';
		}				
		$reminders = $this->Remcron->find('all', array(			
				'conditions' => array('Remcron.del_time >= "'.$nw.'" and Remcron.del_time <= "'.$nwPlusThirty.'"')			
		));
		//$rem_charge = APP_REM_REC_CHARGE;
		foreach($reminders as $rem){
			$id = $rem['Remcron']['id'];
			$user_id = $rem['Remcron']['user_id'];
			$reminder_id = $rem['Remcron']['reminder_id'];
			$message = stripslashes($rem['Remcron']['message']);
			$dont_send_flag = $rem['Remcron']['dont_send_flag'];
			$del_time = $rem['Remcron']['del_time'];
			$mobile = $rem['Remcron']['mobile'];
			
			$msgLen  = strlen($message);
			$x = ceil($msgLen/DEFAULT_MESSAGE_LENGTH);
			$con = $x*DEFAULT_MESSAGE_LENGTH - APP_REM_MSG_FIXED;
			if($msgLen > $con)
				$remCharge = ($x + 1)*EACH_MESSAGE_COST;
			else
				$remCharge = $x*EACH_MESSAGE_COST;
				
			$rem_charge = $remCharge/100;
			
			
			$usr_bal = $this->General->getBalance($user_id);				
			if($usr_bal >= $rem_charge || $dont_send_flag == '0'){
				$tran_id = null;
				if($dont_send_flag == '1'){
					///entry in transaction
					$tran_id = $this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$rem_charge,$data['SMSApp']['id'],$reminder_id,$user_id);
					///update user balance
					$this->General->balanceUpdate($rem_charge,'subtract',$user_id);
				}
				//entry in Log table
				$log_id = $this->General->appLogUpdate($data['SMSApp']['id'],$reminder_id,$tran_id,$message,$mobile,$user_id);
				//send message
				$sender_query = $this->Reminder->query("SELECT mobile from users where id = ".$user_id);
                $sender_mobile = $sender_query['0']['users']['mobile'];
				$this->General->sendMessage($sender_mobile,$mobile,$message,'app',$this->controller_name);
				//delete record
				$this->Remcron->delete($id);

			}else{
				$this->Remcron->delete($id);						
			}
		}
		$this->releaseLock();
		$this->autoRender = false;
	}
	
	function sendRemNw($id) {
		$this->SMSApp->recursive= -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));					
		$reminders = $this->Remcron->find('all', array(			
				'conditions' => array('Remcron.reminder_id'=>$id)			
		));
		
		$receivers = array();
		foreach($reminders as $rem){
			$id = $rem['Remcron']['id'];
			$user_id = $rem['Remcron']['user_id'];
			$reminder_id = $rem['Remcron']['reminder_id'];
			$message = $rem['Remcron']['message'];
			$dont_send_flag = $rem['Remcron']['dont_send_flag'];
			$del_time = $rem['Remcron']['del_time'];
			$mobile = $rem['Remcron']['mobile'];
			$receivers[] = $mobile;
										
			$tran_id = null;				
			//entry in Log table
			$log_id = $this->General->appLogUpdate($data['SMSApp']['id'],$reminder_id,$tran_id,$message,$mobile,$user_id);
			//send message
			//delete record
			$this->Remcron->delete($id);
		}
		$this->General->sendMessage($_SESSION['Auth']['User']['mobile'],implode(",",$receivers),$message,'app',$this->controller_name);
		$this->autoRender = false;
	}
	
	
	function createRemCronEntry($id = null,$ref_date = null) {
		$this->lock();	
		$this->Reminder->recursive = -1;
		if($id){		
			$reminders = $this->Reminder->find('all', array(			
				'conditions' => array('Reminder.id' => $id)			
			));
			$referenceDate = $ref_date;
		}else{
			$reminders = $this->Reminder->find('all', array(			
				'conditions' => array('Reminder.status' => 0)			
			));
			$referenceDate = date('Y-m-d',mktime(0, 0, 0, date("m"), date("d")+1, date("Y")));
		}
	
		
		foreach($reminders as $rem){				
					$id = $rem['Reminder']['id'];  // 41
                    $user_id = $rem['Reminder']['user_id'];  // 8
                    $Reminder_for = $rem['Reminder']['reminder_for'];  // 9892471157,9892471158
                    $Reminder_for_arr = explode(",",$Reminder_for);
                    $Reminder_for_arr_cnt = count($Reminder_for_arr);
                    $message = stripslashes($rem['Reminder']['message']);  // 5
                    $day = $rem['Reminder']['day'];  // 
                    $week = $rem['Reminder']['week'];  // 
                    $month = $rem['Reminder']['month'];  // 
                    $year = $rem['Reminder']['year'];  // 
                    $date = $rem['Reminder']['date'];  // 2010-11-19
                    $time = $rem['Reminder']['time'];  // 00:01:00
                    $end_date = $rem['Reminder']['end_date'];  // 0000-00-00
                    $created = $rem['Reminder']['created'];  // 2010-11-18 09:20:30
                    $modified = $rem['Reminder']['modified'];  // 2010-11-18 09:20:30
                    $first_timer_flag = $rem['Reminder']['first_timer_flag'];  // 0 or 1
					
                    $msgLen  = strlen($message);
					$x = ceil($msgLen/DEFAULT_MESSAGE_LENGTH);
					$con = $x*DEFAULT_MESSAGE_LENGTH - APP_REM_MSG_FIXED;
					if($msgLen > $con)
						$remCharge = ($x + 1)*EACH_MESSAGE_COST;
					else
						$remCharge = $x*EACH_MESSAGE_COST;
						
					$remCharge = $remCharge/100;
					
                    $rem_charge = $Reminder_for_arr_cnt*$remCharge;
                    
                    $active_flaq_query = $this->Reminder->query("SELECT active_flag from reminders where id = ".$id);
                    $active_flag = $active_flaq_query['0']['reminders']['active_flag'];  // 0 or 1
                    
                    if($first_timer_flag == 0){
                    	$this->data['Remcron']['dont_send_flag'] = 0;
					}else{
                    	$this->data['Remcron']['dont_send_flag'] = 1;
                    }
                     
                    if($first_timer_flag == 1){
                    	$usr_bal = $this->General->getBalance($user_id);
                    	if($usr_bal < $rem_charge){                    		
                    		if($active_flag == 0){
                    			$this->Reminder->query("UPDATE reminders set active_flag = 1,modified = '".date('Y-m-d H:i:s')."' where user_id = ".$user_id);
                    			//send a message of insufficient balance
                    			$active_rem_query = $this->Reminder->query("SELECT count(id) as cnt from reminders where user_id = ".$user_id." and status = 0");
                    			$inSufficientBalMsg = $this->General->createMessage("BHULAKKAD_NO_BALANCE",array($active_rem_query['0']['0']['cnt']));
                    			$sender_query = $this->Reminder->query("SELECT mobile from users where id = ".$user_id);
                    			$this->General->sendMessage(SMS_SENDER,$sender_query['0']['users']['mobile'],$inSufficientBalMsg,'template');                    			
                    		}                 		
                    		//continue;
                    	}else{
                    		if($active_flag == 1){
                    			$this->Reminder->query("UPDATE reminders set active_flag = 0 where user_id = ".$user_id);
                    		}
                    	}
                    }
                                    
                    //1 tym
                    if($day == ''  && $week == '' && $month == '' && $year == '' && strtotime($date) == strtotime($referenceDate)){
                    	$this->data['Remcron']['message'] = $message;
						$this->data['Remcron']['del_time'] = $time;
						$this->data['Remcron']['reminder_id'] = $id;
						$this->data['Remcron']['user_id'] = $user_id;
						
                    	for($a=0;$a<$Reminder_for_arr_cnt;$a++){                    	
							$this->data['Remcron']['mobile'] = $Reminder_for_arr[$a];
							$this->Remcron->create();
							$this->Remcron->save($this->data);
                    	}
						$this->Reminder->query("UPDATE reminders set status = 1,first_timer_flag= 1 where id = ".$id);						
						continue;						
                    }
                    
                    //daily
                    if($day != ''  && $week == '' && $month == '' && $year == '' && $end_date != '0000-00-00' && strtotime($referenceDate) > strtotime($end_date)){
                    	$this->Reminder->query("UPDATE reminders set status = 1 where id = ".$id);
                    	continue;
                    }
                    if($day != ''  && $week == '' && $month == '' && $year == '' &&  strtotime($date) <= strtotime($referenceDate) && ($end_date == '0000-00-00' || strtotime($referenceDate) <= strtotime($end_date))){                    	
                    	//(tomo_dateNum - start_dateNum)%freq == 0 then add
                    	if(round((strtotime($referenceDate) - strtotime($date)) / 86400)%$day == '0'){
                    		$this->data['Remcron']['message'] = $message;
							$this->data['Remcron']['del_time'] = $time;
							$this->data['Remcron']['reminder_id'] = $id;
							$this->data['Remcron']['user_id'] = $user_id;
	                    	for($b=0;$b<$Reminder_for_arr_cnt;$b++){                    	
								$this->data['Remcron']['mobile'] = $Reminder_for_arr[$b];
								$this->Remcron->create();
								$this->Remcron->save($this->data);
	                    	}							
							$this->Reminder->query("UPDATE reminders set first_timer_flag= 1 where id = ".$id);							
                    	}                                    	
						continue;						
                    }
                    
                    //weekly
                    if($day == ''  && $week != '' && $month == '' && $year == '' && $end_date != '0000-00-00' && strtotime($referenceDate) > strtotime($end_date)){
                    	$this->Reminder->query("UPDATE reminders set status = 1 where id = ".$id);
                    	continue;
                    }
                    if($day == ''  && $week != '' && $month == '' && $year == '' &&  strtotime($date) <= strtotime($referenceDate) && ($end_date == '0000-00-00' || strtotime($referenceDate) <= strtotime($end_date))){
                    	//tomo_date_weekDayNum is in (weekDayString) then
                    	//(week(tomo_date - start_date))%freq == 0 then add
                    	$weekDayTArr = explode('/',$week); //$weekDayTArr[0] = 1,2,3 etc and $weekDayTArr[1] = 1 to 30
                    	$weekDayArr = explode(',',$weekDayTArr[0]);
                    	
                    	//get start day of the week for tomorows date
                    	
						$dateArr = explode('-',$date);                    	
                    	$startDayWeek = date('Y-m-d',mktime(0, 0, 0, date($dateArr[1]), date($dateArr[2])-date('w', strtotime($referenceDate)), date($dateArr[0])));	 
                    	if(in_array(date('w', strtotime($referenceDate)),$weekDayArr)){
                    		if((floor(abs(strtotime($startDayWeek) - strtotime($referenceDate))/604800))%$weekDayTArr[1] == '0'){                    		
                    			$this->data['Remcron']['message'] = $message;
								$this->data['Remcron']['del_time'] = $time;
								$this->data['Remcron']['reminder_id'] = $id;
								$this->data['Remcron']['user_id'] = $user_id;
	                    		for($c=0;$c<$Reminder_for_arr_cnt;$c++){                    	
									$this->data['Remcron']['mobile'] = $Reminder_for_arr[$c];
									$this->Remcron->create();
									$this->Remcron->save($this->data);
		                    	}								
								$this->Reminder->query("UPDATE reminders set first_timer_flag= 1 where id = ".$id);								
                    		}
                    	}
                    	
						continue;						
                    }
                    
                    //monthly
                    if($day == ''  && $week == '' && $month != '' && $year == '' && $end_date != '0000-00-00' && strtotime($referenceDate) > strtotime($end_date)){
                    	$this->Reminder->query("UPDATE reminders set status = 1 where id = ".$id);
                    	continue;
                    }
                    if($day == ''  && $week == '' && $month != '' && $year == '' &&  strtotime($date) <= strtotime($referenceDate) && ($end_date == '0000-00-00' || strtotime($referenceDate) <= strtotime($end_date))){
                    	//start_date_dayNum == tomo_date_dayNum then
                    	//month(tomo_date - start_date)%freq == 0 then add
                    	if(date('d', strtotime($date)) == date('d', strtotime($referenceDate))){
                    		$referenceDateArr = explode('-',$referenceDate);
                    		$dateArr = explode('-',$date);
                    		$mnthDiffArr = $this->Reminder->query("SELECT PERIOD_DIFF($referenceDateArr[0].$referenceDateArr[1],$dateArr[0].$dateArr[1]) AS MONTHS_BETWEEN");
                    		if($mnthDiffArr['0']['0']['MONTHS_BETWEEN']%$month == '0'){
                    			$this->data['Remcron']['message'] = $message;
								$this->data['Remcron']['del_time'] = $time;
								$this->data['Remcron']['reminder_id'] = $id;
								$this->data['Remcron']['user_id'] = $user_id;
	                    		for($d=0;$d<$Reminder_for_arr_cnt;$d++){                    	
									$this->data['Remcron']['mobile'] = $Reminder_for_arr[$d];
									$this->Remcron->create();
									$this->Remcron->save($this->data);
		                    	}		                    	
								$this->Reminder->query("UPDATE reminders set first_timer_flag= 1 where id = ".$id);	                    		
                    		}                    	
																	
                    	}
                    	continue;
                    }	
                    
                    //yearly
                    if($day == ''  && $week == '' && $month == '' && $year != '' && $end_date != '0000-00-00' && strtotime($referenceDate) > strtotime($end_date)){
                    	$this->Reminder->query("UPDATE reminders set status = 1 where id = ".$id);
                    	continue;
                    }
                    if($day == ''  && $week == '' && $month == '' && $year != '' &&  strtotime($date) <= strtotime($referenceDate) && ($end_date == '0000-00-00' || strtotime($referenceDate) <= strtotime($end_date))){
                    	//start_date_dayNum == tomo_date_dayNum && start_date_mnth == tomo_date_mnth then
                    	//years(tomo_date - start_date)%freq == 0 then add
                    	if(date('d', strtotime($date)) == date('d', strtotime($referenceDate)) && date('m', strtotime($date)) == date('m', strtotime($referenceDate))){
                    		$referenceDateArr = explode('-',$referenceDate);
                    		$dateArr = explode('-',$date);
                    		$yrDiffArr = $this->Reminder->query("SELECT PERIOD_DIFF($referenceDateArr[0],$dateArr[0]) AS YRS_BETWEEN");
                    		if($yrDiffArr['0']['0']['YRS_BETWEEN']%$year == '0'){
                    			$this->data['Remcron']['message'] = $message;
								$this->data['Remcron']['del_time'] = $time;
								$this->data['Remcron']['reminder_id'] = $id;
								$this->data['Remcron']['user_id'] = $user_id;
	                    		for($e=0;$e<$Reminder_for_arr_cnt;$e++){                    	
									$this->data['Remcron']['mobile'] = $Reminder_for_arr[$e];
									$this->Remcron->create();
									$this->Remcron->save($this->data);
		                    	}								
								$this->Reminder->query("UPDATE reminders set first_timer_flag= 1 where id = ".$id);	                    		
                    		}                    	
							continue;											
                    	}                   	
					}
		}
		$this->releaseLock();
		$this->autoRender = false;      	
	}
	
function createRem(){
		$appRemMsg = addslashes(urldecode($_POST['appRemMsg']));
		$appRemForTmp = $_POST['appRemFor'];
		
		$appRemForArr = explode(",",$appRemForTmp);
		$appRemForFinalArr = array();
		//trim the array and make it unique		
		$recCnt = count($appRemForArr);
		$appRemForFinalArr = array();
		for ($i=0;$i<$recCnt;$i++) {
			if (trim($appRemForArr[$i]) != "") {
				array_push($appRemForFinalArr,$appRemForArr[$i]);
			}
		}
		$appRemForFinalArr = array_unique($appRemForFinalArr);
		$appRemFor = implode(",",$appRemForFinalArr);
		
		$remindWhen = $_POST['remindWhen'];
		
		if($remindWhen == 'l'){
			$appRemDate = $_POST['appRemDate'];		
			$appRemTime = $_POST['appRemTime'];
		}else{
			$appRemDate = date('d-m-Y');		
			$appRemTime = date('H:i');
		}
		
		$appRemRepeatChk = $_POST['appRemRepeatChk'];
		$appRemRepeatBy = $_POST['appRemRepeatBy'];
		$appRemRepeatFreq = $_POST['appRemRepeatFreq'];
		$appRemWeekdayStr = $_POST['appRemWeekdayStr'];
		$appRemEndRadio = $_POST['appRemEndRadio'];
		$appRemEndDate = $_POST['appRemEndDate'];
		
		$this->data['Reminder']['user_id'] = $this->Session->read('Auth.User.id');
		$this->data['Reminder']['reminder_for'] = $appRemFor;
		$this->data['Reminder']['message'] = $appRemMsg;
		if($appRemRepeatBy == '1')
			$this->data['Reminder']['day'] = $appRemRepeatFreq;
		else
			$this->data['Reminder']['day'] = NULL;
		
		if($appRemRepeatBy == '2')
			$this->data['Reminder']['week'] = $appRemWeekdayStr."/".$appRemRepeatFreq;
		else	
			$this->data['Reminder']['week'] = NULL;
			
		if($appRemRepeatBy == '3')	
			$this->data['Reminder']['month'] = $appRemRepeatFreq;
		else	
			$this->data['Reminder']['month'] = NULL;
			
		if($appRemRepeatBy == '4')	
			$this->data['Reminder']['year'] = $appRemRepeatFreq;
		else	
			$this->data['Reminder']['year'] = NULL;
				
		$appRemDateArr = explode('-',$appRemDate);							
		$this->data['Reminder']['date'] = $appRemDateArr[2]."-".$appRemDateArr[1]."-".$appRemDateArr[0];
		$this->data['Reminder']['time'] = $appRemTime;
		
		$appRemEndDateArr = explode('-',$appRemEndDate);							
		$this->data['Reminder']['end_date'] = $appRemEndDateArr[2]."-".$appRemEndDateArr[1]."-".$appRemEndDateArr[0];
		$this->data['Reminder']['created'] = date('Y-m-d H:i:s');
		$this->data['Reminder']['modified'] = date('Y-m-d H:i:s');
		
		if($this->data['Reminder']['date'] == date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 35 minutes')) && $this->data['Reminder']['day'] == NULL && $this->data['Reminder']['week'] == NULL && $this->data['Reminder']['month'] == NULL && $this->data['Reminder']['year'] == NULL){
			$this->data['Reminder']['status'] = 1; ///one time for today
		}	
		//echo $appRemRepeatChk."==".$appRemRepeatBy."==".$appRemRepeatFreq; exit;
		//summary msg
			$summary = '';
			$until = '';
			if($appRemRepeatChk == ''){
				// no repeat
				$summary = 'Message is set for '.$this->General->dateTimeFormat($this->data['Reminder']['date']." ".$this->data['Reminder']['time'].":00");	
			}else{
				//repeat				
				if($appRemEndDate != '')
				$until = ", until ".$this->General->dateFormat($this->data['Reminder']['end_date']);
				
				
				if($appRemRepeatFreq == '1'){
					if($appRemRepeatBy == '1')
						$summary .= 'Daily';
					if($appRemRepeatBy == '2'){
						$weekArr = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
						$weekdayStr = '';
						$appRemWeekdayStrArr = explode(',',$appRemWeekdayStr);
						foreach($appRemWeekdayStrArr as $k=>$v)
						$appRemWeekdayStrDayArr[] = $weekArr[$v];
						
						$summary .= 'Weekly on '.implode(', ',$appRemWeekdayStrDayArr);
					}
					if($appRemRepeatBy == '3')	
						$summary .= 'Monthly on '.date('jS',strtotime($this->data['Reminder']['date']));
					if($appRemRepeatBy == '4')	
						$summary .= 'Yearly on '.date('jS M',strtotime($this->data['Reminder']['date']));
					
				}else{
					
					if($appRemRepeatBy == '1')
						$summary .= 'Every '.$appRemRepeatFreq.' days';
					if($appRemRepeatBy == '2'){
						$weekArr = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
						$weekdayStr = '';
						$appRemWeekdayStrArr = explode(',',$appRemWeekdayStr);
						foreach($appRemWeekdayStrArr as $k=>$v)
						$appRemWeekdayStrDayArr[] = $weekArr[$v];
						
						$summary .= 'Every '.$appRemRepeatFreq.' weeks on '.implode(', ',$appRemWeekdayStrDayArr);
					}
					if($appRemRepeatBy == '3')	
						$summary .= 'Every '.$appRemRepeatFreq.' months on '.date('jS',strtotime($this->data['Reminder']['date']));
					if($appRemRepeatBy == '4')	
						$summary .= 'Every '.$appRemRepeatFreq.' years on '.date('jS M',strtotime($this->data['Reminder']['date']));
					
				}
				
			}
			$this->data['Reminder']['first_timer_flag'] = 0;		
			$this->data['Reminder']['sentNow'] = 0;
			if($remindWhen == 'n'){
				$this->data['Reminder']['sentNow'] = 1;
			}
		$this->Reminder->create();
		if ($this->Reminder->save($this->data)){
			$mail_body = $this->Session->read('Auth.User.mobile') . " has created a reminder";
			//$this->General->mailToAdmins("Personal Alert Subscribed: Bhulakkad", $mail_body);
			
			
			//cut the amt here
			$noOfRecievers = count($appRemForFinalArr);
			
			$msgLen  = strlen($_POST['appRemMsg']);
			$x = ceil($msgLen/DEFAULT_MESSAGE_LENGTH);
			$con = $x*DEFAULT_MESSAGE_LENGTH - APP_REM_MSG_FIXED;
			if($msgLen > $con)
				$remCharge = ($x + 1)*EACH_MESSAGE_COST;
			else
				$remCharge = $x*EACH_MESSAGE_COST;
				
			$remCharge = $remCharge/100;
			$remCharge = $noOfRecievers*$remCharge;
						
			$AppCoreData = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
			///entry in transaction
			$this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$remCharge,$AppCoreData['SMSApp']['id'],null,$this->Session->read('Auth.User.id'));
			///update user balance
			$usr_bal = $this->General->balanceUpdate($remCharge,'subtract',$this->Session->read('Auth.User.id'));
			//insert into app user
			$this->General->appUsersUpdate($AppCoreData['SMSApp']['id'],$this->Session->read('Auth.User.id'));
						
			//echo $this->data['Reminder']['date']."===".date('Y-m-d H:i:s')."===".date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' - 32 minutes'));
			$remId = $this->Reminder->id;
			
			
			$nwVar = 1;
			/// TRAI changes
			if(!DND_FLAG || TRANS_FLAG){
				if($remindWhen == 'n'){
						$this->createRemCronEntry($remId,date('Y-m-d'));
						$this->sendRemNw($remId);
						$nwVar = 2;
				}else if($this->data['Reminder']['date'] == date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 35 minutes'))){				
						$this->createRemCronEntry($remId,$this->data['Reminder']['date']);												
				}
			}
			else {
				if($remindWhen == 'n'){
						$this->createRemCronEntry($remId,date('Y-m-d'));
						$this->sendRemNw($remId);
						$nwVar = 2;
				}else if($this->data['Reminder']['date'] == date('Y-m-d')){				
						$this->createRemCronEntry($remId,$this->data['Reminder']['date']);												
				}else if($this->data['Reminder']['date'] == date('Y-m-d',mktime(0, 0, 0, date("m"), date("d")+1, date("Y"))) && intval(date('His')) > 201500){				
						$this->createRemCronEntry($remId,$this->data['Reminder']['date']);												
				}
			}
			//ends
			
			//$usr_bal = $this->General->getBalance($this->Session->read('Auth.User.id'));
			
			if($nwVar == 2)
			$msg = '2^'.$usr_bal.'^<div style="padding-bottom:10px;"><div class="appColLeftBox">Message sent successfully!!</div></div>';
			else
			$msg = '1^'.$usr_bal.'^<div style="padding-bottom:10px;"><div class="appColLeftBox">Message set successfully!!<div>'.$summary.''.$until.'</div></div></div>';
			/*
			$frndNameArr = $this->Reminder->query('select friendlists.nickname from friendlists where friendlists.mobile = "'.$appRemFor.'" && friendlists.user_id = '.$this->Session->read('Auth.User.id'));		
			if(count($frndNameArr) > 0){
				// show success msg
				$msg = '1^'.$usr_bal.'^<div style="padding-bottom:10px;"><div class="appColLeftBox">Reminder created successfully!!<div>'.$summary.''.$until.'</div></div></div>';
			}else{
				// does not exist
				if($appRemFor == $this->Session->read('Auth.User.mobile')){
					$msg = '1^'.$usr_bal.'^<div style="padding-bottom:10px;"><div class="appColLeftBox">Reminder created successfully!!<div>'.$summary.''.$until.'</div></div></div>';
				}else{
				$fnVar = "appRemQikAddFriend('nickName','friendMobile');";
				$skip = "hideNRemove('afterReminderAddDiv',2.0,0)";
				$msg = '0^'.$usr_bal.'^<div style="padding-bottom:10px;">
						<div class="appColLeftBox">Reminder created successfully!!
						<div>Summary: '.$summary.''.$until.'</div>
						<div>You can save Mobile No. '.$appRemFor.' in your contact list for future use.</div>
						<fieldset>
		                <div class="field" style="padding-top: 10px;">
		                    <div class="fieldDetail">
		                         <div class="fieldLabel2 leftFloat"><label for="nickName">Person Name</label></div>
		                         <div class="fieldLabelSpace2">
		                         	<input type="text" style="width: 255px;" tabindex="1" name="data[Friendlist][nickname]" id="nickName" class="">
		                         	<span style="display: none;" id="nickNameErr"></span>
		                         </div>                     
		                 	</div>
		            	</div>
		            	
						<input type="hidden" value="'.$appRemFor.'" name="data[Friendlist][mobile]" id="friendMobile">
						<span id="friendMobileErr" style="display:none"></span>
									
		            	 	<div class="field" style="padding-top: 10px;">
		                    <div class="fieldDetail">
		                         <div class="fieldLabel2 leftFloat">&nbsp;</div>
		                         <div class="fieldLabelSpace2" id="sendButt">
		                         	 <input type="image" onclick="'.$fnVar.'" class="otherSprite oSPos7 leftFloat" style="margin-right:10px" src="/img/spacer.gif" tabindex="6"><a href="javascript:void(0);" onclick="'.$skip.'">No Thanks!</a>
		                                   </div>                     
		                 	</div>
		            	</div>
		            	<div class="clearLeft"></div>
			            </fieldset>			
						</div></div>';
				}
			}
			*/
			echo $msg;					
		}		
		$this->autoRender = false;					
	}
	
	function frndList() {
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		
		$this->Grouplist->recursive = -1;
		$groupList = $this->Grouplist->find('all',array('conditions' => array('Grouplist.user_id' => $this->Session->read('Auth.User.id')), 'order'=>'name'));
		$this->set('groupList',$groupList);
		$friendList = $this->Friendlist->find('all',array(
						'conditions' => array('Friendlist.user_id' => $this->Session->read('Auth.User.id')),
						'order'=>'Friendlist.nickname asc'
		));
		$this->set('friendList',$friendList);
		$this->render('/elements/appRemFrndList');
	}
	
	///grps
	function grpList(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
					
		$groupList = $this->Grouplist->find('all',array('conditions' => array('Grouplist.user_id' => $this->Session->read('Auth.User.id')), 'order'=>'name'));
		//$groupList = $this->Grouplist->query('select Grouplist.*,Friendlist.* from grouplists as Grouplist, grouplists_friendlists as G_F, friendlists as Friendlist where Grouplist.user_id = '.$this->Session->read('Auth.User.id').' and Grouplist.id = G_F.grouplists_id and G_F.friendlists_id = Friendlist.id');
		//add join to get contacts
		$this->set('groupList',$groupList);
		$this->render('/elements/grpList');
	}

	function afterConAdd(){
		$grpId = $_REQUEST['grpId'];
		$groupList = $this->Grouplist->find('all',array('conditions' => array('Grouplist.id' => $grpId)));
		$this->set('groupList',$groupList);
		$this->set('afteradd',1);
		$this->render('/elements/afterConAdd','ajax');
	}
		
	function afterGrpAdd(){
		$flId = $_REQUEST['flId'];
		$friendList = $this->Friendlist->find('all',array(
						'conditions' => array('Friendlist.id' => $flId)
		));
		$this->set('friendList',$friendList);
		$this->set('afteradd',1);
		$this->render('/elements/afterGrpAdd','ajax');
	}
	
	function addConToGrp(){
		$conStr = $_REQUEST['conStr'];
		$grpId = $_REQUEST['grpId'];
		
		$conStrArr = explode(',',$conStr);		
		foreach($conStrArr as $CSA){
			$this->Grouplist->query('insert into grouplists_friendlists (grouplists_id,friendlists_id) values ('.$grpId.','.$CSA.')');
		}
		echo '1';
		$this->autoRender = false;
	}
	
	function addGrpToCon(){
		$grpStr = $_REQUEST['grpStr'];
		$flId = $_REQUEST['flId'];
		
		$grpStrArr = explode(',',$grpStr);		
		foreach($grpStrArr as $GSA){
			$this->Grouplist->query('insert into grouplists_friendlists (grouplists_id,friendlists_id) values ('.$GSA.','.$flId.')');
		}
		echo '1';
		$this->autoRender = false;
	}
	
	function nonGrpMem(){
		$grpId = $_REQUEST['grpId'];
		$groupL = $this->Grouplist->query('select friendlists.id,friendlists.nickname,friendlists.mobile,grouplists_friendlists.id from friendlists left join grouplists_friendlists on (friendlists.id = grouplists_friendlists.friendlists_id  and grouplists_friendlists.grouplists_id='.$grpId.') where  friendlists.user_id ='.$this->Session->read('Auth.User.id').' order by friendlists.nickname');
		$groupList = array();
		$i = 0;
		foreach($groupL as $gl){
			if(is_null($gl['grouplists_friendlists']['id'])){
				$groupList[$i] = $gl;
				$i++;
			}	
		}
		
		$this->set('groupList',$groupList);
		$this->set('conCount',count($groupL));
		$this->set('groupId',$grpId);
		$this->render('/reminders/nonGroupMem','ajax');
	}
	
	function nonConGrp(){
		$flId = $_REQUEST['flId'];
		$groupL = $this->Grouplist->query('select grouplists.id,grouplists.name,grouplists_friendlists.id from grouplists left join grouplists_friendlists on (grouplists.id = grouplists_friendlists.grouplists_id  and grouplists_friendlists.friendlists_id='.$flId.') where  grouplists.user_id ='.$this->Session->read('Auth.User.id').' order by grouplists.name');
		$groupList = array();
		$i = 0;
		foreach($groupL as $gl){
			if(is_null($gl['grouplists_friendlists']['id'])){
				$groupList[$i] = $gl;
				$i++;
			}	
		}
		
		$this->set('groupList',$groupList);
		$this->set('grpCount',count($groupL));
		$this->set('flId',$flId);
		$this->render('/reminders/nonConGrp','ajax');
	}
	
	function addGrpList(){		
		$this->data['Grouplist']['name'] = ucfirst($_REQUEST['groupname']);
		$this->data['Grouplist']['user_id'] = $_SESSION['Auth']['User']['id'];
		//already exists check
		$groupList = $this->Grouplist->find('all',array('conditions' => array('Grouplist.name' =>$_REQUEST['groupname'],'Grouplist.user_id' => $this->Session->read('Auth.User.id'))));
		if($groupList['0']['Grouplist']['name'] != ''){
			echo 'a';
		}else{
			$this->Grouplist->create();
			if($this->Grouplist->save($this->data)){
				echo $this->Grouplist->id;
			}else{
				echo 'e';
			}	
		}	
		$this->autoRender = false;					
	}
	
	function addGrpListNFetch(){		
		$this->data['Grouplist']['name'] = $_REQUEST['groupname'];
		$this->data['Grouplist']['user_id'] = $_SESSION['Auth']['User']['id'];
		//already exists check
		$groupList = $this->Grouplist->find('all',array('conditions' => array('Grouplist.name' =>$_REQUEST['groupname'],'Grouplist.user_id' => $this->Session->read('Auth.User.id'))));
		if($groupList['0']['Grouplist']['name'] != ''){
			echo 'a';
		}else{
			$this->Grouplist->create();
			if($this->Grouplist->save($this->data)){
				$groupListNew = $this->Grouplist->find('all',array('conditions' => array('Grouplist.user_id' => $this->Session->read('Auth.User.id'))));
				$fnVar = 'deselectMultiple($("groupList"));';
				$grpList = '<select id="groupList" multiple size="5" style="width:165px">';
				foreach($groupListNew as $gl){
					$grpList .= "<option value='".$gl['Grouplist']['id']."'>".$gl['Grouplist']['name']."</option>";	                         	
				}
				$grpList .= "</select><a href='javascript:void(0);' onclick='".$fnVar."'>Deselect</a>";
				echo $grpList;
			}else{
				echo 'e';
			}	
		}	
		$this->autoRender = false;					
	}
	
	
	
	//grps ends
	function addFriend(){

		$this->data['Friendlist']['nickname'] = $_REQUEST['nickname'];
		$this->data['Friendlist']['mobile'] = $_REQUEST['mobile'];
		$group = $_REQUEST['group'];
		$this->data['Friendlist']['user_id'] = $_SESSION['Auth']['User']['id'];

		$this->Friendlist->create();
		if($this->Friendlist->save($this->data)){
			$id = $this->Friendlist->id;
			///add to the group
			$groupStrArr = explode(',',$group);		
			foreach($groupStrArr as $GSA){
				$groupList = $this->Grouplist->query('insert into grouplists_friendlists (grouplists_id,friendlists_id) values ('.$GSA.','.$id.')');
			}					
			echo $id;
		}
		else{
			//echo "<script>alert('Either nickname or mobile number is duplicate');</script>";
			echo 'a';
		}
		$this->autoRender = false;
	}
	
	function deleteFriend(){
		$id = $_POST['id'];
		if($this->Friendlist->delete($id)){
			$this->Friendlist->query("delete from grouplists_friendlists where friendlists_id =".$id);
			$data = $this->Friendlist->find('first', array('fields' => array('Friendlist.id'), 'conditions' => array('Friendlist.user_id' => $_SESSION['Auth']['User']['id'])));			
			if($data['Friendlist']['id'] == ''){
				echo '0';
			}else{ 
				echo '1';
			}
		}else{
			echo 'e';
		}
		
		
		$this->autoRender = false;
	
	}
	
	function getRemIds(){
		$idArr = array();
		$this->Reminder->recursive = -1;
		$remiderIds = $this->Reminder->query('select id from reminders where status=0 and user_id ='.$this->Session->read('Auth.User.id'));
		foreach($remiderIds as $ids){							
				array_push($idArr,$ids['reminders']['id']);					
		}
		$remiderIds1 = $this->Reminder->query('select distinct reminder_id from remcrons where user_id ='.$this->Session->read('Auth.User.id'));
		foreach($remiderIds1 as $ids1){							
				array_push($idArr,$ids1['remcrons']['reminder_id']);					
		}
		return $idArr;
	}
	
	function remAppPaginate(){
		$id = $_POST['id'];
		$type = $_POST['type'];
		if($type == 'U'){
			$remId = $this->getRemIds();
			$this->Reminder->recursive = -1;
			$upcomingRem = $this->Reminder->find('all', array(
				'fields' => array('Reminder.*'),			
				'conditions' => array('Reminder.id' => $remId, 'Reminder.id < ' => $id),
				'limit' => APP_REM_REC,
				'order' => 'Reminder.id desc'
			));
			
			
			$j = 0;
			foreach($upcomingRem as $data){
				$finalRecArr = array();			
				$recStr = $data['Reminder']['reminder_for'];
				$recArr = explode(",",$recStr);
				$recQuery = $this->Friendlist->find('all', array(
					'fields' => array('Friendlist.mobile','Friendlist.nickname'),			
					'conditions'=> array('Friendlist.mobile in ('.$recStr.') && Friendlist.user_id = '.$this->Session->read('Auth.User.id'))				
				));
									
				foreach($recQuery as $h){				
					$recStr = str_replace($h['Friendlist']['mobile'],$h['Friendlist']['mobile'].':'.$h['Friendlist']['nickname'],$recStr);				
				}
				$finalRecArr = explode(",",$recStr);
				$finalFinRecArr = array();
				foreach($finalRecArr as $o){
					$expl = explode(":",$o);
					if($expl[1] != ''){
						$finalFinRecArr[$expl[0]] = $expl[1];
					}else{
						$finalFinRecArr[$expl[0]] = $expl[0];
					}
				}
				
				$upcomingRem[$j]['Reminder']['reminder_for'] = $finalFinRecArr;
				$j++; 
			}
			
			
			
			$this->set('upcomingRem',$upcomingRem);
			$totalUpcomingRem = $this->totalUpcomingRem();
			$this->set('totalUpcomingRem',$totalUpcomingRem);
			$this->render('/elements/upcomingRem');
		}
		
		if($type == 'A'){
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		
		$this->AppsLog->recursive = -1;
		$archivedRem = $this->AppsLog->find('all',array(
		'fields' => array('AppsLog.*','group_concat(AppsLog.mobile) as rec'),
		'conditions' => array('AppsLog.user_id' => $this->Session->read('Auth.User.id'),'AppsLog.app_id' => $data['SMSApp']['id'], 'AppsLog.id < ' => $id),
		'limit' => APP_REM_REC,
		'order' => 'AppsLog.id desc',
		'group' => 'AppsLog.ref_id '
		));
				
		$j = 0;
		foreach($archivedRem as $data){
			$finalRecArr = array();			
			$recStr = $data['0']['rec'];
			$recArr = explode(",",$recStr);
			$recQuery = $this->Friendlist->find('all', array(
				'fields' => array('Friendlist.mobile','Friendlist.nickname'),			
				'conditions'=> array('Friendlist.mobile in ('.$recStr.') && Friendlist.user_id = '.$this->Session->read('Auth.User.id'))				
			));
			
			
			
			foreach($recQuery as $h){				
				$recStr = str_replace($h['Friendlist']['mobile'],$h['Friendlist']['mobile'].':'.$h['Friendlist']['nickname'],$recStr);				
			}
			$finalRecArr = explode(",",$recStr);
			$finalFinRecArr = array();
			foreach($finalRecArr as $o){
				$expl = explode(":",$o);
				if($expl[1] != ''){
					$finalFinRecArr[$expl[0]] = $expl[1];
				}else{
					$finalFinRecArr[$expl[0]] = $expl[0];
				}
			}
			
			$archivedRem[$j]['0']['rec'] = $finalFinRecArr;
			$j++; 
		}
		
		
		$this->set('archivedRem',$archivedRem);		
		$this->render('/elements/archivedRem');
		}
	}
	
	function setReminder(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		//USER GROUPS
 		
 		$this->Friendlist->recursive = -1;
 		$friendList = $this->Friendlist->find('all',array('conditions' => array('Friendlist.user_id' => $this->Session->read('Auth.User.id')), 'order' => 'Friendlist.nickname asc'));
 		$flArr = array();
 		foreach($friendList as $fl){
			$flArr[$fl['Friendlist']['nickname']] = $fl['Friendlist']['mobile']; 	 			
 		}
 		
 		$groupList = $this->Grouplist->find('all',array(
 			'conditions' => array('Grouplist.user_id' => $this->Session->read('Auth.User.id')),
 			'order' => 'Grouplist.name asc'
 		));
		$glArr = array();
		if($flArr)
		$glArr['All contacts'] = $flArr;
 		foreach($groupList as $gl){
 			if(count($gl['Friendlist']) < 1)continue;
 			$grpCon = array();
 			foreach($gl['Friendlist'] as $gc){
 				$grpCon[$gc['nickname']] = $gc['mobile']; 
 			}
			$glArr[$gl['Grouplist']['name']] = $grpCon; 	 			
 		}
 		/*echo "<pre>";
 		//print_r($flArr);
 		print_r($glArr);
 		echo "</pre>";
 		*/
 		
		$this->set('contactArr',$glArr);
		
		//upcoming reminders
		$upcomingRem = $this->upcomingRem();
		$this->set('upcomingRem',$upcomingRem);
		$totalUpcomingRem = $this->totalUpcomingRem();
		$this->set('totalUpcomingRem',$totalUpcomingRem);
		
		//Reminders already set
		$archivedRem = $this->archievedRem($data);				
		$this->set('archivedRem',$archivedRem);
		$totalArchivedRem = $this->totalArchivedRem($data);
		$this->set('totalArchivedRem',$totalArchivedRem);
		
		$this->render('set_reminder');
	}
	
	function archievedRem($data){
		$this->AppsLog->recursive = -1;		
		$archivedRem = $this->AppsLog->find('all',array(
		'fields' => array('AppsLog.*','group_concat(AppsLog.mobile) as rec'),
		'conditions' => array('AppsLog.user_id' => $this->Session->read('Auth.User.id'),'AppsLog.app_id' => $data['SMSApp']['id']),
		'limit' => APP_REM_REC,
		'order' => 'AppsLog.id desc',
		'group' => 'AppsLog.ref_id, Date(timestamp)'
		));
		
		$j = 0;
		foreach($archivedRem as $data){
			$finalRecArr = array();			
			$recStr = $data['0']['rec'];
			$recArr = explode(",",$recStr);
			$recQuery = $this->Friendlist->find('all', array(
				'fields' => array('Friendlist.mobile','Friendlist.nickname'),			
				'conditions'=> array('Friendlist.mobile in ('.$recStr.') && Friendlist.user_id = '.$this->Session->read('Auth.User.id'))				
			));
			
			
			
			foreach($recQuery as $h){				
				$recStr = str_replace($h['Friendlist']['mobile'],$h['Friendlist']['mobile'].':'.$h['Friendlist']['nickname'],$recStr);				
			}
			$finalRecArr = explode(",",$recStr);
			$finalFinRecArr = array();
			foreach($finalRecArr as $o){
				$expl = explode(":",$o);
				if($expl[1] != ''){
					$finalFinRecArr[$expl[0]] = $expl[1];
				}else{
					$finalFinRecArr[$expl[0]] = $expl[0];
				}
			}
			
			$archivedRem[$j]['0']['rec'] = $finalFinRecArr;
			$j++; 
		}
		return $archivedRem;
	}
	
	function upcomingRem(){
		
		$this->Reminder->recursive = -1;
		$remId = $this->getRemIds();
		$upcomingRem = $this->Reminder->find('all', array(
			'fields' => array('Reminder.*'),			
			'conditions' => array('Reminder.id' => $remId),
			'limit' => APP_REM_REC,
			'order' => 'Reminder.id desc'
		));
		
		$j = 0;
		foreach($upcomingRem as $data){
			$finalRecArr = array();			
			$recStr = $data['Reminder']['reminder_for'];
			$recArr = explode(",",$recStr);
			$recQuery = $this->Friendlist->find('all', array(
				'fields' => array('Friendlist.mobile','Friendlist.nickname'),			
				'conditions'=> array('Friendlist.mobile in ('.$recStr.') && Friendlist.user_id = '.$this->Session->read('Auth.User.id'))		
			));
									
			foreach($recQuery as $h){				
				$recStr = str_replace($h['Friendlist']['mobile'],$h['Friendlist']['mobile'].':'.$h['Friendlist']['nickname'],$recStr);				
			}
			$finalRecArr = explode(",",$recStr);
			$finalFinRecArr = array();
			foreach($finalRecArr as $o){
				$expl = explode(":",$o);
				if($expl[1] != ''){
					$finalFinRecArr[$expl[0]] = $expl[1];
				}else{
					$finalFinRecArr[$expl[0]] = $expl[0];
				}
			}
			
			$upcomingRem[$j]['Reminder']['reminder_for'] = $finalFinRecArr;
			$j++; 
		}
		return $upcomingRem;
	}
	
	function totalUpcomingRem(){
		$remId = $this->getRemIds();
		$this->Reminder->recursive = -1;
		$upcomingRem = $this->Reminder->find('all', array(
			'fields' => array('Reminder.id'),			
			'conditions' => array('Reminder.id' => $remId)
		));
		return count($upcomingRem);
	}
	
	function totalArchivedRem($data){		
		$this->Reminder->recursive = -1;
		$totalArchivedRem = $this->AppsLog->find('all',array(
		'fields' => array('AppsLog.id'),
		'conditions' => array('AppsLog.user_id' => $this->Session->read('Auth.User.id'),'AppsLog.app_id' => $data['SMSApp']['id']),
		'group' => 'AppsLog.ref_id'
		));
		return count($totalArchivedRem);
	}
	
	function upcomingRemUpdate(){		
		$upcomingRem = $this->upcomingRem();
		$totalUpcomingRem = $this->totalUpcomingRem();
		$this->set('upcomingRem',$upcomingRem);
		$this->set('totalUpcomingRem',$totalUpcomingRem);
		$this->render('/elements/upcomingRem');
	}
	
	function archivedRemUpdate(){
		$this->AppsLog->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$archivedRem = $this->archievedRem($data);
		$this->set('archivedRem',$archivedRem);
		$totalArchivedRem = $this->totalArchivedRem($data);
		$this->set('totalArchivedRem',$totalArchivedRem);
		$this->render('/elements/archivedRem');
	}
	
	function validateRem(){
		$appRemMsg = $_POST['appRemMsg'];
		$appRemFor = $_POST['appRemFor'];
		$appRemForArr = explode(",",$appRemFor);
		$appRemForFinalArr = array();
		//trim the array and make it unique		
		$recCnt = count($appRemForArr);
		$appRemForFinalArr = array();
		for ($i=0;$i<$recCnt;$i++) {
			if (trim($appRemForArr[$i]) != "") {
				array_push($appRemForFinalArr,$appRemForArr[$i]);
			}
		}
		$appRemForFinalArr = array_unique($appRemForFinalArr);
		$receivers = count($appRemForFinalArr);
				
		$appRemDate = $_POST['appRemDate'];		
		$appRemTime = $_POST['appRemTime'];
		
		$appRemRepeatChk = $_POST['appRemRepeatChk'];
		$appRemRepeatBy = $_POST['appRemRepeatBy'];
		$appRemRepeatFreq = $_POST['appRemRepeatFreq'];
		$appRemWeekdayStr = $_POST['appRemWeekdayStr'];
		$appRemEndRadio = $_POST['appRemEndRadio'];
		$appRemEndDate = $_POST['appRemEndDate'];
		$remindWhen = $_POST['remindWhen'];
				
		$err = ''; 		
		
		$msgLen  = strlen($appRemMsg);
		$x = ceil($msgLen/DEFAULT_MESSAGE_LENGTH);
		$con = $x*DEFAULT_MESSAGE_LENGTH - APP_REM_MSG_FIXED;
		if($msgLen > $con)
			$remCharge = ($x + 1)*EACH_MESSAGE_COST;
		else
			$remCharge = $x*EACH_MESSAGE_COST;
			
		$remCharge = $remCharge/100;
			
		$usr_bal = $this->General->getBalance($this->Session->read('Auth.User.id'));			
		if($usr_bal >= $receivers*$remCharge){
			if($remindWhen == 'l'){			
				$appRemDateArr = explode('-',$appRemDate);
				//$appRemDateStr = $appRemDateArr[1].", ".$appRemDateArr[0].", ".$appRemDateArr[2];
				if(!checkdate($appRemDateArr[1],$appRemDateArr[0],$appRemDateArr[2])){
					$err = 'Invalid reminder date';
				}elseif(strtotime($appRemDate)<strtotime(date('Y-m-d'))){
					$err = 'Please select future date';
				}elseif(!$this->General->checkTimeSlot(str_replace(':','',$appRemTime)) && !TRANS_FLAG){// TRAI changes
					$err = TIME_SLOT_ERR_REM;
				}elseif(strtotime($appRemDate." ".$appRemTime) < strtotime("30 minutes")){ //ends
					if(intval(date('Hi')) > (TIME_SLOT_END-1000) && DND_FLAG && !TRANS_FLAG){// TRAI changes
						if($appRemRepeatChk == '1')
						$err = 'You can not set message starting from today. Try future date.';
						else
						$err = 'You can not set message for today. Try future date.';
					}else{//ends
						//$err = 'Please select time more than 30 minutes from now';
						if(date('Y-m-d',strtotime("30 minutes")) == date('Y-m-d')){
							if(intval(date('i',strtotime("30 minutes"))) < 30)
								$err = 'Please select '.date('h:30 A',strtotime("30 minutes")).' or any time after that';
							else
								$err = 'Please select '.date('h:00 A',strtotime("60 minutes")).' or any time after that';
						}else{							
							$err = 'Please select '.date('h:30 A',strtotime("30 minutes")).' or any time after that of '.date('Y-m-d',strtotime("30 minutes"));							
						}
						
					}
				}elseif($appRemRepeatChk == '1'){				
					if($appRemEndRadio == 'until'){
						$appRemEndDateArr = explode('-',$appRemEndDate);
						//$appRemEndDateStr = $appRemEndDateArr[1].", ".$appRemEndDateArr[0].", ".$appRemEndDateArr[2];
					 	if(!checkdate($appRemEndDateArr[1],$appRemEndDateArr[0],$appRemEndDateArr[2])){
							$err = 'Invalid reminder end date';
						}elseif(strtotime($appRemEndDate)<strtotime(date('Y-m-d'))){
							$err = 'Please select future reminder end date';
						}elseif(strtotime($appRemEndDate)<strtotime($appRemDate)){
							$err = 'Reminder end date should not be less than reminder start date';
						}	
					}			
				}
			}
			// TRAI changes
			if($remindWhen == 'n'){
				if(!$this->General->checkTimeSlot() && !TRANS_FLAG){
					$err = TIME_SLOT_ERR_NW;
				}
			}
			//ends
		}else{
			$err = 'Sorry!! You have insufficient balance. To set reminders for '.$receivers.', you must at least have Rs. '.number_format($receivers*$remCharge, 2, '.', '').' in your SMSTadka account. <a href="/users/paynow">Recharge Now</a>';	
		}		
		echo $err;
		$this->autoRender = false;		
	}
	
	function deleteRem(){
		$delRemId = $this->objMd5->decrypt($_POST['delRemId'],encKey);		
		if($this->Reminder->query("update reminders set status = 1 where id =".$delRemId)){
			$this->Remcron->deleteAll(array('Remcron.reminder_id' => $delRemId));		
			echo '1';			
		}else{
			echo '0';
		}
		$this->autoRender = false;
	}
	
	function deleteGrp(){
		$delGrpId = $_POST['delGrpId'];		
		if($this->Reminder->query("delete from grouplists where id =".$delGrpId)){
			$this->Reminder->query("delete from grouplists_friendlists where grouplists_id =".$delGrpId);		
			echo '1';			
		}else{
			echo '0';
		}
		$this->autoRender = false;
	}
	
	function deleteGrpMem(){		
		$grpId = $_POST['grpId'];
		$frndListNo = $_POST['frndListNo'];
		if($this->Reminder->query("delete from grouplists_friendlists where grouplists_id=".$grpId." and friendlists_id =".$frndListNo)){
			echo '1';						
		}else{
			echo '0';
		}
		$this->autoRender = false;
	}
	
	function deleteMemGrp(){		
		$gflId = $_POST['gflId'];
		if($this->Reminder->query("delete from grouplists_friendlists where id=".$gflId)){
			echo '1';						
		}else{
			echo '0';
		}
		$this->autoRender = false;
	}
	
	function deleteRemRec(){		
		$remId = $this->objMd5->decrypt($_POST['remId'],encKey);
		$mobNo = $_POST['mobNo'];

		$getRem = $this->Reminder->find('all',array(
		'fields' => array('Reminder.reminder_for'),
		'conditions' => array('Reminder.id' => $remId)
		));
		
		$remFor = $getRem['0']['Reminder']['reminder_for'];
		$remForArr = explode(',',$remFor);
		$key = array_search($mobNo,$remForArr);
		unset($remForArr[$key]);
		$remForStr = implode(',',$remForArr);

		if($remForStr == '')
		$query = "delete from reminders where id =".$remId;
		else
		$query = "update reminders set reminder_for = '".$remForStr."' where id =".$remId;
		
		if($this->Reminder->query($query)){
			$this->Remcron->deleteAll(array('Remcron.reminder_id' => $remId, 'Remcron.mobile' => $mobNo));
			if($remForStr == '')
			echo '2';
			else
			echo '1';		
						
		}else{
			echo '0';
		}
		$this->autoRender = false;
	}
	
	function deleteArc(){
		$delRemId = $this->objMd5->decrypt($_POST['delArcId'],encKey);		
		if($this->AppsLog->delete($delRemId)){			
			echo '1';			
		}else{
			echo '0';
		}
		$this->autoRender = false;
	}
			
	function contactList(){
		$this->Pnr->recursive = -1;
		$data = $this->Pnr->find('all',array('conditions' => array('user_id' => $this->Session->read('Auth.User.id'))));
		$this->set('data',$data);
		$this->render('my_alerts');
	}

}
?>