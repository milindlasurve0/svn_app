<?php
class GroupsController extends AppController {

	var $name = 'Groups';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $uses = array('Group','User','Package','PackagesUser','CategoriesPackage','Category','Message','Log','Draft','Rss','Pnr','Reminder','Stock','SMSApp','ProductsUser','ProductsPackage','Product','RetailersCoupon','pkg_data_url','pkg_data_short_url','data_fun','cool_video');
	var $components = array('RequestHandler','Email');

	function beforeFilter() {
		if($this->action == 'adminControl' || $this->action == 'getPackages'  || $this->action == 'getPackageInfo'){
				
		}else{
			parent::beforeFilter();
		}
		//$this->Auth->allow('*');
		$this->Auth->allowedActions = array('optoutUsers','makeCycle1','putCoolVideo','updateProdEndDate','updatePkgEndDate','updatePkgEndDateOld','updateExDate','rechargedemo','excelToPhp1','resendMesCron_SMSLane','shootMail','userInfo','changeDNDFlags','mmdownload','allMessages','cleanDatabase','sendFreeCouponCode','freeCouponCode','build_acl','initializePackageCrons','setCronFlags','resendMesCron',
			'twitterCron','missedCallReg','requestBySMS','unsubscribeCron','twitterCronInit',
			'resendMesCron_FREESMSAPI','resendMesCron_24X7SMS_PASS','resendMesCron_24X7SMS','asynchronousCall','commonMessage2','marketingMails','marketMessage','excelToPhp','commonMessage','mailBySMTPAuth','test','test1','makeCycle','resendMesCron_GUPSHUP','fileupload','find_file','filedownload','getCoolVideo');

	}

	function updateExDate(){
		$noOfDays = $_REQUEST['noOfDays'];

		$result = $this->Group->query("select end from products_users where id=$pkgId");
		$newDate=strtotime($noOfDays." day",strtotime($result['0']['products_users']['end']));
		$newDate=date('Y-m-d H:i:s',$newDate);
		$result1 = $this->Group->query("update products_users set end='$newDate' where id=$pkgId");
		echo $newDate;
		$this->autoRender = false;
	}



	function updatePkgEndDate(){
		$loggedInUser= $_SESSION['Auth']['User']['id'];
		$pkgId = $_REQUEST['pkgId'];
		$endDate = $_REQUEST['end'];
		$flag=$_REQUEST['flag'];
		//	$pkgNameForLogs=$_REQUEST['pkgName'];
		$successForLogsCcUpdation=0;

		if($endDate==null)
		{
			echo "Please enter a date before editting";
			exit;

		}

		$today = date("d-m-Y");
			

		$startResult=$this->Group->query("select start,user_id,package_id,end from packages_users where id=$pkgId");//to check start <end
		$startDate=$startResult['0']['packages_users']['start'];
		$userIdForLogs=$startResult['0']['packages_users']['user_id'];
		$oldEndDateForLogs=$startResult['0']['packages_users']['end'];
		$packageIdForLogs=$startResult['0']['packages_users']['package_id'];


		$pkgNameForLogsResult=$this->Group->query("select name from packages where id=$packageIdForLogs");
		$pkgNameForLog=$pkgNameForLogsResult['0']['packages']['name'];
		//$pkgNameForLogs=$pkgNameForLogsResult['0']['packages']['name'];

		$formatEndDate=strtotime($endDate);
		$formatTodayDate=strtotime($today);
		//echo "Formatted End Date is ".$formatEndDate;
		//echo "Formtted todys dte is ".$formatTodayDate;
		//exit;

		$formatStartDate=strtotime($startDate);
		//echo "Formatted End Ddate".$formatEndDate;
		//echo "Formattd Start Date".$formatStartDate;
		$array = explode("-",$endDate);
		//$dd=array[0];
		//$mm=array[1];
		//$yyyy=array[2];

		$newDate=$array[2]."-".$array[1]."-".$array[0];
		//echo $newDate;
		//echo $startDate;

		if($formatEndDate<$formatTodayDate)// changed to $formatTodyDte from $formatStartDate to ensure the edited date is not less than current date.
		{
			echo 'invalid';
			$successForLogsCcUpdation=0;
			exit;
		}
		//echo "Formatted Date".$newDate;
		if($flag == '0')
		$this->Group->query("update packages_users set end='$newDate' where id=$pkgId");
		$this->Group->query("update packages_users set active='1' WHERE id=$pkgId");

		$result1 = $this->Group->query("select end from packages_users where id=$pkgId");
		$packageEndDate=$result1['0']['packages_users']['end'];//nisha

		$tableMsg="End date of Package  : ".$pkgNameForLog." updated from ".$oldEndDateForLogs." to new end date: ".$newDate;

		$msg1="Your package ".$pkgNameForLog." end date has been updated to ".$packageEndDate;

		$resultSendSMSToUser=$this->Group->query("SELECT u.mobile FROM users as u ,packages_users as pu WHERE pu.user_id=$userIdForLogs and pu.user_id=u.id");
		$userMobile=$resultSendSMSToUser['0']['u']['mobile'];
		$this->General->sendMessage('',$userMobile,$msg1);

		$this->User->query("insert into  logs_cc(user_id,description,parent_user_id) values ($userIdForLogs,'$tableMsg',$loggedInUser)");

		echo $packageEndDate;

		$this->autoRender = false;
	}

	function updateProdEndDate(){
		$loggedInUser= $_SESSION['Auth']['User']['id'];
		$prdId = $_REQUEST['prdId'];
		$endDate = $_REQUEST['end'];
		$flag=$_REQUEST['flag'];
		$successForLogsCcUpdation=0;

		if($endDate==null)
		{
			echo "Please enter a date before editting";
			exit;

		}

		$today = date("d-m-Y");
		$startResult=$this->Group->query("select start,user_id,product_id,end from products_users where id=$prdId");//to check start <end
		$startDate=$startResult['0']['products_users']['start'];
		$userIdForLogs=$startResult['0']['products_users']['user_id'];
		$oldEndDateForLogs=$startResult['0']['products_users']['end'];
		$productIdForLogs=$startResult['0']['products_users']['product_id'];

		$prdNameForLogsResult=$this->Group->query("select name from products where id=$productIdForLogs");
		$prdNameForLog=$prdNameForLogsResult['0']['products']['name'];

		$formatEndDate=strtotime($endDate);
		$formatTodayDate=strtotime($today);

		$formatStartDate=strtotime($startDate);
		$array = explode("-",$endDate);


		$newDate=$array[2]."-".$array[1]."-".$array[0];
		if($formatEndDate<$formatTodayDate)// changed to $formatTodyDte from $formatStartDate to ensure the edited date is not less than current date.
		{
			echo 'invalid';
			$successForLogsCcUpdation=0;
			exit;
		}

		if($flag == '0')
		$this->Group->query("update products_users set end='$newDate' where id=$prdId");
		$this->Group->query("update products_users set active='1' WHERE id=$prdId");

		$result1 = $this->Group->query("select end from products_users where id=$prdId");
		$productEndDate=$result1['0']['products_users']['end'];

		$tableMsg="End date of Product  : ".$prdNameForLog." updated from ".$oldEndDateForLogs." to new end date: ".$newDate;
		$this->User->query("insert into  logs_cc(user_id,description,parent_user_id) values ($userIdForLogs,'$tableMsg',$loggedInUser)");
		echo $productEndDate;

		$msg1="Your product ".$prdNameForLog." end date has been updated to ".$newDate;
		$resultSendSMSToUser=$this->Group->query("SELECT u.mobile FROM users as u ,products_users as pu WHERE pu.user_id=$userIdForLogs and pu.user_id=u.id");
		$userMobile=$resultSendSMSToUser['0']['u']['mobile'];
		$this->General->sendMessage('',$userMobile,$msg1);

		$this->autoRender = false;
	}

	function rechargedemo($mobile){
		$msg = 'Welcome to Signal7 demo!
Download links for the sample applications:

Android: http://bit.ly/yiClx0

Symbian: http://bit.ly/wgzEbQ';

		$this->General->sendMessage('',$mobile,$msg,'priority',3);
		$this->autoRender = false;
	}

	function putCoolVideo(){
		$result = $this->Group->query("select * from cool_videos");
		if(count($result) > 0){
			foreach($result as $res){
				$units = array(' B', ' KB', ' MB', ' GB', ' TB');
				for ($i = 0; $res['cool_videos']['size'] >= 1024 && $i < 4; $i++) $res['cool_videos']['size'] /= 1024;

				$data = stripslashes($res['cool_videos']['description'])."<br/>Size: ".round($res['cool_videos']['size'], 2).$units[$i]."<br/>Download from here:<br/>".$res['cool_videos']['short_url'];
				$this->Group->query("update cool_videos set content = '".addslashes($data)."',cyclicData = '' where id=".$res['cool_videos']['id']);
			}
		}

		$this->autoRender = false;
	}

	function getCoolVideo(){
		$res = $this->Group->query("select * from cool_videos where download =0 limit 1");
		if(count($res) > 0){
			$units = array(' B', ' KB', ' MB', ' GB', ' TB');
			for ($i = 0; $res[0]['cool_videos']['size'] >= 1024 && $i < 4; $i++) $res[0]['cool_videos']['size'] /= 1024;
			 

			echo stripslashes($res[0]['cool_videos']['description'])."<br>Size: ".round($res[0]['cool_videos']['size'], 2).$units[$i]."<br>Download from here:<br>".$res[0]['cool_videos']['short_url'];
		}else{
			echo "no videos found";
		}
		$this->Group->query("update cool_videos set download =1 where id=".$res[0]['cool_videos']['id']);
		$this->autoRender = false;
	}

	function fileupload(){
		$error = '';
		if($this->data){
			if(trim($this->data['groups']['desc']) == ''){
				$error = '<div style="background-color:#c0c0c0">Add description</div>';
			}elseif($this->data['groups']['video']['tmp_name'] == '' || strtolower(substr(strrchr($this->data['groups']['video']['name'],"."),1)) != "3gp"){
				$error = '<div style="background-color:#c0c0c0">Select 3gp file to upload</div>';
			}else{
				$this->cool_video->create();
				$this->cool_video->data['cool_video']['reused'] = '0';
				$this->cool_video->data['cool_video']['package_id'] = '152';
				$this->cool_video->data['cool_video']['description'] = addslashes($this->data['groups']['desc']);
				$this->cool_video->data['cool_video']['size'] = addslashes($this->data['groups']['video']['size']);
				$this->cool_video->data['cool_video']['download'] = 0;
				$this->cool_video->data['cool_video']['created'] = date('Y-m-d H:i:s');
				$this->cool_video->data['cool_video']['content'] = '';
				$this->cool_video->data['cool_video']['cyclicData'] = '';
				$this->cool_video->data['cool_video']['cyclicData1'] = '';
				$this->cool_video->save($this->cool_video->data);
				$insert_id = $this->cool_video->id;
				//ends
					
				$enc_insert_id = $this->objMd5->Encrypt($insert_id,encKey);
				$url = $this->General->getBitlyUrl(SITE_NAME.'/groups/filedownload/'.$enc_insert_id.'.3gp');
					
				$this->Group->query("update cool_videos set short_url ='".$url."' where id=".$insert_id);
				move_uploaded_file($this->data['groups']['video']['tmp_name'], "vidupload/".$enc_insert_id.".3gp");
				$error = '<div style="background-color:#c0c0c0">File uploaded successfully</div>';
			}
			$this->set('desc', $this->data['groups']['desc']);
		}else{
			$this->set('desc', '');
		}
		$this->set('error', $error);
	}

	function find_file ($dirname, $fname, &$file_path) {

		$dir = opendir($dirname);

		while ($file = readdir($dir)) {
			if (empty($file_path) && $file != '.' && $file != '..') {
				if (is_dir($dirname.'/'.$file)) {
					$this->find_file($dirname.'/'.$file, $fname, $file_path);
				}
				else {
					if (file_exists($dirname.'/'.$fname)) {
						$file_path = $dirname.'/'.$fname;
						return;
					}
				}
			}
		}

	} // find_file

	function filedownload($file){
		$info = pathinfo($file);
		$pure_file_name =  basename($file,'.'.$info['extension']);

		$dec_insert_id = $this->objMd5->Decrypt($pure_file_name,encKey);
		//tracking of video download
		$this->Group->query("insert into cool_video_stat (cool_video_id,downloaded) values ('".$dec_insert_id."','".date('Y-m-d H:i:s')."')");

		$res = $this->Group->query("select * from cool_videos where download =1 and id=".$dec_insert_id);
		if(count($res)<1)exit;


		define('ALLOWED_REFERRER', '');

		//define('LOG_DOWNLOADS',true);
		//define('LOG_FILE','downloads.log');

		$allowed_ext = array (
		// archives
		  'zip' => 'application/zip',		
		// documents
		  'pdf' => 'application/pdf',
		  'doc' => 'application/msword',
		  'xls' => 'application/vnd.ms-excel',
		  'ppt' => 'application/vnd.ms-powerpoint',		  
		// executables
		  'exe' => 'application/octet-stream',		
		// images
		  'gif' => 'image/gif',
		  'png' => 'image/png',
		  'jpg' => 'image/jpeg',
		  'jpeg' => 'image/jpeg',		
		// audio
		  'mp3' => 'audio/mpeg',
		  'wav' => 'audio/x-wav',		
		// video
		  'mpeg' => 'video/mpeg',
		  'mpg' => 'video/mpeg',
		  'mpe' => 'video/mpeg',
		  'mov' => 'video/quicktime',
		  'avi' => 'video/x-msvideo',
		  '3gp' => 'application/octet-stream'
		  );

		  if (ALLOWED_REFERRER !== ''	&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)){
		  	die("Internal server error. Please contact system administrator.");
		  }

		  set_time_limit(0);

		  if (!isset($file) || empty($file)) {
		  	die("Please specify file name for download.");
		  }

		  if (strpos($file, "\0") !== FALSE) die('');

		  $fname = basename($file);


		  // get full file path (including subfolders)
		  $file_path = '';
		  $this->find_file(BASE_DIR, $fname, $file_path);

		  if (!is_file($file_path)) {
		  	die("File does not exist. Make sure you specified correct file name.");
		  }

		  $fsize = filesize($file_path);
		  $fext = strtolower(substr(strrchr($fname,"."),1));
		  if (!array_key_exists($fext, $allowed_ext)) {
		  	die("Not allowed file type.");
		  }
		  if ($allowed_ext[$fext] == '') {
		  	$mtype = '';
		  	// mime type is not set, get from server settings
		  	if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  	}
		  	else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  	}
		  	if ($mtype == '') {
		    $mtype = "application/force-download";
		  	}
		  }
		  else {
		  	// get mime type defined by admin
		  	$mtype = $allowed_ext[$fext];
		  }

		  if (!isset($_GET['fc']) || empty($_GET['fc'])) {
		  	$asfname = $fname;
		  }
		  else {
		  	// remove some bad chars
		  	$asfname = str_replace(array('"',"'",'\\','/'), '', $_GET['fc']);
		  	if ($asfname === '') $asfname = 'NoName';
		  }

		  // set headers
		  header("Pragma: public");
		  header("Expires: 0");
		  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		  header("Cache-Control: public");
		  header("Content-Description: File Transfer");
		  header("Content-Type: $mtype");
		  header("Content-Disposition: attachment; filename=\"$asfname\"");
		  header("Content-Transfer-Encoding: binary");
		  header("Content-Length: " . $fsize);

		  // download
		  // @readfile($file_path);
		  $file = @fopen($file_path,"rb");
		  if ($file) {
		  	while(!feof($file)) {
		    print(fread($file, 1024*8));
		    flush();
		    if (connection_status()!=0) {
		    	@fclose($file);
		    	die();
		    }
		  	}
		  	@fclose($file);
		  }

		  // log downloads
		  /*if (!LOG_DOWNLOADS) die();

		  $f = @fopen(LOG_FILE, 'a+');
		  if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		  }*/

		  $this->autoRender = false;
	}


	function mmdownload($channel=null){
		if($channel == 'oss')
		$file = 'marketing_oss.zip';

		if($channel == 'tss')
		$file = 'marketing_smartshop.zip';

		//$file = "Marketing.zip";

		$info = pathinfo($file);
		$pure_file_name =  basename($file,'.'.$info['extension']);

		define('ALLOWED_REFERRER', '');
		$allowed_ext = array (
		  'zip' => 'application/zip'		
		  );

		  if (ALLOWED_REFERRER !== ''	&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)){
		  	die("Internal server error. Please contact system administrator.");
		  }

		  set_time_limit(0);

		  if (!isset($file) || empty($file)) {
		  	die("Please specify file name for download.");
		  }

		  if (strpos($file, "\0") !== FALSE) die('');

		  $fname = basename($file);

		  // get full file path (including subfolders)
		  $file_path = '';
		  $this->find_file(BASE_DIR, $fname, $file_path);

		  if (!is_file($file_path)) {
		  	die("File does not exist. Make sure you specified correct file name.");
		  }

		  $fsize = filesize($file_path);
		  $fext = strtolower(substr(strrchr($fname,"."),1));
		  if (!array_key_exists($fext, $allowed_ext)) {
		  	die("Not allowed file type.");
		  }
		  if ($allowed_ext[$fext] == '') {
		  	$mtype = '';
		  	// mime type is not set, get from server settings
		  	if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  	}
		  	else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  	}
		  	if ($mtype == '') {
		    $mtype = "application/force-download";
		  	}
		  }
		  else {
		  	// get mime type defined by admin
		  	$mtype = $allowed_ext[$fext];
		  }

		  if (!isset($_GET['fc']) || empty($_GET['fc'])) {
		  	$asfname = $fname;
		  }
		  else {
		  	// remove some bad chars
		  	$asfname = str_replace(array('"',"'",'\\','/'), '', $_GET['fc']);
		  	if ($asfname === '') $asfname = 'NoName';
		  }

		  // set headers
		  header("Pragma: public");
		  header("Expires: 0");
		  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		  header("Cache-Control: public");
		  header("Content-Description: File Transfer");
		  header("Content-Type: $mtype");
		  header("Content-Disposition: attachment; filename=\"$asfname\"");
		  header("Content-Transfer-Encoding: binary");
		  header("Content-Length: " . $fsize);

		  // download
		  // @readfile($file_path);
		  $file = @fopen($file_path,"rb");
		  if ($file) {
		  	while(!feof($file)) {
		    print(fread($file, 1024*8));
		    flush();
		    if (connection_status()!=0) {
		    	@fclose($file);
		    	die();
		    }
		  	}
		  	@fclose($file);
		  }

		  // log downloads
		  /*if (!LOG_DOWNLOADS) die();

		  $f = @fopen(LOG_FILE, 'a+');
		  if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		  }*/

		  $this->autoRender = false;
	}

	function createShortUrl(){

		$url_id = $_POST['url_id'];
		if(trim($url_id) != ''){
			$this->pkg_data_url->query("delete from pkg_data_urls where id in( select ref_id from pkg_data_short_urls where pkg_data_short_urls.id=".$url_id.")");
			$this->pkg_data_url->query("delete from pkg_data_short_urls where id =".$url_id);
		}

		$urlArr = $_POST['urls'];
		$typeArr = $_POST['type'];
		$titleArr = $_POST['title'];
		/// do all the insert here
		$insIdArr = array();
		$urlCnt = count($urlArr);
		for($k=0;$k<$urlCnt;$k++){
			if(trim($urlArr[$k]) != ''){
				$this->pkg_data_url->create();
				$this->pkg_data_url->data['title'] = trim($titleArr[$k]);
				$this->pkg_data_url->data['url'] = trim($urlArr[$k]);
				$this->pkg_data_url->data['type'] = trim($typeArr[$k]);
				if ($this->pkg_data_url->save($this->pkg_data_url->data)){
					array_push($insIdArr,$this->pkg_data_url->id);
				}
			}
		}

		$this->pkg_data_short_url->create();
		$this->pkg_data_short_url->data['pkg_id'] = $_POST['pkg_id'];
		$this->pkg_data_short_url->data['ref_id'] = implode(",",$insIdArr);
		$this->pkg_data_short_url->data['created'] = date('Y-m-d H:i:s');

		$this->pkg_data_short_url->save($this->pkg_data_short_url->data);
		$insert_id = $this->pkg_data_short_url->id;
		//ends

		$log_id = urlencode($this->objMd5->Encrypt($insert_id,encKey));
		$url = $this->General->getBitlyUrl(SITE_NAME.'packages/moreInfo/'.$log_id);
		echo $insert_id."^^^".$url;
		$this->autoRender = false;
	}

	function index() {
		$this->Group->recursive = 0;
		$this->set('groups', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid group', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('group', $this->Group->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Group->create();
			if ($this->Group->save($this->data)) {
				$this->Session->setFlash(__('The group has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The group could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid group', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Group->save($this->data)) {
				$this->Session->setFlash(__('The group has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The group could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Group->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for group', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Group->delete($id)) {
			$this->Session->setFlash(__('Group deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Group was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

	function asynchronousCall(){
		$random_id = $_REQUEST['random'];
		$data = $this->Group->query("SELECT * from asynchronous_calls where random_id = ".$random_id);
		echo $this->requestAction('/'.$data['0']['asynchronous_calls']['controller'].'s/after_'.$data['0']['asynchronous_calls']['action'],array('pass' => array($data['0']['asynchronous_calls']['params'])));
		$this->Group->query("DELETE FROM asynchronous_calls where random_id = ".$random_id);
		$this->autoRender = false;
	}

	function dataEntry(){
		$categories = $this->Group->query("SELECT distinct category_id from data_tables");
		$required = array();
		foreach($categories as $category){
			$required[] = $category['data_tables']['category_id'];
		}
		$this->set('required', $required);
	}

	function getEntryFields($cat_id=null){

		if($cat_id==null){
			$cat_id = $_POST['catid'];
		}
		$table = $this->Group->query("SELECT table_name from data_tables WHERE category_id = $cat_id");

		$table = $table['0']['data_tables']['table_name'];
		$packs = $this->General->getAllPackages(array($cat_id));
		$pack_arr = array();
		foreach($packs as $pack){
			$pack_arr[] = $pack['Package']['id'];
		}
		$this->set('catId',$cat_id);

		if($table != "data_funs"){
			$logData = $this->Group->query("SELECT content,date FROM $table WHERE $table.package_id in (". implode(",",$pack_arr) .") GROUP BY content ORDER BY id desc  LIMIT 5");
		}
		else {
			$table = "data_fun";
			$this->$table->recursive = -1;
			$logData = $this->$table->find('all', array(
				'fields' => array("Message.content"),
				'conditions' => array("$table.package_id in (". implode(",",$pack_arr) .")"),
				'joins' => array(
			array(
						'table' => 'messages',
						'alias' => 'Message',
						'type' => 'inner',
						'conditions'=> array("$table.message_id = Message.id")
			)
			),
				'order' => "$table.id desc",
				'group' => "$table.message_id",
				'limit' => '5'
				));
		}

		$this->set('logData',$logData);

		$this->set('packs',$packs);
		$this->set('table',$table);
		$this->set('catid',$cat_id);

		$this->render('/groups/entry_fields','ajax');
	}

	function enterData() {
		$table = $this->data['table'];
		foreach($this->data['Package'] as $pack){
			$this->data[$table]['package_id'] = $pack;
			if($table == "data_fun"){
				$array = explode(",",$this->data[$table]['content']);
				foreach($array as $msg){
					$msg = trim($msg);
					$this->data[$table]['message_id'] = $msg;
					$this->$table->create();
					$this->$table->save($this->data);
				}
			}
			else {
				if($table == "data_astrologies"){
					$date = $this->data[$table]['date']['year'] . "-" . $this->data[$table]['date']['month'] . "-" . $this->data[$table]['date']['day'];
				}
				else {
					$date = "0000-00-00";
				}

				$this->Group->query("INSERT INTO $table (package_id,content,date) VALUES (".$this->data[$table]['package_id'].",'".addslashes($this->data[$table]['content'])."','$date')");
			}
		}
		$this->getEntryFields($this->data['category']);
	}

	function adminControl(){
		if($_SESSION['Auth']['User']['group_id'] == 2){
			$oCatData = $this->Category->find('all', array('conditions' => array('Category.parent' => '0','Category.toshow' => '1'),
			'order' => 'FIELD(id,'.CATEGORIES_IN_ORDER.')',
			));
			$this->set('categories', $oCatData);
			$this->layout = 'admin';
			$this->render('/groups/package_control');
		}else{
			$this->redirect('/');
		}

	}

	function emailPanel(){

	}

	function sendEmail(){
		$email = $this->data['email'];
		$emails = explode(",",$email);
		$body = $this->data['body'];
		$subject = $this->data['subject'];
		if(!empty($email)){
			$this->Email->html_body = $body;
			$this->Email->to = $emails;
			$this->Email->subject = $subject;
			$result = $this->Email->send();
		}
		$this->render('email_panel','ajax');
	}

	function getPackages(){
		if($_SESSION['Auth']['User']['group_id'] != 2) exit;
		$cat_id = $_POST['catid'];
		$packs = $this->General->getAllChildPackagesAdmin($cat_id);
		$tmpStr = '';
		foreach($packs as $pk => $pv){
			$tmpStr .= $pk.",";
		}
		$tmpStr .= "0";

		$data = $this->User->query("select flag,package_id from crons_packages where package_id in (".$tmpStr.")");
		$dataArr = array();
		foreach($data as $d){
			$dataArr[$d['crons_packages']['package_id']] = $d['crons_packages']['flag'];
		}

		$func = "";

		$html = "";
		$array = '';
		$array = explode(',',DASH_TWEET);
		foreach($packs as $pk => $pv){
			if(!in_array($pk,$array)){
				//$data = $this->User->query("select flag from crons_packages where package_id = ".$pk);
				$class = "";
				//chk retail users
				$count1 = $this->User->query("select products_users.* from products_users join products_packages on (products_packages.package_id=".$pk." and products_packages.product_id = products_users.product_id) where products_users.active = 1 limit 1");
				//chk online user if theer r no retail users
				//if(count($count1) < 1){
				$count = $this->User->query("select id from packages_users where active = 1 and package_id = ".$pk." limit 2");
				//}

				$send = 'No';
				if(count($count1)>0)
				$send = 'Send';
				else if(count($count) == 1)
				$send = 'Check users';
				else if((count($count)+count($count1)) > 1)
				$send = 'Send';

				$count = "(".count($count).",".count($count1).")";
				if($dataArr[$pk] == 1)
				$class = "taggLinkBG1";

				$func = "getPackageInfo(".$pk.",$cat_id)";

				$html .= '<div class="taggLink taggLink1">
					<div id="pack_'.$pk.'" class="taggLinkBG '.$class.'">
					<div  class="taggLinkBorder">
					<div class="taggLinkCont">';
				$html .= "<a href='javascript:void(0);' onclick='".$func."'>".$pv.$count."</a>";
				$html .= "</div></div></div></div>";
			}
		}
		$html .= "<br class='clearLeft'/>";
		echo $html;
		$this->autoRender = false;
	}

	function getPackageInfo($pack_id=null,$cat_id=null) {
		if($_SESSION['Auth']['User']['group_id'] != 2) exit;
		if($pack_id == null){
			$pack_id = $_POST['packid'];
			$cat_id = $_POST['catid'];
		}

		$this->PackagesUser->recursive = -1;

		$flag = $this->User->query("select flag,starttime,lefttogo from crons_packages where package_id = " .$pack_id);

		$usersData = $this->PackagesUser->find('all', array(
			'fields' => array('User.mobile'),
			'conditions' => array('PackagesUser.package_id' => $pack_id,'PackagesUser.active' => '1'),
			'joins' => array(
		array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'inner',
					'conditions'=> array('PackagesUser.user_id = User.id')
		)
		),
			'limit' => '3'
			));
			$this->ProductsUser->recursive = -1;
			$usersData1 = $this->ProductsUser->find('all', array(
			'fields' => array('User.mobile'),
			'conditions' => array('ProductsUser.active' => '1'),
			'joins' => array(
			array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'inner',
					'conditions'=> array('ProductsUser.user_id = User.id')
			),
			array(
					'table' => 'products_packages',
					'alias' => 'ProductsPackage',
					'type' => 'inner',
					'conditions'=> array('ProductsPackage.package_id = '.$pack_id . ' AND ProductsPackage.product_id = ProductsUser.product_id')
			)
			),
			'limit' => '3'
			));

			$content = '';
			$lefttogo = $flag['0']['crons_packages']['lefttogo'];

			$time = explode(" ",$flag['0']['crons_packages']['starttime']);
			$this->Log->recursive = -1;
			$logData = $this->Log->find('all', array(
			'fields' => array('Log.content','Log.timestamp'),
			'conditions' => array('Log.package_id' => $pack_id),
			'order' => 'Log.timestamp desc',
			'group' => 'Log.content',
			'limit' => '5'
			));

			$this->set('catId',$cat_id);
			$this->set('logData',$logData);


			if($flag['0']['crons_packages']['flag'] == '1' && !(empty($usersData) && empty($usersData1))){
					
				$this->CategoriesPackage->recursive = 0;
				$table_name = $this->CategoriesPackage->find('all', array('fields' => array('data_tables.table_name'),'conditions' => array('CategoriesPackage.package_id' => $pack_id),
							'joins' => array(
				array(
									'table' =>'data_tables',
									'type' => 'inner',
									'conditions' => array('CategoriesPackage.category_id = data_tables.category_id')
				)
				)
				));
				if(!empty($table_name)) {
					$table = $table_name['0']['data_tables']['table_name'];

					if($table == "data_funs"){
						$table = "data_fun";
						$this->$table->recursive = -1;
						$dataToSend = $this->$table->find('first',
						array(
							'fields' => array("$table.id",'Message.content','Message.id'),
							'conditions' => array("$table.flag" => '0',"$table.package_id" => $pack_id),
							'joins' => array(
						array(
									'table' => 'messages',
									'alias' => 'Message',
									'type' => 'inner',
									'conditions'=> array("$table.message_id = Message.id")
						)

						)));
							
						if(!empty($dataToSend)){
							$content = $dataToSend['Message']['content'];
							$this->set('messageId',$dataToSend['Message']['id']);
							$this->set('dataId',$dataToSend[$table]['id']);
						}
						else {
							$this->set('dataId',0);
						}
					}
					else {
						if($table == "data_astrologies"){
							$dataToSend = $this->Group->query("SELECT id,content FROM $table WHERE flag=0 AND package_id=$pack_id AND date = '".date('Y-m-d')."'");
						}
						else {
							$dataToSend = $this->Group->query("SELECT id,content FROM $table WHERE flag=0 AND package_id=$pack_id");
						}
						if(!empty($dataToSend)){
							$content = $dataToSend['0'][$table]['content'];
							$this->set('dataId',$dataToSend['0'][$table]['id']);
						}
						else {
							$this->set('dataId',0);
						}
					}
					$this->set('content',$content);

						
				}
				$this->set('table',$table);
				$this->Package->recursive = -1;
				$packData = $this->Package->find('first', array(
				'conditions' => array('Package.id' => $pack_id)
				));
				$feedData = null;
				$arr = explode(",",AUTOMATED_PACKS);
				if(in_array($pack_id,$arr)) {
					$feedData = $this->Rss->query("select * from rss_feeds, rsses where rsses.id = rss_feeds.rss_id and rsses.package_id = " .$pack_id. " and flag = 0 order by rss_feeds.id desc limit 10");
				}
					
				$content = "*".$packData['Package']['name']."*\n" . $content;
				$this->set('content',$content);
				$this->set('time',$time[1]);
				$this->set('packData',$packData);
				$this->set('userData',$usersData);
				$this->set('lefttogo',$lefttogo);
				$this->set('feedData',$feedData);
					
				$this->set('noOfFields',4);
				$this->set('urlType',array('Image'=>0,'Video'=>1));
					
				$this->render('/groups/send_fields','ajax');
			}

			else {
				if($lefttogo == '0'){
					echo "All the scheduled messages have already sent<br>";
				}
				else
				if($time[1] == '00:00:00'){
					echo "This can be posted anytime<br>";
				}
				else echo "Time at which this should be posted: " . $time[1] ."<br>";
				echo "Users registered to it are " . count($usersData);
				$this->render('/elements/messages_sent','ajax');
			}
	}

	function nextMessage(){
		$data_id = $_POST['dataid'];
		$table = $_POST['table'];
		if($table == "data_fun"){
			$table = "data_funs";
		}
		$this->Group->query("DELETE FROM $table WHERE id = $data_id");
		$this->autoRender = false;
	}

	function submitData(){
		$table = $this->data['table'];
		$pack_id = $this->data['package'];
		$category = $this->data['category'];
		if(empty($table)) $table = '0';
		$content = $this->data[$table]['content'];
		$flag = $this->Group->query("select flag from crons_packages where package_id=".$pack_id);

		if(!empty($content) && $flag['0']['crons_packages']['flag'] == '1'){
			$msgId = null;
			if($table == "data_fun"){
				$msgId = $this->data['message'];
			}
			$dataId = null;
			if(isset($this->data['dataId'])){
				$dataId = $this->data['dataId'];
			}
			$usersData = $this->PackagesUser->find('all', array(
				'fields' => array('User.mobile','User.id'),
				'conditions' => array('PackagesUser.package_id' => $pack_id,'PackagesUser.active' => '1'),
				'joins' => array(
			array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.user_id = User.id')
			),
			array(
						'table' => 'packages',
						'alias' => 'Package',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.package_id = Package.id')
			),
			)));
				
			/* Offline Retailer Users */
			//if(!in_array($pack_id,explode(",",CYCLIC_PACKAGES))){
			$this->ProductsUser->recursive = -1;
			$productUsersData = $this->ProductsUser->find('all', array(
					'fields' => array('distinct User.mobile','User.id'),
					'conditions' => array('ProductsUser.active' => '1'),
					'joins' => array(
			array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'inner',
							'conditions'=> array('ProductsUser.user_id = User.id')
			),
			array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions'=> array('ProductsUser.product_id = Product.id')
			),
			array(
							'table' => 'products_packages',
							'type' => 'inner',
							'conditions'=> array('ProductsUser.product_id = products_packages.product_id','products_packages.package_id = '.$pack_id,'products_packages.misscall is null')
			)
			)));
			$usersData = array_merge($usersData,$productUsersData);
			//}
			/**/
				
			//$this->printArray($usersData); exit;
				
			$mobiles = array();
			$repeated = array();
			$userIds = array();
				
			foreach($usersData as $user){
				$flag = true;
				if($table == "data_fun" && $msgId != ''){
					$logCheck = $this->Log->find('count', array(
						'conditions' => array('Log.message_id' => $msgId,'Log.mobile' => $user['User']['mobile'],'Log.package_id is not null'),
					));
						
					if($logCheck > 0 && $user['User']['mobile'] != DUMMY_USER){
						$repeated[] =   $user['User']['mobile'];
						$userIds[] =   $user['User']['id'];
						$flag = false;
					}
				}
				if($flag){
					//make an entry in log table
					$details = $this->General->getMobileDetails($user['User']['mobile']);
					if(($pack_id == MUMBAI_NEWS && strtoupper($details['2']) == 'MU') || $pack_id != MUMBAI_NEWS){
						$insID = $this->General->logUpdate($pack_id,$msgId,null,nl2br($content),$user['User']['mobile'],$user['User']['id']);
						if($user['User']['mobile'] != DUMMY_USER)
						$mobiles[$insID] = $user['User']['mobile'];
					}
				}
			}
			$pacFreq = $this->Package->find('first', array(
				'fields' => array('Package.frequency,Package.name'),
				'conditions' => array('Package.id' => $pack_id)
			));
				
				
			if(!empty($mobiles)){
				if(empty($pacFreq['Package']['frequency'])) {
					if($table == "data_fun"){
						$this->General->sendMessage('',$mobiles,$this->General->br2newline($content),'pacM');
					}
					else {
						$this->General->sendMessage('',$mobiles,$content,'pacM');
					}
				}
				else {
					if($table == "data_fun"){
						$this->General->sendMessage('',$mobiles,$this->General->br2newline($content),'pac');
					}
					else {
						$this->General->sendMessage('',$mobiles,$content,'pac');
					}
				}
			}
			if($dataId != null && $dataId != '0'){
				$table1 = $table;
				if($table == "data_fun")
				$table1 = "data_funs";
				$this->Group->query("UPDATE $table1 SET flag = 1 WHERE id = $dataId");
			}
				
			if($table == "data_fun" && $msgId != '' && !in_array($pack_id,explode(",",DONT_SHOW_MSG))){
				$this->Message->updateAll(array('Message.toshow' => '1'), array('Message.id' => $msgId));
			}
				
			if(count($repeated) > 0){
				$this->Message->recursive = -1;

				$this->CategoriesPackage->recursive = 0;
				$cat_ids = $this->CategoriesPackage->find('all', array('fields' => array('CategoriesPackage.category_id'),'conditions' => array('CategoriesPackage.package_id' => $pack_id)));
				$ids = array();
				foreach($cat_ids as $cat_id){
					$ids[] = $cat_id['CategoriesPackage']['category_id'];
				}

				/*$logCheck = $this->Message->query("select messages.id,messages.content from messages where messages.toshow = 1 and messages.category_id in(".implode(",",$ids).") and messages.id not in
				 (select message_id from logs where package_id is not null and mobile in (".implode(",",$repeated).") and message_id is not null) order by messages.rating desc limit 1");
				 */
				$logCheck = $this->Message->query("select logs.content,logs.message_id from logs where package_id = $pack_id and logs.timestamp < '".date('Y-m-d H:i:s',strtotime(' - 3 months'))."' group by logs.message_id order by id desc limit 1");

				if(!empty($logCheck)){
					//$content =  "*".$pacFreq['Package']['name']."*\n" . $logCheck['0']['messages']['content'];
					//$msgId = $logCheck['0']['messages']['id'];
						
					$content = $logCheck['0']['logs']['content'];
					$msgId = $logCheck['0']['logs']['message_id'];
						
					$i = 0;
					$mobiles = array();
					foreach($repeated as $mob){
						$insID = $this->General->logUpdate($pack_id,$msgId,null,$content,$mob,$userIds[$i]);
						$mobiles[$insID] = $mob;
						$i++;
					}
						
					$content = strip_tags($content);
					if(empty($pacFreq['Package']['frequency'])){
						$this->General->sendMessage('',$mobiles,$content,'pacM');
					}
					else $this->General->sendMessage('',$mobiles,$content,'pac');
				}
				else {
					//$this->General->mailToAdmins("Empty message for package ".$pacFreq['Package']['name'], "Not delivered to: " . implode(",",$repeated));
				}
			}
				
			$flag = $this->General->cronsPackageUpdate($pack_id);
				
			echo "<script> changeFlag(".$pack_id.",".$flag.");</script>";
		}
		$this->getPackageInfo($pack_id,$category);
	}

	/* Log of messages sent for packages*/
	function packageLogs() {
		$this->Log->recursive = -1;
		$logData = $this->Log->find('all', array(
			'fields' => array('Log.content','Log.timestamp','Log.package_id','Log.mobile'),
			'conditions' => array('Log.package_id is not null'),
			'order' => 'Log.timestamp desc,Log.content'
			));

			$content = "";
			$logs = array();
			$users = array();
			$pack_id = '';
			$i = 0;


			foreach($logData as $log){
					
				if($content == $log['Log']['content'] && $pack_id == $log['Log']['package_id']){
					$users[] = $log['Log']['mobile'];
				}
				else {
					if(!empty($logs)) {
						$logs[$i]['Log']['users'] = implode(",",$users);
						$i++;
					}
					$users = array();
					$this->Package->recursive = -1;
					$pckData = $this->Package->find('first', array(
					'fields' => array('Package.name'),
					'conditions' => array('Package.id' => $log['Log']['package_id']),
					));
					$log['Log']['packageName'] = $pckData['Package']['name'];
					$logs[] = $log;
					$content = $log['Log']['content'];
					$pack_id = $log['Log']['package_id'];
					$users[] = $log['Log']['mobile'];
				}
					
			}

			$count = count($logs);
			$logs[$count-1]['Log']['users'] = implode(",",$users);

			$this->set('logData',$logs);
	}

	/*function for registration through missed call*/
	function missedCallReg($mobile_number=null,$pwd =null)
	{
		if($mobile_number == '9004387418')
		$this->sendMessage(SMS_SENDER,'9004387418','misscall service is active','dnd');
		$tenDigitNum = substr($mobile_number,-10);
		$this->data['User']['mobile'] = $tenDigitNum;
		$exists = $this->User->find('first',
		array('fields' => array('User.verify','User.syspass','User.passflag'),
				'conditions' => array('User.mobile' => $this->data['User']['mobile'])));
		//$exists = $this->General->checkIfUserExists($this->data['User']['mobile']);
		$msg = '';
		if(!empty($exists)){
			//send msg to the user that he has already signed up
			if($exists['User']['verify'] < 3 && $exists['User']['passflag'] == 0 && $exists['User']['verify'] > 0){
				$msg = $this->General->sendPassword($this->data['User']['mobile'],$exists['User']['syspass'],1,1);
				$verify = $exists['User']['verify'] + 1;
				$this->User->updateAll(array('User.verify' => $verify), array('User.mobile' => $this->data['User']['mobile']));
			}
			else if($exists['User']['passflag'] == 0 &&  $exists['User']['verify'] == 3){
				$msg = "You have exceeded the max number of trials. Your password has already been sent. You will be receiving it soon. For more details contact support@smstadka.com";
				$verify = $exists['User']['verify'] + 1;
				$this->User->updateAll(array('User.verify' => $verify), array('User.mobile' => $this->data['User']['mobile']));
			}
			else{
				$msg = 'no';
			}
		}
		else {
			$mobileDetails = $this->General->getMobileDetails($this->data['User']['mobile']);
			if($mobileDetails['0'] != '' || $mobileDetails['1'] != ''){
				$user = $this->General->registerUser($this->data['User']['mobile'],MISSCALL_REG);
				$msg = $this->General->sendPassword($this->data['User']['mobile'],$user['User']['syspass'],1,1);
			}
			else{
				$msg = 'no';
			}
		}
		echo $msg;
		$this->autoRender = false;
	}

	/*function to subscribe packages through SMS*/
	function requestBySMS($mobile_number=null, $msg=null, $pwd=null, $virtual=null)
	{
		if($mobile_number == null){
			$mobile_number = $_REQUEST['mobile'];
			$msg = $_REQUEST['message'];
			$pwd = $_REQUEST['password'];
			$virtual = $_REQUEST['virtual'];
		}
		$msg = trim(urldecode($msg));
		$msg = preg_replace("/'/", '', $msg);
		$mobile_number = trim($mobile_number);
		$mobile_number = substr($mobile_number, -10);

		$marsh_codes = array('MHELP','MLOCATE','MPLAN','MCLAIM','MGET','MCHECK','MCALLBACK','MSOS');
		$mcodes = explode(" ",$msg);
		if(in_array(strtoupper($mcodes[0]),$marsh_codes)){
			$this->Group->query("INSERT INTO log_marsh (mobile,message,timestamp) VALUES ('$mobile_number','$msg','".date("Y-m-d H:i:s")."')");
			$msg = substr($msg,1);
			$url = "http://ussd.signal7.in/marsh_ussd_tests/smsQuery/$mobile_number/".urlencode($msg);
			$data = array();
			$this->General->curl_post_async($url,$data);
			exit;
		}
		else if(strtoupper($mcodes[0]) == 'TRANSID'){
			$trans_id = $mcodes[1];
			$amount = $mcodes[2];
			$url = "http://www.mindsarray.com/demos/vfs/adminTopRequest.php?transid=$trans_id&amount=$amount";
			$data = array();

			//$this->General->curl_post_async($url,$data);
			//asyn req is nt wrking. below code is added
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			curl_exec($ch);
			curl_close($ch);
			exit;
		}

		$msgs = explode("*",$msg , 2);
		$msg = $msgs[0];
		$param = "";
		$data = array();
			
		if(isset($msgs[1])){
			$param = $msgs[1];
		}
		if($pwd != MOBILE_URL_PASSWORD && $_SESSION['Auth']['User']['group_id'] != ADMIN){
			echo "You are not authorized to accesss this link";
		}
		else if(strlen($mobile_number) >= 10){
			$mobile_number = substr($mobile_number, -10);
				
			$msg = trim($msg);
			//If 8 digit number then its from scratch card
			$codes = explode(" ",$msg);
				
			$retail_flag = 0;
			$retailCodes = array('TRY','IPOD','UNSUB','LOCATION','SHOP','REF','PNR','PLAYWIN','ACT','SALE','OPT');
			if(is_numeric($msg) || in_array(strtoupper($codes[0]),$retailCodes)){ //user via retailer
				if(strtoupper($codes[0]) == 'REF'){
					$ret = $this->freeCouponCode($mobile_number,$msg,$param);
				}else{
					$ret = $this->retailerPackageSubscription($mobile_number,$msg,$param);
				}

				$retail_flag = 1;
			}
			else {//online user
				$ret = $this->userPackageSubscription($mobile_number,$msg);
			}
				
			if($ret['sms'] != '' && !empty($ret['sms']))
			{
				$data['sms'] = $ret['sms'];
				$data['mobile'] = $mobile_number;
				if(isset($ret['root']))
				$data['root'] = $ret['root'];
				else $data['root'] = 'user';
			}
				
			if(!empty($param))
			$msg = $msg. "*" . $param;
			$this->Group->query("INSERT INTO log_smsrequest (mobile,message_in,message_out,done,new_user,subscription,timestamp) VALUES ('" . $mobile_number . "','". addslashes($msg). "','". addslashes($ret['sms']) . "',".$ret['done_flag'].",".$ret['new_user'].",".$ret['subscription'].",'" . date("Y-m-d H:i:s"). "')");
		}
		else {
			$ret['mail']['subject'] = "SMS from $mobile_number";
			$ret['mail']['body'] = $msg;
			$ret['mail']['emails'] = 'chirutha@mindsarray.com';
		}

		$url = SERVER_BACKUP . 'users/sendMsgMails';
		if(isset($ret['mail']['subject'])){
			$data['mail_subject'] = $ret['mail']['subject'];
			$data['mail_body'] = $ret['mail']['body'];
		}

		if(!empty($data)){
			//$this->General->curl_post_async($url,$data);
                        $this->General->curl_post($url,$data);
		}

		$this->autoRender = false;
	}

	function userPackageSubscription($mobile_number, $msg){
		$exists = $this->General->checkIfUserExists($mobile_number);
		$mail = array();
		$sms = "";
		$done_flag = 0;
		$subscription = 0;

		$codes = explode(" ",$msg);
		$code = strtoupper($codes[0]);
		$otherCodes = array('HELP','CD','BAL','FORGOT','ADMIN','LOG','REGISTER');
			
		if(!$exists && $code != 'HELP' && $code != 'REGISTER')
		{
			$sms = $this->General->createMessage("INVALID_QUERY",array($msg,"0".VIRTUAL_NUMBER));
			$root = 'template';
			//$mail['subject'] = "Offline: New user $mobile_number";
			$mail['body'] = "Not registered. Message sent '$msg'\n";
			$mail['body'] .= "Msg Sent to User: $sms";
		}
		else if($code == 'REGISTER')
		{
			$this->General->logMisscalls($mobile_number,DUMMY_USER);
			if(!$exists){
				$user = $this->General->registerUser($mobile_number,ONLINE_REG);
				$this->General->sendPassword($mobile_number,$user['User']['syspass'],1);
			}
			else {
				$sms = "You are already registered as SMSTadka User. To get your password send SMS: FORGOT to ". "0".VIRTUAL_NUMBER;
			}
		}
		else if($code == 'LOG')
		{
			$msg_codes = array_diff($codes,array($codes[0]));
			$msg = implode(" ", $msg_codes);
			$this->Group->query("INSERT INTO log_sms (mobile,sms,timestamp) VALUES ('$mobile_number','".addslashes($msg)."','".date("Y-m-d H:i:s")."')");
			$sms = "SMS Successfully inserted in smstadka logs";
		}
		else if($code == 'ADMIN')
		{
			$msg_codes = array_diff($codes,array($codes[0]));
			$msg = implode(" ", $msg_codes);
			$sms = "SMS Successfully sent to all admins";
			$this->General->smsToAdmins($msg,$mobile_number);
		}
		else {
				
			if(count($codes) == 1 && !in_array($code,$otherCodes)){
				$this->Package->recursive = -1;
				$packData = $this->Package->find('first', array('conditions' => array('Package.code' => $code)));
				if(empty($packData)){
					$sms = $this->General->createMessage("INVALID_CODE",array("0".VIRTUAL_NUMBER));
					$root = 'template';
					//$mail['subject'] = "Online: Wrong Package Code Sent";
					$mail['body'] = "Mobile: $mobile_number. Message sent '$msg'\n";
				}
				else {
					$mobileData = $this->General->getUserDataFromMobile($mobile_number);
					$userData = $this->PackagesUser->find('first', array('fields' => array('SUM(PackagesUser.active) as active','SUM(PackagesUser.trial_flag) as trial','COUNT(PackagesUser.id) as ids'),'conditions' => array('PackagesUser.package_id' => $packData['Package']['id'],'PackagesUser.user_id' => $mobileData['id'])));
					$this->ProductsUser->recursive = -1;
					$productUsersData = $this->ProductsUser->find('first', array(
						'fields' => array('ProductsUser.user_id','SUM(ProductsUser.active) as active','SUM(ProductsUser.trial) as trial','COUNT(ProductsUser.id) as ids'),
						'conditions' => array('ProductsUser.user_id' => $mobileData['id']),
						'joins' => array(
					array(
								'table' => 'products',
								'alias' => 'Product',
								'type' => 'inner',
								'conditions'=> array('ProductsUser.product_id = Product.id')
					),
					array(
								'table' => 'products_packages',
								'type' => 'inner',
								'conditions'=> array('ProductsUser.product_id = products_packages.product_id','products_packages.package_id = '.$packData['Package']['id'])
					)
					)));
					if(empty($userData) || $userData['0']['active'] == 0 || $userData['0']['trial']*$userData['0']['ids']*$userData['0']['active'] == 1){
						$make_entry = true;
					}
						
					if($make_entry && (empty($productUsersData) || $productUsersData['0']['active'] == 0 || ($productUsersData['0']['trial']*$productUsersData['0']['ids']*$productUsersData['0']['active'] == 1))){
						if($packData['Package']['price'] <= $mobileData['balance'])
						{
							$draft_id = $this->Draft->find('first',array('fields' => array('Draft.id'),'conditions' => array('Draft.user_id' => $mobileData['id'], 'Draft.refid' =>$packData['Package']['id'],'Draft.type' => 'pck')));
							$this->Draft->delete($draft_id['Draft']['id']);
								
							$this->data['Slot']['id'] = 1;
							$this->data['PackagesUser']['slot_id'] = $this->data['Slot']['id'];
							$this->data['PackagesUser']['package_id'] = $packData['Package']['id'];
							$this->data['PackagesUser']['user_id'] = $mobileData['id'];
							$this->data['PackagesUser']['active'] = '1';
							$this->data['PackagesUser']['start'] = date('Y-m-d H:i:s');
							$this->data['PackagesUser']['end'] = date('Y-m-d H:i:s', strtotime( '+ '.$packData['Package']['validity'].' days'));
								
							$balance = $mobileData['balance'] - $packData['Package']['price'];
							$this->General->userCyclicPackageInsert(null,$packData['Package']['id'],$mobileData['id']);
								
							$this->PackagesUser->create();
							if ($this->PackagesUser->save($this->data)) {

								$this->General->makeOptIn247SMS($mobile_number,1);
								//make an entry in users table for balance update
								$this->General->balanceUpdate($packData['Package']['price'],'subtract',$mobileData['id']);

								//make an entry in transactions table
								$this->General->transactionUpdate(TRANS_ADMIN_DEBIT,$packData['Package']['price'],$packData['Package']['id'],null,$mobileData['id']);

								$vars[] = $packData['Package']['name'];
								$vars[] = $balance;
								$sms = $this->General->createMessage("PACKAGE_SUBSCRIBE_OFFLINE",$vars);
								$root = "template";
								$done_flag = 1;
								$subscription = 1;
								$params = array();
								$params['pck_name'] = $packData['Package']['name'];
								$params['balance'] = $balance;
								$params['pck_id'] = $packData['Package']['id'];
								$params['mobile'] = $mobile_number;
								$params['user_id'] = $mobileData['id'];
								$params['offline'] = 1;
								echo $this->requestAction('/packages/after_subscribe',array('pass' => array(json_encode($params))));
							}
						}
						else {
							$vars[] = $mobileData['balance'];
							$vars[] = "0".VIRTUAL_NUMBER;
							$sms = $this->General->createMessage("PACKAGE_SUBSCR_NOT_ENOUGH_BAL",$vars);
							$root = "template";
						}
					}
					else
					{
						$vars[] = $packData['Package']['name'];
						$vars[] = "0".VIRTUAL_NUMBER;
						$sms = $this->General->createMessage("PACKAGE_ALREADY_SUBSCRIBED",$vars);
						$root = "template";
					}
				}
			}
			else if($code == "U" && count($codes) == 2){
				$code = $codes[1];
				$this->Package->recursive = -1;
				$packData = $this->Package->find('first', array('conditions' => array('Package.code' => $code,'Package.toshow' => 1)));
				if(empty($packData)){
					$sms = $this->General->createMessage("INVALID_CODE",array("0".VIRTUAL_NUMBER));
					$root = 'template';
				}
				else {
					$mobileData = $this->General->getUserDataFromMobile($mobile_number);
					$this->PackagesUser->recursive = -1;
					$userData = $this->PackagesUser->find('first', array('conditions' => array('PackagesUser.package_id' => $packData['Package']['id'],'PackagesUser.user_id' => $mobileData['id'],'PackagesUser.active' => 1)));
					if(empty($userData))
					{
						$vars[] = "0".VIRTUAL_NUMBER;
						$vars[] = "0".VIRTUAL_NUMBER;
						$sms = $this->General->createMessage("PACKAGE_ALREADY_INACTIVE",$vars);
						$root = "template";
					}
					else {
						if($this->PackagesUser->updateAll(array('PackagesUser.active' => '0', 'PackagesUser.end' => "'".date('Y-m-d H:i:s')."'"), array('PackagesUser.id' => $userData['PackagesUser']['id']))){
							$packUserCount = $this->PackagesUser->find('count', array(
											'conditions' => array('PackagesUser.package_id' => $packData['Package']['id'], 'PackagesUser.active' => '0', 'PackagesUser.user_id' =>  $mobileData['id'])
							));
								
							$time = date('Y-m-d H:i:s', strtotime(' - '.MONEY_BACK_DAYS.' days'));
							$moneyBack = explode(',',MONEY_BACK_PACKS);
							if($packUserCount ==  1 && $userData['PackagesUser']['start'] > $time && in_array($packData['Package']['id'],$moneyBack)) { //money back
								$bal = $this->General->balanceUpdate($packData['Package']['price'],'add',$mobileData['id']);

								//make an entry in transactions table
								//$bal = $this->General->getBalance($mobileData['id']);
								$this->General->transactionUpdate(TRANS_ADMIN_UNSCR_CREDIT,$packData['Package']['price'],$packData['Package']['id'],null,$mobileData['id']);
								$mail['body'] = "User " . $mobile_number . " unsubscribed the package " . $packData['Package']['name'] . " within 7 days";
								//$mail['subject'] = "Via SMS: Package UnSubscription";
								$alias = "PACKAGE_UNSUBSCRIBED";
								$vars[] = $packData['Package']['name'];
								$vars[] = $bal;
							}
							else {
								$alias = "PACKAGE_UNSUBSCRIBED_NOBAL";
								$vars[] = $packData['Package']['name'];
							}
							$sms = $this->General->createMessage($alias,$vars);
							$root = "template";
							$done_flag = 1;
							$subscription = 2;
						}
					}
				}
			}
			else if($code == "HELP"){
				//$mail['subject'] = "HELP : ".$mobile_number;
				$mail['body'] = "$msg ".$mobile_number;
				$done_flag = 1;
				$sms = $this->General->createMessage("DEFAULT_MESSAGE_ON_QUERY",array());
				$root = "template";
				$mobileData = $this->General->getUserDataFromMobile($mobile_number);
				if(!empty($mobileData)){
					$this->General->tagUser('HELP',$mobileData['id']);
				}
			}
			else if($code == 'BAL'){
				$mobileData = $this->General->getUserDataFromMobile($mobile_number);
				$bal = 	$mobileData['balance'];
				$sms = $this->General->createMessage("BALANCE_CHECK",array($bal));
				$root = "template";
				$done_flag = 1;
			}
			else if($code == 'FORGOT'){
				$url = SERVER_BACKUP . 'users/forgotPasswordCheck/1/' . $mobile_number;
				$data = array();

				$this->General->curl_post_async($url,$data);
			}
			else if($code == "CD"){
				$done_flag = 1;
				$sms = "Recommended Package Codes:";
				$mobileData = $this->General->getUserDataFromMobile($mobile_number);
				$recom = $this->General->getRecommendedPacks($mobileData['id'],3);
				foreach($recom as $pck){
					$sms .= "\n" . $pck['Package']['name'] . "\n";
					$sms .= "SMS Code: " . $pck['Package']['code'] . "\n";
					$sms .= "Rs " . intVal($pck['Package']['price']) . " for " . $pck['Package']['validity'] . " days\n";
				}
				$sms .= "\nFor more visit www.smstadka.com or Send HELP to 0".VIRTUAL_NUMBER;
			}
			/*else if($code == 'PAY'){
				$mail['subject'] = "PAY : ".$mobile_number;
				$mail['body'] = "Message from User: $msg";
				$sms = $this->General->createMessage("DEFAULT_MESSAGE_ON_QUERY",array());
				$root = "template";
				$done_flag = 1;
				}*/
			else {
				//$mail['subject'] = "Message from User: $mobile_number";
				$mail['body'] = $msg;
				$sms = $this->General->createMessage("INVALID_QUERY",array($msg,"0".VIRTUAL_NUMBER));
				$root = 'template';
			}
		}

		$ret_array['sms'] = $sms;
		$ret_array['mail'] = $mail;
		$ret_array['done_flag'] = $done_flag;
		$ret_array['subscription'] = $subscription;
		$ret_array['new_user'] = 0;
		if(isset($root))
		$ret_array['root'] = $root;

		return $ret_array;
	}

	function sendFreeCouponCode($par1,$par2){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$this->ProductsUser->recursive = -1;
			/*$data = $this->ProductsUser->find('all', array(
			 'fields' => array('ProductsUser.id','ProductsUser.user_id','ProductsUser.product_id','ProductsUser.start'),
			 'conditions' => array('ProductsUser.end is not null and ProductsUser.ref_code is null and ProductsUser.active = 1 and (ProductsUser.count - ProductsUser.trial)>0')
			 ));*/
				
			$data = $this->ProductsUser->find('all', array(
					'fields' => array('ProductsUser.id','ProductsUser.user_id','ProductsUser.product_id','ProductsUser.start','ProductsUser.ref_code'),
					'conditions' => array('ProductsUser.end is not null and ProductsUser.active = 1 and (ProductsUser.count - ProductsUser.trial)>0 AND ProductsUser.product_id NOT IN (' . NO_REF_CODES . ')')
			));
				
			foreach($data as $data1){
				if(true){
					$dArr = explode(' ',$data1['ProductsUser']['start']);
					//if(strtotime($dArr[0]) == strtotime('-10 day', strtotime ( date('Y-m-d') ))){
					if(floor(abs(strtotime(date('Y-m-d')) - strtotime($dArr[0])) / (60*60*24)) % 8 == 0){
						$prodInfo = $this->General->getProductInfo($data1['ProductsUser']['product_id']);
						if($data1['ProductsUser']['ref_code'] == ''){
							$uId = base_convert(time()*rand(1,9),10,36);
							$this->Group->query("update products_users set ref_code = '".$uId."' where id=".$data1['ProductsUser']['id']);
						}else{
							$uId = 	$data1['ProductsUser']['ref_code'];
						}
						$userInfo = $this->General->getUserDataFromId($data1['ProductsUser']['user_id']);

						$smsRef= "Apne dosto ko Dijiye Free 2 day trial code and Get Extra Validity of 3 days for '".$prodInfo['Product']['name']."'
For each code activation on your friends mobile, aapko milegi 3 din ki extra FREE Seva from SMSTadka
Just ask your friends to SMS: REF ".strtoupper($uId)." to 0".VIRTUAL_NUMBER."
Offer is valid till your product is active";

						$this->General->sendMessage(SMS_SENDER,$userInfo['mobile'],$smsRef,'sms');
					}
				}
			}
			$this->releaseLock();
			$this->autoRender = false;
		}
	}

	function freeCouponCode($mobile_number, $msg, $param){
		$sms = '';
		$mail = array();
		$new_user = 0;
		$done_flag = 0;
		$subscription = 0;

		$codeArr = explode(" ",$msg);
		$code = strtoupper(trim($codeArr[1]));

		if(!empty($code)){
			$data = $this->Group->query("Select id,product_id,end,user_id,active from products_users where ref_code='".$code."'");
			if(!empty($data)){//valid code
				$end = $data[0]['products_users']['end'];
				$recId = $data[0]['products_users']['id'];
				$productId = $data[0]['products_users']['product_id'];
				$ref_user = $data[0]['products_users']['user_id'];
				$Refuser = $this->General->getUserDataFromId($ref_user);
				$endArr = explode(' ',$end);

				//get the product name
				$prodInfo = $this->General->getProductInfo($productId);
					
				if($data[0]['products_users']['active'] == 1){// code not expired
					$exists = $this->General->checkIfUserExists($mobile_number);
					if(!$exists){
						$new_user = 1;
						$user = $this->General->registerUser($mobile_number,REF_CODE_REG);
						$user = $user['User'];
					}
					else {
						$user = $this->General->getUserDataFromMobile($mobile_number);
					}
					$user_id = trim($user['id']);
						
					$data = $this->Group->query("SELECT id,sum(trial) as trials,sum(active) as actives FROM products_users WHERE product_id = $productId and user_id = $user_id group by product_id");
					if(!empty($data)){
						if($data['0']['0']['actives'] == 0){
							$sms = $this->General->createMessage("PRODUCT_EXPIRED",array($prodInfo['Product']['name'],$prodInfo['Product']['price'],"0".VIRTUAL_NUMBER));
						}
						else {
							$sms = $this->General->createMessage("PRODUCT_ALREADY_ACTIVE",array($prodInfo['Product']['name'],"0".VIRTUAL_NUMBER));
						}
						$root = 'template';
						$mail['subject'] = "REF Code Sent - Already using service";
						$mail['body'] = "User: $mobile_number, Referred By ". $Refuser['mobile'].", Product: ". $prodInfo['Product']['name'];
					}else{// activate the free trial

						// pkg activation
						$startDate = date('Y-m-d H:i:s');
						$endDate = date('Y-m-d H:i:s', strtotime( '+ 2 days'));
						$this->General->userCyclicPackageInsert($productId,null,$user_id,1);
						$this->General->makeOptIn247SMS($mobile_number,1);
						$this->Group->query("INSERT INTO products_users (product_id,user_id,active,trial,ref_user_id,start,end) VALUES ($productId,$user_id,1,1,'".$Refuser['id']."','$startDate','$endDate')");

						//sms to one who activates
						$sms = $this->General->createMessage("PRODUCT_SUBSCRIPTION_TRIAL",array($prodInfo['Product']['name'],date('d-M-Y',strtotime($endDate))));
						$root = 'template';
						//update end date of one who shares
						$dArr = explode(' ',$end);
						$dArr1 = explode('-',$dArr[0]);
						$d = mktime(0,0,0,$dArr1[1],$dArr1[2],$dArr1[0]);
						$end_date = date("Y-m-d",strtotime("+3 days",$d));
						$end_date1 = $end_date." ".$dArr[1];
						$this->Group->query("update products_users set end = '".$end_date1."' where id=".$recId);

						//sms who shares the code
						$userInfo = $this->General->getUserDataFromId($ref_user);
						$smsRef = "Your subscription of the product '".$prodInfo['Product']['name']."' is extended by 3 days\n";
						$smsRef .= "It will expire on " . date('d-M-Y',strtotime($end_date));
						//$this->General->sendMessage('91'.DUMMY_USER,$userInfo['mobile'],$smsRef,'sms');
						$this->General->sendMessage(SMS_SENDER,$userInfo['mobile'],$smsRef,'retail');

						//mail to admins
						$mail['subject'] = "2 Day Trial using ref code: Product " . $prodInfo['Product']['name'] . " subscribed";
						$mail['body'] = "User $mobile_number subscribed it. Referred by ".$userInfo['mobile'].". Ref code is ".$code;

						$done_flag = 1;
						$subscription = 1;
					}
						
				}else{
					$sms .= "Code: ".$code. " is expired.\n";
					$sms .= "To know more options SMS: TRY to 0".VIRTUAL_NUMBER;
					$mail['subject'] = "Offline Trial With Expired Code for " . $prodInfo['Product']['name'];
					$mail['body'] = "User $mobile_number. Ref code is ".$code;
				}
			}else{
				$sms .= "Code: ".$code. " is invalid.\n";
				$sms .= "To know more options SMS: TRY to 0".VIRTUAL_NUMBER;
				$mail['subject'] = "Offline Trial With Invalid Code";
				$mail['body'] = "User $mobile_number. Ref code is ".$code;
			}
		}else{
			$sms .= "Please SMS valid reference code. E.g. REF 3f5478\n";
			$sms .= "To know more options SMS: TRY to 0".VIRTUAL_NUMBER;
			$mail['subject'] = "Offline Trial Without Ref Code";
			$mail['body'] = "User $mobile_number";
		}
			
		$ret_array['sms'] = $sms;
		$ret_array['mail'] = $mail;
		$ret_array['done_flag'] = $done_flag;
		$ret_array['subscription'] = $subscription;
		$ret_array['new_user'] = $new_user;
			
		return $ret_array;

	}

	function retailerPackageSubscription($mobile_number, $msg, $param){
		$sms = '';
		$mail = array();

		$new_user = 0;
		$done_flag = 0;
		$subscription = 0;

		$codes = explode(" ",$msg);
		if(strtoupper($codes[0]) == 'UNSUB'){
			$code = strtoupper($codes[1]);
			$this->Product->recursive = -1;
			$packData = $this->Product->find('first', array('conditions' => array('Product.code' => $code)));
			if(empty($packData)){
				$sms = $this->General->createMessage("INVALID_CODE",array("0".VIRTUAL_NUMBER));
				$root = 'template';
			}
			else {
				$mobileData = $this->General->getUserDataFromMobile($mobile_number);
				$this->ProductsUser->recursive = -1;
				$userData = $this->ProductsUser->find('first', array('conditions' => array('ProductsUser.product_id' => $packData['Product']['id'],'ProductsUser.user_id' => $mobileData['id'],'ProductsUser.active' => '1')));
				if(empty($userData))
				{
					$sms = $this->General->createMessage("PRODUCT_ALREADY_INACTIVE",array("0".VIRTUAL_NUMBER));
					$root = 'template';
				}
				else {
					if($this->ProductsUser->updateAll(array('ProductsUser.active' => '0', 'ProductsUser.end' => "'".date('Y-m-d H:i:s')."'"), array('ProductsUser.id' => $userData['ProductsUser']['id']))){
						$packUserCount = $this->ProductsUser->find('count', array(
										'conditions' => array('ProductsUser.product_id' => $packData['Product']['id'], 'ProductsUser.active' => '0', 'ProductsUser.user_id' =>  $mobileData['id'])
						));

						$sms = $this->General->createMessage("PRODUCT_UNSUBSCRIBED",array($packData['Product']['name'],"0".VIRTUAL_NUMBER));
						$root = "template";
						$mail['subject'] = "Product Unsubscribed : ".$packData['Product']['name'];
						$mail['body'] = "User: ".$mobile_number;
					}
				}
			}
		}
		else if(strtoupper($codes[0]) == 'PLAYWIN'){
			$mobileData = $this->General->getUserDataFromMobile($mobile_number);
			$this->ProductsUser->recursive = -1;
			$userData = $this->ProductsUser->find('first', array('conditions' => array('ProductsUser.product_id IN ('.PLAYWIN_PKGS.')','ProductsUser.user_id' => $mobileData['id'],'ProductsUser.active' => '1')));
			if(!empty($userData)){
				$data = $this->ProductsUser->query("SELECT content FROM partners_data WHERE product_id=" . $userData['ProductsUser']['product_id'] . " AND (date='".date('Y-m-d')."' OR date = '".date('Y-m-d',strtotime('-1 day'))."') AND sent_flag = 1 ORDER BY modified desc LIMIT 1");
				$mail['subject'] = "PLAYWIN : ".$mobile_number;
				$mail['body'] = "$msg: ".$mobile_number;
				$sms = $data['0']['partners_data']['content'];
				$root = "playwin";
			}
			else {
				$mail['subject'] = "PLAYWIN : ".$mobile_number . " - Playwin Card Not Active";
				$mail['body'] = "$msg: ".$mobile_number;
				$sms = "Dear User,
Please buy a SMSTadka-Playwin Product from your playwin shop to get daily playwin results
To know your nearby shop, send SMS: SHOP to 0".VIRTUAL_NUMBER;
			}
		}
		else if(strtoupper($codes[0]) == 'IPOD'){
			$mail['subject'] = "IPOD : ".$mobile_number;
			$mail['body'] = "$msg: ".$mobile_number;
			$sms = "Sorry, the offer is expired now!!";
			$done_flag = 1;
		}
		else if(strtoupper($codes[0]) == 'LOCATION'){
			$mail['subject'] = "LOCATION : ".$mobile_number;
			$mail['body'] = "$msg: ".$mobile_number;
			$sms = $this->General->createMessage("DEFAULT_MESSAGE_ON_QUERY",array());
			$root = "template";
			$done_flag = 1;
		}
		else if(strtoupper($codes[0]) == 'SHOP'){
			$mail['subject'] = "$msg: ".$mobile_number;
			$mail['body'] = "$msg: ".$mobile_number;
			$sms = $this->General->createMessage("DEFAULT_MESSAGE_ON_QUERY",array());
			$root = "template";
			$done_flag = 1;
			$mobileData = $this->General->getUserDataFromMobile($mobile_number);
			if(!empty($mobileData)){
				$this->General->tagUser('SHOP',$mobileData['id']);
			}
		}
		else if(strtoupper($codes[0]) == 'PNR'){
			if(!isset($codes[1])){
				$mail['subject'] = "PNR Status Check: Not SENT PNR Number".$mobile_number;
				$mail['body'] = "$msg: ".$mobile_number;
				$sms = $this->General->createMessage("PNR_NOT_SENT_NUMBER",array());
				$root = "template";
			}
			else {
				$params['pnr'] = $codes[1];
				$params['mobile'] = $mobile_number;
				echo $this->requestAction('/pnrs/pnrCheck',array('pass' => array(json_encode($params))));
			}
		}
		else if(strtoupper($codes[0]) == 'SALE'){
			$retExists = $this->Group->query("SELECT id FROM vendors_retailers where mobile = '".$mobile_number."'");
			if(isset($retExists['0']['vendors_retailers']['id'])){
				$sale = $this->Group->query("SELECT sum(products.price) as sale FROM vendors_retailers join vendors_activations on (vendors_retailers.retailer_code = vendors_activations.vendor_retail_code) join products_users on (products_users.id = vendors_activations.productuser_id) join products on (products.id = products_users.product_id) WHERE vendors_retailers.mobile = '".$mobile_number."'");
				$sms = "Dear BP, Your total sale till date is Rs.".$sale['0']['0']['sale'];
				$mail['subject'] = "$msg: ".$mobile_number;
				$mail['body'] = "Total sale: Rs.".$sale['0']['0']['sale'];
			}else{
				$sms = $this->General->createMessage("DEFAULT_MESSAGE_ON_QUERY",array());
				$root = "template";
				$mail['subject'] = "$msg: ".$mobile_number;
				$mail['body'] = "Not registered mobile number. Call this number.";
			}
			$done_flag = 1;
			$mobileData = $this->General->getUserDataFromMobile($mobile_number);
			if(!empty($mobileData)){
				$this->General->tagUser('SALE',$mobileData['id']);
			}
		}
		else if(strtoupper($codes[0]) == "ACT"){
			if(count($codes) != 3){
				$sms = $this->General->createMessage("INVALID_CODE",array("0".VIRTUAL_NUMBER));
				$root = 'template';
				$mail['subject'] = "User Trying to Switch: Wrong Code Sent";
				$mail['body'] = "Mobile: $mobile_number. Message sent '$msg'\n";
			}
			else {
				$mobileData = $this->General->getUserDataFromMobile($mobile_number);
				if(empty($mobileData)){
					$sms = $this->General->createMessage("PRODUCT_NOT_SUBSCRIBED",array("0".VIRTUAL_NUMBER));
					$root = 'template';
					$mail['subject'] = "User Trying to Switch: Not registered";
					$mail['body'] = "Mobile: $mobile_number. Message sent '$msg'\n";
				}
				else {
					$url = SERVER_BACKUP . 'retailers/switchProduct';
					$data = array();
						
					$data['mobile'] = $mobile_number;
					$data['user_id'] = $mobileData['id'];
					$data['type'] = $codes[1];
					$data['option'] = $codes[2];
					$this->General->curl_post_async($url,$data);
				}
			}
		}
		else if(strtoupper($codes[0]) == "OPT"){
			if(count($codes) != 2 || strtoupper($codes[1]) != 'TV'){
				$sms = $this->General->createMessage("INVALID_CODE",array("0".VIRTUAL_NUMBER));
				$root = 'template';
				$mail['subject'] = "User Trying to get Options: Wrong Code Sent";
				$mail['body'] = "Mobile: $mobile_number. Message sent '$msg'\n";
			}
			else {
				$prod_data = $this->Group->query("SELECT id,name FROM products WHERE parent_id = ".WOI_PRODUCT." ORDER BY id asc");
				$mobileData = $this->General->getUserDataFromMobile($mobile_number);

				$i = 1;
				$sms = "*TeeVee Options*\n";
				foreach($prod_data as $prod){
					$sms .= "$i - " . $prod['products']['name']."\n";
					$i++;
				}
				$sms .= "To switch to 2nd option, send SMS: ACT TV 2 to 0".VIRTUAL_NUMBER;

				//$mail['subject'] = "User Trying to get Options";
				//$mail['body'] = "Mobile: $mobile_number. Message sent '$msg'\n";
			}
		}
		else {
			if(strtoupper($codes[0]) == 'TRY'){
				if(strtoupper($codes[1]) == "FUN" && isset($codes[2]) && $codes[2] == "18")
				$codes[1] = "FUN18";
				$msg = strtoupper($codes[1]);
			}
			if(!empty($msg)){
				$ret = $this->General->getCouponIfExists($msg);
				if(!empty($ret) && $ret['dry_flag'] == 3){//card reversal .. panel activations
					$sms = $this->General->createMessage("COUPON_NOT_VALID_DRY",array());
					$root = 'template';
					$mail['subject'] = "Card reversal: Coupon deactivated";
					$mail['body'] = "User $mobile_number sent a Code: $msg<br/>";
					$mail['body'] .= "Serial Number: ". $ret['serial_number'];
				}
				else if(!empty($ret) && $ret['dry_flag'] == 2){//dry stock .. for playwin
					$product_id = $ret['product_id'];
					$sms = $this->General->createMessage("COUPON_NOT_VALID_DRY",array());
					$root = 'template';
					$mail['subject'] = "Playwin Dry Stock: Coupon Not Activated Yet";
					
					if(in_array($product_id,explode(",",PAY1_RECHARGE_CARDS))){	
					//if($product_id == PAY1_RECHARGE_CARD_30 || $product_id == PAY1_RECHARGE_CARD_50 || $product_id == PAY1_RECHARGE_CARD_125 || $product_id == PAY1_RECHARGE_CARD_150){
						$mail['subject'] = "Pay1 Recharge Coupon Dry Stock: Coupon Not Activated Yet";
					}
					$mail['body'] = "User $mobile_number sent a Code: $msg<br/>";
					$mail['body'] .= "Serial Number: ". $ret['serial_number'];
				}
				else if(!empty($ret) && $ret['dry_flag'] == 1 && empty($ret['retailer_id'])){//dry stock .. not activated yet
					$sms = $this->General->createMessage("COUPON_NOT_VALID_DRY",array());
					$root = 'template';
					$mail['subject'] = "Dry Stock: Coupon Not Activated Yet";
					$mail['body'] = "User $mobile_number sent a Code: $msg";
					$mail['body'] .= "Serial Number: ". $ret['serial_number'];
				}
				else if(!empty($ret) && empty($ret['user_id'])){ //check if coupon exists & not sold already
					$product_id = $ret['product_id'];
					$done_flag = 1;
					$subscription = 1;
						
					$exists = $this->General->checkIfUserExists($mobile_number);
					if(!$exists){
						$new_user = 1;
						$user = $this->General->registerUser($mobile_number,RETAILER_REG);
						$user = $user['User'];
					}
					else {
						$user = $this->General->getUserDataFromMobile($mobile_number);
					}
						
					/*if(DND_FLAG && $user['dnd_flag'] == 1 && !TRANS_FLAG){
						$this->General->tagUser('DNDUSER',$user['id']);
						$this->General->mailToAdmins("User Registered in DND, NCPR: " . $user['ncpr_pref'], "NCPR Preference: ".$user['ncpr_pref'].". Call, Mobile Number: $mobile_number");
						$message = $mobile_number . " 1";
						$this->General->sendMessage(SMS_SENDER,array('9833032643'),$message,'sms');
						}*/
						
					$user_id = $user['id'];
					$prodInfo = $this->General->getProductInfo($product_id);
					$to_send = true;
						
					if($ret['trial_flag'] == 0){
						$true_flag = true;
						if($prodInfo['Product']['outside_flag'] == 1){
							$true_flag = false;
							$to_send = false;
							if($product_id == PNR_PRODUCT){
								if(empty($param)){
									$mail['subject'] = "PNR number not sent with the code";
									$mail['body'] = "User $mobile_number sent a Code: $msg";
									$sms = $this->General->createMessage("PNR_NUMBER_NOTSENT");
								}
								else {
									$random['random'] = mt_rand();
									$url = SITE_NAME . 'pnrs/retailPNR/' . $param . '/' . $mobile_number;
									$params['coupon_id'] = $ret['coupon_id'];
									$params['user_id'] = $user_id;
									$params['product_id'] = $product_id;
									$params['serial_number'] = $ret['serial_number'];
									$this->General->addAsynchronousCall($random['random'],'pnr','retailPNR',$params);
									$this->General->curl_post_async($url,$random);
								}
							}
							else if($product_id == RECHARGE_CARD_50){
								$true_flag = true;
								$balance = $this->General->balanceUpdate(50,'add',$user_id);
								$this->General->transactionUpdate(TRANS_RETAIL,50,null,null,$user_id);
								if($new_user == 1 || ($new_user == 0 && $user['login_count'] == 0)){
									$sms = $this->General->createMessage("SMSTADKA_RECHARGE",array($mobile_number,$user['syspass'],50,$balance));
								}
								else {
									$sms = $this->General->createMessage("SMSTADKA_RECHARGE_ONLINE",array(50,$balance));
								}
							}
							else if(in_array($product_id,explode(",",PAY1_RECHARGE_CARDS))){	
							//else if($product_id == PAY1_RECHARGE_CARD_30 || $product_id == PAY1_RECHARGE_CARD_50 || $product_id == PAY1_RECHARGE_CARD_125 || $product_id == PAY1_RECHARGE_CARD_150){
								$true_flag = true;
							}
							$root = "template";
						}
						if($true_flag){
							$this->General->executeCouponEnteries($ret['coupon_id'],$user_id,$mobile_number,$product_id,$ret['serial_number'],$prodInfo['Product']['outside_flag'],$param);
						}
					}
					else {
						$data = $this->Group->query("SELECT id,sum(trial) as trials,sum(active) as actives FROM products_users WHERE product_id = $product_id and user_id = $user_id group by product_id");
						if(empty($data)){
							$start = date('Y-m-d H:i:s');
							$end = date('Y-m-d H:i:s', strtotime( '+ 2 days'));
							$this->General->userCyclicPackageInsert($product_id,null,$user_id,1);
							$this->Group->query("INSERT INTO products_users (product_id,user_id,active,trial,start,end) VALUES ($product_id,$user_id,1,1,'$start','$end')");
								
							$sms = $this->General->createMessage("PRODUCT_SUBSCRIPTION_TRIAL",array($prodInfo['Product']['name'],date('d-M-Y',strtotime($end))));
							$root = 'template';
							$mail['subject'] = "2 Day Trial: Offline Product " . $prodInfo['Product']['name'] . " subscribed";
							$mail['body'] = "User $mobile_number subscribed it";
						}
						else{
							if($data['0']['0']['actives'] == 0){
								$sms = $this->General->createMessage("PRODUCT_EXPIRED",array($prodInfo['Product']['name'],$prodInfo['Product']['price'],"0".VIRTUAL_NUMBER));
							}
							else {
								$sms = $this->General->createMessage("PRODUCT_ALREADY_ACTIVE",array($prodInfo['Product']['name'],"0".VIRTUAL_NUMBER));
							}
							$root = 'template';
							$to_send = false;
							$mail['subject'] = "Offline Trial Sent Again: " . $prodInfo['Product']['name'];
							$mail['body'] = "User $mobile_number";
						}
					}
					if($to_send){
						$params = array();
						$params['receiver'] = $mobile_number;
						$params['product'] = $product_id;
						$this->General->addAsynchronousCall('-1','retailer','msgAfterSubscription',$params);
					}
				}
				else if(!empty($ret) && !empty($ret['user_id'])){
					$user = $this->General->getUserDataFromMobile($mobile_number);
					if ($user['id'] == $ret['user_id']){
						$product_id = $ret['product_id'];
						$prodInfo = $this->General->getProductInfo($product_id);
						$sms = $this->General->createMessage("PRODUCT_ALREADY_ACTIVE",array($prodInfo['Product']['name'],"0".VIRTUAL_NUMBER));
						$mail['subject'] = "SMS sent again";
						$mail['body'] = "User $mobile_number sent a Code: $msg";
					}
					else {
						$sms = $this->General->createMessage("COUPON_NOT_VALID",array($msg,"0".VIRTUAL_NUMBER));
						//$mail['subject'] = "Coupon Code not valid";
						$mail['body'] = "User $mobile_number sent a Code: $msg";
					}
					$root = 'template';
				}
				else {
					$sms = $this->General->createMessage("COUPON_NOT_VALID",array($msg,"0".VIRTUAL_NUMBER));
					$root = 'template';
						
					//$mail['subject'] = "Coupon Code not valid";
					$mail['body'] = "User $mobile_number sent a Code: $msg";
				}
			}
			else {
				$sms = 'Get 2 day Free trial.
SMS "TRY LOVE"  for Dil Vil Pyar Vyar
SMS "TRY FUN" for Fun Unlimited
SMS "TRY BLYWOOD" for Bollywood Masti
SMS "TRY CRICKET" for Insta-Cricket
SMS "TRY VIDEO" for Cool Videos
SMS "TRY NEWS" for Instant News
SMS "TRY FUN18" for Naughty Jokes
SMS "TRY BIBLE" for Holy Bible
to 0'.VIRTUAL_NUMBER.'.';
				$mail['body'] = "User $mobile_number Sent Try";
				$mail['subject'] = "Sent Try";
			}
		}
		$ret_array['sms'] = $sms;
		if(isset($root))
		$ret_array['root'] = $root;
		$ret_array['mail'] = $mail;
		$ret_array['done_flag'] = $done_flag;
		$ret_array['subscription'] = $subscription;
		$ret_array['new_user'] = $new_user;
		//echo "<pre>";print_r($ret_array);echo "</pre>";
		return $ret_array;
	}

	/*Function for extending subscription of packages*/
	function extendSubscription($days){
		$this->PackagesUser->updateAll(array('PackagesUser.end' => 'date_ADD(PackagesUser.end,INTERVAL '.$days.' DAY)'), array('PackagesUser.active' => '1','PackagesUser.start < "2010-09-23 12:00:00"'));
		$msg = "Dear User,
There was break in service due to Govt of India bulk SMS ban. 
We have extended your package subscriptions by $days days. Thanks for your patience!";

		$this->PackagesUser->recursive = -1;
		$data = $this->PackagesUser->find('all',array('fields' => array('distinct User.mobile'),'conditions' => array('PackagesUser.active' => '1'),
				'joins' => array(array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'inner',
					'conditions' => array('PackagesUser.user_id = User.id')
		))
		));

		foreach($data as $user){
			$this->General->sendMessage(SMS_SENDER,$user['User']['mobile'],$msg,'dnd');
		}
		$this->autoRender = false;
	}

	/*Function for dummy subscription of package*/
	/*function dummySubscription(){
		$this->Package->recursive = -1;
		$data = $this->Package->find('all',array('fields' => array('Package.id','Package.validity')));

		$this->User->recursive = -1;
		$userId = $this->User->find('first',array('fields' => array('User.id'),'condition' => array('User.mobiles' => DUMMY_USER)));
		foreach($data as $pck){
		$this->data['PackagesUser']['slot_id'] = 1;
		$this->data['PackagesUser']['package_id'] = $pck['Package']['id'];
		$this->data['PackagesUser']['user_id'] = $userId['User']['id'];
		$this->data['PackagesUser']['active'] = '1';
		$this->data['PackagesUser']['start'] = date('Y-m-d H:i:s');
		$this->data['PackagesUser']['end'] = date('Y-m-d H:i:s', strtotime( date('Y-m-d H:i:s') . '+'.$pck['Package']['validity'].' days'));
			
		$this->PackagesUser->create();
		$this->PackagesUser->save($this->data);
		}
		$this->autoRender = false;
		}*/

	function marketMessage(){
		set_time_limit(0);
		ini_set("memory_limit","-1");

		//$query = "SELECT mobile FROM `marketing_numbers` where dnd_flag = -1 and type= 'ICICI CREDIT CARD' limit 0,1000";
		//$query = "SELECT mobile FROM `marketing_numbers` where dnd_flag = 0 and limit 0,3000";
		//$query = "SELECT mobile FROM `marketing_numbers` where type='WOW CENTRAL' limit 0,2000";
		//$query = "SELECT mobile FROM `marketing_numbers` where mobile not in (select mobile from users_live) and type='ICICI CREDIT CARD,WOW CENTRAL' and city = 'MUM' limit 0,20000";
		$query = "SELECT mobile FROM `marketing_numbers` where mobile not in (select mobile from users_live) and type=',WOW CENTRAL' and city = 'MUM' limit 30000,1000";

		$this->User->recursive = -1;
		$data = $this->User->query($query);
		//echo count($data);

		$msg = "Are you a busy professional? No time to read newspaper, no access to fun in office?

Now get Cricket Live Score, Top Stories, Funny Jokes, Stock News Alerts, Health Tips n more on ur SMS

Register now on SMSTadka.com & subscribe to SMS packages. To register just give a Missed call to +917666888676";
		$mobiles = array();
		$i = 0;
		//$this->General->sendMessage(SMS_SENDER,'9819032643,9892471157',$msg,'dnd');
		//echo count($data);
		foreach($data as $mobile){
			$mobiles[] = $mobile['marketing_numbers']['mobile'];
			$i++;
			if($i % 100 == 0){
				$this->General->sendMessage(SMS_SENDER,implode(",",$mobiles),$msg,'dnd');
				//echo count($mobiles);
				//$this->printArray($mobiles);
				//$this->General->sendMessage("91".DUMMY_USER,implode(",",$mobiles),$msg,'market');
				$mobiles = array();
			}
		}

		//$this->General->sendMessage(SMS_SENDER,'9819032643',$msg,'dnd');
		//$this->General->sendMessage(SMS_SENDER,implode(",",$mobiles),$msg,'dnd');
		$this->autoRender = false;
	}

	/*Function for sending common message to users registered but not logged in even once*/
	function commonMessage1(){

		$query = "SELECT mobile,syspass FROM `users` WHERE verify > 0 and syspass is not null";

		$this->User->recursive = -1;
		$data = $this->User->query($query);

		foreach($data as $user){
			$msg = "Introducing SMSTadka.com
Get daily dose of SMS Fun on your mobile.
Subscribe packages of your choice (100+ options).
Your password is ". $user['users']['syspass'] .".
Login now to complete registration
Suggested Packages
*Santa banta Jokes
*Love Shayari
*Cricket Live Score
*Bollywood Tadka
*Beauty Tips
-www.smstadka.com";
			$this->General->sendMessage(SMS_SENDER,$user['users']['mobile'],$msg,'dnd');
		}
		$this->autoRender = false;
	}
	/*Function for sending common message to users registered but not logged in even once*/
	function commonMessage(){

		//$query = "SELECT mobile FROM `users` WHERE dnd_flag = 1 and id not in (select user_id from transactions where type = 1)";
		$query = "SELECT count(id) as counts, mobile_no FROM `gupshup_del_log` WHERE created > '2011-08-01 21:40:00' AND created < '2011-08-01 23:40:00' group by mobile_no having count(id) > 4 order by counts desc";
		//$query = "SELECT mobile FROM `users` WHERE dnd_flag = 0";

		$this->User->recursive = -1;
		$data = $this->User->query($query);
		$msg = "Dear User,
Due to some server issue, Last evening, you have received the playwin results multiple times on your phone.
We strongly apologies for the inconvenience. We will ensure the smooth services now onwards.
Thank you for choosing SMSTadka.";
		$mobiles = array();
		$i = 0;
		foreach($data as $mobile){
			$mobiles[] = $mobile['users']['mobile'];
			$i++;
			if($i % 100 == 0){
				$this->General->sendMessage(SMS_SENDER,implode(",",$mobiles),$msg,'market');
				$mobiles = array();
			}
		}
		$this->General->sendMessage(SMS_SENDER,implode(",",$mobiles),$msg,'market');
		//$this->General->sendMessage(SMS_SENDER,'9819032643,7666888676',$msg,'sms');
		$this->autoRender = false;
	}

	/*Function for sending common message to users registered, logged in as well but never subscribed a package*/
	function commonMessage2(){
		$this->lock();
		$query = "SELECT mobile, balance FROM `users` WHERE verify > 0 and Date(created) = '". date('Y-m-d', strtotime(' - 1 days'))."'";
		$this->User->recursive = -1;
		$data = $this->User->query($query);
		//$this->printArray($data); exit;
		foreach($data as $user){
			$msg = "SMS alerts on your mobile everyday.
You have trials to check some of the most popular packages like
*Santa Banta Jokes
*Funny Shayari
*Beauty Tips
*Cricket Live Score & lot more
Login to www.smstadka.com & choose your favorite package
";
			//$this->General->sendMessage(SMS_SENDER,'9819032643',$msg,'dnd');
			$this->General->sendMessage(SMS_SENDER,$user['users']['mobile'],$msg,'market');
		}
		$this->releaseLock();
		$this->autoRender = false;
	}

	/*Function for sending common message to users whose balance is <= 5*/
	function commonMessage3(){

		$query = "SELECT mobile,balance FROM `users` WHERE balance <= 5";

		$this->User->recursive = -1;
		$data = $this->User->query($query);

		foreach($data as $user){
			$msg = "Dear User,
Your SMSTadka Balance is ".$user['users']['balance'].".
If you wish to recharge your account just login into smstadka.com
You can recharge online via your ATM card, credit card or netbanking.
You can also use offline option of dropping a cheque in your nearest ICICI ATM.
SMS CHQ to 0".VIRTUAL_NUMBER." for more details.
		";
			$this->General->sendMessage(SMS_SENDER,$user['users']['mobile'],$msg,'dnd');
		}
		$this->autoRender = false;
	}

	function marketingMails(){
		$query = "SELECT email_id,id FROM `emails` where related_to = 'ashisharya.iitb@gmail.com' and id in (3) order by id limit 2";
		$datas = $this->Group->query($query);
		foreach($datas as $data){
			$this->requestAction('/groups/mailBySMTPAuth',array('pass' => array(json_encode($data))));
		}
		$this->autoRender = false;
	}

	function mailBySMTPAuth($pass){
		$pars = json_decode($pass,true);
		$params['id'] =  $this->objMd5->Encrypt($pars['emails']['id'], encKey);
		$this->Email->to = array($pars['emails']['email_id']);
		$this->Email->fromName = 'SMSTadka';
		$this->Email->params = $params;
		$this->Email->subject = 'Introducing SMSTadka';
		//$this->Email->attach($_SERVER['DOCUMENT_ROOT'] . '/favicon.ico');
		$result = $this->Email->send();
		if(strpos($result,'SMTP Error') === false){
			$this->Group->query("UPDATE emails set sent_flag = 1 where id = " .  $pars['emails']['id']);
		}
		else {
			//$this->General->mailToUsers('Email Marketing Stopped','Email Marketing Stopped',array('ashish@mindsarray.com'));
		}
	}

	function shootMail(){
		$params['id'] = -1;
		$params['body'] = $_REQUEST['body'];
		$this->Email->to = array($_REQUEST['email']);
		$this->Email->params = $params;
		$this->Email->fromName = $_REQUEST['from'];
		$this->Email->subject = $_REQUEST['sub'];

		if(isset($_REQUEST['username'])){
			$this->Email->smtpUserName = $_REQUEST['username'];
			$this->Email->from = $_REQUEST['username'];
			$this->Email->smtpPassword = $_REQUEST['password'];
		}
		else {
			$this->Email->smtpUserName = EMAIL_ID;
			$this->Email->from = EMAIL_ID;
			$this->Email->smtpPassword = PASSWORD;
		}
	  
		if(isset($_REQUEST['path'])){
			$paths = explode(',',$_REQUEST['path']);
			foreach($paths as $path){
				$this->Email->attach($path);
			}
		}
		$result = $this->Email->send();
		if(isset($_REQUEST['mobile'])){
			//$this->General->sendMessageViaSMSLane('',$_REQUEST['mobile'],$_REQUEST['message'],1);
			$this->General->sendMessage('',array($_REQUEST['mobile']),$_REQUEST['message'],'priority',3);
		}
		$this->autoRender = false;
	}

	/*   All crons are here */

	/* This is a cron function which will initialize all the package crons*/
	function initializePackageCrons($par1,$par2){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$data = $this->Package->find('all', array('fields' => array('Package.id','Package.name')));
			foreach($data as $pack){
				$this->General->initCronsPackage($pack['Package']['id']);
			}
			$this->releaseLock();
			$this->autoRender = false;
		}
	}

	/* This is a cron function which will run every 15 minutes and set flag to 1*/
	function setCronFlags($par1,$par2){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$time = date('Y-m-d H:i:s', strtotime(' + 1 hours'));
			$data = $this->Message->query("select * from crons_packages where (lefttogo > 0 or lefttogo is null) and (starttime is NULL or starttime < '" . $time . "')");
			foreach($data as $cron){
				if(empty($cron['crons_packages']['lefttogo']) || $cron['crons_packages']['lefttogo'] > 0){
					$count = $this->PackagesUser->find('count', array(
						'conditions' => array('PackagesUser.package_id' => $cron['crons_packages']['package_id'],'PackagesUser.active' => '1')));
					$count1 = $this->User->query("select products_users.* from products_users join products_packages on (products_packages.package_id=".$cron['crons_packages']['package_id']." and products_packages.product_id = products_users.product_id) where products_users.active = 1 limit 1");

					if($count + count($count1) > 0)
					$this->Message->query("update crons_packages set flag=1 where package_id = ".$cron['crons_packages']['package_id']);
					else {
						$this->Message->query("update crons_packages set flag=0 where package_id = ".$cron['crons_packages']['package_id']);
					}
				}
			}
			$this->releaseLock();
			$this->autoRender = false;
		}
	}

	/* SMSAchariya CRon for updating delivery report and resending of undelivered messages*/
	/*function resendMesCron($par1,$par2){

	if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
	$data = $this->User->query("select * from msgDelLog where status_flag = 0 and ret_val is not null and resent < 3"); //check status for other messages on api
		
	foreach($data as $cron){
	$status_str = file_get_contents("http://api.smstadka.com/apis/getdlr.php?username=".SMS_USER."&password=".SMS_PASS."&msgid=".$cron['msgDelLog']['ret_val']);
	$status_arr = explode(":", $status_str);
	$status = trim($status_arr[1]);
	if(strtolower($status) == "delivered"){//delivered
	//$this->User->query("update msgDelLog set status_flag=1,status='".$status."' where id=".$cron['msgDelLog']['id']); //update if delivered
	$this->User->query("delete from msgDelLog where id=".$cron['msgDelLog']['id']); //delete if delivered
	}
	else{
	$timestamp = time();
	$resend = true;
	if(strtolower($status) == "sent"){//Sent
	$resend = false;
	}
	if($cron['msgDelLog']['timestamp'] < $timestamp && !$resend){
	$resend = true;
	}
		
	if($resend){ // time to resend
	$resendData = $this->User->query("select logs.*,users.mobile from logs,users where logs.id =".$cron['msgDelLog']['log_id']." and logs.user_id = users.id ");
	$content = strip_tags($resendData[0]['logs']['content']);
	if($resendData[0]['logs']['package_id']){//tadka sent
	$this->General->sendMessage(SMS_SENDER,$resendData[0]['logs']['mobile'],$content,'res');
	}else{//user sent
	$this->General->sendMessage($resendData[0]['users']['mobile'],$resendData[0]['logs']['mobile'],$content,'res');
	}

	$this->User->query("update msgDelLog set status_flag=1,status='".$status."',resent=1 where id=".$cron['msgDelLog']['id']); //update status_flag
	}
	}

	}
		
	$data = $this->User->query("select * from msgDelLog where status_flag = 1 and status='Sent' and resent = 1 and timestamp < ".time()); //update status of messages whose delivery got failed from achariya or not delivered on time
	foreach($data as $cron){
	$status_str = file_get_contents("http://api.smstadka.com/apis/getdlr.php?username=".SMS_USER."&password=".SMS_PASS."&msgid=".$cron['msgDelLog']['ret_val']);
	$status_arr = explode(":", $status_str);
	$status = trim($status_arr[1]);
	$timestamp = time() + 14400;
	$this->User->query("update msgDelLog set status_flag=1,status='".$status."',timestamp=".$timestamp." where id=".$cron['msgDelLog']['id']); //update if delivered
	}
	}

	$this->autoRender = false;
	}
	*/
	/* FreeSMSAPI CRon for updating delivery report and resending of undelivered messages*/
	/*
	 function resendMesCron_FREESMSAPI($par1,$par2){

		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
		//for password
		$data = $this->User->query("select * from msgdel_freesmsapi where status is null and type='pass' and resent = 0"); //check status for other messages on api
		foreach($data as $cron){
		$status_ret = file_get_contents("http://s1.freesmsapi.com/bulksms/response?skey=".FREESMSAPI_KEY_ANURAG."&key=".$cron['msgdel_freesmsapi']['rid']);
		$array = $this->General->xml2array($status_ret);
		$status = $array['response']['contacts']['status'];
		if($status == 'DELIVERED'){//delivered
		$this->User->query("update msgdel_freesmsapi set status = 'DELIVERED' where id=".$cron['msgdel_freesmsapi']['id']); //update if delivered
		}else{
		$timestamp = time();
		//$this->User->query("update msgdel_freesmsapi set status = '$status' where id=".$cron['msgdel_freesmsapi']['id']); //update return value
		if($cron['msgdel_freesmsapi']['timestamp'] < $timestamp){ // time to resend
		$this->General->sendMessage(SMS_SENDER,$cron['msgdel_freesmsapi']['mobile'],$cron['msgdel_freesmsapi']['message'],'resPass');
		$this->User->query("update msgdel_freesmsapi set resent=resent+1 where id=".$cron['msgdel_freesmsapi']['id']); //update resend value and timestamp
		}
		}
		}
		}

		$this->autoRender = false;
		}
		*/
	/* 24X7SMS CRon for updating delivery report and resending of undelivered messages of app or package messages*/
	function resendMesCron_24X7SMS($par1,$par2,$order='desc'){ //runs every 10 mins
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$check_time = date('Y-m-d H:i:s',strtotime('-2 hours'));
			$check_time_max = date('Y-m-d H:i:s',strtotime('-2 minutes'));

			//$this->lock();
			$data = $this->User->query("select distinct ret_val,type,message,mobile,reply from msg247smsdellog USE INDEX (idx_date) where status_flag = 0 and resent = 0 AND created > '$check_time' AND created < '$check_time_max' AND date >= '".date('Y-m-d',strtotime('-1 days'))."' order by id $order limit 200"); //check status for other messages on api
			$tdk_array = array();
			$pay1_array = array();
			$promo_array = array();
			$datas = array();
			foreach($data as $cron){
				$ret = trim($cron['msg247smsdellog']['ret_val']);
				$datas[$ret] = $cron;
					
				if(trim($cron['msg247smsdellog']['type']) == 4){
					$pay1_array[] = $ret;
				}
				else if(trim($cron['msg247smsdellog']['type']) == 2){
					$promo_array[] = $ret;
				}
				else if(trim($cron['msg247smsdellog']['type']) == 1){
					$tdk_array[] = $ret;
				}
			}

			$arr_pay1 = array_chunk($pay1_array,10);
			$arr_tdk = array_chunk($tdk_array,10);
			$arr_promo = array_chunk($promo_array,10);

			foreach($arr_pay1 as $arr){
				$this->process247Rest($arr,$datas,4);
			}
			
			foreach($arr_tdk as $arr){
				$this->process247Rest($arr,$datas,1);
			}
			
			foreach($arr_promo as $arr){
				$this->process247Rest($arr,$datas,2);
			}
			
			//$this->releaseLock();
		}

		$this->autoRender = false;
	}

	function process247Rest($ids,$data,$type){
		$query = "http://smsapi.24x7sms.com/api_1.0/GetReportMsgID.aspx?";
		$adm = "EmailID=".SMS_USER_24X7SMS."&Password=".SMS_PASS_24X7SMS;
		if($type == 4 || $type == 2){
			$adm = "EmailID=".PAYONE_SMS_USER_24X7SMS."&Password=".PAYONE_SMS_PASS_24X7SMS;
		}
		$adm .= "&SenderID=PAYONE&MsgID=".urlencode(implode(",",$ids));
		
		$url = $query.$adm;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST,0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$status_str = curl_exec($ch) ;
		if(empty($status_str))continue;

		$status_arr = explode("<br>",$status_str);
	 	$array = array();

	 	foreach($status_arr as $sts){
	 		$sts = trim(strip_tags($sts));
	 		if(!empty($sts)){
	 			$sts = explode(":",$sts);
	 			$ret_val = trim($sts[0]);
	 			$mobile = trim($sts[1]);
	 			$status = strtolower(trim(strip_tags($sts[2])));
	 			$resend = false;
	 			
	 			if(in_array($status,array("delivrd","deliverd","delivered"))){//delivered
	 				$this->User->query("update msg247smsdellog set status_flag=1,status='DELIVERED' where ret_val='$ret_val'");
	 			}
	 			else if(in_array($status,array("undeliverable","expired","rejected","block","dnd reject"))){//failed
	 				if($status == "block" && $type == 4){
	 					$this->General->makeOptIn247SMS($mobile,4);
	 				}
	 				
	 				if($type == 4){
	 					$this->General->sendMessage(SMS_SENDER,$mobile,$data[$ret_val]['msg247smsdellog']['message'],'fail-24x7sms-payone',$data[$ret_val]['msg247smsdellog']['reply']);
	 				}
	 				else if($type == 1){
	 					$this->General->sendMessage(SMS_SENDER,$mobile,$data[$ret_val]['msg247smsdellog']['message'],'fail-24x7sms',$data[$ret_val]['msg247smsdellog']['reply']);
	 				}
	 				$timestamp = time();
	 				$this->User->query("update msg247smsdellog set status_flag=1,status='".$status."',resent=1,timestamp='$timestamp' where ret_val='$ret_val'");
	 			}
	 		}
	 	}
	}

	function resendMesCron_SMSLane($par1,$par2){ //runs every half an hour
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$query = "http://api2.infobip.com/api/dlrpull?user=mindsarr&password=WtzR!bG?";
			$status = file_get_contents($query);
			$dom = new DOMDocument();
			$dom->loadXML($status);
			$xpath = new domxpath($dom);
			$reports = $xpath->query("/DeliveryReport/message");
			foreach($reports as $node){
				$id = trim($node->getAttribute('id'));
				//$sent = trim($node->getAttribute('sentdate'));//2011/12/16 07:56:41
				$done_date = trim($node->getAttribute('donedate'));//2011/12/16 07:56:45
				$status = trim($node->getAttribute('status'));//DELIVERED
				$done_date = date('Y-m-d H:i:s',strtotime($done_date.'+ 4 hours 30 minutes'));
				$this->Group->query("UPDATE smslanedellog SET status_flag = 1,status= '$status',delivered='$done_date' WHERE ret_val='$id'");
				if($status == "NOT_DELIVERED"){
					$data = $this->Group->query("SELECT * FROM smslanedellog WHERE ret_val='$id'");
					$this->General->addNonSentMessages(SMS_SENDER,$data['0']['smslanedellog']['mobile'],$data['0']['smslanedellog']['message'],'sms',null);
				}
			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	/* 24X7SMS CRon for updating delivery report and resending of passwords*/
	function resendMesCron_24X7SMS_PASS($par1,$par2){// runs every 5 minute

		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$data = $this->User->query("select * from msg247smsdellog where status_flag = 0 and type = 1 and resent = 0 AND date >= '".date('Y-m-d',strtotime('-1 days'))."'"); //check status for password on api
				
			foreach($data as $cron){
				$ret_val = $cron['msg247smsdellog']['ret_val'];
				//$str = "http://opt.smsapi.org/GetReportsMessageID.aspx?UserName=".SMS_USER_24X7SMS."&Password=".SMS_PASS_24X7SMS."&MessageID=".$ret_val;
				$str = "http://smsapi.24x7sms.com/api_1.0/GetReportMsgID.aspx?EmailID=".SMS_USER_24X7SMS."&Password=".SMS_PASS_24X7SMS."&MsgID=".$ret_val;
				//$adm = "EmailID=".SMS_USER_24X7SMS."&Password=".SMS_PASS_24X7SMS."&MsgID=".$ret_val;
				//$status_str = file_get_contents($str);
				$url = $query.$adm;
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST,0);
				//curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
				curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
				curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				curl_setopt($ch, CURLOPT_TIMEOUT, 100);
				$status_str = curl_exec($ch) ;

				$sts = explode("<br>",$status_str);
				$status = trim(strip_tags($sts[2]));
				if(strtolower($status) == "deliverd" || strtolower($status) == "delivered" || strtolower($status) == "deliveredmsg"){//delivered
					$this->User->query("update msg247smsdellog set status='".$status."', status_flag =1 where id=".$cron['msg247smsdellog']['id']); //delete if delivered
				}
				else{
					$timestamp = time();
					$resend = false;
						
					if(empty($status) && $cron['msg247smsdellog']['timestamp'] < $timestamp){
						$resend = true;
					}
					else if(!empty($status) && strtolower($status) != "delivered" && strtolower($status) != "deliverd" && strtolower($status) != "deliveredmsg"){
						$resend = true;
					}
						
					if($resend){ // time to resend
						$content = strip_tags($cron['msg247smsdellog']['message']);
						$this->General->sendMessage(SMS_SENDER,$cron['msg247smsdellog']['mobile'],$content,'resPass');
						$this->User->query("update msg247smsdellog set status_flag=1,status='".$status."',resent=1 where id=".$cron['msg247smsdellog']['id']); //update status_flag
					}
				}
			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	/* GupShup delivery CRon for resending of undelivered messages*/
	function resendMesCron_GUPSHUP($par1,$par2){ //runs every 10 mins

		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$time = date('Y-m-d H:i:s', strtotime(' - 3 hours'));
			$data = $this->Group->query("select message,mobile_no,id,status,created,priority_root from gupshup_del_log where resend_flag = 1 and status != 'SUCCESS' and status is not null and created >= '$time'"); //check status for other messages on api
				
			foreach($data as $cron){
				$ret_val = trim($cron['gupshup_del_log']['status']);
				$created = trim($cron['gupshup_del_log']['created']);
				if($ret_val != 'SUCCESS' && $created >= $time){
					if($cron['gupshup_del_log']['priority_root'] == 'tadka'){
						$root = "fail-gupshup-tadka";
					}
					else if($cron['gupshup_del_log']['priority_root'] == 'high'){
						$root = "fail-gupshup-payone";
					}
					else {
						$root = "fail-gupshup";
					}
					$this->General->sendMessage(SMS_SENDER,$cron['gupshup_del_log']['mobile_no'],strip_tags($cron['gupshup_del_log']['message']),$root);
				}

				$this->Group->query("UPDATE gupshup_del_log SET resend_flag = 2 WHERE id = " . $cron['gupshup_del_log']['id']);
			}
			$this->releaseLock();
		}

		$this->autoRender = false;
	}

	/*function twitterCron($par1,$par2) {
	 if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORDs){
	 $result = shell_exec('curl -u '.TWITTER_USERNAME.':'.TWITTER_PASSWORD.' http://api.twitter.com/1/statuses/home_timeline.xml');

	 $arr = $this->General->xml2array($result, 1, 'tag');

	 $i = 0;
	 foreach($arr['statuses']['status'] as $arr1){

	 $data = $this->Group->query("select id from twitts where twitt_id='".$arr1['id']."'");

	 if(empty($data)){
	 $this->Group->query("INSERT INTO twitts (twitter_id,twitt_id,twitt,created)  VALUES (".$arr1['user']['id'].",".$arr1['id'].",'".addslashes($arr1['text'])."','".date('Y-m-d H:i:s', strtotime($arr1['created_at']))."')");
	 $this->General->sendTweet($arr1['user']['id'],$arr1['text']);

	 }
	 }

	 }
	 $this->autoRender = false;
	 }
	 */
	/* CRon for twitter messages*/

	function twitterCron($par1,$par2) {
		//Configure::write('debug', 2);
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			//$this->lock();
			if(!TRANS_FLAG && !$this->General->checkTimeSlot()){
				$this->autoRender = false;
				$this->releaseLock();
				return;
			}
			App::import('vendor', 'EpiCurl', array('file' => 'EpiCurl.php'));
			App::import('vendor', 'EpiOAuth', array('file' => 'EpiOAuth.php'));
			App::import('vendor', 'EpiTwitter', array('file' => 'EpiTwitter.php'));
				
			$twitterObj = new EpiTwitter(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET);
			$twitterObj->setToken(TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_TOKEN_SECRET);
				
			$hometimeline = $twitterObj->get_statusesHome_timeline();
			$twitter_status = new SimpleXMLElement($hometimeline->responseText);
				
			foreach($twitter_status as $status){

				$data = $this->Group->query("select id from twitts where twitt_id='".$status->id."'");
				if(empty($data)){
					$this->Group->query("INSERT INTO twitts (twitter_id,twitt_id,twitt,created)  VALUES (".$status->user->id.",".$status->id.",'".addslashes($status->text)."','".date('Y-m-d H:i:s', strtotime($status->created_at))."')");
					$this->General->sendTweet($status->user->id,$status->text);
				}
			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	/* CRon for unsubscribing packages */

	function unsubscribeCron($par1,$par2) {
		set_time_limit(0);
		ini_set("memory_limit","-1");

		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$this->PackagesUser->recursive = -1;
			$time = date('Y-m-d', strtotime('- 1 days'));
			$userQ = $this->PackagesUser->find('all',array(
			'fields' => array('distinct PackagesUser.user_id'),
			'conditions' => array('PackagesUser.active' => '1','Date(PackagesUser.end) <= "' .$time.'"')
				
			));
			foreach($userQ as $userD){
				$data1 = $this->PackagesUser->find('all',array(
					'fields' => array('PackagesUser.id','PackagesUser.trial_flag','User.mobile','Package.name','Package.price','Package.id','Package.validity','Package.code'),
					'conditions' => array('PackagesUser.active' => '1','Date(PackagesUser.end) <= "' .$time.'"','PackagesUser.user_id' => $userD['PackagesUser']['user_id']),
					'joins' => array(
				array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'inner',
								'conditions'=> array('PackagesUser.user_id = User.id')
				),
				array(
								'table' => 'packages',
								'alias' => 'Package',
								'type' => 'inner',
								'conditions'=> array('PackagesUser.package_id = Package.id','Package.toshow = 1')
				)
				)));
				$packCon = array();
				$packDiscon = array();
				$packTryCon = array();
				$userId = $userD['PackagesUser']['user_id'];
				//$this->printArray($data1); exit;
				foreach($data1 as $user){

					$balance = $this->General->getBalance($userId) - $user['Package']['price'];
					echo $userId . " % " . $balance;
					$this->PackagesUser->updateAll(array('PackagesUser.active' => '0', 'PackagesUser.end' => "'".date('Y-m-d H:i:s')."'"),
					array('PackagesUser.id' => $user['PackagesUser']['id']));

					//$this->General->makeOptOut247SMS($user['User']['mobile'],1);
						
					if($balance >= 0 && $user['PackagesUser']['trial_flag'] == 0){ //subscribe it again

						$this->data['PackagesUser']['slot_id'] = '1';
						$this->data['PackagesUser']['package_id'] = $user['Package']['id'];
						$this->data['PackagesUser']['user_id'] = $userId;
						$this->data['PackagesUser']['active'] = '1';
						$this->data['PackagesUser']['start'] = date('Y-m-d H:i:s');
						$this->data['PackagesUser']['end'] = date('Y-m-d H:i:s', strtotime( '+ '.$user['Package']['validity'].' days'));
							
						$this->PackagesUser->create();
						if ($this->PackagesUser->save($this->data)) {
							$this->General->makeOptIn247SMS($user['User']['mobile'],1);
							//make an entry in users table for balance update
							$bal = $this->General->balanceUpdate($user['Package']['price'],'subtract',$userId);

							//make an entry in transactions table
							$this->General->transactionUpdate(TRANS_ADMIN_DEBIT,$user['Package']['price'],$user['Package']['id'],null,$userId);

							$packCon[] = $user['Package']['name'];

							$mail_body = $user['User']['mobile'] . " subscription is continued for the package " . $user['Package']['name'];
							//$this->General->mailToAdmins("Subscription Continue ", $mail_body);
						}else{
							$bal = $this->General->getBalance($userId);
						}
					}
					else if($user['PackagesUser']['trial_flag'] == 1){
						if($balance >= 0){
							$msg = "Thank you for trying the package ".$user['Package']['name']."
Your balance is Rs.".$this->General->getBalance($userId).".
To subscribe this package, Send ". $user['Package']['code'] . " to 0".VIRTUAL_NUMBER."
You can also recharge/top-up by login into SMSTadka.com.";
							$this->General->sendMessage(SMS_SENDER,array($user['User']['mobile']),$msg,'market');
						}else {
							$packTryCon[] = $user['Package']['name'];
						}
						$bal = $this->General->getBalance($userId);
					}
					else { //unsubscribe it
						$packDiscon[] = $user['Package']['name'];
						$bal = $this->General->getBalance($userId);
					}
				}
					
				//$bal = $this->General->getBalance($userId);
				if(count($packCon)> 0){
					echo $msg = $this->General->createMessage("PACKAGE_CONTINUED",array(implode(", ",$packCon),$bal,"0".VIRTUAL_NUMBER));
					//$this->General->sendMessage(SMS_SENDER,array($user['User']['mobile']),$msg,'template');
				}
				if(count($packTryCon) > 0){
					echo $msg = "Thank you for trying package(s) ".implode(", ",$packTryCon).
								"\nContinue to enjoy our services by recharging your SMSTadka account.".
								"\nTo know more Send HELP to 0".VIRTUAL_NUMBER." or login to SMSTadka.com.";
					//$this->General->sendMessage(SMS_SENDER,$user['User']['mobile'],$msg,'market');
				}
					
				if(count($packDiscon)> 0){
					echo $msg = "Sorry, your subscription for the package(s) ".implode(", ",$packDiscon)." is discontinued due to insufficient balance.".
								"\nYour balance is Rs.".$bal.
								"\nYou may continue, just recharge your account and re-subscribe for the package(s) by login into your SMSTadka.com account.";		
					//$this->General->sendMessage(SMS_SENDER,array($user['User']['mobile']),$msg,'pac');
				}
			}

			$time = date('Y-m-d', strtotime('+ 2 days'));
			$data2 = $this->PackagesUser->find('all',array(
			'fields' => array('PackagesUser.user_id','User.mobile','User.balance','GROUP_CONCAT(" ",Package.name) as names'),
			'conditions' => array('PackagesUser.active' => '1','PackagesUser.trial_flag' => '0','Date(PackagesUser.end) = "'.$time . '"'),
			'joins' => array(
			array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.user_id = User.id')
			),
			array(
						'table' => 'packages',
						'alias' => 'Package',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.package_id = Package.id')
			)),
			'group' => 'User.id'	
			));
				
			foreach($data2 as $user){
				//about to expire
				$msg = "Dear User,
Following package(s) ".$user['0']['names']. " will expire after 2 days.
Your packages will be continued as per your balance.
Your balance is Rs ".$user['User']['balance'] ."
Login to smstadka.com to recharge/unsubscribe";
				$this->General->sendMessage('',array($user['User']['mobile']),$msg,'pac');
			}
				
			$time = date('Y-m-d', strtotime('+ 1 days'));
				
			$data3 = $this->PackagesUser->find('all',array(
			'fields' => array('PackagesUser.user_id','User.mobile','User.balance','GROUP_CONCAT(" ",Package.name) as names'),
			'conditions' => array('PackagesUser.active' => '1','PackagesUser.trial_flag' => '1','Date(PackagesUser.end) = "'.$time . '"'),
			'joins' => array(
			array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.user_id = User.id')
			),
			array(
						'table' => 'packages',
						'alias' => 'Package',
						'type' => 'inner',
						'conditions'=> array('PackagesUser.package_id = Package.id')
			)),
			'group' => 'User.id'	
			));
			foreach($data3 as $user){
				//about to expire
				$msg = "Dear User,".
						"\nFollowing trial package(s) ".$user['0']['names']. " will expire after 1 day.".
						"\nIf you like the service, Login into smstadka.com for monthly subscriptions.".
						"\nYour balance is Rs ".$user['User']['balance'];
				$this->General->sendMessage('',$user['User']['mobile'],$msg,'market');
			}
			$this->releaseLock();
		}

		$this->autoRender = false;
	}

	/*Cron for clearing database*/
	function cleanDatabase($par1,$par2){//Everyday 4 AM
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			//logs (last 6 months)
			$this->Group->query("DELETE FROM logs WHERE Date(timestamp) < DATE_SUB(CURDATE(),INTERVAL 180 DAY)");
				
			//gupshup_del_log (last 2 months)
			$this->Group->query("DELETE FROM gupshup_del_log WHERE Date(created) < DATE_SUB(CURDATE(),INTERVAL 60 DAY)");
				
			//msg247smsdellog (last 2 months)
			$this->Group->query("DELETE FROM msg247smsdellog WHERE date < DATE_SUB(CURDATE(),INTERVAL 60 DAY)");
				
			//msg247smsdellog (last 2 months)
			$this->Group->query("DELETE FROM msgTataSmsDelLog WHERE date < DATE_SUB(CURDATE(),INTERVAL 60 DAY)");
			
			//msg247smsdellog (last 2 months)
			$this->Group->query("DELETE FROM delivery_vfirst WHERE date < DATE_SUB(CURDATE(),INTERVAL 60 DAY)");
				
			//msg247smsdellog (last 2 months)
			$this->Group->query("DELETE FROM delivery_routesms WHERE date < DATE_SUB(CURDATE(),INTERVAL 60 DAY)");
				
			//modem (last 2 months)
			$this->Group->query("DELETE FROM delivery_modem WHERE date < DATE_SUB(CURDATE(),INTERVAL 60 DAY)");
				
			//rss_feeds (last 1 month)
			$this->Group->query("DELETE FROM rss_feeds WHERE Date(created) < DATE_SUB(CURDATE(),INTERVAL 30 DAY)");
				
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	function scripts(){
		$this->render('/groups/scripts');
	}
	function paymentUsersScript(){
		$query = "SELECT mobile,count(user_id) as num_trials,group_concat(flag) as status,max(start_time) as lastPaymentTry, sum(if(flag = 'SUCCESS', amount, 0)) as TotalRecharge  FROM `payment`,users where users.id = user_id and group_id = 1 group by user_id order by lastPaymentTry desc;";
		$data = $this->Group->query($query);
		$this->set('type','Payment Users');
		$this->set('data',$data);
		$this->render('/groups/list_users');
	}

	function getUserInfo($mobile){

		$user_data = $this->User->find('first',array('conditions' => array('mobile' => $mobile)));
		$mobileDetails = $this->General->getMobileDetails($mobile);
		$payment_details = $this->User->query("SELECT amount,start_time,flag FROM `payment`,users where users.id = user_id and users.mobile='".$mobile."' order by payment.id desc");
		//nisha start
		//$userChkForGroup=$_SESSION[][][]
		$groupId=$user_data['User']['group_id'];
		$uId=$user_data['User']['id'];
		switch($groupId)
		{
			Case 1: $role="MEMBER";break;
			Case 2: $role="ADMIN";break;
			Case 3: $role="AUTHOR";break;
			Case 4: $role="SUPER DISTRIBUTOR";break;
			Case 5: $role="DISTRIBUTOR";break;
			Case 6: $role="RETAILER";break;
		}
		//  echo $name.":  ".$mobile."  is  ".$role;
		//nisha end


		$comments = $this->User->query("SELECT distinct users.name,comments.comment,comments.created from users, comments left join users as u1 ON (comments.itemid= u1.id) where (u1.mobile=$mobile OR comments.mobile='$mobile') and comments.itemtype=0 and users.id = comments.user_id order by comments.id desc");
		$logs=$this->User->query("select u.name,l.description,l.modified from users as u,logs_cc as l where l.user_id=$uId and  u.id=l.parent_user_id order by modified desc");

		//$comments = $this->User->query("SELECT distinct users.name,comments.comment,comments.created from users, comments where ((comments.itemid= users.id and users.mobile=$mobile) OR comments.mobile='$mobile') and comments.itemtype=0 and users.id = comments.user_id and u1.id= users.id order by comments.id desc");
		$products = $this->ProductsUser->find("all",array('fields' => array('ProductsUser.*,Product.name'), 'conditions' => array('ProductsUser.user_id' => $user_data['User']['id']), 'order' => array('ProductsUser.start desc')));
		$retailers = $this->RetailersCoupon->find("all",array('fields' => array('distinct Retailer.*'), 'conditions' => array('RetailersCoupon.user_id' => $user_data['User']['id'],'RetailersCoupon.user_id is not null')));
		$retailers_ext = $this->User->query("SELECT distinct Retailer.* FROM shop_transactions,retailers as Retailer,products_users WHERE shop_transactions.type = " . RETAILER_ACTIVATION . "  AND products_users.id = productuser_id AND products_users.user_id = " . $user_data['User']['id'] . " AND ref1_id = Retailer.id AND Retailer.toshow = 1");

		$msgs = $this->Group->query("SELECT message_in,timestamp FROM log_smsrequest WHERE mobile = '$mobile' order by timestamp desc");

		$tags = $this->General->getUserTags($user_data['User']['id']);
		$this->set('tags',$tags);
		$this->set('products',$products);
		$this->set('mobile',$mobile);
		$this->set('retailers',$retailers);
		$this->set('retailers_ext',$retailers_ext);
		$this->set('msgs',$msgs);
		$this->set('comments',$comments);
		$this->set('logs',$logs);
		$this->set('user_data',$user_data);
		$this->set('mobileDetails',$mobileDetails);
		$this->set('payment_details',$payment_details);
		$this->set('role',$role);
		$states = $this->Group->query("SELECT id,name FROM locator_state ORDER BY name asc");
		//nisha start

		if(!empty($uId)){
			$retailerDataResult=$this->User->query("select mobile,name,email,shopname,area_id,city,state,address,pin from retailers where user_id=$uId");
			if(!empty($retailerDataResult))
			{
				$alreadyPresent=1;
				$this->set('retailerData', $retailerDataResult);
				//get state id from state name
				$stateToIdResult=$this->User->query("select id from locator_state where name='".$retailerDataResult['0']['retailers']['state']."'");
				//get city id from city name
				$cityToIdResult=$this->User->query("select id from locator_city where name='".$retailerDataResult['0']['retailers']['city']."'");
				//get city list from state id
				$cityListResult=$this->User->query("select id,name from locator_city where state_id='".$stateToIdResult['0']['locator_state']['id']."'");
				$areaListResult=$this->User->query("select id,name from locator_area where city_id='".$cityToIdResult['0']['locator_city']['id']."'");

				$this->set('cities',$cityListResult);
				$this->set('areas', $areaListResult);
				$this->set('state_id', $stateToIdResult['0']['locator_state']['id']);
				$this->set('city_id', $cityToIdResult['0']['locator_city']['id']);
				$this->set('area_id', $retailerDataResult['0']['retailers']['area_id']);
				//$this->set('address',retailerData[7]);
			}
			else{
				$alreadyPresent=0;
			}
		}
		$this->set('states',$states);
		$this->render('/groups/user_info');
	}

	function userInfo($mobile,$retCode){
		if($this->Session->read('Auth.User.group_id') == ADMIN){
			$user_data = $this->User->find('first',array('conditions' => array('mobile' => $mobile)));
			$mobileDetails = $this->General->getMobileDetails($mobile);
			//$payment_details = $this->User->query("SELECT amount,start_time,flag FROM `payment`,users where users.id = user_id and users.mobile='".$mobile."' order by payment.id desc");
				
			$comments = $this->User->query("SELECT users.name,comments.comment,comments.created from users as u1, users, comments where comments.itemid= u1.id and u1.mobile=$mobile and comments.itemtype=0 and users.id = comments.user_id order by comments.id desc");
			$products = $this->ProductsUser->find("all",array('fields' => array('ProductsUser.*,Product.name'), 'conditions' => array('ProductsUser.user_id' => $user_data['User']['id']), 'order' => array('ProductsUser.start desc')));
			//$retailers = $this->RetailersCoupon->find("all",array('fields' => array('distinct Retailer.*'), 'conditions' => array('RetailersCoupon.user_id' => $user_data['User']['id'],'RetailersCoupon.user_id is not null')));
			$msgs = $this->Group->query("SELECT message_in,timestamp FROM log_smsrequest WHERE mobile = $mobile order by timestamp desc");
			$tags = $this->General->getUserTags($user_data['User']['id']);
				
			$validSmartUser = $this->Group->query("select id from vendors_activations where productuser_id in (SELECT id FROM products_users WHERE user_id = '".$user_data['User']['id']."') and vendor_id = 5") ;
				
			if(count($validSmartUser) < 1)header("location: /retailers/smartshop");
				
				
			$this->set('products',$products);
			$this->set('retCode',$retCode);
			$this->set('msgs',$msgs);
			$this->set('comments',$comments);
			$this->set('user_data',$user_data);
			$this->set('mobileDetails',$mobileDetails);
			$this->set('tags',$tags);
			$this->render('/groups/userinfo');
		}else{
			$this->redirect('/users/view');
		}
	}

	function getPackInfo($id){
		$this->Package->recursive = -1;
		$package_data = $this->Package->find('first',array('conditions' => array('id' => $id)));
		$this->Log->recursive = -1;
		$unique = $this->Log->find('all',array('fields' => array('distinct content'),'conditions' => array('package_id' => $id)));
		$total = $this->Log->find('count',array('conditions' => array('package_id' => $id)));
		$this->PackagesUser->recursive = -1;
		$active_users = $this->PackagesUser->query("select count(active) as cactive,user_id,users.mobile,min(start) as start,max(end) as end from packages_users left join users on (user_id = users.id) where package_id=$id group by user_id having sum(active) = 1 order by end desc");
		$inactive_users = $this->PackagesUser->query("select count(active) as cactive,user_id,users.mobile,users.balance,min(start) as start,max(end) as end from packages_users left join users on (user_id = users.id) where package_id=$id group by user_id having sum(active) = 0 order by end desc");

		$this->set('unique',count($unique));
		$this->set('total',$total);
		$this->set('pack_data',$package_data);
		$this->set('active_users',$active_users);
		$this->set('inactive_users',$inactive_users);
		$this->render('/groups/pack_info');
	}

	function getAlertsInfo($controller){
		$table = ucfirst($controller);
		if($controller == 'pnr' || $controller == 'stock'){
			$active_data = $this->$table->find('all',array('conditions' => array('status_flag' => 1), 'order' => $table.'.id desc'));
			$inactive_data = $this->$table->find('all',array('conditions' => array('status_flag' => 0), 'order' => $table.'.id desc'));
		}
		else if($controller == 'reminder'){
			$active_data = $this->$table->find('all',array('conditions' => array('status' => 0), 'order' => $table.'.id desc'));
			$inactive_data = $this->$table->find('all',array('conditions' => array('status' => 1), 'order' => $table.'.id desc'));
		}

		$app_info = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $controller)));
		$this->set('active_data', $active_data);
		$this->set('inactive_data', $inactive_data);
		$this->set('app_info', $app_info);
		$this->render('/groups/alert_info');
	}

	function getLastComments(){
		$data = $this->Group->query("SELECT comment,comments.created,users.mobile from comments inner join users on (users.id = comments.itemid) where comments.id in (SELECT max(id) from comments group by itemid) and comments.itemtype= 0 order by comments.id desc");
		$this->set('data',$data);
		$this->render('comments');
	}

	function dailyReport($date){
		$this->User->recursive = -1;

		$total_users_registered = $this->User->find('count');

		$users_registered_Loggedin = $this->User->find('all',array('fields' => array('User.mobile','User.balance'), 'conditions' => array('Date(created)' => $date, 'verify <= 0')));
		$users_registered_notLoggedin = $this->User->find('all', array('fields' => array('User.mobile'),'conditions' => array('Date(created)' => $date, 'verify > 0')));
		$users_registered_viaMisscall = $this->User->find('all',array('fields' => array('User.mobile'), 'conditions' => array('Date(created)' => $date, 'verify = -1')));


		$users_visited = $this->User->find('count',array('conditions' => array('Date(modified)' => $date, 'group_id != 2')));
		$returning_users = $this->User->find('count',array('conditions' => array('Date(modified)' => $date, 'Date(created) != "'.$date.'"', 'group_id != 2')));

		$this->PackagesUser->recursive = -1;
		$packages_subscribed = $this->PackagesUser->query("select count(package_id) as packCount,packages.name,package_id from packages_users left join packages on (packages.id = package_id) where Date(packages_users.start) = '".$date."' and active = 1 and user_id not in (select user_id from packages_users as pck_users where pck_users.package_id = packages_users.package_id and active = 0)  group by package_id order by packCount desc");
		$users_packages_subscribed = $this->PackagesUser->query("select count(user_id) as userCount, users.mobile from packages_users left join packages on (packages.id = package_id) left join users on (user_id = users.id) where Date(packages_users.start) = '".$date."' and active = 1 and package_id not in (select package_id from packages_users as pck_users where pck_users.user_id = packages_users.user_id and active = 0)  group by user_id order by userCount desc");

		$packages_continued = $this->PackagesUser->query("select packages.name,users.mobile,package_id from packages_users left join packages on (packages.id = package_id) left join users on (users.id = user_id) where Date(packages_users.start) = '".$date."' and active = 1 and package_id in (select package_id from packages_users as pck_users where pck_users.user_id = packages_users.user_id and active = 0) order by packages.name");
		$packages_discontinued = $this->PackagesUser->query("select packages.name,users.mobile,package_id from packages_users left join packages on (packages.id = package_id) left join users on (users.id = user_id) where Date(packages_users.end) = '".$date."' and active = 0 and package_id not in (select package_id from packages_users as pck_users where pck_users.user_id = packages_users.user_id and active = 1) order by packages.name");

		$this->Log->recursive = -1;
		$total_messages_sent =  $this->Log->find('count',array('conditions' => array('Date(timestamp)' => $date, 'mobile != "'. DUMMY_USER .'"')));
		$free_messages_sent = $this->Log->find('count',array('conditions' => array('Date(timestamp)' => $date,'package_id is null','message_id is null')));
		$forward_messages_sent = $this->Log->find('count',array('conditions' => array('Date(timestamp)' => $date,'package_id is null','message_id is not null')));
		$package_messages_sent = $this->Log->find('count',array('conditions' => array('Date(timestamp)' => $date,'package_id is not null', 'mobile != "'. DUMMY_USER .'"')));

		$payment_success = $this->Group->query("SELECT mobile,payment.amount FROM `payment`,users where users.id = user_id and Date(payment.start_time) = '".$date."' and flag = 'SUCCESS' order by user_id");
		$payment_failure = $this->Group->query("SELECT mobile,payment.amount FROM `payment`,users where users.id = user_id and Date(payment.start_time) = '".$date."' and flag = 'FAIL' order by user_id");
		$payment_trials = $this->Group->query("SELECT mobile,count(user_id) as num_trials FROM `payment`,users where users.id = user_id and Date(payment.start_time) = '".$date."' group by user_id having sum(if(flag in('SUCCESS','FAIL'), 1, 0)) = 0");

		$pnrs_subs = $this->Pnr->query("SELECT users.mobile,count(pnrs.id) as counts from pnrs inner join users on (pnrs.user_id = users.id) where Date(start) = '". $date . "' and status_flag = 1 group by user_id");
		$stock_subs = $this->Stock->query("SELECT users.mobile,count(stocks.id) as counts from stocks inner join users on (stocks.user_id = users.id) where Date(start) = '". $date . "' and status_flag = 1 group by user_id");
		$reminder_subs = $this->Reminder->query("SELECT users.mobile,count(reminders.id) as counts from reminders inner join users on (reminders.user_id = users.id) where Date(reminders.created) = '". $date . "' and status = 0 group by user_id");

		$pnrs_discon = $this->Pnr->query("SELECT users.mobile,count(pnrs.id) as counts from pnrs inner join users on (pnrs.user_id = users.id) where Date(end) = '". $date . "' and status_flag = 0 group by user_id");
		$stock_discon = $this->Stock->query("SELECT users.mobile,count(stocks.id) as counts from stocks inner join users on (stocks.user_id = users.id) where Date(end) = '". $date . "' and status_flag = 0 group by user_id");


		$products_discon = $this->ProductsUser->find("all",array('fields' => array('ProductsUser.trial','ProductsUser.count','Product.id','Product.name','User.mobile'),
		 'conditions' => array('ProductsUser.active' => 0, 'ProductsUser.end is not null','Date(ProductsUser.end)' => $date)));

		$this->set(compact('pnrs_subs','stock_subs','reminder_subs','pnrs_discon','stock_discon','products_discon'));

		$this->set('date',$date);
		$this->set('total_users_registered',$total_users_registered);
		$this->set('users_registered_Loggedin',$users_registered_Loggedin);
		$this->set('users_registered_notLoggedin',$users_registered_notLoggedin);
		$this->set('users_registered_viaMisscall',$users_registered_viaMisscall);

		$this->set('users_visited',$users_visited);
		$this->set('returning_users',$returning_users);

		$this->set('packages_subscribed',$packages_subscribed);
		$this->set('users_packages_subscribed',$users_packages_subscribed);
		$this->set('packages_continued',$packages_continued);
		$this->set('packages_discontinued',$packages_discontinued);

		$this->set('total_messages_sent',$total_messages_sent);
		$this->set('free_messages_sent',$free_messages_sent);
		$this->set('forward_messages_sent',$forward_messages_sent);
		$this->set('package_messages_sent',$package_messages_sent);

		$this->set('payment_success',$payment_success);
		$this->set('payment_failure',$payment_failure);
		$this->set('payment_trials',$payment_trials);

	}

	function excelToPhp(){
		set_time_limit(0);
		ini_set("memory_limit","-1");
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
		$j = 340;
		while($j< 500){
			$k = $j*20;
			$data = $this->Group->query("SELECT mobile FROM market_data order by id asc LIMIT $k,20");
			if(empty($data))break;
			$this->printArray($data);
			foreach($data as $dt){
				$dnd_data = $this->General->checkDND($dt['market_data']['mobile']);
				$this->Group->query("UPDATE market_data SET dnd_flag = " . $dnd_data['dnd'] . " WHERE mobile = '" . $dt['market_data']['mobile'] . "'");
			}
			sleep(5);
			$j++;
		}
		exit;
		App::import('Vendor', 'excel_reader2');
		$data = new Spreadsheet_Excel_Reader('/home/ashish/Marketing Data/Extra/PUBLICITY SMS 2.xls', true);
		$i = 0;
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;

		$this->printArray($data->sheets[0]['cells']);
		foreach($data->sheets[0]['cells'] as $user){
			//$this->printArray($user);
			$mobile = trim($user[1]);
			$type = '';
			if(strlen($mobile) == 10){
				$this->Group->query("INSERT INTO market_data (mobile,type) VALUES ('$mobile','$type')");
			}
			//echo $mobile;
			/*$name = '';
			 $email = '';
			 if(strlen($mobile) == 10){
				$data_num = $this->Group->query("SELECT * FROM market_data where mobile = '$mobile'");
				if(empty($data_num)){
				$this->Group->query("INSERT INTO market_data (mobile,name,email,type) VALUES ('$mobile','".addslashes($name)."','$email','$type')");
				}
				else {
				$this->Group->query("UPDATE market_data SET name = '".addslashes($name)."',email='$email',type='$type'  WHERE mobile = '$mobile'");
				}
				}*/
				
			$i++;
		}
		$this->autoRender = false;
	}

	function excelToPhp1(){
		set_time_limit(0);
		ini_set("memory_limit","-1");
		error_reporting(E_ALL);
		ini_set("display_errors", 1);

		App::import('Vendor', 'excel_reader2');
		$data = new Spreadsheet_Excel_Reader('Feb 2012 SMSTDK start n stop list.xls', true);
		$i = 0;
		$users = array();
		//$this->Group->query("TRUNCATE log_opt");
		foreach($data->sheets[1]['cells'] as $user){
			if($i > 0){
				$mobile = trim($user[1]);
				$mobile = substr($mobile, -10);
				$time = trim($user[3]) . " " . trim($user[4]);
				$this->Group->query("INSERT INTO log_opt (mobile,type,timestamp) VALUES ('$mobile','START','$time')");
				$users[$mobile]['start'] = $time;
			}
			$i++;
		}
		$i = 0;
		foreach($data->sheets[0]['cells'] as $user){
			if($i > 0){
				$mobile = trim($user[1]);
				$mobile = substr($mobile, -10);
				$time = trim($user[3]) . " " . trim($user[4]);
				$this->Group->query("INSERT INTO log_opt (mobile,type,timestamp) VALUES ('$mobile','STOP','$time')");
				$users[$mobile]['stop'] = $time;
			}
			$i++;
		}
		$mobiles = array();
		foreach($users as $key => $value){
			$mobile = $key;
			if(isset($value['stop'])){
				$opt_flag = 0;
				if(isset($value['start']) && $value['start'] > $value['stop']){
					$opt_flag = 1;
				}
			}
			else {
				$opt_flag = 1;
			}
			$mobiles[$mobile]['opt_flag'] = $opt_flag;
			$this->Group->query("UPDATE users SET opt_flag = $opt_flag WHERE mobile = '$mobile'");
		}
		//$this->printArray($mobiles);

		$this->autoRender = false;
	}

	function allRecentMsgs($package_id)
	{
		$allRecentMsgs = $this->General->getRecentMessages($package_id,'no');
		$this->set('allRecentMsgs',$allRecentMsgs);
	}

	function allMessages($package_id){
		$table = $this->Group->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $package_id AND categories_packages.category_id = data_tables.category_id");
		$table_name = $table['0']['data_tables']['table_name'];
		if($table_name == 'data_funs')$table_name = 'messages';

		if($table_name == 'messages'){
			$data = $this->Group->query("select messages.content,messages.id from $table_name as messages inner join data_funs ON (data_funs.message_id = messages.id) where package_id = $package_id");
		}
		else {
			$data = $this->Group->query("select messages.content,messages.id from $table_name as messages where package_id = $package_id");
		}
		$qry = "SELECT packages.name FROM packages WHERE id = $package_id";
		$data_details = $this->Group->query($qry);

		$this->set('allRecentMsgs',$data);
		$this->set('name',$data_details['0']['packages']['name']);
	}

	function dataStatus()
	{
		$qry = "SELECT packages.name, count(data_funs.package_id) as cnt FROM packages LEFT JOIN data_funs ON (packages.id = data_funs.package_id AND data_funs.flag=0) INNER JOIN categories_packages ON (packages.id = categories_packages.package_id) WHERE category_id in (1,10,17,18) group by packages.id";
		$data_details = $this->Group->query($qry);

		$qry = "SELECT packages.name, count(data_tips.package_id) as cnt FROM packages LEFT JOIN data_tips ON (packages.id = data_tips.package_id AND data_tips.flag=0) INNER JOIN categories_packages ON (packages.id = categories_packages.package_id) WHERE category_id in (48) group by packages.id";
		$tips_data_details = $this->Group->query($qry);

		$qry = "SELECT packages.name, packages.id, count(data_crickets.package_id) as cnt FROM packages LEFT JOIN data_crickets ON (packages.id = data_crickets.package_id AND data_crickets.flag=0) INNER JOIN categories_packages ON (packages.id = categories_packages.package_id) WHERE category_id in (43) group by packages.id";
		$cricket_data_details = $this->Group->query($qry);

		$qry = "SELECT count(*) as cnt FROM cool_videos WHERE download = 0";
		$video_data_details = $this->Group->query($qry);
		$this->set('video_data_details',$video_data_details);

		$this->set('fun_data_details',$data_details);
		$this->set('tips_data_details',$tips_data_details);
		$this->set('cricket_data_details',$cricket_data_details);
		$this->render('fun_data_status');
	}
	function build_acl() {
		set_time_limit(0);
		ini_set("memory_limit","-1");

		if (!Configure::read('debug')) {
			return $this->_stop();
		}
		$log = array();

		$aco =& $this->Acl->Aco;
		$root = $aco->node('controllers');
		if (!$root) {
			$aco->create(array('parent_id' => null, 'model' => null, 'alias' => 'controllers'));
			$root = $aco->save();
			$root['Aco']['id'] = $aco->id;
			$log[] = 'Created Aco node for controllers';
		} else {
			$root = $root[0];
		}

		App::import('Core', 'File');
		$Controllers = Configure::listObjects('controller');
		$appIndex = array_search('App', $Controllers);
		if ($appIndex !== false ) {
			unset($Controllers[$appIndex]);
		}
		$baseMethods = get_class_methods('Controller');
		$baseMethods[] = 'buildAcl';

		$Plugins = $this->_getPluginControllerNames();
		$Controllers = array_merge($Controllers, $Plugins);

		// look at each controller in app/controllers
		foreach ($Controllers as $ctrlName) {
			$methods = $this->_getClassMethods($this->_getPluginControllerPath($ctrlName));
				
			// Do all Plugins First
			if ($this->_isPlugin($ctrlName)){
				$pluginNode = $aco->node('controllers/'.$this->_getPluginName($ctrlName));
				if (!$pluginNode) {
					$aco->create(array('parent_id' => $root['Aco']['id'], 'model' => null, 'alias' => $this->_getPluginName($ctrlName)));
					$pluginNode = $aco->save();
					$pluginNode['Aco']['id'] = $aco->id;
					$log[] = 'Created Aco node for ' . $this->_getPluginName($ctrlName) . ' Plugin';
				}
			}
			// find / make controller node
			$controllerNode = $aco->node('controllers/'.$ctrlName);
			if (!$controllerNode) {
				if ($this->_isPlugin($ctrlName)){
					$pluginNode = $aco->node('controllers/' . $this->_getPluginName($ctrlName));
					$aco->create(array('parent_id' => $pluginNode['0']['Aco']['id'], 'model' => null, 'alias' => $this->_getPluginControllerName($ctrlName)));
					$controllerNode = $aco->save();
					$controllerNode['Aco']['id'] = $aco->id;
					$log[] = 'Created Aco node for ' . $this->_getPluginControllerName($ctrlName) . ' ' . $this->_getPluginName($ctrlName) . ' Plugin Controller';
				} else {
					$aco->create(array('parent_id' => $root['Aco']['id'], 'model' => null, 'alias' => $ctrlName));
					$controllerNode = $aco->save();
					$controllerNode['Aco']['id'] = $aco->id;
					$log[] = 'Created Aco node for ' . $ctrlName;
				}
			} else {
				$controllerNode = $controllerNode[0];
			}
				
			//clean the methods. to remove those in Controller and private actions.
			foreach ($methods as $k => $method) {
				if (strpos($method, '_', 0) === 0) {
					unset($methods[$k]);
					continue;
				}
				if (in_array($method, $baseMethods)) {
					unset($methods[$k]);
					continue;
				}
				$methodNode = $aco->node('controllers/'.$ctrlName.'/'.$method);
				if (!$methodNode) {
					$aco->create(array('parent_id' => $controllerNode['Aco']['id'], 'model' => null, 'alias' => $method));
					$methodNode = $aco->save();
					$log[] = 'Created Aco node for '. $method;
				}
			}
		}
		if(count($log)>0) {
			debug($log);
		}
		$this->autoRender = false;
	}

	function _getClassMethods($ctrlName = null) {
		App::import('Controller', $ctrlName);
		if (strlen(strstr($ctrlName, '.')) > 0) {
			// plugin's controller
			$num = strpos($ctrlName, '.');
			$ctrlName = substr($ctrlName, $num+1);
		}
		$ctrlclass = $ctrlName . 'Controller';
		$methods = get_class_methods($ctrlclass);

		// Add scaffold defaults if scaffolds are being used
		$properties = get_class_vars($ctrlclass);
		if (array_key_exists('scaffold',$properties)) {
			if($properties['scaffold'] == 'admin') {
				$methods = array_merge($methods, array('admin_add', 'admin_edit', 'admin_index', 'admin_view', 'admin_delete'));
			} else {
				$methods = array_merge($methods, array('add', 'edit', 'index', 'view', 'delete'));
			}
		}
		return $methods;
	}

	function _isPlugin($ctrlName = null) {
		$arr = String::tokenize($ctrlName, '/');
		if (count($arr) > 1) {
			return true;
		} else {
			return false;
		}
	}

	function _getPluginControllerPath($ctrlName = null) {
		$arr = String::tokenize($ctrlName, '/');
		if (count($arr) == 2) {
			return $arr[0] . '.' . $arr[1];
		} else {
			return $arr[0];
		}
	}

	function _getPluginName($ctrlName = null) {
		$arr = String::tokenize($ctrlName, '/');
		if (count($arr) == 2) {
			return $arr[0];
		} else {
			return false;
		}
	}

	function _getPluginControllerName($ctrlName = null) {
		$arr = String::tokenize($ctrlName, '/');
		if (count($arr) == 2) {
			return $arr[1];
		} else {
			return false;
		}
	}

	/**
	 * Get the names of the plugin controllers ...
	 *
	 * This function will get an array of the plugin controller names, and
	 * also makes sure the controllers are available for us to get the
	 * method names by doing an App::import for each plugin controller.
	 *
	 * @return array of plugin names.
	 *
	 */
	function _getPluginControllerNames() {
		App::import('Core', 'File', 'Folder');
		$paths = Configure::getInstance();
		$folder =& new Folder();
		$folder->cd(APP . 'plugins');

		// Get the list of plugins
		$Plugins = $folder->read();
		$Plugins = $Plugins[0];
		$arr = array();

		// Loop through the plugins
		foreach($Plugins as $pluginName) {
			// Change directory to the plugin
			$didCD = $folder->cd(APP . 'plugins'. DS . $pluginName . DS . 'controllers');
			// Get a list of the files that have a file name that ends
			// with controller.php
			$files = $folder->findRecursive('.*_controller\.php');
				
			// Loop through the controllers we found in the plugins directory
			foreach($files as $fileName) {
				// Get the base file name
				$file = basename($fileName);

				// Get the controller name
				$file = Inflector::camelize(substr($file, 0, strlen($file)-strlen('_controller.php')));
				if (!preg_match('/^'. Inflector::humanize($pluginName). 'App/', $file)) {
					if (!App::import('Controller', $pluginName.'.'.$file)) {
						debug('Error importing '.$file.' for plugin '.$pluginName);
					} else {
						/// Now prepend the Plugin name ...
						// This is required to allow us to fetch the method names.
						$arr[] = Inflector::humanize($pluginName) . "/" . $file;
					}
				}
			}
		}
		return $arr;
	}

	function changeDNDFlags(){
		set_time_limit(0);
		ini_set("memory_limit","-1");

		if(date('H:i') >= '23:56'){
			$this->User->query("UPDATE scheme_sales SET variable = 0");
			$this->releaseLock();
		}
		$this->lock();
		$this->User->recursive = -1;

		$var = $this->User->query("SELECT variable FROM scheme_sales");
		$var1 = $var['0']['scheme_sales']['variable']*80;
		$data = $this->User->query("SELECT * FROM users ORDER by id LIMIT $var1,80");
		//$data = $this->User->query("SELECT * FROM users WHERE dnd_flag = -1 ORDER by id LIMIT 20");
		foreach($data as $user){
			$check = $this->General->checkDND($user['users']['mobile']);
			if($check['dnd'] == '-1'){
				//$this->General->mailToAdmins('DND Flag Check Not Working','Mobile: ' . $user['users']['mobile']);
			}
			else {
				if($check['dnd'] != $user['users']['dnd_flag'] || $check['preference'] != $user['users']['ncpr_pref']){
					//$this->General->mailToAdmins('User NCPR/DND Preference Changed','Mobile: ' . $user['users']['mobile'] . ', DND - ' . $check['dnd'] . ', NCPR - ' . $check['preference']);
				}
				$this->User->updateAll(array('User.dnd_flag' => $check['dnd'],'User.ncpr_pref' => $check['preference']), array('User.mobile' => $user['users']['mobile']));
			}
		}
		$this->User->query("UPDATE scheme_sales SET variable = variable + 1");
		if(!empty($data))
		//if($var['0']['scheme_sales']['variable'] <= 50)
		$this->releaseLock();
		$this->autoRender = false;
	}

	function test(){
		//$this->General->sendMessage("",array('8619919195'),"test","promo");
		echo "1"; exit;
		//$query = "SELECT refineddata.message_id, (sum(rate) / count(rate)) as ratio from refineddata group by refineddata.message_id having ((sum(rate) / count(rate)) > 3 AND count(rate) >1) OR (sum(rate) / count(rate)) = 5 order by ratio desc";
		$table = $this->Message->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $pack_id AND categories_packages.category_id = data_tables.category_id");
		$table_name = $table['0']['data_tables']['table_name'];
		if($table_name == 'data_funs')$table_name = 'messages';
		$query = "SELECT refineddata.message_id, (sum(rate) / count(rate)) as ratio from refineddata WHERE package_id = $pack_id AND message_id NOT IN (SELECT cyclic_refined.message_id from cyclic_refined WHERE cyclic_refined.table='$table_name') group by refineddata.message_id having ((sum(rate) / count(rate)) > 3 AND count(rate) >1) OR (sum(rate) / count(rate)) = 5 order by ratio desc";

		$this->User->recursive = -1;
		$data = $this->User->query($query);
		$ids = array();
		foreach($data as $ab){
			$ids[] = $ab['refineddata']['message_id'];
		}
		$query = "Select id,cyclicData from $table_name where id in (".implode(",",$ids).") order by Field($table_name.id,".implode(",",$ids).")";

		//$query = "Select logs.message_id, logs.content from logs where message_id in (".implode(",",$ids).") and package_id = $pack_id and message_id not in (select cyclic_refined.message_id from cyclic_refined where cyclic_refined.package_id = $pack_id) group by logs.message_id order by Field(logs.message_id,".implode(",",$ids).")";
		$this->printArray($this->User->query($query));

		$this->autoRender = false;
	}

	function test1($pack_id){
		//$query = "SELECT refineddata.message_id from refineddata group by refineddata.message_id order by rate desc";
		$table = $this->Message->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $pack_id AND categories_packages.category_id = data_tables.category_id");
		$table_name = $table['0']['data_tables']['table_name'];
		if($table_name == 'data_funs')$table_name = 'messages';

		$query = "SELECT refineddata.message_id from refineddata WHERE package_id = $pack_id AND message_id NOT IN (SELECT cyclic_refined.message_id from cyclic_refined WHERE cyclic_refined.table = '$table_name') group by refineddata.message_id order by rate desc";
		$this->User->recursive = -1;
		$data = $this->User->query($query);
		$ids = array();
		foreach($data as $ab){
			$ids[] = $ab['refineddata']['message_id'];
		}
		$query = "Select id,cyclicData from $table_name where id in (".implode(",",$ids).") order by Field($table_name.id,".implode(",",$ids).")";

		//$query = "Select logs.message_id, logs.content from logs where message_id in (".implode(",",$ids).") and package_id = $pack_id and message_id not in (select cyclic_refined.message_id from cyclic_refined where cyclic_refined.package_id = $pack_id) group by logs.message_id order by Field(logs.message_id,".implode(",",$ids).")";
		$this->printArray($this->User->query($query));

		$this->autoRender = false;
	}

	function makeCycle(){
		//$ids = "52,1023,713,851,1235,854,1291,872,1229,831,1416,266,1232,990,1242,869,721,817,1019,1022,951,992,1222,937,712,864,14,1015,18,841,876,814,1161,1016,1226";
		//$ids = "1294,1162,1275,1382,160,1264,159,1102,888,1089,1087,867,1113,1117,1067,1006,895,850,836,1004,1104,896,1008,960,1100,942,849,963,1111,948,862,1036,928,999,681";
		//$ids = "997,1207,1204,1209,1059,427,1058,865,1215,827,822,922,1002,892,940,813,1054,1025,903,964,1057,886,968,1055,1060,1056,848,826,1001,1026,1278,1085,802,1133,816";
		$ids = "311,1576,191,1520,1284,1472,486,1326,1135,1408,843,1173,988,1304,966,684,996,956,863,1474,320,1338,1140,1347,1138,1379,461,1314,1179,1409,110,1406,257,1432,893";
		$id_arr = explode(",",$ids);
		$msg_num = 1;
		foreach($id_arr as $id){
			$this->Group->query("INSERT INTO cyclic_refined (message_id,package_id,cycle,msg_num,cyclic_refined.table) VALUES ($id,67,1,$msg_num,'data_tips')");
			$msg_num++;
		}
		$this->autoRender = false;
	}

	function makeCycle1($package_id){
		$data = $this->Group->query("SELECT message_id,messages.cyclicData,group_concat(cast(user_id as char(10))) as userids FROM `refineddata`,messages WHERE package_id = $package_id AND messages.id = message_id AND cyclicData is not null group by message_id having (sum(rate)/count(refineddata.id) >= 4.0)");
		foreach($data as $dt){
			if(strstr($dt['0']['userids'], '96893029') !== false){
				$ids[] = $dt['refineddata']['message_id'];
				//$this->Group->query("INSERT INTO misscall_refined (message_id,package_id,misscall_refined.table) VALUES (".$dt['refineddata']['message_id'].",$package_id,'messages')");
			}
		}
		$this->printArray($ids);
		$this->autoRender = false;
	}
}
?>