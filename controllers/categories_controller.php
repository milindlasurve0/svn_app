<?php
class CategoriesController extends AppController {
	
	var $name = 'Categories';
	var $helpers = array('Html','Ajax','Javascript','Paginator','Minify');
	var $components = array('General');
	var $uses = array('Message','CategoriesPackage','Metadata');
	
	var $paginate = array(
		'Message' => array('limit' => 10, 
			'order' => array('rating' => 'desc','Message.id' => 'desc'),
			'fields' => array('Message.*','Author.*')
		)
	);
	
	
	function beforeFilter() {
		parent::beforeFilter(); 
		$this->Auth->allowedActions = array('view','getSubcategories');
		//$this->Auth->allow('*');
	}
	
	function index() {
		$this->Category->recursive = 0;
		$this->set('categories', $this->paginate());
	}
	
	
	function view($cat,$scat = null) {
		if (!$cat) {
			$this->redirect(array('controller' => 'users','action' => 'er404','404'));
		}
		$cat_name = $this->General->urlToName($cat);
		
		$this->Category->recursive = 1;
		$this->Category->filterBindings(array('Message','Package'));
		
		$arrCatData = $this->Category->find('first', array('conditions' => array('Category.name' => $cat_name)));
		if(empty($arrCatData)){
			$this->redirect(array('controller' => 'users','action' => 'er404','404'));
		}
		
		if($arrCatData['Category']['parent'] == '0'){ //case of category with only packages not messages
			if($this->Session->read('Auth.User')) { //if logged in then showing it using users/view
				$this->Session->write('category',$arrCatData['Category']['0']['id']);
				$this->redirect(array('controller' => 'users','action' => 'view'));
			}
			else { //individual page if not logged in
				$layout = $arrCatData['Category']['0']['layout'];
				if($layout == 'L'){
					$limit = PACK_LIMIT_L;
				}
				else if($layout == 'G'){
					$limit = PACK_LIMIT_G;
				}
					
				$page = 1;
				if(!$scat){
					$arrPackData= $this->General->getAllPackages(array($arrCatData['Category']['0']['id']),$limit,$page);
					$count= $this->General->getCountPackages(array($arrCatData['Category']['0']['id']));
					$this->set('category', $arrCatData['Category']['0']['id']);
				}
				else {
					$scat_name = $this->General->urlToName($scat);
					$parent_id = $arrCatData['Category']['id'];
					
					$arrCatData = $this->Category->find('first', array('conditions' => array('Category.name' => $scat_name,'Category.parent' => $parent_id)));
					
					if(empty($arrCatData)){
						$this->redirect(array('controller' => 'users','action' => 'er404','404'));
					}
					$layout = $arrCatData['Category']['layout'];
					if($layout == 'L'){
						$limit = PACK_LIMIT_L;
					}
					else if($layout == 'G'){
						$limit = PACK_LIMIT_G;
					}
					
					$arrPackData= $this->General->getAllPackages(array($arrCatData['Category']['id']),$limit,$page);
					$count= $this->General->getCountPackages(array($arrCatData['Category']['id']));
					$this->set('category', $arrCatData['Category']['id']);
				}
				
				$resultCategories= $this->Category->find('all', array(
					'fields' => array('Category.name', 'Category.id'),
					'conditions' => array('Category.parent' =>'0','Category.toshow' => '1'),
					'order' => 'FIELD(id,'.CATEGORIES_IN_ORDER.')'
				));
				$this->set('resultCategories',$resultCategories);
				$this->set('packageData',$arrPackData);
				$this->set('popularPacks',$this->popularPacks);
				$this->set('categoryData', $arrCatData);
				$this->set('count',$count);
				$this->set('limit',$limit);
				$this->set('layout',$layout);
				$this->set('page',$page);
				
				$metaData = $this->Metadata->find('first', array('fields' => array('Metadata.title', 'Metadata.desc','Metadata.h1','Metadata.footer'),'conditions' => array('Metadata.controller' => 'categories','Metadata.action' => 'view','Metadata.ref_id' => $arrCatData['Category']['id'])));
				$this->set('pageTitle',$metaData['Metadata']['title']);
				$this->set('pageDesc',$metaData['Metadata']['desc']);
				$this->set('pageH1',$metaData['Metadata']['h1']);
				$this->set('pageFooter',$metaData['Metadata']['footer']);								
				$this->render('/categories/category');
			}
		}
		else { //category wise messages pages
			$catType = '';
			$parent_id = '';
			if(!$scat){
				if($arrCatData['Category']['parent'] != FUN_ID){
					$arrCatData = $this->Category->find('first', array('fields' => array('Category.name'),'conditions' => array('Category.id' => $arrCatData['Category']['parent'])));
					$this->redirect(array('action' => 'view',$this->General->nameToUrl($arrCatData['Category']['name']),$cat));
				}
				
				$this->Category->recursive = 1;
				$this->Category->filterBindings();
				$arrCatData = $this->Category->find('first', array('conditions' => array('Category.name' => $cat_name)));
		         
				$parent_id = $arrCatData['Category']['id'];
				$subCats_ids = array();
				$i = 0;
				while(!empty($arrCatData['Category'][$i])) {  
					$subCats_ids[] = $arrCatData['Category'][$i]['id'];
					$i++;
				}
				$subCats_ids[] = $parent_id;
				
				$arrMsgData = $this->paginate(array('Message.category_id' => $subCats_ids,'Message.toshow' => '1','Date(Message.created) < "'. MSG_DATE .'"'));
				$catType = 'cat';
				
				$otherCatData = $this->Category->find('all', array('fields' => array('Category.id','Category.name'), 'conditions' => array('Category.name !=' => $cat_name,'Category.parent' => '0','Category.toshow' => '1')));
				$arrPackData= $this->General->getAllPackages(array($arrCatData['Category']['id']));
			}
			else {
				if($arrCatData['Category']['parent'] != FUN_ID){
					$this->redirect(array('controller' => 'users','action' => 'er404','404'));
				}
				
				$scat_name = $this->General->urlToName($scat);
				$parent_id = $arrCatData['Category']['id'];	
				
				$this->Category->recursive = 1;
				$this->Category->filterBindings();
				$arrCatData = $this->Category->find('first', array('conditions' => array('Category.name' => $scat_name,'Category.parent' => $parent_id)));
				
				if(empty($arrCatData)){
					$this->redirect(array('controller' => 'users','action' => 'er404','404'));
				}
				
				$arrMsgData = $this->paginate('Message',array('Message.category_id' => $arrCatData['Category']['id'],'Message.toshow' => '1','Date(Message.created) < "'. MSG_DATE .'"'));
			
				$catType = 'scat';
				$otherCatData = $this->Category->find('all', array('fields' => array('Category.id','Category.name'), 'conditions' => array('Category.name !=' => $scat_name,'Category.parent' => $parent_id,'Category.toshow' => '1')));
				$arrPackData= $this->General->getAllPackages(array($parent_id));
			
				$this->set('categoryLink',$cat);
				$this->set('categoryName',$cat_name);	
			}
			
			$this->set('otherCatData',$otherCatData);
			$this->set('categoryType',$catType);
			$this->set('messageData',$arrMsgData);
			$this->set('packageData',$arrPackData);
			$this->set('categoryData', $arrCatData);
			
			$metaData = $this->Metadata->find('first', array('fields' => array('Metadata.title', 'Metadata.desc','Metadata.h1','Metadata.footer'),'conditions' => array('Metadata.controller' => 'categories','Metadata.action' => 'view','Metadata.ref_id' => $arrCatData['Category']['id'])));
			$this->set('pageTitle',$metaData['Metadata']['title']);
			$this->set('pageDesc',$metaData['Metadata']['desc']);
			$this->set('pageH1',$metaData['Metadata']['h1']);
			$this->set('pageFooter',$metaData['Metadata']['footer']);
			
			//$query = "SELECT tags.url,tags.name,count(`messages_tags`.tag_id) as counts FROM `messages_tags`,tags where tags.id  = `messages_tags`.tag_id group by `messages_tags`.tag_id having count(`messages_tags`.tag_id) > 200";
			$this->set('tagCloud',$this->General->tagCloud());
		
		
		}					
	}
	
	function add() {
		if (!empty($this->data)) {
			$this->Category->create();
			if ($this->Category->save($this->data)) {
				//$this->Session->setFlash(__('The category has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The category could not be saved. Please, try again.', true));
			}
		}
		$packages = $this->Category->Package->find('list');
		$this->set(compact('packages'));
	}
	
	function edit($id = null) {
		if (!$id && empty($this->data)) {
			//$this->Session->setFlash(__('Invalid category', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Category->save($this->data)) {
				//$this->Session->setFlash(__('The category has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The category could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Category->read(null, $id);
		}
		$packages = $this->Category->Package->find('list');
		$this->set(compact('packages'));
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for category', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Category->delete($id)) {
			$this->Session->setFlash(__('Category deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Category was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	function getSubcategories(){
		$this->Category->recursive = -1;
		$subCats = $this->Category->find('all', array('fields' => array('Category.id','Category.name'), 'conditions' => array('Category.parent' => 	$_REQUEST['catid']), 'cache' => 'getSubcategories'.$_REQUEST['catid']));
		
		$str = 'Choose SubCategory : '.
			'<select name="data[Message][category_id]">';
		foreach($subCats as $category) {
			$str .= 	'<option value="'.$category['Category']['id'].'">'.$category['Category']['name'].'</option>';
		}
		$str .= '</select>';
		
		echo $str;
		$this->autoRender = false;	
	}
	
	/*function toCamelCase(){ //script which converts all the categories name to camelcase
		$catData = $this->Category->find('all');
		
		foreach($catData as $cat){
			$this->data['Category']['id'] = $cat['Category']['id'];
			$this->data['Category']['name']  = $this->General->makeCamelcase($cat['Category']['name']);
			$this->Category->save($this->data);
		}
		$this->autoRender = false;
	}*/
	
	/*function capSMS(){ //script which converts Sms to SMS
		$catData = $this->Category->find('all');
		//echo '<pre>';
		//print_r($catData);
		//echo '</pre>';
		
		foreach($catData as $cat){
			
			$this->data['Category']['id'] = $cat['Category']['id'];
			
			$this->data['Category']['name']  = str_replace('Sms', 'SMS', $cat['Category']['name']);
			
			$this->Category->save($this->data);
		}
		$this->autoRender = false;
		
	}*/
}
?>