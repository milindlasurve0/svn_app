<?php

class PnrsController extends AppController {

	var $name = 'Pnrs';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler');
	var $uses = array('Pnr','SMSApp','AppsLog');
	var $controller_name = 'pnr';

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allowedActions = array('about','createPNRAlert','cronPnrTimesInitialize','cronPnrStatusCheck','sendPNRSMS','validatePNR','retailPNR','pnrCheck','getDepartureTimes','sendTrainTimings','sendTrainReminder');
	}
	
	function initial($about = null){
		if($about == null)
			$about = $_REQUEST['about'];
		$this->set('about',$about);
		$this->render('initial');
	}
	
	function faqs(){
		$this->render('faqs');
	}
	
	function about(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		$this->render('about_pnr');
	}
	
	function createAlert(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		$this->render('create_alert');
	}
	
	function myAlerts(){
		$this->Pnr->recursive = -1;
		$this->SMSApp->recursive = -1;
		$data = $this->Pnr->find('all',array('conditions' => array('user_id' => $this->Session->read('Auth.User.id')), 'order' => 'Pnr.status_flag desc, Pnr.journey_date desc, Pnr.start desc'));
		$name = $this->SMSApp->find('first',array('fields' => array('name'),'conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		$this->set('name',$name);
		$this->render('my_alerts');
	}

	function validatePnr(){
		$pnr = $_REQUEST['pnr'];
		$this->Pnr->recursive = -1;
		$getInfo = $this->General->getPNRInfo($pnr);
			
		$getInfo['pnr_number'] = $pnr;
		$this->set('pnrInfo', $getInfo);
		$this->render('/pnrs/validate_pnr','ajax');	
	}
	
	function retailPNR($pnr,$mobile){
		$random = $_REQUEST['random'];
		$this->General->retailPNR($pnr,$mobile,$random);
		$this->autoRender = false;
	}
	
	function pnrCheck($pass){
		$pars = json_decode($pass,true);
		$pnr = $pars['pnr'];
		$mobile = $pars['mobile'];
		
		$this->Pnr->recursive = -1;
		$pnrData = $this->Pnr->find('first', array('conditions' => array('pnr_number' => $pnr, 'mobile' => $mobile)));
		$mail_body = "PNR $pnr -  Mobile: $mobile";
		$vars = array();
		if(empty($pnrData)){
			$alias = "PNR_NOT_ACTIVE";
			$mail_subject = "PNR Alert not active on $mobile";
		}
		else {
			$pnrInfo = $this->General->getPNRInfo($pnr);
			$mail_subject = "PNR Manual Status Check $mobile";
			if(isset($pnrInfo['error']) && ($pnrInfo['error'] == 1 || $pnrInfo['error'] == 0)){
				$alias = "PNR_SERVER_NOT_WORKING";
			}
			else {
				$alias = "PNR_STATUS_EXTRA_INFO";
				if(trim($pnrData['Pnr']['train_info']) != trim($pnrInfo['train_info'])){ //It means there is a change
					$this->Pnr->query("update pnrs set train_info ='".addslashes($pnrInfo['train_info'])."' where id=".$pnrData['Pnr']['id']);
				}
				else {//if there is no change
					$vars[] = "No change since your last update";
				}
				$vars[] = $pnrData['Pnr']['pnr_number'];
				$vars[] = substr($pnrData['Pnr']['title'],0,15). "..\n";
				$vars[] = $this->General->dateFormat(date('Y-m-d H:i:s'));
				
				$num = $pnrInfo['num_passengers'];
				for($i = 0; $i < $num; $i++) {
					$vars[] = $pnrInfo['passengers'][$i]['current_status'];
				}
			}
		}
		$msg = $this->General->createMessage($alias,$vars);
		$this->sendPNRSMS($pnrData['Pnr']['id'],$msg,$mobile);
		//$this->General->mailToAdmins($mail_subject, $mail_body);
	
		$this->autoRender = false;
	}
	
	function checkStatus(){
		$id = $_REQUEST['id'];
		$this->Pnr->recursive = -1;
		$data = $this->Pnr->find('first',array('fields' => array('pnr_number'),'conditions' => array('id' => $id)));
		
		$getInfo = $this->General->getPNRInfo($data['Pnr']['pnr_number']);
		$this->set('id', $id);
		$this->set('pnrInfo', $getInfo);
		$this->render('/pnrs/pnr_status','ajax');
	}

		
	function initlializeDepartureTime($train_number,$boarding,$date,$id){
		$data = $this->getTrainTimings($train_number,$boarding,$date);
		if(!isset($data['error'])){
			$this->Pnr->query("UPDATE pnrs set departure_time = '".$data['departure_time']."',timing_flag=2 where pnrs.id = " . $id);
			$time = date('Y-m-d H:i:s',strtotime($data['departure_time'] . ' - 6 hours'));
			$this->Pnr->query("INSERT INTO pnrs_train_times (pnr_id,time) VALUES (". $id.",'".$time."')");
		}
	}
	
	function initlializeStationArrivalTime($train_number,$to,$date,$id,$first=null){
		$data = $this->getTrainTimings($train_number,$to,$date);
		$this->Pnr->recursive = -1;
		$pnrData = $this->Pnr->query("SELECT departure_time FROM pnrs WHERE id = $id");
			
		if(!empty($pnrData['0']['pnrs']['departure_time'])){
			if($first == null && (isset($data['error']) || $pnrData['0']['pnrs']['departure_time'] > $data['arrival_time'])){
				$new_date = date('Y-m-d',strtotime($date . ' + 1 day'));
				$this->initlializeStationArrivalTime($train_number,$to,$new_date,$id,1);
			}
			else if(!isset($data['error'])){
				$this->Pnr->query("UPDATE pnrs set arrival_time = '".$data['arrival_time']."',timing_arrival_flag=2 where pnrs.id = " . $id);
				$time = date('Y-m-d H:i:s',strtotime($data['arrival_time'] . ' - 4 hours'));
				$this->Pnr->query("UPDATE pnrs_train_times set arrival_time = '$time' WHERE pnr_id = $id");
			}		
		}
	}
	
	function getDepartureTimes($par1,$par2){//once in 6 hrs
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$date = date('Y-m-d',strtotime('+ 1 day'));
			$this->Pnr->recursive = -1;
			$data = $this->Pnr->find('all',array('fields' => array('Pnr.id','Pnr.journey_date','Pnr.boarding','Pnr.train_number'), 'conditions' => array('Pnr.timing_flag' => 1,'(Pnr.journey_date = "' . $date .'" OR Pnr.journey_date = "' . date('Y-m-d') .'")' ,'UPPER(Pnr.chart_status) NOT IN ("DISABLED","CANCELLED")','Pnr.boarding is not null')));
			foreach($data as $pnr){
				$this->initlializeDepartureTime($pnr['Pnr']['train_number'],$pnr['Pnr']['boarding'],$pnr['Pnr']['journey_date'],$pnr['Pnr']['id']);
			}
			
			$date = date('Y-m-d',strtotime('- 1 day'));
			$this->Pnr->recursive = -1;
			$data = $this->Pnr->find('all',array('fields' => array('Pnr.id','Pnr.journey_date','Pnr.to_station','Pnr.train_number'), 'conditions' => array('timing_arrival_flag' => 1,'UPPER(Pnr.chart_status) NOT IN ("DISABLED","CANCELLED")','Pnr.to_station is not null','Pnr.departure_time is not null', '(Pnr.journey_date = "' . $date .'" OR Pnr.journey_date = "' . date('Y-m-d') .'")')));
			foreach($data as $pnr){
				$this->initlializeStationArrivalTime($pnr['Pnr']['train_number'],$pnr['Pnr']['to_station'],date('Y-m-d'),$pnr['Pnr']['id']);
			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}
	
	function sendTrainReminder($par1,$par2){//will run every 3 hours a day 8,11,2,5,8
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$data = $this->Pnr->query("SELECT * FROM pnrs WHERE pnrs.reminder_flag = 1 AND pnrs.departure_time is not null AND pnrs.journey_date =  '".date('Y-m-d',strtotime('+ 1 day'))."' AND pnrs.chart_status != 'DISABLED' AND pnrs.chart_status != 'CANCELLED'");
			foreach($data as $pnr){
				$pnrInfo = $this->General->getPNRInfo($pnr['pnrs']['pnr_number']);
				if(isset($pnrInfo['error']) && ($pnrInfo['error'] == 1 || $pnrInfo['error'] == 0)){
					continue;
				}
				$vars = array();
				$vars[] = $pnr['pnrs']['pnr_number'];
				$vars[] = substr($pnr['pnrs']['title'],0,15). "..";
				$vars[] = date('d-m-Y',strtotime($pnr['pnrs']['journey_date']));
				$vars[] = $pnr['pnrs']['boarding'];
				$vars[] = date('H:i', strtotime($pnr['pnrs']['departure_time']));
				$num = $pnrInfo['num_passengers'];
				for($i = 0; $i < $num; $i++) {
					$vars[] = $pnrInfo['passengers'][$i]['current_status'];
				}
				$msg = $this->General->createMessage("PNR_JOURNEY_REMINDER",$vars);
				$this->sendPNRSMS($pnr['pnrs']['id'],$msg);
				$this->Pnr->query("update pnrs set train_info ='".addslashes($pnrInfo['train_info'])."',reminder_flag=0 where id=".$pnr['pnrs']['id']);
			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}
	
	function sendTrainTimings($par1,$par2){//every 30 mins
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$data = $this->Pnr->query("SELECT pnr_number,title,pnr_id,departure_time,delayed_time,train_number,boarding,journey_date FROM pnrs_train_times inner join pnrs ON (pnrs.id = pnrs_train_times.pnr_id) WHERE pnrs_train_times.time < '" . date('Y-m-d H:i:s') . "' AND pnrs_train_times.status = 1");
			foreach($data as $pnr){
				$id = $pnr['pnrs_train_times']['pnr_id'];
				$dept_time = $pnr['pnrs']['departure_time'];
				$delay_time = $pnr['pnrs']['delayed_time'];
				
				$new_time = date('Y-m-d H:i:s',strtotime($dept_time . ' +'. date('H',strtotime($delay_time)) .' hours +' .date('i',strtotime($delay_time)) . ' minutes'));
				$before_time = date('Y-m-d H:i:s',strtotime($new_time . ' -2 hours'));
				$train_data = $this->getTrainTimings($pnr['pnrs']['train_number'],$pnr['pnrs']['boarding'],$pnr['pnrs']['journey_date']);
				$delay = false;
				if(!isset($train_data['error']) && ($delay_time != $train_data['delayed_time'] || date('Y-m-d H:i:s') > $before_time )){
				 	if($delay_time != $train_data['delayed_time']) $delay = true;
					$delay_time = $train_data['delayed_time'];
				 	$new_time = date('Y-m-d H:i:s',strtotime($dept_time . ' +'. date('H',strtotime($delay_time)) .' hours +' .date('i',strtotime($delay_time)) . ' minutes'));
				
					$vars = array();
					$vars[] = $pnr['pnrs']['pnr_number'];
					$vars[] = substr($pnr['pnrs']['title'],0,15);
					$vars[] = $pnr['pnrs']['boarding'];
					$vars[] = date('H:i', strtotime($new_time));
					if($delay_time == '00:00:00'){
						$alias = "PNR_BEFORE_DEPARTURE";
						$vars[] = date('H:i');
					}
					else {
						$alias = "PNR_BEFORE_DEPARTURE_DELAY";
						$vars[] = date('H',strtotime($delay_time));
						$vars[] = date('i',strtotime($delay_time));
					}
					$msg = $this->General->createMessage($alias,$vars);
					if(date('Y-m-d H:i:s') <= $new_time) $this->sendPNRSMS($id,$msg);
					$this->Pnr->query("UPDATE pnrs set delayed_time = '$delay_time' where id = $id");
					$this->Pnr->query("UPDATE pnrs_train_times set count=count+1 where pnr_id = $id");
				}
				
				if(date('Y-m-d H:i:s') > $new_time){
					$this->Pnr->query("UPDATE pnrs_train_times set status = 0 where pnr_id = $id"); 
				}
				else {
					if($delay) $now = date('Y-m-d H:i:s', strtotime('+ 55 minutes'));
					else $now = date('Y-m-d H:i:s', strtotime('+ 85 minutes'));
					$this->Pnr->query("UPDATE pnrs_train_times set time = '$now' where pnr_id = $id"); 
				}
			}
			
			//For arrival of train to destination
			$data = $this->Pnr->query("SELECT pnr_number,title,pnr_id,pnrs.arrival_time,delayed_arrival_time,train_number,to_station,journey_date FROM pnrs_train_times inner join pnrs ON (pnrs.id = pnrs_train_times.pnr_id) WHERE pnrs_train_times.arrival_time < '" . date('Y-m-d H:i:s') . "' AND pnrs_train_times.arrival_status = 1 AND pnrs_train_times.arrival_time != '0000-00-00 00:00:00'");
			foreach($data as $pnr){
				$id = $pnr['pnrs_train_times']['pnr_id'];
				$arrival_time = $pnr['pnrs']['arrival_time'];
				$delay_time = $pnr['pnrs']['delayed_arrival_time'];
				
				$new_time = date('Y-m-d H:i:s',strtotime($arrival_time . ' +'. date('H',strtotime($delay_time)) .' hours +' .date('i',strtotime($delay_time)) . ' minutes'));
				$before_time = date('Y-m-d H:i:s',strtotime($new_time . ' -2 hours'));
				$train_data = $this->getTrainTimings($pnr['pnrs']['train_number'],$pnr['pnrs']['to_station'],date('Y-m-d',strtotime($arrival_time)));
				$delay = false;
				if(!isset($train_data['error']) && ($delay_time != $train_data['delayed_arrival'] || date('Y-m-d H:i:s') > $before_time )){
				 	if($delay_time != $train_data['delayed_arrival']) $delay = true;
					$delay_time = $train_data['delayed_arrival'];
				 	$new_time = date('Y-m-d H:i:s',strtotime($arrival_time . ' +'. date('H',strtotime($delay_time)) .' hours +' .date('i',strtotime($delay_time)) . ' minutes'));
				
					$vars = array();
					$vars[] = $pnr['pnrs']['pnr_number'];
					$vars[] = substr($pnr['pnrs']['title'],0,15);
					$vars[] = $pnr['pnrs']['to_station'];
					$vars[] = date('H:i', strtotime($new_time));
					
				 	if($delay_time == '00:00:00'){
				 		$alias = "PNR_BEFORE_REACHING";
				 		$vars[] = date('H:i');
				 	}
					else {
						$alias = "PNR_BEFORE_REACHING_DELAY";
						$vars[] = date('H',strtotime($delay_time));
						$vars[] = date('i',strtotime($delay_time));
					}
					$msg = $this->General->createMessage($alias,$vars);
					if(date('Y-m-d H:i:s') <= $new_time) $this->sendPNRSMS($id,$msg);
					$this->Pnr->query("UPDATE pnrs set delayed_arrival_time = '$delay_time' where id = $id");
					$this->Pnr->query("UPDATE pnrs_train_times set arrival_count=arrival_count+1 where pnr_id = $id");
				}
				
				if(date('Y-m-d H:i:s') > $new_time){
					$this->Pnr->query("UPDATE pnrs_train_times set arrival_status = 0 where pnr_id = $id"); 
				}
				else {
					if($delay) $now = date('Y-m-d H:i:s', strtotime('+ 25 minutes'));
					else $now = date('Y-m-d H:i:s', strtotime('+ 55 minutes'));
					$this->Pnr->query("UPDATE pnrs_train_times set arrival_time = '$now' where pnr_id = $id"); 
				}
			}
			$this->releaseLock();
		}
		$this->autoRender = false;	
	}

	function getTrainTimings($train_number,$boarding,$date){
		//Configure::write('debug', 0);
		$url = 'http://www.trainenquiry.com/RunningIslTrSt.aspx?tr='.$train_number.'&st='.$boarding.'&dt='.date('d-m-Y',strtotime($date));
		$user_agent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$page= curl_exec($ch);
		
		if($page)
		{
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);

			$content = $this->getTrainTimeData($domDocument);
			curl_close ($ch);
			return $content;
		}
		else
		{
			curl_close ($ch);
			$data = array();
			$data['error'] = "1"; //Site is down
			return $data;
		}

	}

	function getTrainTimeData($domDocument)
	{
		//create an XPath object out of the document and query it for the supplied xpath
		$xpath = "//table[contains(concat(' ', normalize-space(@id), ' '), 'Table3')]/tbody/tr/td[2]";
		$domXPath = new DOMXPath($domDocument);
		$domNodeList = $domXPath->query($xpath);
		$data = array();
		$i = 0;
		foreach($domNodeList as $node){
			$domDocument1 = new DOMDocument();
			foreach($node->childNodes as $childNode){
				$domDocument1->appendChild($domDocument1->importNode($childNode, true));
				$html = $domDocument1->saveHTML();
				if($i == 2)
					$data['arrival_time'] = date('Y-m-d H:i:s', strtotime($html));
				if($i == 3){
					if(preg_match("/^([01][0-9]|2[0-3]):([0-5][0-9])$/", $html, $match)){
						$data['delayed_arrival'] = date('H:i:s', strtotime($html));
					}
					else {
						$data['delayed_arrival'] = '00:00:00';
					}	
				}
				if($i == 5)
					$data['departure_time'] = date('Y-m-d H:i:s', strtotime($html));
				if($i == 6){
					if(preg_match("/^([01][0-9]|2[0-3]):([0-5][0-9])$/", $html, $match)){
						$data['delayed_time'] = date('H:i:s', strtotime($html));
					}
					else {
						$data['delayed_time'] = '00:00:00';
					}
				}
			}
			$i++;
		}
		if($i == 0){
			$data['error'] = "0"; //Invalid entries
		}
		return $data;
	}

	function createPNRAlert($pnrInfo=null,$retailer_flag=null){

		$this->Pnr->recursive = -1;
		$retail = false;
		if($pnrInfo==null){
			$mobile = trim($_REQUEST['mobile']);
			$pnr = trim($_REQUEST['pnr_number']);
			$user_id = $this->Session->read('Auth.User.id');
			if(empty($mobile)){
				$mobile = $this->Session->read('Auth.User.mobile');
			}
			$title = $_REQUEST['title'];
			$journey_date = $_REQUEST['journey_date'];
			$chart_status = $_REQUEST['chart_status'];
			$train_number = $_REQUEST['train_number'];
			$boarding = $_REQUEST['boarding'];
			$to = $_REQUEST['to'];
		}
		else {
			if($pnrInfo == -1)$pnrInfo = $_REQUEST;
			$retail = true;
			$mobile = $pnrInfo['mobile'];
			$pnr = $pnrInfo['pnr_number'];
			$user_id = $pnrInfo['user_id'];
			$journey_date = $pnrInfo['journey_date'];
			$chart_status = $pnrInfo['tkt_status'];
			$train_number = $pnrInfo['train_number'];
			$boarding = $pnrInfo['boarding'];
			if(isset($pnrInfo['retailer_id'])){
				$retailer_id = $pnrInfo['retailer_id'];
				$product_id = $pnrInfo['product_id'];
			}
			$to = $pnrInfo['to'];
			$title = substr(ucwords(strtolower($pnrInfo['train_name'])) . " - " . date("dMy",strtotime($pnrInfo['journey_date'])),0,20);
		}
		
		$data = $this->Pnr->find('first',array('conditions' => array('pnr_number' => $pnr,'mobile' => $mobile,'user_id' => $user_id,'status_flag' => 1)));
		if(empty($data)){
			$this->SMSApp->recursive= -1;
			$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => 'pnr')));
			$price = $this->General->getBalance($user_id) - $data['SMSApp']['basic_price'];	
			
			if($price < 0 && !$retail){
				echo '0';
				$this->autoRender = false;
			}
			else {
				$this->data['Pnr']['user_id'] = $user_id;
				$this->data['Pnr']['pnr_number'] = $pnr;
				$this->data['Pnr']['title'] = $title;
				$this->data['Pnr']['mobile'] = $mobile;
				$this->data['Pnr']['journey_date'] = $journey_date;
				//$this->data['Pnr']['train_info'] = $_REQUEST['train_info'];
				$this->data['Pnr']['chart_status'] = $chart_status;
				$this->data['Pnr']['train_number'] = $train_number;
				$this->data['Pnr']['boarding'] = $boarding;
				$this->data['Pnr']['to_station'] = $to;
				$this->data['Pnr']['status_flag'] = 1;
				$this->data['Pnr']['reminder_flag'] = 1;
				$this->data['Pnr']['price'] = $data['SMSApp']['basic_price'];
				$this->data['Pnr']['start'] = date('Y-m-d H:i:s');
				if(isset($retailer_id)){
					$this->data['Pnr']['retailer_id'] = $retailer_id;
					$this->data['Pnr']['product_id'] = $product_id;
				}
				if(!empty($retailer_flag))
					$this->data['Pnr']['retailer_flag'] = $retailer_flag;
				$this->Pnr->create();
				if ($this->Pnr->save($this->data)) {
					$this->General->makeOptIn247SMS($mobile,1);
					$this->initlializeDepartureTime($train_number,$boarding,$journey_date,$this->Pnr->id);
					$this->initlializeStationArrivalTime($train_number,$to,$journey_date,$this->Pnr->id);
					if(!$retail){
						$bal = $this->General->balanceUpdate($this->data['Pnr']['price'],'subtract');
							
						//make an entry in transactions table
						//$bal = $this->General->getBalance($user_id);
						
						if($mobile == $this->Session->read('Auth.User.mobile')){
							$alias = "PACKAGE_SUBSCRIBE";
							$vars[] = "PNR Alert '".$this->data['Pnr']['title']."'";
							$vars[] = $bal;
						}
						else {
							$alias = "PNR_SUBSCRIBE";
							$vars[] = $this->data['Pnr']['title'];
							$vars[] = $this->data['Pnr']['pnr_number'];
						}
						$msg = $this->General->createMessage($alias,$vars);
						$this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$this->data['Pnr']['price'],$data['SMSApp']['id'],$this->Pnr->id,null);
					
						//$this->General->sendMessage(SMS_SENDER,$mobile,$msg,'dnd');
						$this->General->sendMessage(SMS_SENDER,array($mobile),$msg,'template');
	
						$mail_body = $this->Session->read('Auth.User.mobile') . " has created a pnr alert";
						//$this->General->mailToAdmins("Personal Alert Subscribed: PNR Alert", $mail_body);
	
						//make an entry in app_users table
						$this->General->appUsersUpdate($data['SMSApp']['id']);
						//make an entry in pnr times to create an alert
						$this->pnrTimesUpdate($this->Pnr->id,'new');
						$this->set('pnr_number',$this->data['Pnr']['pnr_number']);
						$this->set('pnr_title',$this->data['Pnr']['title']);
						$this->set('balance',$bal);
						$this->render('/pnrs/confirmed_alert','ajax');
					}
					else {
						$this->pnrTimesUpdate($this->Pnr->id,'new');
						//make an entry in app_users table
						$this->General->appUsersUpdate($data['SMSApp']['id'],$user_id);
					}
				}
			}
			if($retail)
				return true;
		}
		else {
			if($retail)
				return false;
			else {	
				echo '1#' . $mobile;
				$this->autoRender = false;
			}
		}
	}

	function pnrTimesUpdate($id,$type=null){
		if($type == 'new'){
			$this->Pnr->query("INSERT into pnrs_times (pnr_id,time,status) VALUES ($id,'".date('H:i:s')."',1)");
		}
		else {
			$this->Pnr->recursive = -1;
			$data = $this->Pnr->find('first',array('fields' => array('Pnr.journey_date','pnrs_times.count','Pnr.departure_time','Pnr.delayed_time'), 'conditions' => array('Pnr.id' => $id),
						'joins' => array(
								array(
									'table' => 'pnrs_times',
									'type' => 'inner',
									'conditions' => array('Pnr.id = pnrs_times.pnr_id')	
								)
						)
			));
				
			$journey_date = $data['Pnr']['journey_date'];
			$upper_threshold = date('Y-m-d', strtotime(' + 3 days'));
			if($upper_threshold >= $journey_date){//2 day ticket; 6 times
				//to set time here between 8AM to 11PM every 3 hrs
				//TRAI Changes
				if(DND_FLAG && !TRANS_FLAG)
					$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '02:45:00') where pnr_id = $id"); //added 02:45 hrs	
				else {
					if(empty($data['Pnr']['departure_time'])){
						$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '03:00:00') where pnr_id = $id"); //added 3 hrs
					}
					else {
						
						$to_time=strtotime(date('Y-m-d H:i:s'));
						$from_time=strtotime($data['Pnr']['departure_time']);
						$mins = round(abs($from_time - $to_time) / 60,2);
						if($to_time > $from_time){
							$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '00:30:00') where pnr_id = $id"); //add 30 mins
						}
						else if($mins < 240){
							$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '00:15:00') where pnr_id = $id"); //add 15 mins
						}
						else if($mins < 360){
							$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '00:30:00') where pnr_id = $id"); //add 30 mins
						}
						else if($mins < 720){
							$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '01:00:00') where pnr_id = $id"); //added 1 hr
						}
						else if($mins < 1440){
							$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '02:00:00') where pnr_id = $id"); //added 2 hrs
						}
						else {
							$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '03:00:00') where pnr_id = $id"); //added 3 hrs
						}
					}
				}
			}else {
				//It will get initialized by initialize cron
				if($data['pnrs_times']['count'] == 0){
					$this->Pnr->query("UPDATE pnrs_times set count = 1, time='18:00:00' where pnr_id = $id");
				}
				else if($data['pnrs_times']['count'] == 1){
					$this->Pnr->query("UPDATE pnrs_times set status=0, count = 2 where pnr_id = $id");
				}
			}
		}
	}

	function cronPnrTimesInitialize($par1,$par2){ //every day in night 12:30 AM
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$this->Pnr->recursive = -1;
			$data = $this->Pnr->find('all',array('fields' => array('Pnr.id,journey_date','pnrs_times.time','Pnr.departure_time'), 'conditions' => array('status_flag' => 1),
					'joins' => array( array('table' => 'pnrs_times','type' => 'left','conditions' => array('pnr_id = Pnr.id')))));
			foreach($data as $pnr){
				$journey_date = $pnr['Pnr']['journey_date'];
				$upper_threshold = date('Y-m-d', strtotime(' + 3 days'));
				$last_day = date('Y-m-d H:i:s', strtotime(' + 12 hours'));
				if(!empty($pnr['Pnr']['departure_time']) && $last_day >= $pnr['Pnr']['departure_time']){
					$this->Pnr->query("UPDATE pnrs_times set time = '00:00:00',status=1,count=0 where pnr_id = ".$pnr['Pnr']['id']); //Night 0 AM
				}
				else if($upper_threshold >= $journey_date){
					//TRAI Changes 
					if(DND_FLAG)
						$this->Pnr->query("UPDATE pnrs_times set time = '09:15:00',status=1,count=0 where pnr_id = ".$pnr['Pnr']['id']); //Morning 9 AM
					else 
						$this->Pnr->query("UPDATE pnrs_times set time = '08:00:00',status=1,count=0 where pnr_id = ".$pnr['Pnr']['id']); //Morning 8 AM
							
				}else {
					$this->Pnr->query("UPDATE pnrs_times set time = '10:00:00',status=1,count=0 where pnr_id = ".$pnr['Pnr']['id']); //Morning 10 AM
				}
	
			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	function cronPnrStatusCheck($par1,$par2){ //every 15 mins
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			if(!$this->General->checkTimeSlot() && !TRANS_FLAG){
				$this->autoRender = false;
				$this->releaseLock();
				return;
			}
			
			$time = date('H:i:s');
			$this->Pnr->recursive = -1;
			
			$start = '08:00:00';
			$end = '23:00:00';
			
			if($time >= $start && $time <= $end){ //between 8AM and 11PM only
				$data = $this->Pnr->query("SELECT pnr_id FROM pnrs_times where time < '".$time."' and status = 1 LIMIT 20");
				foreach($data as $pnr){
					$pnrData = $this->Pnr->find('first', array('conditions' => array('id' => $pnr['pnrs_times']['pnr_id'])));
					$pnrInfo = $this->General->getPNRInfo($pnrData['Pnr']['pnr_number']);
					$update = true;
					if(isset($pnrInfo['error']) && ($pnrInfo['error'] == 1 || $pnrInfo['error'] == 0)){
						$update = false;
						//railway site is down or service is not available right now
						$this->Pnr->query("UPDATE pnrs_times set time = ADDTIME(time, '00:15:00') where pnr_id = ".$pnr['pnrs_times']['pnr_id']); //updated the time by 30 mins
					}
					else {
						if(trim($pnrData['Pnr']['train_info']) != trim($pnrInfo['train_info'])){ //It means there is a change
							$alias = "PNR_STATUS";
							$vars = array();
							if(isset($pnrInfo['error'])){
								$alias = "PNR_STATUS_EXTRA_INFO";
								if($pnrInfo['error'] == "3" || $pnrInfo['error'] == "6"){
									$vars[] = "Hurray!! Your ticket is confirmed.";
								}
								else if($pnrInfo['error'] == "5"){
									$vars[] =  "Chart has been prepared !!";
								}
								else if($pnrInfo['error'] == "4"){
									$vars[] = "Your ticket has been cancelled.";
								}
							}
							$vars[] = $pnrData['Pnr']['pnr_number'];
							$vars[] = substr($pnrData['Pnr']['title'],0,15). "..";
							$vars[] = $this->General->dateFormat(date('Y-m-d H:i:s'));
							$num = $pnrInfo['num_passengers'];
							for($i = 0; $i < $num; $i++) {
								$vars[] = $pnrInfo['passengers'][$i]['current_status'];
							}
							$msg = $this->General->createMessage($alias,$vars);
							$this->sendPNRSMS($pnr['pnrs_times']['pnr_id'],$msg);
							$this->Pnr->query("update pnrs set train_info ='".addslashes($pnrInfo['train_info'])."' where id=".$pnr['pnrs_times']['pnr_id']);
						}
						else {//if there is no change
							$this->SMSApp->recursive= -1;
							$sms_app = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
							$this->AppsLog->recursive = -1;
							$log = $this->AppsLog->find('first',array('fields' => array('timestamp'),'conditions' => array('app_id' => $sms_app['SMSApp']['id'],'ref_id' => $pnr['pnrs_times']['pnr_id']), 'order' => 'timestamp desc'));
							$last_timestamp = $log['AppsLog']['timestamp'];
							
							$time = date('Y-m-d H:i:s', strtotime($last_timestamp . ' + 7 days'));
							if($time < date('Y-m-d H:i:s')){
								$vars = array();
								$vars[] = substr($pnrData['Pnr']['title'],0,15);
								$vars[] = $pnrData['Pnr']['pnr_number'];
								$vars[] = $this->General->dateFormat(date('Y-m-d H:i:s'));
								$num = $pnrInfo['num_passengers'];
								for($i = 0; $i < $num; $i++) {
									$vars[] = $pnrInfo['passengers'][$i]['current_status'];
								}
								$msg = $this->General->createMessage("PNR_STATUS_NO_UPDATE",$vars);
								$this->sendPNRSMS($pnr['pnrs_times']['pnr_id'],$msg);
							}
						}
					}	
					if(isset($pnrInfo['error'])){
						if($pnrInfo['error'] == "3")
							$this->removePnrAlert($pnr['pnrs_times']['pnr_id'],"CONFIRMED");
						else if($pnrInfo['error'] == "4")
							$this->removePnrAlert($pnr['pnrs_times']['pnr_id'],"CANCELLED");
						else if($pnrInfo['error'] == "5")
							$this->removePnrAlert($pnr['pnrs_times']['pnr_id'],"CHART PREPARED");
						else if($update)$this->pnrTimesUpdate($pnr['pnrs_times']['pnr_id']);			
					}
					else {
						$this->pnrTimesUpdate($pnr['pnrs_times']['pnr_id']);
					}
				}
			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	function removePnrAlert($id,$type){
		$this->Pnr->query("update pnrs set status_flag = 0,end= '".date('Y-m-d H-i-s')."',chart_status='".$type."' where id=$id");
		$this->Pnr->query("delete from pnrs_times where pnr_id=$id");
		$this->Pnr->recursive = -1;
		$user = $this->Pnr->find('first',array('fields' => array('title','mobile'),'conditions' => array('id' => $id)));
//		/$this->General->makeOptOut247SMS($user['Pnr']['mobile'],1);
		
		if($type == "CANCELLED"){
			$this->Pnr->query("delete from pnrs_train_times where pnr_id=$id");
		}
	}
	
	function disablePnrAlert(){
		$id = $_REQUEST['id'];
		
		$this->Pnr->query("update pnrs set status_flag = 0,end= '".date('Y-m-d H-i-s')."',chart_status='DISABLED' where id=$id");
		$this->Pnr->query("delete from pnrs_times where pnr_id=$id");
		$this->Pnr->recursive = -1;
		$data = $this->Pnr->find('first',array('fields' => array('Pnr.title','Pnr.mobile'), 'conditions' => array('id' => $id)));
		//$this->General->makeOptOut247SMS($data['Pnr']['mobile'],1);
		
		$msg = "Dear User, You have successfully disabled your PNR alert '". $data['Pnr']['title']. "'.\nThanks for using SMSTadka-PNR Alert";
		$this->Pnr->query("delete from pnrs_train_times where pnr_id=$id");
		$this->sendPNRSMS($id,$msg);
		echo $msg;
		$this->autoRender = false;
	}

	function sendPNRSMS($id,$msg,$mobile=null){
		$this->Pnr->recursive = -1;
		$user = $this->Pnr->find('first',array('fields' => array('User.mobile','Pnr.title','User.id','Pnr.mobile','Pnr.retailer_id','Pnr.product_id','Retailer.signature'),
						'joins' => array(
									array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'inner',
										'conditions' => array('User.id = Pnr.user_id')
									),
									array(
										'table' => 'retailers',
										'alias' => 'Retailer',
										'type' => 'left',
										'conditions' => array('Retailer.id = Pnr.retailer_id')
									)
						),
						'conditions' => array('Pnr.id' => $id)			
		));
		//log update
		$this->SMSApp->recursive= -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		if(!empty($user['Pnr']['mobile'])){
			$mobile = $user['Pnr']['mobile'];
		}
		else if(isset($user['User']['mobile'])){
			$mobile = $user['User']['mobile'];
		}
		
		if(!empty($user['Retailer']['signature']) && ($user['Pnr']['product_id'] == PNR_HAPPY_JOURNEY || $user['Pnr']['product_id'] == PNR_SIGNATURE)){
			$msg .= "\n\n" . $user['Retailer']['signature'];
		}
		
		$log_id = $this->General->appLogUpdate($data['SMSApp']['id'],$id,null,$msg,$mobile,$user['User']['id']);
		$receiver[$log_id] = $mobile;
		//$this->General->sendMessage(SMS_SENDER,$receiver,$msg,'app',$this->controller_name);
		$this->General->sendMessage(SMS_SENDER,$receiver,$msg,'template',$this->controller_name);
	}
}
?>