<?php
class LogsController extends AppController {

	var $name = 'Logs';
	var $helpers = array('Html','Ajax','Javascript');
	var $uses = array('Log');
	
	function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('*');
		//$this->Auth->allow('login','mobileCheck','captcha','captchaValidate','dashboard','account','setting');
		$this->Auth->allowedActions = array('killIdleConnections','modemDeliveryUpdate','GSLogCheck','routeDLRCheck','coolVideoStat','sendNONSentMessages','tataSmsDLRUpdate','testTataSms');
	}

	function index() {
		$this->Log->recursive = 0;
		$this->set('logs', $this->paginate());
	}
	
	function coolVideoStat(){
		$res = $this->Log->query("SELECT cool_video_id,count(cool_video_id) as cnt,description,short_url FROM cool_video_stat left join cool_videos on ( cool_videos.id = cool_video_stat.cool_video_id) group by cool_video_id order by cnt desc");		
		$i = 0;
		$j = 1;
		echo "<b>Download Stat:</b>";
		echo "<table border='1' cellpadding='0px' cellspacing='0px'>";
		echo "<tr>";
		echo "<td> &nbsp;Index &nbsp;</td>";
		echo "<td> &nbsp;Video Id  &nbsp;</td>";
		echo "<td> &nbsp;Download Count &nbsp;</td>";
		echo "<td> &nbsp;Video Desc &nbsp;</td>";
		echo "<td> &nbsp;Video URL &nbsp;</td>";
		echo "</tr>";
		foreach($res as $res1){
			echo "<tr>";
			echo "<td>".$j."</td>";
			echo "<td>&nbsp;".$res1['cool_video_stat']['cool_video_id']."</td>";
			echo "<td>&nbsp;".$res1['0']['cnt']."</td>";
			echo "<td>&nbsp;".$res1['cool_videos']['description']."</td>";
			
			if(!is_null($res1['cool_videos']['short_url']))
			echo "<td>&nbsp;<a href='".$res1['cool_videos']['short_url']."' target='_blank'>".$res1['cool_videos']['short_url']."</a></td>";
			else
			echo "<td>&nbsp;URL Manipulation/Record deleted</td>";
			
			echo "</tr>";
			$i++;
			$j++;			
		}
		echo "</table>";
		
		$this->autoRender = false;	
	}
	
	function GSLogCheck(){
		$externalId=$_REQUEST['externalId'];
		$deliveredTS=$_REQUEST['deliveredTS'];
		$status=$_REQUEST['status'];
		$cause=$_REQUEST['cause'];
		$phoneNo=$_REQUEST['phoneNo'];
		
		$myFile = "/var/www/gupshup.txt";
		$fh = fopen($myFile, 'a') or die("can't open file");
		$stringData = date('Y-m-d H:i:s')."====".$externalId."\n";
		fwrite($fh, $stringData);
		
		$deliveredTS = date('Y-m-d H:i:s', $deliveredTS/1000);
		$query = "Update gupshup_del_log set deliverd='".$deliveredTS."', status='".$status."', cause='".$cause."' where unique_id = '".$externalId."'";
		$this->Log->query($query);		
		
		fclose($fh);
		$this->autoRender = false;				
	}
	
	function routeDLRCheck(){
		$sender=$_REQUEST['sSender'];
		$mobile=$_REQUEST['sMobileNo'];
		$deliveredTS=$_REQUEST['dtDone'];
		$msgid=$_REQUEST['sMessageId'];
		$status=$_REQUEST['sStatus'];
		$charge=$_REQUEST['iCharge'];
		$status_flag = 0;
		$createdTS=$_REQUEST['dtSubmit'];
		
		if($status == 'DELIVRD')$status_flag = 1;
		
		$timediff = date('Y-m-d H:i:s',strtotime($createdTS." +15 minutes"));
		
		$query = "UPDATE delivery_routesms SET status_flag=$status_flag, sender='$sender', updated='$deliveredTS', status='$status', charge=$charge WHERE msg_id = '$msgid' AND mobile = '$mobile'";
		$this->Log->query($query);
		
		if($status != 'DELIVRD' && $timediff >= $deliveredTS){
			$data = $this->Log->query("SELECT message,route,status_flag FROM delivery_routesms WHERE mobile='$mobile' AND msg_id = '$msgid'");
			if(empty($data['0']['delivery_routesms']['route']))$route = 'payone';
			else $route = $data['0']['delivery_routesms']['route'];
			if($data['0']['delivery_routesms']['status_flag'] == 0)
				$this->General->sendMessage('',substr($mobile,-10),$data['0']['delivery_routesms']['message'],'fail-routesms-'.$route);
		}
		
		$this->autoRender = false;
	}
        
	/*function tataSmsDLRUpdate(){ 
                $this->autoRender = false;
                file_put_contents('/tmp/tataapi_update.txt', Date("Y-m-d H:i:s  -- ").json_encode($_REQUEST)."\n", FILE_APPEND | LOCK_EX);
                return 1;		
	}*/
        function testTataSms($mobile){ 
                $this->autoRender = false;
                //$this->General->sendMessage('',substr($mobile,-10),"Testing SMS Tata");
                $this->General->sendMessage('',substr($mobile,-10),"Test Tata Fail Msg",'fail-tatasms-payone');
                file_put_contents('/tmp/tataapi_update.txt', Date("Y-m-d H:i:s  -- ").$mobile."\n", FILE_APPEND | LOCK_EX);
                return 1;		
	}
        
        
        function tataSmsDLRUpdate(){
            try{
                $this->autoRender = false;
                //$logger = $this->General->dumpLog('TATA_API_HIT', 'TATA_apihit');
                $logger = $this->General->dumpLog('TATA_API_HIT_UPDATE', 'TATA_apihit_update');
                $logger->info("Dlr received : ".json_encode($_REQUEST));
                //file_put_contents('/tmp/tataapi_update.txt', Date("Y-m-d H:i:s  -- ").json_encode($_REQUEST)."\n", FILE_APPEND | LOCK_EX);
                
                $retval = isset($_REQUEST['responseid'])?trim($_REQUEST['responseid']):"";
                $status = isset($_REQUEST['status'])?trim($_REQUEST['status']):"";
                $mobile = isset($_REQUEST['mobile'])?trim($_REQUEST['mobile']):"";

                //if($status === 'Buffered')$status_flag = 1;
                if($status === 'Delivered')$status_flag = 2;
                if($status === 'Failed')$status_flag = 3;
                if($status === 'Expired')$status_flag = 4;
                
                
                if($status === 'Delivered'){
                    try{
                        $query = "UPDATE msgTataSmsDelLog SET status_flag='$status_flag', status='$status',timestamp = '".time()."' WHERE ret_val = '$retval'";
                        $this->Log->query($query);                    
                    } catch (Exception $ex) {
                        $logger->debug("tataSmsDLRUpdate exception in query : ".$ex->getTraceAsString());
                    }
                }elseif($status != 'Buffered'){
                    $data = $this->Log->query("SELECT message,timestamp,status_flag,mobile,sender FROM msgTataSmsDelLog WHERE ret_val = '$retval'");                        
                    $duration = time() - $data['0']['msgTataSmsDelLog']['timestamp'];
                    $query = "UPDATE msgTataSmsDelLog SET status_flag=$status_flag, status='$status',timestamp = '".time()."' WHERE ret_val = '$retval'";
                    $this->Log->query($query);
                    $mobile = $data['0']['msgTataSmsDelLog']['mobile'];                    
                    if($data['0']['msgTataSmsDelLog']['status_flag'] == 0 && $duration <= 60*60){ // check duration and only update within 1/2 hr
                            $this->General->sendMessage('',substr($mobile,-10),$data['0']['msgTataSmsDelLog']['message'],'fail-tatasms-payone',$data['0']['msgTataSmsDelLog']['sender']);
                            $logger->debug("sms failed from tata : RESENDING ");
                    }
                }

                $this->autoRender = false;
            }  catch (Exception $ex){
                $logger->debug("tataSmsDLRUpdate exception : ".$ex->getTraceAsString());
            }
        }
     
        
	function modemDeliveryUpdate(){
		$externalId=$_REQUEST['externalId'];
		$deliveredTS=$_REQUEST['deliveredTS'];
		$status=$_REQUEST['status'];
		$delivered_by=$_REQUEST['mobile'];
		$status .= "<br/>From mobile: $delivered_by";
		
		//$deliveredTS = date('Y-m-d H:i:s', $deliveredTS/1000);
		$query = "Update delivery_modem set delivered_time='$deliveredTS',status='$status',status_flag=1 where ret_val = '$externalId'";
		$this->Log->query($query);		
		
		$this->autoRender = false;				
	}
	
	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid log', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('log', $this->Log->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Log->create();
			if ($this->Log->save($this->data)) {
				$this->Session->setFlash(__('The log has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The log could not be saved. Please, try again.', true));
			}
		}
		$users = $this->Log->User->find('list');
		$packages = $this->Log->Package->find('list');
		$messages = $this->Log->Message->find('list');
		$transactions = $this->Log->Transaction->find('list');
		$this->set(compact('users', 'packages', 'messages', 'transactions'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid log', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Log->save($this->data)) {
				$this->Session->setFlash(__('The log has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The log could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Log->read(null, $id);
		}
		$users = $this->Log->User->find('list');
		$packages = $this->Log->Package->find('list');
		$messages = $this->Log->Message->find('list');
		$transactions = $this->Log->Transaction->find('list');
		$this->set(compact('users', 'packages', 'messages', 'transactions'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for log', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Log->delete($id)) {
			$this->Session->setFlash(__('Log deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Log was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	function countMessageLogs($par){
		if($par == "To"){
			$count = $this->Log->find('count', array(
						'conditions' => array('Log.mobile' => $this->Session->read('Auth.User.mobile'),'Log.content is not null')	
					));
		}
		else if($par == "By"){
			$count = $this->Log->find('count', array(
						'conditions' => array('Log.package_id is null','Log.user_id' => $this->Session->read('Auth.User.id'),'Log.content is not null')	
					));
		}
		
		return $count;		
	}
	
	function userMessageLogs($par=null){
		$this->Log->recursive = -1;
		$limit = 5;
		$messages_to = array();
		$messages_by = array();
		$count_to = null;
		$count_by = null;
		$pop = false;
		
		if($par === null){
			$par = $_POST['par'];
			$limit = 100;
			$pop = true;
		}
		
		if($par == "To" || $par == null){
			$messages_to = $this->Log->find('all', array(
				'fields' => array('Log.content','Log.timestamp','Log.package_id','Log.mobile','Package.name','Package.url','msgDelLog.status_flag','User.mobile'),
				'conditions' => array('Log.mobile' => $this->Session->read('Auth.User.mobile'),'Log.content is not null'),
				'order' => 'Log.timestamp desc',
				'joins' => array(
							array(
								'table' => 'packages',
								'alias' => 'Package',
								'type' => 'left',
								'conditions'=> array("Package.id = Log.package_id")
							),
							array(
								'table' => 'msgDelLog',
								'type' => 'left',
								'conditions'=> array("msgDelLog.log_id = Log.id")
							),
							array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'left',
								'conditions'=> array("Log.user_id = User.id")
							),	
					),
				'limit' => $limit	
			));
			$count_to = $this->countMessageLogs('To');
		}
		if($par == "By" || $par == null){
			$messages_by = $this->Log->find('all', array(
				'fields' => array('Log.content','Log.timestamp','Log.mobile','msgDelLog.status_flag'),
				'conditions' => array('Log.package_id is null','Log.user_id' => $this->Session->read('Auth.User.id'),'Log.content is not null'),
				'order' => 'Log.timestamp desc',
				'joins' => array(
							array(
								'table' => 'msgDelLog',
								'type' => 'left',
								'conditions'=> array("msgDelLog.log_id = Log.id")
							)),
				'limit' => $limit			
			));
			$count_by = $this->countMessageLogs('By');
		}
		$this->set('messages_to',$messages_to);
		$this->set('messages_by',$messages_by);
		$this->set('count_to',$count_to);
		$this->set('count_by',$count_by);
		$this->set('par',$par);
		$this->set('pop',$pop);
		$this->render('/logs/user_message_logs','ajax');
	}
	
	function sendNONSentMessages(){//cron which runs every morning 9:35 AM
		set_time_limit(0);
		ini_set("memory_limit","-1");
		$data = $this->Log->query("SELECT * FROM log_notsent WHERE delivered_flag = 0");
		foreach($data as $dt){
			if($dt['log_notsent']['array_flag'] == 1){
				$receivers = explode(",",$dt['log_notsent']['receivers']);
			}
			else {
				$receivers = $dt['log_notsent']['receivers'];
			}
			$this->General->sendMessage($dt['log_notsent']['sender'],$receivers,$dt['log_notsent']['message'],$dt['log_notsent']['type'],$dt['log_notsent']['app_name']);
			$this->Log->query("UPDATE log_notsent SET delivered_flag=1,delivered_time='".date('Y-m-d H:i:s')."' WHERE id = " . $dt['log_notsent']['id']);
		}
		$this->autoRender = false;
	}
	
	function killIdleConnections(){
		$result = $this->Log->query("SHOW FULL PROCESSLIST");
		foreach ($result as $res) {
			$process_id=$res['0']['Id'];
			if ($res['0']['Time'] > 100 && $res['0']['db'] == 'smstadka') {
				$mail_body = json_encode($res);
				//$this->General->mailToUsers("Killing mysql connection - smstadka",$mail_body,array('ashish@mindsarray.com'));
				$this->Log->query("KILL $process_id");
			}
		}
		$this->autoRender = false;
	}
	
	function test(){
		echo "1"; exit;
	}
}
?>