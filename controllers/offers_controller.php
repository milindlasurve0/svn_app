<?php
class OffersController extends AppController {
	
	var $name = 'Offers';
	var $helpers = array('Html','Ajax','Javascript','Paginator','Minify');
	var $components = array('General');
	var $uses = array('Message','CategoriesPackage','Metadata');
	
	function beforeFilter() {
		parent::beforeFilter(); 
		//$this->Auth->allowedActions = array('view','getSubcategories');
		$this->Auth->allow('*');
	}
	
	function index($id =null) {
		$this->redirect('/index.php');
		if(isset($id)){
			$id = urldecode($id);
			$id = $this->objMd5->Decrypt($id,encKey);
			$data = $this->Message->query("SELECT vendors_retailers.* FROM retailers_winners INNER JOIN vendors_retailers ON (vendors_retailers.vendor_id=retailers_winners.vendor_id AND vendors_retailers.retailer_code=retailers_winners.retailer_code) WHERE retailers_winners.id = $id");
			$this->set('data',$data['0']);
			$this->set('new',1);
		}
		$winners = $this->Message->query("SELECT retailers_winners.*,vendors_retailers.name,vendors_retailers.city,vendors_retailers.state,vendors_retailers.id FROM retailers_winners INNER JOIN vendors_retailers ON (vendors_retailers.retailer_code=retailers_winners.retailer_code AND vendors_retailers.vendor_id=retailers_winners.vendor_id) WHERE retailers_winners.toshow=1 AND retailers_winners.vendor_id = ".VENDOR_OSS ." order by retailers_winners.id desc");
		
		//$this->Message->query("UPDATE scheme_sales SET pageHit = pageHit + 1");
		$this->set('vendor_id',VENDOR_OSS);
		$this->set('winners',$winners);
		$this->layout = 'ajax';
		$this->render('oss');
	}
	
	function view($name,$id =null) {
		$this->redirect('/index.php');
		if(isset($id)){
			$id = urldecode($id);
			$id = $this->objMd5->Decrypt($id,encKey);
			$data = $this->Message->query("SELECT vendors_retailers.* FROM retailers_winners INNER JOIN vendors_retailers ON (vendors_retailers.vendor_id=retailers_winners.vendor_id AND vendors_retailers.retailer_code=retailers_winners.retailer_code) WHERE retailers_winners.id = $id");
			$this->set('data',$data['0']);
			$this->set('new',1);
		}

		$winners = $this->Message->query("SELECT retailers_winners.retailer_code,retailers_winners.type FROM retailers_winners INNER JOIN vendors_retailers ON (vendors_retailers.retailer_code=retailers_winners.retailer_code AND vendors_retailers.vendor_id=retailers_winners.vendor_id) WHERE retailers_winners.toshow=1 AND retailers_winners.vendor_id = ".VENDOR_TSS ." order by retailers_winners.id desc");
		
		//$this->Message->query("UPDATE scheme_sales SET pageHit = pageHit + 1");
		$this->set('winners',$winners);
		$this->set('vendor_id',VENDOR_TSS);
		$this->layout = 'ajax';
		$this->render('smartshop');
	}
	
	function terms_conditions($client=null){
		if($client == 'tss')
		$client = 'tss';
		else
		$client = 'oss';
		
		$this->set('client',$client);
		$this->layout = 'ajax';
		$this->render('terms');
	}
	
	function winnerInfoUpdate(){
		$id = $this->data['id'];
		$data = $this->Message->query("UPDATE vendors_retailers SET mobile='".$this->data['mobile']."',name='".addslashes($this->data['name'])."',email='".addslashes($this->data['email'])."',address='".addslashes($this->data['address'])."' WHERE id = $id");
		$this->layout = 'ajax';
		echo "Your information saved successfully. You will get a call in some time<script>window.location='".SITE_NAME."offers';</script>";
		//$this->General->mailToAdmins("Winner information saved successfully!!", "Mobile: " . $this->data['mobile'] . "<br/>Name: " . $this->data['name']. "<br/>Email: " . $this->data['email']. "<br/>Address: " . $this->data['address']);
		$this->autoRender = false;
	}
}
?>