<?php
class SitemapsController extends AppController{
 
	var $name = 'Sitemaps';
	var $uses = array('User', 'Category', 'Message', 'Tag','Package');
	var $helpers = array('Time');
	var $components = array('RequestHandler');
	
	function beforeFilter() {
		parent::beforeFilter(); 
		$this->Auth->allowedActions = array('index');
		//$this->Auth->allow('*');
	}
	function index (){	
		//prevent xml validation errors caused by sql log
		$this->Category->recursive=-1;
		$this->Message->recursive=-1;
		$this->Tag->recursive=-1;
		$this->Package->recursive=-1;
		$this->SMSApp->recursive=-1;
		
		$this->set('categoriesP', $this->Category->find('all', array( 'conditions' => array('toshow'=>1,'parent = 0 or id in ('.FUN_CAT.')'), 'fields' => array('modified','name'))));
		$this->set('categoriesC', $this->Category->find('all', array( 'conditions' => array('toshow'=>1,'parent != 0'), 'fields' => array('modified','name','parent'))));
		
		$this->set('packages', $this->Package->find('all', array( 'conditions' => array('Category.toshow' => 1,'Package.toshow' => 1), 'fields' => array('Package.modified','Package.url'),
								'joins' => array(
								    array(
								        'table' => 'categories_packages',
								        'type' => 'inner',
								    	'alias' => 'CategoriesPackage',
								        'conditions'=> array('CategoriesPackage.package_id = Package.id')
								    ),
								    array(
								        'table' => 'categories',
								        'type' => 'inner',
								    	'alias' => 'Category',
								        'conditions'=> array('CategoriesPackage.category_id = Category.id')
								    )
					    
						))));
		/*$this->set('messages', $this->Message->find('all', array( 'conditions' => array('Message.toshow'=>1,'Date(Message.created) < "'. date('Y-m-d',strtotime('- ' . MSG_DURATION . ' days')) .'"'), 'fields' => array('Message.modified','Message.url','Category.name'),
					'joins' => array(
								    array(
								        'table' => 'categories',
								        'type' => 'inner',
								    	'alias' => 'Category',
								        'conditions'=> array('Category.id = Message.category_id and Category.toshow = 1')
								    )
					))
		));*/
		$this->set('tags', $this->Tag->find('all', array( 'fields' => array('url'))));
		$this->set('apps', $this->SMSApp->find('all', array( 'fields' => array('url'))));	
	}
}
?>