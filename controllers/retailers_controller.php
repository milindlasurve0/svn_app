<?php
class RetailersController extends AppController {

	var $name = 'Retailers';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler','Shop');
	var $uses = array('Package','PackagesUser','Retailer','RetailersPayment','RetailersCoupon','Product','ProductsPackage','ProductsUser','Coupon','SalesmansPayment','Log','RetailerInfo','SuperDistributor','ShopTransaction','VendorsActivation');

	function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('*');
		$this->Auth->allowedActions = array('test','addCity','activateCards','submitPaymentForCreditDebit','editCyclicData','vendorRetailerInfos','tagRetailers','trasportVendorRetailerToRetailer','switchProduct','addUsersToCycle','playwinManual','smartshop','contentPartnerSubscription','vendor','updateLocation','locateUser','reports','getCouponInfo','allRetailers','pushResult','pushPlaywinResult','sendPlywinResults','unsubscribeProduct','unsubscribeTrialProduct','userCyclicPackageUpdate','initializeCronCyclicPackages','cronCyclicPackages','mobileScriptCheck','checkIfWorking','getLastComments','cyclicData','product','salesman','addSalesmanPayment','msgAfterSubscription','locator','all','locate','retailProducts','becomeRetailer','getRetailersByArea','getAreasByCity','getCitiesByState','become','superDistributor','transferSuperDistributorBalance','assignSuperDistributorCoupons','disRetailBenefits');
	}

	function activateCards($id)
	{
		//print_r($_REQUEST);
		//exit;
		$number=$_REQUEST['data']['cardNumbers'];
		if(is_nan($number))
		{
			echo "Please enter a valid card number.";
			exit;

		}

		$this->Retailer->query("update coupons set dry_flag=0 where serialNumber=$number");
		$this->autoRender=false;
	}
	function submitPaymentForCreditDebit()
	{
		$loggedInUser= $_SESSION['Auth']['User']['id'];
		$today=date('Y-m-d H:i:s');
		$crDbType=0;
		$amt=$_REQUEST['amount'];
		$details=$_REQUEST['details'];
		$type=$_REQUEST['type'];
		$id=$_REQUEST['id'];
		$amt1=$amt;
		if($type=="debit")
		{
			$amt1=(-1)*($amt);
			$crDbType=1;
		}
		$numbering = $this->Shop->getCreditDebitNumber($id,$loggedInUser,SUPER_DISTRIBUTOR,$crDbType);

		$this->Retailer->query("update super_distributors set balance=balance+".$amt1." where id=$id");
		$this->Retailer->query("insert into shop_creditdebit (to_id,to_groupid,amount,type,numbering,description,timestamp) values ($id,".SUPER_DISTRIBUTOR.",$amt,$crDbType,'$numbering','$details','$today')");
		//		$this->Retailer->query("insert into shop_transactions (type,amount,timestamp) values ($crDbType,$amt,'$today')");
		echo "Balance successfully updated.";
		$this->autoRender=false;
	}

	function tagRetailers($id = null)
	{
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/users/view');

		$i=1;
		$extra = "";
		if($id != null){
			$extra = "AND id = $id";
		}
		$query=$this->Retailer->query("select user_id,mobile,parent_id from retailers where toshow = 1 $extra");

		foreach($query as $data){
			//$chkUserIdInRetailers=$this->Retailer->query("select mobile,user_id,id from retailers where id=$i");
			$UserIdInRetailers=$data['retailers']['user_id'];
			$mobile=$data['retailers']['mobile'];
			$parentId=$data['retailers']['parent_id'];

			$userId=$UserIdInRetailers;
			if($UserIdInRetailers==null)//checking userid in retailers
			{
				$chkInUsers=$this->Retailer->query("select id,group_id from users where mobile='$mobile'");
				if(count($chkInUsers)==0)
				{
					$newUserData=$this->General->registerUser($mobile,FORCE_RETAILER_REG,RETAILER);
					$userId=$newUserData['User']['id'];
				}
				else {
					$userId=$chkInUsers['0']['users']['id'];
				}
				$this->Retailer->query("update retailers set user_id=$userId where mobile='$mobile'");
				$this->General->updateGroupIdForRetailer($userId);
			}

			if($parentId == null){
				$this->General->tagUser('OURRETAILER',$userId);
			}
			$i++;

		}
		$this->autoRender=false;

	}


	function trasportVendorRetailerToRetailer($id = null)
	{
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/users/view');

		$count=1;
		if($id != null){
			$extra = "AND id = $id";
		}
		$query=$this->Retailer->query("select user_id,mobile from vendors_retailers");
		foreach($query as $data)
		{
			$retailerUserId=$data['vendors_retailers']['user_id']; // get UID from V_R n
			$retailerMobile=$data['vendors_retailers']['mobile']; // get mobile from V_R n
			if($retailerMobile!=null)   //mobile not null start
			{
				if($retailerUserId!=null)//UID not null start 1)
				{
					$flag=true;
					$checkInRetailer=$this->Retailer->query("select user_id from retailers where user_id=$retailerUserId");
					$checkRetailerData=$checkInRetailer['0']['retailers']['user_id'];

					if($checkRetailerData==null)  // UID not present in retailer
					{
						//echo "Case 1.".$checkRetailerData;
						$this->insertIntoRetailer($retailerUserId);
						echo "Inserted into Retailer table Case 1";
						//$this->Retailer->query("insert into retailers values('','','$retailerUserId','','0','','','','0','','$retailerMobile','$vendorRetailerEmail','$vendorRetailerName','','0','$vendorRetailerCity','$vendorRetailerState','$vendorRetailerAddress','','1','date()','date()')");
						echo "TAG Function";
					}
					else
					{
						//mail to admins
						//$this->General->mailToAdmins("Same retailer re-updation ","User Id".$retailerUserId);
					}
					$tag=$this->assignTags($retailerUserId);
					$this->General->tagUser($tag,$retailerUserId);
				}// UID not null end
				else //UID not null else-> UID not present 2)
				{
					//Check if mobile present in users.
					$userMobileResult=$this->Retailer->query("select id from users where mobile='$retailerMobile'");
					//$userMobile=$userMobileResult['0']['users']['mobile'];
					$userId=$userMobileResult['0']['users']['id'];
					//	echo "USER ID".$userId;
					if($userId==null) //  mobile present in users startile='' 2a)
					{
						$registereduser=$this->General->registerUser($retailerMobile,FORCE_RETAILER_REG,RETAILER);
						$userId=$registereduser['User']['id'];
					}// mobile present in users end
					$this->General->updateGroupIdForRetailer($userId);
					$this->Retailer->query("update vendors_retailers set user_id='$userId' where mobile='$retailerMobile'");
					$tag=$this->assignTags($userId);
					$this->General->tagUser($tag,$userId);
					$this->insertIntoRetailer($userId);
				}

			}//mobile not null end
			$count++;
		}//end of for each
		$this->autoRender=false;
	}

	function vendorRetailerInfos(){
		$vendorRetailersData=$this->Retailer->query("SELECT * FROM vendors_retailers WHERE user_id is not null");
		$this->printArray($vendorRetailersData);
		$this->autoRender = false;
	}

	function assignTags($userId)
	{
		//echo "In assigntags function";
		$vendorRetailersData=$this->Retailer->query("select vendor_id from vendors_retailers where user_id=$userId");
		$vendorTagId=$vendorRetailersData['0']['vendors_retailers']['vendor_id'];
		if($vendorTagId==VENDOR_OSS)
		{
			$tag="OSSRETAILER";
		}
		else if($vendorTagId==VENDOR_TSS)
		{
			$tag="SMARTSHOPRETAILER";
		}
		else
		{
			$tag="Testing";
		}
		//echo $tag;
		return $tag;
	}


	function insertIntoRetailer($userId)
	{

		//to be inserted.
		$vendorRetailersData=$this->Retailer->query("select name,email,address,city,state,mobile from vendors_retailers where user_id=$userId");
		//$vendorRetailerVendorId=$vendorRetailersData['0']['vendors_retailers']['vendor_id'];
		$vendorRetailerName=addslashes($vendorRetailersData['0']['vendors_retailers']['name']);
		$vendorRetailerEmail=$vendorRetailersData['0']['vendors_retailers']['email'];
		$vendorRetailerAddress=addslashes($vendorRetailersData['0']['vendors_retailers']['address']);
		$vendorRetailerCity=$vendorRetailersData['0']['vendors_retailers']['city'];
		$vendorRetailerState=$vendorRetailersData['0']['vendors_retailers']['state'];
		$retailerMobile=$vendorRetailersData['0']['vendors_retailers']['mobile'];
		$this->Retailer->query("insert into retailers values('','','$userId','','0','','','','0','','$retailerMobile','$vendorRetailerEmail','$vendorRetailerName','','0','$vendorRetailerCity','$vendorRetailerState','$vendorRetailerAddress','','1','date()','date()')");
		//echo "Inserted !!";
		$this->autoRender=false;
	}


	function playwinManual(){
		//$userData = $this->ProductsUser->query("SELECT mobile FROM `users`,mobile_numbering,products_users WHERE SUBSTR(users.mobile,1,4) = mobile_numbering.number AND mobile_numbering.operator = 'AT' AND products_users.user_id = users.id AND products_users.product_id in (".PLAYWIN_PKGS.") AND products_users.active = 1");
		$userData = $this->ProductsUser->query("SELECT distinct users.mobile FROM `users`,products_users WHERE products_users.user_id = users.id AND products_users.product_id in (".PLAYWIN_PKGS.") AND products_users.active = 1");
			
		$mobiles = array();
		foreach($userData as $user){
			$mobiles[] = $user['users']['mobile'];
		}
		if(!empty($mobiles)){
			//$data = $this->ProductsUser->query("SELECT content FROM partners_data WHERE product_id=12 AND (date='".date('Y-m-d')."' OR date = '".date('Y-m-d',strtotime('-1 day'))."') AND sent_flag = 1 ORDER BY modified desc LIMIT 1");
			//$sms = $data['0']['partners_data']['content'];
			$sms = "*Playwin Result*
Fast Digit Lottery, 27/01/2012
Winning No.: 18 20 22 27 29 
JP Not Hit.
www.myplaywin.com";
			$this->General->sendMessage('',$mobiles,$sms,'playwin');
		}
		$this->autoRender = false;
	}
	function pushPlaywinResult(){}

	function pushResult(){
		$rec = $this->Retailer->query("select * from playwin_cron where sent_flag = 0 and id = 1");
		if($rec){
			//send 12,13
			$this->Retailer->query("UPDATE playwin_cron set sent_flag = 1 where id = 1");
			$users = $this->Retailer->query("select distinct users.mobile from  products_users inner join users on (products_users.user_id = users.id) where product_id in (" .PLAYWIN_PKGS. ") and active = 1");
			$sms = urldecode($_POST['content']);

			$mobiles = array();
			foreach($users as $user){
				if($user['users']['mobile'] != DUMMY_USER)
				array_push($mobiles, $user['users']['mobile']);
			}
			$this->General->sendMessage('',$mobiles,$sms,'playwin');
			echo 'Sent successfully!';
		}else{
			echo "Already sent.";
		}
		$this->autoRender = false;
	}

	function sendPlywinResults($par1,$par2,$time=null){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$day = date('N');
			if(isset($time) && $time == 'first'){
				//$this->General->mailToUsers("Playwin result - pre",$time,array('ashish@mindsarray.com'));
								
				$this->Retailer->query("UPDATE playwin_cron SET sent=0,draw_date='".date('Y-m-d')."',game='' WHERE draw_day = $day");
			}else{
				$rec = $this->Retailer->query("SELECT sent,results,draw_date,game FROM playwin_cron WHERE draw_day = $day");
				//$this->General->mailToUsers("Playwin result - pre",json_encode($rec),array('ashish@mindsarray.com'));
				
				if($rec && $rec['0']['playwin_cron']['sent'] < $rec['0']['playwin_cron']['results']){
					$ipArr = array('www.myplaywin.com','202.46.200.176','202.46.200.162','202.46.200.163');
					foreach($ipArr as $iA){
						$url = 'https://'.$iA.'/PlaywinResultXML.aspx';
						$params = ""; //you must know what you want to post
						//$user_agent = "Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)";
						$user_agent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST,1);
						curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
						curl_setopt($ch, CURLOPT_URL,$url);
						curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
						curl_setopt($ch, CURLOPT_TIMEOUT, 100);

						$Rec_Data=curl_exec ($ch);
						
						$array = $this->General->xml2array($Rec_Data);
						if(count($array)>0)break;
					}

					//$this->General->mailToUsers("Playwin result - pre",json_encode($array),array('ashish@mindsarray.com'));
				
						
					if($array)
					{
						$i = 0;
						foreach($array['Results']['GameResult'] as $arr){
							//echo $rec['0']['playwin_cron']['draw_date'];
							$oriDate = $arr['DrawDate'];
							$expDate = explode(" ",$oriDate);
							$expDatePart = explode("/",$expDate[0]);
							$finalDate = $expDatePart[2]."-".$expDatePart[1]."-".$expDatePart[0];
							$disDrawDate = date("M d",strtotime($finalDate));

							$nxtDrawDate = $arr['NextDrawDate'];
							$expNxtDrawDate = explode(" ",$nxtDrawDate);
							$expNxtDatePart = explode("/",$expNxtDrawDate[0]);
							$disNxtDrawDate = date("M d",strtotime($expNxtDatePart[2]."-".$expNxtDatePart[1]."-".$expNxtDatePart[0]));
							//echo $arr['Game'].$arr['Result'].$finalDate;
							//echo "  ".strtotime($finalDate)."==".strtotime($rec['0']['playwin_cron']['draw_date']);
							//echo "<br>";
							$sent = $rec['0']['playwin_cron']['sent'];
							if($finalDate == $rec['0']['playwin_cron']['draw_date']) $i++;

							if($finalDate == $rec['0']['playwin_cron']['draw_date'] && $arr['Game'] != $rec['0']['playwin_cron']['game']){
								//send 12,13
								$this->Retailer->query("UPDATE playwin_cron SET sent = sent + 1,game='".$arr['Game']."' WHERE draw_day = $day");
								$users = $this->Retailer->query("select distinct users.mobile from  products_users inner join users on (products_users.user_id = users.id) where product_id in (" .PLAYWIN_PKGS. ") and active = 1");
								$sms = "*Playwin Result*";
								$sms .= "\n".$arr['Game'].", ".$expDate[0];
								$sms .= "\nWinning No.: ".$arr['Result'];

								if(strtolower($arr['FirstPrizeHit']) == 'no')
								$sms .= "\nNo 1st Prize.";

								if(strtolower($arr['JackpotHit']) == 'yes' || strtolower($arr['FirstPrizeHit']) == 'yes')
								$sms .= "\n1st Prize Won!";

								$nxtJDateArr = explode(" ",$arr['NextDrawDate']);

								$week = ' Week';
								if(strtolower(trim($arr['Game'])) == 'fast digit lottery')
								$week = ' Draw';

								$sms .= "\nNext".$week." 1st Prize: ".$arr['NextFirstPrize'].", ".$expNxtDrawDate[0];
								$sms .= "\nwww.myplaywin.com";
								$mobiles = array();
								foreach($users as $user){
									if($user['users']['mobile'] != DUMMY_USER)
									array_push($mobiles, $user['users']['mobile']);
								}
								$sms = str_replace("  "," ",$sms);
								$this->General->sendMessage('',$mobiles,$sms,'playwin');
								//$this->General->mailToUsers("Playwin result",$sms,array('ashish@mindsarray.com'));
								$playwin_packs = explode(",",PLAYWIN_PKGS);
								foreach($playwin_packs as $pack){
									$this->Retailer->query("INSERT INTO partners_data (partner_id,product_id,content,date,sent_flag,created,modified) VALUES (".PARTNER_PLAYWIN.",".$pack.",'".addslashes($sms)."','".date('Y-m-d')."',1,'".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')");
								}
								break;
							}
						}
					}else{
						$sub = 'No XML response from Playwin.';
						$msg = 'Use the following link to manually send Playwin results<br><a href="http://www.smstadka.com/retailers/pushPlaywinResult">http://www.smstadka.com/retailers/pushPlaywinResult</a>';
						//$this->General->mailToAdmins($sub,$msg);
					}
				}

			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	function index($id = null){
		if($id == null){
			$this->Retailer->recursive = -1;
			$retailers = $this->Retailer->find('all',array('fields' => array('Retailer.*','Salesman.id','Salesman.name','Salesman.area'),
					'joins' => array(
			array(
									'table' => 'salesmans',
									'alias' => 'Salesman',
									'type' => 'left',
									'conditions' => array('Salesman.id = Retailer.salesman_id')
			)
			)

			));
			$i = 0;
			foreach($retailers as $retailer){
				$this->RetailersCoupon->recursive = -1;
				$data = $this->RetailersCoupon->find('all',array('fields' => array('count(RetailersCoupon.coupon_id) as total','SUM(IF (RetailersCoupon.user_id is not null, 1, 0)) as sold','SUM(IF (RetailersCoupon.user_id is not null AND Date(RetailersCoupon.modified) = "'.date('Y-m-d').'", 1, 0)) as soldToday'),'conditions' => array('RetailersCoupon.retailer_id' => $retailer['Retailer']['id'])));
				$lastVisit = $this->RetailersCoupon->query("select created from comments where itemtype = 1 and itemid = ".$retailer['Retailer']['id']." order by id desc limit 1");
				if(isset($lastVisit['0']['comments']['created']))$retailers[$i]['Retailer']['lastVisited'] = $lastVisit['0']['comments']['created'];
				$retailers[$i]['Coupon']['sold'] = $data['0']['0']['sold'];
				$retailers[$i]['Coupon']['total'] = $data['0']['0']['total'];
				$retailers[$i]['Coupon']['soldToday'] = $data['0']['0']['soldToday'];
				$i++;
			}

			$salesmans = $this->Retailer->query('SELECT Salesman.id,Salesman.name,Salesman.mobile,Salesman.area, GROUP_CONCAT(retailers.id) as retails FROM salesmans as Salesman LEFT JOIN retailers ON (Salesman.id = retailers.salesman_id AND retailers.salesman_id is not null) GROUP BY Salesman.id');
			$i = 0;
			foreach($salesmans as $salesman){
				if(!empty($salesman['0']['retails'])){
					$query = $this->Retailer->query("SELECT SUM(amount) as amount FROM retailers_payments WHERE retailers_payments.retailer_id IN (". $salesman['0']['retails']. ")");
					$salesmans[$i]['0']['num'] = count(explode(",",$salesman['0']['retails']));
					$salesmans[$i]['0']['amount'] = $query['0']['0']['amount'];
					$query = $this->Retailer->query("SELECT COUNT(user_id) as sold FROM retailers_coupons WHERE retailers_coupons.retailer_id IN (". $salesman['0']['retails']. ") AND retailers_coupons.user_id is not null");
					$salesmans[$i]['0']['sold']  = 	$query['0']['0']['sold'];

				}
				$i++;
			}
			$super_distributors = $this->Retailer->query('SELECT super_distributors.id,super_distributors.name,users.mobile,super_distributors.balance FROM super_distributors INNER JOIN users ON (super_distributors.user_id = users.id)');

			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = 1 ORDER BY name asc");
			$states = $this->Retailer->query("SELECT id,name FROM locator_state ORDER BY name asc");
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = 1 ORDER BY name asc");
			//$this->set('products',$products);
			$this->set('retailers',$retailers);
			$this->set('salesmans',$salesmans);
			$this->set(compact('super_distributors'));
			$this->set('areas',$areas);
			$this->set('states',$states);
			$this->set('cities',$cities);
		}
		else {
			$this->Retailer->recursive = -1;

			$payments = $this->Retailer->query("SELECT retailers_payments.amount,retailers_payments.type, users.name,retailers_payments.timestamp from retailers_payments inner join users on (users.id = user_id) where retailer_id = $id order by retailers_payments.timestamp desc");
			$products = $this->Retailer->query("SELECT products.name, COUNT(coupon_id) as total, SUM(CASE WHEN user_id is NULL THEN 0 ELSE 1 END) as sold from retailers_coupons inner join coupons on (retailers_coupons.coupon_id = coupons.id) inner join products on (product_id = products.id) where retailer_id = $id group by product_id");

			$comments = $this->Retailer->query("SELECT users.name,comments.comment,comments.created from retailers as u1, users, comments where comments.itemid= u1.id and u1.id= $id and comments.itemtype=1 and users.id = comments.user_id order by comments.id desc");

			$added = $this->Retailer->query("SELECT products.name, COUNT(coupon_id) as total, Date(retailers_coupons.created) as date from retailers_coupons inner join coupons on (retailers_coupons.coupon_id = coupons.id) inner join products on (product_id = products.id) where retailer_id = $id group by Date(retailers_coupons.created),product_id order by retailers_coupons.created desc");

			$users = $this->Retailer->query("SELECT users.mobile, COUNT(retailers_coupons.id) as total from retailers_coupons inner join users on (retailers_coupons.user_id = users.id) where retailer_id = $id AND retailers_coupons.user_id is not null group by retailers_coupons.user_id order by total desc");

			$retailer = $this->Retailer->find('first',array('fields' => array('Retailer.*','Salesman.id','Salesman.name','Salesman.area'),'conditions' => array('Retailer.id' => $id),
					'joins' => array(
			array(
									'table' => 'salesmans',
									'alias' => 'Salesman',
									'type' => 'left',
									'conditions' => array('Salesman.id = Retailer.salesman_id')
			)
			)
			));
			$this->set('retailer',$retailer);
			$this->set('products',$products);
			$this->set('payments',$payments);
			$this->set('comments',$comments);
			$this->set('added',$added);
			$this->set('users',$users);
			$this->render('get_retailer');
		}
	}

	function reports($date = null){
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/users/view');
		$this->Product->recursive = -1;
		$products = $this->Product->find('all',array('conditions' => array('Product.parent_id is null')));
		$i = 0;
		foreach($products as $product){
			$this->ProductsUser->recursive = -1;
			$data = $this->ProductsUser->find('all',array('fields' => array('SUM(ProductsUser.count - ProductsUser.trial) as sold','SUM(IF (ProductsUser.trial = 1 AND Date(ProductsUser.start) = "'. date('Y-m-d') .'", 1, 0)) as trialsToday','SUM(ProductsUser.trial) as trials'),'conditions' => array('ProductsUser.product_id' => $product['Product']['id'])));
			$products[$i]['Product']['sold'] = $data['0']['0']['sold'];
			$products[$i]['Product']['trials'] = $data['0']['0']['trials'];
			//$products[$i]['Product']['soldToday'] = $data['0']['0']['soldToday'];
			$products[$i]['Product']['trialsToday'] = $data['0']['0']['trialsToday'];
			$this->ShopTransaction->recursive = -1;
			$data_panel = $this->ShopTransaction->find('all',array('fields' => array('COUNT(id) as sold'),'conditions' => array('ShopTransaction.type' => RETAILER_ACTIVATION, 'ShopTransaction.ref2_id' => $product['Product']['id'])));
			$products[$i]['Product']['panelSold'] = $data_panel['0']['0']['sold'];
			$this->VendorsActivation->recursive = -1;
			$data_vendors = $this->VendorsActivation->find('all',array('fields' => array('COUNT(VendorsActivation.id) as sold'),
					'joins' => array(
			array(
									'table' => 'products_users',
									'alias' => 'ProductsUser',
									'type' => 'inner',
									'conditions' => array('VendorsActivation.productuser_id = ProductsUser.id', 'ProductsUser.product_id = ' . $product['Product']['id'])
			)
			)
			));
			$products[$i]['Product']['vendorSold'] = $data_vendors['0']['0']['sold'];

			$i++;
		}

		//SMSTadka own channel
		$smstadka = $this->RetailersCoupon->find('all',array('fields' => array('Product.id','Product.name','SUM(IF (Date(RetailersCoupon.modified) = "'.$date.'", 1, 0)) as soldToday','COUNT(RetailersCoupon.id) as soldTotal'), 'conditions' => array('RetailersCoupon.user_id is not null','RetailersCoupon.superdistributor_id is null','Product.id NOT IN ('.PLAYWIN_PKGS.')'),
				'joins' =>array(
		array(
								'table' => 'coupons',
								'type' => 'inner',
								'alias' => 'Coupon',
								'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')
		),
		array(
								'table' => 'products',
								'type' => 'inner',
								'alias' => 'Product',
								'conditions' => array('Product.id = Coupon.product_id')
		)
		),
				'group' => 'Product.id'
				));

				$playwin_data = array();
				foreach(explode(",",PLAYWIN_PKGS) as $product){
					$prodData = $this->Product->findById($product);
					$playwin_data[$product]['name'] = $prodData['Product']['name'];
					$playwin_data[$product]['cards_data'] = $this->RetailersCoupon->find('first',array('fields' => array('SUM(IF (Date(RetailersCoupon.modified) = "'.$date.'", 1, 0)) as soldToday','COUNT(RetailersCoupon.id) as soldTotal'), 'conditions' => array('RetailersCoupon.user_id is not null','Coupon.product_id' => $product),
					'joins' =>array(
					array(
									'table' => 'coupons',
									'type' => 'inner',
									'alias' => 'Coupon',
									'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')
					)
					)
					));
					$playwin_data[$product]['online_data'] = $this->ShopTransaction->find('first',array('fields' => array('SUM(IF (Date(ShopTransaction.timestamp) = "'.$date.'", 1, 0)) as soldToday','COUNT(ShopTransaction.id) as soldTotal'), 'conditions' => array('ShopTransaction.type' => RETAILER_ACTIVATION,'ShopTransaction.ref2_id' => $product,'SuperDistributor.active' => 1),
					'joins' =>array(
					array(
									'table' => 'shop_transactions',
									'type' => 'inner',
									'alias' => 'ShopTransaction1',
									'conditions' => array('ShopTransaction.id = ShopTransaction1.ref2_id','ShopTransaction1.type = ' . COMMISSION_SUPERDISTRIBUTOR)
					),
					array(
									'table' => 'super_distributors',
									'type' => 'inner',
									'alias' => 'SuperDistributor',
									'conditions' => array('SuperDistributor.id = ShopTransaction1.ref1_id')
					)
					)
					));
				}
				//Super Distributor Channel
				$this->SuperDistributor->recursive = -1;
				$s_dists = $this->SuperDistributor->find('all',array('conditions' => array('active = 1')));
				$s_dist_data = array();
				foreach($s_dists as $s_dist){
					$s_dist_id = $s_dist['SuperDistributor']['id'];
					$s_dist_data[$s_dist_id]['name'] =  $s_dist['SuperDistributor']['company'];
					foreach($products as $product) {
						if(!in_array($product['Product']['id'],explode(",",PLAYWIN_PKGS))){
							$sd_cards = $this->RetailersCoupon->find('first',array('fields' => array('SUM(IF (Date(RetailersCoupon.modified) = "'.$date.'", 1, 0)) as soldToday','COUNT(RetailersCoupon.id) as soldTotal'), 'conditions' => array('RetailersCoupon.user_id is not null','RetailersCoupon.superdistributor_id' => $s_dist_id,'Coupon.product_id' => $product['Product']['id']),
							'joins' =>array(
							array(
											'table' => 'coupons',
											'type' => 'inner',
											'alias' => 'Coupon',
											'conditions' => array('Coupon.id = RetailersCoupon.coupon_id')
							)
							)
							));
							$s_dist_data[$s_dist_id][$product['Product']['id']]['name'] = $product['Product']['name'];
							$s_dist_data[$s_dist_id][$product['Product']['id']]['cards_data'] =  $sd_cards;

							$sd_online = $this->ShopTransaction->find('first',array('fields' => array('SUM(IF (Date(ShopTransaction.timestamp) = "'.$date.'", 1, 0)) as soldToday','COUNT(ShopTransaction.id) as soldTotal'), 'conditions' => array('ShopTransaction.type' => RETAILER_ACTIVATION,'ShopTransaction.ref2_id' => $product['Product']['id']),
							'joins' =>array(
							array(
											'table' => 'shop_transactions',
											'type' => 'inner',
											'alias' => 'ShopTransaction1',
											'conditions' => array('ShopTransaction.id = ShopTransaction1.ref2_id','ShopTransaction1.type = ' . COMMISSION_SUPERDISTRIBUTOR,'ShopTransaction1.ref1_id = ' . $s_dist_id)
							)
							)
							));
							$s_dist_data[$s_dist_id][$product['Product']['id']]['online_data'] =  $sd_online;
						}
					}
				}
				//VENDOR Channel like OSS, SmartShop
				$vendors = $this->Retailer->query("SELECT id,company FROM vendors WHERE active = 1");
				$vendors_data = array();
				foreach($vendors as $vendor){
					$vendor_id = $vendor['vendors']['id'];
					$vendors_data[$vendor_id]['company'] = $vendor['vendors']['company'];
					$vendors_data[$vendor_id]['data'] = $this->VendorsActivation->find('all',array('fields' => array('Product.id','Product.name','SUM(IF (Date(VendorsActivation.timestamp) = "'.$date.'", 1, 0)) as soldToday','COUNT(VendorsActivation.id) as soldTotal'), 'conditions' => array('VendorsActivation.vendor_id' => $vendor_id),
					'joins' =>array(
					array(
									'table' => 'products_users',
									'type' => 'inner',
									'alias' => 'ProductsUser',
									'conditions' => array('ProductsUser.id = VendorsActivation.productuser_id')
					),
					array(
									'table' => 'products',
									'type' => 'inner',
									'alias' => 'Product',
									'conditions' => array('ProductsUser.product_id = Product.id')
					)
					),
					'group' => 'Product.id'
					));
				}
				$this->set('playwin_data',$playwin_data);
				$this->set('vendors_data',$vendors_data);
				$this->set('s_dist_data',$s_dist_data);
				$this->set('smstadka',$smstadka);
				$this->set('products',$products);
				$this->set('date',$date);
	}

	function vendor($username,$date=null){
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/users/view');
		$vendor = $this->Retailer->query("SELECT id FROM vendors WHERE username='" . strtoupper($username) ."'");
		$id = $vendor['0']['vendors']['id'];
		if($date == null)$date = date('Y-m-d');
		$retailers = $this->Retailer->query("SELECT users.followup,vendors_retailers.*,count(vendors_activations.id) as totalTrans,SUM(products.price) as totalSaleTill,SUM(if(Date(vendors_activations.timestamp) = '".$date."',products.price,0)) as saleToday FROM vendors_retailers INNER JOIN vendors_activations ON (vendors_activations.vendor_id = vendors_retailers.vendor_id AND vendors_activations.vendor_retail_code = vendors_retailers.retailer_code) INNER JOIN products_users ON (vendors_activations.productuser_id = products_users.id) INNER JOIN products ON (products.id = products_users.product_id) LEFT JOIN users ON(vendors_retailers.mobile=users.mobile) WHERE vendors_activations.vendor_id = $id AND Date(vendors_activations.timestamp) <= '".$date."' GROUP BY vendors_activations.vendor_retail_code order by saleToday desc,weeklySale desc,totalSaleTill desc");

		$winners = $this->Retailer->query("SELECT retailers_winners.*,vendors_retailers.name FROM retailers_winners INNER JOIN vendors_retailers ON (vendors_retailers.retailer_code=retailers_winners.retailer_code AND vendors_retailers.vendor_id=retailers_winners.vendor_id) WHERE retailers_winners.vendor_id = $id");

		$vendors_data = $this->VendorsActivation->find('all',array('fields' => array('Product.id','Product.name','SUM(IF (Date(VendorsActivation.timestamp) = "'.$date.'", 1, 0)) as soldToday','COUNT(VendorsActivation.id) as soldTotal'), 'conditions' => array('VendorsActivation.vendor_id' => $id),
				'joins' =>array(
		array(
								'table' => 'products_users',
								'type' => 'inner',
								'alias' => 'ProductsUser',
								'conditions' => array('ProductsUser.id = VendorsActivation.productuser_id')
		),
		array(
								'table' => 'products',
								'type' => 'inner',
								'alias' => 'Product',
								'conditions' => array('ProductsUser.product_id = Product.id')
		)
		),
				'group' => 'Product.id'
				));
				$this->set('vendors_data',$vendors_data);
				$this->set('name',$username);
				$this->set('retailers',$retailers);
				$this->set('winners',$winners);
	}

	function smartshop($date=null){
		if($this->Session->read('Auth.User.group_id') == ADMIN || $this->Session->read('Auth.User.mobile') == '9321333444'){
			$vendor = $this->Retailer->query("SELECT id FROM vendors WHERE username='smartshop'");
			$id = $vendor['0']['vendors']['id'];
			if($date == null)$date = date('Y-m-d');
			$retailers = $this->Retailer->query("SELECT vendors_retailers.*,count(vendors_activations.id) as totalTrans,SUM(products.price) as totalSaleTill,SUM(if(Date(vendors_activations.timestamp) = '".$date."',products.price,0)) as saleToday FROM vendors_retailers INNER JOIN vendors_activations ON (vendors_activations.vendor_id = vendors_retailers.vendor_id AND vendors_activations.vendor_retail_code = vendors_retailers.retailer_code) INNER JOIN products_users ON (vendors_activations.productuser_id = products_users.id) INNER JOIN products ON (products.id = products_users.product_id) WHERE vendors_activations.vendor_id = $id AND Date(vendors_activations.timestamp) <= '".$date."' GROUP BY vendors_activations.vendor_retail_code order by saleToday desc,weeklySale desc,totalSaleTill desc");
			$k = 0;
			foreach($retailers as $r){
				$possNo = $this->Retailer->query("select mobile,followup from users where id = (select user_id from products_users where id = (SELECT productuser_id from vendors_activations where vendor_retail_code = '".$r['vendors_retailers']['retailer_code']."' order by timestamp asc limit 1))");
				$retailers[$k]['vendors_retailers']['possNo'] = $possNo[0]['users']['mobile'];
				$retailers[$k]['vendors_retailers']['followup'] = $possNo[0]['users']['followup'];
				$k++;
			}
			$winners = $this->Retailer->query("SELECT retailers_winners.*,vendors_retailers.name FROM retailers_winners INNER JOIN vendors_retailers ON (vendors_retailers.retailer_code=retailers_winners.retailer_code AND vendors_retailers.vendor_id=retailers_winners.vendor_id) WHERE retailers_winners.vendor_id = $id");

			$vendors_data = $this->VendorsActivation->find('all',array('fields' => array('Product.id','Product.name','SUM(IF (Date(VendorsActivation.timestamp) = "'.$date.'", 1, 0)) as soldToday','COUNT(VendorsActivation.id) as soldTotal'), 'conditions' => array('VendorsActivation.vendor_id' => $id),
					'joins' =>array(
			array(
									'table' => 'products_users',
									'type' => 'inner',
									'alias' => 'ProductsUser',
									'conditions' => array('ProductsUser.id = VendorsActivation.productuser_id')
			),
			array(
									'table' => 'products',
									'type' => 'inner',
									'alias' => 'Product',
									'conditions' => array('ProductsUser.product_id = Product.id')
			)
			),
					'group' => 'Product.id'
					));
					$this->set('vendors_data',$vendors_data);
					$this->set('name',$username);
					$this->set('retailers',$retailers);
					$this->set('winners',$winners);
					$this->render('vendorsmart');
		}else{
			$this->redirect('/users/view');
		}
	}

	function allRetailers($ids = null){
		$this->Retailer->recursive = -1;

		$id_array = explode(',',$ids);
		$data = array();
		foreach($id_array as $id){
			$payments = $this->Retailer->query("SELECT SUM(retailers_payments.amount) as amounts FROM retailers_payments inner join users on (users.id = user_id) where retailer_id = $id");
			$products = $this->Retailer->query("SELECT products.name, COUNT(coupon_id) as total, SUM(CASE WHEN user_id is NULL THEN 0 ELSE 1 END) as sold from retailers_coupons inner join coupons on (retailers_coupons.coupon_id = coupons.id) inner join products on (product_id = products.id) where retailer_id = $id group by product_id");

			$comments = $this->Retailer->query("SELECT users.name,comments.comment,comments.created from retailers as u1, users, comments where comments.itemid= u1.id and u1.id= $id and comments.itemtype=1 and users.id = comments.user_id order by comments.id desc");

			$retailer = $this->Retailer->find('first',array('fields' => array('Retailer.*','Salesman.id','Salesman.name','Salesman.area'),'conditions' => array('Retailer.id' => $id),
					'joins' => array(
			array(
									'table' => 'salesmans',
									'alias' => 'Salesman',
									'type' => 'left',
									'conditions' => array('Salesman.id = Retailer.salesman_id')
			)
			)

			));
			$data[$id]['retailer'] = $retailer;
			$data[$id]['products'] = $products;
			$data[$id]['payments'] = $payments;
			$data[$id]['comments'] = $comments;
		}

		$this->set('data',$data);
		$this->render('all_retailer');
	}

	function locator(){
		if($this->Session->read('Auth.User')) {
			$this->Session->write('param','retailer');
			$this->redirect(array('controller' => 'users','action' => 'view'));
		}
		else {
			$this->render('list');
		}
	}

	function all($bRet = null){
		$products = $this->Product->find('all',array('fields' => array('Product.name','Product.id','Product.outside_flag','Product.price','Product.validity','Product.code'), 'conditions' => array('Product.toshow' => 1)));
		$this->set('products',$products);
		if($bRet == 'bRetailer')
		$this->set('flag',bRetailer);
		if($bRet == 'products')
		$this->set('flag',products);

		$this->render('list');
	}

	function locate(){
		$this->Retailer->recursive = -1;
		$states = $this->Retailer->query("SELECT id,name FROM locator_state ORDER BY name asc");
		//$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ".$states['0']['locator_state']['id']." ORDER BY name asc");
		//$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id =  ".$cities['0']['locator_city']['id']." ORDER BY name asc");
		$this->set('areas',$areas);
		$this->set('states',$states);
		$this->set('cities',$cities);
	}

	function locateUser(){
		$this->Retailer->recursive = -1;
		$states = $this->Retailer->query("SELECT id,name FROM locator_state where toShow = 1 ORDER BY name asc");
		//$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ".$states['0']['locator_state']['id']." ORDER BY name asc");
		//$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id =  ".$cities['0']['locator_city']['id']." ORDER BY name asc");

		//$this->set('areas',$areas);
		$this->set('states',$states);
		//$this->set('cities',$cities);
		$this->render('locate');
	}



	function getCitiesByState(){
		$id = $_REQUEST['state_id'];
		$type = $_REQUEST['type'];

		$this->Retailer->recursive = -1;
		if($type == 'r'){
			$frmType = 'Retailer';
			$fn = "getAreas(this.options[this.selectedIndex].value,'r')";
			$areas = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = $id ORDER BY name asc");
		}else if ($type == 'd'){
			$frmType = 'Distributor';
			$fn = "getAreas(this.options[this.selectedIndex].value,'d')";
			$areas = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = $id ORDER BY name asc");
		}else if ($type == 'u'){
			$frmType = 'Retailer';
			$fn = "getAreas(this.options[this.selectedIndex].value,'u')";
			$areas = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = $id and toShow = 1 ORDER BY name asc");
		}

		$html = '<select tabindex="8" id="city" name="data['.$frmType.'][city]" onchange="'.$fn.'">';
		$html .= '<option value="0">Select City</option>';
		foreach($areas as $area) {
			$html .= '<option value="'.$area['locator_city']['id'].'">'.$area['locator_city']['name'].'</option>';
		}
		$html .= '</select>';
		echo $html;
		$this->autoRender = false;
	}

	function getAreasByCity(){
		$id = $_REQUEST['city_id'];
		$type = $_REQUEST['type'];

		$this->Retailer->recursive = -1;
		if($type == 'r'){
			$frmType = 'Retailer';
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = $id ORDER BY name asc");
		}else if ($type == 'd'){
			$frmType = 'Distributor';
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = $id ORDER BY name asc");
		}else if ($type == 'u'){
			$frmType = 'Retailer';
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = $id and toShow = 1 ORDER BY name asc");
		}


		$html = '<select tabindex="9" id="area" name="data['.$frmType.'][area_id]">';
		$html .= '<option value="0">Select Area</option>';
		foreach($areas as $area) {
			$html .= '<option value="'.$area['locator_area']['id'].'">'.$area['locator_area']['name'].'</option>';
		}
		$html .= '</select>';
		echo $html;
		$this->autoRender = false;
	}

	function getRetailersByArea(){
		$id = $_REQUEST['area_id'];
		$page = 1;
		$limit = 10;

		if(isset($_REQUEST['page'])){
			$page = $_REQUEST['page'];
		}

		$this->Retailer->recursive = -1;

		$str = ($page-1)*$limit . "," . $limit;

		$retailers = $this->Retailer->find('all',array('fields' => array('Retailer.*','locator_area.name'),'conditions' => array('Retailer.area_id' => $id,'Retailer.toshow' => 1), 'order' => 'pin',
				'joins' => array(
		array(
								'table' => 'locator_area',
								'type' => 'inner',
								'conditions' => array('Retailer.area_id = locator_area.id')
		)
		),
				'limit' => $str
		));

		$count = $this->Retailer->find('count',array('conditions' => array('Retailer.area_id' => $id,'toshow' => 1)));

		$this->set('retailers',$retailers);
		$this->set('page',$page);
		$this->set('limit',$limit);
		$this->set('count',$count);
		$this->render('retailer_list');
	}

	function retailProducts(){
		$this->Product->recursive = -1;
		$products = $this->Product->find('all',array('conditions' => array('Product.toshow' => 1)));
		$this->set('products',$products);

	}

	function disRetailBenefits(){
		//echo "1";
		//$this->autoRender = false;
		//$this->render('dis_retail_benefits','ajax');
	}

	function becomeRetailer(){

	}

	function become(){
		$this->RetailerInfo->create();
		$toadd = true;
		if(empty($this->data['RetailerInfo']['name']) || empty($this->data['RetailerInfo']['mobile']) || empty($this->data['RetailerInfo']['city']) || empty($this->data['RetailerInfo']['state']) || empty($this->data['RetailerInfo']['address'])){
			$toadd = false;
		}
		$this->data['RetailerInfo']['created'] = date("Y-m-d H:i:s");
		if ($toadd && $this->RetailerInfo->save($this->data)) {
			$this->Session->setFlash(__('Details Saved Successfully', true));
			$this->render('become_retailer','ajax');
			$msg = "Mobile: " . $this->data['RetailerInfo']['mobile'];
			$msg .= "<br/>Name: " . $this->data['RetailerInfo']['name'];
			$msg .= "<br/>Address: " . $this->data['RetailerInfo']['address'] . ", " . $this->data['RetailerInfo']['city'] . ", " . $this->data['RetailerInfo']['state'];
			$msg .= "<br/>Products Interested in: " .  $this->data['RetailerInfo']['products'];
			$msg .= "<br/>Comments: " .  $this->data['RetailerInfo']['comments'];
			//$this->General->mailToAdmins($this->data['RetailerInfo']['mobile'] . " Wants To Become Retailer", $msg);
		}
		else {
			$this->Session->setFlash(__('Please fill entries properly', true));
			$this->render('become_retailer','ajax');
		}
		$this->autoRender = false;
	}

	function add(){

		$this->data['Retailer']['created'] = date('Y-m-d H:i:s');
		$this->data['Retailer']['modified'] = date('Y-m-d H:i:s');
		if(isset($this->data['Retailer']['salesman_id']) && $this->data['Retailer']['salesman_id'] == 0)
		$this->data['Retailer']['salesman_id'] = null;
		$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);
		$this->data['Retailer']['state'] = $state['0']['locator_state']['name'];

		$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);
		$this->data['Retailer']['city'] = $city['0']['locator_city']['name'];
		$mobileNumber = $this->data['Retailer']['mobile'];
		$validMobile=$this->General->mobileValidate($mobileNumber);

		$this->General->updateLocation($this->data['Retailer']['area_id']);
		if($validMobile!=null)
		{
			echo "Please enter a valid mobile number";
			exit;
		}

		//For user_id field in retialers table
		$checkUseridInUser=$this->Retailer->query("select id,group_id from users where mobile='$mobileNumber'");
		$groupIdInUsers=$checkUseridInUser['0']['users']['group_id'];
		//echo $groupIdInUsers;

		if(empty($checkUseridInUser['0']['users']['id']))//user is present in users table
		{
			$registereduser=$this->General->registerUser($mobileNumber,FORCE_RETAILER_REG,RETAILER);
			$this->data['Retailer']['user_id']=$registereduser['User']['id'];

		}
		else{
			$this->data['Retailer']['user_id']=$checkUseridInUser['0']['users']['id'];

			//updating group_id of Retailer ,if not admin.
			if($groupIdInUsers!=2)
			$this->General->updateGroupIdForRetailer($this->data['Retailer']['user_id']);


		}
			
		$checkMobileinRetailerResult=$this->Retailer->query("select id,mobile from retailers where mobile='$mobileNumber'");

		if(empty($checkMobileinRetailerResult['0']['retailers']['mobile']))
		{
			// insert in retailer table
			$this->Retailer->create();
			if ($this->Retailer->save($this->data))
			{
				echo 'The Retailer has been saved';
			}
		}
		else
		{
			$this->data['Retailer']['opening_balance']=0;
			$this->data['Retailer']['slab_id']=0;
			$this->data['Retailer']['id']=$checkMobileinRetailerResult['0']['retailers']['id'];
			$this->Retailer->Save($this->data);
			echo 'The Retailer has been saved'; // nisha added new
		}

			
		$this->autoRender = false;
	}

	function addRetailerPayment($id){
		$this->data['RetailersPayment']['retailer_id'] = $id;
		$this->data['RetailersPayment']['user_id'] = $this->Session->read('Auth.User.id');
		$this->data['RetailersPayment']['timestamp'] = date('Y-m-d H:i:s');

		if (!empty($this->data)) {
			$this->RetailersPayment->create();
			if ($this->RetailersPayment->save($this->data)) {
				$this->redirect(array('action' => 'index',$id));
			}
		}
	}

	function addRetailerCoupons($id){
		$ids = explode(",",$this->data['numbers']);
		$count = 0;

		foreach($ids as $coupons) {
			$coupons = trim($coupons);
			$id_list = explode("-",$coupons);

			if(count($id_list) == 1) $id_list[1] = $id_list[0];

			for($i = trim($id_list[0]); $i <= trim($id_list[1]); $i++){
				$coupon_id = $this->Retailer->query("SELECT id from coupons where serialNumber = $i");
				$this->RetailersCoupon->recursive = -1;
				$data = $this->RetailersCoupon->find('first',array('conditions' => array('RetailersCoupon.coupon_id' => $coupon_id['0']['coupons']['id'])));
				if(empty($data)){
					$this->data['RetailersCoupon']['coupon_id'] = $coupon_id['0']['coupons']['id'];
					$this->data['RetailersCoupon']['retailer_id'] = $id;
					$this->data['RetailersCoupon']['created'] = date('Y-m-d H:i:s');
					$this->data['RetailersCoupon']['modified'] = date('Y-m-d H:i:s');

					$this->RetailersCoupon->create();
					if($this->RetailersCoupon->save($this->data))
					{
						$count++;
						$this->Retailer->query("UPDATE coupons SET dry_flag = 0 where serialNumber = $i");
					}
				}
				else {
					$this->RetailersCoupon->query("UPDATE retailers_coupons set retailer_id = $id where coupon_id = " . $coupon_id['0']['coupons']['id']);
				}
			}
		}
		$this->Session->setFlash(__($count . ' coupons added successfully', true));
		$this->redirect(array('action' => 'index',$id));
	}



	function userCyclicPackageUpdate($package_id,$user_id){

		$query = "SELECT cycle,count,id FROM cyclic_user_packages WHERE package_id = $package_id AND user_id = $user_id order by id desc";
		$data = $this->Retailer->query($query);

		$query1 = "SELECT frequency FROM cyclic_packages WHERE package_id = $package_id";
		$freq = $this->Retailer->query($query1);
		$freq = $freq['0']['cyclic_packages']['frequency'];

		$count = $data['0']['cyclic_user_packages']['count'];
		if($count >= (CYCLIC_LENGTH*$freq - 1)){
			//$cycle = $data['0']['cyclic_user_packages']['cycle'] + 1;
			$cycle = $this->General->getNewCycle($package_id,$user_id);

			$this->Retailer->query("INSERT INTO cyclic_user_packages (package_id,user_id,cycle,count) VALUES (".$package_id.",$user_id,$cycle,0)");
		}
		$this->Retailer->query("UPDATE cyclic_user_packages SET count = count + 1 WHERE id = " . $data['0']['cyclic_user_packages']['id']);
	}

	/*It initializes crons for cyclic packages */
	function initializeCronCyclicPackages($par1,$par2){ //Once 12:30 AM night
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$this->Retailer->query("UPDATE cyclic_crons SET status = 1");
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	/*It sends message to users from different cycles running*/
	function cronCyclicPackages($par1,$par2){//runs every 30 mins
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$time = date('H:i:s', strtotime(' + 10 minutes'));
			$data = $this->Retailer->query("SELECT package_id,id FROM cyclic_crons WHERE time < '" . $time . "' and status = 1");

			foreach($data as $package){
				$pck_id = $package['cyclic_crons']['package_id'];
				$cycle_id = $package['cyclic_crons']['id'];
				$this->Retailer->query("UPDATE cyclic_crons SET status = 0 WHERE id = " . $package['cyclic_crons']['id']);

				$this->ProductsPackage->recursive = -1;
				$query = "(`ProductsPackage`.`cycle_ids` like '%,$cycle_id,%' OR `ProductsPackage`.`cycle_ids` like '$cycle_id,%' OR `ProductsPackage`.`cycle_ids` like '%,$cycle_id' OR `ProductsPackage`.`cycle_ids` like '$cycle_id')";
				$users = $this->ProductsPackage->find('all',array('fields' => array('distinct User.id','User.mobile'),'conditions' => array('ProductsPackage.package_id' => $pck_id, $query),
						'joins' => array (
				array(
										'table' => 'products_users',
										'alias' => 'ProductsUser',
										'type' => 'inner',
										'conditions' => array('ProductsUser.product_id = ProductsPackage.product_id','ProductsUser.active = 1')
				),
				array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'inner',
										'conditions' => array('User.id = ProductsUser.user_id')
				)
				)
				));

				$arrayUsers = array();
				$userIds = array();
				foreach($users as $user){
					$user_id = $user['User']['id'];
					$arrayUsers[$user_id] = $user['User']['mobile'];
					$userIds[] = $user_id;
				}
				$this->PackagesUser->recursive = -1;
				$query = "(`Package`.`cycle_ids` like '%,$cycle_id,%' OR `Package`.`cycle_ids` like '$cycle_id,%' OR `Package`.`cycle_ids` like '%,$cycle_id' OR `Package`.`cycle_ids` like '$cycle_id')";

				$users = $this->PackagesUser->find('all',array('fields' => array('User.id','User.mobile'),'conditions' => array('PackagesUser.package_id' => $pck_id,'PackagesUser.active' => 1),
						'joins' => array (
				array(
										'table' => 'packages',
										'alias' => 'Package',
										'type' => 'inner',
										'conditions' => array('Package.id = PackagesUser.package_id',$query)
				),
				array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'inner',
										'conditions' => array('User.id = PackagesUser.user_id')
				)
				)
				));
				foreach($users as $user){
					$user_id = $user['User']['id'];
					$arrayUsers[$user_id] = $user['User']['mobile'];
					$userIds[] = $user_id;
				}
				$cycles = $this->PackagesUser->query("SELECT table1.user_id,table1.cycle FROM (SELECT distinct user_id,cycle FROM cyclic_user_packages WHERE package_id = $pck_id AND user_id in (".implode(",",$userIds).") order by id desc) as table1 group by table1.user_id");
				$userArray = array();

				foreach($cycles as $user){
					$cycle = $user['table1']['cycle'];
					$user_id = $user['table1']['user_id'];
					//$done = true;
					/*if($user['ProductsPackage']['product_id'] == JOKES_PLUS){
						$date = date('d');
						if($pck_id == JOKES_PLUS_FUN && ($date % 4 == 0 || $date % 4 == 1)){
						$done = false;
						}
						else if($pck_id == JOKES_PLUS_SHAYARI && ($date % 4 == 2 || $date % 4 == 3)){
						$done = false;
						}
						}*/
					if(!isset($userArray[$cycle]) || !in_array($arrayUsers[$user_id],$userArray[$cycle])){
						$userArray[$cycle][] = $arrayUsers[$user_id];
					}
					$this->userCyclicPackageUpdate($pck_id,$user_id);
				}


				foreach($userArray as $key => $value){
					$cycle = $key;
					$mobiles = $value;
					//$contentData = $this->Retailer->query("SELECT distinct logs.content,cyclic_packages.id,cyclic_packages.msg_num,cyclic_packages.frequency FROM cyclic_packages INNER JOIN cyclic_refined ON (cyclic_packages.msg_num = cyclic_refined.msg_num AND cyclic_packages.package_id = cyclic_refined.package_id AND cyclic_packages.cycle = cyclic_refined.cycle) INNER JOIN logs ON (logs.message_id = cyclic_refined.message_id) WHERE cyclic_packages.package_id = $pck_id AND cyclic_packages.cycle=$cycle");
					$table = $this->Retailer->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $pck_id AND categories_packages.category_id = data_tables.category_id");
					$table_name = $table['0']['data_tables']['table_name'];
					if($table_name == 'data_funs')$table_name = 'messages';
					$contentData = $this->Retailer->query("SELECT distinct cyclicData,cyclic_packages.id,cyclic_packages.msg_num,cyclic_packages.frequency FROM cyclic_packages INNER JOIN cyclic_refined ON (cyclic_packages.msg_num = cyclic_refined.msg_num AND cyclic_packages.package_id = cyclic_refined.package_id AND cyclic_packages.cycle = cyclic_refined.cycle) INNER JOIN $table_name ON ($table_name.id = cyclic_refined.message_id) WHERE cyclic_packages.package_id = $pck_id AND cyclic_packages.cycle=$cycle AND cyclic_refined.table='$table_name'");

					if(!empty($contentData)){
						$content = $contentData['0'][$table_name]['cyclicData'];
						//if($pck_id == 142)$root = 'modem'; else $root = 'retail';
						$this->General->sendMessage(SMS_SENDER,$mobiles,strip_tags($content),$root);

						$num = $contentData['0']['cyclic_packages']['msg_num'];
						if($cycle > 0 && $num >= CYCLIC_LENGTH*$contentData['0']['cyclic_packages']['frequency']){
							$num = 1;
						}
						else if($cycle == 0 && $num >= TRIAL_CYCLIC_LENGTH*$contentData['0']['cyclic_packages']['frequency']){
							$num = 1;
						}
						else {
							$num++;
						}
						$this->Retailer->query("UPDATE cyclic_packages SET msg_num = $num WHERE id = ". $contentData['0']['cyclic_packages']['id']);
					}

					$this->Retailer->query("UPDATE cyclic_packages SET msg_num = 1 WHERE msg_num = ".(31*$contentData['0']['cyclic_packages']['frequency']));
				}

			}
			$this->releaseLock();
		}
		$this->autoRender = false;
	}

	/*Last 2 day check for a product */

	function unsubscribeProduct($par1,$par2) { //Once a day around 9:30 AM
		set_time_limit(0);
		ini_set("memory_limit","-1");
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$this->ProductsUser->recursive = -1;

			$msgs = $this->ProductsUser->query("SELECT product_id, product_expiry, product_before1, product_after1, product_after2 FROM products_copies");
			$messages = array();
			foreach($msgs as $msg){
				$messages[$msg['products_copies']['product_id']]['product_expiry'] = $msg['products_copies']['product_expiry'];
				$messages[$msg['products_copies']['product_id']]['product_before1'] = $msg['products_copies']['product_before1'];
				$messages[$msg['products_copies']['product_id']]['product_after1'] = $msg['products_copies']['product_after1'];
				$messages[$msg['products_copies']['product_id']]['product_after2'] = $msg['products_copies']['product_after2'];
			}

			// 2 days before expiry
			$time = date('Y-m-d', strtotime(' + 2 days'));
			$userQ = $this->ProductsUser->find('all',array(
					'fields' => array('distinct ProductsUser.user_id'),
					'conditions' => array('ProductsUser.active' => '1','ProductsUser.trial' => '0','Date(ProductsUser.end) = "' .$time.'"')
			));
			foreach($userQ as $userD){
					
				$data2 = $this->ProductsUser->find('all',array(
						'fields' => array('ProductsUser.user_id','User.mobile','User.balance','Product.name','Product.id'),
						'conditions' => array('ProductsUser.active' => '1','ProductsUser.trial' => '0','Date(ProductsUser.end) = "'.$time . '"','ProductsUser.user_id' => $userD['ProductsUser']['user_id']),
						'joins' => array(
				array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'inner',
										'conditions'=> array('ProductsUser.user_id = User.id')
				),
				array(
										'table' => 'products',
										'alias' => 'Product',
										'type' => 'inner',
										'conditions'=> array('ProductsUser.product_id = Product.id')
				))
				));
				foreach($data2 as $user){
					//about to expire
					$msg = $messages[$user['Product']['id']]['product_before1'] ;
					$this->General->sendMessage('',array($user['User']['mobile']),$msg,'retail');
				}
			}
			//Expiry Date
			$time = date('Y-m-d', strtotime(' - 1 days'));
			$userQ = $this->ProductsUser->find('all',array(
					'fields' => array('distinct ProductsUser.user_id'),
					'conditions' => array('ProductsUser.active' => '1','ProductsUser.trial' => '0', 'end is not null','Date(ProductsUser.end) <= "' .$time.'"')
			));
			foreach($userQ as $userD){
				$data1 = $this->ProductsUser->find('all',array(
						'fields' => array('ProductsUser.id','User.mobile','Product.name','Product.price','Product.id','Product.validity'),
						'conditions' => array('ProductsUser.active' => '1','ProductsUser.trial' => '0', 'end is not null' , 'Date(ProductsUser.end) <= "' .$time.'"','ProductsUser.user_id' => $userD['ProductsUser']['user_id']),
						'joins' => array(
				array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'inner',
										'conditions'=> array('ProductsUser.user_id = User.id')
				),
				array(
										'table' => 'products',
										'alias' => 'Product',
										'type' => 'inner',
										'conditions'=> array('ProductsUser.product_id = Product.id')
				)
				)));
				$packDiscon = array();
				$mobile="";
				foreach($data1 as $user){
					$this->ProductsUser->updateAll(array('ProductsUser.active' => '0', 'ProductsUser.end' => "'".date('Y-m-d H:i:s')."'"),
					array('ProductsUser.id' => $user['ProductsUser']['id']));
					//$this->General->makeOptOut247SMS($user['User']['mobile'],1);

					$msg = $messages[$user['Product']['id']]['product_expiry'];
					$this->General->sendMessage(SMS_SENDER,array($user['User']['mobile']),$msg,'retail');
				}
			}
			// 2 days after expiry
			$time = date('Y-m-d', strtotime(' - 2 days'));
			$data1 = $this->ProductsUser->find('all',array(
					'fields' => array('User.mobile','ProductsUser.product_id'),
					'conditions' => array('ProductsUser.trial' => '0'),
					'joins' => array(
			array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'inner',
									'conditions' => array('ProductsUser.user_id = User.id')
			)
			),
					'group' => 'ProductsUser.user_id,ProductsUser.product_id HAVING (MAX(Date(ProductsUser.end)) = "' . $time . '" AND SUM(ProductsUser.active) = 0)'
					)
					);

					foreach($data1 as $user){
						$msg = $messages[$user['ProductsUser']['product_id']]['product_after1'];
						$this->General->sendMessage(SMS_SENDER,$user['User']['mobile'],$msg,'market');
					}

					// 4 days after expiry
					$time = date('Y-m-d', strtotime(' - 4 days'));
					$data1 = $this->ProductsUser->find('all',array(
					'fields' => array('User.mobile','ProductsUser.product_id'),
					'conditions' => array('ProductsUser.trial' => '0'),
					'joins' => array(
					array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'inner',
									'conditions' => array('ProductsUser.user_id = User.id')
					)
					),
					'group' => 'ProductsUser.user_id,ProductsUser.product_id HAVING (MAX(Date(ProductsUser.end)) = "' . $time . '" AND SUM(ProductsUser.active) = 0)'
					)
					);

					foreach($data1 as $user){
						$message = substr(strip_tags($this->General->getLastMessageSent($user['ProductsUser']['product_id'])),0,160);
						$msg = $message . "\n" . $messages[$user['ProductsUser']['product_id']]['product_after2'];
						$this->General->sendMessage(SMS_SENDER,$user['User']['mobile'],$msg,'market');
					}
					$this->releaseLock();
		}
		$this->autoRender = false;
	}

	function unsubscribeTrialProduct($par1,$par2) { //Once a day around 9:30 AM
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$this->ProductsUser->recursive = -1;
			$msgs = $this->ProductsUser->query("SELECT product_id, trial_after1, trial_after2 FROM products_copies");
			$messages = array();
			foreach($msgs as $msg){
				$messages[$msg['products_copies']['product_id']]['trial_after1'] = $msg['products_copies']['trial_after1'];
				$messages[$msg['products_copies']['product_id']]['trial_after2'] = $msg['products_copies']['trial_after2'];
			}
			//Expired today
			$time = date('Y-m-d', strtotime(' - 1 days'));
			$userQ = $this->ProductsUser->find('all',array(
					'fields' => array('distinct ProductsUser.user_id'),
					'conditions' => array('ProductsUser.active' => '1','ProductsUser.trial' => '1','end is not null','Date(ProductsUser.end) <= "' .$time.'"')
			));
			foreach($userQ as $userD){
				$data1 = $this->ProductsUser->find('all',array(
						'fields' => array('ProductsUser.id','User.mobile','Product.name','Product.price','Product.id','Product.validity'),
						'conditions' => array('ProductsUser.active' => '1','ProductsUser.trial' => '1','end is not null','Date(ProductsUser.end) <= "' .$time.'"','ProductsUser.user_id' => $userD['ProductsUser']['user_id']),
						'joins' => array(
				array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'inner',
										'conditions'=> array('ProductsUser.user_id = User.id')
				),
				array(
										'table' => 'products',
										'alias' => 'Product',
										'type' => 'inner',
										'conditions'=> array('ProductsUser.product_id = Product.id')
				)
				)));
				$packDiscon = array();
				$mobile="";
				foreach($data1 as $user){
					$packDiscon[] = $user['Product']['name'];
					$this->ProductsUser->updateAll(array('ProductsUser.active' => '0', 'ProductsUser.end' => "'".date('Y-m-d H:i:s')."'"),
					array('ProductsUser.id' => $user['ProductsUser']['id']));
					$mobile = $user['User']['mobile'];
				}
				if(count($packDiscon)> 0){
					$msg = "Dear User,
					Your trial for product(s) '".implode(", ",$packDiscon)."' expired today.
					Aaj hi activate kijiye !! SMSTadka cards are available at nearby store in your area.
					Najdiki SMSTadka Shop jaanne ke liye, SMS: SHOP to 0".VIRTUAL_NUMBER.".";
					$this->General->sendMessage(SMS_SENDER,array($mobile),$msg,'retail');
				}
			}


			//2 days after expiry
			$time = date('Y-m-d', strtotime(' - 2 days'));
			$data1 = $this->ProductsUser->find('all',array(
					'fields' => array('User.mobile','ProductsUser.product_id'),
					'conditions' => array('ProductsUser.active' => '0','ProductsUser.trial' => '1','Date(ProductsUser.end) = "' .$time.'"'),
					'joins' => array(
			array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'inner',
									'conditions' => array('ProductsUser.user_id = User.id')
			)
			),
					'group' => 'ProductsUser.user_id,ProductsUser.product_id HAVING (COUNT(ProductsUser.id) = 1)'
					)
					);

					foreach($data1 as $user){
						$msg = $messages[$user['ProductsUser']['product_id']]['trial_after1'];
						$this->General->sendMessage(SMS_SENDER,$user['User']['mobile'],$msg,'market');
					}

					//4 days after expiry
					$time = date('Y-m-d', strtotime(' - 4 days'));
					$data1 = $this->ProductsUser->find('all',array(
					'fields' => array('User.mobile','ProductsUser.product_id'),
					'conditions' => array('ProductsUser.active' => '0','ProductsUser.trial' => '1','Date(ProductsUser.end) = "' .$time.'"'),
					'joins' => array(
					array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'inner',
									'conditions' => array('ProductsUser.user_id = User.id')
					)
					),
					'group' => 'ProductsUser.user_id,ProductsUser.product_id HAVING (COUNT(ProductsUser.id) = 1)'
					)
					);

					foreach($data1 as $user){
						$message = substr(strip_tags($this->General->getLastMessageSent($user['ProductsUser']['product_id'])),0,160);
						$msg = $message . "\n" . $messages[$user['ProductsUser']['product_id']]['trial_after2'];
						$this->General->sendMessage(SMS_SENDER,$user['User']['mobile'],$msg,'market');
					}
		}
		$this->releaseLock();
		$this->autoRender = false;
	}

	function generateCoupons($product,$total,$expiry){
		$this->autoRender = false;
		set_time_limit(0);
		ini_set("memory_limit","-1");
		$i = 0;
		$j = 0;
		
		//Configure::write('debug', 2);
		
		if(empty($_SESSION['Auth'])){
			echo "You are not authorized"; exit;
		}
		
		$prefix_data = $this->ProductsUser->query("SELECT max(serialNumber) as sno,products.name FROM  products left join coupons ON (products.id = product_id) WHERE product_id = $product");
		
		if(empty($prefix_data)) {
			echo "Not possible to generate coupons"; exit;
		}
		if(!empty($prefix_data['0']['0']['sno'])){
			$prefix = (int)$prefix_data['0']['0']['sno'] + 1;
		}
		else {
			$prefix = "6" . $product . "00001";
			$prefix = (int)$prefix;
		}
		
		$product_name = $prefix_data['0']['products']['name'];
		
		App::import('Helper','csv');
		$this->layout = null;
		$csv = new CsvHelper();
		$line = array('SerialNumber','Code');
		$csv->addRow($line);
		
		while($i < $total){
			$num = mt_rand(100,999);
			$num1 = mt_rand(10000,99999);
			$primeNum = $this->findNextPrime($num1);

			$sno = $prefix + $i;
			$code = (int)($num . "" . $primeNum);
			
			$this->data['Coupon']['serialNumber'] = $sno;
			$this->data['Coupon']['code'] = $code;
			$this->data['Coupon']['product_id'] = $product;
			$this->data['Coupon']['dry_flag'] = 2;
			$this->data['Coupon']['expiry_date'] = $expiry;
			$this->Coupon->create();
			if($this->Coupon->save($this->data)){
				$i++;
				$csv->addRow(array($sno,$code));
			}
			else {
				$j++;
			}
		}
		
		$this->General->mailToUsers("Coupons generated for Infibeam","Coupons created: $total ($prefix - ".($prefix+$total -1).")<br/>Product: $product_name<br/>Expiry date set: $expiry",array('nivedita@mindsarray.com','tadka@mindsarray.com'));
		
		echo $csv->render($product."_coupons_".$total.".csv");
		
	}


	function findNextPrime($number){
		$num = (int)sqrt($number);
		$i = 2;
		while($i <= $num){
			if($number % $i == 0) break;
			$i++;
		}
		if($i <= $num){
			$number = $number + 1;
			return $this->findNextPrime($number);
		}
		else {
			return $number;
		}
	}

	function getLastComments(){
		$data = $this->Retailer->query("SELECT comment,comments.created,retailers.name,retailers.id from comments inner join retailers on (retailers.id = comments.itemid) where comments.id in (SELECT max(id) from comments group by itemid) and comments.itemtype= 1 order by comments.id desc");
		$this->set('data',$data);
		$this->render('comments');
	}

	function mobileScriptCheck(){
		$this->Retailer->query("Update mobile_script set last_time = '".date("Y-m-d H:i:s")."'");
		$this->autoRender = false;
	}

	function checkIfWorking(){
		$this->lock();
		$time = date('Y-m-d H:i:s', strtotime(' - 30 minutes'));
		$data = $this->Retailer->query("SELECT * FROM mobile_script WHERE last_time > '" . $time . "'");
		if(empty($data)){
			$msg = date('Y-m-d H:i:s') . ' Reliance Mobile Script Not Working';
			//$this->General->mailToAdmins($msg, '');
			$this->General->sendMessage(SMS_SENDER,array('9819032643'),$msg,'retail');
		}
		$this->releaseLock();
		$this->autoRender = false;
	}

	function cyclicData($package_id,$cycle,$range=null){

		$table = $this->Package->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $package_id AND categories_packages.category_id = data_tables.category_id");
		$table_name = $table['0']['data_tables']['table_name'];
		if($table_name == 'data_funs')$table_name = 'messages';

		$pck_name = $this->Package->findById($package_id);
		if($cycle == 'all')
		$ids = $this->Retailer->query("SELECT GROUP_CONCAT(message_id) as ids FROM cyclic_refined WHERE cyclic_refined.package_id = $package_id ORDER BY cycle,msg_num");
		else
		$ids = $this->Retailer->query("SELECT GROUP_CONCAT(message_id) as ids FROM cyclic_refined WHERE cyclic_refined.package_id = $package_id AND cyclic_refined.cycle = $cycle ORDER BY msg_num");

		if($cycle == 'all')
		$data = $this->Retailer->query("SELECT table1.id,table1.cyclicData,cyclic_refined.cycle FROM cyclic_refined INNER JOIN $table_name as table1 ON (table1.id = cyclic_refined.message_id) WHERE cyclic_refined.package_id = $package_id AND cyclic_refined.table = '$table_name' ORDER by Field(cyclic_refined.message_id,".$ids['0']['0']['ids'].")");
		else
		$data = $this->Retailer->query("SELECT table1.id,table1.cyclicData,cyclic_refined.cycle FROM cyclic_refined INNER JOIN $table_name as table1 ON (table1.id = cyclic_refined.message_id) WHERE cyclic_refined.package_id = $package_id AND cyclic_refined.cycle = $cycle AND cyclic_refined.table = '$table_name' ORDER by Field(cyclic_refined.message_id,".$ids['0']['0']['ids'].")");

		$i = 0;
		foreach($data as $d){
			$data[$i]['table1']['length'] = strlen(strip_tags($d['table1']['cyclicData']));
			$i++;
		}
		//$data = $this->Retailer->query("SELECT logs.content,logs.message_id FROM logs WHERE logs.message_id in (".$ids['0']['0']['ids'].")  GROUP BY logs.message_id ORDER by Field(logs.message_id,".$ids['0']['0']['ids'].")");
		if($range != null){
			$rangeArr = explode("-",$range);
			$this->set('min',$rangeArr[0]);
			$this->set('max',$rangeArr[1]);
			$this->set('range',$range);
		}
		$this->set('data',$data);
		$this->set('name',$pck_name['Package']['name']);
	}

	function editCyclicData($package_id,$cycle,$range=null){
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/users/view');
		$table = $this->Package->query("SELECT table_name FROM categories_packages,data_tables WHERE categories_packages.package_id = $package_id AND categories_packages.category_id = data_tables.category_id");
		$table_name = $table['0']['data_tables']['table_name'];
		if($table_name == 'data_funs')$table_name = 'messages';
		$user_id = $this->Session->read('Auth.User.id');
		$pck_name = $this->Package->findById($package_id);
		if($cycle == 'all')
		$ids = $this->Retailer->query("SELECT GROUP_CONCAT(message_id) as ids FROM cyclic_refined WHERE cyclic_refined.package_id = $package_id ORDER BY cycle,msg_num");
		else
		$ids = $this->Retailer->query("SELECT GROUP_CONCAT(message_id) as ids FROM cyclic_refined WHERE cyclic_refined.package_id = $package_id AND cyclic_refined.cycle = $cycle ORDER BY msg_num");

		if($cycle == 'all')
		$data = $this->Retailer->query("SELECT content.id,content.cyclicData,cyclic_refined.cycle,refineddata.rate FROM cyclic_refined INNER JOIN $table_name as content ON (content.id = cyclic_refined.message_id) LEFT JOIN refineddata ON (refineddata.message_id = cyclic_refined.message_id AND refineddata.package_id = cyclic_refined.package_id AND refineddata.user_id = $user_id) WHERE cyclic_refined.package_id = $package_id AND cyclic_refined.table = '$table_name' ORDER by Field(cyclic_refined.message_id,".$ids['0']['0']['ids'].")");
		else
		$data = $this->Retailer->query("SELECT content.id,content.cyclicData,cyclic_refined.cycle,refineddata.rate FROM cyclic_refined INNER JOIN $table_name as content ON (content.id = cyclic_refined.message_id) LEFT JOIN refineddata ON (refineddata.message_id = cyclic_refined.message_id AND refineddata.package_id = cyclic_refined.package_id AND refineddata.user_id = $user_id) WHERE cyclic_refined.package_id = $package_id AND cyclic_refined.cycle = $cycle AND cyclic_refined.table = '$table_name' ORDER by Field(cyclic_refined.message_id,".$ids['0']['0']['ids'].")");

		$i = 0;
		foreach($data as $d){
			$data[$i]['content']['length'] = strlen(strip_tags($d['content']['cyclicData']));
			$i++;
		}
		//$data = $this->Retailer->query("SELECT logs.content,logs.message_id FROM logs WHERE logs.message_id in (".$ids['0']['0']['ids'].")  GROUP BY logs.message_id ORDER by Field(logs.message_id,".$ids['0']['0']['ids'].")");
		if($range != null){
			$rangeArr = explode("-",$range);
			$this->set('min',$rangeArr[0]);
			$this->set('max',$rangeArr[1]);
			$this->set('range',$range);
		}
		$this->set('data',$data);
		$this->set('name',$pck_name['Package']['name']);
		$this->set('package_id',$package_id);
		$this->render('refined_data');
	}

	function product($id){
		$this->Product->recursive = -1;
		$product = $this->Product->findById($id);
		$this->ProductsUser->recursive = -1;
		
		if($_SESSION['Auth']['User']['group_id'] == ADMIN){
		

			$active = $this->ProductsUser->find('all',array('fields' => array('User.mobile,min(ProductsUser.start) as minDate,max(ProductsUser.end) as maxDate,sum(ProductsUser.count) as counts,sum(ProductsUser.trial) as trial'),'conditions' => array('ProductsUser.product_id' => $id),
					'joins' => array(
			array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'inner',
									'conditions' => array('User.id = ProductsUser.user_id')
			)
			),
					'group' => array('ProductsUser.user_id having sum(active) = 1'),
					'order' => array('counts desc')
			));
	
			$inactive = $this->ProductsUser->find('all',array('fields' => array('User.mobile,min(ProductsUser.start) as minDate,max(ProductsUser.end) as maxDate,sum(ProductsUser.count) as counts,sum(ProductsUser.trial) as trial'),'conditions' => array('ProductsUser.product_id' => $id),
					'joins' => array(
			array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'inner',
									'conditions' => array('User.id = ProductsUser.user_id')
			)
			),
					'group' => array('ProductsUser.user_id having sum(active) = 0'),
					'order' => array('counts desc')
			));

		}
		else {
			$this->redirect('/users/view');
		}
		$this->set('product',$product);
		$this->set('active',$active);
		$this->set('inactive',$inactive);
	}

	function superDistributor($id){
		$super_distributor = $this->SuperDistributor->findById($id);

		$payments = $this->ShopTransaction->find('all',array('fields' => array('ShopTransaction.amount,ShopTransaction.timestamp,User.name'), 'conditions' => array('ref2_id' => $id,'type' => ADMIN_TRANSFER),
				'joins' => array(
		array (
								'table' => 'users',
								'alias' => 'User',
								'type' => 'inner',
								'conditions' => array('ShopTransaction.ref1_id = User.id')
		)
		)
		));
		$products = $this->RetailersCoupon->find('all',array('fields' => array('Product.name','count(Coupon.id) as counts'),'conditions' => array('superdistributor_id' => $id),
				'joins' => array(
		array (
								'table' => 'coupons',
								'alias' => 'Coupon',
								'type' => 'inner',
								'conditions' => array('RetailersCoupon.coupon_id = Coupon.id')
		),
		array (
								'table' => 'products',
								'alias' => 'Product',
								'type' => 'inner',
								'conditions' => array('Product.id = Coupon.product_id')
		)
		),
				'group' => 'Coupon.product_id'
				));
				$comments = $this->Retailer->query("SELECT users.name,comments.comment,comments.created from super_distributors as u1, users, comments where comments.itemid= u1.id and u1.id= $id and comments.itemtype=3 and users.id = comments.user_id order by comments.id desc");

				$added = $this->Retailer->query("SELECT products.name, COUNT(coupon_id) as total, Date(retailers_coupons.created) as date from retailers_coupons inner join coupons on (retailers_coupons.coupon_id = coupons.id) inner join products on (product_id = products.id) where superdistributor_id = $id group by Date(retailers_coupons.created),product_id order by retailers_coupons.created desc");
					
				$this->set(compact('comments','added','payments','products','super_distributor'));


	}

	function transferSuperDistributorBalance($id,$user_id){
		$this->Shop->shopTransactionUpdate(ADMIN_TRANSFER,$this->data['ShopTransaction']['amount'],$this->Session->read('Auth.User.id'),$id);
		$this->Shop->shopBalanceUpdate($this->data['ShopTransaction']['amount'],'add',$id,SUPER_DISTRIBUTOR);
		$this->redirect(array('action' => 'superDistributor',$id));
	}

	function assignSuperDistributorCoupons($id){
		$ids = explode(",",$this->data['numbers']);
		$count = 0;
		$already_assigned = array();
		foreach($ids as $coupons) {
			$coupons = trim($coupons);
			$id_list = explode("-",$coupons);

			if(count($id_list) == 1) $id_list[1] = $id_list[0];

			for($i = trim($id_list[0]); $i <= trim($id_list[1]); $i++){
				$coupon_id = $this->Retailer->query("SELECT id from coupons where serialNumber = $i");
				$this->RetailersCoupon->recursive = -1;
				$data = $this->RetailersCoupon->find('first',array('conditions' => array('RetailersCoupon.coupon_id' => $coupon_id['0']['coupons']['id'])));
				if(empty($data)){
					$this->data['RetailersCoupon']['coupon_id'] = $coupon_id['0']['coupons']['id'];
					$this->data['RetailersCoupon']['superdistributor_id'] = $id;
					$this->data['RetailersCoupon']['created'] = date('Y-m-d H:i:s');
					$this->data['RetailersCoupon']['modified'] = date('Y-m-d H:i:s');

					$this->RetailersCoupon->create();
					if($this->RetailersCoupon->save($this->data))
					{
						$count++;
					}
				}
				else {
					$already_assigned[] = $i;
				}
				$this->Retailer->query("UPDATE coupons SET dry_flag = 0 where serialNumber = $i");
			}
		}
		$msg = $count . ' coupons added successfully.';
		if(!empty($already_assigned)){
			$msg .= " Following coupons already assigned: " . implode(",",$already_assigned);
		}
		$this->Session->setFlash(__($msg, true));
		$this->redirect(array('action' => 'superDistributor',$id));
	}

	function salesman($id){
		$this->Retailer->recursive = -1;
		$salesman = $this->Retailer->query("SELECT * from salesmans as Salesman where id = " .$id);
		$retailers = $this->Retailer->find('all',array('conditions' => array('Retailer.salesman_id' => $id)));

		$payments = $this->Retailer->query("SELECT salesmans_payments.amount,salesmans_payments.type, users.name,salesmans_payments.timestamp from salesmans_payments inner join users on (users.id = user_id) where salesman_id = $id order by salesmans_payments.timestamp desc");
		$comments = $this->Retailer->query("SELECT users.name,comments.comment,comments.created from salesmans as u1, users, comments where comments.itemid= u1.id and u1.id= $id and comments.itemtype=2 and users.id = comments.user_id order by comments.id desc");
			
		$this->set('salesman',$salesman['0']);
		$this->set('retailers', $retailers);
		$this->set('payments', $payments);
		$this->set('comments', $comments);
	}

	function addSalesmanPayment($id){
		$this->data['SalesmansPayment']['salesman_id'] = $id;
		$this->data['SalesmansPayment']['user_id'] = $this->Session->read('Auth.User.id');
		$this->data['SalesmansPayment']['timestamp'] = date('Y-m-d H:i:s');

		if (!empty($this->data)) {
			$this->SalesmansPayment->create();
			if ($this->SalesmansPayment->save($this->data)) {
				$this->redirect(array('action' => 'salesman',$id));
			}
		}
	}


	function msgAfterSubscription($par1,$par2){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			$this->lock();
			$this->ProductsPackage->recursive = -1;
			$calls = $this->ProductsPackage->query("SELECT id,params FROM asynchronous_calls WHERE controller = 'retailer' LIMIT 10");

			$array = array();
			$ids = array();
			foreach($calls as $call){
				$param = json_decode($call['asynchronous_calls']['params']);
				$ids[] = $call['asynchronous_calls']['id'];
				$array[$param->product][] = $param->receiver;
			}

			foreach($array as $key => $value){
				$product_id = $key;
				$mobile_numbers = $value;
				$message = $this->General->getLastMessageSent($product_id);
				if(!empty($message) && !empty($mobile_numbers)){
					$this->General->sendMessage(SMS_SENDER,$mobile_numbers,strip_tags($message),'pacM');
				}
			}
			if(!empty($calls))
			$this->ProductsPackage->query("DELETE FROM asynchronous_calls WHERE id in (".implode(",",$ids).")");
			$this->releaseLock();
			$this->autoRender = false;
		}
	}

	function switchProduct(){
		$type = $_REQUEST['type'];
		$option = $_REQUEST['option'];
		$user_id = $_REQUEST['user_id'];
		$mobile = $_REQUEST['mobile'];
		if(strtoupper($type) == 'TV'){
			$product_id = WOI_PRODUCT;
			$limit = $option - 1;
			$prod = $this->Retailer->query("SELECT id,name FROM products WHERE parent_id = $product_id LIMIT $limit,1");
			if(empty($prod)){
				$prod_data = $this->Retailer->query("SELECT id,name FROM products WHERE parent_id = $product_id ORDER BY id asc");
				$i = 1;
				foreach($prod_data as $prod){
					$msg .= "$i - " . $prod['products']['name']."\n";
					$i++;
				}
				$sms = "Sorry, You have sent a wrong option to us.\n".$msg."To switch to 2nd option, send SMS: ACT $type 2 to 0".VIRTUAL_NUMBER.
				"\nFor more help, Send HELP to 0".VIRTUAL_NUMBER.".";
				$mail['subject'] = "User Trying to Switch: Wrong Code Sent";
				$mail['body'] = "$mobile: Message sent ACT $type $option";
			}
			else {
				$check = $this->Retailer->query("SELECT id FROM products_users WHERE product_id = $product_id AND user_id = $user_id AND active = 1");
				if(empty($check)){
					$sms = $this->General->createMessage("PRODUCT_NOT_SUBSCRIBED",array("0".VIRTUAL_NUMBER));
					$root = 'template';
					$mail['subject'] = "User Trying to Switch: Not Subscribed";
					$mail['body'] = "$mobile: Message sent ACT $type $option";
				}
				else {
					$this->General->updateProductSwitch($product_id,$user_id,$prod['0']['products']['id'],'change');
					$sms = $this->General->createMessage("PRODUCT_WOI_SWITCH",array($prod['0']['products']['name']));
					$root = 'template';
					$mail['subject'] = "User Switched product";
					$mail['body'] = "Mobile: $mobile, Switched to " . $prod['0']['products']['name'];
				}
			}

		}
		else {
			$sms = $this->General->createMessage("INVALID_CODE",array("0".VIRTUAL_NUMBER));
			$root = 'template';
			$mail['subject'] = "User Trying to Switch: Wrong Code Sent";
			$mail['body'] = "Mobile: $mobile. Message sent ACT $type $option\n";
		}

		$url = SERVER_BACKUP . 'users/sendMsgMails';
		$data = array();

		//$data['mail_subject'] = $mail['subject'];
		$data['mail_body'] = $mail['body'];
		$data['sms'] =  $sms;
		if(isset($root))$data['root'] = $root;
		else $data['root'] =  'retail';
		$data['mobile'] = $mobile;
		$this->General->curl_post_async($url,$data);

	}


	function getCouponInfo($code=null,$serial=null){
		
		
		if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == 3){
			$toshow = false;
			if($code != -1 && $code != null){
				$query = "coupons.code = '$code' ";
				$toshow = true;
			}
			if($serial != null){
				$query = "coupons.serialNumber = '$serial' ";
				$toshow = true;
			}
			$data = array();
			if($toshow){
				$query = "SELECT coupons.*,retailers_coupons.*,users.mobile,products.name,retailers.id,retailers.name,retailers.shopname,distributors.id,distributors.company,super_distributors.id,super_distributors.company FROM coupons LEFT JOIN retailers_coupons ON (retailers_coupons.coupon_id = coupons.id)
				LEFT JOIN users ON (retailers_coupons.user_id = users.id) LEFT JOIN retailers ON (retailers_coupons.retailer_id = retailers.id) LEFT JOIN distributors ON (retailers_coupons.distributor_id = distributors.id) LEFT JOIN super_distributors ON (retailers_coupons.superdistributor_id = super_distributors.id)
				LEFT JOIN products ON (coupons.product_id = products.id) WHERE $query";
				$data = $this->Retailer->query($query);
			}
                        
			$this->set('code',$code);
			$this->set('serial',$serial);
			$this->set('data',$data);
			//$this->printArray($data);
		}
		else {
			$this->redirect('/users/view');
		}
		$this->layout = 'shop';
	}

	function addUsersToCycle($package_id){
		if($this->Session->read('Auth.User.group_id') != ADMIN)$this->redirect('/users/view');
		$query = "SELECT `packages_users`.*  FROM `packages_users`,packages WHERE `active` = 1 AND packages.id = package_id and package_id = $package_id AND cycle_ids is not null";
		$data = $this->Retailer->query($query);
		$users = array();
		foreach($data as $dt){
			if($dt['packages_users']['trial_flag'] == 1){
				$cycle = 0;
			}
			else {
				$cycle = $this->General->getNewCycle($package_id,$dt['packages_users']['user_id']);
			}
			$users[$dt['packages_users']['user_id']] = $cycle;
		}

		$query = "SELECT `products_users`.*  FROM `products_users`,products_packages WHERE `products_users`.`active` = 1 AND products_packages.package_id = $package_id AND products_users.product_id = products_packages.product_id and cycle_ids is not null";
		$data = $this->Retailer->query($query);
		foreach($data as $dt){
			if(($dt['products_users']['count'] - $dt['products_users']['trial']) == 0){
				$cycle = 0;
			}
			else {
				$cycle = $this->General->getNewCycle($package_id,$dt['products_users']['user_id']);
			}
			if(!isset($users[$dt['products_users']['user_id']]) || (isset($users[$dt['products_users']['user_id']]) && $users[$dt['products_users']['user_id']] == 0 && $cycle !=0)){
				$users[$dt['products_users']['user_id']] = $cycle;
			}
		}
		foreach($users as $key=>$value){
			$this->Retailer->query("INSERT INTO cyclic_user_packages (package_id,user_id,cycle,count) VALUES (".$package_id.",$key,$value,0)");
		}

		$this->autoRender = false;
	}

	function test(){
		echo "1"; exit;
	}
}