<?php
class StocksController extends AppController {
	
	var $name = 'Stocks';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $components = array('RequestHandler');
	var $uses = array('Stock','SMSApp');
	var $controller_name = 'stock';
	
	function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('*');
		$this->Auth->allowedActions = array('about','cronSendingStockPrice','cronSendingStockNews','cronToTakeOutMoney','cronContinueStocks','getStockData','getFeedNews','addFeeds','convertTo320AndSend','sendStockSMS','setNewsPage','stockNews');
	}
	
	function initial($about = null){
		if($about == null)
			$about = $_REQUEST['about'];
		$this->set('about',$about);
		$this->render('initial');
	}
	
	function createAlert(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		$this->render('create_alert');
	}
	
	function createStockAlert(){
		$code = trim($_REQUEST['stock_code']);
		$comp = trim($_REQUEST['company']);
		$nse_flag = substr($code,0,1);
		$code = substr($code,1);
		$news_flag = $_REQUEST['news_flag'];
		
		$this->Stock->recursive = -1;
		
		if($nse_flag) {
			$query = "SELECT id,company from stock_companies where nse_code = '$code' and company = '".$comp."' and nse_flag = 1";
		}
		else {
			$query = "SELECT id,company from stock_companies where bse_code = '$code' and company = '".$comp."' and bse_flag = 1";
		}
		
		$company = $this->Stock->query($query);
		
		if(!empty($company)){
			$stock_company = "NSE: " . $company['0']['stock_companies']['company'];
		
			$stock_data = $this->Stock->find('first',array('conditions' => array('stock_code' => $code,'user_id' => $this->Session->read('Auth.User.id'),'nse_flag' => $nse_flag, '(status_flag = 1 or (status_flag = 0 and del_flag = 1))')));
			
			if(empty($stock_data) || $stock_data['Stock']['status_flag'] == '0'){
				$this->SMSApp->recursive= -1;
				$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
	
				if($news_flag){
					$basic_price = APP_STOCK_PRICE_WITH_NEWS;
				}
				else {
					$basic_price = APP_STOCK_PRICE;
				}
				$price = $this->General->getBalance($this->Session->read('Auth.User.id')) - $basic_price;
				if($price < 0){
					echo '0';
					$this->autoRender = false;
				}
				else {
					if(empty($stock_data)){
						$this->data['Stock']['user_id'] = $this->Session->read('Auth.User.id');
						$this->data['Stock']['stock_code'] = $code;
						$this->data['Stock']['company_id'] = $company['0']['stock_companies']['id'];
						$this->data['Stock']['nse_flag'] = $nse_flag;
						$this->data['Stock']['news_flag'] = $news_flag;
						$this->data['Stock']['status_flag'] = 1;
						$this->data['Stock']['price'] = $basic_price;
						$this->data['Stock']['start'] = date('Y-m-d H:i:s');
						
						$this->Stock->create();
						$this->Stock->save($this->data);
						$stock_id = $this->Stock->id;
						$this->General->makeOptIn247SMS($this->Session->read('Auth.User.mobile'),1);
				
					}
					else if($stock_data['Stock']['status_flag'] == '0'){
						$this->Stock->query("UPDATE stocks set news_flag=$news_flag,status_flag=1,price=$basic_price,temp_flag=0,del_flag=0,start='" . date("Y-m-d H:i:s"). "',end=NULL where id= ". $stock_data['Stock']['id']);
						$stock_id = $stock_data['Stock']['id'];
					}
					
					$bal = $this->General->balanceUpdate($basic_price,'subtract');

					//make an entry in transactions table
					//$bal = $this->General->getBalance($this->Session->read('Auth.User.id'));
					$this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$basic_price,$data['SMSApp']['id'],$stock_id,null);
					
					if($nse_flag){
						$stock_company = "NSE: " . $company['0']['stock_companies']['company'];
					}
					else {
						$stock_company = "BSE: " . $company['0']['stock_companies']['company'];
					}
					
					$params = array();
					$params['stock_code'] = $code;
					$params['company_id'] = $company['0']['stock_companies']['id'];
					$params['nse_flag'] = $nse_flag;
					$params['news_flag'] = $news_flag;
					$params['id'] = $stock_id;
					$params['mobile'] = $this->Session->read('Auth.User.mobile');
					$params['userId'] = $this->Session->read('Auth.User.id');
					$this->General->addAsynchronousCall($_REQUEST['random'], $this->controller_name,'createStockAlert',$params);
					
					//make an entry in app_users table
					$this->General->appUsersUpdate($data['SMSApp']['id']);
					
					//to calculate days its gonna survive
					$days = $this->calculateDays($bal);
					$this->set('days',$days);
					$this->set('company',$stock_company);
					$this->set('price',$basic_price);
					$this->set('balance',$bal);
					$this->render('/stocks/confirmed_alert','ajax');
				
				}
			}
			else {
				echo '1';
				$this->autoRender = false;
			}
		}
		else {
			echo '2';
			$this->autoRender = false;
		}	
	}
	
	function after_createStockAlert($pass){
		$pars = json_decode($pass,true);
		
		$this->convertTo320AndSend(array($pars),$pars['id'],$pars['mobile'],1,1,0,$pars['userId']);
		
		$mail_body = $pars['mobile'] . " has created a stock alert";
		//$this->General->mailToAdmins("Personal Alert Subscribed: Stock Alert", $mail_body);
		$this->autoRender = false;
	}
	
	function autoCompleteCompany(){
		$name = $this->data['Stock']['company'];
		$this->Stock->recursive = -1;
		$data = $this->Stock->query("SELECT * from stock_companies where company like '".$name."%'");
		$this->set('companies',$data);
		$this->render('auto_complete_stock');
	}
	
	function enableStockAlert(){
		$id = $_REQUEST['id'];
		$this->Stock->recursive = -1;
		
		$stock = $this->Stock->find('first',array('conditions' => array('id' => $id,'status_flag' => 0)));
		if(!empty($stock)){
			$news_flag = $stock['Stock']['news_flag'];
			if($news_flag){
				$basic_price = APP_STOCK_PRICE_WITH_NEWS;
			}
			else {
				$basic_price = APP_STOCK_PRICE;
			}
			$price = $this->General->getBalance($this->Session->read('Auth.User.id')) - $basic_price;
			if($price < 0){
				echo '1'; //not have enough balance
			}
			else {
				$this->General->makeOptIn247SMS($this->Session->read('Auth.User.mobile'),1);
				
				$this->Stock->query("UPDATE stocks SET status_flag = 1,temp_flag=0,del_flag=0,start='".date('Y-m-d H:i:s')."',end=NULL where id = $id");
				$this->General->balanceUpdate($basic_price,'subtract');
				
				$this->SMSApp->recursive= -1;
				$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
			
				//make an entry in transactions table
				$this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$basic_price,$data['SMSApp']['id'],$id,null);
				
				echo '2'; //enabled script
				echo "<script> reloadBalance('".$price."'); </script>";
				
				$params = array();
				$params['stock_code'] = $stock['Stock']['stock_code'];
				$params['company_id'] = $stock['Stock']['company_id'];
				$params['nse_flag'] = $stock['Stock']['nse_flag'];
				$params['news_flag'] = $stock['Stock']['news_flag'];
				$params['id'] = $stock['Stock']['id'];
				$params['mobile'] = $this->Session->read('Auth.User.mobile');
				$params['userId'] = $this->Session->read('Auth.User.id');
				$this->General->addAsynchronousCall($_REQUEST['random'], $this->controller_name,'enableStockAlert',$params);
			}
		}
		else {
			echo '0'; //Wrong data
		}
		$this->autoRender = false;
	}
	
	function after_enableStockAlert($pass){
		$pars = json_decode($pass,true);
		$this->convertTo320AndSend(array($pars),$pars['id'],$pars['mobile'],1,1,0,$pars['userId']);
		$this->autoRender = false;
	}
	
	function disableStockAlert(){
		$id = $_REQUEST['id'];
		$this->Stock->recursive = -1;
		$count = $this->Stock->find('count',array('conditions' => array('id' => $id, 'status_flag' => 1)));
		if($count == 0){
			echo "0"; //Wrong data
		}
		else {
			//$this->General->makeOptOut247SMS($this->Session->read('Auth.User.mobile'),1);
			$this->Stock->query("UPDATE stocks SET status_flag=0,end='".date('Y-m-d H:i:s')."' where id = $id");
			echo "1"; //Updated
		}
		$this->autoRender = false;
	}
	
	function editStockAlert(){
		$id = $_REQUEST['id'];
		$news_flag = $_REQUEST['news_flag'];
		$this->Stock->recursive = -1;
		$stock = $this->Stock->find('first',array('conditions' => array('id' => $id)));
		if($stock['Stock']['news_flag'] == $news_flag){
			echo "0"; //Already done
		}
		else {
			if($news_flag == 0){
				$this->Stock->query("UPDATE stocks SET news_flag = $news_flag,price=".APP_STOCK_PRICE." where id = $id");
			} else if($news_flag == 1){
				$this->Stock->query("UPDATE stocks SET news_flag = $news_flag,price=".APP_STOCK_PRICE_WITH_NEWS." where id = $id");
				
				$basic_price = APP_STOCK_PRICE_WITH_NEWS - APP_STOCK_PRICE;
				$bal = $this->General->balanceUpdate($basic_price,'subtract'); //cut extra 75 paise from users account
				
				$this->SMSApp->recursive= -1;
				$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
			
				//make an entry in transactions table
				$this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$basic_price,$data['SMSApp']['id'],$id,null);
				//$bal = $this->General->getBalance($this->Session->read('Auth.User.id'));
						
				echo "<script> reloadBalance('".$bal."'); </script>";
				
				$params = array();
				$params['stock_code'] = $stock['Stock']['stock_code'];
				$params['company_id'] = $stock['Stock']['company_id'];
				$params['nse_flag'] = $stock['Stock']['nse_flag'];
				$params['news_flag'] = $news_flag;
				$params['id'] = $id;
				$params['mobile'] = $this->Session->read('Auth.User.mobile');
				$params['userId'] = $this->Session->read('Auth.User.id');
				$this->General->addAsynchronousCall($_REQUEST['random'], $this->controller_name,'editStockAlert',$params);
			}
			echo "1"; //Updated
		}
		$this->autoRender = false;
	}
	
	function after_editStockAlert($pass){
		$pars = json_decode($pass,true);
		$this->convertTo320AndSend(array($pars),$pars['id'],$pars['mobile'],0,1,0,$pars['userId']);
		$this->autoRender = false;
	}
	
	function removeAlert(){
		$id = $_REQUEST['id'];
		//$this->General->makeOptOut247SMS($this->Session->read('Auth.User.mobile'),1);
		$this->Stock->query("UPDATE stocks SET status_flag=0,del_flag=1,end='".date('Y-m-d H:i:s')."' WHERE id = $id");
		$this->autoRender = false;
	}
	
	function calculateDays($bal){
		$data = $this->Stock->query('SELECT sum(price) as price from stocks where user_id = '. $this->Session->read('Auth.User.id') . ' and status_flag=1');
		if($data['0']['0']['price'] == 0)
			$days = 0;
		else $days = intval($bal / $data['0']['0']['price']);
		return $days;
	}
	
	function about(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		$this->render('about_stock');
	}
	
	function learnMore(){
		$this->SMSApp->recursive = -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		$this->set('data',$data);
		$this->render('learn_more');
	}
	
	function faqs(){
		$this->render('faqs');
	}
	
	function getRightSide(){
		$balance = $this->General->getBalance($_SESSION['Auth']['User']['id']);
		$days = $this->calculateDays($balance);
		$this->set(compact('balance','days'));
		$this->render('right_col');
	}
	
	function getActiveStocks($last=null) {
		$this->Stock->recursive = -1;
		$count = 5;
		$first = 1;
		if($last == null){
			$last = $_REQUEST['last'];
			$first = 0;
		}
		$limit = $last . "," . $count;
		
		$data = $this->Stock->find('all',array('fields' => array('Stock.*','stock_companies.company'), 
								'conditions' => array('Stock.user_id' => $this->Session->read('Auth.User.id'), 'status_flag' => '1'),
								'joins' => array(
											array(
											 'table' => 'stock_companies',
											 'type' => 'left',
											 'conditions' => array('Stock.company_id = stock_companies.id')
											)
								),
								'order' => 'Stock.id desc',
								'limit' => $limit
								));
		$this->set('total',($last + count($data)));
		$this->set('first',$first);
		$this->set('count',$count);						
		$this->set('activeStocks', $data);
		$this->render('active_scripts');						
		//return $data;						
	}
	
	function getInActiveStocks($last=null) {
		$this->Stock->recursive = -1;
		$count = 5;
		$first = 1;
		if($last == null){
			$last = $_REQUEST['last'];
			$first = 0;
		}
		$limit = $last . "," . $count;
		
		$data = $this->Stock->find('all',array('fields' => array('Stock.*','stock_companies.company'), 
								'conditions' => array('Stock.user_id' => $this->Session->read('Auth.User.id'), 'status_flag' => '0', 'del_flag' => '0'),
								'joins' => array(
											array(
											 'table' => 'stock_companies',
											 'type' => 'left',
											 'conditions' => array('Stock.company_id = stock_companies.id')
											)
								),
								'order' => 'Stock.end desc',
								'limit' => $limit
								));
		$this->set('total',($last + count($data)));
		$this->set('first',$first);
		$this->set('count',$count);				
		$this->set('inActiveStocks', $data);
		$this->render('inactive_scripts');							
								
		//return $data;	
	}
	
	function cronSendingStockPrice(){ //4 PM every day once on weekdays
		$this->lock();
		if(date('D') != 'Sun' && date ('D') != 'Sat') {
			$this->SMSApp->recursive = -1;
			$this->Stock->recursive = -1;
			$id = $this->SMSApp->find('first',array('fields' => array('id'),'conditions' => array('controller_name' => $this->controller_name)));
			$app_id = $id['SMSApp']['id'];
			
			$codes = $this->Stock->find('all',array('fields' => array('distinct Stock.stock_code, Stock.nse_flag'), 
									'conditions' => array('Stock.status_flag' => '1')));
			
			//$this->printArray($codes); exit;
			$codesData = array();
			foreach($codes as $code){
				$codesData[$code['Stock']['stock_code']] = $this->getStockData($code['Stock']['stock_code'],$code['Stock']['nse_flag']);	
			}
			
			$users = $this->Stock->find('all',array('fields' => array('distinct Stock.user_id','users.mobile','users.balance'), 
									'conditions' => array('Stock.status_flag' => '1'),
									'joins' => array(
												array(
												 'table' => 'users',
												 'type' => 'left',
												 'conditions' => array('Stock.user_id = users.id')
												))));
			foreach($users as $user){
				$userId = $user['Stock']['user_id'];
				$companies = $this->Stock->find('all',array('conditions' => array('Stock.user_id' => $userId, 'status_flag' => '1')));	
				$pars = array();
				$stock_ids = '';
				foreach($companies as $company){
					$stock_ids .= $company['Stock']['id'] . ",";
					$par = array();
					$par['stock_code'] = $company['Stock']['stock_code'];
					$par['company_id'] = $company['Stock']['company_id'];
					$par['nse_flag'] = $company['Stock']['nse_flag'];
					$par['news_flag'] = $company['Stock']['news_flag'];
					$par['id'] = $company['Stock']['id'];
					$pars[] = $par;
				}
				$stock_ids = substr($stock_ids, 0, -1);
				$this->convertTo320AndSend($pars,$stock_ids,$user['users']['mobile'],1,0,1,$userId,$codesData,null);
			}
		}
		$this->releaseLock();
		$this->autoRender = false;
	}
	
	function cronSendingStockNews(){ //Every 2 hrs between 8 AM to 10 PM
		$this->lock();
		$this->SMSApp->recursive = -1;
		$this->Stock->recursive = -1;
		$id = $this->SMSApp->find('first',array('fields' => array('id'),'conditions' => array('controller_name' => $this->controller_name)));
		$app_id = $id['SMSApp']['id'];
		
		$codes = $this->Stock->find('all',array('fields' => array('distinct Stock.company_id'), 
								'conditions' => array('Stock.status_flag' => '1','Stock.news_flag' => '1')));
		
		$newsData = array();
		foreach($codes as $code){
			$news = $this->getFeedNews($code['Stock']['company_id']);
			if(!empty($news)){
				$newsData[$code['Stock']['company_id']] = $news;
			}
		}
		
		$users = $this->Stock->find('all',array('fields' => array('distinct Stock.user_id','users.mobile','users.balance'), 
								'conditions' => array('Stock.status_flag' => '1'),
								'joins' => array(
											array(
											 'table' => 'users',
											 'type' => 'left',
											 'conditions' => array('Stock.user_id = users.id')
											))));
		foreach($users as $user){
			
			$userId = $user['Stock']['user_id'];
			$companies = $this->Stock->find('all',array('conditions' => array('Stock.user_id' => $userId, 'status_flag' => '1')));	
			$pars = array();
			$comps = array();
			if(!empty($newsData)){
				$stock_ids = '';
				foreach($companies as $company){
					$stock_ids .= $company['Stock']['id'] . ",";
					if(!in_array($company['Stock']['company_id'], $comps)){
						$par = array();
						$par['stock_code'] = $company['Stock']['stock_code'];
						$par['company_id'] = $company['Stock']['company_id'];
						$par['nse_flag'] = $company['Stock']['nse_flag'];
						$par['news_flag'] = $company['Stock']['news_flag'];
						$par['id'] = $company['Stock']['id'];
						$pars[] = $par;
						$comps[] = $company['Stock']['company_id'];
					}
				}
				$stock_ids = substr($stock_ids, 0, -1);
				$this->convertTo320AndSend($pars,$stock_ids,$user['users']['mobile'],0,1,1,$userId,null,$newsData);
			}
		}
		$this->releaseLock();
		$this->autoRender = false;	
	}
	
	function cronToTakeOutMoney(){ //Every morning 8AM
		$this->lock();
		$this->SMSApp->recursive = -1;
		$this->Stock->recursive = -1;
		$id = $this->SMSApp->find('first',array('fields' => array('id'),'conditions' => array('controller_name' => $this->controller_name)));
		$app_id = $id['SMSApp']['id'];
		$users = $this->Stock->find('all',array('fields' => array('distinct Stock.user_id','users.mobile','users.balance'), 
								'conditions' => array('Stock.status_flag' => '1'),
								'joins' => array(
											array(
											 'table' => 'users',
											 'type' => 'left',
											 'conditions' => array('Stock.user_id = users.id')
											))));
		foreach ($users as $user){
			$balance = $user['users']['balance'];
			$userId = $user['Stock']['user_id'];
			$data = $this->Stock->find('all',array('fields' => array('Stock.*','stock_companies.company'), 
								'conditions' => array('Stock.status_flag' => '1','Stock.user_id' => $userId),
								'joins' => array(
											array(
											 'table' => 'stock_companies',
											 'type' => 'left',
											 'conditions' => array('Stock.company_id = stock_companies.id')
											)
								)		
					));
			$alertDisCon = array();
			foreach($data as $stock){
				if(($stock['Stock']['news_flag'] == 1) || ($stock['Stock']['news_flag'] == 0 && date('D') != 'Sun' && date ('D') != 'Sat')){
					$price = $stock['Stock']['price'];
					if($balance >= $price){
						$this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$price,$app_id,$stock['Stock']['id'],$userId);
						$this->General->balanceUpdate($price,'subtract',$userId);
						$balance = $balance - $price;
					}
					else {
						$pre = 'BSE: ';
						if($stock['Stock']['nse_flag'] == '1'){
							$pre = 'NSE: ';
						}
						$this->Stock->query("UPDATE stocks SET temp_flag = 1,status_flag=0,end='".date('Y-m-d H:i:s')."' where id=".$stock['Stock']['id']);
						$alertDisCon[] = $pre . $stock['stock_companies']['company'];
					}
				}	
			}
			
			if(!empty($alertDisCon)){
				$msg = $this->General->createMessage("STOCKS_DISCONTINUED",array(implode(",",$alertDisCon),$balance));
				$this->General->sendMessage(SMS_SENDER,$user['users']['mobile'],$msg,'template');
			}
		}
		$this->releaseLock();
		$this->autoRender = false;
	}
	
	function cronContinueStocks(){ //Every 15 mins
		$this->lock();
		$this->Stock->recursive = -1;
		$this->SMSApp->recursive = -1;
		$id = $this->SMSApp->find('first',array('fields' => array('id'),'conditions' => array('controller_name' => $this->controller_name)));
		$app_id = $id['SMSApp']['id'];
		
		$users = $this->Stock->query("SELECT user_id,users.mobile,users.balance from payment,users where user_id = users.id and flag = 'SUCCESS' and ADDTIME(end_time, '00:30:00') > '" . date('Y-m-d H:i:s') ."'" );
		foreach($users as $user){
			$balance = $user['users']['balance'];
			$userId = $user['payment']['user_id'];
			$data = $this->Stock->find('all',array('fields' => array('Stock.*','stock_companies.company'), 
								'conditions' => array('Stock.status_flag' => '0','Stock.temp_flag' => '1','Stock.user_id' => $userId),
								'joins' => array(
											array(
											 'table' => 'stock_companies',
											 'type' => 'left',
											 'conditions' => array('Stock.company_id = stock_companies.id')
											)
								)		
					));
			$alertCon = array();
			$pars = array();
			$stock_ids = array();
			
			foreach($data as $stock){
				$price = $stock['Stock']['price'];
				if($balance >= $price){
					$this->General->appTransactionUpdate(TRANS_ADMIN_DEBIT,$price,$app_id,$stock['Stock']['id'],$userId);
					$this->General->balanceUpdate($price,'subtract',$userId);
					$balance = $balance - $price;
					$pre = 'BSE: ';
					if($stock['Stock']['nse_flag'] == '1'){
						$pre = 'NSE: ';
					}
					$alertCon[] = $pre . $stock['stock_companies']['company'];
					$this->Stock->query('UPDATE stocks SET temp_flag = 0,status_flag=1,end=NULL where id='.$stock['Stock']['id']);
					
					$stock_ids[] = $stock['Stock']['id'];
					$par = array();
					$par['stock_code'] = $stock['Stock']['stock_code'];
					$par['company_id'] = $stock['Stock']['company_id'];
					$par['nse_flag'] = $stock['Stock']['nse_flag'];
					$par['news_flag'] = $stock['Stock']['news_flag'];
					$par['id'] = $stock['Stock']['id'];
					$pars[] = $par;
				}
			}

			if(!empty($alertCon)){
				$msg = $this->General->createMessage("STOCKS_CONTINUED",array(implode(",",$alertCon),$balance));
				$this->General->sendMessage(SMS_SENDER,$user['users']['mobile'],$msg,'template');
				
				$this->convertTo320AndSend($pars,implode(",",$stock_ids),$user['users']['mobile'],1,1,0,$userId);
			}
		}
		$this->releaseLock();
		$this->autoRender = false;					
	}
	
	function curl_getStockCompanies($url,$nse,$bse){
		$userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		// make the cURL request to $target_url
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$page= curl_exec($ch);
		//echo "html : ".$page; 
		if($page)
		{
			// create a document out of the well formed content
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);
			
			$content1 = $this->getCompanies($domDocument,$nse,$bse);
			//echo $content1;
			curl_close ($ch);
			return $content1;		
		}
		else
		{
			curl_close ($ch);
			return '999';
		}
		
	}
	
	function getCompanies($domDocument,$nse,$bse){
		$xpath = "//table[contains(concat(' ', normalize-space(@class), ' '), 'dataTable')]/tbody/tr";
		$domXPath = new DOMXPath($domDocument);
		$domNodeList = $domXPath->query($xpath);
		$data = array();
		$i = 0;
		foreach($domNodeList as $node){
			$domDocument1 = new DOMDocument();
			foreach($node->childNodes as $childNode){
				$domDocument1->appendChild($domDocument1->importNode($childNode, true));
			}
			$domXPath1 = new DOMXPath($domDocument1);
			$comp = $this->General->getHTMLFromNode($domXPath1->query("/td[1]/a")->item(0));
			$code = $this->General->getHTMLFromNode($domXPath1->query("/td[2]")->item(0));
			$this->saveCompany($comp,$code,$nse,$bse);
			$i++;
		}
		return $i;
	}
	
	function saveCompany($comp,$code,$nse,$bse){
		$comp = addslashes($comp);
		$code = addslashes($code);
		$check = $this->Stock->query("select count(*) as counts from stock_companies where company='".$comp."'");
		if($check['0']['0']['counts'] == '0'){
			$nse_code = null;
			$bse_code = null;
			if($nse == '1')
				$nse_code = $code;
			if($bse == '1')
				$bse_code = $code;
			$this->Stock->query("INSERT INTO stock_companies (company,nse_flag,bse_flag,nse_code,bse_code) VALUES ('$comp',$nse,$bse,'$nse_code','$bse_code')");
		}
		else {
			if($nse == '1'){
				$this->Stock->query("UPDATE stock_companies set nse_flag=$nse,nse_code='$code' where company='".$comp."'");
			}
			else if($bse == '1'){
				$this->Stock->query("UPDATE stock_companies set bse_flag=$bse,bse_code='$code' where company='".$comp."'");
			}
		}
	}
	
	function getAllCompanies(){
		set_time_limit(0);
		ini_set("memory_limit","-1");
		//BSE
		$i = 0;
		while(true) {
			$url = "http://money.rediff.com/companies/All/".(1000*$i + 1) ."-". 1000*($i+1);
			$data = $this->curl_getStockCompanies($url,0,1);
			$i++;
			if($data == 0) break;
		}
		//NSE
		$i = 0;
		while(true) {
			$url = "http://money.rediff.com/companies/nseall/All/".(1000*$i + 1) ."-". 1000*($i+1);
			$data = $this->curl_getStockCompanies($url,1,0);
			$i++;
			if($data == 0) break;
		}
		$this->autoRender = false;
	}
	
	function getStockData($code,$nse){
		if($nse == 0){ //BSE Data
			$code = "BOM:" . $code;
		}
		else { //NSE Data
			$code = "NSE:" . $code;
		}
		
		$url = 'http://www.google.com/finance/info?client=ig&q='.urlencode($code);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$data = curl_exec($ch);
		curl_close($ch);
		
		$data = str_replace('\x26','', $data);
		//to change data
		$data = strstr($data, '{');
		//$data = strstr(trim($data, ']', true);
		$data = substr(trim($data),0,-1);
		$stock_data = (array)json_decode($data);
		return $stock_data;
	}
	
	function getFeedNews($company_id){
		$codes = $this->Stock->query("select nse_code,bse_code,nse_flag,bse_flag from stock_companies where id='".$company_id."'");
		$rssData1 = array();
		$rssData2 = array();
		$rssData = array();
		
		if($codes['0']['stock_companies']['nse_flag'] == 1){
			$nse_code = $codes['0']['stock_companies']['nse_code'];
			$feed_code = "NSE:" . $nse_code;
			$url = "http://www.google.com/finance/company_news?q=".urlencode($feed_code)."&output=rss";
			$rssData1 = $this->General->RSS_Display($url,10);
		}
		if($codes['0']['stock_companies']['bse_flag'] == 1){
			$bse_code = $codes['0']['stock_companies']['bse_code'];
			$feed_code = "BOM:" . $bse_code;
			$url = "http://www.google.com/finance/company_news?q=".urlencode($feed_code)."&output=rss";
			$rssData2 = $this->General->RSS_Display($url,10);
		}
		
		if(empty($rssData1)){
			$rssData = $rssData2; 
		}
		else if(empty($rssData2)){
			$rssData = $rssData1; 
		}
		else{
			$rssData = array_merge($rssData1, $rssData2);
		}
		
		$newFeeds = $this->addFeeds($company_id,$rssData);
		return $newFeeds;
	}
	
	function addFeeds($company_id,$rssData){
		$newFeeds = array();
		$links = array();
		foreach($rssData as $rss){
			if(in_array($rss['link'],$links)) continue;
			$links[] = $rss['link'];
			$find = $this->Stock->query("SELECT * FROM stock_feeds WHERE link='".$rss['link']."' and company_id = '".$company_id."'");
			if(empty($find)){
				$this->Stock->query("INSERT INTO stock_feeds (company_id,title,link,description,created) VALUES ($company_id,'".addslashes($rss['title'])."','".$rss['link']."','".addslashes($rss['description'])."','".$rss['date']."')");
				if($rss['date'] >= date('Y-m-d', strtotime(' - 1 days'))){
					if(strlen($rss['title']) >= MIN_TITLE_LENGTH && str_word_count($rss['title']) >= MIN_WORD_COUNT){
						$news = array();
						$news['stock_feeds']['title'] = $rss['title'];
						$id = $this->Stock->query("SELECT id FROM stock_feeds WHERE link='".$rss['link']."' and company_id = '".$company_id."'");
						$news['stock_feeds']['id'] = $id['0']['stock_feeds']['id'];
						$newFeeds[] = $news;
					}
				}
			}
		}
		return $newFeeds;
	}
	
	function convertTo320AndSend($stocks,$stock_ids,$mobile,$in_price,$in_news,$cron_flag,$user_id,$stock_datas=null,$news_datas=null){
		$default_msg = "*Stock Tracker*";
		$full_msg = $default_msg;
		$len = 0;
		$post_flag = false;
		$newsIds = array();	
		foreach($stocks as $stock){
			
			$query = "SELECT company from stock_companies where id = ".$stock['company_id'];
			$company = $this->Stock->query($query);
			$company = "\n" . $company['0']['stock_companies']['company'] ."\n";
				
			if($in_price){
				//if(date('D') == 'Sun' || date ('D') == 'Sat'){
				if(true){
					$code = $stock['stock_code'];
					if(!$cron_flag)
						$stock_data = $this->getStockData($stock['stock_code'],$stock['nse_flag']);
					else 
						$stock_data = $stock_datas[$code];
					$pre = "BSE: ";
					if($stock['nse_flag'])
						$pre = "NSE: ";
					$msg = $company . $pre . $stock_data['l'] . " " . $stock_data['c'] . " (" . $stock_data['cp'] . "%)\n";
					if(strlen($msg . $full_msg) > MAX_STOCK_MSG_LENGTH){
						$this->sendStockSMS($stock_ids, $full_msg, $mobile, $user_id);
						$full_msg = $default_msg . $msg;
					}
					else {
						$full_msg .= $msg;
					}
					$post_flag = true;
				}
				else {
					$in_price = 0;
				}
			}
			
			if($in_news && $stock['news_flag']){
				$company_id = $stock['company_id'];
				$newsData = array();
				if(!$cron_flag)
					$newsData = $this->Stock->query("SELECT * FROM stock_feeds WHERE company_id = '".$company_id."' and created > '" . date('Y-m-d', strtotime(' - 1 days')) . "'");
				else if(!empty($news_datas[$company_id]))
					$newsData = $news_datas[$company_id];
				$count = 0;
				$msg = '';
				
				foreach($newsData as $news){
					if(strlen($news['stock_feeds']['title']) >= MIN_TITLE_LENGTH && str_word_count($news['stock_feeds']['title']) >= MIN_WORD_COUNT){
						$pre = '';
						if($count ==0 && !$in_price)
							$pre = $company;
						$msg = $pre . "#" . stripslashes($news['stock_feeds']['title']) ."\n";
						if(strlen($msg . $full_msg) > MAX_STOCK_MSG_LENGTH){
							$this->sendStockSMS($stock_ids, $full_msg, $mobile, $user_id, $newsIds);
							if($count > 0 || ($in_price && $count == 0)){
								$full_msg = $default_msg . $company . $msg;
							}
							else $full_msg = $default_msg . $msg;
							$newsIds = array();
						}
						else {
							$full_msg .= $msg;
						}
						$newsIds[] = $news['stock_feeds']['id'];
						$post_flag = true;
						$count++;
					}
				}
			}
		}
		if($post_flag)
			$this->sendStockSMS($stock_ids, $full_msg, $mobile, $user_id,$newsIds);
	}
	
	function setNewsPage($log_id,$user_id,$news_ids){
		$this->Stock->query("INSERT INTO stock_news (log_id,user_id,feed_ids,created) VALUES ($log_id,$user_id,'".implode(",",$news_ids)."','".date("Y-m-d H:i:s")."')");
	}
	
	function stockNews($log_id){
		$log_id = $this->objMd5->decrypt($log_id,encKey);
		$data = $this->Stock->query("SELECT id,feed_ids,created,user_id from stock_news where log_id=$log_id");
		$ids = $data['0']['stock_news']['feed_ids'];
		
		$feeds = $this->Stock->query("SELECT link,title,description from stock_feeds where id in ($ids)");
		
		$prevUrl = $this->Stock->query("SELECT log_id from stock_news where id < ".$data['0']['stock_news']['id'] . " and user_id = " . $data['0']['stock_news']['user_id'] . " order by id desc limit 1");
		$nextUrl = $this->Stock->query("SELECT log_id from stock_news where id > ".$data['0']['stock_news']['id'] . " and user_id = " . $data['0']['stock_news']['user_id'] . " order by id asc limit 1");
		
		if(!empty($prevUrl))
		$this->set('prevUrl',urlencode($this->objMd5->Encrypt($prevUrl['0']['stock_news']['log_id'],encKey)));
		if(!empty($nextUrl))
		$this->set('nextUrl',urlencode($this->objMd5->Encrypt($nextUrl['0']['stock_news']['log_id'],encKey)));
		$this->set('feeds',$feeds);
		$this->set('time',$data['0']['stock_news']['created']);
		$this->render('news_page','mdata');
	}
	
	function sendStockSMS($id,$msg,$mobile,$user_id,$newsIds=null){
		//log update
		$msg = strip_tags(html_entity_decode($msg,ENT_QUOTES));
		$this->SMSApp->recursive= -1;
		$data = $this->SMSApp->find('first',array('conditions' => array('controller_name' => $this->controller_name)));
		
		$log_id = $this->General->appLogUpdate($data['SMSApp']['id'],$id,null,$msg,$mobile,$user_id);
		if(!empty($newsIds)){
			$this->setNewsPage($log_id,$user_id,$newsIds);
			$log_id = urlencode($this->objMd5->Encrypt($log_id,encKey));
			$url = $this->General->getBitlyUrl(MAIN_SERVER.'stocks/stockNews/'.$log_id);
			$msg .= "\nRead more: ".$url;
		}
		$receiver[$log_id] = $mobile;
		//echo nl2br($msg) . "<br>";
		$this->General->sendMessage(SMS_SENDER,$receiver,$msg,'app',$this->controller_name);
	}
	
}
?>