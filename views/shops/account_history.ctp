<div>
	<?php echo $this->element('shop_upper_tabs',array('tob_tab' => 'reports'));?>
    <div id="pageContent" style="min-height:500px;position:relative;">
    	<div class="loginCont">
    		<?php echo $this->element('shop_side_reports',array('side_tab' => 'transfer'));?>
    		<div id="innerDiv">
	  			<fieldset style="padding:0px;border:0px;margin:0px;">
	  			<div>
    			<span style="font-weight:bold;margin-right:10px;">Select Date Range: </span>From<input type="text" style="margin-left:10px; width: 100px; cursor: pointer;" maxlength="10" onmouseover="fnInitCalendar(this, 'fromDate','restrict=true,open=true')" id="fromDate" name="fromDate" value="<?php if(isset($date_from)) echo date('d-m-Y', strtotime($date_from));?>"> - To<input type="text" style="margin-left:10px; width: 100px; cursor: pointer;" maxlength="10" onmouseover="fnInitCalendar(this, 'toDate','restrict=true,open=true')" id="toDate" name="toDate" value="<?php if(isset($date_to)) echo date('d-m-Y', strtotime($date_to));?>">
    			
    			<span style="margin-left:30px;" id="submit"><input type="button" value="Search" class="retailBut enabledBut" style="padding: 0 5px 3px" id="sub" onclick="findHistory();"></span>
    			</div>
    			<div style="margin-top:10px;"><span id="date_err" class="error" style="display:none;">Error: Please select dates</span></div>
				<div class="appTitle" style="margin-top:20px;">Transaction History <?php if(isset($date_from) && isset($date_to)) echo "(". date('d-m-Y', strtotime($date_from)) . " - " .  date('d-m-Y', strtotime($date_to)) . ")"; ?></div>
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="ListTable" summary="Transactions">
        			<thead>
			          <tr class="noAltRow altRow">
			            <th style="width:80px;">Date</th>
			            <th style="width:150px;">Particulars</th>
			            <th class="number" style="width:70px">Debit (<span><img class='rupeeBkt' src='/img/rs.gif'/></span>)</th>
			            <th class="number" style="width:70px">Credit (<span><img class='rupeeBkt' src='/img/rs.gif'/></span>)</th>
			          </tr>
			        </thead>
                    <tbody>
                    <?php if(!isset($empty) && empty($transactions)) { ?>
                    <tr>
                    	<td colspan="4"><span class="success">No Results Found !!</span></td>
                    </tr>
                    
                    <?php } ?>
                    <?php $i=0; foreach($transactions as $transaction){ 
                    	if($i%2 == 0)$class = '';
                    	else $class = 'altRow';
                    ?>
                      <tr class="<?php echo $class; ?>"> 
			            <td><?php echo date('d-m-Y H:i:s', strtotime($transaction['transactions']['timestamp'])); ?></td>
			            <?php if($transaction['transactions']['id'] == 'TOCREDITDEBIT'){
	    			    if($transaction['transactions']['type'] == 0) $note = "Credit Note";
	    			    else if($transaction['transactions']['type'] == 1) $note = "Debit Note";
	    			    if(empty($transaction['transactions']['refid']))$company = "MindsArray";
	    			    else $company = $transaction['transactions']['refid'];
	    			    ?>
    			    	<td> <b><?php echo $note; ?></b>: FROM <i><?php echo $company;?></i></td>
			        	<td class="number"><?php if($transaction['transactions']['type'] == 1) echo $transaction['transactions']['amount']; else "-";?></td>
			        	<td class="number"><?php if($transaction['transactions']['type'] == 0) echo $transaction['transactions']['amount']; else "-";?></td>
    			    	
    			    	<?php } else if($transaction['transactions']['id'] == 'FROMCREDITDEBIT'){
	    			    if($transaction['transactions']['type'] == 0) $note = "Credit Note";
	    			    else if($transaction['transactions']['type'] == 1) $note = "Debit Note";
	    			    if(empty($transaction['transactions']['refid']))$company = "MindsArray";
	    			    else $company = $transaction['transactions']['refid'];
	    			    ?>
    			    	<td> <b><?php echo $note; ?></b>: TO <i><?php echo $company;?></i></td>
			        	<td class="number"><?php if($transaction['transactions']['type'] == 0) echo $transaction['transactions']['amount']; else "-";?></td>
			        	<td class="number"><?php if($transaction['transactions']['type'] == 1) echo $transaction['transactions']['amount']; else "-";?></td>
    			    
			            <?php } else if($transaction['transactions']['type'] == ADMIN_TRANSFER) { ?>
			            <td> SMSTadka Credits</td>
			            <td class="number">-</td>
			            <td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
			            <?php } else if($transaction['transactions']['type'] == SDIST_DIST_BALANCE_TRANSFER) { 
			            	if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR) {
			            ?>
			            <td> Transferred to <?php echo $transaction['transactions']['name']; ?> (<?php echo $transaction['transactions']['refid']; ?>)</td>
			            <td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
			            <td class="number">-</td>
			            <?php } else if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR) {?>
			            <td> Transferred by <?php echo $transaction['transactions']['name']; ?></td>
			            <td class="number">-</td>
			            <td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
			            
			            <?php } }  else if($transaction['transactions']['type'] == DIST_RETL_BALANCE_TRANSFER) { 
			            if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR) {
			            ?>
			            <td> Transferred to <?php echo $transaction['transactions']['name']; ?> (<?php echo $transaction['transactions']['refid']; ?>)</td>
			            <td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
			            <td class="number">-</td>
			            <?php } else if($_SESSION['Auth']['User']['group_id'] == RETAILER) {?>
			            <td> Transferred by Distributor (<?php echo $transaction['transactions']['name']; ?>)</td>
			            <td class="number">-</td>
			            <td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
    			      </tr>
    			    <?php }}
    			    else if($transaction['transactions']['type'] == COMMISSION_DISTRIBUTOR && $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){?>
			    		<td> Commission Transferred to <?php echo $transaction['transactions']['name'];?> </td>
		        		<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
		        		<td class="number">-</td>
    			    <?php }
    			    else if($transaction['transactions']['type'] == COMMISSION_RETAILER && $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){?>
			    		<td> Commission Transferred to <?php echo $transaction['transactions']['name'];?> </td>
		        		<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
		        		<td class="number">-</td>
    			    <?php }
    			    else if($transaction['transactions']['type'] == COMMISSION_SUPERDISTRIBUTOR || $transaction['transactions']['type'] == COMMISSION_DISTRIBUTOR || $transaction['transactions']['type'] == COMMISSION_RETAILER){ 
    			    ?>
    			    	<td> Commission (<?php if($transaction['transactions']['name'] == RETAILER_ACTIVATION) echo "Online Activation"; else echo "Card Activation";?>) </td>
			        	<td class="number">-</td>
			        	<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
    			    <?php }
    			    else if($transaction['transactions']['type'] == TDS_DISTRIBUTOR && $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){?>
			    		<td> TDS taken from <?php echo $transaction['transactions']['name'];?> </td>
		        		<td class="number">-</td>
		        		<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
    			    <?php }
    			    else if($transaction['transactions']['type'] == TDS_RETAILER && $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){?>
			    		<td> TDS taken from <?php echo $transaction['transactions']['name'];?> </td>
		        		<td class="number">-</td>
		        		<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
    			    <?php }
    			    else if($transaction['transactions']['type'] == TDS_SUPERDISTRIBUTOR || $transaction['transactions']['type'] == TDS_DISTRIBUTOR || $transaction['transactions']['type'] == TDS_RETAILER){ 
    			    ?>
    			    	<td> TDS (<?php if($transaction['transactions']['name'] == RETAILER_ACTIVATION) echo "Online Activation"; else echo "Card Activation";?>) </td>
			        	<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
			        	<td class="number">-</td>
    			    <?php } 
    			    else if($transaction['transactions']['type'] == DISTRIBUTOR_ACTIVATION){ 
    			    ?>
    			    	<td> Cards Activated for <?php echo $transaction['transactions']['name']; ?></td>
			        	<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
			        	<td class="number">-</td>
    			    <?php }
    			    else if($transaction['transactions']['type'] == RETAILER_ACTIVATION){ 
    			    ?>
    			    	<td> Card Sold (<?php echo $transaction['transactions']['name']; ?>) </td>
			        	<td class="number"><?php echo $transaction['transactions']['amount']; ?></td>
			        	<td class="number">-</td>
    			    <?php }
    			    $i++; } ?> 					    			      
			         </tbody>	         
			   	</table>
			</fieldset>
   			</div>
   			<br class="clearLeft" />
 		</div>
    	
    </div>
 </div>
<br class="clearRight" />
</div>
<script>
function findHistory(){
	var html = $('submit').innerHTML;
	showLoader3('submit');
	var date_from = $('fromDate').value;
	var date_to = $('toDate').value;
	if(date_from == '' || date_to == ''){
		$('date_err').show();
		$('submit').innerHTML = html;
	}
	else {
		$('date_err').hide();
		date_from = date_from.replace(/-/g,"");
		date_to = date_to.replace(/-/g,"");
		window.location = "http://" + siteName + "/shops/accountHistory/"+date_from+"-"+date_to;
	}
}
</script>