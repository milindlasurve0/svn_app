<div class="loginCont">
  <div>
    <div class="leftFloat dashboardPack">
      <div class="catList">
        <ul id='innerul'>
            <?php 
           		$class1 = 'sel';
           		$class2 = '';
           		if($about == 1){
           			$class1 = '';
           			$class2 = 'sel';
           		}
           ?>
           <li name='innerli'><?php echo $ajax->link( 
    				'Bhulakkad Home ', 
    				array('action' => 'setReminder'),
    				array('id' => 'createAlert', 'class' => "$class1" ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("createAlert").removeClassName("loader")' )
					); ?> </li>
		  <li name='innerli'><?php echo $ajax->link( 
    				'Contacts ', 
    				array('action' => 'frndList'),
    				array('id' => 'appRemfrndList', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("appRemfrndList").removeClassName("loader")' )
					); ?> </li>			
		  <li name='innerli'><?php echo $ajax->link( 
    				'Groups ', 
    				array('action' => 'grpList'),
    				array('id' => 'grpList', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("appRemfrndList").removeClassName("loader")' )
					); ?> </li>		
		  <li name='innerli'><?php echo $ajax->link( 
    				'About Bhulakkad ', 
    				array('action' => 'about'),
    				array('id' => 'aboutReminder', 'class' => "$class2" ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("aboutReminder").removeClassName("loader")' )
					); ?> </li>
		  <li name='innerli'><?php echo $ajax->link( 
    				"FAQs ", 
    				array('action' => 'faqs'),
    				array('id' => 'appRemfaqs', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("appRemfaqs").removeClassName("loader")' )
					); ?> </li>
		  <li class="hList" name='innerli'>
           		<?php echo $ajax->link( 
    				'Personal Alerts Home ', 
    				array('controller' => 'apps','action' => 'getApps'),
    				array('id' => 'allApps', 'class' => '' ,'update' => 'pageContent','onclick' => 'changeInnerTabClass(this);','complete' => '$("allApps").removeClassName("loader")' )
				); ?>
		  </li>						
        </ul>
      </div>
      
      <div  style="display:none">
      	<div class="color4 fntSz19" style="position:relative;">
      		<span class="testimonial" style=" top:35px;">"</span>PNR Alert helped me track my PNR and inturn I saved my precious time doing the thesis i am required to. Thanks SMS Tadka for this service.
      		<span class="testimonial" style="bottom:-17px;position:absolute">"</span>
      	</div>
      	<div style="padding-top:10px;text-align:right;">
      		<div class="color3 fntSz17 strng rightFloat">
      			<img src="/img/spacer.gif" width="60px" height="60px" style="float:left; margin-right:5px;">
      			<div style="overflow:hidden; float:left;"><div>-&nbsp;Astha&nbsp;Gulatee</div><div>College&nbsp;Student</div></div>
      			<div class="clearLeft"></div>
      		</div>
      	</div>
      </div>
      
    </div>
    
    <div style="margin-left: 223px;" id="innerDiv">
      	<?php
      		if($about == 1)	{
      			echo "<script>$('aboutReminder').simulate('click');</script>";
      		}
      		else {
      			echo "<script>$('createAlert').simulate('click');</script>";
      		}
      	?>
    </div>
    <br class="clearLeft" />
 </div>