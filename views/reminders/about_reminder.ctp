<?php if(isset($_SESSION['Auth']['User'])){ ?>
<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<?php echo $ajax->link( 
    				'<< Back to Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?>
		</span>
	</div>
	<span>
		<?php echo $ajax->link( 
    				'Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?> 
	</span> &nbsp;&gt;&gt;&nbsp; <span>About Bhulakkad</span>
</div>
<?php } ?>
<div class="appColRight1" style="font-size:0.9em">
	<!-- No -->
	<div class="rightColSpace">
      	<div style="position: relative;" id="recentMsgs">
      	  <div style="" class="antina"></div>                  
          <div class="SampJokesTitle">SAMPLE MESSAGES(SMS)</div>
          <div style="height: 415px;" id="">
            <div class="container">
            <input type="hidden" id="msgNum" value="1"/>
            
              <ul style="overflow: hidden; height: 345px;" class="recentMsgsCont">
                <li style="display: block;" id="sampSMS1" class="sampleSMS">
                  <p class="strng" style="border-bottom:2px solid #c3c3c3; padding-bottom:5px;">From: TD-SMSTADKA</p>
                  <p style="padding-right:3px">From: &lt;Your Mobile Number&gt;</p>
                  <p class="strng" style="padding-top:20px;">Your Message [<?php echo APP_REM_MSG_LMT; ?> char long]:</p>
                  <p>Wish you a many many happy returns of the day. May God bless you with health, wealth and prosperity in your life 

<br><br>HAPPY BIRTHDAY TO YOU.</p>
                </li>
                <li style="display: none;" id="sampSMS2" class="sampleSMS">
                  <p class="strng" style="border-bottom:2px solid #c3c3c3; padding-bottom:5px;">From: TD-SMSTADKA</p>
                  <p class="strng" style="padding-top:20px;">Your Message [<?php echo APP_REM_MSG_LMT; ?> char long]:</p>
                  <p>Please attend the wedding of my daughter Deepika at Sai Function Hall, Tagore Nagar on 10th August at 8 PM. - Kumar & Family.</p>
                </li>
                <li style="display: none;" id="sampSMS3" class="sampleSMS">
                  <p class="strng" style="border-bottom:2px solid #c3c3c3; padding-bottom:5px;">From: TD-SMSTADKA</p>
                  <p class="strng" style="padding-top:20px;">Your Message [<?php echo APP_REM_MSG_LMT; ?> char long]:</p>
                  <p>Remember, your medical insurance, car insurance is due on 5th Sep, 2010</p>
                </li>
                <li style="display: none;" id="sampSMS4" class="sampleSMS">
                  <p class="strng" style="border-bottom:2px solid #c3c3c3; padding-bottom:5px;">From: TD-SMSTADKA</p>
                  <p style="padding-right:3px">From: &lt;Your Mobile Number&gt;</p>
                  <p class="strng" style="padding-top:20px;">Your Message [<?php echo APP_REM_MSG_LMT; ?> char long]:</p>
                  <p>Dear Kartik,<br> you have been assigned a task to look after all the management work in my absence.</p>
                </li>
                <li style="display: none;" id="sampSMS5" class="sampleSMS">
                  <p class="strng" style="border-bottom:2px solid #c3c3c3; padding-bottom:5px;">From: TD-SMSTADKA</p>
                  <p style="padding-right:3px">From: &lt;Your Mobile Number&gt;</p>
                  <p class="strng" style="padding-top:20px;">Your Message [<?php echo APP_REM_MSG_LMT; ?> char long]:</p>
                  <p>I hope you have a wonderful christmas
have a great new year !
Hopefully santa will be extra good to you .
enjoy your holidays !</p>
                </li>
              </ul>
              <div style="padding: 10px 0px; margin-left: 40%;">
                <ul class="sampleNo">                  
                  <li><a onclick="showSample('pre')" href="javascript:void(0)">&lt;</a>                  
                  </li><li><a onclick="showSample('next')" href="javascript:void(0)">&gt;</a></li>
                </ul>
                <br class="clearLeft">
              </div>
              <!-- <div align="center"><a target="_blank" href="/groups/allRecentMsgs/34">View All</a></div> -->
              </div>
          </div>
		</div>
    </div>
	<!-- no -->
</div>
<div class="appColLeft1 info" id="appReminderAddDiv">
	<div class="title6"><span class="fntSz19 strng"><?php echo $data['SMSApp']['name']; ?></span> - Send Future SMS to Groups</div>
	
	<div class="priceBox">
		<div class="appSmallBox1 appImg4">
			<div class="fntSz15 strng"><em>Send future SMS to groups (customers, friends, colleagues & social groups).</em></div>
			<div style="margin:10px 5px 10px 0;"><span class="fntSz15 strng">Price:</span> <?php echo $objGeneral->getPrice($data['SMSApp']['basic_price']); ?> <?php echo $data['SMSApp']['price_tag']; ?></div>
			<input type="button" value="Start Using Now!" class="css3But4" onclick="showApp('<?php echo $data['SMSApp']['controller_name'];?>')">
		</div>
	</div>
	<p>Use it for sending <span class="strng">group SMS </span>for your business, personal or social,  <span class="strng">set reminders/future messages</span> for individuals and groups, both. Send up to  <span class="strng"><?php echo APP_REM_MSG_LMT -  APP_REM_MSG_FIXED; ?> Chars</span> long messages.</p>
    <p><span class="strng">*Super long SMS at a super cheap price with Future Delivery Option*</span><br/> Note: Receiver will receive messages from your own registered number.</p>
	<div>
	<span class="strng">Where all you can use it</span>
	<ol class="info">
		<li>Personal use - Be up-to-date
	<br><span class="fntSz12">(Reminder for Doctor's Appointment, Wife's birthday, Relative's anniversary etc.)</span></li>
		<li>Social greetings and reminders - Be Social<br><span class="fntSz12">
			(Greeting Messages for others like birthday, anniversary etc.)</span></li>
		<li>Client/Customer Reminders - Bring business, 
	know clients' next appointment<br><span class="fntSz12">(Doctors, Lawyers, CAs), New offer announcements,  
            Payment reminders etc.</span></li>
        <li>Collaboration and Management - Get things done<br> 
	<span class="fntSz12">(setting up agendas, todos, meeting schedule, notices, announcements &  
            milestones etc.)</span></li>
	</ol>
	</div>
	<div>
	<span class="strng">Why Bhulakkad?</span>
	<ol class="info">
		<li>100% Assured on time delivery of reminders</li> 
		<li>Receipient receives message from your mobile number and not from SMSTadka</li>
		<li>Very easy to use (Send SMSs to groups/ individuals/ yourself)<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg3"></div></li>
		<li>Upto <?php echo APP_REM_MSG_LMT -  APP_REM_MSG_FIXED; ?> character message support<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg4"></div></li>
		<li>Repeat reminders on Daily, Weekly, Monthly or Yearly basis<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg5"></div></li>
		<li>Most Simplified, Powerful and Effective reminder service<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg6"></div></li>
	</ol>
	</div>
	<div class="rowDividerIn">
		Don't be a Bhulakkad, <input type="button" class="otherSprite oSPos27" onclick="window.scroll(0,0);showApp('<?php echo $data['SMSApp']['controller_name'];?>')">
	</div>
	<br>
</div>
<div class="clearRight">&nbsp;</div>