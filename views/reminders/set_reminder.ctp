 <div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<?php echo $ajax->link( 
    				'<< Back to Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?>
		</span>
	</div>
	<span>
		<?php echo $ajax->link( 
    				'Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?> 
	</span> &nbsp;&gt;&gt;&nbsp; <span><?php echo $data['SMSApp']['name']; ?></span>
  </div>
<?php if(DND_FLAG && !TRANS_FLAG) {?> 
 <div class="notification1">Due to the new guidelines of TRAI, we cannot guarantee the delivery of reminders and sms to DND registered numbers "AS OF NOW".  <a target="_blank" href="http://nccptrai.gov.in/nccpregistry/">Read more</a>
</div>
<?php } ?>
   <!-- Bhullakkad App -->
   		<div class="appColRight" style="font-size:0.9em">
   			<div class="appDataGrp" >
	      		<div class="appDataTitle" >Active Future Messages</div>
	      		<div id="upcomingRemDiv">
	      		<div id="upcomingRemTab">	      	
	      		<?php echo $this->element('upcomingRem',array('upcomingRem'=>$upcomingRem,'totalUpcomingRem'=>$totalUpcomingRem)); ?>
	      		</div>
	      		</div>
	      		
	      		<div id="UpcomingRemVMDiv" style="text-align:right"></div>	      		
	      		<div id="UpcomingRemBSDiv" style="padding: 5px 0pt;"></div>
      		</div>
      		<?php if(count($archivedRem)> 0){ ?>
      		<div class="appDataGrp" >
	      		<div class="appDataTitle" >Sent Messages</div>
	      		<div id="archivedRemDiv">
	      		<div id="archivedRemTab">	      		
	      		<?php echo $this->element('archivedRem',array('archivedRem'=>$archivedRem,'totalArchivedRem'=>$totalArchivedRem)); ?>
	      		</div>
	      		</div>
	      		
	      		<div id="archivedRemVMDiv" style="text-align:right"></div>	      			      		 
      		</div>
      		<?php }?>
      		
		</div>
		<div id="wrapper" class="appColLeft">
		<div id="afterReminderAddDiv">		
		</div>
		</div>
   		<div class="appColLeft" id="appReminderAddDiv">
      		<?php echo $this->element('addReminder'); ?>		
		</div>
		<div class="clearRight">&nbsp;</div>
   <!-- Bhullakkad App ends -->   
 
 <input type="hidden" value="0" id="remAppUpcomingPagCnt" name="remAppUpcomingPagCnt" >
  <input type="hidden" value="" id="remAppUpcomingPag" name="remAppUpcomingPag" >
  <input type="hidden" value="0" id="remAppArcPagCnt" name="remAppArcPagCnt" >
  <input type="hidden" value="" id="remAppArcPag" name="remAppArcPag" >