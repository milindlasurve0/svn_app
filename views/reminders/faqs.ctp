<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<a onclick=" event.returnValue = false; return false;" id="link1538067051" href="/apps/getApps">&lt;&lt; Back to Personal Alerts</a>		</span>
	</div>
	<span>
		<a onclick=" event.returnValue = false; return false;" id="link1904207106" href="/apps/getApps">Personal Alerts</a> 
	</span> &nbsp;&gt;&gt;&nbsp; <span>FAQs</span>
  </div>
  <div class="info" style="padding: 0px 20px 20px 10px;">
	<div class="title5">What is Bhulakkad?</div>
	<p>Bhulakkad is an SMS based reminder service/Future SMS service in which you can set reminders for yourself as well as friends,colleagues,clients and others.
		<br>Best thing about Bhulakkad is its simplicity and easy to use interface. Bhulakkad can be a powerful personal organizer as well as a simple SMS based collaboration tool/CRM application.
		<br>There can be various other innovative ways you can use Bhulakkad.</p>
			
	<div class="title5">Why the application is called Bhulakkad?</div>
	<p>Bhulakkad is a Hindi term for Forgetter.<br>
	Bhulakkad is dedicated to the people who are super busy in their lives and find it difficult to manage multiple things including social and personal fronts.<br>
	Now, never forget to wish on birthday or anniversary, be that person your boss or relative.<br>
	If this application would have been there at the time of Ghajini (famous Bollywood movie), life of Aamir would have been a lot easier.
	</p>
	
	<div class="title5">How my recipient will receive message? Will there be any Ad inside the message?</div>
	<p>Recipient will get the reminder message from your number registered with us.<br>
No there will not be any Ad or promotional content inside the message that recipient will receive.</p>

	<div class="title5">How will I get message for my reminder?</div>
	<p>You will receive reminder SMS from sender SMSTADKA.</p>

	<div class="title5">How can I use Bhulakkad at my work?</div>
	<p>As a manager, you can set todos, milestones and event details for your team members.
<br>
You can set daily agenda for your team using Bhulakkad and members will get daily morning message for the tasks they need to finish during the day.
<br>
If you are into sales, You can set future SMS for your clients for various things like reminding for the next payments, wishing them on birthday or anniversary, reminding for renewal of services.
<br>Be smart and use this application smartly to improve your business and work efficiency and share your innovative ideas with us as well.</p>


	<div class="title5">How can I improve my social and personal life using Bhulakkad?</div>
	<p>Facebook wishes do not count in India to a large extent. SMS is more personal and expressive.
<br>
Just list down all the important dates of all the people who you wish to "Wish", set a personal message for each of them. You can find a lot of good wish SMSes on SMStadka. It's all done. You don't need to worry about people accusing you of being anti-social.
<br>
You can set reminders for your parents for doctor's appointments and medical schedules.
You can impress your girlfriend/boyfriend by remembering all the important dates like first date, first candle light dinner, first...the list goes on and on.</p>

<div class="title5">How can I use Bhulakkad for being more organized?</div>
	<p>Well, this is where you need it most.
<br>
Self organisation comes from within; Bhulakkad can be an amazing tool to achieve it.
<br>
You can set up all the important dates like bill payment, important meetings and so many other things in life that you just keep forgetting.</p>

</div>