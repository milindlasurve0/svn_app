

<script>

function countChars(id){
	var str = $('transl'+id).value;
	$('left'+id).innerHTML=str.length;
	$('hid'+id).value=str.length;
}

</script>
<?php 

$table = 'Message';
foreach($data as $message) {
	if(count($message['Tag']) == 0){
	$strD = strip_tags(trim($message['Message']['content']));
	?>
	
	<div id="divRefine<?php echo $message[$table]['id']?>">
		Message ID: <?php echo $message[$table]['id']?>
		<form id="form<?php echo $message[$table]['id']?>" name="refineMessages<?php echo $message[$table]['id']?>" action="/messages/editMessage/<?php echo $message[$table]['id'];?>">
		
		
		<br>
		<textarea onkeyup="countChars(<?php echo $message[$table]['id']?>);" onkeydown="countChars(<?php echo $message[$table]['id']?>);" class="input textarea" id="transl<?php echo $message[$table]['id']?>" name="data[Message][content]" style="height: 150px; width: 502px; line-height: 1.5em; font-family: Arial,Helvetica,sans-serif; font-size: 14px; direction: ltr;" autocomplete="off"><?php echo $strD;?></textarea>
		<br>
		
		<small><i>
                  <span>
                  	<span id="left<?php echo $message[$table]['id'];?>"> <?php echo $message[$table]['charCount'];?> </span>
                   <input type="hidden" id="hid<?php echo $message[$table]['id'];?>" name="data[Message][charCount]" value="<?php echo $message[$table]['charCount'];?>">
                    </input>&nbsp;characters
                  </span>
                </i></small>
		<br> <br>
		
		Title: <input type="text" value="<?php echo $message[$table]['title'];?>" name="data[Message][title]"><br>
		Category: <?php echo $message['Category']['name']; ?>
		<br>
		<div id="Tag_<?php echo $message[$table]['id'];?>"> <script> var url = '/tags/getTags'; new Ajax.Updater("Tag_<?php echo $message[$table]['id'];?>", url, {
  			parameters: {catid:<?php echo $message['Category']['parent'];?>},
  			evalScripts:true
		});	 </script></div>
		<?php echo $ajax->submit(
					'Submit', 
					array('url'=> array('action'=>'editMessage',$message[$table]['id']), 
										'update' => 'divRefine'.$message[$table]['id'])); ?>
		
		</form>
		
		
	</div>
	<hr><br>
<?php }} ?>