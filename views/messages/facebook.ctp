	<div>
	<?php echo $this->element('rightPackages',array('packData' => $packageData)); ?>
	<div class="leftCol">
		<div class="box2 ie6Fix2">
	    	<div class="header1">
	    		<div id="message">
	    			<?php if(isset($prevUrl)) {?>
						<span class="leftFloat">
						<?php echo $this->Html->link(__('<<', true), array('controller'=>'messages','action' => 'view',$catUrl,$prevUrl)); ?>
						</span>
					<?php } else {?>
						<span class="leftFloat lightText">
						&lt;&lt;
						</span>	
					<?php } ?>
					<?php if(isset($nextUrl)) {?>
						<span class="rightFloat">
						<?php echo $this->Html->link(__('>>', true), array('controller'=>'messages','action' => 'view',$catUrl,$nextUrl)); ?>
						</span>
					<?php } else {?>
						<span class="rightFloat lightText">
						&gt;&gt;
						</span>	
					<?php } ?>		
					
					<?php echo $message['Message']['title']; ?>
					<iframe src="http://www.facebook.com/plugins/like.php?href=<?php echo SITE_NAME.'messages/facebook/'.$message['Message']['url']; ?>&amp;layout=button_count&amp;show_faces=false&amp;width=450&amp;action=like&amp;font=lucida+grande&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:120px; height:21px;" allowTransparency="true"></iframe>
				</div>
	    	</div>
	        <div class="bottSpace">
	        	<div class="ie6Fix2">
			        <div class="inlineBlockElement">
	                <div class="SampJokesBG">
	                	<div class="rightFloat rightCol1 actionLinks1" style="padding-top:5px;">
	                    	<?php echo $this->element('message_footer',array('id' => $message['Message']['id'],'url'=>$message['Message']['url']));?>
	                    </div>
	                    <div class="leftCol1" style="border-right:1px solid #efefef;padding-top:5px;padding-bottom:5px;">                        
	                        <div class="SampJokesFull" style="background:#ffffff; padding:4px; padding-left:0px; margin-bottom:5px;">
	                        	<?php echo $message['Message']['content']; ?>
							</div>
	    					<div class="tagged">Category: 
	    						<?php echo $this->Html->link(__($message['Category']['name'], true), array('controller'=>'categories','action' => 'view',$objGeneral->nameToUrl($parentCat),$objGeneral->nameToUrl($message['Category']['name']))) ?>
	    					</div>
	    					
	    					<?php if(!empty($message['Tag'])){ ?>
	                        <div class="tagged ie6Fix2"> 
	                            <div class="leftFloat"> Tagged: </div>
	                            <div style="margin-left:50px;">
	                                <?php  $i=0; foreach($message['Tag'] as $tag) { if($i < 6) {?>
	                                <div class="taggLink taggLink1">
	                                  <div class="taggLinkBG">
	                                    <div class="taggLinkBorder">
	                                      <div class="taggLinkCont"><?php  echo $this->Html->link(__($tag['name'], true), array('controller' => 'tags','action' => 'view',$tag['url'])) ?></div>
	                                    </div>
	                                  </div>
	                                </div>
	                                <?php  } else break; $i++;} ?>                                
	                                <br class="clearLeft" />
	                            </div>
	                            <div class="clearLeft"></div>                                 		              		
	                        </div>
	                    	<?php } ?>
						</div>
	                    <br class="clearRight" />
	                </div>
	              	</div>
	            </div>
	        </div>
	    </div>
		<div class="box2 ie6Fix2">
	    	<div class="header">Similar Messages</div>
	        <div>
	        	<div class="ie6Fix2">
			        <div class="cont">
			        	<div class="SampJokes">
	                    	<ul style="padding-left:16px;">
	                    		<?php foreach($simMessages as $sim_mess) {?>
	                        		<li>
	                        			<?php echo $this->Html->link(__($sim_mess['Message']['title'], true), array('controller'=>'messages','action' => 'view',$catUrl,$sim_mess['Message']['url'])); ?>
	                        		</li>
	                            <?php } ?>
	                        </ul>
	                    </div>		        	
	              	</div>
	            </div>
	        </div>
	    </div>
	</div>
	<br class="clearRight" />
	
	<div class="catRow">
	    			<?php echo $this->element('categories',array('ocatData' => $ocatData));?>
	</div>
	</div>