<script>
function countChars(id){
	var str = $('transl'+id).value;
	$('left'+id).innerHTML=str.length + <?php echo ADSPACE; ?>;
}
</script>
<h3>Total Refined : <?php echo $count; ?></h3>
<?php
foreach($data as $message) {
	if(empty($message['content']['cyclicData'])){
		$strD = "*" . $packData['packages']['name'] ."*\n";
		$strD .= trim(strip_tags($message['content']['content'])); 
	}
	else {
		$strD = trim(strip_tags($message['content']['cyclicData'])); 
	}
	
	//$strD = trim(strip_tags($message['logs']['content']));
	?>
	<form id="form<?php echo $message['content']['id']?>" name="refineMessages<?php echo $message['content']['id']?>"> 
		
	<div id="divRefine<?php echo $message['content']['id']; ?>" class="rightFloat" style="margin-top:50px;">
		<?php echo $ajax->submit(
					'Submit', 
					array('url'=> array('action'=>'addRefinedData',$packData['packages']['id'],$message['content']['id']), 
										'after' => '$("divRefine'.$message['content']['id'].'").innerHTML = "Submitted";', 'update' => 'divRefine'.$message['content']['id'])); ?>
	</div>
	
	<div>
		<div class="leftFloat" style="padding-right:200px;">
		<textarea onkeyup="countChars(<?php echo $message['content']['id']?>);" onkeydown="countChars(<?php echo $message['content']['id']?>);" 
		class="input textarea" id="transl<?php echo $message['content']['id']?>" 
		name="data[Message][content]" style="height: 150px; width: 502px; line-height: 1.5em; 
		font-family: Arial,Helvetica,sans-serif; font-size: 14px; direction: ltr;" autocomplete="off"><?php echo $strD;?></textarea>
		<br>
		
		<small><i>
                  <span>
                  	<span id="left<?php echo $message['content']['id'];?>"> <?php echo (strlen($strD) + ADSPACE);?> </span>
                    </input>&nbsp;characters
                  </span>
                </i></small>
        </div>       
		<div style="padding-top:50px;">
			My Rating: <select name="data[Message][rating]" id="Rate<?php echo $message['content']['id']?>">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
			</select>				
		 </div>
		 <div class="clearLeft"></div>
		</div>
		
	<div class="clearBoth"> </div>
	</form>
		
	<hr><br>
<?php } ?>