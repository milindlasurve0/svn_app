<script>
function countChars(id){
	var str = $('transl'+id).value;
	$('left'+id).innerHTML=str.length;
}
</script>
<h2><?php echo $title; ?></h2><br/>
<?php
foreach($messages as $message) {
	$strD = trim(strip_tags($message['Message']['content']));
	?>
	<form id="form<?php echo $message['Message']['id']?>" name="refineMessages<?php echo $message['Message']['id']?>"> 
		
	<div id="divRefine<?php echo $message['Message']['id']; ?>" class="rightFloat" style="margin-top:50px;">
		<?php echo $ajax->submit(
					'Submit', 
					array('url'=> array('action'=>'addOnMobileData',$message['Message']['id'],$category_id), 
										'after' => '$("divRefine'.$message['Message']['id'].'").innerHTML = "Submitted";', 'update' => 'divRefine'.$message['Message']['id'])); ?>
	</div>
	
	<div id="divRemove<?php echo $message['Message']['id']; ?>" class="rightFloat" style="margin-top:50px;margin-right:20px;">
		<?php echo $ajax->submit(
					'Remove', 
					array('url'=> array('action'=>'removeOnMobileData',$message['Message']['id'],$category_id), 
										'after' => '$("divRemove'.$message['Message']['id'].'").innerHTML = "Submitted";', 'update' => 'divRemove'.$message['Message']['id'])); ?>
	</div>
	
	<div>
		<div class="leftFloat" style="padding-right:200px;">
		<textarea onkeyup="countChars(<?php echo $message['Message']['id']?>);" onkeydown="countChars(<?php echo $message['Message']['id']?>);" 
		class="input textarea" id="transl<?php echo $message['Message']['id']?>" 
		name="data[Message][content]" style="height: 150px; width: 502px; line-height: 1.5em; 
		font-family: Arial,Helvetica,sans-serif; font-size: 14px; direction: ltr;" autocomplete="off"><?php echo $strD;?></textarea>
		<br>
		
		<small><i>
                  <span>
                  	<span id="left<?php echo $message['Message']['id'];?>"> <?php echo strlen($strD);?> </span>
                    </input>&nbsp;characters
                  </span>
                </i></small>
        </div>
		 <div class="clearLeft"></div>
		</div>
		
	<div class="clearBoth"> </div>
	</form>
		
	<hr><br>
<?php } ?>