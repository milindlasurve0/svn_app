

<script>
function countChars(id){
	var str = $('transl'+id).value;
	$('left'+id).innerHTML=str.length;
	$('hid'+id).value=str.length;
}

function Ajaxupdate(cat_div,tag_div,id){
	var brokenstring= id.split("_"); 
	msgId = brokenstring[0];
	catId = brokenstring[1];
	var url = '/categories/getSubCategories';
	new Ajax.Updater(cat_div, url, {
  			parameters: {catid: catId},
  			evalScripts:true
		});
		
	var url = '/tags/getTags';
	new Ajax.Updater(tag_div, url, {
  			parameters: {catid:catId,msgid:msgId},
  			evalScripts:true
		});	
		
}
</script>
<?php 


//echo "<pre>"; print_r($data); echo "</pre>";
foreach($data as $message) {
	$strD = strtolower(strip_tags(trim($message[$table]['sms'])));
	if($table == "dump_brainy_quotes"){
		$strD .= "\n~" . $message[$table]['author'];
	}
	?>
	<div id="divRefine<?php echo $message[$table]['id']?>">
		<form id="form<?php echo $message[$table]['id']?>" name="refineMessages<?php echo $message[$table]['id']?>" action="/messages/submitMessage"> 
		
		Category (on site): <?php if($table != "dump_brainy_quotes") {
		
				echo $message[$table]['category'];}
				else{
					echo  $message['dump_brainy_topics']['category'];
				}
		?> <br>
		
		<br>
		<textarea onkeyup="countChars(<?php echo $message[$table]['id']?>);" onkeydown="countChars(<?php echo $message[$table]['id']?>);" 
		class="input textarea" id="transl<?php echo $message[$table]['id']?>" 
		name="data[Message][content]" style="height: 150px; width: 502px; line-height: 1.5em; 
		font-family: Arial,Helvetica,sans-serif; font-size: 14px; direction: ltr;" autocomplete="off">
		
		<?php echo $strD;?>
		
		</textarea>
		<br>
		
		<small><i>
                  <span>
                  	<span id="left<?php echo $message[$table]['id'];?>"> <?php echo strlen($strD);?> </span>
                    <input type="hidden" id="hid<?php echo $message[$table]['id'];?>" name="data[Message][charCount]" value="<?php echo strlen($strD);?>">
                    </input>&nbsp;characters
                  </span>
                </i></small>
		<br> <br>
		<div>
		Title: <input type="text" value="" name="data[Message][title]"><br>
		Choose Category: <select id="Category<?php echo $message[$table]['id']?>" onchange="Ajaxupdate('ocategory<?php echo $message[$table]['id']?>','otag<?php echo $message[$table]['id']?>',this.options[this.selectedIndex].id)">
							<option  value="0"> Select </option>
							<?php foreach($categories as $category) {?>
								<option id="<?php echo $message[$table]['id']?>_<?php echo $category['categories']['id'];?>" value="<?php echo $category['categories']['id'];?>"><?php echo $category['categories']['name']?></option>
							<?php } ?>
						</select>
						
						<div id="ocategory<?php echo $message[$table]['id']?>"></div>
						<div id="otag<?php echo $message[$table]['id']?>"></div>
		</div> 
		<div >
			My Rating: <select name="data[Message][rating]" id="Rate<?php echo $message[$table]['id']?>">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
			</select>				
		 </div>
		Add to Package: <br>
<select id="packages" multiple="multiple" name="data[Package][]">
<?php foreach($packs as $pack) {?>
<option value="<?php echo $pack['Package']['id']; ?>"><?php echo $pack['Package']['name']; ?></option>
<?php } ?>
</select>
		<br>
		<div style="margin-top:5px;" id="sub<?php echo $message[$table]['id'];?>">
		<?php echo $ajax->submit(
					'Submit', 
					array('url'=> array('action'=>'submitMessage',$message[$table]['id'],$table),
										'after' => '$("sub'.$message[$table]['id'].'").innerHTML = "Submitted";',
										'update' => 'divRefine'.$message[$table]['id'])); ?>
		
		<?php echo $ajax->link( 
    				'Delete', 
    				array('action' => 'deleteMessage', $message[$table]['id'],$table), 
    				array( 'update' => 'divRefine'.$message[$table]['id'], 'after' => '$("sub'.$message[$table]['id'].'").innerHTML = "Submitted";')); ?> 
    	</div>
		</form>
		
		
	</div>
	<hr><br>
<?php } ?>