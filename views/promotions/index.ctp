<script>
	function f_share(appId,quote){
		FB.init({
           appId:appId, cookie:true,
           status:true, xfbml:true
        });

		FB.ui(
			{method: 'apprequests', message: 'You should learn more about this cool facebook app.', data: decodeURI(quote)}
		);	
	}

	function f_publish(appId,quote){
	var temp = decodeURIComponent(quote.replace(/\+/g, " "));
	
	FB.init({
           appId:appId, cookie:true,
           status:true, xfbml:true
        });

        FB.ui({ method: 'feed',
           message: temp});	
	}
</script>
<style type="text/css">
@charset "utf-8";


body, p, ul, ol, li, h1, form { margin:0px; padding:0px; font-family:georgia; }
ol { margin-left:22px; }
ol.info  { padding-bottom:15px; }
.info p { padding-bottom:20px; }
ol.info li { padding-bottom:12px; }

body { text-align:center; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:0.9em; color:#000;}


.blankState {padding-top:10px;font-size:0.8em;}

a, a:visited { text-decoration:none; color:#0167a9; }
a:hover { background:none; }
a img { border:0px; }

h1 { padding:10px; background:#efefef; border-bottom:1px solid #ec724a; text-align:left; font-weight:bold; font-size:1.2em;
	-moz-border-radius-topleft:8px;-moz-border-radius-topright:8px;
	-webkit-border-top-left-radius:8px;-webkit-border-top-right-radius:8px;
	border-top-left-radius:8px;border-top-right-radius:8px;
}

/* Common classes */

.header { padding-left:20px; padding-right:20px; }
.cont { }
.container { padding:5px 0px 15px;  }
.cursorWait { cursor:wait; }

.bottSpace { padding:10px 0px; }

.highlight { font-weight:bold; }

.smallerFont { font-size:0.8em; }

.smallTitle { color:#4d5e69; }

.leftFloat { float:left }
.rightFloat { float:right }
.clearRight { clear:right; }
.clearLeft { clear:left; }
.clearBoth { clear:both; }
br.clearLeft, br.clearRight, br.clearBoth, div.clearBoth, div.clearLeft, div.clearRight { line-height:0px; font-size:0px; height:0px; }

.alignCenter { text-align:center; }

.space1 { padding:5px 0px 5px 0px; }
.space2 { padding-bottom:20px; padding-left:22px; }


#recentMsgs {
	background:#efefef;
	border:2px solid #c3c3c3;
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px; 
}

.recentMsgsCont { 
	list-style:none; list-style-type:none;padding:0px 8px;
	border:2px solid #c3c3c3;background:#fff;
	margin:0px 20px;
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
}

.antina { position:absolute; top:0px; left:0px;
	border:2px solid #c3c3c3;
	border-bottom:0px;
	background:#efefef;
	-webkit-border-top-left-radius-: 8px;
	-moz-border-radius-topleft: 8px;
	border-top-left-radius: 8px;
	-webkit-border-top-right-radius-: 8px 8px;
	-moz-border-radius-topright: 8px 8px;
	border-top-right-radius: 8px;
	width:20px; height:15px;top:-12px; left:-2px;	 
 }
 
 #recentMsgs {
 	margin-top:10px;
 }
 

.adjust1 { margin-left: 70px; }
.posRlative { position:relative; }
.appRemRepeatBox { display: none; background: none repeat scroll 0% 0% rgb(208, 207, 207); border: 1px solid rgb(85, 85, 85); padding: 0px 0px 12px 12px; margin-bottom: 20px; }

* html .ie71{
	padding-top:0px !imprtant;
}


.retailAddBox { width:240px; margin:10px 5px 10px 0px; display:inline-block; vertical-align:top }
.retailData { margin-left:25px; }
.retailDataLabel { float:left;width:80px; }
.sampleNo { margin:0px auto; float:left; 
}

	
	a:hover.retailBut, .retailBut { background:url(/img/buttonRetailer.png); height:23px; padding:0px 5px 3px; border:0px; color:#fff; }
.enabledBut { background-position:0px 0px; }
.disabledBut { background-position:0px -32px; }
.horiPack { color:#1b67a1;font-size:14px;font-weight:bold;float:left; padding:2px;background:#efefef;margin-right:2px; }
.horiPack img { width:105px; }
</style>
  
  <div>
    <div class="rightColSpace">
      	<div style="position: relative;width: 700px;" id="recentMsgs" >
      	  <!--<div style="" class="antina"></div>-->                  
          <div class="SampJokesTitle"></div>
          <div>
            <div style="width: 700px;" class="container">
              <div style="padding:0px 10px 0px 20px;">
                            
              <h1 style="color:#f56421;font-size:40px; padding-top:30px;font-weight:normal; padding-left:0px;">
              <img alt="smstadka" style="position:relative; top:-20px;float:right; width:200px" src="/img/fblogo.png?1234">
              	Random Quotes
              </h1>
              </div>
              <div style="margin:15px 20px 0px 30px;padding:0px 20px 0px 30px;background:url(/img/quote-left.png) no-repeat">              		 
              <!-- <ul style="text-align:left;">                 
             	  <li style="display: block;height:auto;"> -->
                  	<p id="sampSMS2" style="float:left;background:url(/img/quote-right.png) top right no-repeat;font-size:21px;color:#1b67a1;padding:10px 0px 15px;"><?php echo $quote; ?></p>
                 <!-- </li>                                 
               </ul> --><br style="clear:left;">
              </div>
              <div style="margin-left: 4%; padding: 10px 0px;">
              	<div style="float:left;margin-right: 10px;">
					<a href="javascript:void(0);" class="retailBut enabledBut" onclick="f_share('<?php echo $appId; ?>','<?php echo urlencode($quote); ?>');">Share with your friends</a>
				</div>
				<div style="float:left;margin-right: 10px;">
					<a href="javascript:void(0);" class="retailBut enabledBut" onclick="window.location.reload();">Next Quote</a>
				</div>
				<div style="float:left;margin-right: 10px;">
					<a href="javascript:void(0);" class="retailBut enabledBut" onclick="f_publish('<?php echo $appId; ?>','<?php echo urlencode($quote); ?>');">Publish on your Wall</a>
				</div>
                <!-- <ul class="sampleNo" style="margin-right: 2px;">                                                     
                  <li><a onclick="window.location.reload();" href="javascript:void(0)">&gt;</a></li>                  
                </ul>&nbsp;&nbsp;
                <ul class="sampleNo" style="margin-right: 2px;">                                                     
                  <li><a href="javascript:void(0)" onclick="f_share('<?php echo $appId; ?>','<?php echo urlencode($quote); ?>');"> share</a></li>                  
                </ul>&nbsp;&nbsp;
                <ul class="sampleNo">                                                     
                  <li><a href="javascript:void(0)" onclick="f_publish('<?php echo $appId; ?>','<?php echo urlencode($quote); ?>');"> publish</a></li>                  
                </ul> -->                
                <br class="clearLeft">
              </div>              
          </div>
		</div>
    </div>
  </div>
  
  <div style="width:700px;">
  	<div style="padding:0px 10px 10px 20px;">                            
      <div style="border-bottom:1px solid #EC724A;color:#f56421;font-size:33px;padding-bottom:10px;padding-top:30px;font-weight:normal; padding-left:0px;">              
      	Get quotes daily on your mobile
      </div>
      <p style="padding-top:20px;color:#1b67a1;font-size:14px;font-weight:bold;">
      	Get various quotes on your mobile daily by subscribing to SMSTadka online packages
      </P>
      <div style="padding-top:20px;">
      	<ul style="list-style:none;">
      		<li class="horiPack"><a href="/packages/view/quotes" target="_blank"><img src="/img/packages/package_45.gif"></a><br>Quotes<br>&nbsp;</li>
      		<!-- <li class="horiPack"><img src="/img/packages/package_47.gif"><br>Funny<br>Quotes</li> -->
      		<li class="horiPack"><a href="/packages/view/life-quotes" target="_blank"><img src="/img/packages/package_48.gif"></a><br>Life<br>Quotes</li>
			<li class="horiPack"><a href="/packages/view/attitude-quotes" target="_blank"><img src="/img/packages/package_46.gif"></a><br>Attitude<br>Quotes</li>
			<li class="horiPack"><a href="/packages/view/famous-quotes" target="_blank"><img src="/img/packages/package_49.gif"></a><br>Famous<br>Quotes</li>
			<li class="horiPack"><a href="/packages/view/love-quotes" target="_blank"><img src="/img/packages/package_50.gif"></a><br>Love<br>Quotes</li>
			<li class="horiPack"><a href="/packages/view/friendship-quotes" target="_blank"><img src="/img/packages/package_51.gif"></a><br>Friendship<br>Quotes</li>      		      		
      	</ul>
      	<br style="clear:left">
      	<div style="margin: 20px 0 0 10px;">
					<a href="javascript:void(0);" class="retailBut enabledBut" onclick="#">Subscribe Now!</a>
				</div>
      </div>
  	</div>
  </div>
  <!--<br/>
  <div class="leftFloat box1" style="text-align:left;width:100%">
    <div class="header">
    	SMSTadka Quotes	
    </div>
    <div class="cont">
      <ul>      
        <li><?php echo "Today's quote: this is real".$quote; ?></li>
      </ul>
      <div class="moreLink">      	
      </div>
      <a href="javascript:void(0)" onclick="f_share('<?php echo $appId; ?>','<?php echo urlencode($quote); ?>');"> share</a>
<a href="javascript:void(0)" onclick="f_publish('<?php echo $appId; ?>','<?php echo urlencode($quote); ?>');"> publish</a>
    </div>
    <div class="box1Footer"></div>    
  </div>-->