
<form id="send" name="send" method="post" accept-charset="utf-8" >
<div>
<br><b>Number of the users registered: <?php echo $count; ?></b> <br>
<b>Total SMS Sent: <?php echo count($logs); ?>, Left to sent: <?php echo (10 - count($logs));?></b>

<?php echo $this->element('messages_sent',array('logData' => $logs));?>

<div>
	<table width="700">
	<tr>
		<td width="350" valign="top"> 
			<textarea onkeyup="countChars();" onkeydown="countChars();" 
			class="input textarea" id="transl" 
			name="data[content]" style="height: 250px; width: 350px; line-height: 1.5em; 
			font-family: Arial,Helvetica,sans-serif; font-size: 14px; direction: ltr;" autocomplete="off"></textarea>
		</td>	
	</tr>
	</table>
	<br>		
	<small><i>
	<span id="left"> 0&nbsp;chars </span>
	</i>
    </small>
		
<br><br>
<div id="submitted"></div>
<div class="field">
	<?php echo $ajax->submit('/img/spacer.gif', array('class' => 'otherSprite oSPos7','url'=> array('controller'=>'promotions', 'action'=>'promoShoot'), 'update' => 'data','condition' => 'dataValidate($("transl").value,140)','after' => '$("transl").value = "";$("submitted").innerHTML = "Submitted successfully";')); ?>
</div>
</div>
<div class="clearRight"></div>
</form>
<script>
var count = <?php echo count($logs); ?>;
function countChars(){
	var str = $('transl').value;
	$('left').innerHTML= str.length + " chars";
}
function dataValidate(name,maxlength){
	if(count == 10){
		alert("Total message limit exceeded");
		return false;
	}
	else if( (name == null) || (name.length == 0)){
		alert("Enter some data");
		return false;
	}
	if(maxlength != -1 && name.length > maxlength){
		alert("Message should contain maximum of " + maxlength + " characters");
		return false;
	}	
	return true;	
}
</script>