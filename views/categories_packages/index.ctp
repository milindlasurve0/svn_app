<div class="categoriesPackages index">
	<h2><?php __('Categories Packages');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('category_id');?></th>
			<th><?php echo $this->Paginator->sort('package_id');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($categoriesPackages as $categoriesPackage):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $categoriesPackage['CategoriesPackage']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($categoriesPackage['Category']['name'], array('controller' => 'categories', 'action' => 'view', $categoriesPackage['Category']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($categoriesPackage['Package']['name'], array('controller' => 'packages', 'action' => 'view', $categoriesPackage['Package']['id'])); ?>
		</td>
		<td><?php echo $categoriesPackage['CategoriesPackage']['created']; ?>&nbsp;</td>
		<td><?php echo $categoriesPackage['CategoriesPackage']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $categoriesPackage['CategoriesPackage']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $categoriesPackage['CategoriesPackage']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $categoriesPackage['CategoriesPackage']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $categoriesPackage['CategoriesPackage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Categories Package', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Categories', true), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category', true), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages', true), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package', true), array('controller' => 'packages', 'action' => 'add')); ?> </li>
	</ul>
</div>