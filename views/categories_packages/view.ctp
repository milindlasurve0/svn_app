<div class="categoriesPackages view">
<h2><?php  __('Categories Package');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoriesPackage['CategoriesPackage']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Category'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($categoriesPackage['Category']['name'], array('controller' => 'categories', 'action' => 'view', $categoriesPackage['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Package'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($categoriesPackage['Package']['name'], array('controller' => 'packages', 'action' => 'view', $categoriesPackage['Package']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoriesPackage['CategoriesPackage']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoriesPackage['CategoriesPackage']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Categories Package', true), array('action' => 'edit', $categoriesPackage['CategoriesPackage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Categories Package', true), array('action' => 'delete', $categoriesPackage['CategoriesPackage']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $categoriesPackage['CategoriesPackage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories Packages', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Categories Package', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories', true), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category', true), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages', true), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package', true), array('controller' => 'packages', 'action' => 'add')); ?> </li>
	</ul>
</div>
