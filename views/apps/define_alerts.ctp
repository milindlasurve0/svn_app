<div class="info" style="padding: 0px 20px 20px 10px;">
	<div class="title5">What are Personal Alerts?</div>
	<p>Personal Alert service by SMSTadka.com is a set of new tools offered to create your own alerts depending upon your specific needs.<br>
		<br>All you need to do is, configure your specific choices and you'll receive SMS alerts based on that.
<br>For example: Choose the stock you wish to track and you will receive the News and price related to that stock.
		<br>Set reminders, future messages with wishes for yourself or any other Indian mobile number.
		<br>SMSTadka promises you to bring more new applications to make your life easier and happier.
		
		</p>
	<div class="title5">How is it different from packages?</div>
	<p>Packages are not customizable, you will get alerts specific to the package.<br> 
However in personal alerts you can create your own custom alerts.</p>

	<div class="title5">Are you planning to build more such applications?</div>
	<p>SMStadka is coming up with various other such applications for many other fields like health, education, entertainment and many more.<br>
We are also inviting developers to join hands and build their own applications on our platform. for more details write to us at <a href="mailto:developers@smstadka.com">developers@smstadka.com</a></p>
</div>