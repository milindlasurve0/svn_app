<script type="text/javascript" src="/js/calendar.js"></script>
<script>
function updateScrPlus(id){
	var url = '/apps/updateScrPlus';
	var rand   = Math.random(9999);
	var pars   = "id="+id+"&text="+encodeURIComponent($('scorePlus'+id).value)+"&rand="+rand;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 							
						alert('Done');				
					}
				});
}

function clearScore(id){
	var url = '/apps/clearScore';
	var rand   = Math.random(9999);
	var pars   = "id="+id+"&rand="+rand;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						$('score'+id).innerHTML = '';
						alert('Done');				
					}
				});
}
</script>
<input type="hidden" value="<?php echo $id; ?>" >
<?php if($id != ''){ ?>
<form name="createMatch" method="post" action="/apps/updateMatch/<?php echo $id; ?>">
<?php }else{ ?>
<form name="createMatch" method="post" action="/apps/createMatch">
<?php } ?>
<table>
	<tr>
		<td colspan="3"><h2>Set Matches</h2></td>
	</tr>
	<tr>
		<td colspan="3">
			<?php
				if($message != ''){
					echo "<h4 style='color:red'>".$message."</h4>";
				}
			?>
		</td>
	</tr>	
	<tr>
		<td>
			Match Title
		</td>
		<td>
			<input type="text" name="matchTitle" id="matchTitle" value="<?php echo $matchTitle; ?>">
		</td>
	</tr>
	<tr>
		<td>
			Match Data URL
		</td>
		<td>
			<input type="text" name="matchUrl" id="matchUrl" value="<?php echo $matchUrl; ?>">
		</td>
	</tr>
	<tr>
		<td>
			Match Data Back-up URL
		</td>
		<td>
			<input type="text" name="matchBakUrl" id="matchBakUrl" value="<?php echo $matchBakUrl; ?>">
		</td>		
	</tr>
	<tr>
		<td>
			Match Type
		</td>
		<td>
			<?php $typeArr = array("ODI","T20","Test"); ?>
			<select name="matchType" id="matchType">
	        	<?php 
	        	 foreach($typeArr as $k => $v){
	        	 	$selected = '';
	        	 	if($k == $matchType)
						$selected = 'selected';	        	 	
	        		echo '<option value="'.$v.'"  '.$selected.'>'.$v.'</option>';
	        	 }
	        	?>							          
		    </select>
		</td>		
	</tr>
	<tr>
		<td>
			Match Day & Time
		</td>
		<td>
			Date: <input type="text" name="matchDate" id="matchDate" value="<?php echo $matchDate; ?>" onmouseover="fnInitCalendar(this, 'matchDate','expiry=true,close=true')" style="width:117px;" />
			<?php $hourArr = array("08" => "08:00 AM","09" => "09:00 AM","10" => "10:00 AM","11" => "11:00 AM","12" => "12:00 PM","13" => "01:00 PM","14" => "02:00 PM","15" => "03:00 PM","16" => "04:00 PM","17" => "05:00 PM","18" => "06:00 PM","19" => "07:00 PM","20" => "08:00 PM","21" => "09:00 PM","22" => "10:00 PM","23" => "11:00 PM","00" => "00:00 AM","01" => "01:00 AM","02" => "02:00 AM","03" => "03:00 AM","04" => "04:00 AM","05" => "05:00 AM","06" => "06:00 AM","07" => "07:00 AM"); ?>
			Hour: <select name="matchHr" id="matchHr" style="width:80px">
	        	<?php 
	        	 foreach($hourArr as $k => $v){
	        	 	$selected = '';
	        	 	if($k == $matchHr)
						$selected = 'selected';
							        	 		        	 	
	        		echo '<option value="'.$k.'" '.$selected.'>'.$v.'</option>';
	        	 }
	        	?>							          
		    </select>
		    <?php $minArr = array("00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"); ?>
			Minute: <select name="matchMin" id="matchMin" >
	        	<?php 
	        	 foreach($minArr as $k => $v){
	        	 	$selected = '';
	        	 	if($v == $matchMin)
						$selected = 'selected';	        	 	
	        		echo '<option value="'.$v.'"  '.$selected.'>'.$v.'</option>';
	        	 }
	        	?>							          
		    </select>
		</td>
	</tr>
	<tr>
		<td>
			Duration
		</td>
		<td>
			<?php $hrArr = array("02","03","04","05","06","07","08","09","10"); ?>
			Hour: <select name="matchDur" id="matchDur">
	        	<?php 
	        	 foreach($hrArr as $k => $v){
	        	 	$selected = '';
	        	 	if($v == $matchDur)
						$selected = 'selected';	        	 	
	        		echo '<option value="'.$v.'"  '.$selected.'>'.$v.'</option>';
	        	 }
	        	?>							          
		    </select>		    
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="<?php echo $id != ''? 'Update' : 'Add'; ?>">
		</td>
	</tr>
	
</table>
</form>
<br/><br/><br/>
<table width="100%" border="1" cellspacing="0">
	<tr>
		<td colspan="4"><h2>Live Matches</h2><br/></td>
	</tr>
	<tr>
		<td style="width:20%">
			<b>Match Info</b>
		</td>
		<td style="width:20%">
			<b>Live Score</b>
		</td>
		<td style="width:20%">
			<b>Extra Msg</b>
		</td>
		<td>
			<b>Match URL</b>
		</td>		
	</tr>	
<?php if(count($matches)>0){foreach($matches as $m){ ?>

	<tr <?php if($m['cricketMatches']['status'] == '1'){ echo "bgcolor='#C0C0C0'"; } ?> >
		<td>
			<?php echo $m['cricketMatches']['matchTitle']." (".$m['cricketMatches']['type'].")"; ?>
			<a href="/apps/updateMatch/<?php echo $m['cricketMatches']['id']; ?>" target="_self">Edit</a>
			</br>
			<?php echo $objGeneral->dateTimeFormat($m['cricketMatches']['timing']);?>
			</br>
			<?php echo "Duration: ".$m['cricketMatches']['duration']." Hrs";?>
			</br> 
			<span id="matchStatus<?php echo $m['cricketMatches']['id']; ?>">
				<?php 
					$status = 'Deactivate';
					if($m['cricketMatches']['status'] == '1')
						$status = 'Activate';
				?>
				<a href="/apps/setMatchStatus/<?php echo $m['cricketMatches']['id']; ?>"><?php echo $status; ?></a>
			</span>
		</td>
		<td>
			<span id="score<?php echo $m['cricketMatches']['id']; ?>"><?php echo $m['cricketMatches']['score']; ?></span>
			</br><a href="javascript:void(0)" onclick="clearScore('<?php echo $m['cricketMatches']['id']; ?>')">Clear</a> |  
			<?php 
				$status = 'Pause updates';
				if($m['cricketMatches']['paused'] == '1'){
					echo "<span style='font-size:15px;'><b>PAUSED</b></span>";
					$status = 'Resume updates';
				}
			?>
			<a href="/apps/scorePause/<?php echo $m['cricketMatches']['id']; ?>"><?php echo $status; ?></a> |
			
		</td>
		<td>
			<textarea name="scorePlus<?php echo $m['cricketMatches']['id']; ?>" id="scorePlus<?php echo $m['cricketMatches']['id']; ?>"><?php echo stripslashes($m['cricketMatches']['scorePlus']); ?></textarea>
			</br><a href="javascript:void(0)" onclick="updateScrPlus('<?php echo $m['cricketMatches']['id']; ?>')">Update</a>
		</td>		
		<td >
			URL: <a href="<?php echo $m['cricketMatches']['matchUrl']; ?>" target="_blank"><?php echo $m['cricketMatches']['matchUrl']; ?></a>
			</br>
			Back-up: <a href="<?php echo $m['cricketMatches']['matchBakUrl']; ?>" target="_blank"><?php echo $m['cricketMatches']['matchBakUrl']; ?></a>
		</td>		
	</tr>

<?php }}else{ ?>
	<tr>
		<td colspan="4">No Live Matches<br/></td>
	</tr>
<?php } ?>
</table>
<br/><br/><br/>