<div class="loginCont">
  <div>
    <div class="leftFloat dashboardPack">
      <div class="catList">
        <ul id='innerul'>
          <li class="hList" name='innerli'>
           		<?php echo $ajax->link( 
    				'All Personal Alerts', 
    				array('action' => 'allApps'),
    				array('id' => 'allApps', 'class' => 'sel' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("allApps").removeClassName("loader")' )
					); ?>    				
		  </li>
		  <?php if(count($apps) == 0) { ?>
		  <li class="hList" name='innerli'><?php echo $ajax->link( 
    				'My Alerts', 
    				array('action' => 'myApps'),
    				array('id' => 'myApps', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("myApps").removeClassName("loader")' )
					); ?> </li>
		  <?php } else { ?>
		  <li> <a class="hList" href="javascript:void(0);">My Alerts</a>
          	<div class="sublist">
          		<ul>
          		<?php foreach($apps as $app) {
          			  $id = 'app' . $app['SMSApp']['id'];
          		?>
          			<li name='innerli'>
          				<a href="javascript:void(0);" onclick="subApp('<?php echo $app['SMSApp']['controller_name']; ?>',0);"><?php echo $app['SMSApp']['name']; ?> </a>
          			</li>
          		<?php } ?>
          		</ul>
          	</div>	
          </li>	
		  <?php } ?>	
		  <li class="hList" name='innerli'><?php echo $ajax->link( 
    				'What are Personal Alerts?', 
    				array('action' => 'defineAlerts'),
    				array('id' => 'defineAlerts', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("defineAlerts").removeClassName("loader")' )
					); ?> </li>					
        </ul>
      </div>
      
      <div style="display:none">
      	<div class="color4 fntSz19" style="position:relative;">
      		<span class="testimonial" style=" top:35px;">"</span>PNR Alert helped me track my PNR and inturn I saved my precious time doing the thesis i am required to. Thanks SMS Tadka for this service.
      		<span class="testimonial" style="bottom:-17px;position:absolute">"</span>
      	</div>
      	<div style="padding-top:10px;text-align:right;">
      		<div class="color3 fntSz17 strng rightFloat">
      			<img src="/img/spacer.gif" width="60px" height="60px" style="float:left; margin-right:5px;">
      			<div style="overflow:hidden; float:left;"><div>-&nbsp;Astha&nbsp;Gulatee</div><div>College&nbsp;Student</div></div>
      			<div class="clearLeft"></div>
      		</div> 
      	</div>
      </div>
    </div>
    
    <div style="margin-left:223px" id="innerDiv">
      	<?php
			echo $this->element('all_apps');
      	?>
    </div>
    <br class="clearLeft" />
 </div>