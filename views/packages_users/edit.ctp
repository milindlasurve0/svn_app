<div class="packagesUsers form">
<?php echo $this->Form->create('PackagesUser');?>
	<fieldset>
 		<legend><?php __('Edit Packages User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('package_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('review');
		echo $this->Form->input('active');
		echo $this->Form->input('slot_id');
		echo $this->Form->input('start');
		echo $this->Form->input('end');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('PackagesUser.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('PackagesUser.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Packages Users', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Packages', true), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package', true), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Slots', true), array('controller' => 'slots', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slot', true), array('controller' => 'slots', 'action' => 'add')); ?> </li>
	</ul>
</div>