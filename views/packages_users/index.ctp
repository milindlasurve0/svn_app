<div class="packagesUsers index">
	<h2><?php __('Packages Users');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('package_id');?></th>
			<th><?php echo $this->Paginator->sort('user_id');?></th>
			<th><?php echo $this->Paginator->sort('review');?></th>
			<th><?php echo $this->Paginator->sort('active');?></th>
			<th><?php echo $this->Paginator->sort('slot_id');?></th>
			<th><?php echo $this->Paginator->sort('start');?></th>
			<th><?php echo $this->Paginator->sort('end');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($packagesUsers as $packagesUser):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $packagesUser['PackagesUser']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($packagesUser['Package']['name'], array('controller' => 'packages', 'action' => 'view', $packagesUser['Package']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($packagesUser['User']['id'], array('controller' => 'users', 'action' => 'view', $packagesUser['User']['id'])); ?>
		</td>
		<td><?php echo $packagesUser['PackagesUser']['review']; ?>&nbsp;</td>
		<td><?php echo $packagesUser['PackagesUser']['active']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($packagesUser['Slot']['name'], array('controller' => 'slots', 'action' => 'view', $packagesUser['Slot']['id'])); ?>
		</td>
		<td><?php echo $packagesUser['PackagesUser']['start']; ?>&nbsp;</td>
		<td><?php echo $packagesUser['PackagesUser']['end']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $packagesUser['PackagesUser']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $packagesUser['PackagesUser']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $packagesUser['PackagesUser']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $packagesUser['PackagesUser']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Packages User', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Packages', true), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package', true), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Slots', true), array('controller' => 'slots', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slot', true), array('controller' => 'slots', 'action' => 'add')); ?> </li>
	</ul>
</div>