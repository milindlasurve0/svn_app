<div class="box2" style="width:670px;">
            
            <div class="pack2" style="margin-left:0px;min-height:100px;max-height:600px;overflow-y:auto;">
            	<table border="0" cellpadding="0" cellspacing="0" style="width:650px;" summary="Active Packages">
        			<caption class="header">
			        Active Packages
			        </caption>
			        
			        <thead>
			          <tr class="noAltRow">			            
			            <th style="width:30%">Package Name</th>			            
                        <th style="width:17%">Subscribed Date</th>
			            <th style="width:17%">Expiry Date</th>
                        <th style="width:16%">Amount</th>
                        <th style="width:10%">&nbsp;</th>
			          </tr>
                      </thead>
                      <tbody>            <?php foreach($resultActive as $active) {?>
             
                                      
                                      <tr>
                                      	
                                       <td>
                                      <?php echo $this->Html->link(__($active['packages']['name'], true), array('controller' => 'packages','action' => 'view',$active['packages']['url'])) ?>
                                      	</td>
                                        <td><?php echo $objGeneral->dateFormat($active['PackagesUser']['start']);?></td>
                                        <td><?php echo $objGeneral->dateFormat($active['PackagesUser']['end']);?></td>
                                        <td><?php echo $active['packages']['price'];?></td>
                                        <td>
                                        	<a href="javascript:void(0);" onclick="subPackage('<?php echo $objMd5->encrypt($active['PackagesUser']['package_id'],encKey);?>','unsub');"> Unsubscribe </a>
                                        </td>
                                      </tr>
                                     
             	 		<?php } ?> 
              			</tbody>
			    </table>
            </div>
          </div>
       