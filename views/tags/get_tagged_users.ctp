<div style="width:600px;">
<?php foreach($data as $dt) { ?>
<div class="appDataCont appDataContClr1" style="margin-bottom:20px;">
	<div class="rightFloat">
		<span style="padding-top: 2px;" class="leftFloat"><?php echo $dt['mobile_numbering_area']['area_name']; ?></span>
	</div>
	<span><a href="/groups/getUserInfo/<?php echo $dt['User']['mobile']; ?>"><?php echo $dt['User']['mobile']; ?></a></span>
	<br class="clearBoth"/>
	<?php if(isset($dt['Comment'])) { ?>
	<div style="padding-top:20px;">
		<div class="rightFloat">
			<span class="leftFloat"><?php echo $dt['Comment']['created']; ?></span>
		</div>
		<span><b>Last Comment</b></span>
		
		<p class="appDataDesc"><?php echo $dt['Comment']['comment']; ?></p>
	</div>
	<br class="clearBoth"/>
	<?php } ?>
	<div class="tagged ie6Fix2" style="padding-top:10px;"> 
        <span class="leftFloat" style="font-size:10pt"> <b>Tagged : </b></span>
        <div style="margin-left:50px;">
        	<?php foreach($dt['Tags'] as $tag) { ?>
			<div class="taggLink taggLink1" style="font-size:8pt">
              <div class="taggLinkBG">
                <div class="taggLinkBorder">
                  <div class="taggLinkCont"><a href="/tags/getTaggedUsers/<?php echo $tag['Tagging']['name']; ?>"><b><?php echo $tag['Tagging']['name']; ?></b></a></div>
                </div>
              </div>
            </div>
 			<?php } ?>
        </div>
        <div class="clearLeft"></div>                                 		              		
    </div>
</div>
<?php } ?>
</div>