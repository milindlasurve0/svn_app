<div style="width:760px;"> 
    <div>       
        <div>
          <div class="box2" style="margin:0px;">
            <div class="pack2" style="margin:0px 10px 15px 0px;min-height:100px;max-height:600px;overflow-y:auto;" >
            	<?php if($par == null || $par == "To") {?>
            	<table border="0" cellpadding="0" cellspacing="0"  summary="Messages Sent To You" class="ListTable">
			<caption class="header">
	        Messages Sent To You (<?php echo $count_to;?>)
	        </caption>
	        <?php if ($count_to > 0) { ?>
	         
	          <?php foreach($messages_to as $log) {?>
	         
	          <tr>
	          	<td><ul class="box6Cont2" style="padding:0px;margin:0px;">
	          		<li class="box6Cont2" style="background:none; border-top:1px solid #f3f2f2;border-bottom:1px solid #f3f2f2;padding:0px;margin:0px;">
					    <div class="packageDetail">
					      <div style="width:700px;">
					      	<div class="packName" style="padding-bottom:5px;"> 
					      	<?php if(empty($log['Log']['package_id'])) {?>
	            				From: <?php echo $log['User']['mobile'];?>
	            			<?php } else {?>
	            
					        <a href="/packages/view/<?php echo $log['Package']['url']; ?>"><?php echo $log['Package']['name']; ?></a>
					        <?php } ?>
					        <span class="smallerFont" style="color:#888; font-size:0.7em; font-weight:normal;"> <?php echo $objGeneral->dateTimeFormat($log['Log']['timestamp']);?></span></div>
					        <div class="packDesc oldSMS"> <?php echo $log['Log']['content']; ?>
					        </div>           
					        <div style="margin-top:2px; width:555px;" >            	
					          &nbsp;
					        </div>
					        <div class="clearRight"></div>
					      </div>
					      <div class="clearLeft"></div>
					    </div>        
					  </li> </ul>
	          	</td>
	          </tr>
	          <?php } } else { ?>
	          <tr class="noAltRow"><td>
	          	<p class="blankState"> There seems to be ZERO messages sent to you by SMSTadka. Subscribe to Packages to start receiving messages. This section shows you the history (Record) of all the messages sent to you either by us or by your friends.<br>
	          	This will update you the status of messages.</p></td></tr>
	          <?php } }?>	
	    </table>
	    
		<?php if($par == null || $par == "By") {?>
	    <table border="0" cellpadding="0" cellspacing="0" summary="Messages Sent By You" class="ListTable" width="100%">
			<caption class="header">
	        Messages Sent By You (<?php echo $count_by;?>)
	        </caption>
	        <?php if ($count_by > 0) { ?>
	          <tr>
	            <th style="width:40%">Message</th>
	            <th style="width:15%">To Whom</th>
	            <th style="width:25%">Time</th>
	            <th style="width:20%">Status</th>
	          </tr>
	          <?php foreach($messages_by as $log) {?>
	          <tr>
	            <td><?php echo urldecode($log['Log']['content']);?></td>
	            <td><?php echo $log['Log']['mobile'];?></td>
	            <td><?php echo $objGeneral->dateTimeFormat($log['Log']['timestamp']);?></td>
	            <?php if($log['msgDelLog']['status_flag'] == "0") {?>
	            	<td>Message Sent</td>
	            <?php } else {?>
	            	<td>Message Delivered</td>
	            <?php }?>
	            
	          </tr>
	          <?php } } else { ?>
	          <tr class="noAltRow"><td>
	          <p class="blankState"> Presently, You have not sent any message, Go to 'Send SMS' tab to start sending SMSes to anyone in India (via SMSTadka). You can also forward super cool messages/Jokes/Shayris and a lot more from our Messages section (Links shown at the bottom). <br> This section also provides the delivery status of your messages. </p>
	          </td></tr>
	          <?php } }?>	
	          
	    </table>
          	</div>
          	<?php if(!$pop) {
          		if($par == "By" && $count_by > 5){ ?>
					<div class="rightFloat"> <a href='javascript:void(0);' onclick='allMessageLogs("By");'> Click here to view all messages sent by you</a></div><br class="clearRight" />
			<?php	}
				else if($par == "To" && $count_to > 5){ ?>
					<div class="rightFloat"> <a href='javascript:void(0);' onclick='allMessageLogs("To");'> Click here to view all messages sent to you</a></div><br class="clearRight" />
				
			<?php } } ?>
        </div>
    </div>
</div>
<br class="clearRight" />
</div>
 <script>//$$('table.ListTable tr:nth-child(odd)').invoke("addClassName", "altRow");</script>