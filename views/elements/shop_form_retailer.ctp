<?php echo $form->create('shop'); ?>
     	<fieldset class="fields1" style="border:0px;margin:0px;">
			<div class="appTitle">New Retailer</div>
				<div>
				<div class="field" style="padding-top:5px;">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="username" class="compulsory">Name</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="1" type="text" id="username" name="data[Retailer][name]"  value="<?php if(isset($data))echo $data['Retailer']['name']; ?>"/>
                         </div>
                 	</div>
                 	<div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="pan" class="compulsory"> PAN Number </label></div>
                         <div class="fieldLabelSpace1">
                         	 <input tabindex="2" type="text" id="pan" name="data[Retailer][pan_number]" value="<?php if(isset($data))echo $data['Retailer']['pan_number']; ?>"/>
                         </div>                    
                 	</div><div class="clearLeft">&nbsp;</div>
            	 </div>
            	 </div>
            	 <div class="altRow">         	 
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="mobile" class="compulsory">Mobile</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="3" type="text" id="mobile" name="data[Retailer][mobile]" value ="<?php if(isset($data))echo $data['Retailer']['mobile']; ?>"/>
                         </div>                     
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="email">E-mail</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="4" type="text" id="email" name="data[Retailer][email]" value="<?php if(isset($data))echo $data['Retailer']['email']; ?>"/>
                         </div>                     
                 	</div>
            	 </div>
            	 </div>
            	 <div>
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                    	<div class="fieldLabel1 leftFloat"><label for="state" class="compulsory"> State </label></div>
                    	<div class="fieldLabelSpace1">
                         <select tabindex="5" id="state" name="data[Retailer][state]" onchange="getCities(this.options[this.selectedIndex].value,'r')" style="width:148px">
							<option value="0">Select State</option> 
							<?php foreach($states as $state) {?>
								<option value="<?php echo $state['locator_state']['id'];?>" <?php if(isset($data) && $data['Retailer']['state'] ==  $state['locator_state']['id']) echo "selected";?>><?php echo $state['locator_state']['name']; ?></option>
							<?php } ?>
						</select>
						</div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                        <div class="fieldLabel1 leftFloat"><label for="city" class="compulsory">City</label></div>
                        <div class="fieldLabelSpace1">
                         <div id="cityDD">
                        <select tabindex="6" id="city" name="data[Retailer][city]" onchange="getAreas(this.options[this.selectedIndex].value,'r')" style="width:148px">
							<option value="0">Select City</option>
							<?php foreach($cities as $city) {?>
								<option value="<?php echo $city['locator_city']['id'];?>" <?php if(isset($data) && $data['Retailer']['city'] ==  $city['locator_city']['id']) echo "selected";?>><?php echo $city['locator_city']['name']; ?></option>
							<?php } ?>
						</select>
						</div>
						</div>                    
                 	</div>
            	 </div>
            	 </div>
            	 <div class="altRow">
              	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="area" class="compulsory"> Area </label></div>
                         <div class="fieldLabelSpace1">
                         <div id="areaDD">
                         	<select tabindex="7" id="area" name="data[Retailer][area_id]" style="width:148px">
								<option value="0">Select Area</option>
								<?php foreach($areas as $area) {?>
									<option value="<?php echo $area['locator_area']['id'];?>" <?php if(isset($data) && $data['Retailer']['area_id'] ==  $area['locator_area']['id']) echo "selected";?>><?php echo $area['locator_area']['name']; ?></option>
								<?php } ?>
							</select>
							</div>
                         </div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="pin" class="compulsory">Pin Code</label></div>
                         <div class="fieldLabelSpace1"">
                         	<input tabindex="8" type="text" id="pin" name="data[Retailer][pin]" value ="<?php if(isset($data))echo $data['Retailer']['pin']; ?>"/>
                         </div>
                    </div>
            	 </div>
            	 </div>
            	 <div>
              	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="shopname" class="compulsory"> Shop Name </label></div>
                         <div class="fieldLabelSpace1">
                         	 <input tabindex="9" type="text" id="shopname" name="data[Retailer][shopname]" value="<?php if(isset($data))echo $data['Retailer']['shopname']; ?>"/>
                         </div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="address" class="compulsory">Address</label></div>
                         <div class="fieldLabelSpace1"">
                            <textarea tabindex="10" id="address" name="data[Retailer][address]" style="width:180px;height:55px;"><?php if(isset($data))echo $data['Retailer']['address']; ?></textarea>
                         </div>
                    </div>
            	 </div>
            	 </div>
            	 <div class="altRow">
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="login" class="compulsory">SMS Login Details</label></div>
                         <div class="fieldLabelSpace1">
                            <input type="checkbox" tabindex="11" id="login" name="data[login]" <?php if(isset($data['login']) && $data['login'] == 'on') echo "checked"; else if(!isset($data['login'])) echo "checked";?>>
                         </div>
                    </div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="slab" class="compulsory">Assign Slab</label></div>
                         <div class="fieldLabelSpace1">
                            <select tabindex="12" id="slab" name="data[Retailer][slab_id]" >
							<?php foreach($slabs as $slab) {?>
								<option value="<?php echo $slab['Slab']['id'];?>" <?php if(isset($data) && $slab['Slab']['id'] == $data['Retailer']['slab_id']) echo "selected";?>><?php echo $slab['Slab']['name']; ?></option>
							<?php } ?>
							</select>
                         </div>
                    </div>
            	 </div>
            	 </div>
                 <div class="field">               		
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat">&nbsp;</div>
                         <div class="fieldLabelSpace1" id="sub_butt">
                         	<?php echo $ajax->submit('Create Retailer', array('id' => 'sub', 'tabindex'=>'13','url'=> array('controller'=>'shops', 'action'=>'createRetailer'), 'class' => 'retailBut enabledBut', 'after' => 'showLoader2("sub_butt");', 'update' => 'innerDiv')); ?>
                         </div>                         
                    </div>
                </div>
                <div class="field">    
                    <div class="fieldDetail">                         
                         <div>
                            <?php echo $this->Session->flash();?>
                         </div>   
                    </div>
            	 </div>	
		</fieldset>
<?php echo $form->end(); ?>
<script>
if($('username'))
	$('username').focus();	
</script>