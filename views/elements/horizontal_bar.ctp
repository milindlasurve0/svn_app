<!-- Two column row two -->
<div class="rowDivider">
	<!-- Left column -->
		<div class="mainColumnLeft">			
			<?php echo $this->element('horizontal_packs');?>
			<div style="background:#EFEFEF ;">
				<div style="border-bottom:1px solid #EC724A;color:#FC4817;font-size:20px;padding:10px 0px 10px 10px;font-weight:normal;">              
      				Our Partners
      			</div>
      			<div style="padding:10px 0px 10px 10px;"><img src="/img/partners.gif"></div>
			</div>
		</div>
	<!-- Left column ends -->
	<!-- Right column -->
		<div class="mainColumnRight">
			<div class="title2 color3 strng">Personalized Alerts</div>
			<div class="title2Sub color2">Choose from the following services & create personal alerts.</div>
			<div class="appMainBox">
            <!-- App box 1 -->
			<?php foreach($allApps as $app) { ?>
			<a href="/apps/view/<?php echo $app['SMSApp']['url'];?>">
	        	
			<div class="appSmallBox adjust appImg<?php echo $app['SMSApp']['id'];?> appSmallBoxie6">
				<div class="leftFloat"><!-- <img src="/img/spacer.gif" height="93px" /> -->&nbsp;</div>
	            <div class="appSmallBoxCont fntSz15 ">
		            <div class="color1 strng"><div class="rightFloat"><?php echo $objGeneral->getPrice($app['SMSApp']['basic_price']) . " " . $app['SMSApp']['price_tag'];?> </div><span class="fntSz17"><?php echo $app['SMSApp']['name'];?></span></div>
                                
					<ul class="color5 ulStyle1">
						<?php $desc = explode("\n",$app['SMSApp']['shortDesc']); 
    							foreach($desc as $post) {
    					?>
        				<li><?php echo $post; ?></li>
    					<?php } ?>					
					</ul>
                    <div class="strng rightFloat css3But2" style="margin-top:-2px;"><?php echo $app['SMSApp']['call_to_action']; ?></div>	
                    <div class="clearRight">&nbsp;</div>		            
	            </div>
	           
	        </div> </a>
	        <?php } ?>
            </div>
            <!-- div style="display: block;">
		      	<div style="position: relative;" class="color4 fntSz19">
		      		<span style="top: 35px;" class="testimonial">"</span>PNR Alert helped me track my PNR and inturn I saved my precious time doing the thesis i am required to. Thanks SMS Tadka for this service.
		      		<span style="bottom: -17px;right:0px;font-family:Georgia; position: absolute;" class="testimonial">"</span>
		      	</div>
		      	<div style="padding-top: 10px; text-align: right;">
		      		<div class="color3 fntSz17 strng leftFloat">
		      			<img width="60px" height="60px" style="float: left; margin-right: 5px;" src="/img/testimonial1.jpg">
		      			<div style="overflow: hidden; float: left;"><div>-&nbsp;Astha&nbsp;Gulatee</div><div>College&nbsp;Student</div></div>
		      			<div class="clearLeft"></div>
		      		</div>
		      		<br class="clearLeft"><br> 
		      	</div>
		    </div -->
		    <div style="display: block;margin-top: 45px;" ><img src="/img/testimonial.gif"/> </div> 
		</div>
	<!-- Right column ends -->
	<div class="clearLeft">&nbsp;</div>
</div>
<!-- Two column row two ends -->
