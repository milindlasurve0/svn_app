<div class="tabs">
  <ul id='navTabs'>
    <?php if($_SESSION['Auth']['User']['group_id'] == RETAILER) {?>
	<li class='<?php if($tob_tab == "home") echo "sel";?>'>
		<a href="/shops/view">Home</a>
	</li>
	<?php } ?>
	<?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR) {?>
	<li class='<?php if($tob_tab == "activity") echo "sel";?>'>
		<a href="/shops/transfer">Activities</a>
	</li>
	<?php } ?>
	<li class='<?php if($tob_tab == "setting") echo "sel";?>'>
		<a href="/shops/changePassword">My Profile</a>
	</li>
	<li class='<?php if($tob_tab == "reports") echo "sel";?>'>
		<a href="/shops/accountHistory">Reports</a>
	</li>
   </ul>
  <div class="clearLeft"></div>
</div>