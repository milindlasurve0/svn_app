<?php echo $form->create('shop'); 

	foreach($editData as $data){
	
?>

     	<fieldset class="fields1" style="border:0px;margin:0px;">
			

<?php if($type == 'r'){ ?>
<input  type="hidden" id="username" name="data[Retailer][id]"  value="<?php if(isset($data))echo $data['Retailer']['id']; ?>"/>
			<div class="appTitle">Edit Retailer<span style="float:right"><a href="/shops/allRetailer"><< back</a></span></div>
				<div>
				<div class="field" style="padding-top:5px;">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="username" class="compulsory">Name</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="1" type="text" id="username" name="data[Retailer][name]"  value="<?php if(isset($data))echo $data['Retailer']['name']; ?>"/>
                         </div>                     
                 	</div>
                 	<div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="pan" class="compulsory"> PAN Number </label></div>
                         <div class="fieldLabelSpace1">
                         	 <input tabindex="2" type="text" id="pan" name="data[Retailer][pan_number]" value="<?php if(isset($data))echo $data['Retailer']['pan_number']; ?>"/>
                         </div>                    
                 	</div>         
            	 </div>
            	 </div>
            	 <div class="altRow">         	 
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="mobile" class="compulsory">Mobile</label></div>
                         <div class="fieldLabelSpace1">
                            <?php echo $data['users']['mobile']; ?>
                            <input tabindex="3" type="hidden" id="mobile" name="data[users][mobile]" value ="<?php if(isset($data))echo $data['users']['mobile']; ?>"/>
                         </div>                     
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="email">E-mail</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="4" type="text" id="email" name="data[Retailer][email]" value="<?php if(isset($data))echo $data['Retailer']['email']; ?>"/>
                         </div>                     
                 	</div>
            	 </div>
            	 </div>
            	 <div>
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                    	<div class="fieldLabel1 leftFloat"><label for="state" class="compulsory"> State </label></div>
                    	<div class="fieldLabelSpace1">
                         <select tabindex="5" id="state" name="data[Retailer][state]" onchange="getCities(this.options[this.selectedIndex].value,'r')" style="width:148px">
                         	<option value="0">Select State</option>
							<?php foreach($states as $state) {?>
								<option value="<?php echo $state['locator_state']['id'];?>" <?php if(isset($data) && $data['Retailer']['state'] ==  $state['locator_state']['name']) echo "selected"; ?>><?php echo $state['locator_state']['name']; ?></option>
							<?php } ?>
						</select>
						</div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                        <div class="fieldLabel1 leftFloat"><label for="city" class="compulsory">City</label></div>
                        <div class="fieldLabelSpace1" id="cityDD">
                        <select tabindex="6" id="city" name="data[Retailer][city]" onchange="getAreas(this.options[this.selectedIndex].value,'r')" style="width:148px">
                        	<option value="0">Select City</option>
							<?php foreach($cities as $city) {?>
								<option value="<?php echo $city['locator_city']['id'];?>" <?php if(isset($data) && $data['Retailer']['city'] ==  $city['locator_city']['name']) echo "selected";  ?>><?php echo $city['locator_city']['name']; ?></option>
							<?php } ?>
						</select>
						</div>                    
                 	</div>
            	 </div>
            	 </div>
            	 <div class="altRow">
              	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="area" class="compulsory"> Area </label></div>
                         <div class="fieldLabelSpace1" id="areaDD">
                         	<select tabindex="7" id="area" name="data[Retailer][area_id]" style="width:148px">
                         		<option value="0">Select Area</option>
								<?php foreach($areas as $area) {?>
									<option value="<?php echo $area['locator_area']['id'];?>" <?php if(isset($data) && $data['Retailer']['area_id'] ==  $area['locator_area']['id']) echo "selected"; ?>><?php echo $area['locator_area']['name']; ?></option>
								<?php } ?>
							</select>
                         </div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="pin" class="compulsory">Pin Code</label></div>
                         <div class="fieldLabelSpace1"">
                         	<input tabindex="8" type="text" id="pin" name="data[Retailer][pin]" value ="<?php if(isset($data))echo $data['Retailer']['pin']; ?>"/>
                         </div>
                    </div>
            	 </div>
            	 </div>
            	 <div>
              	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="shopname" class="compulsory"> Shop Name </label></div>
                         <div class="fieldLabelSpace1">
                         	 <input tabindex="9" type="text" id="shopname" name="data[Retailer][shopname]" value="<?php if(isset($data))echo $data['Retailer']['shopname']; else echo $edata[$modName]['shopname']; ?>"/>
                         </div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="address" class="compulsory">Address</label></div>
                         <div class="fieldLabelSpace1"">
                            <textarea tabindex="10" id="address" name="data[Retailer][address]" style="width:180px;height:55px;"><?php if(isset($data))echo $data['Retailer']['address'];?></textarea>
                         </div>
                    </div>
            	 </div>
            	 </div>
            	 <div class="altRow">
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <!--<div class="fieldLabel1 leftFloat"><label for="login" class="compulsory">SMS Login Details</label></div>
                         <div class="fieldLabelSpace1">
                            <input type="checkbox" tabindex="10" id="login" name="data[login]" <?php if(isset($data['login']) && $data['login'] == 'on') echo "checked";?>>
                         </div>-->
                    </div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="slab" class="compulsory">Assign Slab</label></div>
                         <div class="fieldLabelSpace1">
                            <select tabindex="11" id="slab" name="data[Retailer][slab_id]" >
							<?php foreach($slabs as $slab) {?>
								<option value="<?php echo $slab['Slab']['id'];?>" <?php if(isset($data) && $slab['Slab']['id'] == $data['Retailer']['slab_id']) echo "selected"; ?>><?php echo $slab['Slab']['name']; ?></option>
							<?php } ?>
							</select>
                         </div>
                    </div>
            	 </div>
            	 </div>
                 <div class="field">               		
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat">&nbsp;</div>
                         <div class="fieldLabelSpace1" id="sub_butt">
                         	<?php echo $ajax->submit('Next >>', array('id' => 'sub', 'tabindex'=>'12','url'=> array('controller'=>'shops', 'action'=>'editRetValidation'), 'class' => 'retailBut enabledBut', 'after' => 'showLoader2("sub_butt");', 'update' => 'innerDiv')); ?>
                         </div>                         
                    </div>
                </div>
                <div class="field">    
                    <div class="fieldDetail">
                         
                         <div class="inlineErr1">
                            <?php echo $this->Session->flash();?>
                         </div>   
                    </div>
            	
<?php }else if($type == 'd'){ ?>
<input  type="hidden" id="username" name="data[Distributor][id]"  value="<?php if(isset($data))echo $data['Distributor']['id']; ?>"/>
			<div class="appTitle">Edit Distributor<span style="float:right"><a href="/shops/allRetailer"><< back</a></span></div>
				<div>
				<div class="field" style="padding-top:5px;">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="username" class="compulsory">Name</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="1" type="text" id="username" name="data[Distributor][name]"  value="<?php if(isset($data))echo $data['Distributor']['name']; ?>"/>
                         </div>                     
                 	</div>
                 	<div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="company" class="compulsory">Company Name</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="2" type="text" id="company" name="data[Distributor][company]" value="<?php if(isset($data))echo $data['Distributor']['company']; ?>"/>
                         </div>                     
                 	</div>
            	 </div>
            	 </div>
            	 <div class="altRow">         	 
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="mobile" class="compulsory">Mobile</label></div>
                         <div class="fieldLabelSpace1">
                            <?php echo $data['users']['mobile']; ?>
                            <input tabindex="3" type="hidden" id="mobile" name="data[users][mobile]" value ="<?php if(isset($data))echo $data['users']['mobile']; ?>"/>
                         </div>                     
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="email">E-mail</label></div>
                         <div class="fieldLabelSpace1">
                            <input tabindex="4" type="text" id="email" name="data[Distributor][email]" value="<?php if(isset($data))echo $data['Distributor']['email'];?>"/>
                         </div>                     
                 	</div>
            	 </div>
            	 </div>
            	 <div>
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                    	<div class="fieldLabel1 leftFloat"><label for="state" class="compulsory"> State </label></div>
                    	<div class="fieldLabelSpace1">
                         <select tabindex="5" id="state" name="data[Distributor][state]" onchange="getCities(this.options[this.selectedIndex].value,'d')" style="width:148px">
                         <option value="0">Select State</option> 
							<?php foreach($states as $state) {?>
								<option value="<?php echo $state['locator_state']['id'];?>" <?php if(isset($data) && $data['Distributor']['state'] ==  $state['locator_state']['name']) echo "selected"; ?>><?php echo $state['locator_state']['name']; ?></option>
							<?php } ?>
						</select>
						</div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                        <div class="fieldLabel1 leftFloat"><label for="city" class="compulsory">City</label></div>
                        <div class="fieldLabelSpace1" id="cityDD">
                        <select tabindex="6" id="city" name="data[Distributor][city]" >
                        <option value="0">Select City</option>
							<?php foreach($cities as $city) {?>
								<option value="<?php echo $city['locator_city']['id'];?>" <?php if(isset($data) && strtolower(trim($data['Distributor']['city'])) ==  strtolower(trim($city['locator_city']['name']))){ echo "selected";} ?>><?php echo $city['locator_city']['name']; ?></option>
							<?php } ?>
						</select>
						</div>                    
                 	</div>
            	 </div>
            	 </div>
            	 <div class="altRow">
              	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="area" class="compulsory"> Area Range </label></div>
                         <div class="fieldLabelSpace1">
                         	 <input tabindex="7" type="text" id="area" name="data[Distributor][area_range]" value="<?php if(isset($data))echo $data['Distributor']['area_range'];  ?>"/>
                         </div>                    
                 	</div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="address" class="compulsory">Company Address</label></div>
                         <div class="fieldLabelSpace1"">
                            <textarea tabindex="8" id="address" name="data[Distributor][address]" style="width:180px;height:55px;"><?php if(isset($data))echo $data['Distributor']['address']; ?></textarea>
                         </div>
                    </div>
            	 </div>
            	 </div>
            	 <div>
              	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="pan" class="compulsory"> PAN Number </label></div>
                         <div class="fieldLabelSpace1">
                         	 <input tabindex="9" type="text" id="pan" name="data[Distributor][pan_number]" value="<?php if(isset($data))echo $data['Distributor']['pan_number']; ?>"/>
                         </div>                    
                 	</div>
                 	<div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="slab" class="compulsory">Assign Slab</label></div>
                         <div class="fieldLabelSpace1">
                            <select tabindex="10" id="slab" name="data[Distributor][slab_id]" >
							<?php foreach($slabs as $slab) {?>
								<option value="<?php echo $slab['Slab']['id'];?>" <?php if(isset($data) && $slab['Slab']['id'] == $data['Distributor']['slab_id']) echo "selected";?>><?php echo $slab['Slab']['name']; ?></option>
							<?php } ?>
							</select>
                         </div>
                    </div>
            	 </div>
            	 </div>
            	 <!--<div class="altRow">
            	 <div class="field">
                    <div class="fieldDetail leftFloat" style="width:350px;">
                         <div class="fieldLabel1 leftFloat"><label for="login" class="compulsory">SMS Login Details</label></div>
                         <div class="fieldLabelSpace1">
                            <input type="checkbox" tabindex="9" id="login" name="data[login]" <?php if(isset($data['login']) && $data['login'] == 'on') echo "checked";?>>
                         </div>
                         
                    </div>            	 
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat"><label for="slab" class="compulsory">Assign Slab</label></div>
                         <div class="fieldLabelSpace1">
                            <select tabindex="10" id="slab" name="data[Distributor][slab_id]" >
							<?php foreach($slabs as $slab) {?>
								<option value="<?php echo $slab['Slab']['id'];?>" <?php if(isset($data) && $slab['Slab']['id'] == $data['Distributor']['slab_id']) echo "selected";?>><?php echo $slab['Slab']['name']; ?></option>
							<?php } ?>
							</select>
                         </div>
                    </div>
            	 </div>
            	 </div>-->
                 <div class="field"  style="padding-top:20px">               		
                    <div class="fieldDetail">
                         <div class="fieldLabel1 leftFloat">&nbsp;</div>
                         <div class="fieldLabelSpace1" id="sub_butt">
                         	<?php echo $ajax->submit('Next >>', array('id' => 'sub', 'tabindex'=>'11','url'=> array('controller'=>'shops', 'action'=>'editDistValidation'), 'class' => 'retailBut enabledBut', 'after' => 'showLoader2("sub_butt");', 'update' => 'innerDiv')); ?>
                         </div>                         
                    </div>
                </div>
                <div class="field">    
                    <div class="fieldDetail">
                         <!-- <div class="fieldLabel leftFloat">&nbsp;</div> -->
                         <div class="inlineErr1">
                            <?php echo $this->Session->flash();?>
                         </div>   
                    </div>
<?php } ?>
            	 </div>	
		</fieldset>
<?php } echo $form->end(); ?>

<script>
if($('username'))
	$('username').focus();	
</script>
