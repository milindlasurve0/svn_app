<script>
function addNewCity()
{

var newCity=$('newCity').value;
var stateDD = document.getElementById("state");
var stateValue = stateDD.options[stateDD.selectedIndex].text;

var url = '/users/addNewCity';
	var pars   = "newCity="+newCity+"&stateValue="+stateValue;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						var html = transport.responseText;
						var optn;
						var cityDropDown=$('cityDD');
						optn = document.createElement('OPTION');
						optn.text = newCity;
						optn.value = html;
						cityDropDown.options.add(optn);
						showCityTb();
						$('newCity').value = "";
						
					}
				});
}


function addNewArea()
{

var newArea=$('newArea1').value;
var stateDD = document.getElementById("state");
var stateValue = stateDD.options[stateDD.selectedIndex].text;

var cityDD=document.getElementById("cityDD");
var cityValue = cityDD.options[cityDD.selectedIndex].text;


var url = '/users/addNewArea';
	var pars   = "newArea="+newArea+"&stateValue="+stateValue+"&cityValue="+cityValue;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						var html = transport.responseText;
						var optn;
						var AreaDropDown=$('areaDD');
						optn = document.createElement('OPTION');
						optn.text = newArea;
						optn.value = html;
						AreaDropDown.options.add(optn);
						showAreaTb();
						$('newArea1').value = "";
						
					}
				});


}

function showCityTb()
{
if($("city1").style.display == 'none')
 {
 	$('cityHref').hide();
 	$("city1").show();
 	$("city2").hide();
 }
	else
	{
	$('cityHref').show();
	$("city1").hide();		
	$("city2").show();
	}
}

function showAreaTb()
{
if($("area1").style.display == 'none')
{
	$('areaHref').hide();
 	$("area1").show();
 	$("area2").hide();
 }
	else
	{
	$('areaHref').show();
	$("area1").hide();		
	$("area2").show();
	}
}


</script>




<?php echo $form->create('retailer'); /* print_r($retailerData);*/ ?>
							     	<fieldset class="fields">
										<div class="title3">New Retailer</div>
											<div class="field" style="padding-top:10px;">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="username"> Name </label></div>
							                         <div class="fieldLabelSpace">
							                            <input tabindex="4" type="text" id="username" name="data[Retailer][name]" <?php if(isset($retailerData['0']['retailers']['name'])) { ?> value="<?php echo $retailerData['0']['retailers']['name']; ?>" <?php } ?> />
							                         </div>                     
							                 	</div>
							            	 </div>
							            	 
							            	 <div class="field">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="mobile"> Mobile </label></div>
							                         
							                         <div class="fieldLabelSpace">
							                         	<input tabindex="5" type="text" id="mobile" name="data[Retailer][mobile]" value="<?php if(isset($retailerData['0']['retailers']['mobile'])) echo $retailerData['0']['retailers']['mobile']; else if(isset($user_data['User']['mobile'])) echo $user_data['User']['mobile'];?>">   
							                         </div>                     
							                 	</div>
							            	 </div>
							            	 
							            <div class="field">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="email">E-mail</label></div>
							                         <div class="fieldLabelSpace">
							                            <input tabindex="12" type="text" id="email" name="data[Retailer][email]" <?php if(isset($retailerData['0']['retailers']['email'])) { ?> value="<?php echo $retailerData['0']['retailers']['email']; ?>" <?php } ?> />
							                         </div>
							                    </div>
							            	 </div>
							     						            	
							            	
							            	 <div class="field">
							                    <div class="fieldDetail">
							                    	<div class="fieldLabel leftFloat"><label for="state"> State </label></div>
							                         <select tabindex="7" id="state" name="data[Retailer][state]" onchange="getCities(this.options[this.selectedIndex].value,'r') ">
							                         <option value="0">Select State</option>													
														<?php foreach($states as $state) {?>
														<?php if(isset($state_id) && trim($state_id) == trim($state['locator_state']['id'])) { ?>														
							                         	<option selected="selected" value="<?php echo $state['locator_state']['id'];?>" ><?php echo $state['locator_state']['name']; ?></option>
							                         	<?php }else{ ?>
														<option value="<?php echo $state['locator_state']['id'];?>"><?php echo $state['locator_state']['name']; ?></option>
														<?php 
														      }
														} ?>
													</select>                    
							                 	</div>
							            	 </div>
							            	 
					
							            	
							            	 <div class="field">
							                    <div class="fieldDetail">
							                      <div id="city1" style="display: none">
							                    	 <div class="fieldLabel leftFloat"><label for="city">Add City</label></div>
							                         <div class="fieldLabelSpace">
							                            <input type="text" id="newCity" name="newCity1" value=""/></br>
							                         	<input type="button" name="Add" value="ADD" onclick="addNewCity()" /> <input type="button" name="Back" value="Back" onclick="showCityTb()"/>							                         
							                         </div>
							                      </div>
							                      <div id="city2">
							                      	<div class="fieldLabel leftFloat"><label for="city">City</label></div>
							                         <div class="fieldLabelSpace">
													 
							                            <select tabindex="8" id="cityDD" name="data[Retailer][city]" onchange="getAreas(this.options[this.selectedIndex].value,'r') ">
							                         	<option value="0">Select City</option>
														<?php foreach($cities as $city) {?>
														<?php if(isset($city_id) && trim($city_id) == trim($city['locator_city']['id'])) { ?>														
							                         	<option selected="selected" value="<?php echo $city['locator_city']['id'];?>"><?php echo $city['locator_city']['name']; ?></option>
							                         	<?php }else{ ?>
														
															<option value="<?php echo $city['locator_city']['id'];?>"><?php echo $city['locator_city']['name']; ?></option>
														<?php }} ?>
													</select>
													
													 <div id="cityHref">
							                         <a href="javascript:void(0)" onclick="showCityTb()">Add new City</a>
							                         </div>								                         
							                          
													</div> 
							                         
							                      </div>
							                         
							                         				                              
							                 	</div>
							            	 </div>
							              
							              	<div class="field">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="area"> Area </label></div>
							                         <div class="fieldLabelSpace">
							                         <div id="area1" style="display: none">
							                         <input type="text" id="newArea1" value=""/></br>
							                         <input type="button" name="Add" value="ADD" onclick="addNewArea()"/>   <input type="button" name="Back" value="Back" onclick="showAreaTb()"/>
							                         </div>
							                         <div id="area2">
							                         	<select tabindex="9" id="areaDD" name="data[Retailer][area_id]">
															<option value="0">Select Area</option>
															<?php foreach($areas as $area) {?>
																<?php if(isset($area_id) && trim($area_id) == trim($area['locator_area']['id'])) { ?>														
							                         	<option selected="selected" value="<?php echo $area['locator_area']['id'];?>" ><?php echo $area['locator_area']['name']; ?></option>
							                         	<?php }else{ ?>
													
																<option value="<?php echo $area['locator_area']['id'];?>"><?php echo $area['locator_area']['name']; ?></option>
															<?php }} ?>
														</select>
							                         </div>
							                         <div id="areaHref">
							                         <a href="javascript:void(0)" onclick="showAreaTb()">Add new Area</a></br>
							                         </div>
							                 		                     
							                 	</div>
							                 	
							                 	
							            	 </div>
							            	 </br>
							            	 <div class="field">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="shopname">Shop Name</label></div>
							                         <div class="fieldLabelSpace">
							                            <input tabindex="10" type="text" id="shopname" name="data[Retailer][shopname]" <?php if(isset($retailerData['0']['retailers']['shopname'])) { ?> value="<?php echo $retailerData['0']['retailers']['shopname']; ?>" <?php } ?> />
							                         </div>
							                    </div>
							            	 </div>
							            	 
							                 <div class="field">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="address">Address</label></div>
							                         <div class="fieldLabelSpace">
							                            <textarea tabindex="11" id="address" name="data[Retailer][address]" style="width:215px;height:55px;"><?php if(isset($retailerData['0']['retailers']['address'])) { echo $retailerData['0']['retailers']['address'];  } ?></textarea>
							                         </div>
							                    </div>
							            	 </div>
							            	 
							            	<div class="field">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="pin">Pin</label></div>
							                         <div class="fieldLabelSpace">
							                            <input tabindex="12" type="text" id="pin" name="data[Retailer][pin]" <?php if(isset($retailerData['0']['retailers']['pin'])) { ?> value="<?php echo $retailerData['0']['retailers']['pin']; ?>" <?php } ?> />
							                         </div>
							                    </div>
							            	 </div>  
							            	
							            	 <div class="field">
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat"><label for="retailer">Retailer</label></div>
							                         <div class="fieldLabelSpace">
							                              
							                            <select tabindex="12" id="retailer"  name="data[Retailer][retailer]">
							                            <option value="0">Select Retailer</option>
   															<option value="PLAYWINRETAILER">Playwin Retailer</option>
 							 								<option value="OSSRETAILER">OSS Retailer</option>
						     								<option value="SMARTSHOPRETAILER">SmartShop Retailer</option>
 							 								<option value="OURRETAILER">Our Retailer</option>
															</select> 
							                             
							                         </div>
							                    </div>
							            	 </div>  
							            	 
							        
							                 <div class="field">               		
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat">&nbsp;</div>
							                         <div class="fieldLabelSpace" id="sub_butt">
							                         	<?php echo $ajax->submit('spacer.gif', array('tabindex' => '4','url'=> array('controller'=>'retailers', 'action'=>'add'), 'class' => 'otherSprite oSPos7','after' => 'submitRetailer()', 'update' => 'stupid')); ?> 
                            
							                            <?php //echo $form->submit('spacer.gif', array('id' => 'sub', 'tabindex'=>'13','class' => 'otherSprite oSPos7')); ?>
							                         </div>                         
							                    </div>
							                </div>
							                
							                <div class="field">    
							                    <div class="fieldDetail">
							                         <div class="fieldLabel leftFloat">&nbsp;</div>
							                         <div class="fieldLabelSpace" id="stupid" style="color:#004B91">
							                            <?php //echo $this->Session->flash();?>
							                         </div>   
							                    </div>
							            	 </div>	
									</fieldset>
							<?php echo $form->end(); ?>