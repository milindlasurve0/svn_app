
    <div class="leftFloat dashboardPack">
      <div class="catList">
        <ul id='innerul'>
        	<?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR) { ?>
        	 <li>
        	 <a class="hList" href="javascript:void(0);"><p>
        	 	<?php echo 'Distributor';?>
        	 	</p>
        	 </a>
          	
        	<div class="sublist">
        		<ul>
        			<li name='innerli'>
        				<a href="/shops/transfer" class="<?php if($side_tab == 'transfer') echo 'sel';?>">Balance Transfer</a>
        			</li>
        			<li name='innerli'>
        				<a href="/shops/allotCards" class="<?php if($side_tab == 'allot') echo 'sel';?>">Allot Cards</a>
        			</li>
					<li name='innerli'>
        				<a href="/shops/allRetailer" class="<?php if($side_tab == 'allretailer') echo 'sel';?>">List Distributors</a>
        			</li>
        			<li name='innerli'>
        				<a href="/shops/formDistributor" class="<?php if($side_tab == 'create') echo 'sel';?>">Create Distributor</a>
        			</li>
        			<?php if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR) { ?>
        			<li name='innerli'>
        				<a href="/shops/createCreditDebitNotes" class="<?php if($side_tab == 'createCredit') echo 'sel';?>">Create Credit/Debit Note</a>
        			</li>
        			
        			<li name='innerli'>
        				<a href="/shops/retailerListing" class="<?php if($side_tab == 'retailerList') echo 'sel';?>">Retailers List</a>
        			</li>
        			<?php } ?>
        		</ul>
        	</div>
        	</li>
        	<?php } else if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR) { ?>
        		<li>
        	 <a class="hList" href="javascript:void(0);"><p>
        	 	<?php echo 'Retailer';?>
        	 	</p>
        	 </a>
          	
        	<div class="sublist">
        		<ul>
        			<li name='innerli'>
        				<a href="/shops/transfer" class="<?php if($side_tab == 'transfer') echo 'sel';?>">Balance Transfer</a>
        			</li>
        			<li name='innerli'>
        				<a href="/shops/activateCards" class="<?php if($side_tab == 'activate') echo 'sel';?>">Activate Cards</a>
        			</li>
					<li name='innerli'>
        				<a href="/shops/allRetailer" class="<?php if($side_tab == 'allretailer') echo 'sel';?>">List Retailers</a>
        			</li>
        			<li name='innerli'>
        				<a href="/shops/formRetailer" class="<?php if($side_tab == 'create') echo 'sel';?>">Create Retailer</a>
        			</li>
        		</ul>
        	</div>
        	</li>
        	<?php } ?>
        	
		</ul>
    </div>
  </div>  
  