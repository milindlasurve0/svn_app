<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<title>SMS Tadka</title>
	
<?php echo $minify->css(array('retailer'),RETAIL_STYLE_CSS_VERSION); ?>
</head>
<body>
	<div id="container">
		<div id="content" class="mContainer">		
			<?php echo $content_for_layout; ?>
		</div>
	</div>
</body>
</html>