<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<title>SMS Tadka</title>
<?php echo $minify->css(array('retailer'),RETAIL_STYLE_CSS_VERSION); ?>
<?php echo $minify->css(array('networks'),RETAIL_STYLE_CSS_VERSION); ?>
</head>
<body>
<?php echo $minify->js(array('merge'),MERGE_JS_VERSION); ?>
<?php echo $minify->js(array('merge1'),MERGE_1_JS_VERSION); ?>
<?php echo $minify->js(array('script_app'),SCRIPT_APP_JS_VERSION); ?>
<?php echo $minify->js(array('network'),NETWORK_JS_VERSION); ?>
<?php flush(); ?>
<div class="headerIndex">
	<div class="headerMainCont">
		<div class="headerSpace">
			<div style="float: left; position: relative;" class="logo">
				<?php echo $html->image("logo.png?1234", array("url" => "/networks")); ?>
	        	<span style="" class="slogan fntSz17 strng positionSlogan">"Bring mobile closer to life"</span>
	        </div>
	        <div style="position: relative;">
	        	<div class="rightFloat">
	        		<div class="leftFloat">
	        			<?php if($_SESSION['partnerId'] != ''){ ?>
	        			<div class="headerLinks1">
	        				<div class="globalLinks strng rightFloat">
	        					<ul>
    								<li style="border-right: 0px none; padding-right: 15px ! important; font-weight: normal;">Welcome <?php echo ucfirst($_SESSION['username']); ?>,</li>
    								<!--<li><a alt="Customer Support" href="#">Link1</a></li>
    								<li><a target="_blank" alt="Feedback opens in new window" href="#">Link2</a></li>-->
    								<li style="padding-right: 0px ! important; margin-right: 0px ! important;" class="lastElement"> <a href="/networks/logout">Logout</a></li>
  								</ul>
  								<br class="clearLeft">
	        				</div>
	        				<br class="clearRight">
	        				<div style="text-align: right; float: right; padding-top: 50px;" class="globalLinks strng fntSz17" id="UserBalance">
 Total : (<?php echo $totSub; ?>)  &nbsp;&nbsp;<span style="color: rgb(204, 204, 204); font-weight: normal;">|</span>&nbsp;&nbsp; Active : (<?php echo $totActive; ?>)</div>
	        			</div>
	        			<?php } ?>
	        		</div>
	        	</div>
	        </div>
			<div class="clearBoth">&nbsp;</div>
		</div>
	</div>
</div>
<?php echo $this->element('popup_element');?>
<div class="<?php if($_SESSION['partnerId'] != '') echo 'tabsBgMainNoticeDND'; ?>">
	<div class="mainCont">
		<div class="container">
			<?php  if($_SESSION['partnerId'] != '')echo $this->element('nw_main_nav');?>
			<div style=" position: relative;">
				<div class="loginCont">
					<div>
						<?php  if($_SESSION['partnerId'] != '')echo $this->element('nw_lft_nav');?>
						<div style="float: left;">
							<div style="width: 760px;">
							
							<?php echo $content_for_layout; ?>	
								
							<br class="clearRight">
							</div>
						</div>
						<br class="clearLeft">
					</div>
				</div>
			</div>
			<div>&nbsp;</div>
		</div>
		<div class="footer" id="footer">
   		 	<span class="rightFloat"><a target="_blank" href="http://www.mindsarray.com">About us</a> | <a target="_blank" href="http://blog.smstadka.com">Blog</a> | <a href="/users/dnd">Do Not Disturb Registry</a> | <a alt="Contact Us opens in new window" target="_blank" href="http://blog.smstadka.com/contact-us">Contact Us</a> | <a target="_blank" href="http://blog.smstadka.com/privacy-policy">Privacy Policy</a> | <a alt="Terms of Services" target="_blank" href="http://blog.smstadka.com/terms-and-condition">Terms of Service</a> | <a target="_blank" href="http://blog.smstadka.com/faq">FAQs</a> | <a target="_blank" alt="Feedback opens in new window" href="http://blog.smstadka.com/feedback">Feedback</a><a target="_blank" href="http://www.rapidssl.com/"><img align="absmiddle" class="oSPos30 otherSprite" src="/img/spacer.gif"></a></span>
         	All Rights Reserved &copy; 2011 SMSTadka.com
    	</div>
	</div>
</div>
</body>
<script>
function showLoader3(id){
	$(id).innerHTML = '<span id="loader2" class="loader2" style="display:inline-block; width:50px">&nbsp;</span>';
}
</script>
</html>