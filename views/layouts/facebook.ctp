<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<title>SMS Tadka</title>
<meta name="description" content="<?php echo $message['Message']['content']; ?>" />
  <link type="text/css" rel="stylesheet" href="/css/style.cgz?10">
</head>
<body>
	<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
    <?php 
    	echo $html->script('prototype');
		//echo $html->script('scriptaculous');
		echo $html->script('script');
		echo $html->script('effects');
		echo $html->script('carousel');
		
    ?>
		<script>
			var EACH_MESSAGE_COST = <?php echo EACH_MESSAGE_COST; ?>;
			var DEFAULT_MESSAGE_LENGTH = <?php echo DEFAULT_MESSAGE_LENGTH; ?>;
			var APP_REM_MSG_FIXED = <?php echo APP_REM_MSG_FIXED; ?>;
		</script>
		<div class="headerIndex">
            <div class="headerMainCont">
                <div id="rightHeaderSpace" class="headerSpace">                    
                     <?php if ($this->Session->read('Auth.User')) { echo '<div class="headerLinks1 rightFloat">' . $this->Html->link(__('Logout', true), array('controller' => 'users', 'action' => 'logout')); } else {?>                     	
                    <div class="rightFloat" style="width:420px;">                  
	                    <div class="signup_TL">
                            <div class="signup_TR">
                                <div class="signup_T">&nbsp;</div>                    
                            </div>
                    	</div>
	                    <div class="signup_L">
                            <div class="signup_R">
                                <div class="signup_M">                            	
                                    <div class="signupCont">
	                            		<?php echo $form->create('User', array('controller' => 'users','action' => 'login')); ?> 
	                                    <div class="leftFloat" style="padding-right:10px">
	                                        <div><label for="email">Mobile</label></div>
	                                        <div style="padding-top:5px;"><input type="text" id="mobile" tabindex="1" style="width:143px" name="data[User][mobile]"/></div>
	                                        <div class="signup_link1 lightText i6space" ><input type="checkbox" tabindex="3" /> Keep me logged in</div>
	                                    </div>
	                                    <div class="leftFloat" style="padding-right:10px">
	                                    	<?php //echo "<pre>";print_r($_SESSION); echo "</pre>";?>
	                                        <div><label for="label">Password</label></div>
	                                        <div style="padding-top:5px;"><input type="password" id="password" tabindex="2" name="data[User][password]" style="width:143px" autocomplete="off"/></div>
	                                        <div class="lightText signup_link2"><a href="javascript:void(0);" onclick="forgetPassword();">Forgot password?</a></div>
	                                    </div>
	                                    <div class="leftFloat">
	                                        <div>&nbsp;</div>
	                                        <div style="padding-top:5px;">
	                                        	<input type="image" value="Submit" src="/img/butLogin.png" onclick="return mobileValidate(document.getElementById('mobile').value);"/>
	                                      	
	                                        </div>
	                                    </div><br class="clearLeft" />
	                   					<?php echo $form->end(); ?>
	                                </div>
	                            </div>                    
	                    	</div>
	                    </div>
	                    <div class="signup_BL">
                            <div class="signup_BR">
                                <div class="signup_B">&nbsp;</div>
                            </div>
                        </div>
	                    <?php } ?>
                    </div>               
                    <div class="logo">
                    <?php echo $html->image("logo.png?211", array("url" => "/users/view/")); ?>
                    </div>
                    <br class="clearRight" />
                </div>
            </div>
    	</div>
    	
<div id="container" class="mainCont">	
		<div id="content" class="container">
			<?php echo $this->element('popup_element');?>
			<?php //echo $this->Session->flash('auth'); ?>
			<?php //echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>

		</div>
		
       
			<div class="category">
            <div class="footerDivider" >&nbsp;</div>
			<?php foreach($catData as $category) {?>	
		      <div class="leftFloat box1">
		        <div class="header">
		        	<?php echo $this->Html->link(__($category['Category']['name'], true), array('controller' => 'categories','action' => 'view',$objGeneral->nameToUrl($category['Category']['name']))) ?>
		        </div>
		        <div class="cont">
		          <ul>
		        <?php $i = 0; while(!empty($category['Category'][$i])) { if($i < 5) { ?>  
		            <li>
		            	<?php  echo $this->Html->link(__($category['Category'][$i]['name'], true), array('controller' => 'categories','action' => 'view',$objGeneral->nameToUrl($category['Category']['name']), $objGeneral->nameToUrl($category['Category'][$i]['name']))) ?>
		            </li>
		    	<?php
				} $i++; }
				if($i < 5)
				{
					while($i<5)
					{
				 ?> 
                 	<li>&nbsp;</li> 
                  <?php
				  $i++;
				  }
				 }
				  ?>      	            
		          </ul>
		          <div class="moreLink">
		          	<?php echo $this->Html->link(__('more ..', true), array('controller' => 'categories','action' => 'view',$objGeneral->nameToUrl($category['Category']['name']))) ?>
		          </div>
		        </div>
		        <div class="box1Footer"></div>
		      </div>
		      <?php } ?>
		      <br class="clearLeft" />
		    </div>
	    			
		<div id="footer" class="footer">
			<span class="rightFloat"><a href="http://www.mindsarray.com" target="_blank">About us</a> | <a href="http://blog.smstadka.com" target="_blank">Blog</a> | <a href="/users/dnd">Do Not Disturb Registry</a> | <a href="http://blog.smstadka.com/contact-us" target="_blank" alt="Contact Us opens in new window">Contact Us</a> | <a href="http://blog.smstadka.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://blog.smstadka.com/terms-and-condition" target="_blank" alt="Terms of Services">Terms of Service</a> | <a href="http://blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="http://blog.smstadka.com/feedback" alt="Feedback opens in new window" target="_blank">Feedback</a><a href="http://www.rapidssl.com/" target="_blank"><img src="/img/spacer.gif" class="oSPos30 otherSprite" align="absmiddle"></a></span>
			All Rights Reserved © 6 Dev Solution
    <!-- <span class="rightFloat"><a href="http://blog.smstadka.com/contact-us" target="_blank">Contact Us</a> | <a href="#">Privacy Policy</a> | <a href="http://blog.smstadka.com/terms-and-condition" target="_blank">Terms of Service</a> | <a href="http://blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="#">Feedback</a></span></div> -->		
		</div>
	<?php echo $this->element('sql_dump'); ?>
	<script> if($('mobile'))$('mobile').focus();</script>
</body>
</html>