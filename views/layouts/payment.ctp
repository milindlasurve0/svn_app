<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<?php if(($this->params['controller'] == 'messages' && $this->params['action'] == 'facebook')){ ?>
<title><?php echo $message['Message']['title']; ?></title>
<meta name="description" content="<?php echo htmlspecialchars($messageContent); ?>" />
<?php }else if(isset($pageTitle)){ 
$pageNum = '';
if(isset($this->params['paging']['Message'])) {  $pageNum = " | " . $this->params['paging']['Message']['page']; }
if(isset($this->params['paging']['MessagesTag'])) { $pageNum = " | " . $this->params['paging']['MessagesTag']['page']; }
?>
<title><?php echo $pageTitle . $pageNum;?></title>
<meta name="description" content="<?php echo htmlspecialchars($pageDesc) . $pageNum; ?>" />
<?php } else {?>
<title>SMS Alerts for India | Stock Market SMS Updates | Bollywood SMS News | Best SMS Jokes | Cricket Live Score on SMS | News Headlines on SMS | Twitter on SMS | Cooking tips on SMS</title>
<meta name="description" content="SMSTadka.com is a SMS service for Indian Mobile users. Choose your favorite SMS Monthly Package on Astrology, Bollywood, Cricket Live Score, News Headlines, Funny SMS Jokes, Stock Market SMS Alerts, Health Tips, Cooking Tips and a lot more.<br>
Subscribe to the India's Best SMS Content Portal." />
<?php } ?>

<link type="text/css" rel="stylesheet" href="<?php echo SITE_NAME_SEC; ?>css/style.<?php echo CSS_TYPE; ?>?<?php echo STYLE_CSS_VERSION;?>">
<!--[if gt IE 6]>
	<link type="text/css" rel="stylesheet" href="<?php echo SITE_NAME_SEC; ?>css/style_ie.<?php echo CSS_TYPE; ?>?<?php echo STYLE_CSS_IE_VERSION;?>">   
<![endif]-->
<!--[if lt IE 7]>
	<link type="text/css" rel="stylesheet" href="<?php echo SITE_NAME_SEC; ?>css/style_ie6.<?php echo CSS_TYPE; ?>?<?php echo STYLE_CSS_IE_VERSION;?>">   
<![endif]-->

<?php if($_SERVER['SERVER_NAME'] == 'www.smstadka.com')	{ ?>
	<meta name="google-site-verification" content="wNAuDSOQ7yd9aVA3OGicamOQs3DV1hP293QMkDddDqs" />
<?php } ?>
</head>
<?php flush(); ?> 
<body>
<style type="text/css">
.taggLinkBG1 {background-color: #FF8800 !important;}
li.selected    { background-color: #ffb; }
</style>
<script src="<?php echo SITE_NAME_SEC; ?>js/merge.<?php echo JS_TYPE; ?>?<?php echo SCRIPT_APP_JS_VERSION; ?>" type="text/javascript"></script>
<script src="<?php echo SITE_NAME_SEC; ?>js/script_app.<?php echo JS_TYPE; ?>?<?php echo SCRIPT_APP_JS_VERSION; ?>" type="text/javascript"></script>
<?php flush(); ?>
<div class="headerIndex">
    <div class="headerMainCont">
        <div class="headerSpace">
        	
            <div class="logo" style="float:left;position:relative;">
            	<?php if(!isset($_SESSION['Auth']['User']['mobile']) || ($_SESSION['Auth']['User']['mobile'] != '9892609560' && $_SESSION['Auth']['User']['mobile'] != '9819852204') ) echo $html->image("logo.png?211", array("url" => SITE_NAME."users/view")); ?>
            	<span class="slogan fntSz17 strng positionSlogan" style="">"Bring mobile closer to life"</span>
            </div>
            <div id="rightHeaderSpace" style="position:relative">                    
             <?php  echo $this->element('right_header_sec'); ?>
         	</div>
            <div class="clearBoth">&nbsp;</div>
    	</div>
	</div>
</div>    
<?php flush(); ?>    	
<div id="tabsBgMain" class="tabsBgMain">

			
		
<div id="container" class="mainCont">
	<div id="login_user" style="display:none"> <?php echo $this->element('login_sessionOut');?> </div>
		
	<div id="content" class="container">
		<?php echo $content_for_layout; ?>
	</div>
		
   <?php flush(); ?>			    			
	<div id="footer" class="footer">
	 	<span class="rightFloat"><a href="http://www.mindsarray.com" target="_blank">About us</a> | <a href="http://blog.smstadka.com" target="_blank">Blog</a> | <a href="<?php echo SITE_NAME; ?>users/dnd">Do Not Disturb Registry</a> | <a href="http://blog.smstadka.com/contact-us" target="_blank" alt="Contact Us opens in new window">Contact Us</a> | <a href="http://blog.smstadka.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://blog.smstadka.com/terms-and-condition" target="_blank" alt="Terms of Services">Terms of Service</a> | <a href="http://blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="http://blog.smstadka.com/feedback" alt="Feedback opens in new window" target="_blank">Feedback</a><a href="http://www.rapidssl.com/" target="_blank"><img src="/img/spacer.gif" class="oSPos30 otherSprite" align="absmiddle"></a></span>
     	All Rights Reserved © <?php echo date('Y'); ?> SMSTadka.com
	</div>	
</div>
<?php if($_SERVER['REQUEST_URI'] == "/users/view" || $_SERVER['REQUEST_URI'] == "/users/view/") echo "</div>";?>
<?php flush(); ?>
	<script src="<?php echo SITE_NAME_SEC; ?>js/merge1.<?php echo JS_TYPE; ?>?<?php echo SCRIPT_APP_JS_VERSION; ?>" type="text/javascript"></script>	
	 <?php if($_SERVER['SERVER_NAME'] == 'www.smstadka.com' && !(isset($_SESSION['Auth']['User']) && $_SESSION['Auth']['User']['group_id'] == 2)){ ?>  
	  <script type="text/javascript">	
	  var _gaq = _gaq || [];
	  _gaq.push(['pageTracker._setAccount', 'UA-18505922-1']);
	  _gaq.push(['pageTracker._trackPageview']);
	         
	  (function() { 
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();	
	</script>
	<script type="text/javascript">
		window.onload=function() {
		var plend = new Date();
		var plload = plend.getTime() - plstart.getTime();
		if(plload<1000)
		lc = "Very Fast";
		else if (plload<2000)
		lc = "Fast";
		else if (plload<3000)
		lc = "Medium";
		else if (plload<5000)
		lc = "Sluggish";
		else if (plload<10000)
		lc = "Slow";
		else
		lc="Very Slow";
		var fn = document.location.pathname;
		if( document.location.search)
		fn += document.location.search;
		try {
		_gaq.push(['loadTracker._setAccount', 'UA-18505922-2']);
		_gaq.push(['loadTracker._trackEvent','Page Load (ms)',lc + ' Loading Pages',fn,plload]);
		_gaq.push(['loadTracker._trackPageview']);
		} catch(err){}
		}
	</script>
	
	<!-- clickheat code starts -->
	<?php
		$pageUrl='';
		$params=$this->params;		
		$pageWeSee=$params['controller'];
		$pageWeSee.='/'.$params['action'];
		$ps='';
		foreach( $params['pass'] as $p){
			$ps.='/'.$p;
		}
		$pageUrl=$pageWeSee.$ps;
	?>
	<script type="text/javascript" src="http://www.smstadka.com/clickheat/js/clickheat.js"></script><noscript><p><a href="http://www.labsmedia.com/index.html">Free marketing tools</a></p></noscript><script type="text/javascript"><!--
clickHeatSite = 'www.smstadka.com';clickHeatGroup = '<?php echo $pageUrl; ?>';clickHeatServer = 'http://www.smstadka.com/clickheat/click.php';initClickHeat(); //-->
</script>
	<!-- clickheat code ends -->
   <?php } ?>
</body>
</html>