<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<?php if($this->params['controller'] == 'messages' && ($this->params['action'] == 'socialshare' || $this->params['action'] == 'facebook')){ ?>
<?php if($refCodeShare && $this->params['action'] == 'socialshare'){ ?>
<title><?php echo $refCodeTitle; ?></title>
<meta name="description" content="<?php echo $refCodeDesc; ?>" />
<?php }else if($this->params['action'] == 'facebook'){ ?>
<title><?php echo $message['Message']['title']; ?></title>
<meta name="description" content="<?php echo htmlspecialchars($messageContent); ?>" />
<?php } ?>

<?php }else if(isset($pageTitle)){ 
$pageNum = '';
if(isset($this->params['paging']['Message'])) {  $pageNum = " | " . $this->params['paging']['Message']['page']; }
if(isset($this->params['paging']['MessagesTag'])) { $pageNum = " | " . $this->params['paging']['MessagesTag']['page']; }
?>
<title><?php echo $pageTitle . $pageNum;?></title>
<meta name="description" content="<?php echo htmlspecialchars($pageDesc) . $pageNum; ?>" />
<?php } else {?>
<title>SMS Alerts for India | Stock Market SMS Updates | Bollywood SMS News | Best SMS Jokes | Cricket Live Score on SMS | News Headlines on SMS | Twitter on SMS | Cooking tips on SMS</title>
<meta name="description" content="SMSTadka.com is a SMS service for Indian Mobile users. Choose your favorite SMS Monthly Package on Astrology, Bollywood, Cricket Live Score, News Headlines, Funny SMS Jokes, Stock Market SMS Alerts, Health Tips, Cooking Tips and a lot more.<br>Subscribe to the India's Best SMS Content Portal." />
<?php } ?>
   <?php 
    	//echo $html->css('style');
   		//echo $html->script('dpEncodeRequest');
    ?>
    <script type="text/javascript">
		var plstart = new Date();
	</script>
	<?php echo $minify->css(array('style'),STYLE_CSS_VERSION); ?>
   <!-- <link type="text/css" rel="stylesheet" href="/css/style.<?php echo CSS_TYPE; ?>?<?php echo STYLE_CSS_VERSION;?>"> -->
   <!--[if gt IE 6]>
   		<?php echo $minify->css(array('style_ie'),STYLE_CSS_IE_VERSION); ?>
   <![endif]-->
   <!--[if lt IE 7]>
   		<?php echo $minify->css(array('style_ie6'),STYLE_CSS_IE_VERSION); ?>
	<![endif]-->
   <?php
	//---------------------------------
	// doConditionalGet function definition
	//---------------------------------
	function doConditionalGet($timestamp) {
	  // Any method for creating ETag is ok,
	  //  but should be unique
	  $etag = '"'.md5($timestamp).'"';
	
	  // Send the headers
	  header("Last-Modified: $timestamp");
	  header("ETag: $etag");
	
	  // If browser provided IMS and ETag, get them
	  // Otherwise, set variable to 'false'
	  $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ?
	      stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;
	  $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ?
	      stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) : false;
	  if (!$if_modified_since && !$if_none_match) {
	      return; }
	
	  // If at least one value exists, validate them
	  if ($if_none_match && $if_none_match != $etag) {
	      return; // ETag exists but doesn't match
		}
	
	  if ($if_modified_since && $if_modified_since != $timestamp) {
	      return; // IMS exists but doesn't match
		}
	
	  // Nothing has changed since last request
	  // Return a 304 and do NOT run the rest of page
	  header('HTTP/1.1 304 Not Modified');
	  exit;
	}

	//--------------------------------------------------
	// Call doConditionalGet function with hard-coded LM date
	// Format is the web logging standard
	//--------------------------------------------------
	//echo $this->params['controller']."===".$this->params['action'];
	if(isset($_SESSION['Auth']['User']) || ($this->params['controller'] == 'packages' &&  $this->params['action'] == 'view')){
	}else{ 
	//doConditionalGet('Wed, 15 Aug 2010 01:00:00 GMT');
	}
	?>
<?php if($_SERVER['SERVER_NAME'] == 'www.smstadka.com')	{ ?>
<meta name="google-site-verification" content="wNAuDSOQ7yd9aVA3OGicamOQs3DV1hP293QMkDddDqs" />
<?php } ?>
</head>
<?php flush(); ?> 
<body>

<style type="text/css">
.taggLinkBG1 {background-color: #FF8800 !important;}
li.selected    { background-color: #ffb; }
</style>
	<?php echo $minify->js(array('merge'),MERGE_JS_VERSION); ?>
    <?php 
    	
   		//echo $html->script('scriptaculous');
   		//echo $html->script('controls');	
    ?>
    <?php echo $minify->js(array('script_app'),SCRIPT_APP_JS_VERSION); ?>
   <!-- <script src="/js/script_app.<?php echo JS_TYPE; ?>?<?php echo SCRIPT_APP_JS_VERSION; ?>" type="text/javascript"></script> -->
    <?php flush(); ?>
		<script>
			var EACH_MESSAGE_COST = <?php echo EACH_MESSAGE_COST; ?>;
			var DEFAULT_MESSAGE_LENGTH = <?php echo DEFAULT_MESSAGE_LENGTH; ?>;
			var APP_REM_MSG_FIXED = <?php echo APP_REM_MSG_FIXED; ?>;
			var ADSPACE = <?php echo ADSPACE; ?>;
			var APP_REM_MSG_LMT = <?php echo APP_REM_MSG_LMT; ?>;
			var DND_FLAG = <?php echo DND_FLAG; ?>;
			
		</script>
		<div class="headerIndex">
            <div class="headerMainCont">
                <div class="headerSpace">
                	
	                <div class="logo" style="float:left;position:relative;">
	                	<?php if(!isset($_SESSION['Auth']['User']['mobile']) || ($_SESSION['Auth']['User']['mobile'] != '9892609560' && $_SESSION['Auth']['User']['mobile'] != '9819852204') ) echo $html->image("logo.png?1234", array("url" => "/users/view")); ?>
	                	<span class="slogan fntSz17 strng positionSlogan" style="">"Bring mobile closer to life"</span>
	                </div>
                    <div id="rightHeaderSpace" style="position:relative">                    
                     <?php  echo $this->element('right_header'); ?>
	             	</div>
	                <div class="clearBoth">&nbsp;</div>
            	</div>
    		</div>
    	</div>
    		
    	<?php flush(); ?>
    	
		<?php if((isset($_SESSION['Auth']['User']['passflag']) && $_SESSION['Auth']['User']['passflag'] == '0') && ($_SERVER['REQUEST_URI'] == "/users/view" || $_SERVER['REQUEST_URI'] == "/users/view/")  )   {?>
			<div id="tabsBgMainNotice" class="<?php if(DND_FLAG == 1 && !TRANS_FLAG) { echo 'tabsBgMainNoticeDND'; } else { echo 'tabsBgMainNotice'; } ?>">
		<?php } else if($_SERVER['REQUEST_URI'] == "/users/view" || $_SERVER['REQUEST_URI'] == "/users/view/") { ?>
    	<div id="tabsBgMain" class="<?php if(DND_FLAG == 1 && !TRANS_FLAG) { echo 'tabsBgMainNoticeDND'; } else { echo 'tabsBgMainNotice'; } ?>">
		<?php } ?>
		
		 <?php if(isset($_SESSION['Auth']) && $_SESSION['Auth']['User'] && $this->params['controller'] != 'categories' && $this->params['action'] != 'view'){ ?>	
		<div class="tabsBgMainNotice">
		<?php } ?>
		<div id="container" class="mainCont">
			<?php // TRAI Purpose if(isset($_SESSION['Auth']['User']['passflag']) && $_SESSION['Auth']['User']['passflag'] == '0') {?>
			<?php if(isset($_SESSION['Auth']['User'])) {?>
			<div id="notice" style="<?php if(DND_FLAG && !TRANS_FLAG) echo 'height:30px';?>">
    			<!-- Commented for TRAI Purpose -->
    			<?php if(DND_FLAG && !TRANS_FLAG) { ?>
                <div class="notification" style="position:relative"><!--<img src="/img/spacer.gif" class="nNotice" /> You seem to be using system generated password. <a href='javascript:void(0)' onclick='itsfine();'>It's fine.</a> OR <a href='/users/userPasswordChange'>Click here to change your password</a>--> 
                
                <?php if($_SESSION['Auth']['User']['dnd_flag'] == 1 && !TRANS_FLAG) { ?>
&nbsp;Due to <a href="http://nccptrai.gov.in/nccpregistry/" target="_blank">new TRAI guidelines</a>, DND numbers will not receive commercial messages. To receive alerts from SMSTadka.com, you must de-register by calling 1909. <a target="_blank" href="http://nccptrai.gov.in/nccpregistry/">Read More</a><br/>
&nbsp;You can also get your sms alerts on your email-id. Just set your email-id in "My Account -> Personal Details" section. For any help, send HELP as sms to 09223178889
				<?php } else { ?>
                &nbsp;Telecom regulator TRAI has enforced new SMS regulations from Sept 27. <br/>&nbsp;You will now send & receive messages only between 9 AM to 8:30 PM from a numeric sender-id <a target="_blank" href="http://nccptrai.gov.in/nccpregistry/">Read More</a>
                <?php } ?>
                
                </div>
                <?php } ?>			
    		</div>
    		<?php  } ?>
    		<?php echo $this->element('popup_element');?>
			<?php echo $this->element('auto_close');?>
			<div id="login_user" style="display:none"> <?php echo $this->element('login_sessionOut');?> </div>
				
			<div id="content" class="container">
				<?php echo $content_for_layout; ?>
			</div>
		
		<?php echo $this->element('categoryEle'); ?>
		<div id="footer" class="footer">
   		 	<span class="rightFloat"><a href="http://www.mindsarray.com" target="_blank">About us</a> | <a href="http://blog.smstadka.com" target="_blank">Blog</a> | <a href="/users/dnd">Do Not Disturb Registry</a> | <a href="http://blog.smstadka.com/contact-us" target="_blank" alt="Contact Us opens in new window">Contact Us</a> | <a href="http://blog.smstadka.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://blog.smstadka.com/terms-and-condition" target="_blank" alt="Terms of Services">Terms of Service</a> | <a href="http://blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="http://blog.smstadka.com/feedback" alt="Feedback opens in new window" target="_blank">Feedback</a><a href="http://www.rapidssl.com/" target="_blank"><img src="/img/spacer.gif" class="oSPos30 otherSprite" align="absmiddle"></a></span>
         	All Rights Reserved © <?php echo date('Y'); ?> SMSTadka.com
    	</div>	
	</div>
	<?php if(isset($_SESSION['Auth']['User'])){?>	
	</div>
	<?php } ?>
<?php if($_SERVER['REQUEST_URI'] == "/users/view" || $_SERVER['REQUEST_URI'] == "/users/view/") echo "</div>";?>
	<?php //echo $this->element('sql_dump'); ?>
	<?php flush(); ?>	
	<script> if($('mobile'))$('mobile').focus();</script>
	<script type="text/javascript">
		var APP_STOCK_PRICE = <?php echo APP_STOCK_PRICE; ?>;
		var APP_STOCK_PRICE_WITH_NEWS = <?php echo APP_STOCK_PRICE_WITH_NEWS; ?>;
	</script>
	<?php echo $minify->js(array('merge1'),MERGE_1_JS_VERSION); ?>
	 <?php if($_SERVER['SERVER_NAME'] == 'www.smstadka.com' && !(isset($_SESSION['Auth']['User']) && $_SESSION['Auth']['User']['group_id'] == 2)){ ?>  
	  <script type="text/javascript">	
	  var _gaq = _gaq || [];
	  _gaq.push(['pageTracker._setAccount', 'UA-18505922-1']);
	  _gaq.push(['pageTracker._trackPageview']);
	         
	  (function() { 
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();	
	</script>
	<script type="text/javascript">
		window.onload=function() {
		var plend = new Date();
		var plload = plend.getTime() - plstart.getTime();
		if(plload<1000)
		lc = "Very Fast";
		else if (plload<2000)
		lc = "Fast";
		else if (plload<3000)
		lc = "Medium";
		else if (plload<5000)
		lc = "Sluggish";
		else if (plload<10000)
		lc = "Slow";
		else
		lc="Very Slow";
		var fn = document.location.pathname;
		if( document.location.search)
		fn += document.location.search;
		try {
		_gaq.push(['loadTracker._setAccount', 'UA-18505922-2']);
		_gaq.push(['loadTracker._trackEvent','Page Load (ms)',lc + ' Loading Pages',fn,plload]);
		_gaq.push(['loadTracker._trackPageview']);
		} catch(err){}
		}
	</script>
	
	<!-- clickheat code starts -->
	<?php /*
		$pageUrl='';
		$params=$this->params;		
		$pageWeSee=$params['controller'];
		$pageWeSee.='/'.$params['action'];
		$ps='';
		foreach( $params['pass'] as $p){
			$ps.='/'.$p;
		}
		$pageUrl=$pageWeSee.$ps;*/
	?>
	<!-- <script type="text/javascript" src="http://www.smstadka.com/clickheat/js/clickheat.js"></script><noscript><p><a href="http://www.labsmedia.com/index.html">Free marketing tools</a></p></noscript><script type="text/javascript">
clickHeatSite = 'www.smstadka.com';clickHeatGroup = '<?php //echo $pageUrl; ?>';clickHeatServer = 'http://www.smstadka.com/clickheat/click.php';initClickHeat();
</script> -->
	<!-- clickheat code ends -->
   <?php } ?>
   
   <?php if(($this->params['controller'] == 'categories' && $this->params['action'] == 'view') || ($this->params['controller'] == 'messages' && $this->params['action'] == 'view')  || ($this->params['controller'] == 'tags' && $this->params['action'] == 'view') || ($this->params['controller'] == 'messages' && $this->params['action'] == 'facebook')){ ?>
	<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
	<?php } ?>	
</body>
</html>