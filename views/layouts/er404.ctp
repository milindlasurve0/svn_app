<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<title>SMS Tadka</title>
 <?php echo $minify->css(array('style'),STYLE_CSS_VERSION); ?>
 <!-- <link type="text/css" rel="stylesheet" href="/css/style.<?php echo CSS_TYPE; ?>?<?php echo STYLE_CSS_VERSION;?>"> -->
</head>
<body>
<table  border="0" align="center">
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td valign="top">
		<div class="logo" style="float:left;">
	    	<?php echo $html->image("logo.png?211", array("url" => "/users/view/","height"=>"60px")); ?>
	    </div>
	</td>
	<td width="80%"><?php echo $content_for_layout; ?></td>
</tr>
<tr>
	<td colspan="2">
	 <div id="footer" class="footer">
   		 	<span class="rightFloat"><a href="http://www.mindsarray.com" target="_blank">About us</a> | <a href="http://blog.smstadka.com" target="_blank">Blog</a> | <a href="/users/dnd">Do Not Disturb Registry</a> | <a href="http://blog.smstadka.com/contact-us" target="_blank" alt="Contact Us opens in new window">Contact Us</a> | <a href="http://blog.smstadka.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://blog.smstadka.com/terms-and-condition" target="_blank" alt="Terms of Services">Terms of Service</a> | <a href="http://blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="http://blog.smstadka.com/feedback" alt="Feedback opens in new window" target="_blank">Feedback</a><a href="http://www.rapidssl.com/" target="_blank"><img src="/img/spacer.gif" class="oSPos30 otherSprite" align="absmiddle"></a></span>
         	All Rights Reserved © <?php echo date('Y'); ?> SMSTadka.com
    	</div>
	</td>	
</tr>
</table>

 
	
</body>
</html>