<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<title>SMS Tadka</title>
	
<link type="text/css" rel="stylesheet" href="<?php echo SERVER_BACKUP; ?>css/m_style.css?<?php echo M_STYLE_CSS_VERSION;?>">
</head>
<body>

	<div class="mHeader">
        <div class="mLogo">
            <?php echo $html->image("mLogo.png?123", array("url" => "/")); ?>
        </div>
    </div>
	<div id="container">
		<div id="content" class="mContainer">		
			<?php echo $content_for_layout; ?>
		</div>
				    			
		<div id="footer">
         	<a href="<?php echo SITE_NAME; ?>">www.SMSTadka.com</a>
    	</div>
	</div>
	<script type="text/javascript">	
	  var _gaq = _gaq || [];
	  _gaq.push(['pageTracker._setAccount', 'UA-18505922-1']);
	  _gaq.push(['pageTracker._trackPageview']);
	         
	  (function() { 
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();	
	</script>
</body>
</html>