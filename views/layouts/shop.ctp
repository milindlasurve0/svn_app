<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico">
<title>SMS Tadka</title>

<?php echo $minify->css(array('retailer'),RETAIL_STYLE_CSS_VERSION); ?>
<!--[if gt IE 6]>
	<?php echo $minify->css(array('style_ie'),STYLE_CSS_IE_VERSION); ?>
<![endif]-->
<!--[if lt IE 7]>
	<?php echo $minify->css(array('style_ie6'),STYLE_CSS_IE_VERSION); ?>
<![endif]-->
   
</head>
<body>

<style type="text/css">
.taggLinkBG1 {background-color: #FF8800 !important;}
.success {
background: #FFCC00;
padding: 2px 5px;
}
.error{
font-size: 1em;
background: red;
padding: 2px 5px;
color: white;
}
</style>
<?php echo $minify->js(array('merge'),MERGE_JS_VERSION); ?>
<?php echo $minify->js(array('script_app'),SCRIPT_APP_JS_VERSION); ?>
    <?php flush(); ?>
		<script>
			var PNR_PRODUCT = <?php echo PNR_PRODUCT; ?>;
			var PNR_HAPPY_JOURNEY = <?php echo PNR_HAPPY_JOURNEY; ?>;
			var PNR_SIGNATURE = <?php echo PNR_SIGNATURE; ?>;
			var RECHARGE_CARD_50 = <?php echo RECHARGE_CARD_50; ?>;
		</script>
		<div class="headerIndex">
            <div class="headerMainCont">
                <div class="headerSpace">                	
	                <div class="logo" style="float:left;position:relative;">
	                	<?php if($_SESSION['Auth']['User']['group_id'] != ADMIN) echo $html->image("logo.png?211", array("url" => SITE_NAME."shops/view/")); 
	                	else echo $html->image("logo.png?211", array("url" => SITE_NAME."users/view/"));
	                	?>
	                	
	                	<span class="slogan fntSz17 strng positionSlogan" style="">"Bring mobile closer to life"</span>
	                </div>
                    <div id="rightHeaderSpace" style="position:relative">                    
                     	<?php  if($_SESSION['Auth']['User']['group_id'] != ADMIN)echo $this->element('shop_header'); ?>
	             	</div>
	                <div class="clearBoth">&nbsp;</div>
            	</div>
    		</div>
    	</div>
    	
		<div id="container" class="mainCont">
		<?php echo $this->element('popup_element');?>
		<div id="login_user" style="display:none"> <?php echo $this->element('login_sessionOut');?> </div>
		<div id="content" class="container">				
				<?php echo $content_for_layout; ?>	
			</div>				    			
		<div id="footer" class="footer">
   		 	<span class="rightFloat"><a href="http://www.mindsarray.com" target="_blank">About us</a> | <a href="http://blog.smstadka.com" target="_blank">Blog</a> | <a href="<?php echo SITE_NAME; ?>users/dnd">Do Not Disturb Registry</a> | <a href="http://blog.smstadka.com/contact-us" target="_blank" alt="Contact Us opens in new window">Contact Us</a> | <a href="http://blog.smstadka.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://blog.smstadka.com/terms-and-condition" target="_blank" alt="Terms of Services">Terms of Service</a> | <a href="http://blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="http://blog.smstadka.com/feedback" alt="Feedback opens in new window" target="_blank">Feedback</a><a href="http://www.rapidssl.com/" target="_blank"><img src="/img/spacer.gif"  class="oSPos30 otherSprite" align="absmiddle"></a></span>
         	All Rights Reserved © <?php echo date('Y'); ?> SMSTadka.com
    	</div>
		
</div>
	<?php echo $this->element('sql_dump'); ?>
	<?php echo $minify->js(array('merge1'),MERGE_1_JS_VERSION); ?>
</body>
<script>
function showLoader3(id){
	$(id).innerHTML = '<span id="loader2" class="loader2" style="display:inline-block; width:50px">&nbsp;</span>';
}
</script>
</html>