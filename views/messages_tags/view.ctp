<div class="messagesTags view">
<h2><?php  __('Messages Tag');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $messagesTag['MessagesTag']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tag'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($messagesTag['Tag']['name'], array('controller' => 'tags', 'action' => 'view', $messagesTag['Tag']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Message'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($messagesTag['Message']['id'], array('controller' => 'messages', 'action' => 'view', $messagesTag['Message']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Messages Tag', true), array('action' => 'edit', $messagesTag['MessagesTag']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Messages Tag', true), array('action' => 'delete', $messagesTag['MessagesTag']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $messagesTag['MessagesTag']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Messages Tags', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Messages Tag', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tags', true), array('controller' => 'tags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tag', true), array('controller' => 'tags', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Messages', true), array('controller' => 'messages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Message', true), array('controller' => 'messages', 'action' => 'add')); ?> </li>
	</ul>
</div>
