<div class="subject"><span style="float:right"><?php echo $objGeneral->dateTimeFormat($time);?></span>Stock Tracker News</div>

<?php foreach($feeds as $feed) {?>
<div class="info">		
	<div><?php echo $feed['stock_feeds']['description'];?> <a href="<?php echo $feed['stock_feeds']['link'];?>" class="readMore">Read more</a> </div>
</div>
<?php } ?>
<div class="paging"><?php if(isset($prevUrl)) { ?><span style="float:left"><a href="/stocks/stockNews/<?php echo $prevUrl;?>"><< Prev</a></span><?php } if(isset($nextUrl)) { ?><span style="float:right"><a href="/stocks/stockNews/<?php echo $nextUrl;?>">Next >></a></span><?php } ?></div>
<div style="clear:both;margin-top:20px"></div>