<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<?php echo $ajax->link( 
    				'<< Back to Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?>
		</span>
	</div>
	<span>
		<?php echo $ajax->link( 
    				'Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?> 
	</span> &nbsp;&gt;&gt;&nbsp; <span><?php echo $data['SMSApp']['name']; ?></span>
</div>
   
   <!-- Stock App -->
   		<div id="appColRight" class="appColRight">
   		<?php
			echo $this->requestAction('/stocks/getRightSide');
      	?>
      	</div>
   		<div id="stockData" class="appColLeft">
      		<div class="appColLeftBox">
      			<div class="appTitle">Start tracking your Stock</div>
      			<div id="stockRecharge" class="inlineErr1" style="display:none">You do not have enough balance. <a href="/users/paynow">Click here to recharge</a></div>
      			<form name="stockForm" action="return;">
				<fieldset>
					<div style="padding-top: 10px;" class="field">
	                    <div class="fieldDetail">
	                         <div class="fieldLabel2 leftFloat"><label for="username">Stock Name:</label></div>
	                         <div class="fieldLabelSpace2">
	                         	<input type="text" value="" autocomplete="off" name="data[Stock][company]" id="stockauto" tabindex="1" style="width:280px">
	                         	<div class="autoComplete position2" id="AppStockFor_autoComplete" style="display: none;"></div>
							        <script> new Ajax.Autocompleter("stockauto", "AppStockFor_autoComplete", "/stocks/autoCompleteCompany", {paramName: "data[Stock][company]", 
							  minChars: 3,  afterUpdateElement : changeHideStock
							  });</script>
	                            <br>
	                            <span class="hints">Start with the first 3 chars of company name</span>
	                            <span id="err_pname" class="inlineErr1" style="display: none;"></span>
	                            <input type="hidden" value="" id="stockCode">
	                            <div id="nsebseflag" style="margin-top:10px"></div>
	                         </div>                     
	                 	</div>
	            	</div>
	            	<div style="padding-top: 10px;" class="field">
	                    <div class="fieldDetail">
	                         <div class="fieldLabel2 leftFloat"><label for="username">Stock Alert:</label></div>
	                         <div class="fieldLabelSpace2 appBold" style="position:relative; left:-6px;">
	                            <div class="rightFloat" style="padding-top:2px;">- <span><img src="/img/rs.gif" class="rupee1"></span><?php echo APP_STOCK_PRICE_WITH_NEWS ?> per stock </div><span class="leftFloat"><input checked="checked" name="newsFlag" type="radio" value="1"></span><span class="leftFloat" style="padding-top:2px;">Price Summary with News</span>	                            
	                         </div><div class="clearBoth">&nbsp;</div>
	                         <div class="fieldLabelSpace2" style="padding-left:14px;"><span class="hints">Instant News alerts related to your stock & daily price summary on Market Days</span></div>                     
	                 	</div>
	            	</div>
	            	<div style="padding-top: 10px;" class="field">
	                    <div class="fieldDetail">
	                         <div class="fieldLabel2 leftFloat">&nbsp;</div>
	                         <div class="fieldLabelSpace2 appBold" style="position:relative; left:-6px;">
	                            <span class="rightFloat" style="padding-top:2px;">- <?php echo APP_STOCK_PRICE*100 ?> paise per stock</span><span class="leftFloat"><input name="newsFlag" type="radio" value="0"></span><span class="leftFloat" style="padding-top:2px;">Price Summary</span>
	                         </div><div class="clearBoth">&nbsp;</div>
	                         <div class="fieldLabelSpace2" style="padding-left:14px;"><span class="hints">Daily price summary on Market Days</span></div>                     
	                 	</div>
	            	</div>
	            	<div style="padding-top: 10px;" class="field">
	                    <div class="fieldDetail">
	                         <div class="fieldLabel2 leftFloat">&nbsp;</div>
	                         <div id="sendButt" class="leftFloat">
	                         	<a href="javascript:void(0);" onclick="createStockAlert();" ><img src="/img/spacer.gif" class="otherSprite oSPos7" /></a>  
	                         </div>                     
	                 	</div>
	            	</div>
	            	<div class="clearLeft"></div>
	            </fieldset>
	           	</form>
			</div>		
		</div>
		<div class="clearRight">&nbsp;</div>
   <!-- Stock App ends -->	
   
   <script>$('stockauto').focus();</script>
   
    