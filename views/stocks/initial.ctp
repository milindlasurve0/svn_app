<div class="loginCont">
  <div>
    <div class="leftFloat dashboardPack">
      <div class="catList">
        <ul id='innerul'>
           <?php 
           		$class1 = 'sel';
           		$class2 = '';
           		if($about == 1){
           			$class1 = '';
           			$class2 = 'sel';
           		}
           ?>	
           <li name='innerli'><?php echo $ajax->link( 
    				'Stock Tracker Alerts ', 
    				array('action' => 'createAlert'),
    				array('id' => 'createAlert', 'class' => "$class1" ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("createAlert").removeClassName("loader")' )
					); ?> </li>
		  <li name='innerli'><?php echo $ajax->link( 
    				'About Stock Tracker ', 
    				array('action' => 'about'),
    				array('id' => 'about', 'class' => "$class2" ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("about").removeClassName("loader")' )
					); ?> </li>
		  <li name='innerli'><?php echo $ajax->link( 
    				'Learn More ', 
    				array('action' => 'learnMore'),
    				array('id' => 'learn', 'class' => "" ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("learn").removeClassName("loader")' )
					); ?> </li>			
		  <li name='innerli'><?php echo $ajax->link( 
    				"FAQs ",
    				array('action' => 'faqs'),
    				array('id' => 'faqs', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("faqs").removeClassName("loader")' )
					); ?> </li>
		   <li class="hList" name='innerli'>
           		<?php echo $ajax->link( 
    				'Personal Alerts Home ', 
    				array('controller' => 'apps','action' => 'getApps'),
    				array('id' => 'allApps', 'class' => '' ,'update' => 'pageContent','onclick' => 'changeInnerTabClass(this);','complete' => '$("allApps").removeClassName("loader")' )
				); ?>    				
		  </li>		
        </ul>
        
      </div>
    </div>
    
    <div style="margin-left:223px;" id="innerDiv">
      	<?php
      		if($about == 1)	{
      			echo "<script>$('about').simulate('click');</script>";
      		}
      		else {
      			echo "<script>$('createAlert').simulate('click');</script>";
      		}
      	?>      	
    </div>
    <br class="clearLeft" />
 </div>