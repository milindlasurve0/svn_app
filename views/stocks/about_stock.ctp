<?php if(isset($_SESSION['Auth']['User'])){ ?> 
<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<?php echo $ajax->link( 
    				'<< Back to Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?>
		</span>
	</div>
	<span>
		<?php echo $ajax->link( 
    				'Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ),
    				array( 'update' => 'pageContent')
					); ?> 
	</span> &nbsp;&gt;&gt;&nbsp; <span>About Stock Tracker</span>
</div>
<?php } ?>
<div class="appColRight1" style="font-size:0.9em">
	<!-- No -->
	<div class="rightColSpace">
      	<div style="position: relative;" id="recentMsgs">
      	  <div style="" class="antina"></div>                  
          <div class="SampJokesTitle">SAMPLE MESSAGES(SMS)</div>
          <div style="height: 415px;" id="">
            <div class="container">
            <input type="hidden" id="msgNum" value="1"/>
            
              <ul style="overflow: hidden; height: 345px;" class="recentMsgsCont">
              	 <li style="display: block;" id="sampSMS1" class="sampleSMS">
                  <p>
                  *Stock Tracker*<br/>
Bharti Airtel Ltd.<br/>
#Bharti to delist Zambia unit from local bourse<br/>
<br/>
Tata Consultancy Services Ltd.<br/>
#Indian IT CO TATA Consultancy eyes investment in Madhya Pradesh<br/>
<br/>
ICICI Bank Ltd.<br/>
#SBI, ICICI Bank may go up: Astromoneyguru<br/><br/>
Read more: http://bit.ly/eDIH7H
				  </p>
                </li>
                <li style="display: none;" id="sampSMS2" class="sampleSMS">
                  <p>
                  *Stock Tracker*<br/>
Bharti Airtel Ltd.<br/>
#Bharti looks stabilised on the ARPUs front: Avinash Gorakshekar, Anagram Capital Ltd<br/>
#Singtel says buys $30 mln worth of shares in Bharti Airtel<br/><br/>
Read more: http://bit.ly/eLnejh
				  </p>
                </li>
                <li style="display: none;" id="sampSMS3" class="sampleSMS">
                  <p>
                  *Stock Tracker*<br/>
Reliance Broadcast Network Ltd.<br/>
BSE: 79.95 +3.80 (4.99%)<br/>
<br/>
Cholamandalam Investment & Finance Company Ltd.<br/>
NSE: 176.85 -0.25 (-0.14%)<br/>
<br/>
Infosys Technologies Ltd.<br/>
NSE: 3,370.15 +2.25 (0.07%)<br/>
<br/>
Tata Communications Ltd.<br/>
BSE: 249.60 +0.40 (0.16%)<br/>
<br/>
Reliance Capital Ltd.<br/>
NSE: 654.60 +17.95 (2.82%)
                  </p>
                </li>
              </ul>
              <div style="padding: 10px 0px; margin-left: 40%;">
                <ul class="sampleNo">                  
                  <li><a onclick="showSample('pre')" href="javascript:void(0)">&lt;</a>                  
                  </li><li><a onclick="showSample('next')" href="javascript:void(0)">&gt;</a></li>
                </ul>
                <br class="clearLeft">
              </div>
              <!-- <div align="center"><a target="_blank" href="/groups/allRecentMsgs/34">View All</a></div> -->
              </div>
          </div>
		</div>
    </div>
	<!-- no -->
</div>
<div class="appColLeft1 info" id="appReminderAddDiv">
	<div class="title6"><span class="fntSz19 strng"><?php echo $data['SMSApp']['name']; ?></span> - Track your favourite stocks</div>
	<div class="priceBox">
		<div class="appSmallBox1 appImg2">
			<div class="fntSz15 strng"><em>Indian Markets are booming, Indian economy is growing.</em></div>
			<div style="margin:10px 5px 10px 0;"><span class="fntSz15 strng">Price:</span> <?php echo $objGeneral->getPrice($data['SMSApp']['basic_price']); ?> <?php echo $data['SMSApp']['price_tag']; ?></span></div>
			<input type="button" class="otherSprite oSPos28" onclick="showApp('<?php echo $data['SMSApp']['controller_name']; ?>')">
		</div>		
	</div>
	<p><span class="strng">Do you wish to take part in the India growth story?</span><br>
	SMSTadka presents you a Simple yet very powerful tool to track your favourite stocks on daily basis.</p>
	
	<div>
	<span class="strng">Let's understand what "Stock Tracker" does</span>
	<ol class="info">
		<li>Daily price summary SMS of your chosen stocks.</li>
		<li>News associated with your favourite stocks which can affect the 
         price movement.</li>
	</ol>
	</div>
	
	<div>
	<span class="strng">Now, what it doesn't do</span>
	<ol class="info">
		<li>NO unsolicited Tips or Advices from broking firms.</li>
		<li>NO trading or buy/sell calls.</li>
		<li>NO flooding of your mobile Inbox with unwanted information.<br></li>
	</ol>
	</div>
	
	<div>
	<span class="strng">Why Stock tracker? And how it works</span>
	<ol class="info">
		<li>Tool for beginners as well as experts</li>
		<li>Easy interface to add/remove/disable the alerts<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg7"></div></li>
		<li>Pay only for the days your stock is active</li>
		<li>Very attractive price for a powerful service on your mobile<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg8"></div></li>
		<li>Optionally get only daily price summary after market hours</li>
		<li>Track both NSE as well as BSE Stocks<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg9"></div></li>
		<li>Get SMS Alerts on your mobile on every important news event and price summary on each market days.</li>
		<li>For GPRS Users (Mobile with Internet): Follow the Short URL at the bottom of the NEWS SMS for reading the whole story online.
		<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg9"></div>	
		</li>
	</ol>
	</div>
	<div class="rowDividerIn">
		Keep an eye on your stocks, <input type="button" onclick="showApp('<?php echo $data['SMSApp']['controller_name']; ?>')" class="otherSprite oSPos28">
	</div>
	<br>
</div>
<div class="clearRight">&nbsp;</div>