<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<a onclick=" event.returnValue = false; return false;" id="link1315137610" href="/apps/getApps">&lt;&lt; Back to Personal Alerts</a>		</span>
	</div>
	<span>
		<a onclick=" event.returnValue = false; return false;" id="link1226195820" href="/apps/getApps">Personal Alerts</a> 
	</span> &nbsp;&gt;&gt;&nbsp; <span>FAQs</span>
</div>
<div class="info" style="padding: 0px 20px 20px 10px;">
	<div class="title5">What is Stock Tracker?</div>
	<p>It is an SMS service which enables you to track your favourite stocks on BSE and NSE. Stock Tracker gives you the price summary and News related to the stocks chosen by you.</p>
	
	<div class="title5">Will I get buying and selling Calls/Tips related to my stocks?</div>
	<p>No, Stock Tracker will not give you any advice by any expert. It is purely a reporting tool, which gives you information published over various media sources.</p>
	
	<div class="title5">How many SMSes will I receive per day?</div>
	<p>If you have chosen only price summary you'll get one SMS (<?php echo MAX_STOCK_MSG_LENGTH; ?> Char) per 5 stocks.<br>
In case you have chosen for the News option, you'll be receiving an SMS as soon as any significant News broke out in the market. You should expect at least 2-3 SMSes for the News (per 5 stocks).</p>
	
	<div class="title5">What is the URL, I receive at the end of the message?</div>
	<p>It is a short URL that redirects you to SMSTadka, it will show the list of the stories on the message with the detailed story links and short description.<br/>
If you are willing to read more about the headline on the SMS, you can visit this short url to read the whole story from the actual source url.</p>
	
	<div class="title5">How much will I be charged for using this service?</div>
	<p>For receiving price summary at the end of the market day, you'll be charged 25 paisa per stock per day.<br>

For receiving news along with the price, you'll be charged Rs. 1 per day.<br>

Amount will be deducted from your SMSTadka balance. You can recharge your SMSTadka account <a href="/users/paynow">here</a>.<br>

Note: You'll not be charged anything from your mobile operator.</p>

	<div class="title5">How can I Refill/Recharge my SMSTadka balance?</div>
	<p>You can recharge your SMSTadka balance using both online as well as offline payment modes.
<br>
Online payment modes are credit card, ATM (Debit) Card, Net-Banking, Paymate.
<br>
Offline payment modes are DD, Cheque. <a href="/users/paynow">Know more</a> about it.</p>
</div>