<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>SMSTadka offers exciting offers for Mobileseva Busniess Partners!!!</title>
<?php echo $minify->css(array('offers'),6); ?>
<?php //echo $minify->css(array('style'),STYLE_CSS_VERSION); ?>
</head>

<body class="bg">
<?php echo $minify->js(array('merge'),MERGE_JS_VERSION); ?>
<?php echo $minify->js(array('script_app'),SCRIPT_APP_JS_VERSION); ?>
<script>
	function showSubscribe(){	
		$('bg').show();
		if (window.innerHeight && window.scrollMaxY) {// Firefox
		yWithScroll = window.innerHeight + window.scrollMaxY;
		} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		yWithScroll = document.body.scrollHeight;
		} else { // works in Explorer 6 Strict, Mozilla (not FF) and Safari
		yWithScroll = document.body.offsetHeight;
		}
		$('bg').style.height = yWithScroll + 'px';
		centerPos('successPopup');
	}
	
	function closeSubscribe(){
		$('successPopup').hide();
		$('bg').hide();
	}
	
</script>
<?php if(isset($new) && $new == 1) { ?>
<div id="bg" class="popOutline" style="position: absolute; z-index: 989; top:0;left:0;width:98%;display:none">
</div>
<div id="successPopup" class="popOutline" style="position: absolute; z-index: 990;display:none">
	<div class="popCont popContWidth">
		<div class="rightFloat popClose"><a onclick="closeSubscribe();" href="javascript:void(0);">x</a></div>
		<div>&nbsp;</div>
		<div id="successPopupMsg">
		Congrats "<?php echo $data['vendors_retailers']['retailer_code'];?>" !! You have won an SMSTadka daily prize<br/>
		<?php if(empty($data['vendors_retailers']['mobile'])) { ?>
		Please enter below information to avail your gift<br/>
		<?php echo $this->element('winner_form',$data); ?>
		<?php } else { ?>
		Please check if the below information is correct.<br/>
		<?php echo $this->element('winner_form',$data); ?>
		<?php } ?>
		</div>
	</div>	
</div>
<?php } ?>
<div class="headerIndex">
  <div class="headerMainCont">
  	<div class="ossLogo"></div>
    <div class="logo"></div>
    <div class="clearBoth"></div>
  </div>
</div>
<div class="mainCont">
	<h1>SELL SMSTADKA PACKS AND WIN PRIZES INSTANTLY!</h1>
    <div style="margin:20px 30px">
	   	<div class="box">
        	<div class="gift g1"></div>
            <!--<span>Rs. 3500</span>-->
        </div>
	   	<div class="box">
        	<div class="gift g2"></div>
            <!--<span>Rs. 3999</span>-->
        </div>
	   	<div class="box">
        	<div class="gift g3"></div>
            <!--<span>Rs. 2499</span>-->
        </div>
	   	<div class="box">
        	<div class="gift g4"></div>
            <!--<span>Rs. 1199</span>-->
        </div>
	   	<div class="box">
        	<div class="gift g5"></div>
            <!--<span>Rs. 28000</span>-->
        </div>
	   	<div class="box">
        	<div class="gift g6"></div>
            <!--<span>Rs. 5499</span>-->
        </div>
	   	<div class="box">
        	<div class="gift g7"></div>
            <!--<span>Rs. 20000</span>-->
        </div>
        <div class="clearBoth"></div>
    </div>
    <div style="margin:40px 0">
		<div style="width:85%;margin:0px auto">
        	<div class="activate a1 leftFloat"></div>
        	<h2 style="text-align:center;">!!!Go - Goa Offer!!!</h2>
            <p align="center" class="pBlueBig"><img style="margin-top:-50px;" class="goa goa1 rightFloat" src="/img/offers_images/spacer.gif">
            <span style="display:block; padding-top:35px;">On every <span style="color:#FF4615;">Rs.1000</span> sale of SMSTadka packs, win assured prizes & get a chance to win a Goa trip.</span>            
            <!--<span style="display:block; padding-top:10px;font-size:16px;">On every <span style="color:#FF4615;font-weight:bold;">Rs.1000 sale</span>, get an assured prize<br> and an Entry coupon for Goa trip. <br/>A lucky draw will be held on <span style="color:#FF4615;">1st January 2012</span>. And the winner will enjoy a Goa trip.</span>
            <span style="font-size:15px"><i>Offer valid till <span style="color:#FF4615;">31st December</span>. To know your total sale, sms SALE to 09223178889.</i><span>--></p><br><br>
            <div style="width:850px; margin-top:20px" class="leftFloat">
        		<h2>!!!Lots of Prizes to be WON!!!</h2>
            <p align="center" class="pBlueBig">On every activation of SMSTadka Packs, get a chance to win a prize! <br><!--<i>Also Win Assured Prizes every week</i>--></p>
        	</div>
        </div>
        <!--<div style="margin-top:10px;width:800px" class="leftFloat">&nbsp;</div>-->
        <br style="clear:right" /> 

        <div style="text-align:left;display:none" class="leftFloat">
        	<h3>Congratulations!!!</h3>
            <div style="width:340px; border:none" class="boxBp">
            	<div class="ossBP leftFloat"></div>
                <div style="margin-left:100px"><b>Sumer Gupta</b> from Ranchi has won a Camera Worth 9999/- <br>Congratulations!!! <img src="/img/offers_images/spacer.gif" class="giftSm g3Sm"> </div>
                <div class="clearLeft"></div>
            </div>
        </div>
    </div>
    <div class="clearBoth"></div>
   	<!--<h3>"Exclusive Offer only for MobileSeva Business Partners" &ndash; Because we value you</h3>-->
    <div style="margin:20px 0">
    	<div class="<?php if(!empty($winners)) echo 'mainColumnLeft'; else echo 'mainColumnLeft1';?>">
        	<div class="mainTitle">Contest Details</div>
            <div class="subTitle">How to win prizes? 
                <p>
                	On every activation of SMSTadka packs (Check Mobileseva.in VAS Option), you have a chance to win exciting prizes mentioned above.
                </p>
            </div>
            <div class="subTitle">Go - Goa Offer 
                <p>
                	Have a party at Goa for FREE! Sell SMSTadka packs and win a trip for two at Goa worth Rs. 20000. On every sell of SMSTadka packs worth Rs. 1000 win an assured gift and an entry coupon for Goa trip. Sell more to collect multiple coupons and increase your chances to be the winner. For eg. If you sell packs of Rs. 5000, you get 5 assured gifts and 5 Goa coupons. The more the merrier.<br/><br/> 
                	A lucky draw will be held on <span style="color:#FF4615;">1st January 2012</span> and winners will be announced. Lots of other prizes to be won. Offer valid till <span style="color:#FF4615;">31st December</span>.<br/><br/>
                	<b>Trip features:</b><br/>
                	1) Entry for 2 people<br/>
                	2) 3 days 2 nights stay in a superior room<br/>
                	3) Complimentary entry into Crown Lounge & Casino with free lucky chips worth Rs.15000<br/><br/>
                	<b>Note: To know your total sale, sms SALE to 09223178889</b>.                	
                </p>
            </div>
            <!--<div class="subTitle">Weekly Assured Prizes &ndash; Launch Offer 
                <p>
                	<img style="margin-top:-30px" class="activate a2 rightFloat" src="/img/offers_images/spacer.gif">You also have a chance to win assured prizes this week. <span style="color:#D04615">Activate SMSTadka packs only worth Rs. 700 and win an confirmed prize &ndash; Reebok Watch worth Rs 2,499/-</span> <br> Offer started on 17th October 2011. Offer will close on 12.00 AM Night, Sunday, 23rd October and winners will be announced at 05.00 PM, Monday, 24th October 2011.
					
                </p>
            </div>-->
            
 <div class="subTitle">How to Activate SMSTadka Packs?
                <p>
                1. &nbsp; Login to MobileSeva using your account. <br><br>
                2. &nbsp; Go to VAS Option in the MobileSeva.<br>
                <img style="margin-left:30px" class="activate a3" src="/img/offers_images/spacer.gif"><br><br>
                3. &nbsp; Select SMSTadka as operator.<br>
                <img style="margin-left:30px" class="activate a4" src="/img/offers_images/spacer.gif"><br><br>
                4. &nbsp; Find  the exciting new products(SMS Alert Packs) from SMSTadka.<br>
                <img style="margin-left:30px" class="activate a5" src="/img/offers_images/spacer.gif">                
                <br><br>
                5. &nbsp; Offer these services to your customers. <br> <span style="margin-left:30px">(For any assistance, Contact 022 - 66846684 / 022 - 42384238 or </span><br><span style="margin-left:30px"><b>SMS: HELP OSSBP to 09223178889)</b></span><br><br>
                6. &nbsp; On every activation of SMSTadka Pack, you have a chance to <br> <span style="margin-left:30px">win exciting prizes.<!--worth above Rs.1199/- --></span><br><br>
                <span style="margin-left:30px"><a target="_blank" class="cssBut" href="http://www.mobileseva.in/Index.aspx#vas">Start Activating Now</a></span><br><br>
                </p>
            </div>
            <div class="subTitle">Now increase your SMSTadka sale!!
                <p>
                1. &nbsp; We always provide help to our retailers to increase their SMSTadka sale so they can earn extra money from the existing setup.<br>	
                2. &nbsp; SMSTadka provides printable version of its Marketing Material. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/groups/mmdownload/oss" target="_blank">Click to download Marketing Material.</a><br>
                3. &nbsp; You download the marketing Material and print it as per your need. Put that on display in your shop. It will increase visibility of SMSTadka products and eventually help in increasing the sale.<br><br>
                <span style="margin-left:30px"><a target="_blank" class="cssBut" href="/groups/mmdownload/oss" target="_blank">Download Marketing Material</a></span><br><br>
                </p>
            </div>
            
            <div class="subTitle">Important Note
            <p>
            1. &nbsp; One customer can activate more than one pack at a time. <br/>
            2. &nbsp; Activating same pack twice will give validity of two months. <br/><span style="margin-left:30px">Ex. 3 times activation will give three months validity<br/>
            3. &nbsp; Each pack will always give fresh and new messages
            </p>
            </div>
           <!-- <div class="subTitle">How to know the result instantly?
                <p>
                	<img class="activate a6 rightFloat" src="/img/offers_images/spacer.gif">Immediately, after successful activation, you will be presented the result. Winners will be displayed on the right hand side.
					
                </p>
            </div>-->
        </div>
        <div class="<?php if(!empty($winners)) echo 'mainColumnRight'; else echo 'mainColumnRight1';?>">
	        <div class="mainTitle">Latest Winners !!!</div>
	        <?php foreach($winners as $winner) { if(true || (!empty($winner['vendors_retailers']['name']) && !empty($winner['vendors_retailers']['city']))) { ?>
            <div class="boxBp">
            	<div class="leftFloat"><img height="90px" width="90px" src="/img/retailers/img_<?php echo $winner['vendors_retailers']['id'];?>.jpg"/></div>
                <div style="margin-left:100px"><b><?php echo $winner['vendors_retailers']['name']; ?></b> from <?php echo $winner['vendors_retailers']['city'] . "(" . $winner['vendors_retailers']['state'] . ")";?> has won a 
                <?php if($winner['retailers_winners']['type'] == "daily") { ?>
                Daily Lucky Prize Worth Rs. 2500/- <br>Congratulations!!! 
                <?php } if($winner['retailers_winners']['type'] == "weekly") {?>
                Weekly Confirmed Prize as Reebok Watch Worth Rs. 2499/- <br>Congratulations!!! <img class="giftSm g3Sm rightFloat" src="/img/offers_images/spacer.gif">
                <?php } ?>
                </div>
                <div class="clearLeft"></div>
            </div>
            <?php } } ?>
        </div>
		<div class="clearBoth"></div>
   	</div>
</div>
<div id="footer">
	All Rights Reserved &copy; 2011 SMSTadka.com. <a href="/offers/terms_conditions">View Terms &amp; Conditions here</a>
</div>
<?php if(isset($new) && $new == 1) echo "<script>showSubscribe()</script>";?>
<script type="text/javascript">	
	  var _gaq = _gaq || [];
	  _gaq.push(['pageTracker._setAccount', 'UA-18505922-1']);
	  _gaq.push(['pageTracker._trackPageview']);
	         
	  (function() { 
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();	
	</script>
</body></html>