<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>SMSTadka offers exciting offers for <?php if($client == 'tss'){ echo 'Smartshop'; }else{ echo 'Mobileseva';} ?> Busniess Partners!!!</title>
<?php echo $minify->css(array('offers'),3); ?>
</head>

<body class="bg">
<div class="headerIndex">
<?php if($client == 'tss'){ ?>
  <div class="headerMainCont" style="padding:0; background:url(/img/smartshop_bg.gif) top left repeat-x">
  	<div class="ossLogo"><img src="/img/smartshop_logo.gif"></div>
    <div class="logo" style="margin-top:45px"></div>
    <div class="clearBoth"></div>
  </div>
<?php }else{ ?>
	<div class="headerMainCont">
		<div class="ossLogo"></div>
	    <div class="logo"></div>
	    <div class="clearBoth"></div>
	</div>    	
<?php } ?>  
</div>
<div class="mainCont" style="height:450px">
	<div class="subTitle" style="margin-bottom:10px">Terms and Conditions</div>
	<div style="width:800px">
	    <ul>
	    	<li style="margin-bottom:5px;">Lucky winners will be decided only by lucky draw and final decision will be taken only by SMSTadka.</li>
	    	<li style="margin-bottom:5px;">The terms of the contest are subjected to change based on business volume.</li>
	    	<!--<li style="margin-bottom:5px;">The offer is valid only till 26th Oct 2011.</li>-->
	    	<?php if($client == 'tss'){ ?>
			<li style="margin-bottom:5px;">To be eligible for the lucky draw, registrants must be Indian citizens or Indian permanent residents and Smartshop Business Partner with an active TSS Login</li>
			<?php }else{ ?>
			<li style="margin-bottom:5px;">To be eligible for the lucky draw, registrants must be Indian citizens or Indian permanent residents and OSS Business Partner with an active MobileSeva Login</li>    	
			<?php } ?>  
	    	
	    	<li style="margin-bottom:5px;">Each lucky winner must provide his contact details to receive the prizes at their address. The prizes will be sent via courier to the communication address provided. In case of the wrong address or courier service not available in the area, SMSTadka will not be responsible for the delivery.</li>
	    	<li style="margin-bottom:5px;">Winners must agree to provide correct contact details for proper communication related to the delivery of the prizes.</li>
	    	<li style="margin-bottom:5px;">SMSTadka also reserves the right to disqualify any members / entries without having to enter into any correspondence on their decisions.</li>
	    	<li style="margin-bottom:5px;">Your prize will be delivered to you within 20 working days. <?php if($client == 'tss'){ }else{ ?>For more details send SMS: 'HELP OFFER' to 09223178889.<?php } ?></li>
	    </ul>
	</div>
</div>
<div id="footer">
	All Rights Reserved &copy; 2011 SMSTadka.com.
</div>
</body></html>