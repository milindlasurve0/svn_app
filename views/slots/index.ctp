<div class="slots index">
	<h2><?php __('Slots');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('start');?></th>
			<th><?php echo $this->Paginator->sort('end');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($slots as $slot):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $slot['Slot']['id']; ?>&nbsp;</td>
		<td><?php echo $slot['Slot']['name']; ?>&nbsp;</td>
		<td><?php echo $slot['Slot']['start']; ?>&nbsp;</td>
		<td><?php echo $slot['Slot']['end']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $slot['Slot']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $slot['Slot']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $slot['Slot']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $slot['Slot']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Slot', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Packages Users', true), array('controller' => 'packages_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packages User', true), array('controller' => 'packages_users', 'action' => 'add')); ?> </li>
	</ul>
</div>