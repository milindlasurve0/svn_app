<div class="slots view">
<h2><?php  __('Slot');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $slot['Slot']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $slot['Slot']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Start'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $slot['Slot']['start']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('End'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $slot['Slot']['end']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Slot', true), array('action' => 'edit', $slot['Slot']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Slot', true), array('action' => 'delete', $slot['Slot']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $slot['Slot']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Slots', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slot', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages Users', true), array('controller' => 'packages_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packages User', true), array('controller' => 'packages_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Packages Users');?></h3>
	<?php if (!empty($slot['PackagesUser'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Package Id'); ?></th>
		<th><?php __('User Id'); ?></th>
		<th><?php __('Review'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Slot Id'); ?></th>
		<th><?php __('Start'); ?></th>
		<th><?php __('End'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($slot['PackagesUser'] as $packagesUser):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $packagesUser['id'];?></td>
			<td><?php echo $packagesUser['package_id'];?></td>
			<td><?php echo $packagesUser['user_id'];?></td>
			<td><?php echo $packagesUser['review'];?></td>
			<td><?php echo $packagesUser['active'];?></td>
			<td><?php echo $packagesUser['slot_id'];?></td>
			<td><?php echo $packagesUser['start'];?></td>
			<td><?php echo $packagesUser['end'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'packages_users', 'action' => 'view', $packagesUser['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'packages_users', 'action' => 'edit', $packagesUser['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'packages_users', 'action' => 'delete', $packagesUser['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $packagesUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Packages User', true), array('controller' => 'packages_users', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
