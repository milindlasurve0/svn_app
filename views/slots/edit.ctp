<div class="slots form">
<?php echo $this->Form->create('Slot');?>
	<fieldset>
 		<legend><?php __('Edit Slot'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('start');
		echo $this->Form->input('end');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Slot.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Slot.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Slots', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Packages Users', true), array('controller' => 'packages_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packages User', true), array('controller' => 'packages_users', 'action' => 'add')); ?> </li>
	</ul>
</div>