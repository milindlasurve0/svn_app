<?php  //echo "<pre>"; print_r($user_data); echo "</pre>";?>

<script>

function addComment(id,mobile){
	var url = '/comments/addComment';
	var rand   = Math.random(9999);
	var pars   = "id="+id+"&text="+encodeURIComponent($('commentArea').value)+"&followupdate="+$('followupdate').value+"&email="+$('userEmail').value+"&type="+0+"&mobile="+mobile+"&rand="+rand;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						var html = transport.responseText;
						Element.insert('commentBox',{top:html});
						$('commentArea').value = "";
						$('userEmail').value = "";						
						$('followupdate').value = "";				
					}
				});
}

function submitRetailer(retailerId){
	if(!retailerId)
		retailerId = $('userId').value;
		
   var Index = $("retailer").selectedIndex;
   var setValue = $("retailer").options[Index].value;
 
     var url = '/users/submitRetailer';
     var pars   = "retailerNameValue="+setValue+"&userId="+retailerId;
     var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{
						var html = transport.responseText;
						if(html==1){
							var tmp = '<div class="taggLink taggLink1" style="font-size:9pt"><div class="taggLinkBG"><div class="taggLinkBorder"><div class="taggLinkCont"><a href="/tags/getTaggedUsers/'+setValue+'"><b>'+setValue+'</b></a></div></div></div></div>';
							$("Values").innerHTML = $("Values").innerHTML + tmp;
						}
								
					}
				});
  }
    

function showRetailerForm()
{
	retailerNumber = $('userId').value;
	if($("retailBox").style.display == 'none')
	$("retailBox").show();
	else
	$("retailBox").hide();		
			
}

function addNewRow()
{
var table = $("pwd");
var row = table.insertRow(2);
row.id="test";
var cell1 = row.insertCell(0);
var cell2 = row.insertCell(1);
var element2 = document.createElement("input");
var element3=document.createElement("input");
element2.id = "change_mobile";
element2.type = "text";
element3.type="button";
element3.value="Send Password";
element3.onclick = new Function("sendPassword1()");
cell1.innerHTML="Enter number for sending password";
cell2.appendChild(element2);
cell2.appendChild(element3);
}
 
function sendPassword1(mobile)
{

	var url = '/users/sendPassword1';
	var pars   = "mobile="+$('change_mobile').value;

	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						$('pin').innerHTML= transport.responseText;											
																
					}
				});
				if($('userInfo').style.display == 'none'){
		$('userInfo').style.display = 'block';
		$('numberUpdation').style.display = 'none';
	}else{
		$('userInfo').style.display = 'none';
		$('numberUpdation').style.display = 'block';
	}
}

function display()
{
if($('userInfo').style.display == 'none'){
		$('userInfo').style.display = 'block';
		$('numberUpdation').style.display = 'none';
	}else{
		$('userInfo').style.display = 'none';
		$('numberUpdation').style.display = 'block';
		$('test').style.display='none';
	}
}

function addNewNumber(oldNumber,newNumber)
{
	
	
	var oldNumber=oldNumber;
	var newNumber=newNumber;
	if(newNumber=='')
	{
	alert("New Number field is blank. Please enter a new number");
	return;
	}
	if (mobileValidate(newNumber)==false)
	{
	alert("Please enter a valid mobile number.");
	return;
	}
	
	if(newNumber==oldNumber)
	{
	alert ("Old number and new number provided are same. Please give a new number.");
	return;
 	}
 	
	var confirmNumber=prompt("Please confirm your new number");
	
	if(confirmNumber==null)
	{
	$('newNumber').value ='';
	return;
	}	
	if(newNumber<confirmNumber||newNumber>confirmNumber)
	{
	alert ('Mismatch between New Number and Confirmed number. Enter again.');
	return;
 	}
	var url = '/users/addNewNumber';
	var pars   = "oldNumber="+oldNumber+"&newNumber="+newNumber;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						var html = transport.responseText;
						var res = transport.responseText;
						var arr = res.split('^^^');
						if(arr[0] == '1'){
							$('test').innerHTML="Your number "+oldNumber+" has been changed to "+newNumber+". "+arr[1];
							$('oldNumber').value = "";
							$('newNumber').value = "";						
							$('confirmNumber').value = "";
						}else{
							alert('Try again.');						
						}
						
										
					}
				});
}




function updateExpDate(id)
{
 	var noOfDays=$('days'+id).value;
 	if(isNaN(noOfDays)){
 	alert ('Please enter a valid number');
 	 	return;
 	}
	var url = '/groups/updateExDate';
	var rand   = Math.random(9999);
	var pars   = "noOfDays="+noOfDays+"&id="+id+"&rand="+rand;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						//alert(transport.responseText);
						$('end'+id).innerHTML = transport.responseText;
						$('days'+id).value = ''; 
					}
				});
}


function activatePackage(pkgUsrId){
	var tmp = "fnInitCalendar(this, 'calender"+pkgUsrId+"','close=true')";
	var tmp1="updatePkgEndDate("+pkgUsrId+",'0')";
	$('action'+pkgUsrId).innerHTML = '<input type="text" id="calender'+pkgUsrId+'" onmouseover="'+tmp+'" /><input type="button" value="ADD" onclick="'+tmp1+'" />';
}

function updatePkgEndDate(pkgId,flag)
{
	var newEndDate = '';
	if($('calender'+pkgId))
	newEndDate=$('calender'+pkgId).value;
   	
	var url = '/groups/updatePkgEndDate';
	var pars   = "end="+newEndDate+"&pkgId="+pkgId+"&flag="+flag;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
						var chkSuccess=1;
							
						if(transport.responseText=='invalid')
						{
						alert("Invalid dates.");
						chkSuccess=0;
						return;
						}
						
						if(transport.responseText=='Please enter a date before editting')
						{					
						chkSuccess=0;
						}
						$('endDate'+pkgId).innerHTML = transport.responseText;
						$('calender'+pkgId).value = '';
						if(chkSuccess)
						$('active'+pkgId).innerHTML = 'Active';
					}
				});
}

function activateProduct(prdUsrId){
	var tmp = "fnInitCalendar(this, 'calenderPrd"+prdUsrId+"','close=true')";
	var tmp1="updateProdEndDate("+prdUsrId+",'0')";
	$('actionProduct'+prdUsrId).innerHTML = '<input type="text" id="calenderPrd'+prdUsrId+'" onmouseover="'+tmp+'" /><input type="button" value="ADD" onclick="'+tmp1+'" />';
}

function submitPayment(usrId){
var userId=usrId;
var amount=$('payAmount').value;
if(isNaN(amount))
{
alert("Please enter a valid amount.");
return;
}
var details=$('otherDetails').value;

var Index = $("PaymentSelection").selectedIndex;
var setValue = $("PaymentSelection").options[Index].value;

var url = '/users/submitPayment';
	var pars   = "userId="+userId+"&amount="+amount+"&details="+details+"&type="+setValue;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
					$('response').innerHTML = transport.responseText;
}});
}



function updateProdEndDate(prdId,flag)
{
var newEndDate = '';
	if($('calenderPrd'+prdId))
	newEndDate=$('calenderPrd'+prdId).value;
  	
	var url = '/groups/updateProdEndDate';
	var pars   = "end="+newEndDate+"&prdId="+prdId+"&flag="+flag;
	var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{ 	
					var chkSuccess=1;
							
						if(transport.responseText=='invalid')
						{
						alert("Invalid dates.");
						chkSuccess=0;
						return;
						}
						
						if(transport.responseText=='Please enter a date before editting')
						{					
						chkSuccess=0;
						}
						$('endDateForProduct'+prdId).innerHTML = transport.responseText;
						$('calenderPrd'+prdId).value = '';
						if(chkSuccess)
						$('activeProduct1'+prdId).innerHTML = 'Active';
					}
				});
}

function openCloseSMS(){
	if($('smsArea').style.display == 'none'){
		$('smsArea').show();
	}
	else {
		$('smsArea').hide();
	}
}
</script>
<table border="0" cellpadding="0" cellspacing="0" summary="List Users" width="100%" align="center">

	<tr>
		<td valign="top" width="50%">
			<div id="userInfo" style="display:block">
			<table id="pwd" border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
				<caption class="header">
				User Info
				</caption>
			<?php if(!empty($user_data['User']['name'])) {?>
				<tr align="left">
					<td>Name</td>
					<td><?php echo $user_data['User']['name']; ?></td>
				</tr>
			<?php }?>
			
				<tr align="left">
					<td>Mobile</td>
					<td><?php echo $user_data['User']['mobile'];echo "&nbsp";echo "&nbsp";echo "&nbsp";echo "&nbsp"; ?><input type=button id="change" value="CHANGE"  onclick="addNewRow()"></td>
				</tr>
				
				
				<tr align="left">
					<td>User Id</td>
					<td><?php echo $user_data['User']['id']; ?><input type="hidden" id="userId" value="<?php echo $user_data['User']['id']; ?>"></td>
				</tr>
				
				<tr align="left">
					<td>Type</td>
					<td><?php echo /* $user_data['User']['name'].":  ". $user_data['User']['mobile']."  is  ".*/ $role; ?></td>
				</tr>
				
				<tr align="left">
					<td>Email</td>
					<td><?php echo $user_data['User']['email']; ?></td>
				</tr>
				<tr align="left">
					<td>Balance</td>
					<td><?php echo $user_data['User']['balance']; ?></td>
				</tr>
			
				<tr align="left">
					<td>Registered On</td>
					<td><?php echo $user_data['User']['created']; ?></td>
				</tr>
				<tr align="left">
					<td>No. of times logined since 2010-12-09</td>
					<td><?php echo $user_data['User']['login_count']; ?></td>
				</tr>
				<tr align="left">
					<td>Last Logined On</td>
					<td><?php echo $user_data['User']['modified']; ?></td>
				</tr>
				<tr align="left">
					<td>State</td>
					<td><?php echo $mobileDetails[0]; ?></td>
				</tr>
				<tr align="left">
					<td>Telecom Operator</td>
					<td><?php echo $mobileDetails[1]; ?></td>
				</tr>
				<tr align="left">
					<td>DND User</td>
					<td><?php if($user_data['User']['dnd_flag'] == 1) echo "Yes"; else echo "No"; ?></td>
				</tr>
				<tr align="left">
					<td>NCPR Preference</td>
					<td><?php echo $user_data['User']['ncpr_pref']; ?></td>
				</tr>
				<tr align="left">
					<td>Opt Status</td>
					<td><?php if($user_data['User']['opt_flag'] == 0) echo 'Not Opted'; else if($user_data['User']['opt_flag'] == 1) echo 'Opted'; else echo 'Unknown'; ?></td>
				</tr>
				<tr align="left">
					<td> Free SMS Sent </td>
					<td> <?php echo $user_data['User']['smsfree_count']; ?></td>
				</tr>
				<tr align="left">
					<td> SMS Forwarded </td>
					<td> <?php echo $user_data['User']['smsfwd_count']; ?></td>
				</tr>
				<tr align="left">
					<td> Follow-up date</td>
					<td> <?php if(isset($user_data['User']['followup']))echo date('j M, y',strtotime($user_data['User']['followup'])); ?></td>
				</tr>
			</table>
			</div>
			<div id="numberUpdation" style='display:none'>
					<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
						<tr><!-- sdfs-->
							<td>Old Number</td>
							<td><?php echo $user_data['User']['mobile'];  ?></td>
						</tr>
						<tr><!-- sdfs-->
							<td>New Number</td>
							<td><input type="text" id="newNumber" ></td>
						</tr>
						<tr>
							<td>Pin</td>
							<td><span id="pin"></span></td>
						</tr>
						<tr>
							<td><input type="button" value="CHANGE"
							 onclick="addNewNumber('<?php echo $user_data['User']['mobile'];  ?>',$('newNumber').value)">&nbsp;&nbsp;<input type=button id="change" value="Back"  onclick="display();"></td>
							<td>&nbsp;
							<tr>
							</td>
						</tr>
					</table>
			</div>	
			</br>
			</br>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="margin-top:20px;">
				
				
				<tr><td>
				<div class="tagged ie6Fix2"> 
                        <span class="leftFloat" style="font-size:11pt"> Tagged :</span>                     
                        <div style="margin-left:80px;" id="Values">
                        	<?php foreach($tags as $tag) { ?>
							<div class="taggLink taggLink1" style="font-size:9pt">
                              <div class="taggLinkBG">
                                <div class="taggLinkBorder">
                                  <div class="taggLinkCont"><a href="/tags/getTaggedUsers/<?php echo $tag['Tagging']['name']; ?>"><b><?php echo $tag['Tagging']['name']; ?></b></a></div>
                                </div>
                              </div>
                            </div>
                 			<?php } ?>
                        </div>
                        <div class="clearLeft"></div>                                 		              		
                    </div>
				</td></tr>
			<!--	Nisha Commented Start
			<tr><td>
				<select id="RetailerSelection">
   							<option value="PLAYWINRETAILER">Playwin Retailer</option>
 							 <option value="OSSRETAILER">OSS Retailer</option>
						     <option value="SMARTSHOPRETAILER">SmartShop Retailer </option>
 							 <option value="OURRETAILER">Our Retailer</option>
						</select>  
					<input type="button" value="Sumbit" onclick="submitRetailer('<?php echo $user_data['User']['id']; ?>')"/>
				</td></tr>
			Nisha Commented End-->
			</table>
		</td>

		<td valign="top" width="50%" style="padding-left:50px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
				<caption class="header">
				<?php echo count($comments); ?> comments (<a href="/groups/getLastComments"> All comments </a>)
				</caption>
				<tr><td>
					<div id="commentBox">
					<?php
						foreach($comments as $comment){ 
							echo $this->element('commentElement',array('comment' => $comment)); 
						}
					?>
					</div>
					<div style="padding-top:20px">
						<table>
						<tr><td><textarea class="input textarea" id="commentArea" style="height: 70px; width: 450px; line-height: 1.5em; 
						font-family: Arial,Helvetica,sans-serif; font-size: 14px; direction: ltr;" autocomplete="off"></textarea></td></tr>
						<tr><td>Email: <input type="text" name="userEmail" id="userEmail"></td></tr>
						<tr><td>Follow up: <input type="text" name="followupdate" id="followupdate" value="" onmouseover="fnInitCalendar(this, 'followupdate','close=true')" /></td></tr>
						<tr><td><input type="image" onclick="addComment(<?php if(!empty($user_data['User']['id']))echo $user_data['User']['id']; else echo '-1'; ?>,'<?php echo $mobile; ?>');" class="otherSprite oSPos7" src="/img/spacer.gif"></td></tr>
						</table>
					</div>
			<a href="javascript:void(0)" onclick="showRetailerForm()">RETAILERS FORM</a>
				<div id="retailBox" style="display:none" >
					<?php echo $this->element('retailer_form');?>
				</div>	

					<div style="margin-top:10px;"><a href="javascript:void(0);" onclick="openCloseSMS();">Send SMS</a></div>
					<div id="smsArea" style="display:none">
					<?php echo $this->element('push_sms',array('mobile' => $mobile));?>
					</div>
				</td></tr>
			</table>	
		</td>
		</td>
	</tr>	
</table>	




<div id="logsBox">
<span id=> USER EVENTS </span>
					<?php
						foreach($logs as $data){ 
							echo $this->element('logs_cc',array('data' => $data)); 
						}
					?>

</div>

		
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="padding-top:50px;">
	<tr>
		<td valign="top" width="50%">
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
			<caption class="header">
			Products
			</caption>
			<tr align="left">
	  			<th width="14%">Name</th>
	  			<th width="8%">Status</th>
	  			<th width="8%">Trial/Product</th>
	  			<th width="10%">Start Date</th>
	  			<th width="10%">End Date</th>
	  			<th width="80%">Edit</th>
	  		</tr>
	  	       
			<?php $heap=array(); foreach($products as $product){ ?>
			<tr align="left">
			<td> <a href="/retailers/product/<?php echo $product['ProductsUser']['product_id'];?>">
				<?php echo $product['Product']['name']; ?></a>
				<?php 
					if(in_array($product['ProductsUser']['product_id'],$heap))
					$exist=1;
					else
					$exist=0;	
				?></td>
			<td><span id="activeProduct1<?php echo $product['ProductsUser']['id']; ?>"> <?php if($product['ProductsUser']['active'] == '1') { echo "Active"; array_push($heap,$product['ProductsUser']['product_id']); } else { echo "InActive"; }?></span></td>
			<td> <?php if($product['ProductsUser']['trial'] == '1' && $product['ProductsUser']['count'] > 1){ echo "Trial + (Product - " . ($product['ProductsUser']['count'] - $product['ProductsUser']['trial']). " )"; } else if($product['ProductsUser']['trial'] == '1') { echo "Trial"; } else { echo "Product - " . $product['ProductsUser']['count']; }?></td>
			<td> <?php echo $product['ProductsUser']['start'];?></td>
			<td> <span id="endDateForProduct<?php echo $product['ProductsUser']['id']; ?>"><?php echo $product['ProductsUser']['end']; ?></span></td>
			<td style="text-align:center;background-color:#E3FADB;width:100px;textarea-width:20px">
			
			<span id="actionProduct<?php echo $product['ProductsUser']['id'] ;?>">
			<?php if($product['ProductsUser']['active'] == '1' && $product['ProductsUser']['end']!=null && $exist==0){ ?> 
			<input type="text" id="calenderPrd<?php echo  $product['ProductsUser']['id']; ?>" onmouseover="fnInitCalendar(this, 'calenderPrd<?php echo  $product['ProductsUser']['id']; ?>','close=true')" />       
            <input type="button" value="ADD" onclick="updateProdEndDate('<?php echo $product['ProductsUser']['id']; ?>','0')" /></span>    
			<?php } else { if($product['ProductsUser']['end']!=null && $exist==0){ ?>
			<input type="button" value="ACTIVATE" onclick="activateProduct('<?php echo $product['ProductsUser']['id'];?>')" /> <?php }} ?>
	  	    
	  	    </tr>
			<?php } ?>
			</table>
		</td>
		
		<td valign="top" width="25%" style="padding-left:50px;">
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
			<caption class="header">
			Retailers
			</caption>
			<tr align="left">
	  			<th width="5%">Name</th>
	  			<th width="20%">Address</th>
	  		</tr>
	  		
	  		
	  	       
			<?php foreach($retailers as $retailer){ ?>
			<tr align="left">
				<td> <a href="/retailers/index/<?php echo $retailer['Retailer']['id'];?>"><?php echo $retailer['Retailer']['name'];?></a></td>
				<td> <?php echo $retailer['Retailer']['shopname'] . "<br/>" . $retailer['Retailer']['address'];?></td>
			</tr>
			<?php } ?>
			<?php foreach($retailers_ext as $retailer){ ?>
			<tr align="left">
				<td> <a href="/retailers/index/<?php echo $retailer['Retailer']['id'];?>"><?php echo $retailer['Retailer']['name'];?></a></td>
				<td> <?php echo $retailer['Retailer']['shopname'] . "<br/>" . $retailer['Retailer']['address'];?></td>
			</tr>
			<?php } ?>
			</table>
		</td>
		<td valign="top" width="25%" style="padding-left:50px;">
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
			<caption class="header">
			SMSes Sent By User
			</caption>
			<tr><td>
					<div id="commentBox">
					<?php foreach($msgs as $msg){ ?>
						<div class="appDataCont appDataContClr1">
							<div class="rightFloat">
								<span style="padding-top: 2px;" class="leftFloat"><?php echo $msg['log_smsrequest']['timestamp']; ?></span>
							</div>
							<br class="clearRight"/>
							<p class="appDataDesc"><?php echo $msg['log_smsrequest']['message_in']; ?></p>
						</div>
					<?php }?>
					</div>
				</td></tr>
			</table>
		</td>
	</tr>

</table>


<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="padding-top:50px;" >
	<tr>
		<td valign="top" width="50%">
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left" style="margin-bottom:60px;" >
			<caption class="header">
			
			Packages
			</caption>
			<tr align="left">
	  			<th width="15%">Name</th>
	  			<th width="10%">Status</th>
	  			<th width="12%">Start Date</th>
	  			<th width="13%">End Date</th>
	  			<th width="80%">Edit</th>
	  		</tr>
	  	    
	  	    <?php $heap=array(); foreach($user_data['Package'] as $package){ ?>
			<tr align="left">
			<td><a href="/groups/getPackInfo/<?php echo $package['id'];?>"><?php echo $package['name'];?></a>
				<?php 
					if(in_array($package['id'],$heap))
					$exist=1;
					else
					$exist=0;	
					
				?>
			</td>
			<td><span id="active<?php echo $package['PackagesUser']['id'];?>"> <?php if($package['PackagesUser']['active'] == '1') { echo "Active"; array_push($heap,$package['id']);} else { echo "InActive"; array_push($heap,$package['id']);}?></span></td>
			<td><span id="start<?php echo $package['PackagesUser']['id'];?>"><?php echo $package['PackagesUser']['start'];?></span></td>
			<td><span id="endDate<?php echo $package['PackagesUser']['id'];?>"> <?php echo $package['PackagesUser']['end']; ?></span></td>
			<td style="text-align:center;background-color:#E3FADB;width:100px;textarea-width:20px">
			<span id="action<?php echo $package['PackagesUser']['id'];?>">
			<?php if($package['PackagesUser']['active'] == '1' && $exist==0){ ?> 
            <input type="text" id="calender<?php echo $package['PackagesUser']['id'];?>" onmouseover="fnInitCalendar(this, 'calender<?php echo $package['PackagesUser']['id'];?>','close=true')" />       
            <input type="button" value="ADD" onclick="updatePkgEndDate('<?php echo $package['PackagesUser']['id'];?>','0')" /></span>    
			<?php } else if($exist==0) { ?>
			<input type="button" value="ACTIVATE" onclick="activatePackage('<?php echo $package['PackagesUser']['id'];?>')" /> <?php } ?>
	  	    
	  	    </tr>
			<?php } ?>
			</table>
			
		</td>
		
				
		
		<td valign="top" width="50%" style="padding-left:50px;">
			<table>
				<tr>
				<td>Mode of Payment :</td>
				<td><select id="PaymentSelection">
   							<option value="FREE">Free</option>
 							 <option value="CASH">Cash</option>
						     <option value="NEFT">NEFT</option>
 							 <option value="CHEQUE">Cheque</option>
 							 <option value="DD">DD</option>
				    </select>
				</td>  
			    </tr>
			
				<tr><td>Amount(in Rupees):</td>
						<td><input type="text" id="payAmount"/></td>
					    </br>
				</tr>
			
				<tr>
				<td>Other Details:</td>
				<td><input type="text" id="otherDetails"/></td>
			    <td><input type="button" value="Sumbit" onclick="submitPayment('<?php echo $user_data['User']['id']; ?>')"/></td>	
			    <td><span id="response"> </span></td>				    
				</tr>
			</table>
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
			<caption class="header">
			Payment Details
			</caption>
			
			<tr align="left">
	  			<th width="15%">Date</th>
	  			<th width="20%">Flag</th>
	  			<th width="15%">Amount</th>
	  		</tr>
	  	       
			<?php foreach($payment_details as $payment){ ?>
			<tr align="left">
				<td> <?php echo $payment['payment']['start_time'];?></td>
				<td> <?php if(empty($payment['payment']['flag'])) { echo "TRY"; } else { echo $payment['payment']['flag']; }?></td>
				<td> <?php echo $payment['payment']['amount'];?></td>
			</tr>
			<?php } ?>
			</table>
		</td>
	</tr>

</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="padding-top:50px;">
	<tr>
		<td valign="top" width="33%" style="padding-right:10px;">
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
			<caption class="header">
			<a href="/groups/getAlertsInfo/pnr">PNR Alerts</a>
			</caption>
			<tr align="left">
	  			<th width="10%">PNR Title</th>
	  			<th width="7%">Status</th>
	  			<th width="8%">Start Date</th>
	  			<th width="8%">End Date</th>
	  		</tr>
	  	       
			<?php foreach($user_data['Pnr'] as $pnr){ ?>
			<tr align="left">
			<td><?php echo $pnr['title'];?></td>
			<td> <?php echo ucfirst(strtolower($pnr['chart_status'])); ?></td>
			<td> <?php echo Date('Y-m-d', strtotime($pnr['start']));?></td>
			<td> <?php if(!empty($pnr['end'])) echo Date('Y-m-d', strtotime($pnr['end']));?></td>
			</tr>
			<?php } ?>
			</table>
		</td>
		<td valign="top" width="33%" style="padding-right:10px;">
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
			<caption class="header">
			<a href="/groups/getAlertsInfo/stock">Stock Alerts</a>
			</caption>
			<tr align="left">
	  			<th width="10%">Stock Code (NSE/BSE)</th>
	  			<th width="7%">Status</th>
	  			<th width="8%">Start Date</th>
	  			<th width="8%">End Date</th>
	  		</tr>
	  	       
			<?php foreach($user_data['Stock'] as $stock){ ?>
			<tr align="left">
			<td><?php echo $stock['stock_code']; if($stock['news_flag'] == 1) echo " (News)";?></td>
			<td> <?php if($stock['status_flag'] == '1') { echo "Active"; } else if($stock['status_flag'] == '0' && $stock['temp_flag'] == '0') { echo "Disabled"; } else if($stock['status_flag'] == '0' && $stock['temp_flag'] == '1') {echo "Disabled - No Money";} ?></td>
			<td> <?php echo Date('Y-m-d', strtotime($stock['start']));?></td>
			<td> <?php if(!empty($stock['end'])) echo Date('Y-m-d', strtotime($stock['end']));?></td>
			</tr>
			<?php } ?>
			</table>
		</td>
		<td valign="top" width="33%">
			<table border="1" cellpadding="0" cellspacing="0" width="100%" align="left">
			<caption class="header">
			<a href="/groups/getAlertsInfo/reminder">Reminders</a>
			</caption>
			<tr align="left">
	  			<th width="10%">For</th>
	  			<th width="8%">Type</th>
	  			<th width="8%">Status</th>
	  			<th width="8%">Start Date</th>
	  		</tr>
	  	       
			<?php foreach($user_data['Reminder'] as $reminder){ ?>
			<tr align="left">
			<td><?php if($user_data['User']['mobile'] == $reminder['reminder_for'])echo "Me"; else {echo "Others (" . count(explode(",",$reminder['reminder_for'])) . ")" ;} ?></td>
			<td> <?php if(!empty($reminder['day']))echo "Daily"; else if(!empty($reminder['week']))echo "Weekly"; else if(!empty($reminder['month']))echo "Monthly"; else if(!empty($reminder['year']))echo "Yearly"; else echo "One Time";?></td>
			<td> <?php if($reminder['status'] == '0') echo "Active"; else echo "In-Active"; ?></td>
			<td> <?php echo Date('Y-m-d', strtotime($reminder['created']));?></td>
			</tr>
			<?php } ?>
			</table>
		</td>
	</tr>

</table>