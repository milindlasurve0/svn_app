<div style="min-height:200px;">
<table border="0" cellpadding="0" cellspacing="0" summary="Scripts" align="left" width="100%">
	<caption class="header">
	<b>Scripts</b>
	</caption>
	<tr align="left">
		<td width="5%">1)</td>
		<td width="95%"><a href="/groups/paymentUsersScript">Users who tried payment</a></td>
	</tr>
	<tr align="left">
		<td width="5%">2)</td>
		<td width="95%"><a href="/groups/dailyReport/<?php echo date("Y-m-d"); ?>">Daily Report (<?php echo date("Y-m-d"); ?>)</a></td>
	</tr>
	<tr align="left">
		<td width="5%">3)</td>
		<td width="95%"><a href="/groups/dataStatus"> Data Entered Status </a></td>
	</tr>
	
	<tr align="left">
		<td width="5%">4)</td>
		<td width="95%"><a href="/groups/getLastComments"> Last Comments by Admins For Users </a></td>
	</tr>
	
	<tr align="left">
		<td width="5%">5)</td>
		<td width="95%"><a href="/retailers/getLastComments"> Last Comments by Admins For Retailers </a></td>
	</tr>
	
	<tr align="left">
		<td width="5%">6)</td>
		<td width="95%"><a href="/retailers"> Retailer Stats </a></td>
	</tr>
	
	<tr align="left">
		<td width="5%">7)</td>
		<td width="95%"><a href="/groups/allRecentMsgs/20"> Messages sent for a package (Cricket Live Score) </a></td>
	</tr>
	
	<tr align="left">
		<td width="5%">8)</td>
		<td width="95%"><a href="/logs/coolVideoStat"> Cool Video Download Stat </a></td>
	</tr>
	
	<tr align="left">
		<td width="5%">9)</td>
		<td width="95%"><a href="/retailers/getCouponInfo"> Coupon Status </a></td>
	</tr>	
	
	<tr align="left">
		<td width="5%">10)</td>
		<td width="95%"><a href="/retailers/reports/<?php echo date("Y-m-d"); ?>"> Retail Daily Reports (<?php echo date("Y-m-d"); ?>) </a></td>
	</tr>		

	<tr align="left">
		<td width="5%">11)</td>
		<td width="95%"><a href="/tags/getTaggedUsers/OSS"> User Search by Tags </a></td>
	</tr>
	<tr align="left">
		<td width="5%">12)</td>
		<td width="95%"><a href="/promotions/userDelivery"> User Delivery </a></td>
	</tr>
	<tr align="left">
		<td width="5%">13)</td>
		<td width="95%"><a href="/retailers/pushPlaywinResult"> Playwin manual push</a></td>
	</tr>
	<tr align="left">
		<td width="5%">14)</td>
		<td width="95%"><a href="/promotions/followup"> Follow-ups</a></td>
	</tr>
</table>
</div>