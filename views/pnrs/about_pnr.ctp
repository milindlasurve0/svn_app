<?php if(isset($_SESSION['Auth']['User'])){ ?>
<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<?php echo $ajax->link( 
    				'<< Back to Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?>
		</span>
	</div>
	<span>
		<?php echo $ajax->link( 
    				'Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?> 
	</span> &nbsp;&gt;&gt;&nbsp; <span>About PNR</span>
</div>
<?php } ?>
<div class="appColRight1" style="font-size:0.9em;">
	<!-- No -->
	<div class="rightColSpace">
      	<div style="position: relative;" id="recentMsgs">
      	  <div style="" class="antina"></div>        
          <div class="SampJokesTitle">SAMPLE MESSAGES(SMS)</div>
          <div style="height: 415px;" id="">
            <div class="container">
            <input type="hidden" id="msgNum" value="1"/>
              <ul style="overflow: hidden; height: 345px;" class="recentMsgsCont">
              	<li style="" id="sampSMS1" class="sampleSMS">
                  <p>*PNR ALERT*<br/>
					PNR Number: 2438599843<br/>
					Title: Mumbai-Goa 12th Dec<br/>
					PNR Status on 12th Dec 2010<br/>
					#Passenger 1 : S2, 31<br/>
					#Passenger 2 : S2, 34<br/>
					#Passenger 3 : S2, 35<br/>
					#Passenger 4 : S2, 38</p>
                </li>
              	<li style="" id="sampSMS2" class="sampleSMS">
                  <p>*PNR ALERT*<br/>
					PNR Number: 2438599843<br/>
					Title: Mumbai-Goa 12th Dec<br/>
					PNR Status on 12th Dec 2010<br/>
					#Passenger 1 : CNF<br/>
					#Passenger 2 : CNF<br/>
					#Passenger 3 : CNF<br/>
					#Passenger 4 : CNF</p>
                </li>
                <li style="" id="sampSMS3" class="sampleSMS">
                  <p>*PNR ALERT*<br/>
					PNR Number: 2438599843<br/>
					Title: Mumbai-Goa 12th Dec<br/>
					PNR Status on 10th Dec 2010<br/>
					#Passenger 1 : W/L 5<br/>
					#Passenger 2 : W/L 6<br/>
					#Passenger 3 : W/L 7<br/>
					#Passenger 4 : W/L 8</p>
                </li>
                <li style="" id="sampSMS4" class="sampleSMS">
                  <p>*PNR ALERT*<br/>
					PNR Number: 2438599843<br/>
					Title: Mumbai-Goa 12th Dec<br/>
					PNR Status on 9th Dec 2010<br/>
					#Passenger 1 : W/L 8<br/>
					#Passenger 2 : W/L 9<br/>
					#Passenger 3 : W/L 10<br/>
					#Passenger 4 : W/L 11</p>
                 </li>
              </ul>
              <div style="padding: 10px 0px; margin-left: 40%;">
                <ul class="sampleNo">                  
                  <li><a onclick="showSample('pre')" href="javascript:void(0)">&lt;</a>                  
                  </li><li><a onclick="showSample('next')" href="javascript:void(0)">&gt;</a></li>
                </ul>
                <br class="clearLeft">
              </div>
              </div>
          </div>
		</div>
    </div>
	<!-- no -->
</div>
<div class="appColLeft1 info" id="appReminderAddDiv" style=" padding-left: 10px;">
    <div class="title6"><span class="fntSz19 strng"><?php echo $data['SMSApp']['name']; ?></span> - Keep track of your PNR Status</div>
	<div class="priceBox">
		<div class="appSmallBox1 appImg1">
			<div class="fntSz15 strng"><em>Get PNR Updates on SMS for your waitlisted/RAC tickets.</em></div>
			<div style="margin:10px 5px 10px 0;"><span class="fntSz15 strng">Price:</span> <?php echo $objGeneral->getPrice($data['SMSApp']['basic_price']); ?> <?php echo $data['SMSApp']['price_tag']; ?></div>
			<input type="button" class="otherSprite oSPos26" onclick="showApp('<?php echo $data['SMSApp']['controller_name']; ?>');">
		</div>		
	</div>
	<div>
	<span class="strng">How it works</span>
	<ol class="info">
		<li>Submit your PNR number of the ticket you wish to track.<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg1" onclick="showApp('<?php echo $data['SMSApp']['controller_name']; ?>');" style="cursor:pointer" border="0"></div></li>
		<!-- <li>Your SMSTadka account needs to have a balance of at least Rs. 7 to create a PNR alert.</li> -->
		<li>After creating a successful PNR alert, you will receive one SMS Daily for the latest PNR status of the ticket.</li>
        <li>You will receive an SMS every 3 hrs, prior to 2 days before the journey.</li>
	</ol>
	</div>
	<div>
	<span class="strng">Benefits</span>
	<ol class="info">
		<li>No need to call and check PNR status again and again.</li>
		<li>139 service of Railways costs Rs. 3 per SMS, which is expensive.</li>
		<li>Auto alert service; do not require PNR number handy every time to track the status. Just submit once and your PNR status will reach you daily.</li>
        <li>Very easy to track multiple tickets.<div class="abtImg"><img src="/img/spacer.gif" class="aboutImg2"></div></li>
	</ol>
	</div>
	<div class="rowDividerIn">
		So get started, <input type="button" class="otherSprite oSPos26" onclick="showApp('<?php echo $data['SMSApp']['controller_name']; ?>');">
	</div>
	<br>
</div>
<div class="clearRight">&nbsp;</div>