<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<?php echo $ajax->link( 
    				'<< Back to Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?>
		</span>
	</div>
	<span> 
		<?php echo $ajax->link( 
    				'Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?> 
	</span> &nbsp;&gt;&gt;&nbsp; <span>My Alerts</span>
</div>
  <!-- Right column of content -->
  <div style="padding-left: 10px; font-size: 1.1em;">
    <div class="appTitle">My Subscribed PNR Alerts</div>
    <div>
     <?php if(empty($data)) { ?>
     Your subscribed pnr alerts will come here. You can come anytime and check the status of your PNR from here as well
     <?php } else { ?>
      <table cellspacing="0" cellpadding="0" border="0" width="100%" class="dataTable1">
        <thead>
          <tr>
            <th style="width: 200px;">PNR Title</th>
            <th style="width: 110px;">PNR Number</th>
            <th style="width: 110px;">Mobile No.</th>
            <th style="width: 150px;">Alert Status</th>
            <th style="width: 180px;">&nbsp;</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        <?php $i = 0; foreach($data as $pnr) {
        	$i++;
        	$class="bg1";
        	if($i%2 == 1) $class = "bg";
        ?>
          <tr class="<?php echo $class; ?>">
            <td><?php echo $pnr['Pnr']['title'];?></td>
            <td><?php echo $pnr['Pnr']['pnr_number'];?></td>
            <td><?php if(empty($pnr['Pnr']['mobile'])) echo $_SESSION['Auth']['User']['mobile']; else echo $pnr['Pnr']['mobile'];?></td>
            <td id="flag<?php echo $pnr['Pnr']['id']; ?>"><?php if($pnr['Pnr']['status_flag'] == '0') echo 'Inactive'; else echo 'Active';?></td>
            <?php if($pnr['Pnr']['status_flag'] == '0') {?>
            <td><?php echo ucwords(strtolower($pnr['Pnr']['chart_status'])); ?></td>
            <td>&nbsp;</td>
            <?php } else {?>
            <td id="chart<?php echo $pnr['Pnr']['id']; ?>"><a href="javascript:void(0);" onclick="checkPNRStatus(<?php echo $pnr['Pnr']['id'];?>);">Check Status</a></td>
            <td id="action<?php echo $pnr['Pnr']['id']; ?>"><a href="javascript:void(0);" onclick="removePNRAlert(<?php echo $pnr['Pnr']['id'];?>,0);">Remove</a></td>
         	<?php } ?> 
          </tr>
        
          <tr class="nobg" ><td id="status<?php echo $pnr['Pnr']['id']; ?>" colspan="5"></td>
          </tr>
        <?php } ?>   
        </tbody>
      </table>
      <?php } ?>
    </div>
  </div>
  <div class="clearRight">&nbsp;</div>