<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<?php echo $ajax->link( 
    				'<< Back to Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?>
		</span>
	</div>
	<span>
		<?php echo $ajax->link( 
    				'Personal Alerts', 
    				array('controller' => 'apps' ,'action' => 'getApps' ), 
    				array( 'update' => 'pageContent')
					); ?> 
	</span> &nbsp;&gt;&gt;&nbsp; <span><?php echo $data['SMSApp']['name']; ?></span>
</div>
  <!-- Right column of content -->
  <div class="rightCol5 rightFloat">
    <div class="appBoxTitle"><?php echo $data['SMSApp']['name']; ?></div>
    <div style="padding-top:15px;"><img src="/img/spacer.gif" class="appImages appImg<?php echo $data['SMSApp']['id']; ?> leftFloat" style="margin-right:10px;" />
      <div style="font-size:1.2em;">Per PNR Alert<br />
        <span><img src="/img/rs.gif" class="rupee1"></span><?php echo $data['SMSApp']['basic_price']; ?></div>
    </div>
    <div class="clearLeft">&nbsp;</div>
    <div class="appBoxCont">
    <ul class="appBoxPoint">
    <?php $desc = explode("\n",$data['SMSApp']['description']); 
    	foreach($desc as $post) {
    ?>
        <li><?php echo $post; ?></li>
    <?php } ?>      
	</ul>
    </div>
  </div>
  <div id="pnrData" class="leftCol5" style="padding-left:10px;">
     <div class="appColLeftBox">
      			<div class="appTitle">Create PNR Alert</div>
      <div class="field" style="padding-top:10px;">
        <fieldset>
        <div class="fieldLabel1 leftFloat">
          <label for="pnrNumber">Enter PNR Number:</label>
        </div>
        <div class="leftFloat" style="margin-right:15px;width:210px;">
          <input type="text" name="" id="pnrNumber" style="width:200px;">
          <span id="err_eg"  class="hints">e.g. 10 digit No. on ticket 2438599843</span><span style="display:none;" class="inlineErr1" id="err_pname">Please enter a valid PNR Number</span> </div>
        <div id="sendButt" class="leftFloat"> <input type="image" onclick="sendPNR();" class="otherSprite oSPos7" src="/img/spacer.gif"> </div>
        <div class="clearLeft">&nbsp;</div>
        </fieldset>
      </div>
   </div> 
   </div>
   
   <script>
   $('pnrNumber').focus();
   </script>