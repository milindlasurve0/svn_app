<div class="loginCont">
  <div>
    <div class="leftFloat dashboardPack" style="margin-right: 5px;">
      <div class="catList">
        <ul id='innerul'>
           <?php 
           		$class1 = 'sel';
           		$class2 = '';
           		if($about == 1){
           			$class1 = '';
           			$class2 = 'sel';
           		}
           ?>
           <li name='innerli'><?php echo $ajax->link( 
    				'Create PNR Alert ', 
    				array('action' => 'createAlert'),
    				array('id' => 'createAlert', 'class' => "$class1" ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("createAlert").removeClassName("loader")' )
					); ?> </li>
		  <li name='innerli'><?php echo $ajax->link( 
    				'My Alerts ', 
    				array('action' => 'myAlerts'),
    				array('id' => 'myAlert', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("myAlert").removeClassName("loader")' )
					); ?> </li>
		  <li name='innerli'><?php echo $ajax->link( 
    				"About PNR Alerts ", 
    				array('action' => 'about'),
    				array('id' => 'about', 'class' => "$class2" ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("about").removeClassName("loader")' )
					); ?> </li>					
		  <li name='innerli'><?php echo $ajax->link( 
    				"FAQs ", 
    				array('action' => 'faqs'),
    				array('id' => 'faqs', 'class' => '' ,'update' => 'innerDiv','onclick' => 'changeInnerTabClass(this);','complete' => '$("faqs").removeClassName("loader")' )
					); ?> </li>
		  <li class="hList" name='innerli'>
           		<?php echo $ajax->link( 
    				'Personal Alerts Home ', 
    				array('controller' => 'apps','action' => 'getApps'),
    				array('id' => 'allApps', 'class' => '' ,'update' => 'pageContent','onclick' => 'changeInnerTabClass(this);','complete' => '$("allApps").removeClassName("loader")' )
				); ?>		
		  </li>				
        </ul>
      </div>
    </div>
    
    <div style="margin-left: 210px;" id="innerDiv">
    	<div class="ie6Fix2 inlineBlockElement">
      	<?php
      		if($about == 1)	{
      			echo "<script>$('about').simulate('click');</script>";
      		}
      		else {
      			echo "<script>$('createAlert').simulate('click');</script>";
      		}
      	?>
        </div>
    </div>
    <br class="clearLeft" />
 </div>