<div class="appColLeftBox">	<div class="appTitle">Your PNR Status for <?php echo $pnrInfo['pnr_number']; ?></div>
   <?php if(isset($pnrInfo['error']) && ($pnrInfo['error'] == '0' || $pnrInfo['error'] == '1')) {?>
   		<div style="margin-top:10px;">
    		<span id="chart_status">PNR Status is not available from the Railway Server. It might be a case that railway servers are not responding at this moment.<br/>Please try between morning 8 AM to 11 PM</span>
    	</div>
   <?php } else { ?>
    	<table cellspacing="0" cellpadding="0" border="0" class="dataTable1">
        	<thead>
            	<tr>
            		<th style="width: 60px;">Train No.</th>
                    <th style="width: 130px;">Train Name</th>
                    <th style="width: 100px;">Journey Date</th>
                    <th style="width: 90px;">From - To</th>
                    <th>Class</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
            		<td><?php echo $pnrInfo['train_number']; ?></td>
            		<td><?php echo $pnrInfo['train_name']; ?></td>
                    <td id="journey_date"><?php echo $pnrInfo['journey_date']; ?></td>
                    <td><?php echo $pnrInfo['from'] . " - " . $pnrInfo['to']; ?></td>
                    <td><?php echo $pnrInfo['class'];?></td>
                </tr>
            </tbody>

        </table>
        
        <table cellspacing="0" cellpadding="0" border="0" class="dataTable1">
        	<thead>
            	<tr>
            		<th style="width: 100px;">Passengers</th>
                    <th style="width: 130px;">Booking Status</th>
                    <th style="width: 100px;">Current Status</th>
                    
                </tr>
            </thead>
            
            <tbody>
            <?php $num = $pnrInfo['num_passengers']; 
					for($i = 0; $i < $num; $i++) {?>
				<tr>
					<td><?php echo ($i+1);?></td>  
					<td><?php echo $pnrInfo['passengers'][$i]['booking_status']; ?> </td>
					<td><?php echo $pnrInfo['passengers'][$i]['current_status']; ?> </td>
				</tr>
			<?php } ?>
            </tbody>

        </table>
        <div class="marginTop">
    		<strong style="color:#595959">Ticket Status:</strong> <span id="chart_status"><?php echo ucwords(strtolower($pnrInfo['tkt_status'])); ?></span>
    	</div>
    	<?php } ?>
         <?php if(isset($pnrInfo['error']) && $pnrInfo['error'] != '0' &&  $pnrInfo['error'] != '1' ) {?>
      <div id="notify" style="border-top: 1px solid rgb(186, 186, 186); margin-top: 20px; padding-top: 20px; font-size: 1.4em; font-weight: bold;">
      	 <?php if($pnrInfo['error'] == "3" || $pnrInfo['error'] == "6") {?>
      	 Ticket is already cleared
      	 <?php } else if($pnrInfo['error'] == "4") {?>
      	 Ticket is cancelled
      	 <?php } else if($pnrInfo['error'] == "5") {?>
      	 Chart has been prepared already 
      	 <?php } ?>
      </div>
    <?php } 
        else { ?>
      <div id="notify" style="display:none;border-top: 1px solid rgb(186, 186, 186); margin-top: 20px; padding-top: 20px; font-size: 1.4em; font-weight: bold;">Insufficient funds. <a href="/users/paynow">Click here to recharge</a></div>
      <?php } ?>
    </div>
    </div>
   <?php if(!isset($pnrInfo['error'])) {?>
      
<div class="field appColLeftBox marginTop">
<div class="appTitle">Set a title & proceed</div>
<div style="padding-top: 10px;">
<fieldset>
        <div class="fieldLabel1 leftFloat" >
          <label for="pnrTitle">Your Alert Title:</label>
        </div>
        
        <div style="margin-right:15px;width:210px;" class="leftFloat">
          <input type="text" style="width: 200px;" id="pnrTitle" value="<?php echo substr(ucwords(strtolower($pnrInfo['train_name'])) . " - " . date("dMy",strtotime($pnrInfo['journey_date'])),0,20); ?>">
          <span class="hints">e.g. Chacha Ticket, Kerala Trip </span>
          
         </div>
         <div id="recepient" style="display:none">
	        <div class="fieldLabel1 leftFloat" >
	          <label for="pnrMobile">Mobile Number:</label>
	        </div>
	        
	        <div style="margin-right:15px;width:210px;" class="leftFloat">
	          <input type="text" style="width: 200px;" id="pnrMobile" value="">
	          <span class="hints">e.g. 9819032643</span>
	         </div>
         </div>
         <div id="add_number">
	        <div class="fieldLabel1 leftFloat" >
	          <label>&nbsp;</label>
	        </div>
	        
	            	
	        
	        <!--<div style="margin-right:15px;width:210px;font-size:11px" class="leftFloat" >
	         <br/> <a href="javascript:void(0)" onclick="$('add_number').hide();$('recepient').show();">Set for other mobile number</a><br/><br/>
	         </div>-->
         </div>
        
	       
         <div class="fieldLabel1 leftFloat" >
          <label for="">&nbsp;</label>
        </div>
        <div style="margin-right:15px;width:210px;" id="sendButt" class="leftFloat">
        	<input type="image" onclick="createAlert()" class="otherSprite oSPos7" src="/img/spacer.gif">
        	<br/><span id="err_pname" class="inlineErr1" style="display: none;"></span>
        </div>
        <input type="hidden" id="pnr_number" value="<?php echo $pnrInfo['pnr_number']; ?>">
		<input type="hidden" id="train_info" value="<?php echo $pnrInfo['train_info']; ?>">
		<input type="hidden" id="train_number" value="<?php echo $pnrInfo['train_number']; ?>">
		<input type="hidden" id="boarding" value="<?php echo $pnrInfo['boarding']; ?>">
		<input type="hidden" id="to" value="<?php echo $pnrInfo['to']; ?>">
        
        <div class="clearLeft">&amp;nbdp;</div>
        </fieldset>
        </div>
      </div>   </div>
      <?php } ?>
	  
