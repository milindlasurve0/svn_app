<div style="padding: 0px 20px 20px 10px;">
	<div class="rightFloat"> 
		<span id="back">
			<a onclick=" event.returnValue = false; return false;" id="link1387290908" href="/apps/getApps">&lt;&lt; Back to Personal Alerts</a>		</span>
	</div>
	<span>
		<a onclick=" event.returnValue = false; return false;" id="link1393390118" href="/apps/getApps">Personal Alerts</a> 
	</span> &nbsp;&gt;&gt;&nbsp; <span>FAQs</span>
</div>
<div class="info" style="padding: 0px 20px 20px 10px;">
	<div class="title5">What is PNR Alerts?</div>
	<p>It is a service offered by SMSTadka to get the status of your wait-listed tickets.
You just need to enter the PNR number of your WL/RAC ticket and you will receive SMS updates every day, if there is a change in the status of your ticket.</p>
	<div class="title5">How it is better than 139 service of railways or Google PNR service?</div>
	<p>SMSTadka PNR alerts keeps you updated on regular basis if there is a change in your PNR.<br>
139 service of railway requires you to send a costly SMS ( Rs. 3 - 5 per SMS depending upon your operator ) to get a PNR update. Each time you wish to know your PNR status you need to send an SMS to 139 which will cost you minimum Rs 3.
<br>SMSTadka offers you this service with a onetime cost of just Rs. 15 even if your ticket is due after 3 months.</p>
	
	<div class="title5">How many messages will I get after subscribing to PNR Alerts?</div>
	<p>We will not flood your inbox. You will get one SMS every evening with the latest PNR status if there is a change in the status of your ticket.<br>
Also, very importantly note that you will get update in every 2 hrs, 3 days prior to your journey date to ensure that you can take proper decision.<br>
You will not get any SMS once your ticket is confirmed (Of course you don't need it). You will get SMS alert only if there is a change compared to the previous status (Why bug you with the same info?).</p>

	<div class="title5">How do I pay?</div>
	<p>SMStadka.com works on pre-paid basis. Recharging SMSTadka account is simple and smooth. You can recharge your account using credit card, debit card, net-banking, paymate and a variety of pre-paid cash cards - you can even send us a cheque.<br>
The recharge amount can also be used to use other services of SMStadka.</p>
</div>