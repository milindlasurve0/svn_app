<div style="border:1px solid #595959; padding:0 0 10px 10px">
	<div class="rightFloat" style="border-left:1px solid #595959; border-bottom:1px solid #595959; padding:3px 5px"><a href="javascript:void(0);" onclick="closeStatus(<?php echo $id;?>);">Close X</a></div>
                <table cellspacing="0" cellpadding="0" border="0" width="100%" class="dataTable1">
                  <thead>
                    <tr>
                      <th style="width: 120px;">Train Number</th>
                      <th style="width: 180px;">Train Name</th>
                      <th style="width: 120px;">Journey Date</th>
                      <th style="width: 120px;">From - To</th>
                      <th>Class</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
	            		<td><?php echo $pnrInfo['train_number']; ?></td>
	            		<td><?php echo $pnrInfo['train_name']; ?></td>
	                    <td id="journey_date"><?php echo $pnrInfo['journey_date']; ?></td>
	                    <td><?php echo $pnrInfo['from'] . " - " . $pnrInfo['to']; ?></td>
	                    <td><?php echo $pnrInfo['class'];?></td>
	                </tr>
                  </tbody>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" class="dataTable1">
                  <thead>
                    <tr>
                      <th style="width: 120px;">Passengers</th>
                      <th style="width: 180px;">Booking Status</th>
                      <th>Current Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $num = $pnrInfo['num_passengers']; 
						for($i = 0; $i < $num; $i++) {?>
						<tr>
							<td><?php echo ($i+1);?></td>  
							<td><?php echo $pnrInfo['passengers'][$i]['booking_status']; ?> </td>
							<td><?php echo $pnrInfo['passengers'][$i]['current_status']; ?> </td>
						</tr>
					<?php } ?>
                  </tbody>
                </table>
                <div style="padding-top: 15px;"> <strong>Ticket Status:</strong> <?php echo ucwords(strtolower($pnrInfo['tkt_status'])); ?> </div>
              </div>