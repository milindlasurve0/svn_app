<div class="appColLeftBox">	
	<div class="appTitle">Your PNR Alert Confirmed for <?php echo $pnr_number;?></div>
	<div class="marginTop">
        Your PNR alert <strong><?php echo $pnr_title; ?></strong> is created successfully. </div>
      <div class="marginTop">
        You can find list of all your PNR alerts in <strong>
        <?php echo $ajax->link( 
            'My PNR Alerts', 
            array('controller' => 'pnrs','action' => 'myAlerts'),
            array('update' => 'innerDiv','onclick' => 'changeInnerTabClass($("myAlert"));','complete' => '$("myAlert").removeClassName("loader")' )
        ); ?>
        </strong> or <strong>
            <?php echo $ajax->link( 
                'Create new PNR Alert', 
                array('controller' => 'pnrs', 'action' => 'createAlert'),
                array('update' => 'innerDiv','onclick' => 'changeInnerTabClass($("createAlert"));','complete' => '$("createAlert").removeClassName("loader")' )
                ); ?> 
        </strong> or <strong>
                <?php echo $ajax->link( 
                    'Check out our other apps', 
                    array('controller' => 'apps', 'action' => 'getApps'),
                    array('update' => 'pageContent')
                    ); ?> </strong>
        </div>
        <div class="marginTop">Your current account balance is <span><img class="rupee1" src="images/rs.gif"></span><?php echo $balance; ?> </div>    
</div>

<script> reloadBalance(<?php echo $balance; ?>); </script>
              
               