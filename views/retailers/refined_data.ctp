<?php //echo "<pre>"; print_r($data); echo "</pre>";?>
<script>
function countChars(id){
	var str = $('transl'+id).value;
	$('left'+id).innerHTML=str.length + <?php echo ADSPACE; ?>;
}
</script>
<?php
foreach($data as $message) {
	$strD = trim(strip_tags($message['content']['cyclicData'])); 
	?>
	<form id="form<?php echo $message['content']['id']?>" name="refineMessages<?php echo $message['content']['id']?>"> 
		
	<div id="divRefine<?php echo $message['content']['id']; ?>" class="rightFloat" style="margin-top:50px;">
		<?php echo $ajax->submit(
					'Submit', 
					array('url'=> array('controller' => 'messages','action'=>'addRefinedData',$package_id,$message['content']['id']), 
										'after' => '$("divRefine'.$message['content']['id'].'").innerHTML = "Submitted";', 'update' => 'divRefine'.$message['content']['id'])); ?>
	</div>
	
	<div>
		<div class="leftFloat" style="padding-right:200px;">
		Message id: <?php echo $message['content']['id']; ?>, Cycle: <?php echo $message['cyclic_refined']['cycle']; ?><br/>
		<textarea onkeyup="countChars(<?php echo $message['content']['id']?>);" onkeydown="countChars(<?php echo $message['content']['id']?>);" 
		class="input textarea" id="transl<?php echo $message['content']['id']?>" 
		name="data[Message][content]" style="height: 150px; width: 502px; line-height: 1.5em; 
		font-family: Arial,Helvetica,sans-serif; font-size: 14px; direction: ltr;" autocomplete="off"><?php echo $strD;?></textarea>
		<br>
		
		<small><i>
                  <span>
                  	<span id="left<?php echo $message['content']['id'];?>"> <?php echo (strlen($strD) + ADSPACE);?> </span>
                    </input>&nbsp;characters
                  </span>
                </i></small>
        </div>       
		<div style="padding-top:50px;">
			My Rating: <select name="data[Message][rating]" id="Rate<?php echo $message['content']['id']?>">
							<option <?php if(!empty($message['refineddata']['rate']) && $message['refineddata']['rate'] == '1') echo 'selected="selected"';?> value="1">1</option>
							<option <?php if(!empty($message['refineddata']['rate']) && $message['refineddata']['rate'] == '2') echo 'selected="selected"';?> value="2">2</option>
							<option <?php if(!empty($message['refineddata']['rate']) && $message['refineddata']['rate'] == '3') echo 'selected="selected"';?> value="3">3</option>
							<option <?php if(!empty($message['refineddata']['rate']) && $message['refineddata']['rate'] == '4') echo 'selected="selected"';?> value="4">4</option>
							<option <?php if(!empty($message['refineddata']['rate']) && $message['refineddata']['rate'] == '5') echo 'selected="selected"';?> value="5">5</option>
			</select>				
		 </div>
		 <div class="clearLeft"></div>
		</div>
		
	<div class="clearBoth"> </div>
	</form>
		
	<hr><br>
<?php } ?>