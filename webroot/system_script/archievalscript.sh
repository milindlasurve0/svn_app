#!/bin/bash
#DATE=`date --date='2 days ago' +%Y%m%d`
DATE=`date --date='yesterday' +%Y%m%d`
cd /var/log/apps
filelist=`ls *"$DATE.log"`

for i in $filelist
do
    tar -zcvf  $i".tar.gz"  $i &&
    #mv $i /tmp/testdir/. 
    #mv $i /tmp/tempdir/. 
    echo "archived $i"
    echo "removing $i"
    rm -f $i
done
cd -
