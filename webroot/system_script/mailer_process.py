import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import os
import redis
import logging
import time
import sys
import json
import time

REDIS_HOST = 'redis1.oq14zy.0001.use1.cache.amazonaws.com'
REDIS_PORT = 6300
REDIS_PASSWORD = ''


def redis_connect(logger):
    global REDIS_HOST
    global REDIS_PORT
    global REDIS_PASSWORD
    try:
      rediscon = redis.Redis(REDIS_HOST,REDIS_PORT,password=REDIS_PASSWORD)
    except:
      if logger is not None:
        logger.info("Unexpected error: redis_connect : ", sys.exc_info()[0])
      return False
    return rediscon


def create_logger(logfilename):
    logger = logging.getLogger(logfilename + '_')
    hdlr = logging.FileHandler('/var/log/apps/' + logfilename + '.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    return logger


def sendMail(to, fro, subject, text, files=[],server="localhost"):
    assert type(to)==list
    assert type(files)==list

    msg = MIMEMultipart()
    msg['From'] = fro
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text,'html') )

    for file in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"'
                       % os.path.basename(file))
        msg.attach(part)

    smtp = smtplib.SMTP(server)
    smtp.sendmail(fro, to, msg.as_string() )
    smtp.close()


def mailManager():
    try:
        robj = redis_connect(None)
        logger = create_logger('maillog_via_py')
        
        #print " redis obj : " + str(robj)
        while(1):
            try:
                if robj is False :
                    robj = redis_connect(None)
                    time.sleep(5)
                    logger.info("issue in redis OBJ : " + str(robj) )
                    continue
                
                to_ids = ""
                from_id = "admin <admin@smstadka.com>"
                em_data = robj.brpop('smstadka-mail')
                #print "email data from redis : " + str(em_data)
                if em_data is None or em_data[1] is None:
                    continue

                emaildata = json.loads(em_data[1])
                
                logger.info("json data converted to array")
                if 'emails' in emaildata.keys() :
                    to_ids = emaildata['emails'].encode("utf-8").split(',')
		else:
                    to_ids = ['ashish@mindsarray.com','chirutha@mindsarray.com','anurag@mindsarray.com','vinit@mindsarray.com']
		if 'sender_id' in emaildata.keys() :
                    from_id = emaildata['sender_id'].encode("utf-8")

                if 'tadka@mindsarray.com' in to_ids:
                    to_ids.remove('tadka@mindsarray.com')
                    to_ids.extend(['ashish@mindsarray.com','chirutha@mindsarray.com'])

                
                subject = emaildata['mail_subject'].encode("utf-8")
                mail_body = emaildata['mail_body'].encode("utf-8")

                logger.info("sending mail ::  to :" + str(to_ids) + " | from : " + str(from_id) + " | subject : " + str(subject) + " | Body : " + str(mail_body) )
                resp = sendMail(to_ids,from_id,subject,mail_body)
                logger.info("mail send successfully")
            except:
                logger.info("Unexpected error inside while : mail manager :" + str(sys.exc_info()[0]))
                pass
            time.sleep(0.01)
    except:
        logger.info("Unexpected error outside : mail manager :" + str(sys.exc_info()[0]))
        pass


if __name__ == '__main__':
    mailManager()

