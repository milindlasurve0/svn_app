#!/bin/bash

pcount=1
while [ $pcount -ne 0 ]
do
    processlist=`ps -ef | egrep "pay1_new|smsProcess|sendMsgMail|sendMails|resultSendMessageVia247SMS|resultSendMessageViaTata" | egrep -v "grep|color" | awk '{print $2}'`
    for i in $processlist
    do       
       `kill -9 $i &`
    done
    pcount=`ps -ef | egrep "pay1_new|smsProcess|sendMsgMail|sendMails|resultSendMessageVia247SMS|resultSendMessageViaTata" | egrep -v "grep|color" | awk '{print $2}' | wc -w`
done
