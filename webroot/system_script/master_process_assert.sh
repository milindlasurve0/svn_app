#!/bin/bash
smsprocess_path="../apis/smsProcess.php"
smsprocess_count=`ps -ef |  grep "smsProcess.php" | egrep -v "color|grep" | wc -l`
pay1process_count=`ps -ef |  grep "pay1_new.php" | egrep -v "color|grep" | wc -l`

sendsmsmail=`ps -ef | grep "sendMsgMail" | egrep -v "color|grep" | wc -l`
smsSendvia247sms=`ps -ef | grep "resultSendMessageVia247SMS" | egrep -v "color|grep" | wc -l`

echo "Total process of smsprocess = $smsprocess_count"
req=1

if [ $smsprocess_count -lt $req ];
  then
      #echo "test mail "|mail -s "shell test" nandan@mindsarray.com
      cd ../apis
      while [ $smsprocess_count -lt $req ]
      do
         /usr/bin/php smsProcess.php &
         ((smsprocess_count += 1))
      done
      cd -
fi

if [ $pay1process_count -lt $req ];
        then
        cd ../apis
        while [ $pay1process_count -lt $req ]
        do
            /usr/bin/php pay1_new.php &
            ((pay1process_count += 1))
        done
        cd -
fi

if [ $sendsmsmail -lt $req ];
        then
        sh sendMsgMail.sh &
fi

if [ $smsSendvia247sms -lt $req ];
        then
        sh sendMsgMailvia247sms.sh &
fi


