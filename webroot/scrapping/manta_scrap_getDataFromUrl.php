<?php

set_time_limit(0);
ini_set("memory_limit","-1");
ini_set( "display_errors", 0);
include("simple_html_dom.php");

class WebScrap
{
	private $url;
	private $xpath;

	public function WebScrap($url,$xpath)
	{
		$this->url = $url;
		$this->xpath = $xpath;
	}

	public function GetScrap()
	{
		// use Tidy to try to make the page well formed
		//$page = $this->TidyIt($this->url);
		$userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';

		// make the cURL request to $target_url
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($ch, CURLOPT_URL,$this->url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$page= curl_exec($ch);
		
		if($page)
		{
			// create a document out of the well formed content
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);
			
			$this->getEachScrap($domDocument);
			curl_close($ch);
			return '1';		
		}
		else
		{	curl_close($ch);
			return '999';
		}
	}
	
	private function getEachScrap($domDocument)
	{
		$domXPath = new DOMXPath($domDocument);		
		$domNodeList = $domXPath->query($this->xpath);
		
		$content = $this->GetHTMLFromNodeList($domNodeList);
		
		return $content;
	}

	
	private function saveData($comp_name,$comp_addr,$person_name,$email_id,$contact)
	{
		$sql1 = "update manta_data set comp_name='".addslashes($comp_name)."', comp_addr='".addslashes($comp_addr)."', person_name='".addslashes($person_name)."', email_id='".addslashes($email_id)."', contact='$contact' where comp_url = '" . $this->url."'";
		$result1 = mysql_query($sql1);
		
		if(!$result1)
		{
			echo "inner query failed";
			echo $sql1;
		}
	}

	private function TidyIt($url)
	{
		$tidy = new tidy();
		$tidy->parseFile($url);
		$tidy->cleanRepair();
		return $tidy;
	}

	private function GetHTMLFromNodeList($domNodeList)
	{
		$html = '';
		foreach($domNodeList as $node){
			
			$data = '';
			foreach($node->childNodes as $childNode){
				$domDocument1 = new DOMDocument();
				$domDocument1->appendChild($domDocument1->importNode($childNode, true));
				
				$data = $data . $domDocument1->saveHTML();
			}
			
			$html1 = str_get_html($data);
			
			$title = $html1->find('a.company-name h1',0)->innertext; //to enter
			$address = $html1->find('div.adr',0);
			$street_addr = $address->find('div.street-address',0)->innertext;
			$locality = $address->find('span.locality',0)->innertext;
			$region = $address->find('span.region',0)->innertext;
			$postal_code = $address->find('span.postal-code',0)->innertext;
			
			$address = $street_addr . ", " . $locality . ", " . $region . ", " . $postal_code;  //to enter
			
			//$email = $html1->find('p.email script',0)->innertext;
			
			//$email = str_replace(array(';','wr("','")'), '', $email);
			$telephone1 = $html1->find('p.telephone',0)->innertext; // to enter
			
			
			$html2 = file_get_html($this->url);
			
			$contact_p = $html2->find('ul.items li div.vcard a h6',0)->innertext; //to enter
			$telephone = $html2->find('ul.items li div.vcard p.tel',0)->innertext; //to enter
			$email = $html2->find('ul.items li div.vcard p.email script',0)->innertext;
			
			$email = str_replace(array(';','wr("','")'), '', $email);
			if(trim($telephone) == '')
			  $telephone = $telephone1;
			  
			echo "<br><br>Business Name: " . $title;
			echo "<br>Contact Person: " . $contact_p;
			echo "<br>Email Id: " . $email;
			echo "<br>Address: " . $address;
			echo "<br>Contact Number: " . $telephone;
			$this->saveData($title,$address,$contact_p,$email,$telephone);
			
			$html .= $data;
		}
		
		return $html;
	
	}

}


$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';

$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die  ('Error connecting to mysql');

$dbname = 'manta_new';
mysql_select_db($dbname);

$result = mysql_query("select distinct comp_url from manta_data where email_id is null");
$i = 0;
while($row = mysql_fetch_array($result)){
	$url = $row['comp_url'];
	$xPath = "//div[contains(concat(' ',normalize-space(@class),' '),' company-info vcard ')]";

	$scrap = new WebScrap($url,$xPath);
	$data = $scrap->GetScrap();
	flush();
	/*if($i == 10)
		break;
		
	$i++;*/	
}

?>