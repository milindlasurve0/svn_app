<?php

set_time_limit(0);
ini_set("memory_limit","-1");

class WebScrap
{
	private $url;
	private $xpath;
	
	public function WebScrap($url,$xpath)
		{
		$this->url = $url;
		$this->xpath = $xpath;
		}
	
	public function GetScrap()
		{
		// use Tidy to try to make the page well formed
		//$page = $this->TidyIt($this->url);
		$userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		
		// make the cURL request to $target_url
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($ch, CURLOPT_URL,$this->url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$page= curl_exec($ch);
		
		if($page)
		{
			// create a document out of the well formed content
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);
			
			$cnt = $this->getEachScrap($domDocument);
			curl_close($ch);
			return $cnt;
		}
		else
		{
			curl_close($ch);
			return '999';
		}
		}
	
	private function getEachScrap($domDocument)
		{
		$domXPath = new DOMXPath($domDocument);		
		$domNodeList = $domXPath->query($this->xpath);
		
		$content = $this->GetHTMLFromNodeList($domNodeList);
		
		return $content;
		}	
	
	private function saveData($comp_url,$web_url)
		{
		
		$sql1 = "update manta_data set web_url = '$web_url' where comp_url = '$comp_url')";
		$result1 = mysql_query($sql1);
		
		if(!$result1)
		{
			echo "inner query failed";
			echo $sql1;
		}
		}
	
	private function TidyIt($url)
		{
		$tidy = new tidy();
		$tidy->parseFile($url);
		$tidy->cleanRepair();
		return $tidy;
		}
	
	private function GetHTMLFromNodeList($domNodeList)
		{
		$html = '';
		foreach($domNodeList as $node){
			
			$data = '';
			foreach($node->childNodes as $childNode){
				$domDocument1 = new DOMDocument();
				$domDocument1->appendChild($domDocument1->importNode($childNode, true));		
				$data = $data . $domDocument1->saveHTML();
			}
			$this->saveData($this->url,$data);
			$html .= $data;
		}
		
		return $html;
		
		}
}


$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';

$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to mysql');

$dbname = 'manta_new';
mysql_select_db($dbname);

$xPath = "//div[contains(concat(' ',normalize-space(@class),' '),' company-info vcard ')]//p/a/@href";
$result = mysql_query("select comp_url from manta_data where id <= 10000 and web_url is null");
$i = 0;
while($row = mysql_fetch_array($result)){
	$url = $row['comp_url'];
	$scrap = new WebScrap($url,$xPath);
	$data = $scrap->GetScrap();
	if($i == 5) break;
	$i++;
}


//echo $data;

?>