<?php

set_time_limit(0);
ini_set("memory_limit","-1");

class WebScrap
{
	private $url;
	private $xpath;
	private $id;
	
	public function WebScrap($id,$url,$xpath)
		{
		$this->url = $url;
		$this->id = $id;
		$this->xpath = $xpath;
		}
	
	public function GetScrap()
		{
		// use Tidy to try to make the page well formed
		//$page = $this->TidyIt($this->url);
		$userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		
		// make the cURL request to $target_url
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($ch, CURLOPT_URL,$this->url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$page= curl_exec($ch);
		
		if($page)
		{
			// create a document out of the well formed content
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);
			
			$cnt = $this->getEachScrap($domDocument);
			curl_close($ch);
			return $cnt;
		}
		else
		{
			curl_close($ch);
			return '999';
		}
		}
	
	private function getEachScrap($domDocument)
		{
		$domXPath = new DOMXPath($domDocument);		
		$domNodeList = $domXPath->query($this->xpath);
		
		$content = $this->GetHTMLFromNodeList($domNodeList);
		
		return $content;
		}	
	
	private function saveData($comp_url)
		{
		
		$sql1 = "INSERT INTO manta_data(caturl_id, comp_url) VALUES (".$this->id.",'".$comp_url."')";
		$result1 = mysql_query($sql1);
		
		if(!$result1)
		{
			echo "inner query failed";
			echo $sql1;
		}
		}
	
	private function TidyIt($url)
		{
		$tidy = new tidy();
		$tidy->parseFile($url);
		$tidy->cleanRepair();
		return $tidy;
		}
	
	private function GetHTMLFromNodeList($domNodeList)
		{
		$html = '';
		foreach($domNodeList as $node){
			
			$data = '';
			foreach($node->childNodes as $childNode){
				$domDocument1 = new DOMDocument();
				$domDocument1->appendChild($domDocument1->importNode($childNode, true));
				
				$data = $data . $domDocument1->saveHTML();
			}
			$this->saveData($data);
			$html .= $data;
		}
		
		return $html;
		
		}
}


$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';

$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to mysql');

$dbname = 'manta_new';
mysql_select_db($dbname);

$result = mysql_query("select * from manta_urls where id > 255");
while($row = mysql_fetch_array($result)){
	$i = 1;
	$xPath = "//div[contains(concat(' ',normalize-space(@class),' '),' fn org ')]/h2/a/@href";

	while(true){
		$url = $row['cat_url'] . "&pg=".$i;
		$scrap = new WebScrap($row['id'],$url,$xPath);
		$data = $scrap->GetScrap();
		
		if(trim($data) == '' || trim($data) == '999') {
			break;
		}
		$i++;
	}
	//$j++;
	//break;
}


//echo $data;

?>