<?php 
define('DB_HOST','localhost'); 
define('DB_USER','root');
define('DB_PASS','');
define('DB_DB','smsmodem');

define('MAX_SMS_LIMIT',200);

define('STATE_READY',0);
define('STATE_BUSY',1);
define('STATE_STOP',2);

define('SMS_SENDING',2);
define('SMS_SENT',1);
define('SMS_TOBESEND',0);

define('INCOMING_NUMS',"'7666888676','7303897886','9619972172','9681087993'");

//define('SERVER_BACKUP','http://107.22.174.199/');
define('SERVER_BACKUP','http://www.smstadka.com/');
define('DOCUMENT_ROOT','/home/ashish/workspace1/smstadka/app/webroot/');

$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die ('Error connecting to mysql');
mysql_select_db(DB_DB);
mysql_query("set time_zone = '+5:30'");

define('SPECIAL_CHARS','@,$,_,^,{,},\,[,~,],|');

function getNumberofSMS($message){
	 $num = numberOfChars($message);
	 if($num <= 160){
	 	return 1;
	 }
	 else {
	 	return ceil($num/153);
	 }
}

function numberOfChars($message){
	$special_2chars = array("^","{","}","\\","[","~","]","|");
	$length = strlen( $message );
    $i = 1;
    $num = 0;
	while ($i <= $length) {
		// Convert this character to a 7 bits value and insert it into the array
		$char = substr( $message ,$i-1,1);
		if(!in_array($char,$special_2chars)){
            $num++;
		}
		else {
            $num = $num + 2;
		}
        $i++;
	}
	return $num;
}

function splitMessage($message,$split){
	$special_2chars = array("^","{","}","\\","[","~","]","|");
	$length = strlen( $message );
	$messages = array();
    $i = 1;
    $num = 0;
    $msg = '';
	while ($i <= $length) {
		// Convert this character to a 7 bits value and insert it into the array
		$char = substr( $message ,$i-1,1);
		if(!in_array($char,$special_2chars)){
            $num++;
		}
		else {
            $num = $num + 2;
		}
		//echo $num . "<br/>";
		if($num == $split || $num == ($split + 1)){
			if($num == $split){
				$messages[] = $msg.$char;
				$num = 0;
				$msg = '';
			}
			else {
				$messages[] = $msg;
				$num = 2;
				$msg = $char;
			}
		}
		else {
			$msg .= $char;	
		}
		$i++;
	}
	if($msg != ''){
		$messages[] = $msg;
	}
	return $messages;
}

function curl_post_async($url, $params=null)
{
    foreach ($params as $key => &$val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}

function matchTemplate($sms, $template){
	$explode = explode('@__123__@',$template);
	
	$vars = array();
	$ret = true;
	$i = 0;
	$start = 0;
	$log = "";
	foreach($explode as $exp){
		$log .= "Checking $exp";
		if(!empty($exp)){
			$index = strpos($sms,$exp);
			$log .= ": $index\n";
			if($index === false){
				$ret = false;
				break;
			}
			else {
				if($i != 0){
					$var = substr($sms,0,$index);
					$vars[] = trim($var);
				}
				$sms = substr($sms,$index+strlen($exp));
			}
		}
		else if($i != 0){
			$vars[] = $sms;
		}
		$i++;
	}
	
	if($ret){
		$out['status'] = 'success';
		$out['vars'] = $vars;
	}
	else {
		$out['status'] = 'failure';
		$out['vars'] = $vars;
	}
	$out['sms'] = $sms;
	$out['logs'] = $log;
	return $out;
}
?>