<?php 
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
include "communicator.php";

class Modem{
	private $stateProgram;
	
	function initializeModem($newday=null){
		$comm = new communicator();
		mysql_query("UPDATE sim_details SET active_flag = 0,device_num = 0");
		
		$ports = $comm->findUSBDevices();
		foreach($ports as $key => $val){
			$i = $key+1;
		//for($i = 1;$i<=10;$i++){
			$scid = $comm->getSCID($i);
			if(!empty($scid)){
				$result = mysql_query("SELECT * FROM sim_details WHERE scid='$scid'");
				$rows = mysql_num_rows($result);
				
				if($rows > 0){
					$arr = mysql_fetch_array($result);
					/*if($arr['sms_flag'] == 0){
						$ret = $comm->emptyInbox($i);
						if($ret == 1){
							$ret = $comm->emptyInbox($i);
						}
					}*/
					mysql_query("UPDATE sim_details SET device_num=$i, active_flag=1, state=".STATE_READY." WHERE scid='$scid'") or die ('Error in updation');
				}
				else {
					mysql_query("INSERT INTO sim_details (scid,sms_left_day,device_num,active_flag) VALUES ('$scid',".MAX_SMS_LIMIT.",$i,1)");
				}
			}
		}
		
		if($newday != null){
			mysql_query("UPDATE sim_details SET sms_left_day = ".MAX_SMS_LIMIT);
		}
		
	}
	
	function setSIMStatus($status,$device){
		mysql_query("UPDATE sim_details SET state = $status WHERE device_num = $device");
	}
	
	function setSMSStatus($status,$message_id){
		$query = "UPDATE messages SET sent_flag = $status WHERE id = $message_id";
		if($status == SMS_SENDING){
			$query = "UPDATE messages SET sent_flag = $status,trials=trials+1 WHERE id = $message_id";
		}
		mysql_query($query);
	}
	
	function updateSMSCount($count,$device){
		mysql_query("UPDATE sim_details SET sms_left_day = sms_left_day - $count,total_sms_gone = total_sms_gone + $count WHERE device_num = $device");
	}
	
	function getSMSLeft($device){
		$result = mysql_query("SELECT sms_left_day FROM sim_details WHERE device_num=$device");	
		$rows = mysql_fetch_array($result);
		return $rows['sms_left_day'];
	}
	
	function updateMessageReport($message_id,$device,$status){
		$result = mysql_query("SELECT mobile FROM sim_details WHERE device_num=$device");	
		$rows = mysql_fetch_array($result);
		mysql_query("UPDATE messages SET status = '".addslashes($status)."',sent_flag=".SMS_SENT.",delivered_by='".$rows['mobile']."',delivered_time='".date('Y-m-d H:i:s')."' WHERE id = $message_id");
		
		$url = SERVER_BACKUP . 'logs/modemDeliveryUpdate';
		$data['externalId'] = $message_id;
		$data['mobile'] = $rows['mobile'];
		$data['deliveredTS'] = date('Y-m-d H:i:s');
		$data['status'] = $status;
		curl_post_async($url,$data);
	}
	
	function sendSMS($mobile,$message,$device,$message_id,$trials){
		$message = str_replace("\r\n","\n",$message);
		$message = trim($message);
        $sms_count = getNumberofSMS($message);
		if(strlen($mobile) == 10) $mobile = "91" . $mobile;
        
		$sms_left = $this->getSMSLeft($device);
		if($sms_left >= $sms_count){
			$this->setSIMStatus(STATE_BUSY,$device);
			$this->setSMSStatus(SMS_SENDING,$message_id);
			$comm = new communicator();
			$out = $comm->sendMessage($device,$mobile,$message);
			
			$this->updateSMSCount($sms_count,$device);
			if($out['stop']){
				mysql_query("UPDATE messages SET sent_flag=" .SMS_TOBESEND. ", trials=trials-1 WHERE id = $message_id");
				
				$url = SERVER_BACKUP . 'users/sendMsgMails';
				$result = mysql_query("SELECT mobile FROM sim_details WHERE device_num = $device");
				$dev_mobile = mysql_fetch_array($result);
			
				if(isset($out['restart'])){
					if($out['restart']){
						$data['mail_subject'] = "GSM Modem: Resetting Modem Now";
						$data['mail_body'] = "Device id $device was not responding. Restarting modem";
						$this->restart();
					}
					else {
						$data['mail_subject'] = "GSM Modem: Device not responding";
						$data['mail_body'] = "Device id $device was not responding. Stopping the device";
						$this->setSIMStatus(STATE_STOP,$device);
					}
					$data['emails'] = 'ashish@mindsarray.com';
				}
				else {
					if($out['num'] == '10'){
						$data['mail_subject'] = "GSM Modem: SIM Not inserted properly";	
					}
					else if($out['num'] == '21'){
						$data['mail_subject'] = "GSM Modem: No balance in SMS Sending SIM";
					}
					$data['mail_body'] = "Mobile: ".$dev_mobile['mobile'].", Device: $device";
					$data['mail_body'] .= "<br/>Output from machine: ".$out['out'];
					$data['emails'] = 'ashish@mindsarray.com';
					$this->setSIMStatus(STATE_STOP,$device);
				}
				
				//curl_post_async($url,$data);
			}
			else if($out['delay']){
				mysql_query("UPDATE messages SET sent_flag=" .SMS_TOBESEND. ", created = ADDTIME(created, '00:15:00') WHERE id = $message_id");
				$url = SERVER_BACKUP . 'users/sendMsgMails';
				$data['mail_subject'] = "GSM Modem: Operator Network Problem";
				$data['mail_body'] = "Message cannot be send now. Scheduled to 15 mins later";
				$data['mail_body'] .= "<br/>Device: $device, Message: $message_id, Output from machine: " . $out['out'];
				$data['emails'] = 'ashish@mindsarray.com';
				//curl_post_async($url,$data);
				$this->setSIMStatus(STATE_READY,$device);
			}
			else if($out['status'] == 1 || $trials >= 2){
				if($out['num'] != -1){
					$url = SERVER_BACKUP . 'users/sendMsgMails';
					$data['mail_subject'] = "GSM Modem: New CMS/CME Error Came in: ".$out['num'];
					$data['mail_body'] = "Device: $device, Message: $message_id, Output from machine: " . $out['out'];
					$data['emails'] = 'ashish@mindsarray.com';
					//curl_post_async($url,$data);
				}
				
				if($trials >= 1 && $out['status'] != 1){
					$url = SERVER_BACKUP . 'users/sendMsgMails';
					$data['mail_subject'] = "GSM Modem: Trials exceeded, Tried $trials times";
					$data['mail_body'] = "Device: $device, Message: $message_id, Output from machine: " . $out['out'];
					$data['emails'] = 'ashish@mindsarray.com';
					//curl_post_async($url,$data);
					$this->updateMessageReport($message_id,$device,'Not delivered');
				}
				else {
					$this->updateMessageReport($message_id,$device,'Delivered');
				}
				
				$this->setSIMStatus(STATE_READY,$device);
			}
			else{
				$this->setSMSStatus(SMS_TOBESEND,$message_id);
				
				if($out['num'] != -1){
					$url = SERVER_BACKUP . 'users/sendMsgMails';
					$data['mail_subject'] = "GSM Modem: New CMS/CME Error Came in: ".$out['num'];
					$data['mail_body'] = "Device: $device, Message: $message_id, Output from machine: " . $out['out'];
					$data['emails'] = 'ashish@mindsarray.com';
					//curl_post_async($url,$data);
				}
				
				/*$url = SERVER_BACKUP . 'users/sendMsgMails';
				$data['mail_subject'] = "Error while sending SMS";
				
				$result = mysql_query("SELECT mobile FROM sim_details WHERE device_num = $device");
				$dev_mobile = mysql_fetch_array($result);
			
				$data['mail_body'] = "Cannot send SMS to $mobile, Prob in SIM: ".$dev_mobile['mobile'].", Output from machine: " . $out['out'];
				$data['mail_body'] .= "<br/>Device: $device, Message: $message_id";
					
				$data['emails'] = 'ashish@mindsarray.com';
				curl_post_async($url,$data);*/
				sleep(20);
				$this->setSIMStatus(STATE_READY,$device);
			}
		}
		else {
			$result = mysql_query("SELECT mobile FROM sim_details WHERE device_num = $device");
			$dev_mobile = mysql_fetch_array($result);
				
			$this->setSIMStatus(STATE_STOP,$device);
			$url = SERVER_BACKUP . 'users/sendMsgMails';
			$data['mail_subject'] = "SMS Limit of 1 Modem SIM exhausted";
			$data['mail_body'] = "Mobile: ".$dev_mobile['mobile'].", Device: $device";
			$data['emails'] = 'tadka@mindsarray.com';
			//curl_post_async($url,$data);
		}
	}
	
	function receiveSMSCheck(){
		$time = date('H');
		
		if($time > '08'){
			$time = date('Y-m-d H:i:s', strtotime('- 15 minutes'));
			//$nums = explode(",",INCOMING_NUMS);
			$nums = array('7666888676','7303897886');
			foreach($nums as $num){
				
				$result = mysql_query("SELECT * FROM read_msg WHERE mobile = $num AND sms_time > '$time'");
				$count = mysql_num_rows($result);
				if($count < 5){
					$url = SERVER_BACKUP . 'users/sendMsgMails';
					$data['mail_subject'] = "GSM Modem: $num has received very less smses in last 5 mins";
					$data['mail_body'] = "No. of SMS received in last 5 mins: $count";
					//curl_post_async($url,$data);
					
					if($count == 0) {
						$data['mail_subject'] = "GSM Modem: $num is not receiving any sms";
						$data['mail_body'] = "Restarting modem";
						$data['emails'] = 'ashish@mindsarray.com';
						curl_post_async($url,$data);
						$this->restart();
					}
					else {
						$result = mysql_query("SELECT * FROM sim_details WHERE mobile = $num");
						$device = mysql_fetch_array($result);
						if($device['active_flag'] == 0){
							$data['mail_subject'] = "(SOS)Receiving SIM $num is not active";
							$data['mail_body'] = "$num is not active";
							$data['emails'] = 'tadka@mindsarray.com';
							//curl_post_async($url,$data);
						}
						else {
							mysql_query("UPDATE sim_details SET active_flag = 0 WHERE mobile = $num");
							sleep(5);
							$comm = new communicator();
							$comm->emptyInbox($device['device_num']-1);
							mysql_query("UPDATE sim_details SET active_flag = 1 WHERE mobile = $num");
						}
					}
				}
			}
		}
	}
	
	function receiveSMS($device){
		$this->setSIMStatus(STATE_BUSY,$device);
		$comm = new communicator();
		$out = $comm->receiveSMS($device);
		
		$result = mysql_query("SELECT mobile,sms_flag FROM sim_details WHERE device_num = $device");
		$dev_mobile = mysql_fetch_array($result);
		$mobile = $dev_mobile['mobile'];
		
		if(isset($out['reset'])){
			$this->setSIMStatus(STATE_READY,$device);
			$url = SERVER_BACKUP . 'users/sendMsgMails';
			$data['mail_subject'] = "GSM Modem: $mobile is not responding";
			$data['mail_body'] = "Restarting modem";
			//curl_post_async($url,$data);
			$this->restart();
		}
		else {
			$dropped = false;
			$all = array();
			foreach($out as $sms){
				mysql_query("INSERT INTO read_msg (mobile,message,sender,sms_time,sms_log,timestamp) VALUES ('$mobile','".addslashes($sms['msg'])."','".addslashes($sms['sender'])."','".addslashes($sms['time'])."','".addslashes($sms['sms'])."','".date('Y-m-d H:i:s')."')");
				if(($dev_mobile['sms_flag'] == '1' && preg_match('/^\+?[0-9]+$/i',$sms['sender'])) || $dev_mobile['sms_flag'] == '0'){
					//if(in_array($mobile,explode(",",INCOMING_NUMS))){
					if($mobile == '7666888676' || $mobile == '7303897886' || $mobile == '9619972172' || $mobile == '9681087993'){
						$sender = substr($sms['sender'],-10);	
						
						$right_time = date('Y-m-d H:i:s',strtotime('- 5 mins'));
						if(true){
						//if($sms['time'] >= $right_time){
							$data = array();
							$data['mobile'] = $sender;	
							$data['message'] = $sms['msg'];
							$data['timestamp'] = $sms['time'];
							$data['code'] = "91$mobile";
							$all[] = $data;
							
							$check1 = strpos($sender,"ICICI");
							$check2 = strpos($sender,"HDFCBK");
							$check3 = strpos($sender,"KOTAKB");
							$check4 = strpos($sender,"MAHABK");
							$check5 = strpos($sender,"SBI");
							
							$data1 = array();
								
							if($check1 !== false){
								$data1['sender'] =  "ICICI";
								$template = "Dear Customer, Your Ac @__123__@ is credited with INR@__123__@ on @__123__@. Info.@__123__@. Your Net Available Balance is INR@__123__@.@__123__@";
								$ret = matchTemplate($sms['msg'],$template);
								if($ret['status'] == 'success'){
									$vars = $ret['vars'];
									$data1['amount'] = $vars[1];
									$data1['transid'] = $vars[3];
									$data1['available'] = $vars[4];
									if($vars[0] == 'XXXXXXXX1578'){
										$data1['sender'] =  "ICICI1";
									}
									else if($vars[0] == 'XXXXXXXX0005'){
										$data1['sender'] =  "ICICI2";
									}
								}
							}
							else if($check2 !== false){
								$data1['sender'] =  "HDFC";
								$template = "Amt of INR @__123__@ deposited to @__123__@ towards @__123__@ Value @__123__@";
								$ret = matchTemplate($sms['msg'],$template);
								if($ret['status'] == 'success'){
									$vars = $ret['vars'];
									$data1['amount'] = $vars[0];
									$data1['transid'] = $vars[2];
								}
							}
							else if($check3 !== false){
								$data1['sender'] =  "KOTAK";
								$template = "@__123__@NR @__123__@ is credited to your A/c @__123__@ on @__123__@ on account of @__123__@.Combined Available Balance is INR @__123__@.@__123__@";
								$ret = matchTemplate($sms['msg'],$template);
								if($ret['status'] == 'success'){
									$vars = $ret['vars'];
									$data1['amount'] = $vars[1];
									$data1['transid'] = $vars[4];
									$data1['available'] = $vars[5];
								}
							}
							else if($check4 !== false){
								$data1['sender'] =  "BOM";
								$template = "Your A/c No @__123__@ has been credited by Rs. @__123__@ on @__123__@.Your Account Balance is : Rs. @__123__@ CR.You can withdraw up to Rs. @__123__@";
								$ret = matchTemplate($sms['msg'],$template);
								if($ret['status'] == 'success'){
									$vars = $ret['vars'];
									$data1['amount'] = $vars[1];
									$data1['transid'] = "BOM";
									$data1['available'] = $vars[3];
								}
							}
							else if($check5 !== false){
								$data1['sender'] =  "SBI";
								$template = "Your AC @__123__@ Credited INR @__123__@ on @__123__@ -@__123__@Avl Bal INR @__123__@";
								$ret = matchTemplate($sms['msg'],$template);
								if($ret['status'] == 'success'){
									$vars = $ret['vars'];
									$data1['amount'] = $vars[1];
									$data1['transid'] = $vars[3];
									$data1['available'] = $vars[4];
								}
							}
							
							if(isset($data1['sender'])){
								$data1['time'] =  $sms['time'];
								$data1['msg'] =  $sms['msg'];
								$data1['process'] =  "limits";
								curl_post_async("http://cc.pay1.in/limits/server.php",$data1);
							}
						}
						else {
							$dropped = true;
						}
					}
				}
			}
			
			if(!empty($all)){
				$url = SERVER_BACKUP . 'apis/sms.php';
				$data = array();
				$data['data'] = json_encode($all);
				curl_post_async($url,$data);	
			}
			if($dropped){
				//$ret = $comm->emptyInbox($device-1);
				/*if($ret > 0){
					$url = SERVER_BACKUP . 'users/sendMsgMails';
					$data['mail_subject'] = "GSM Modem: Empty $mobile done";
					$data['mail_body'] = "GSM Modem: Empty $ret messages";
					$data['emails'] = 'ashish@mindsarray.com';
					curl_post_async($url,$data);
				}*/
			}
			$this->setSIMStatus(STATE_READY,$device);
		}
	}
	
	function reset(){
		mysql_query("UPDATE sim_details SET state=0,sms_left_day = ".MAX_SMS_LIMIT);
		$time = date('Y-m-d H:i:s',strtotime('-4 hours'));
		mysql_query("UPDATE messages SET sent_flag=1 WHERE created <= '$time'");
	}
	
	function restart(){
		$shell_query = "nohup php " . DOCUMENT_ROOT . "modem/restart.php > /dev/null 2> /dev/null & echo $!";
		shell_exec($shell_query);
	}
	
	function start($newday=null){
		$this->initializeModem($newday);
		
		$shell_query = "nohup php " . DOCUMENT_ROOT . "modem/sender.php receive > /dev/null 2> /dev/null & echo $!";
		shell_exec($shell_query);
		
		while(true){
			//get ready devices
			$result = mysql_query("SELECT device_num FROM sim_details WHERE state=".STATE_READY. " AND active_flag = 1 AND sms_flag = 1 ORDER BY RAND()");
			$count = mysql_num_rows($result);
			
			$result_sms = mysql_query("SELECT mobile,id,sms FROM messages WHERE sent_flag=".SMS_TOBESEND." AND created <= '".date('Y-m-d H:i:s')."' order by priority desc LIMIT $count");
			
			while($sms = mysql_fetch_array($result_sms)){
				$device = mysql_fetch_array($result);
				$shell_query = "nohup php " . DOCUMENT_ROOT . "modem/sender.php sms " . $device['device_num']. " ". $sms['id'] ." > /dev/null 2> /dev/null & echo $!";
				shell_exec($shell_query);			
			}
			
			sleep(5);
		}
	}
	
	function receive(){
		while(true){
			//get ready devices
			/*$result = mysql_query("SELECT device_num FROM sim_details WHERE state=".STATE_READY. " AND active_flag = 1 AND sms_flag = 1 ORDER BY RAND() LIMIT 2");
			
			while($device = mysql_fetch_array($result)){
				$shell_query = "nohup php " . DOCUMENT_ROOT . "modem/sender.php rec " . $device['device_num'] ." > /dev/null 2> /dev/null & echo $!";
				shell_exec($shell_query);			
			}*/
			
			$result = mysql_query("SELECT device_num FROM sim_details WHERE state=".STATE_READY. " AND active_flag = 1 AND mobile in (".INCOMING_NUMS.")");
			
			while($device = mysql_fetch_array($result)){
				$ret = shell_exec("ps ax  | grep 'modem/sender.php rec " . $device['device_num'] ."' | wc -l");
				if($ret > 2){
					continue;
				}
				$shell_query = "nohup php " . DOCUMENT_ROOT . "modem/sender.php rec " . $device['device_num'] ." > /dev/null 2> /dev/null & echo $!";
				shell_exec($shell_query);			
			}
			
			sleep(10);
		}
	}
}
 
if(isset($argv)){
	$what = trim($argv[1]);
	if(strtoupper($what) == 'SMS'){
		$device = trim($argv[2]);
		$sms_id = trim($argv[3]);
		$modem = new Modem();
		$result_sms = mysql_query("SELECT mobile,sms,trials FROM messages WHERE id = $sms_id");
		$sms = mysql_fetch_array($result_sms);
		$modem->sendSMS($sms['mobile'],$sms['sms'],$device,$sms_id,$sms['trials']);
	}
	else if(strtoupper($what) == 'START'){
		$modem = new Modem();
		$modem->start();
	}
	else if(strtoupper($what) == 'RECEIVE'){
		$modem = new Modem();
		$modem->receive();
	}
	else if(strtoupper($what) == 'RESET'){
		$modem = new Modem();
		$modem->reset();
	}
	else if(strtoupper($what) == 'WAP'){
		$device = trim($argv[2]);
		$comm = new communicator();
		$comm->sendWAP($device);
	}
	else if(strtoupper($what) == 'SEND'){
		$command = trim($argv[2]);
		$device = trim($argv[3]);
		$wait = trim($argv[4]);
		$comm = new communicator();
		$ret = $comm->sendCommand($device,$command,$wait);
		echo $ret;
	}
	else if(strtoupper($what) == 'REC'){
		$device = trim($argv[2]);
		$modem = new Modem();
		$modem->receiveSMS($device);
	}
	else if(strtoupper($what) == 'TEST'){
		echo "123";
	}
	else if(strtoupper($what) == 'EMPTY'){
		$device = trim($argv[2]);
		$comm = new communicator();
		mysql_query("UPDATE sim_details SET active_flag = 0 WHERE device_num = $device");
		sleep(5);
		$comm->emptyInbox($device-1);
		mysql_query("UPDATE sim_details SET active_flag = 1 WHERE device_num = $device");
	}
	else if(strtoupper($what) == 'RECCHECK'){
		$modem = new Modem();
		$modem->receiveSMSCheck();
	}
}
else if(isset($_REQUEST)){
	$query = $_REQUEST['query'];
	if(strtoupper($query) == "SMS"){
		$ips = array('184.72.255.71');
		if(!in_array($_SERVER['REMOTE_ADDR'],$ips)){
			exit;
		}
		$mobiles=explode(",",urldecode($_REQUEST['mobile']));
		$msg = urldecode($_REQUEST['message']);
		$priority = urldecode($_REQUEST['priority']);
		$ids = array();
		foreach($mobiles as $mobile){
			if(strlen($mobile) == 10){
				$mobile = "91" . $mobile;
			}
			mysql_query("INSERT INTO messages (mobile,sms,priority,created) VALUES ('$mobile','".addslashes($msg)."',$priority,'".date('Y-m-d H:i:s')."')");
			$ids[] = mysql_insert_id();
		}
		
		echo implode(",",$ids);
	}
	else if(strtoupper($query) == "START"){
		//shell_exec("nohup php " . DOCUMENT_ROOT . "modem/sender.php start " . $device['device_num']. " ". $sms['id'] ." > /dev/null 2> /dev/null & echo $!");
	}
	else if(strtoupper($query) == "ANU"){
		$arr2 = '9886118103,8088788666,9620384800,9886398415,8023333096,8904365816,9916072031,9448828592,9611934859,9845297554,9844567310,9141687098,9538886649,9448080883,9620050338,9916135764,8197833585,9535985202,9886118103,9242236494,9886398415,9845019734,9844032681,9449028612,8884711213,9902910396,9448080883,8197833585,9945472518,9916457890,9900022110,9916654536,9449028612,9739001447,8212404059,8095551948,9880182368,9620384800,9902420409,7259291770,9739001447,9620201046,9535985202,9901469966,9448350340,9916654536,9900094772,8453976773,8892221919,8066060000,8792604118,9448350340,9945472518,9901098169,9845835917,9739513250,9900022110,9880182368,9611934859,9980222040,9480700944,9591840652,9880945022,9141687098,8040298553,9711958191,9845609729,9900551005,8088801857,9900049636,7483014078,8904495919,9342836376,9620050338,8951199317,9900108813,9916832514,9900102440,9019556942,9900022110,9880182368,9986071195,8023571523,9845013253,9448425605,9743959697,8095684147,9972530187,9856164895,9928047965,9335368458,9886401935,9418101061,8123042079,9425756474,9902005312,9986546700,9812016725,9445516863,8081890454,9448064124,9164619650,9740600395';
        $arr2 = explode(",",$arr2);
        $msg = "Limited seats left for World Rummy Tournament on 5th May @Bangalore City Institute. Finale in GOA
Prizes-Rs.1Cr
Call Lohith on 8904675149 to register Now!";
		$i = 0;
		
		foreach($arr2 as $mobi){
			$mobi = substr($mobi, -10);
			$mobi = "91" . $mobi;
			mysql_query("INSERT INTO messages (mobile,sms,priority,created) VALUES ('$mobi','".addslashes($msg)."',2,'".date('Y-m-d H:i:s')."')");
		}
	}
}


//echo $pid = shell_exec("nohup php /home/ashish/workspace/smstadka/app/webroot/apis/sms_sender.php 1 9819032643 hello > /dev/null 2> /dev/null & echo $!");
//echo $pid = shell_exec("nohup php /home/ashish/workspace/smstadka/app/webroot/apis/sms_sender.php 2 9819032643 \"$msg\" > /dev/null 2> /dev/null & echo $!");
//echo $pid = shell_exec("nohup php /home/ashish/workspace/smstadka/app/webroot/apis/sms_sender.php 3 9819032643 hello1 > /dev/null 2> /dev/null & echo $!");
//echo $pid = shell_exec("nohup php /home/ashish/workspace/smstadka/app/webroot/apis/sms_sender.php 4 9819032643 hello2 > /dev/null 2> /dev/null & echo $!");
//echo $pid = shell_exec("nohup php /home/ashish/workspace/smstadka/app/webroot/apis/sms_sender.php 5 9819032643 hello3 > /dev/null 2> /dev/null & echo $!");

?>