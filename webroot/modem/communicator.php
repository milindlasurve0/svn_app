<?php
include "php_serial.class.php";
include "pdu.php";

class communicator {
	function sendMessage($device,$mobile,$msg,$pduMessage=null){
		$device = $device - 1;
		$ret = true;
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/ttyUSB".$device); //usb2 is 9833032643
		$stat = array();
		
		if($devicePos){
			$serial->confBaudRate(9600);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
			
			// Then we need to open it
			$serial->deviceOpen();
			if($pduMessage == null){
				$pduGenerator = new Pdu();
				$pduMessage = $pduGenerator->generatePDU($mobile, $msg);
			}
			$serial->sendMessage("AT+CMGF=0\r");
			sleep(1);
			$check = $serial->readPort();
			$pos = strpos($check,"\r\nOK\r\n");
			
			if($pos === false)$ret = false;
			
			if($ret){
				$err_cme = array('256','261');
				$err_cms = array('500','50','0','38','47');
				$cms_num = -1;
				$cme_num = -1;
				$delay = false;
				$stop = false;
				$num = -1;
				
				$out = "";
				foreach($pduMessage as $pdu){
					$serial->sendMessage("AT+CMGS=".$pdu['cmgslen']."\r");
					sleep(1);
					$message = $pdu['pdu'];
					$serial->sendMessage("{$message}".chr(26));
					
					sleep(6);
					$read = $serial->readPort();
					sleep(1);
					$out .= $read;
				}
				
				preg_match_all('/\+CMGS:/i', $out, $matches);
				if(count($matches['0']) != count($pduMessage)){
					$ret = false;
				}
				
				echo $out;
				
				if(!$ret){//if ret is false .. find out the reason
				
					preg_match('/CME ERROR: [0-9]+/', $out, $matches1);
					preg_match('/CMS ERROR: [0-9]+/', $out, $matches2);
						
					if(!empty($matches1)){
						$cme_num = substr($matches1[0],strlen('CME ERROR: '));
						$cme_num = trim($cme_num);
						if(in_array($cme_num,$err_cme)){
							$ret = false;
							$cme_num = -1;
						}
						else if(in_array($cme_num,array('258','100'))){//ok to go
							$cme_num = -1;
						}
						else if($cme_num == '10'){
							$stop = true;
						}
					}
					
					if(!empty($matches2)){
						$cms_num = substr($matches2[0],strlen('CMS ERROR: '));
						$cms_num = trim($cms_num);
						if(in_array($cms_num,$err_cms)){
							$ret = false;
							if($cms_num == '38' || $cms_num == '47') $delay = true;
							$cms_num = -1;
						}
						else if($cms_num == '21'){
							$stop = true;
						}
					}
				
					if($cme_num != -1){
						$num = $cme_num;
					}
					else if($cms_num != -1){
						$num = $cms_num;
					}
				}
				$serial->deviceClose();
				
				$stat['status'] = $ret;
				$stat['out'] = $out;
				$stat['num'] = $num;
				$stat['delay'] = $delay;
				$stat['stop'] = $stop;
				return $stat;
			}
			else return array('stop' => true,'restart' => false);
		}
		else return array('stop' => true,'restart' => true);
	}
	
	function receiveSMS($device){
		$device = $device - 1;
		
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/ttyUSB".$device); //usb2 is 9833032643
		if($devicePos){
			$serial->confBaudRate(9600);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
			
			// Then we need to open it
			$serial->deviceOpen();
			$serial->sendMessage("AT+CMGF=1\r");
			sleep(1);
			//usleep(500000);
			$serial->sendMessage("AT+CPMS=\"MT\"\r");
			sleep(1);
			//usleep(500000);
			$serial->sendMessage("AT+CMGL=\"ALL\"\r");
			sleep(4);
			$out = $serial->readPort();
			sleep(1);
			$array = explode("+CMGL: ",$out);
			
			$smses = array();
			foreach($array as $sms){
				$index = strpos($sms,"\n");
				if($index !== false){
					$params = substr($sms,0,$index);
					$params = str_replace("\"","",$params);
					$params = explode(",",$params);
					$msg = substr($sms,$index+1);
					$msg = str_replace("\r\nOK\r\n","",$msg);
				}
				else {
					$params = str_replace("\"","",$sms);
					$params = explode(",",$params);
				}
					
				$data['id'] = $params[0];
				if(isset($params[2])){
					if($params[1] == "REC UNREAD"){
						$data['sender'] = $params[2];
						$dates = explode("/",trim($params[4]));
						$data['time'] = "20" . $dates[0] . "-" . $dates[1] . "-" . $dates[2] . " " . substr($params[5],0,8);
						$data['sms'] = $sms;
						
						if(empty($msg)){
							$data['msg'] = $sms;
						}
						else {
							$pos = strrpos($msg, "\n", -1);
							if($pos !== false)$msg = substr($msg,0,$pos);
							$data['msg'] = $msg;
						}
						$smses[] = $data;
					}
					$serial->sendMessage("AT+CMGD=".$data['id']."\r");
					sleep(1);
					//usleep(500000);
				}
			}
			
			$serial->deviceClose();
		}
		else {
			$smses['reset'] = true; 
		}
		return $smses;
	}
	
	function emptyInbox($device){
		//$device = $device - 1;
		$ret = 0;
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/ttyUSB".$device); //usb2 is 9833032643
		if($devicePos){
			$serial->confBaudRate(9600);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
			
			// Then we need to open it
			$serial->deviceOpen();
			$serial->sendMessage("AT+CPMS=\"MT\"\r");
			sleep(1);
			//usleep(500000);
			$serial->sendMessage("AT+CMGL=\"ALL\"\r");
			sleep(4);
			$out = $serial->readPort();
			sleep(1);
			$array = explode("+CMGL: ",$out);
			
			foreach($array as $sms){
				$index = strpos($sms,"\n");
				if($index !== false){
					$params = substr($sms,0,$index);
					$params = str_replace("\"","",$params);
					$params = explode(",",$params);
					$msg = substr($sms,$index+1);
					$msg = str_replace("\r\nOK\r\n","",$msg);
				}
				else {
					$params = str_replace("\"","",$sms);
					$params = explode(",",$params);
				}
					
				$data['id'] = $params[0];
				if(trim($params[1]) != 'REC UNREAD'){
					$serial->sendMessage("AT+CMGD=".$data['id']."\r");
					sleep(1);
					//usleep(500000);
					$ret++;
				}
			}
			
			$serial->deviceClose();
		}
		return $ret;
	}
	
	function sendCommand($device,$command,$wait){
		$device = $device - 1;
		
		$serial = new phpSerial();
		$serial->deviceSet("/dev/ttyUSB".$device); //usb2 is 9833032643
		$serial->confBaudRate(9600);
		$serial->confParity("none");
		$serial->confCharacterLength(8);
		$serial->confStopBits(1);
		$serial->confFlowControl("none");
		
		// Then we need to open it
		$serial->deviceOpen();
		
		$serial->sendMessage($command."\r");
		
		sleep($wait);
		$out = $serial->readPort();
		$serial->deviceClose();
		return $out;
	}
	
	function recharge(){
		//videocon//
		//Sender: +50123
		//Success Template: Topup for Rs.10 done on 919076218025, RefID 220440327 on 12/04/2012 at 04:40PM. Pre bal 1030.00 Post bal 1020.00.
	}
	
	function get_string_between($string, $start, $end){
		$string = " ".$string;
		$ini = strpos($string,$start);
		if ($ini == 0) return "";
		$ini += strlen($start);
		$len = strpos($string,$end,$ini) - $ini;
		return substr($string,$ini,$len);
	}
	
	function getSCID($device){
		$out = $this->sendCommand($device,"AT^SCID",1);
		return trim($this->get_string_between($out,"^SCID:","OK"));
	}
	
	function sendWAP($device){
		$device = $device - 1;
		$ret = true;
		$serial = new phpSerial();
		$serial->deviceSet("/dev/ttyUSB".$device); //usb2 is 9833032643
		$serial->confBaudRate(9600);
		$serial->confParity("none");
		$serial->confCharacterLength(8);
		$serial->confStopBits(1);
		$serial->confFlowControl("none");
		
		// Then we need to open it
		$serial->deviceOpen();
		$pduMessage = array();
		
		$pduMessage[0]['pdu'] = "0001000B9119676688686700042E0B05040B84C0020003F001010A060403B081EA02066A0085
09036D6F62696C65746964696E67732E636F6D2F0001";
		$pduMessage[0]['cmgslen'] = "60";
		
		$serial->sendMessage("AT+CMGF=0\r");
		sleep(1);
		$err = array('256','261','100');
		$err_cms = array('21','500');
		foreach($pduMessage as $pdu){
			$serial->sendMessage("AT+CMGS=".$pdu['cmgslen']."\r");
			sleep(1);
			$message = $pdu['pdu'];
			$serial->sendMessage("{$message}".chr(26));
			sleep(5);
			$out = $serial->readPort();
			if(strpos($out, "CME")){
				foreach ($err as $token) {
					if (stristr($out, $token) !== FALSE) {
				        echo "String contains: $token<br/>";
				        $ret = false;
				        break;
				    }
				}
			}
			if(strpos($out, "CMS")){
				foreach ($err_cms as $token) {
					if (stristr($out, $token) !== FALSE) {
				        //No balance
				        $ret = false;
				        break;
					}
				}
			}
			
			sleep(2);
		}
		echo $out;
		$stat = array();
		if(empty($out)) {
			$ret = false;
			$out = "Device not working";
		}
		$stat['status'] = $ret;
		$stat['out'] = $out;
			
		$serial->deviceClose();
		return $stat;
	}
	
	function findUSBDevices(){
		$ports = shell_exec("ls -l /dev | grep 'ttyUSB' | awk '{print $10}' | awk -F'B' '{print $2}' | sort -n");
		$ports = explode("\n",$ports);
		$ports = array_unique($ports);
		$final = array();
		foreach($ports as $port){
			if($port != '' && !isset($final[$port])){
				$final[$port]['device'] = '';
				$final[$port]['bus'] = '';
			}
		}
		
		return $final;
	}
}
?>