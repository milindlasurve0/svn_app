/* scripts.js starts here */
var siteName = 'www.atadka.com';
var noFriends = 0;
/*new script to detect browser by dinesh */
(function(){
	  
	  var eventMatchers = {
	    'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
	    'MouseEvents': /^(?:click|mouse(?:down|up|over|move|out))$/
	  };
	  var defaultOptions = {
	    pointerX: 0,
	    pointerY: 0,
	    button: 0,
	    ctrlKey: false,
	    altKey: false,
	    shiftKey: false,
	    metaKey: false,
	    bubbles: true,
	    cancelable: true
	  };
	  
	  Event.simulate = function(element, eventName) {
	    var options = Object.extend(defaultOptions, arguments[2] || { });
	    var oEvent, eventType = null;
	    
	    element = $(element);
	    
	    for (var name in eventMatchers) {
	      if (eventMatchers[name].test(eventName)) { eventType = name; break; }
	    }

	    if (!eventType)
	      throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

	    if (document.createEvent) {
	      oEvent = document.createEvent(eventType);
	      if (eventType == 'HTMLEvents') {
	        oEvent.initEvent(eventName, options.bubbles, options.cancelable);
	      }
	      else {
	        oEvent.initMouseEvent(eventName, options.bubbles, options.cancelable, document.defaultView,
	          options.button, options.pointerX, options.pointerY, options.pointerX, options.pointerY,
	          options.ctrlKey, options.altKey, options.shiftKey, options.metaKey, options.button, element);
	      }
	      element.dispatchEvent(oEvent);
	    }
	    else {
	      options.clientX = options.pointerX;
	      options.clientY = options.pointerY;
	      oEvent = Object.extend(document.createEventObject(), options);
	      element.fireEvent('on' + eventName, oEvent);
	    }
	    return element;
	  };
	  
	  Element.addMethods({ simulate: Event.simulate });
	})();
	
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

function changeTabClass(obj){
	var navTabs = $('navTabs').childElements();
	var len = navTabs.length;
	
	for(var j=0;j<len; j++)
	{	
		if(navTabs[j].id)
			navTabs[j].removeClassName('sel');
	}
	
	obj.parentNode.className = 'sel';
	showLoader('pageContent');
}

function changeInnerTabClass(obj){
	var navTabs = $$('ul#innerul li');
	var len = navTabs.length;
	
	for(var j=0;j<len; j++)
	{	
		if(navTabs[j].name="innerli"){
			if(navTabs[j].firstDescendant().className != "hList")
				navTabs[j].firstDescendant().className = '';
		}
	}
	if($('innerDiv')) {
		showLoader('innerDiv');
	}
	else {
		obj.addClassName('loader');
	}
	obj.className = 'sel';
	
}

function paymentTabs(obj,divId){
	var navTabs = $$('ul#innerul li');
	var len = navTabs.length;
	
	for(var j=0;j<len; j++)
	{	
		if(navTabs[j].name="innerli"){
			if(navTabs[j].firstDescendant().className != "hList")
				navTabs[j].firstDescendant().className = '';
		}
	}
	
	obj.className = 'sel';
	if(divId == 'paymentDiv'){
		$('paymentDiv').show();
		$('chequeDiv').hide();
		$('billingDiv').hide();		
		$('custCareDiv').hide();
	}else if(divId == 'chequeDiv'){
		$('paymentDiv').hide();
		$('chequeDiv').show();
		$('custCareDiv').hide();
		$('billingDiv').hide();
	}else if(divId == 'billingDiv'){
		$('paymentDiv').hide();
		$('chequeDiv').hide();
		$('billingDiv').show();
		$('custCareDiv').hide();
	}else if(divId == 'custCareDiv'){
		$('paymentDiv').hide();
		$('chequeDiv').hide();
		$('billingDiv').hide();
		$('custCareDiv').show();
	}
	
}

function changeIndexTab(obj){
	var id = obj.parentNode.className;
	var navTabs = $$('ul#landingPgTabs li a');
	var len = navTabs.length;
	for(var j=0;j<len; j++)
	{
		navTabs[j].removeClassName('sel');
	}
	obj.addClassName('sel');
	var divs = $('landingPgTabCont').immediateDescendants();
	var len = divs.length;
	for(var j=0;j<len; j++)
	{
		if(divs[j].id != '')
		divs[j].hide();
	}
	$(id).show();
}

function amtValidate(y) {

	if (y == "")
	{
		alert("Plese enter proper amount");
      	return false;
	}
	if(isNaN(y)||y.indexOf(" ")!=-1)
   	{
      	alert("Plese enter proper amount");
      	return false;
   	}
	/*if (y < 20)
	{
		alert("Plese enter minimum 20");
      	return false;
	}*/
	//alert(y.length)
	for (var i=0;i<(y.length);i++)
	{
		//alert(y.charCodeAt(i));
		if (y.charCodeAt(i) == 46) {
			alert("Recharge amount should not be decimal no.");
			$('amount').focus();
			return false;
		}
	}
        return true;
}

function mobileValidate(y){
  if(isNaN(y)||y.indexOf(" ")!=-1)
   {
	  /*if($('divErr')){
    	  $('divErr').innerHTML = '<div class="errMessage1" id="flashMessage">Your mobile number should contain numeric values</div>';
      }
      else{*/
    	  alert("Your mobile number should contain numeric values");
      //}	  
      return false;
   }
   if (y.length != 10)
   {
	  /*if($('divErr')){
		  $('divErr').innerHTML = '<div class="errMessage1" id="flashMessage">Your mobile number should be a 10 digit number</div>';
      }
      else{*/ 
    	  alert("Your mobile number should be a 10 digit number");
      //}
      return false;
   }
   if (y.charAt(0)!="9" && y.charAt(0)!="8" && y.charAt(0)!="7")
   {
	   /*if($('divErr')){
		  $('divErr').innerHTML = '<div class="errMessage1" id="flashMessage">Your mobile number should start with 9, 8 or 7</div>';
	   }
	   else{*/
		   alert("Your mobile number should start with 9, 8 or 7");
	   //}
	   return false;
   }
  return true;
}
function dthNoValidate(y){
    
  if(y=='' || isNaN(y)||y.indexOf(" ")!=-1)
   {
	  
    	  alert("Your DTH number should contain numeric values");
     	  
      return false;
   }
   if (y.length > 12)
   {
	 
    	  alert("Your DTH number should contain less then 12 digits .");
     
      return false;
   }
  return true;
}
function nameValidate(name,fieldName,maxlength){
	if( (name == null) || (name.length == 0)){
		/*if($('divErr')){
			$('divErr').innerHTML = '<div class="errMessage1" id="flashMessage">Enter '+fieldName+'</div>';
		}
		else{*/
			alert("Enter "+fieldName);
		//}		
		return false;
	}
	if(maxlength != -1 && name.length > maxlength){
		/*if($('divErr')){
			$('divErr').innerHTML = '<div class="errMessage1" id="flashMessage">'+fieldName+' should contain maximum of '+maxlength+' characters</div>';
		}
		else{*/
			alert(fieldName + " should contain maximum of " + maxlength + " characters");
		//}		
		return false;
	}
	var re = /[^a-zA-Z0-9 ]/g;
	if (re.test(name)){
		/*if($('divErr')){
			$('divErr').innerHTML = '<div class="errMessage1" id="flashMessage">'+fieldName+' should not contain any special characters</div>';
		}
		else{*/
			alert(fieldName + " should contain alphanumeric (A-Z, a-z, 0-9) characters only." );
		//}		
		return false;
	}	
	return true;	
}

function changePassValidation(){
	if($('pass1').value.strip() == ''){
		alert("Enter your Current password.");
		return false;
	} 
	else if($('pass2').value.strip() == ''){
		alert("Enter new password.");
		return false;
	}
	else if($('pass3').value.strip() == ''){
		alert("Re-enter new password");
		return false;
	}
	else if($('pass2').value.strip() != $('pass3').value.strip()){
		alert("New passwords do not match");
		return false;
	}
	return true;
}
function rechargeValidation(typ){
  
	if(typ == 'mobile' && ! mobileValidate($('mobile_no').value.strip())){
		return false;
	} else if(typ == 'dth' && ! dthNoValidate($('dth_no').value.strip())){           
               
		return false;
                
	}else if($('s_provider').value.strip() == ''){
           
                alert("Please select a operator name .");
		return false;
	}
	else if( ! amtValidate( $('amount').value.strip() ) ){
                return false;
	}
        
	return true;
}
function signUpValidation(mobile,captcha){
	if(mobileValidate(mobile)){
		if(nameValidate(captcha,'Verification code',4)){
			/*if($('frgetSub')){
				showLoader2('frgetSub');
				$('frgetSub').show();
			}*/
			return true;
		}	
	}
	return false;
}

function defineSMSCode(id,code){
	$(id).innerHTML = 'Registered user can also subscribe to this package by sending SMS as '+code+' to 09223178889.';
	$(id).show();
}

/*document.observe('dom:loaded', function(){
	$$('table.dataTableBody tr:nth-child(odd)').invoke("addClassName", "altRow");
	

});*/
document.observe('dom:loaded', function(){
	if($('vertical_carousel')){
		var VCLength = $$('#vertical_carousel ul li.ie6Fix2');
		switch(VCLength.length)
		{
			case 5: $('vertical_carousel').setStyle({ height:'680px'}); break;
			case 4: $('vertical_carousel').setStyle({ height:'445px'}); break;
			case 3: $('vertical_carousel').setStyle({ height:'330px'}); break;
			case 2: $('vertical_carousel').setStyle({ height:'230px'}); break;
			case 1: $('vertical_carousel').setStyle({ height:'130px'}); break;
			default: $('vertical_carousel').setStyle({ height:'665px'});
		}
	}
	
	
});

function altRow() {
	//alert("he");
	$$('table.ListTable tr:nth-child(even)').invoke("addClassName", "altRow");
	$$('table.dataTableBody tr:nth-child(even)').invoke("addClassName", "altRow");
	//alert("he");
}

var signupDefault = 'Enter 10 digit Mobile Number here';


function getWinDimension(){
		var winDim = document.viewport.getDimensions();
		this.windowHeight = winDim.height;
		this.windowWidth = winDim.width;
		
		if(BrowserDetect.browser == 'Explorer')
		{
			if (typeof window.innerWidth != 'undefined')
            {
                 this.windowWidth = window.innerWidth,
                 this.windowHeight = window.innerHeight
            }
           
           // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
           
            else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
            {
                  this.windowWidth = document.documentElement.clientWidth;
                  this.windowHeight = document.documentElement.clientHeight;
            }
           
            // older versions of IE
           
            else
            { 
            	if(document.getElementsByTagName('body')['0'])
            	{
                	this.windowWidth = document.getElementsByTagName('body')['0'].clientWidth;
                	this.windowHeight = document.getElementsByTagName('body')['0'].clientHeight;
            	}
            }
		}
		
	}
	getWinDimension();
/*** Position Elements to center of the page ***/
	function centerPos(element){
		  var deltaX;
		  var deltaY;
		  element = $(element);
		  
		  if(!element._centered){
			  Element.setStyle(element, {position: 'absolute', zIndex: 990});
			  element._centered = true;
		  }
		  
		  if(element.id == 'popUpDiv'){
			  var width = '';
			  var elem = ($('messagePopUpDiv').childElements())[0];
			  if(!elem || !elem.style.width)
				  width = '400px';
			  else 
				  width = (elem.style.width).slice(0, -2).strip()*1 + 30 + 'px';
			  Element.setStyle(element, {width: width});
		  }
		  else {
			  Element.setStyle(element, {width: 'auto'});
		  }
		  
		  var dims = Element.getDimensions(element);
		  Position.prepare(); 
		  var winWidth,winHeight;
		  
		  var paddingPopupTop = 0;
		  winWidth = this.windowWidth;
		  winHeight = this.windowHeight;
		  if(BrowserDetect.browser == 'Explorer'){		 	
		 		deltaX = (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
				deltaY = (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
		  }else{
		 		deltaX = Position.deltaX;
				deltaY = Position.deltaY;				
		  }
		 
		 var offLeft = ( deltaX + Math.floor((winWidth-dims.width)/2));
		 var offTop = ( deltaY + Math.floor((winHeight-dims.height)/2));
		 var adjustRatio = winHeight*paddingPopupTop;
		
		 element.style.top = (dims.height >= winHeight)? ((deltaY)? (adjustRatio + deltaY + "px") : adjustRatio + "px"):((offTop != null && offTop > 0) ? offTop : '0')+ 'px';
		 element.style.left = ((offLeft != null && offLeft > 0) ? offLeft :'0') + 'px';
		 
		 /*var tempTop = element.style.top;
		 var tempIntTop = tempTop.slice(0,element.style.top.length-2);
		 if(tempIntTop < 0)
		 {
		 	element.style.top = '0px';
		 }*/
		 
		$(element).show();
	}
	
	function calculateCost(length){
		return Math.ceil(length/DEFAULT_MESSAGE_LENGTH)*EACH_MESSAGE_COST;
	}
	
	function calculateCostRem(length){
		var y  = length;
		var x = Math.ceil(length/DEFAULT_MESSAGE_LENGTH);
		var con = x*DEFAULT_MESSAGE_LENGTH - APP_REM_MSG_FIXED;
		if(y > con)
			return (x + 1)*EACH_MESSAGE_COST;
		else
			return x*EACH_MESSAGE_COST;		
	}
	
	function countCharacters(id,out){
		var str = $(id).value.length;
		var cost = calculateCost(str);
		//var cost = calculateCost(str-DEFAULT_MESSAGE_LENGTH+ADSPACE);
		
		$(out).innerHTML=str + " chars "+cost + " Paise";
	}
	
	function resCharacters(id,limit,out){
		var str = $(id).value.length;
		var cost = calculateCostRem(str);
		//$(out).innerHTML=str + '/' + limit + " chars ";
		$(out).innerHTML=str + '/' + limit + " chars - "+cost + " Paise";
		if(str > limit){
			alert('Message limit: '+limit+' chars only');
			$(id).value = $(id).value.substring(0,limit);
			str = $(id).value.length;
			//$(out).innerHTML=str + " chars ";
			cost = calculateCostRem(str);
			$(out).innerHTML=str + '/' + limit + " chars - "+cost + " Paise";
			return false;
		}	
	}
	
	function addFriend(nameId,mobileId){
		var name = $(nameId).value;
		var mobile = $(mobileId).value;
		var ret = false;
		if(nameValidate(name,'Friend Name',20)){
			if(mobileValidate(mobile)){
				ret = true;
			}
		}
		
		if(ret){
			var url    = '/users/addFriend/';	
			var rand   = Math.random(9999);
			var pars   = "nickname="+name+"&mobile="+mobile+"&rand="+rand;
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
							onSuccess:function(transport)
							{
								if(transport.responseText == "null")
								{
									alert('Mobile number already exists in your friend list');
								}
								else
								{
									
									if(noFriends && noFriends == 1){		
										$('friendListTable').innerHTML = '<table cellpadding="0px" cellspacing="0px" border="0" class="dataTable2" style="margin-bottom:0">'
								                                    +'<tr><th colspan="4">Friend List</th></tr>'
																	+'<tr class="nobg">'
								                                        +'<td style="width:26px;"><input  type="checkbox" name="checkAll" id="checkAll" onclick="selectAll(this,\'friendsTableDiv\',\'data[Friendlist][id]\')"/></td>'
																		+'<td width="250px"  style="font-weight:bold;">Name</th>'
								                                        +'<td width="100px;"  style="font-weight:bold;">Mobile No.</th>'
								                                        +'<td>&nbsp;</th>'
								                                    +'</tr></table>';
										noFriends = 0;										
									 }
									
									var id = transport.responseText;
									var html = '<tr id="frnd'+mobile+'">'
			                        		+'<td style="width:26px;"><input title="" onClick="selectFriend(\'friendsTableDiv\',\'data[Friendlist][id]\')" checked="checked" type="checkbox" name="data[Friendlist][id]" value="'+id+'"/></td>'
				                        	+'<td width="250px" id="nickname_'+id+'">'+name+'</td>'
			                        		+'<td width="100px">'+mobile+'</td>'
											+'<td><img src="/img/spacer.gif" onclick="delFriend(\''+mobile+'\',\''+id+'\')" title="Delete" alt="Delete" class="otherSprite oSPos18"></td></tr>';
											
									Element.insert('friendsTable', { top: html });
									$(nameId).value = '';
									$(mobileId).value = '';
									selectFriend('friendsTableDiv','data[Friendlist][id]');
								}	
							}
					});
		
		}
	}
	
	function delFriend(frnd,id){			
			var url    = '/users/deleteFriend/';	
			var rand   = Math.random(9999);
			var pars   = "id="+id+"&rand="+rand;
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
							onSuccess:function(transport)
							{								
								if(transport.responseText == "null")
								{
									alert('Error while deleting, please try again');
								}
								else
								{
									selectFriend('friendsTableDiv','data[Friendlist][id]');
									if(transport.responseText == "1")
									{
										$('frnd'+frnd).remove();
									}else if(transport.responseText == "0"){
										$('frnd'+frnd).remove();
										$('friendsTable').innerHTML = '';
										$('friendListTable').innerHTML = '<table cellpadding="0px" cellspacing="0px" border="0" class="dataTable2" style="margin-bottom:0">'
								                                    +'<tr><th colspan="4">Friend List</th></tr>'
																	+'<tr class="nobg"><td colspan="4" style="padding:5px 0;">Create your own friend list. Add friends using the box below. And share cool messages with them with one click.</td></tr></table>';
  									noFriends = 1;
									}																		
								}	
							}
					});			
	}
	
	function ajaxUpdaterCall(url,params,updateDiv,success,complete){
		
		new Ajax.Updater(updateDiv, url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onSuccess: function(response){ if(success != '') {eval(success);}},
	  			onComplete: function(response){ if(complete != ''){eval(complete);}}
			});
	}
	
	function sendMessage(id,type,from){
		popupSwap();
		showLoader2('errMessagePopUp');
		centerPos('errPopUp');
		
		var url = '/users/beforeSend';
		var params = {'id' : id,'to' : type,'from' : from};
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
					$('errPopUp').hide();
		  			centerPos('popUpDiv');
		  		}
		});
	}
	
	function sendMessageToFriends(msgId){
		var elems = $$('input');
		
		var len = elems.length;
		
		var num = 0;
		var ids = '';
		for(var i =0;i<len;i++){
			if(elems[i].name == 'data[Friendlist][id]' && elems[i].checked){
				if(num > 0){
					ids = ids + ",";
				}	
				ids = ids + elems[i].value;
				num++;
			}
		}
		
		var content = $('textContent').value;
		var mobile = $('mobileNumber').value.strip();
		var toSend = false;
		if(mobile != ''){
			if(mobileValidate(mobile))
				toSend = true;
		}
		else {
			if(num < 1){
				alert("First choose atleast one friend from the list or enter a mobile number");
			}
			else
				toSend = true; 
		}
		
		if(toSend){
			if(content.strip().length < 1){
				alert("Message cannot be empty");
			}
			else {
				if(msgId == '0'){
					var url = '/messages/sendSMSToFriendsFree/';
				}	
				else {
					var url = '/messages/sendSMSToFriends/';
				}
				var random = Math.floor(Math.random()*100000000);				
				var params = {'msgId' : msgId,'content' :encodeURIComponent(content),'ids': ids,'mobile':mobile,'random' : random};
				showLoader2('sendButt');
				new Ajax.Updater('messagePopUpDiv', url, {
		  			parameters: params,
		  			evalScripts:true,
		  			onComplete: function(response){
		  				centerPos('popUpDiv');
		  				if($('sendButt') && msgId == '0'){
		  					$('sendButt').innerHTML = '<input type="image" src="/img/spacer.gif" class="otherSprite oSPos8"  onClick="sendMessageToFriends(0);">';
		  				}
		  				else if($('sendButt')){
		  					$('sendButt').innerHTML = '<input type="image" src="/img/spacer.gif" class="otherSprite oSPos8"  onClick="sendMessageToFriends(null);">';
		  				}
		  				
		  				if($('textContent')) $('textContent').value = '';
		  				asynchronousCall(random);
		  			}
				});
			}
		
		}
		
	}
	
	function sendMessageDraft(id){
		var url = '/messages/sendMessageDraft';
		var params = {'id' : id};
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){centerPos('popUpDiv');}
		});
	}
	
	function signin(e,par)
	{
	 	var characterCode;
		if(e && e.which)
		{
			 e = e;
			 characterCode = e.which;
		}
		else
		{    
			 e = event;
			 characterCode = e.keyCode; 
		}
		
		if(characterCode == 13)
		{
			 login(par);
			 return false;
		}
		return true;
	}
	
	function dndVal(e,par)
	{
	 	var characterCode;
		if(e && e.which)
		{
			 e = e;
			 characterCode = e.which;
		}
		else
		{    
			 e = event;
			 characterCode = e.keyCode; 
		}
		
		if(characterCode == 13)
		{
			 if(mobileValidate(par)){
			 	dndChk();
			 }
			 
		}
		
	}
	
	function signup(e,par)
	{
	 	var characterCode;
		if(e && e.which)
		{
			 e = e;
			 characterCode = e.which;
		}
		else
		{    
			 e = event;
			 characterCode = e.keyCode; 
		}
		
		if(characterCode == 13)
		{
			if(par == "main"){
				captchaValidate();
			}
			else {
				//alert('hello');
			}
			return false;
		}
		return true;
	}
	
	function login(par){
		if(par == 'network'){
			enter(); return false;
		}
		
		var mobile = '';
		var password = '';
		var func = 'login("top")';
		if(par == 'top'){
			mobile = $('mobile').value;
			password = $('password').value;
		}
		else if(par == ''){
			mobile = $('suserMobile').value;
			password = $('suserPassword').value;
		}
		else 
		{
			mobile = $('userMobile').value;
			password = $('userPassword').value;
		}
		
		if(mobileValidate(mobile) && nameValidate(password,"your password",-1)){
			if(par == 'top')
				showLoader2('loginButt');
			else if(par == ''){
				var innerHTML = $('sloginSignIn').innerHTML;
				showLoader2('sloginSignIn');
			}
			else if($('loginSignIn')){
				var innerHTML = $('loginSignIn').innerHTML;
				showLoader2('loginSignIn');
			}
			var url = '/users/afterLogin';
			if($('refCouponCode'))par = 'redeem';
			var params = {'mobile' : mobile,'password' : password,'param' : par};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
					{		
						var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');						
						if(text == '0'){
							if(par != 'top'){
								if(par == ''){
									$('suserMobile').addClassName('err');
									$('suserPassword').addClassName('err');
									$('sUloginErrMessage').innerHTML = "Login&nbsp;Failed";
									if($('sloginSignIn')){
										$('sloginSignIn').innerHTML = innerHTML;
									}
								}
								else {
									$('userMobile').addClassName('err');
									$('userPassword').addClassName('err');
									$('UloginErrMessage').innerHTML = "Login&nbsp;Failed";
									if($('loginSignIn')){
										$('loginSignIn').innerHTML = innerHTML;
									}
								}
							}
							else {
								$('mobile').addClassName('err');
								$('password').addClassName('err');
								$('loginErrMessage').innerHTML = "Login&nbsp;Failed";
								
								$('loginButt').innerHTML = "<input type='image' onclick='"+func+"' src='/img/spacer.gif' class='otherSprite oSPos5' value='Submit' tabindex='3'>";
								$('popUpDiv').hide();
							}
						}
						else if(text == '1'){ //login success
							window.location = "http://"+siteName+"/users/view/";
						}
						else if(text == '2'){ //login success
							//nothing
							$('popUpDiv').hide();
						}
						else if(text == '3'){ //retailer login
							window.location = "http://"+siteName+"/shops/view";
						}
						else if(text == '4'){ //login success
							var cpCode = $('refCouponCode').value;
							window.location = "http://"+siteName+"/users/redeemCredits/"+cpCode;
						}
						else {
							changeLoginStatus();
							if($('regData'))
								$('regData').hide();
							
							if(text != ''){
								$('messagePopUpDiv').innerHTML = text;
								text.evalScripts();
					  			centerPos('popUpDiv');
							}else {
								$('popUpDiv').hide();
							}
						}
					}
			});
		}
	}
	
	function asynchronousCall(random){
		var url = '/groups/asynchronousCall';
		var params = {'random' : random};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params});		
	}
	
	function register(){
		popupSwap();
		var url = '/users/register';
		var params = {};
		//$('messagePopUpDiv').innerHTML = '<div align="center"><img src="/img/loader2.gif" /></div>';
		//centerPos('popUpDiv');
		showLoader2('errMessagePopUp');
		//$('errMessagePopUp').innerHTML = '<div align="center"><img src="/img/loader2.gif" /></div>';
		centerPos('errPopUp');
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){$('errPopUp').hide();centerPos('popUpDiv');}
		});
	}
	
	function captchaValidate(){		
		if(signUpValidation($("mobileLogin").value,$("CaptchaText").value)){
			//showLoader('loginDiv');
			
			//$('loader_reg').show();
			var url = '/users/captchaValidate';
			var params = {'mobile' : $("mobileLogin").value,'captcha': $('CaptchaText').value};
			showLoader2('regButt');
			new Ajax.Updater('loginDiv', url, {
		  			parameters: params,
		  			evalScripts:true
			});
		}
	}
	
	function restoreReg(page)
	{
		var params = {'page' : page};
		var url = '/users/restoreReg';
			showLoader2('resReg');
			new Ajax.Updater('loginDiv', url, {
					parameters: params,
		  			evalScripts:true,
		  			onComplete:function(transport){
					if(page == 'popup'){	
						$("popUpDiv").hide();
						$("loginDiv").style.width="685px";
						centerPos("popUpDiv");
					}
				}
			});
	}
	
	function itsfine(){		
		
			var url = '/users/itsfine';
			var params = {};
			$('notice').hide();
			$('tabsBgMainNotice').removeClassName('tabsBgMainNotice');
			$('tabsBgMainNotice').addClassName('tabsBgMain');
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
				onSuccess:function(transport){}
				});
	}
	
	function changeLoginStatus(){
		
		var url = '/users/rightHeader';
		var params = {};
		
		new Ajax.Updater('rightHeaderSpace', url, {
	  			parameters: params,
	  			evalScripts:true
		});
	}
	
	function showLoader(id){
		$(id).innerHTML = '<div id="loader1" class="loader1">&nbsp;</div>';
	}
	
	function showLoader2(id){
           	$(id).innerHTML = '<div id="loader2" class="loader2">&nbsp;</div>';
	}
	
	function ajax403Handling(){
		popupSwap();
		if($('errMessagePopUp')){
			showLoader2('errMessagePopUp');		
			centerPos('errPopUp');
		}
		if($('loader1'))$('loader1').hide();
    	if($('loader2'))$('loader2').hide();
		if($('subscribePopup')){
			closeSubscribe(1);
			if($('sub_butt1'))$('sub_butt1').innerHTML = '<a href="javascript:void(0);" class="retailBut enabledBut" onclick="subPackage()">Ok</a>';
		}
		$('messagePopUpDiv').innerHTML = $('login_user').innerHTML;
		if($('errPopUp'))$('errPopUp').hide();
		$('message').innerHTML = "<div class='popupTitle color2 popupTitlePadd'>Please login to continue ..</div>";
    	$('message').show();
    	centerPos('popUpDiv');
	}
	
	function getPage(num){
		var url = '/users/fetchMorePackages';
		var params = {'id' : $('category').value,'page':num,'count':$('packageCount').value};
		showLoader('filterPackages'); 
		window.scrollTo(0,0);	//for scrolling to top			
		new Ajax.Updater('filterPackages', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
					$('pageNum').value = num;
				}
		});
	}
	
	function getPackage(name){
		var url = '/packages/getPackage';
		var params = {'id' : name};
		showLoader('filterPackages'); 
		new Ajax.Updater('filterPackages', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
					getBreadCrumb('catName',1);
					//$('catName').innerHTML = "<a href='javascript:void(0);' onclick='getPage("+$('pageNum').value+")'>" + $('cat_'+$('category').value).innerHTML + "</a>";
					//$('back').innerHTML = "<a href='javascript:void(0);' onclick='getPage("+$('pageNum').value+")'> Back </a>";
				}
		});
		
	}
	
	function getBreadCrumb(bid, lflag){//link flag .. to give link or not
		var html = '';
		var abc = $$('ul#innerul li input');
		var flag = false;
		var obj = $('cat_'+$('category').value);
		for (var i=0; i<=(abc.length-1); i++) {
			if(abc[i].type == "hidden" && abc[i].value.indexOf($('category').value + ",") != -1){
				var id = abc[i].id.substring(3);
				html = "<a href='/categories/view/"+$(id).innerHTML.toLowerCase()+"'>" + $(id).innerHTML +  "</a>" + " >> ";
				if(lflag == 0){
					html += obj.innerHTML;
				}
				else {
					html += "<a href='javascript:void(0);' onclick='getPage("+$('pageNum').value+")'>" + obj.innerHTML +  "</a>";
				}
				flag = true;
				if($('Pagedesc')){
					var desc = obj.innerHTML;
					if($('category').value == 40){
						desc = 'Everyone understands the importance of "Luck Factor" in life. Surely our karma has an important role for success. However, Luck is equally essential to be on our side. So, why not find out what stars have to say about us everyday just by subscribing to daily horoscope.<br/>Choose your sun-sign and subscribe for daily horoscope on your mobile.';
					}
					else if($('category').value == 42){
						desc = 'Bollywood Fan? How about knowing what is happening in the life of your favourite stars right on SMS.<br/>Choose your favourite celebrity and receive all important news , gossip, personal life details daily on SMS.';
					}
					else if($('category').value == 50){
						desc = 'Celebrities are tweeting all the time about their views, opinions, personal life and almost about everything.<br/>This is simply fun to know what Salman is doing Or Katrina is making her next hot move.<br/>Or it is really useful to follow media persons as they are the first to break news on twitter.<br/>Subscribe now to your favourite twitter celeb and bring buzz on your life.';
					}
					else if($('category').value == 53){
						desc = 'Find out what is happening in the industry of your choice. Select the industry you wish to keep a watch on. This could be from the perspective of either career or investment or both.You can choose the industry which you belong or willing to invest.<br/>Subscribe to your preferred industry and start getting the important news about it.';
					}
					$('Pagedesc').innerHTML = desc;
				}
				break;
			}
		}
		if(!flag){
			if(lflag == 0){
				html += obj.innerHTML;
			}
			else {
				html += "<a href='javascript:void(0);' onclick='getPage("+$('pageNum').value+")'>" + $('cat_'+$('category').value).innerHTML +  "</a>";
			}
		}
		
		$(bid).innerHTML = html;
	}
	
	function fetchPackages(obj,catid){
		$('category').value = catid;
		changeInnerTabClass(obj);
		var url = '/users/fetchPackages';
		var params = {'data' : catid};
		new Ajax.Updater('filterPackages', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ 
	  				$('packageCount').value = $('count').value;
	  				$('pageNum').value = 1;
	  				obj.removeClassName('loader');
	  				window.scrollTo(0,0);
	  			}
		});
	}
	
	function subPackageTrial(id,type,obj){
		
		popupSwap();
		showLoader2('errMessagePopUp');		
		centerPos('errPopUp');
		var url = '/users/beforeSubscribe';
		var params = {'id' : id,'type' : type,'trial' : 1};
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
					$('errPopUp').hide();
		  			centerPos('popUpDiv');
					//setTimeout(function(){ $('popUpDiv').hide();}, 4000);
				}							
		});
	}
	
	function subPackage(id,type,obj){
		popupSwap();
		showLoader2('errMessagePopUp');		
		centerPos('errPopUp');
		
		var url = '/users/beforeSubscribe';
		var params = {'id' : id,'type' : type, 'trial' : 0};
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
					$('errPopUp').hide();
		  			centerPos('popUpDiv');
					//setTimeout(function(){ $('popUpDiv').hide();}, 4000);
				}							
		});
	}
	
	function morePackages(){
		
		changeTabClass($$('li#lipack a')[0]);
		window.scrollTo(0,0);
		var url = '/users/package';
		var params = {};
		new Ajax.Updater('pageContent', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ 
	  				
	  			}
		});
		/*var url = '/users/packageGallery';
		window.location = url;*/
	}
	
	function showPackage(cat,pack){
		
		changeTabClass($$('li#lipack a')[0]);
		window.scrollTo(0,0);
		var url = '/users/package';
		var params = {'category' : cat, 'package' : pack};
		new Ajax.Updater('pageContent', url, {
	  			parameters: params,
	  			evalScripts:true
		});
		/*var url = '/users/packageGallery';
		window.location = url;*/
	}
	
	function changeButt(id) {
		if($(id)){
			$(id).childElements().invoke("removeClassName", "butSubscribe");
			$(id).childElements().invoke("addClassName", "butUnSubscribe");
		}		
	}
	
	function submitPackage(){
		var url = '/packages/subscribe';
		var random = Math.floor(Math.random()*100000000);
		var params = {'package' : $('packageName').value, 'random' : random, 'trial' : $('trialPack').value};
		if($('subOkButt'))
		{
			showLoader2('subOkButt');
		}
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
	  				centerPos('popUpDiv');
	  				asynchronousCall(random);
	  			}
		});		
	}
	
	function unsubPackage(){
		var url = '/packages/unsubscribe';
		var params = {'package' : $('packageName').value};
		if($('unsubOkButt'))
		{
			showLoader2('unsubOkButt');
		}
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ 
	  				centerPos('popUpDiv');
	  				//setTimeout(function(){ $('popUpDiv').hide();}, 4000);
	  			}
		});
	}
	
	function emptySMSPage(){
		if($('textContent')) {
			$('textContent').value = '';
			countCharacters('textContent','charCount');
			//$('checkAll').checked = '';
			//selectAll($('checkAll'),'friendsTableDiv','data[Friendlist][id]');
			$('nickName').value = '';
			$('friendMobile').value = '';
		}
	}

	function reloadBalance(bal){
		if($('UserBalance')){
	 		$('UserBalance').innerHTML = 'Balance : <span><img class="rupee1" src="/img/rs.gif"/></span>' + bal +'&nbsp;&nbsp;<span style="color:#CCC;font-weight:normal">|</span>&nbsp;&nbsp; <a href="/users/paynow">Pay here</a>';
	 	}
	 }
	
	function reloadShopBalance(bal){
		if($('UserBalance')){
	 		$('UserBalance').innerHTML = 'Balance : <span><img class="rupee1" src="/img/rs.gif"/></span>' + bal +'&nbsp;&nbsp;<span style="color:#CCC;font-weight:normal">';
	 	}
	 }
	
	function allTransactions(){
		popupSwap();
		showLoader2('errMessagePopUp');		
		centerPos('errPopUp');

		var url = '/transactions/getAlltransactions';
		var params = {};
		//showLoader2('messagePopUpDiv');		
		//$('messagePopUpDiv').innerHTML = '<div align="center"><img src="/img/loader2.gif" /></div>';
		new Ajax.Updater('messagePopUpDiv', url, {
  			parameters: params,
  			evalScripts:true,
  			onComplete: function(response){
				$('errPopUp').hide();
  				centerPos('popUpDiv');
  			}
	});
	
	}
	
	function allMessageLogs(par){
		$('popUpDiv').hide();
		var url = '/logs/userMessageLogs';
		var params = {'par': par};
		showLoader2('errMessagePopUp');	
		//$('messagePopUpDiv').innerHTML = '<div align="center"><img src="/img/loader2.gif" /></div>';
		centerPos('errPopUp');
		//centerPos('popUpDiv');
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
					$('errPopUp').hide();
	  				centerPos('popUpDiv');
	  			}
		});
	}
	
	function allPendingTasks(){
		$('popUpDiv').hide();
		var url = '/users/draft';
		var params = {};
		showLoader2('errMessagePopUp');		
		//$('messagePopUpDiv').innerHTML = '<div align="center"><img src="/img/loader2.gif" /></div>';
		//$('popUpDiv').show();
		centerPos('errPopUp');
		//centerPos('popUpDiv');
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){
					$('errPopUp').hide();
	  				centerPos('popUpDiv');
	  			}
		});
	}
	
	function allActivePacks(){
		var url = '/packages/getAllActive';
		var params = {};
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ centerPos('popUpDiv');}
		});
	}
	
	function allExpiredPacks(){
		var url = '/packages/getAllExpired';
		var params = {};
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ centerPos('popUpDiv');}
		});
	}
	
	function deleteDraft(id){
	
		Element.remove('draft_'+id);
		var url = '/users/deleteDraft';
		var params = {'id' : id};
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ centerPos('popUpDiv');}
		});
	}
	
	function refreshDashBox(obj,num){
		$('dashClick').hide();
		var title = $('dashTitle'+num).innerHTML.strip();
		var titles = new Array(6);
		
		titles["News"] = "news";
		titles["Bollywood"] = "bolly";
		titles["Twitter"] = "tweet";
		titles["Cricket"] = "cricket";
		titles["Market"] = "market";
		titles["Tips"] = "tips";
		 
		var url = '/users/refreshDashBox';
		var params = {'par' : titles[title]};
		$('dashLoader'+num).innerHTML = '<img src="/img/loader2.gif" />';
		new Ajax.Updater('dashMsgs'+num, url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ $('dashLoader'+num).innerHTML = '';}
		});
	}
	
	function closePopUp(){
		$('popUpDiv').hide();
		$('messagePopUpDiv').innerHTML = '';
		if($('mobile'))
	    	$('mobile').focus();
		
		if($('bg'))$('bg').hide();
	}
	
	function closePopUp1(obj){
		//alert(obj);
		Effect.Shrink(obj,{'direction':'top-left'});

	}
	
	function selectAll(obj,divId,name){
		var elems = $$('div#'+divId+ ' input');
		var len = elems.length;
		if(obj.checked){
			for (var i=0;i<len;i++){
				if(elems[i].name == name){
					elems[i].checked = true;
				}
			}
		}else {
			for (var i=0;i<len;i++){
				if(elems[i].name == name){
					elems[i].checked = false;
				}
			}
		}
		
		selectFriend(divId,name);
	}
	
	function selectFriend(divId,name){
		var elems = $$('div#'+divId+ ' input');
		var len = elems.length;
		var num = 0;
		var frds = '';
		for (var i=0;i<len;i++){
			if(elems[i].name == name && elems[i].checked){
				num++;
				frds = frds + friendHTMLFormat($('nickname_'+elems[i].value).innerHTML);
			}
		}
		
		/*if(num > 0) {
			$('defMsg').innerHTML = "Message will be sent to : ";
		}	
		else {
			$('defMsg').innerHTML = "";
		}*/ 
		frds = trim(frds," ");
		if (frds == "")
			frds = 'Select friend(s) from the Friend List.';
		$('numFriends').innerHTML = frds;
	}
	
	function friendHTMLFormat(name){
		var html = '<div class="taggLink taggLink1">'
            			+'<div class="taggLinkBG">'
                			+'<div class="taggLinkBorder">'
                  				+'<div class="taggLinkCont">';
        html = html + name;
        html = html          	+'</div>'
        					+'</div>'
        				+'</div>'
        			+'</div>';
        return html;
	}
	
	function forgetPassword(){
		popupSwap();
		var url = '/users/forgotPassword';
		var params = {};
		showLoader2('errMessagePopUp');
		//$('errMessagePopUp').innerHTML = '<div align="center"><img src="/img/loader2.gif" /></div>';
		centerPos('errPopUp');
		//centerPos('popUpDiv');
		
		new Ajax.Updater('messagePopUpDiv', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(response){ $('errPopUp').hide(); centerPos('popUpDiv');}
		});
	
	}
	
	function popupSwap(){
		if($('popUpDiv'))
			$('popUpDiv').hide();		
	}
	function resendPassword()
	{
		var mobile = $('userMobile').value;
		
		var url = '/users/resendPassword';		
		var params = {'mob' : mobile};
		showLoader2('resPass');
		//$('messagePopUpDiv').innerHTML = '<div align="center"><img src="/img/loader2.gif" /></div>';
		//centerPos('popUpDiv');
		new Ajax.Updater('responseMessage', url, {
	  			parameters: params,
	  			evalScripts:true,
	  			onComplete: function(transport){ 
			$('resPass').innerHTML = '&nbsp;or&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="resendPassword();">Re-send Password</a></div>';
			//alert(transport.responseText);
			//centerPos('popUpDiv'); 
			
				}
		});		
	}
	
	function trim(str, chars) {
		return ltrim(rtrim(str, chars), chars);
	}
 
	function ltrim(str, chars) {
		chars = chars || "\\s";
		return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
	}
	 
	function rtrim(str, chars) {
		chars = chars || "\\s";
		return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
	} 
	
	function openPopup(bitly){
    	var url = '/users/twitt/'+bitly;
    	newwindow=window.open(url,'name','height=600,width=800,left=600,top=150');
		if (window.focus) {newwindow.focus()}
    
    }
	
var currSMS = 1;

function showSample(direction)
{	
	var id = $('msgNum').value;
	var msg = $$('ul.recentMsgsCont li');
	var len = msg.length;
	if(direction == 'pre'){
		if(id > 1) {
			id--;
		}
		else id = len;
	}
	if(direction == 'next'){
		if(id < len) {
			id++;
		}
		else id = 1;
	}
	
	for(var i =1;i<=len;i++){
		$('sampSMS'+i).hide();
	}
	$('sampSMS'+id).show();
	$('msgNum').value = id;
	
	
	/*var counter = $$('ul.sampleNo li');
	counter = parseInt((counter.length) - 3);	
	if ((id != 0) && (currSMS != id)) // If no. not click and curr sms not clicked
	{	
		changeSample(id);
	}
	else if (currSMS != id)
	{
		if(direction == 'pre') { // If 1 than go to no 5 else come back			
			if (currSMS == 1) {
				changeSample(counter);
			}				
			else {
				id = parseInt(currSMS);changeSample(id-1);
			}
		}			
		else {			
			if (currSMS == counter) {
				changeSample(1);
			} else {				
				id = parseInt(currSMS); id += 1;
				changeSample(id); 
			}
		}		
	}*/
}

/*function changeSample(id)
{	
	var samples = $$('ul.sampleNo');	
	var sampleNo = $$('ul.sampleNo li a');
	for (var i=0; i<=(sampleNo.length-1); i++) {
		$(sampleNo[i]).removeClassName('selectedSample');		
	}	
	var SMS = "sampSMS";
	$(SMS+currSMS).hide();
	//$(SMS+currSMS).fade( { queue: 'front', duration: 1.0, from: 1, to: 0 } );		
	samples[0].addClassName('samples');
	sampleNo[id].addClassName('selectedSample');
	currSMS = id;
	$(SMS+id).appear( { queue: 'end', duration: 1.0, from: 0, to: 1 } );	
	
	setTimeout(
		function(){ samples[0].removeClassName('samples');}, 100);
	
}*/
	
	function mobileVerCodeSend(){
    	var dnd = $('dnd').value;
		if(dnd.strip() == ''){
			$('dnd').focus();
			alert("Please enter your mobile number");
		}else if(mobileValidate(dnd)){
			var tobeblocked = $('tobeblocked').value;
			var re = /[^\s,0-9]/g;
			if(tobeblocked.strip() == ''){
				$('tobeblocked').focus();
				alert('Please enter the mobile number(s) to be blocked');
				return false;
			}else if (re.test(tobeblocked)){
				$('tobeblocked').focus();
				alert("Please enter proper mobile numbers and separate the numbers by comma(,). No other special character is allowed" );
				return false;
			}else{
				tobeblockedArr = Array();				
				tobeblockedArr = $('tobeblocked').value.split(",");
				var arrCnt1 = tobeblockedArr.length;
				tobeblockedArrAct = Array();
				var l = 0;
				for(var k=0;k<arrCnt1;k++){
					if(tobeblockedArr[k].strip() != ""){
						tobeblockedArrAct[l] = tobeblockedArr[k].strip();
						l = l+1;
					}
				}
				
				
				var arrCnt = tobeblockedArrAct.length;
				
				if (arrCnt > 10)
				{
					$('tobeblocked').focus();
    				alert("Maximum 10 numbers are allowed at a time");
    				return false;
				}
				
				for(var k=0;k<arrCnt;k++){
					if (tobeblockedArrAct[k].length != 10)
  					 {
  					 	$('tobeblocked').focus();
        				alert("Your mobile number "+tobeblockedArrAct[k]+" should be a 10 digit number");
        				return false;
   					}
   					if (tobeblockedArrAct[k].charAt(0)!="9" && tobeblockedArrAct[k].charAt(0)!="8" && tobeblockedArrAct[k].charAt(0)!="7")
					{
						$('tobeblocked').focus();
					    alert("Your mobile number "+tobeblockedArrAct[k]+" should start with 9, 8 or 7");
					    return false;
					}
   		  		
			  		
		  		}
				if($('captchaCode').value.strip() == ''){
					$('captchaCode').focus();
					alert('Please enter word verification code');
				}else{										
					var url = '/users/dndverification';
					var params = {'code' : $('captchaCode').value.strip(),'number':dnd.strip(),'tobeblocked':tobeblocked.strip()};
					
					var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
					onSuccess:function(transport)
							{	
								var res  = transport.responseText.split('^^^^');	
								if(res[1] == '0'){
									$('dnderrordiv').show();
									$('dnderrordiv').innerHTML = res[0];
								}else{
									$('blockform').innerHTML = res[0];
								}
								
							}
					});
				}
			}
			
		}else{
				$('dnd').focus();
		}
	}
	function sendVerCode(){
		var url = '/users/resenddndcode';
		var params = {'number':1};
		
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
		onSuccess:function(transport)
				{	
					alert('Verification code has been sent again to the mentioned mobile number');
					
				}
		});
	}
	
	function dndblock(){
		var mobileVerCode = $('mobileVerCode').value;
		if(mobileVerCode.strip() == ''){
			$('mobileVerCode').focus();
			alert("Please enter the verification code");
			return false;
		}
		
		var url = '/users/dndblock';
		var params = {'code':mobileVerCode};
		
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
		onSuccess:function(transport)
				{	
						var res  = transport.responseText.split('^^^^');	
						if(res[1] == '0'){
							$('dnderrordiv').show();
							$('dnderrordiv').innerHTML = res[0];
						}else{
							$('blockform').innerHTML = res[0];
						}
					//alert('Verification code has been sent again to the mentioned mobile number');
					//$('blockform').innerHTML = transport.responseText;
					
				}
		});
		
	}
	
	function amtSelect(amt){
		document.getElementById('amountobepaid').value = amt;	
	}
	
	function encodeTxnRequest()	
	{
		$('paymentSubmit').innerHTML = 'Redirecting to a secure Payment Gateway page...';
		var url    = '/users/prepayment/';	
		var rand   = Math.random(9999);
		var pars   = "amount="+document.getElementById('amountobepaid').value+"&rand="+rand;
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
						onSuccess:function(transport)
						{
							var id = transport.responseText;
							if(id != 'fail'){
							var arrVar=new Array();
							arrVar = id.split('*^!!');
							//document.getElementById('paymentGatewayDiv').innerHTML = document.getElementById('paymentGatewayDiv').innerHTML + '<input type="hidden" style="width:500px" id="requestparameter" name="requestparameter" value="201008181000020|DOM|IND|INR|'+document.getElementById('amountobepaid').value+'|t4yi8954|others|https://www.smstadka.com/users/paymentResponse/1|https://www.smstadka.com/users/paymentResponse/0|DirecPay">';
							//document.getElementById('requestparameter').value = document.getElementById('requestparameter').value.replace('t4yi8954',arrVar[0]);
							//document.getElementById('requestparameter').value = document.getElementById('requestparameter').value.replace('others',arrVar[1]);
							//document.ecom.requestparameter.value = encodeValue(document.ecom.requestparameter.value);
							document.getElementById('paymentGatewayDiv').innerHTML = '<input type="hidden" id="requestparameter" name="requestparameter" value="'+arrVar[0]+'" /><input type="hidden" id="billingDtls"" name="billingDtls"" value="'+arrVar[1]+'" /><input type="hidden" id="shippingDtls" name="shippingDtls" value="'+arrVar[2]+'" />';
							document.ecom.submit();
							document.getElementById('paymentGatewayDiv').innerHTML = '';
							}else{
								alert('Server error, please try again');
							}
						}
				});
	}
	
	function payICC()	
	{
		$('paymentSubmit').innerHTML = 'Redirecting to a secure Payment Gateway page...';
		var payAmt = $('amountobepaid').value;
		var url    = '/users/prepaymentPayICC/';	
		var rand   = Math.random(9999);
		var pars   = "rand="+rand+"&payAmt="+payAmt;
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
						onSuccess:function(transport)
						{
							var id = transport.responseText;
							if(id != 'fail'){
							document.getElementById('paymentGatewayDivICC').innerHTML = id;
							document.formPayICC.submit();
							document.getElementById('paymentGatewayDivICC').innerHTML = '';
							}else{
								alert('Server error, please try again');
							}
						}
				});
	}
	
	function payOSS()	
	{
		$('paymentSubmit').innerHTML = 'Redirecting to a secure Payment Gateway page...';
		var payAmt = $('amountobepaid').value;
		var url    = '/users/prepaymentPayOSS/';	
		var rand   = Math.random(9999);
		var pars   = "rand="+rand+"&payAmt="+payAmt;
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
						onSuccess:function(transport)
						{
							var id = transport.responseText;
							if(id != 'fail'){
							document.getElementById('paymentGatewayDivOSS').innerHTML = id;
							document.formPayOSS.submit();
							document.getElementById('paymentGatewayDivOSS').innerHTML = '';
							}else{
								alert('Server error, please try again');
							}
						}
				});
	}
	
	function getmoreDashbord()
	{
		$('getmoreDashbordLoader').innerHTML = '<img src="/img/loader2.gif" />';
		var url    = '/users/dashboard/';	
		var rand   = Math.random(9999);
		var pars   = "rand="+rand+"&upper="+$('dashMoreVal').value;
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
						onSuccess:function(transport)
						{
							var id = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
							id = id.strip();
							if(id != '0'){
								var ele = $('dashMoreVal');  
								ele.parentNode.removeChild(ele);							
								$('recentMessege').innerHTML = $('recentMessege').innerHTML + id; 
								$('getmoreDashbordLoader').innerHTML = '';
							}
							else {
								$('getMoreDashboard').innerHTML = '<li class="box6Cont2" style="background:none; border-top:1px solid #f3f2f2;border-bottom:1px solid #f3f2f2;">No more data </li>';
							}
						}
				});
	}
	
	function dashMouseOver(e,id){
		//e.down(1).next(0).down(0).next(1).down(0).show();
		id= 'hidMsg' + id;
		$(id).show();
		Element.setStyle(e,{'background':'#f2f1f1'});
		e.id = "smstadka";
		var data = $$('li#smstadka ul li.uList');
		data.each(function(e1) {
			Element.setStyle(e1,{'background':'#FFF'});	
		});
		e.id = "";
	}
	
	function dashMouseOut(e,id){
		id= 'hidMsg' + id;
		$(id).hide();
		//e.down(1).next(2).down(0).hide();
		Element.setStyle(e,{'background':'none'});
		e.id = "smstadka";
		var data = $$('li#smstadka ul li.uList');
		data.each(function(e1) {
			Element.setStyle(e1,{'background':'#F9F8F8'});	
		});
		e.id = "";
	}
	
	
	/**
	 *
	 * payment gateway
	 *
	 **/

	function encodeValue(val)
	{
		var encodedText = Base64.encode(val);
		
		var lenEncTxt = encodedText.length;

		var str1 = encodedText.slice(0,1);
		var str2 = encodedText.slice(1, lenEncTxt);

		var str3 = str1 + "T" + str2;

		var encVal = Base64.encode(str3);
		
		return encVal;
	}

	function decodeValue(val)
	{
		var decodedText = Base64.decode(val);

		var lenDecTxt = decodedText.length;

		var str1 = decodedText.slice(0,1);
		var str2 = decodedText.slice(2, lenDecTxt);

		var str3 = str1 + str2;
		
		var decVal = Base64.decode(str3);

		return decVal;
	}

	var Base64 = {
		// private property
		_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	 
		// public method for encoding
		encode : function (input) {
			var output = "";
			var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
			var i = 0;
	 
			input = Base64._utf8_encode(input);
	 
			while (i < input.length) {
	 			chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);
	 
				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;
	 
				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}
	 
				output = output +
				this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
				this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
	 		}
	 
			return output;
		},
	 
		// public method for decoding
		decode : function (input) {
			var output = "";
			var chr1, chr2, chr3;
			var enc1, enc2, enc3, enc4;
			var i = 0;
	 
			input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
	 
			while (i < input.length) {
	 			enc1 = this._keyStr.indexOf(input.charAt(i++));
				enc2 = this._keyStr.indexOf(input.charAt(i++));
				enc3 = this._keyStr.indexOf(input.charAt(i++));
				enc4 = this._keyStr.indexOf(input.charAt(i++));
	 
				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;
	 
				output = output + String.fromCharCode(chr1);
	 
				if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				}
				if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				}
	 		}
	 
			output = Base64._utf8_decode(output);
	 
			return output;
	 	},
	 
		// private method for UTF-8 encoding
		_utf8_encode : function (string) {
			string = string.replace(/\r\n/g,"\n");
			var utftext = "";
	 
			for (var n = 0; n < string.length; n++) {
	 			var c = string.charCodeAt(n);
	 
				if (c < 128) {
					utftext += String.fromCharCode(c);
				}
				else if((c > 127) && (c < 2048)) {
					utftext += String.fromCharCode((c >> 6) | 192);
					utftext += String.fromCharCode((c & 63) | 128);
				}
				else {
					utftext += String.fromCharCode((c >> 12) | 224);
					utftext += String.fromCharCode(((c >> 6) & 63) | 128);
					utftext += String.fromCharCode((c & 63) | 128);
				}
	 		}

			return utftext;
		},
	 
		// private method for UTF-8 decoding
		_utf8_decode : function (utftext) {
			var string = "";
			var i = 0;
			var c = c1 = c2 = 0;
	 
			while ( i < utftext.length ) {
	 			c = utftext.charCodeAt(i);
	 
				if (c < 128) {
					string += String.fromCharCode(c);
					i++;
				}
				else if((c > 191) && (c < 224)) {
					c2 = utftext.charCodeAt(i+1);
					string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
					i += 2;
				}
				else {
					c2 = utftext.charCodeAt(i+1);
					c3 = utftext.charCodeAt(i+2);
					string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
					i += 3;
				}
	 		}

			return string;
		}
	};
	
	/* scripts.js ends here */
	
	/* app.js starts here */
	/* Start PNR Alert APP */
	function subApp(controller,about){
		if($('liapp').className != 'sel'){
			changeTabClass($$('li#liapp a')[0]);
			window.scrollTo(0,0);
		}
		var url = '/'+controller+'s/initial';
		var params = {'about' : about};

		new Ajax.Updater('pageContent', url, {
			parameters: params,
			evalScripts:true					
		});
	}

	function showApp(controller){
		if($('createAlert')){
			$('createAlert').simulate('click');
		}
		else {
			var url = '/apps/showApp';
			var params = {'controller': controller};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
				onSuccess:function(transport)
				{
					var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
					if(text == '0'){
						window.location = '/users/view';
					}
					else {
						$('messagePopUpDiv').innerHTML = text;
						text.evalScripts();
						centerPos('popUpDiv');
					}
				}
			});
		}
	}

	function pnrValidate(y){
		if(isNaN(y)||y.indexOf(" ")!=-1)
		{
			showAPPerror('Please Enter a valid PNR number','pnrNumber');
			return false;
		}
		if (y.length != 10)
		{
			showAPPerror('Please Enter a valid PNR number','pnrNumber');
			return false;
		}
		return true;
	}

	function showAPPerror(text,id){
		$('err_pname').innerHTML = text;
		$('err_pname').show();
		$(id).addClassName('err');
		$(id).focus();
	}

	function hideAPPerror(id){
		$('err_pname').hide();
		$(id).removeClassName('err');
	}

	function sendPNR(){
		var pnr = $('pnrNumber').value;
		if(pnrValidate(pnr)){
			var html = $('sendButt').innerHTML;
			showLoader2('sendButt');
			var url = '/pnrs/validatePNR';
			var params = {'pnr': pnr};
			
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
				onSuccess:function(transport)
				{
					var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
					if(text == '0'){
						showAPPerror('PNR Number is invalid/ Facility Not Available Now','pnrNumber');
						$('sendButt').innerHTML = html;
					}
					else if(text == '1'){
						showAPPerror('Railway site is down; Please try again later','pnrNumber');
						$('sendButt').innerHTML = html;
					}
					else if(text == '2'){
						showAPPerror('PNR Alert for this ticket is already active','pnrNumber');
						$('sendButt').innerHTML = html;
					}
					else {
						$('pnrData').innerHTML = text;
					}
					
				}});
		}
	}

	function pnrAlert(){
		var title = $('pnrTitle').value;
		var mobile = $('pnrMobile').value;
		
		var url = '/pnrs/createPNRAlert';
		var params = {'pnr_number' : $("pnr_number").value,'title': title,'journey_date' : $('journey_date').innerHTML,'train_info' : $('train_info').value,'chart_status':$('chart_status').innerHTML, 'mobile' : mobile, 'train_number' : $('train_number').value, 'boarding' : $('boarding').value,'to' : $('to').value};
		var html = $('sendButt').innerHTML;
		showLoader2('sendButt');
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{
				var text = transport.responseText;
				var texts = text.split('#');
				if(texts[0] == '0'){
					$('notify').show();
					$('sendButt').innerHTML = html;
				}
				else if(texts[0] == '1'){
					$('messagePopUpDiv').innerHTML = "PNR Alert for this PNR is already active on " + texts[1];
					centerPos('popUpDiv');
					$('sendButt').innerHTML = html;
				}
				else {
					$('pnrData').innerHTML = text;
					text.evalScripts();
				}
		}});
	}
	
	function createAlert(){
		var title = $('pnrTitle').value;
		var mobile = $('pnrMobile').value;
		if((title == null) || (title.length == 0)){
			showAPPerror('Please Enter Your PNR Title','pnrTitle');
		}
		else if(title.length > 20){
			showAPPerror('Your PNR Title should be less than 20 characters','pnrTitle');
		}
		else if(mobile.length != 0 && isNaN(mobile))
		{
			showAPPerror('Your mobile number should contain numeric values','pnrMobile');
		}
		else if (mobile.length != 0 && mobile.length != 10)
	    {
			showAPPerror('Your mobile number should be a 10 digit number','pnrMobile');
	    }
		else if(mobile.length == 10){
			var html = "<div class='appTitle'>You have entered " + mobile + "</div>";
			
			html += "<div>Are you sure this is correct? If yes, click OK to proceed or CANCEL to change the mobile no.</div>" +
					"<div class='field'>" +
						"<div id='subOkButt'>" +
							"<a href='javascript:void(0);' onclick='$(\"popUpDiv\").hide();pnrAlert();' class='buttSprite leftFloat' style='margin-top: 5px;'><img src='/img/spacer.gif' class='butOk'></a>"+
							"<a href='javascript:void(0);' onclick='$(\"popUpDiv\").hide();' class='buttSprite leftFloat' style='margin-top: 5px; margin-left:10px;'><img src='/img/spacer.gif' class='butCancel'></a>"+
						"</div>"+
					"</div>";
			
			$('messagePopUpDiv').innerHTML= html;
			centerPos('popUpDiv');
		}
		else {
			pnrAlert();
		}
	}

	function checkPNRStatus(id){
		var url = '/pnrs/checkStatus';
		var params = {'id': id};
		var html = $('chart'+id).innerHTML;
		showLoader2('chart'+id);
		new Ajax.Updater('status'+id, url, {
			parameters: params,
			evalScripts:true,
			onComplete: function(response){ $('chart'+id).innerHTML = html;}
		});
	}

	function closeStatus(id){
		$('status' + id).innerHTML = "";
	}

	function removePNRAlert(id,flag){
		if(flag == 0) {
			$('messagePopUpDiv').innerHTML = "<div>You really sure, you want to remove this alert?</div>"+
					"<br><a style='margin-top:5px;' class='buttSprite rightFloat' onclick='removePNRAlert("+id+",1)' id='sendButt' href='javascript:void(0);'><img class='butOk' src='/img/spacer.gif'></a>";				
			centerPos('popUpDiv');
		}
		else {
			var url = '/pnrs/disablePnrAlert';
			var params = {'id': id};
			var html = $('sendButt').innerHTML;
			showLoader2('sendButt');
			new Ajax.Updater('messagePopUpDiv', url, {
				parameters: params,
				evalScripts:true,
				onComplete: function(response){
					centerPos('popUpDiv');
					$('flag'+id).innerHTML = 'Inactive';
					$('chart'+id).innerHTML = 'DISABLED';
					$('action'+id).innerHTML = '&nbsp;';
		  		}
			});
		}
	}
	/* End of PNR Alert APP */

	/* Start of Stock Alert APP*/ 

	function createStockAlert(){
		
		var radio_butts = document.stockForm.nseFlag;
		if(radio_butts == undefined){
			showAPPerror('Enter your company name','stockauto');
		}
		else {
			var code = '';
			if(radio_butts.length == undefined){
				code = radio_butts.value;
			}
			else {
				for(var i=0;i<radio_butts.length;i++){
					if(radio_butts[i].checked){
						code = radio_butts[i].value;
						break;
					}
				}
			}
			var radio_butts = document.stockForm.newsFlag;
			var news_flag = 0;
			for(var i=0;i<radio_butts.length;i++){
				if(radio_butts[i].checked){
					news_flag = radio_butts[i].value;
					break;
				}
			}
			var random = Math.floor(Math.random()*100000000);
			var url = '/stocks/createStockAlert';
			var params = {'stock_code' : code, 'company' : $('stockauto').value, 'news_flag' : news_flag,'random' : random};
			var html = $('sendButt').innerHTML;
			showLoader2('sendButt');
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
				onSuccess:function(transport)
				{
					var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
					if(text == '0'){
						//showAPPerror('recharge','stockauto');
						$('stockRecharge').show();
						$('sendButt').innerHTML = html;
					}
					else if(text == '1'){
						showAPPerror('already active','stockauto');
						$('sendButt').innerHTML = html;
					}
					else if(text == '2'){
						showAPPerror('oops!! something went wrong','stockauto');
						$('sendButt').innerHTML = html;
					}
					else {
						$('stockData').innerHTML = text;
						text.evalScripts();
						if(document.stockForm)
						document.stockForm.reset();
						refreshRHSStock();
						asynchronousCall(random);
					}
				}});
		}
	}

	function closeAppBox(id){
		$('code'+id).hide();
	}

	function subscribeScriptNews(id){
		$('codeDiv'+id).innerHTML = 'You will receive news related to this stock as it happens. Price<span><img src="/img/rs.gif" class="rupee1"></span>'+APP_STOCK_PRICE_WITH_NEWS+'per day. '+
						'Click Ok to continue.<br/>'+
						'<a style="margin-top:5px;" class="buttSprite rightFloat" onclick="editScript('+id+',1)" id="sub'+id+'" href="javascript:void(0);"><img class="butOk" src="/img/spacer.gif"></a>';					
		$('code'+id).show();
	}

	function unsubscribeScriptNews(id){
		$('codeDiv'+id).innerHTML = 'You will only receive daily price summary on market days. Click Ok to continue.<br/><div style="margin-top:10px;" class="fieldLabelSpace2 rightFloat" id="sub'+id+'">'+
		                            '<input type="image" onclick="editScript('+id+',0)" class="otherSprite oSPos7" src="/img/spacer.gif">'+
		                         '</div>';
		$('code'+id).show();
	}

	function editScript(id,news_flag){
		var url = '/stocks/editStockAlert';
		var random = Math.floor(Math.random()*100000000);
		var params = {'id' : id,'news_flag' : news_flag,'random' : random};
		showLoader2('sub'+id);
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{
				var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
				
				var innerHTML = "";
				if(text == '0'){
					innerHTML = "Oops!! Something went wrong";
				}
				else {
					if(news_flag == 0){
						innerHTML = "You have successfully stopped news for this alert";
					}
					else if(news_flag == 1){
						innerHTML = "You have successfully started news for this alert";
						text.evalScripts();
						asynchronousCall(random);	
					}
					window.setTimeout('refreshRHSStock()',1000);
				}
				
				$('codeDiv'+id).innerHTML = innerHTML;
			}
		});
	}


	function unsubscribeScript(id){
		$('codeDiv'+id).innerHTML = 'Do you wish to pause this stock alert?'+
									'<br><a style="margin-top:5px;" class="buttSprite rightFloat" onclick="disableScript('+id+')" id="sub'+id+'" href="javascript:void(0);"><img class="butOk" src="/img/spacer.gif"></a>';
		$('code'+id).show();
	}

	function disableScript(id){
		var url = '/stocks/disableStockAlert';
		var params = {'id' : id};
		showLoader2('sub'+id);
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{
				var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
				
				var innerHTML = "";
				if(text == '0'){
					innerHTML = "Oops!! Something went wrong";
				}
				else if(text == '1'){
					innerHTML = "You have successfully paused this alert";
					window.setTimeout('refreshRHSStock()',1000);
				}
				
				$('codeDiv'+id).innerHTML = innerHTML;
			}
		});
	}

	function subscribeScript(id){
		$('codeDiv'+id).innerHTML = 'Do you wish to restart this stock alert?'+
									'<br><a style="margin-top:5px;" class="buttSprite rightFloat" onclick="enableScript('+id+')" id="sub'+id+'" href="javascript:void(0);"><img class="butOk" src="/img/spacer.gif"></a>';
		$('code'+id).show();
	}

	function enableScript(id){
		var url = '/stocks/enableStockAlert';
		var random = Math.floor(Math.random()*100000000);
		var params = {'id' : id,'random': random};
		showLoader2('sub'+id);
		
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{
				var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
				var innerHTML = "";
				if(text == '0'){
					innerHTML = "oops!! Something went wrong";
				}
				else if(text == '1'){
					innerHTML = "Sorry you don't have enough balance for starting this alert !!";
				}
				else {
					innerHTML = "Congrats!! You have successfully started this alert";
				}
				
				$('codeDiv'+id).innerHTML = innerHTML;
				if(text == 0 || text == 1){
					/*window.setTimeout(function(id) {$('code'+id).hide();}, 1000);*/
				}
				else {
					text.evalScripts();
					asynchronousCall(random);
					window.setTimeout('refreshRHSStock()',1000);
				}
				
			}
		});
	}

	function changeHideStock(text,li){
		var txt = '';
		if (document.all) // IE Stuff
		{
		   txt = li.innerText;   
		} 
		else // Mozilla does not work with innerText
		{
		   txt = li.textContent;
		}
		$('stockauto').value = txt;
		var codes = (li.id).split('_');
		var nse_val = '';
		var bse_val = '';
		var html = '';
		var checked = 'checked="checked"';
		
		if(codes[0].substr(0,1) == '1'){
			nse_val = "1" + codes[0].substr(1);
			html += '<input '+checked+' name="nseFlag" type="radio" value="'+nse_val+'" style="margin:0"> <span style="margin-right:15px">NSE</span>';
		}
		if(codes[1].substr(0,1) == '1'){
			bse_val = "0" + codes[1].substr(1);
			if(nse_val != '')
				checked = "";
			html += '<input '+checked+' name="nseFlag" type="radio" value="'+bse_val+'" style="margin:0"> <span>BSE</span>';
		}
		
		$('nsebseflag').innerHTML = html;
		hideAPPerror('stockauto');
	}

	function refreshRHSStock(){
		var url = '/stocks/getRightSide';
		var params = {};
		
		new Ajax.Updater('appColRight', url, {
	  			parameters: params,
	  			evalScripts:true
		});
	}

	function deleteScript(id){
		$('codeDiv'+id).innerHTML = 'Are you sure you wish delete this script?'+
		                            '<br><a style="margin-top:5px;" class="buttSprite rightFloat" onclick="deleteScriptPerm('+id+')" id="sub'+id+'" href="javascript:void(0);"><img class="butOk" src="/img/spacer.gif"></a>';	                            
		$('code'+id).show();
	}

	function deleteScriptPerm(id){
		var url = '/stocks/removeAlert';
		var params = {'id' : id};
		showLoader2('sub'+id);
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{
				var text = transport.responseText.replace(/<!--[^(-->)]+-->/g, '');
				var	innerHTML = "You have successfully deleted this alert";
				$('codeDiv'+id).innerHTML = innerHTML;
				window.setTimeout('refreshRHSStock()',1000);
			}
		});
	}


	function moreActiveStocks(){
		var url = '/stocks/getActiveStocks';
		var params = {'last': $('numAct').value};
		var html = $('viewMoreAct').innerHTML;
		showLoader2('viewMoreAct');
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{
				Element.insert($('viewMoreAct').parentNode, {
				  before: transport.responseText
				});
				$('viewMoreAct').innerHTML = html;
			}});
	}

	function moreInActiveStocks(){
		var url = '/stocks/getInActiveStocks';
		var params = {'last': $('numInAct').value};
		var html = $('viewMoreInAct').innerHTML;
		showLoader2('viewMoreInAct');
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{
				Element.insert($('viewMoreInAct').parentNode, {
				  before: transport.responseText
				});
				$('viewMoreInAct').innerHTML = html;
			}});
	}

	/* End of Stock Alert APP */

	/* reminder app*/
	function appRemRepeat(val){
		if(val =='1'){
			$('appRemRepeatType').innerHTML = 'Days';
		}else if(val =='2'){
			$('appRemRepeatType').innerHTML = 'Weeks';
		}else if(val =='3'){
			$('appRemRepeatType').innerHTML = 'Months';
		}else if(val =='4'){
			$('appRemRepeatType').innerHTML = 'Years';
		}	
		
		if(val =='2'){
			$('appRemWeekBox').show();
		}else{
			$('appRemWeekBox').hide();
		}
		
	}

	function appMobileValidate(y){
	  var errMsg = '';
	  if(y == ''){
		  errMsg = 'Mention a 10 digit mobile No.';
	  }else if(isNaN(y)||y.indexOf(" ")!=-1){
		  errMsg = 'Mobile number should contain numeric values';
	   }else if (y.length != 10){
		  errMsg = 'Mobile no. should be a 10 digit number';
	   }else if (y.charAt(0)!="9" && y.charAt(0)!="8" && y.charAt(0)!="7")
	   {
		  errMsg = 'Mobile number should start with 9, 8 or 7';                
	   }
	  	return errMsg;
	}

	function remValidate(y){
		$errFlag = 0;
		var tobesent = $('AppRemFor').value;		
		if(get_r_receivers($('r_receivers')) == '' && tobesent.strip() == ''){
			$('appRemFor_err').show();
			$('appRemFor_err').innerHTML = 'Please select/enter at least one mobile number.';
			$('AppRemFor').addClassName('err');
			$errFlag = 1;
			//return false;			
		}else if(tobesent.strip() != ''){
			var re = /[^\s,0-9]/g;
			if (re.test(tobesent)){
				$('appRemFor_err').show();
				$('appRemFor_err').innerHTML = 'Please enter proper mobile numbers and separate the numbers by comma (,). No other special character is allowed.';
				$('AppRemFor').addClassName('err');
				$errFlag = 1;
			}else{
				tobesentArr = Array();				
				tobesentArr = tobesent.split(",");
				var arrCnt1 = tobesentArr.length;
				tobesentArrAct = Array();
				var l = 0;
				for(var k=0;k<arrCnt1;k++){
					if(tobesentArr[k].strip() != ""){
						tobesentArrAct[l] = tobesentArr[k].strip();
						l = l+1;
					}
				}
				
				var arrCnt = tobesentArrAct.length;
				var lenErrStr = '';
				var startErrStr = '';
				var finalErrStr = '';
				for(var k=0;k<arrCnt;k++){
					
					if (tobesentArrAct[k].length != 10){
						lenErrStr += "<"+tobesentArrAct[k]+">";        				
   					}
   					
					if (tobesentArrAct[k].charAt(0)!="9" && tobesentArrAct[k].charAt(0)!="8" && tobesentArrAct[k].charAt(0)!="7"){
   						startErrStr += "<"+tobesentArrAct[k]+">";
					}   			   							  		
		  		}
				
				if(lenErrStr != ''){
					finalErrStr += "Your mobile number(s) "+lenErrStr+" should be a 10 digit number. </br></br>"; 
				}
				
				if(startErrStr != ''){
					finalErrStr += "Your mobile number(s) "+startErrStr+" should start with 9, 8 or 7"; 
				}
				
				if(finalErrStr != ''){
					$('AppRemFor').focus();
					$('appRemFor_err').show();
					$('appRemFor_err').innerHTML = finalErrStr;
					$('AppRemFor').addClassName('err');
					$errFlag = 1;
				}else{
					$('appRemFor_err').hide();
					$('appRemFor_err').innerHTML = '';
					$('AppRemFor').removeClassName('err');
				}
			}								
		}else{			
			$('appRemFor_err').hide();
			$('appRemFor_err').innerHTML = '';
			$('AppRemFor').removeClassName('err');
		}
		
		if($('appRemMsg').value.strip() == "Your message here" || $('appRemMsg').value.strip() == ""){		
			$('appRemMsg_err').show();
			$('appRemMsg_err').innerHTML = 'Please enter message.';
			$('appRemMsg').addClassName('err');
			$errFlag = 1;
			//return false;
		}else{
			$('appRemMsg_err').hide();
			$('appRemMsg_err').innerHTML = '';
			$('appRemMsg').removeClassName('err');		
		}
				
		if(remindWhen() == 'l'){
			if($('appRemDate').value.strip() == ""){		
				$('appRemDateTime_err').show();
				$('appRemDateTime_err').innerHTML = 'Please select date.';
				$('appRemDate').addClassName('err');
				$errFlag = 1;
				//return false;
			}else{
				$('appRemDateTime_err').hide();
				$('appRemDateTime_err').innerHTML = '';
				$('appRemDate').removeClassName('err');		
			}
			if($('appRemRepeatChk').checked == true){
				if($('appRemRepeatBy').value == "2"){								
					var chkCnt = 0;
					var chkLength = document.getElementsByName('appRemWeekday').length;
					for (var i=0; i < chkLength; i++){
					   if (document.getElementsByName('appRemWeekday')[i].checked)			      
						   chkCnt++;			   
					}
				
					if(chkCnt < 1){	//checkbox check
						$('appRemWeekday_err').show();
						$('appRemWeekday_err').innerHTML = 'Specify a day of the week';
						$errFlag = 1;
						//return false;
					}else{
						$('appRemWeekday_err').hide();
						$('appRemWeekday_err').innerHTML = '';				
					}
				}
				
				var radioChecked = '';

				
				if($('appRemEndRadioN').checked == true)
					radioChecked = 'never';
				
				if($('appRemEndRadioU').checked == true)
					radioChecked = 'until';
				
				
				if(radioChecked == "until"){		
					if($('appRemEndDate').value.strip() == ""){		
						$('appRemEndDate_err').show();
						$('appRemEndDate_err').innerHTML = 'Specify an end date';
						$('appRemEndDate').addClassName('err');
						$errFlag = 1;
						//return false;
					}else{
						$('appRemEndDate_err').hide();
						$('appRemEndDate_err').innerHTML = '';
						$('appRemEndDate').removeClassName('err');		
					}
				}else{
					$('appRemEndDate_err').hide();
					$('appRemEndDate_err').innerHTML = '';
					$('appRemEndDate').removeClassName('err');
				}		
			}
		}
		
		if($errFlag == 0)
		return true;
		else
		return false;
	}

	function setReminder(){
		$('appRemAjaxErr').innerHTML = '';
		$('appRemAjaxErr').hide();
		if(remValidate()){
			var reminderAdd = $('appReminderAddDiv').innerHTML;
			var html = $('sendButt').innerHTML;
			showLoader2('sendButt');
			
			var appRemMsg = $('appRemMsg').value;
			var appRemFor = '';
			var varRemindWhen = remindWhen();
			var appRemDate = $('appRemDate').value;
			var appRemTime = $('appRemTime').options[$('appRemTime').selectedIndex].value;
			
			var	appRemRepeatChk = '';
			var appRemRepeatBy = '';
			var appRemRepeatFreq = '';
			var appRemWeekdayStr = '';
			var appRemEndRadio = '';
			var appRemEndDate = '';
			
			/*if($('appRemForRadioM').checked == true)
				appRemFor = 'me';
			
			if($('appRemForRadioO').checked == true){
				appRemFor = $('AppRemFor').value;			
			}*/	
			appRemFor =  $('AppRemFor').value+','+get_r_receivers($('r_receivers'));
			
			if(varRemindWhen == 'l'){
				if($('appRemRepeatChk').checked == true){
					appRemRepeatChk = 1;
					appRemRepeatBy = $('appRemRepeatBy').options[$('appRemRepeatBy').selectedIndex].value; 
					appRemRepeatFreq = $('appRemRepeatFreq').options[$('appRemRepeatFreq').selectedIndex].value;
					var chkLength = document.getElementsByName('appRemWeekday').length;
					for (var i=0; i < chkLength; i++){
					   if (document.getElementsByName('appRemWeekday')[i].checked){
						   if(appRemWeekdayStr == '')
							   appRemWeekdayStr = document.getElementsByName('appRemWeekday')[i].value;
						   else   
							   appRemWeekdayStr = appRemWeekdayStr + "," +document.getElementsByName('appRemWeekday')[i].value;
					   }   
					}
					
				}
				
				if($('appRemEndRadioN').checked == true)
					appRemEndRadio = 'never';
				
				if($('appRemEndRadioU').checked == true){
					appRemEndRadio = 'until';
					appRemEndDate = $('appRemEndDate').value;
				}	
			}
			var url = '/reminders/validateRem';
			var params = {'appRemMsg':encodeURIComponent(appRemMsg),'appRemFor':appRemFor,'remindWhen':varRemindWhen,'appRemDate':appRemDate,'appRemTime':appRemTime,'appRemRepeatChk':appRemRepeatChk,'appRemRepeatBy':appRemRepeatBy,'appRemRepeatFreq':appRemRepeatFreq,'appRemWeekdayStr':appRemWeekdayStr,'appRemEndRadio':appRemEndRadio,'appRemEndDate':appRemEndDate};
			var paramsStr = 'appRemMsg='+encodeURIComponent(appRemMsg)+'&appRemFor='+appRemFor+'&remindWhen='+varRemindWhen+'&appRemDate='+appRemDate+'&appRemTime='+appRemTime+'&appRemRepeatChk='+appRemRepeatChk+'&appRemRepeatBy='+appRemRepeatBy+'&appRemRepeatFreq='+appRemRepeatFreq+'&appRemWeekdayStr='+appRemWeekdayStr+'&appRemEndRadio='+appRemEndRadio+'&appRemEndDate='+appRemEndDate;
			
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: paramsStr,
				onSuccess:function(transport)
				{		
					if(transport.responseText != "")
					{
						$('appRemAjaxErr').show();
						$('appRemAjaxErr').innerHTML = transport.responseText;	
						$('sendButt').innerHTML = html;
					}
					else
					{										
						$('appRemAjaxErr').innerHTML = '';
						$('appRemAjaxErr').hide();
						
						var urlUpdate = '/reminders/createRem';
						var myAjax1 = new Ajax.Request(urlUpdate, {method: 'post', parameters: paramsStr,
							onSuccess:function(transport)
							{
								$('sendButt').innerHTML = html;
								if(transport.responseText == "")
								{
									alert('Oops!! Something went wrong.');
								}
								else
								{	
									window.scroll(0,0);
									var resArr = transport.responseText.split('^');
									reloadBalance(resArr[1]);
									$('afterReminderAddDiv').innerHTML = resArr[2];
									hideNRemove('afterReminderAddDiv',2,10);
																											
									if(resArr[0] == '1'){
										var url1 = '/reminders/upcomingRemUpdate';
										var params1 = {};
										var myAjax1 = new Ajax.Request(url1, {method: 'post', parameters: params1,
											onSuccess:function(transport1)
											{			
												transport1.responseText.evalScripts();
												$('upcomingRemTab').innerHTML = transport1.responseText;										
											}
										});
										
									}
									
									if(resArr[0] == '2'){//archived
										var url1 = '/reminders/archivedRemUpdate';
										var params1 = {};
										var myAjax1 = new Ajax.Request(url1, {method: 'post', parameters: params1,
											onSuccess:function(transport1)
											{			
												transport1.responseText.evalScripts();
												$('archivedRemTab').innerHTML = transport1.responseText;							
											}
										});
									}
									
									
									$('appReminderAddDiv').innerHTML = reminderAdd;
									$('appRemCharLmt').innerHTML = 'Upto '+(APP_REM_MSG_LMT - APP_REM_MSG_FIXED)+' chars';									
									$('appRemRepeatBox').hide();
									$('remindLater').show();
									$('appRemRepeatChk').checked = false;
								}
							}
						});																					
					}	
				}
			});	
			
		}
	}

	function delReminder(delRemId){
		
		var html = $('delRemButton'+delRemId).innerHTML;
		showLoader2('delRemButton'+delRemId);
		
		var url = '/reminders/deleteRem';
		var params = {'delRemId':delRemId};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{			
				if(transport.responseText == "0"){
					$('delRemButton'+delRemId).innerHTML = html;
					//alert('Oops!! something went wrong while deleting, please try again');
				}else{											
					var url1 = '/reminders/upcomingRemUpdate';
					var params1 = {};
					var myAjax1 = new Ajax.Request(url1, {method: 'post', parameters: params1,
						onSuccess:function(transport1)
						{			
							transport1.responseText.evalScripts();
							$('upcomingRemTab').innerHTML = transport1.responseText;							
						}
					});				
				}
			}
		});		
	}
	
	function delRemGrp(Id){
		
		var html = $('delRemGrpButton'+Id).innerHTML;
		showLoader2('delRemGrpButton'+Id);
		
		var url = '/reminders/deleteGrp';
		var params = {'delGrpId':Id};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{			
				if(transport.responseText == "0"){
					$('delRemGrpButton'+Id).innerHTML = html;
					//alert('Oops!! something went wrong while deleting, please try again');
				}else{
					$('main_'+Id).remove();
					if($('allGroups').innerHTML.strip() == ''){
						$('blankStateGrpList').innerHTML = '<div class="dataTable2"  style="font-size:0.9em">You don\'t have any groups created yet. Create groups to organize your contacts better.</div>';
					}							
				}
			}
		});		
	}
	
	function delRemRec(remId, mobNo){
		
		var html = $(mobNo+'_'+remId).innerHTML;
		showLoader2(mobNo+'_'+remId);
		
		var url = '/reminders/deleteRemRec';
		var params = {'remId':remId,'mobNo':mobNo};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{			
				if(transport.responseText == "0"){
					$(mobNo+'_'+remId).innerHTML = html;
					//alert('Oops!! something went wrong while deleting, please try again');
				}else{
					$(mobNo+'_'+remId).remove();
					if(transport.responseText == "2"){
						var url1 = '/reminders/upcomingRemUpdate';
						var params1 = {};
						var myAjax1 = new Ajax.Request(url1, {method: 'post', parameters: params1,
							onSuccess:function(transport1)
							{			
								transport1.responseText.evalScripts();
								$('upcomingRemTab').innerHTML = transport1.responseText;							
							}
						});
					}										
				}
			}
		});		
	}
	
	function delGrpFrnd(grpId, frndListNo){
		
		var html = $(frndListNo+'_'+grpId).innerHTML;
		showLoader2(frndListNo+'_'+grpId);
		
		var url = '/reminders/deleteGrpMem';
		var params = {'grpId':grpId,'frndListNo':frndListNo};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{			
				if(transport.responseText == "0"){
					$(frndListNo+'_'+grpId).innerHTML = html;
					//alert('Oops!! something went wrong while deleting, please try again');
				}else{
					$(frndListNo+'_'+grpId).remove();
					$('addToGrps'+grpId).innerHTML = '';
					$('addToGrps'+grpId).hide();					
				}
			}
		});		
	}
	
	function delFrndGrp(gflId,flId){
		
		var html = $('gfl_'+gflId).innerHTML;
		showLoader2('gfl_'+gflId);
		
		var url = '/reminders/deleteMemGrp';
		var params = {'gflId':gflId};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{			
				if(transport.responseText == "0"){
					$('gfl_'+gflId).innerHTML = html;
					//alert('Oops!! something went wrong while deleting, please try again');
				}else{					
					$('gfl_'+gflId).remove();
					$('addToCons'+flId).innerHTML = '';
					$('addToCons'+flId).hide();
				}
			}
		});		
	}
	
	
	function addToGroup(grpId){
		if($('addToGrps'+grpId).style.display == 'none'){
			$('addToGrps'+grpId).show();
			if($('addToGrps'+grpId).innerHTML.strip() == ''){
				showLoader2('addToGrps'+grpId);				
				var url = '/reminders/nonGrpMem';
				var params = {'grpId':grpId};
				var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
					onSuccess:function(transport)
					{			
						$('addToGrps'+grpId).innerHTML = transport.responseText;						
					}
				});
			}
		}else{
			$('addToGrps'+grpId).hide();
		}					
	}
	
	function addToContact(flId){
		if($('addToCons'+flId).style.display == 'none'){
			$('addToCons'+flId).show();
			if($('addToCons'+flId).innerHTML.strip() == ''){
				showLoader2('addToCons'+flId);				
				var url = '/reminders/nonConGrp';
				var params = {'flId':flId};
				var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
					onSuccess:function(transport)
					{			
						$('addToCons'+flId).innerHTML = transport.responseText;						
					}
				});
			}
		}else{
			$('addToCons'+flId).hide();
		}					
	}

	function delArchived(delArcId){
		var url = '/reminders/deleteArc';
		var params = {'delArcId':delArcId};
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
			onSuccess:function(transport)
			{			
				if(transport.responseText == "0"){
					alert('Oops!! something went wrong while deleting, please try again');
				}else{											
					var url1 = '/reminders/archivedRemUpdate';
					var params1 = {};
					var myAjax1 = new Ajax.Request(url1, {method: 'post', parameters: params1,
						onSuccess:function(transport1)
						{			
							transport1.responseText.evalScripts();
							$('archivedRemTab').innerHTML = transport1.responseText;							
						}
					});				
				}
			}
		});		
	}

	function appNameValidate(name,fieldName,maxlength){
		var errMsg = '';
		if( (name == null) || (name.length == 0)){
			errMsg = "Enter "+fieldName;
		}
		if(maxlength != -1 && name.length > maxlength){
			errMsg = fieldName + " should contain maximum of " + maxlength + " characters";
		}
		var re = /[^a-zA-Z0-9 ]/g;
		if (re.test(name)){
			errMsg = fieldName + " should contain alphanumeric (A-Z, a-z, 0-9) characters only.";
		}	
		return errMsg;	
	}
	
	function getMultiple(ob)
	{
		var selectedArray = new Array();
		var selObj = ob;
		var i;
		var len = selObj.options.length;
		for (i=0; i<len; i++) {
		  if (selObj.options[i].selected) {
			selectedArray.push(selObj.options[i].value);  
		  }
		}
	    return selectedArray.toString();
	}
	
	function deselectMultiple(ob){
		while (ob.selectedIndex != -1)
	    {
	        ob.options[ob.selectedIndex].selected = false;
	    }
	} 
	
	function appRemAddFriend(nameId,mobileId){
		if($('appRemGroupaddBox')) $('appRemGroupaddBox').hide();
		var name = $(nameId).value;
		var mobile = $(mobileId).value;	
		
		var group = '';
		if($('groupList'))
		group = getMultiple($('groupList'));
		
		var errMsg ='';
		var errMsg1 ='';
		errMsg = appNameValidate(name,'Name',20);
		errMsg1 = appMobileValidate(mobile);
		
		if(errMsg){
			$('nickName').addClassName('err');
			$('nickNameErr').show();
			$('nickNameErr').addClassName('inlineErr1');
			$('nickNameErr').innerHTML = errMsg;
		}else{
			$('nickName').removeClassName('err');			
			$('nickNameErr').removeClassName('inlineErr1');
			$('nickNameErr').innerHTML = errMsg;
			$('nickNameErr').hide();
		}
		
		if(errMsg1){
			$('friendMobile').addClassName('err');
			$('friendMobileErr').show();
			$('friendMobileErr').addClassName('inlineErr1');
			$('friendMobileErr').innerHTML = errMsg1;			
		}else{
			$('friendMobile').removeClassName('err');			
			$('friendMobileErr').removeClassName('inlineErr1');
			$('friendMobileErr').innerHTML = errMsg1;
			$('friendMobileErr').hide();
		}
		
		if(errMsg == '' && errMsg1 == ''){
			var html = $('sendButt').innerHTML;
			showLoader2('sendButt');
			var url    = '/reminders/addFriend/';	
			var rand   = Math.random(9999);
			var pars   = "nickname="+name+"&group="+group+"&mobile="+mobile+"&rand="+rand;
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
							onSuccess:function(transport)
							{
								$('sendButt').innerHTML = html;
								if(transport.responseText == "a")
								{
									$('friendMobile').addClassName('err');
									$('friendMobileErr').show();
									$('friendMobileErr').addClassName('inlineErr1');
									$('friendMobileErr').innerHTML = 'Mobile number already exists in your contact list';
								}
								else
								{
									if($('groupList'))deselectMultiple($('groupList'));
									$(nameId).value = '';
									$(mobileId).value = '';	
									$('afterContactAddDiv').innerHTML = '<div style="padding-bottom:10px;"><div class="appColLeftBox">Contact saved successfully!!</div></div>';
									hideNRemove('afterContactAddDiv',2,5);
									var url    = '/reminders/afterGrpAdd';	
									var rand   = Math.random(9999);
									var flId = transport.responseText;
									var pars   = "flId="+flId;
									var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
											onSuccess:function(transport)
											{
												
												if($('allFriends').innerHTML.strip() == ''){
													$('blankStateFrndList').innerHTML = '<div class="strng" style="border-bottom:1px solid #5D5D5D"><tr><th width="195px">Name</th><th width="100px">Mobile No.</th><th>&nbsp;</th></tr></table></div>';
												}
												$('allFriends').innerHTML = '<div id="main_'+flId+'">'+transport.responseText+'</div>'+$('allFriends').innerHTML;
											}
									});																									
								}	
							}
					});
		
		}
	}

	function appRemQikAddFriend(nameId,mobileId){
		var name = $(nameId).value;
		var mobile = $(mobileId).value;
		var errMsg ='';
		var errMsg1 ='';
		errMsg = appNameValidate(name,'Name',20);
		errMsg1 = appMobileValidate(mobile);
		
		if(errMsg){
			$('nickName').addClassName('err');
			$('nickNameErr').show();
			$('nickNameErr').addClassName('inlineErr1');
			$('nickNameErr').innerHTML = errMsg;
		}else{
			$('nickName').removeClassName('err');			
			$('nickNameErr').removeClassName('inlineErr1');
			$('nickNameErr').innerHTML = errMsg;
			$('nickNameErr').hide();
		}
		
		if(errMsg1){
			$('friendMobile').addClassName('err');
			$('friendMobileErr').show();
			$('friendMobileErr').addClassName('inlineErr1');
			$('friendMobileErr').innerHTML = errMsg1;			
		}else{
			$('friendMobile').removeClassName('err');			
			$('friendMobileErr').removeClassName('inlineErr1');
			$('friendMobileErr').innerHTML = errMsg1;
			$('friendMobileErr').hide();
		}

		if(errMsg == '' && errMsg1 == ''){
			var url    = '/reminders/addFriend/';	
			var rand   = Math.random(9999);
			var pars   = "nickname="+name+"&mobile="+mobile+"&rand="+rand;
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
							onSuccess:function(transport)
							{
								if(transport.responseText == "null")
								{
									alert('Mobile number already exists in your contact list');
								}
								else
								{								
									$('afterReminderAddDiv').innerHTML = '<div style="padding-bottom:10px;"><div class="appColLeftBox">Contact saved successfully!!</div></div>';
									hideNRemove('afterReminderAddDiv',2,5);
								}	
							}
					});
		
		}
	}

	function appRemDelFriend(frnd,id){
		var html = $('delRemConButton'+id).innerHTML;
		showLoader2('delRemConButton'+id);
		var url    = '/reminders/deleteFriend/';	
		var rand   = Math.random(9999);
		var pars   = "id="+id+"&rand="+rand;
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
						onSuccess:function(transport)
						{								
							if(transport.responseText == "e")
							{
								$('delRemConButton'+id).innerHTML = html;
								//alert('Error while deleting, please try again');
							}
							else
							{	
								$('main_'+id).remove();
								if(transport.responseText == "0"){
									if($('allFriends').innerHTML.strip() == ''){
										$('blankStateFrndList').innerHTML = '<div class="dataTable2"  style="font-size:0.9em">You can maintain a contact list of mobile numbers. This list can be used site wide for features like Bhulakkad, Free SMS & Message forwarding.</div>';
									}
								}																		
							}	
						}
				});			
	}

	function appRemPagination(){
		var url    = '/reminders/remAppPaginate';	
		var rand   = Math.random(9999);
		var pars   = "id="+$('remAppUpcomingPag').value+"&rand="+rand+"&type=U";
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars, evalJS : true,
						onSuccess:function(transport)
						{
							transport.responseText.evalScripts();						
							if($('remAppUpcomingPagCnt').value == '0'){
								$('UpcomingRemVMDiv').remove();							
							}else{
								
								$('upcomingRemTab').innerHTML =  $('upcomingRemTab').innerHTML + transport.responseText;
							}
						}
				});
	}

	function appRemArcPagination(){
		var url    = '/reminders/remAppPaginate';	
		var rand   = Math.random(9999);
		var pars   = "id="+$('remAppArcPag').value+"&rand="+rand+"&type=A";
		var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
						onSuccess:function(transport)
						{
							transport.responseText.evalScripts();						
							if($('remAppArcPagCnt').value == '0'){
								$('archivedRemVMDiv').remove();
							}else{
								$('archivedRemTab').innerHTML =  $('archivedRemTab').innerHTML + transport.responseText;
							}
						}
				});
	}

	function addFrndNameBox(val){
		
		if(val.length == 10){
			if(!$('appRemForName'))
			$('appRemForNameSpan').innerHTML = '<input type="text" name="appRemForName" id="appRemForName" style="width:180px;" value="dinesh"/>';
		}
		
	}

	function closeRemDelBox(id){
		$('appDelRem'+id).innerHTML = '';
		$('appRemCnfDelBox'+id).hide();
	}

	function closeRemConDelBox(id){
		$('appDelRemCon'+id).innerHTML = '';
		$('appRemConCnfDelBox'+id).hide();
	}
	
	function closeRemGrpDelBox(id){
		$('appDelRemGrp'+id).innerHTML = '';
		$('appRemGrpCnfDelBox'+id).hide();
	}
	
	function confirmDelReminder(id){
		var delFn = "delReminder('"+id+"')";
		$('appDelRem'+id).innerHTML = 'Are you sure you wish to remove this message?'+
		'<br><div id="delRemButton'+id+'"><a style="margin-top:5px;" class="buttSprite rightFloat" onclick="'+delFn+'" href="javascript:void(0);"><img class="butOk" src="/img/spacer.gif"></a></div><div class="clearRight">&nbsp;</div>';    
		$('appRemCnfDelBox'+id).show();
	}
	
	function confirmDelRemGroup(id){
		var delFn = "delRemGrp('"+id+"')";
		$('appDelRemGrp'+id).innerHTML = 'Are you sure you wish to remove this group?'+
		'<br><div id="delRemGrpButton'+id+'"><a style="margin-top:5px;" class="buttSprite rightFloat" onclick="'+delFn+'" href="javascript:void(0);"><img class="butOk" src="/img/spacer.gif"></a></div><div class="clearRight">&nbsp;</div>';    
		$('appRemGrpCnfDelBox'+id).show();
	}

	function confirmDelRemCon(mob,id){
		var delFn = "appRemDelFriend('"+mob+"','"+id+"')";
		$('appDelRemCon'+id).innerHTML = 'Are you sure you wish to remove this contact?'+
		'<br><div id="delRemConButton'+id+'"><a style="margin-top:5px;" class="buttSprite rightFloat" onclick="'+delFn+'" href="javascript:void(0);"><img class="butOk" src="/img/spacer.gif"></a></div><div class="clearRight">&nbsp;</div>';    
		$('appRemConCnfDelBox'+id).show();
	}
	
	function hideNRemove(id,dur,del){
		$(id).show();
		Effect.SlideUp(id,{duration:dur,delay:del, afterFinish: function(){$(id).innerHTML = '';$(id).show();}});
		
	}
	
	function get_r_receivers(obj){
		 var r_rec_str = new Array();
		 var children = obj.descendants();
	     for(var k=0;k<children.length;k++)
	     {         
	         if(children[k].tagName.toLowerCase()=='input' && children[k].type=='checkbox' && children[k].checked == true && children[k].value != ''){
	        	 r_rec_str.push(children[k].value);
	         }	 
	     }
	     return r_rec_str;	         
	 }
	 
	 function showChildren(obj,imgNo,img)
	 {
		 var children = 'li_'+obj+'_children';
		 if($(children).style.display == 'none'){
			img.src = '/img/imgs/minus'+imgNo+'.gif'; 
		 	$(children).show();
	 	 }else{
	 		img.src = '/img/imgs/plus'+imgNo+'.gif'; 
			$(children).hide();
		 }	 
	 }
	 
	 function checkChildren(obj,srcObj)
	 {
	     var children = obj.immediateDescendants();
	     for(var i=0;i<children.length;i++)
	     {
	         if(children[i].tagName.toLowerCase()=='input' && children[i].type=='checkbox' && children[i]!=srcObj)
	             children[i].checked = srcObj.checked;
	 
	         // recursive call
	         checkChildren(children[i],srcObj);
	     }
	 }

	 function checkParent(obj,srcObj)
	 {
	     
	     if(srcObj.checked== true){ // on check
	    	 var children = obj.descendants();               	
		     for(var i=0;i<children.length;i++)
		     {         
		         if(children[i].tagName.toLowerCase()=='input' && children[i].type=='checkbox'){
		             children[i].checked = srcObj.checked;
		             break;
		         }    
	             	
		     }
	     }else{// on uncheck         
	    	 var children = obj.descendants();
	    	 var parentChkVar = '';
	         var parentChk = false;
	         for(var i=0;i<children.length;i++)
	         {         
	             if(children[i].tagName.toLowerCase()=='input' && children[i].type=='checkbox'){
	            	 parentChkVar = children[i];
	            	 break;
	             }	 
	         }
			i++;
	         var children = obj.descendants();
	         for(var k=i;k<children.length;k++)
	         {         
	             if(children[k].tagName.toLowerCase()=='input' && children[k].type=='checkbox' && children[k].checked == true){
	            	 parentChk = true;
	            	 break;
	             }	 
	         }

	         parentChkVar.checked = parentChk;
	    		
	     }
	 }
	 
	 function remindWhen(){
		var chkLength = document.getElementsByName('appRemSend').length;
		for (var i=0; i < chkLength; i++){
		   if (document.getElementsByName('appRemSend')[i].checked){
			   var sel = document.getElementsByName('appRemSend')[i].value;
			   if(sel == 'l'){
				   $('remindLater').show();
			   }else{
				   $('remindLater').hide();
				   $('appRemRepeatBox').hide()
				   $('appRemRepeatChk').checked = false;
			   }
		   }
		}
		return sel;
	 }
	 
	 function appRemGrpAdd(grpName){
		 
		 	$(grpName).removeClassName('err');			
			$('groupNameErr').removeClassName('inlineErr1');
			$('groupNameErr').innerHTML = '';
			$('groupNameErr').hide();
			
			var name = $(grpName).value.strip();
			
			var errMsg ='';			
			errMsg = appNameValidate(name,'Group name',50);			
			if(errMsg){
				$(grpName).addClassName('err');
				$('groupNameErr').show();
				$('groupNameErr').addClassName('inlineErr1');
				$('groupNameErr').innerHTML = errMsg;
			}else{
				$(grpName).removeClassName('err');			
				$('groupNameErr').removeClassName('inlineErr1');
				$('groupNameErr').innerHTML = '';
				$('groupNameErr').hide();
			}
			
			
			if(errMsg == ''){
				var html = $('sendButt').innerHTML;
				showLoader2('sendButt');
				var url    = '/reminders/addGrpList';	
				var rand   = Math.random(9999);
				var pars   = "groupname="+name+"&rand="+rand;
				var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
								onSuccess:function(transport)
								{
									$('sendButt').innerHTML = html;
									if(transport.responseText == "e"){
										$(grpName).addClassName('err');
										$('groupNameErr').show();
										$('groupNameErr').addClassName('inlineErr1');
										$('groupNameErr').innerHTML = 'Oops!! Something went wrong. Please try again.';
									}else if(transport.responseText == "a"){
										$(grpName).addClassName('err');
										$('groupNameErr').show();
										$('groupNameErr').addClassName('inlineErr1');
										$('groupNameErr').innerHTML = 'Group name already exists. Please try another name.';
									}else{
										
										$(grpName).value = '';										
										$('afterGroupAddDiv').innerHTML = '<div style="padding-bottom:10px;"><div class="appColLeftBox">Group created successfully!!</div></div>';
										hideNRemove('afterGroupAddDiv',2,5);
										var url    = '/reminders/afterConAdd';	
										var rand   = Math.random(9999);
										var newGRpID = transport.responseText;
										var pars   = "grpId="+newGRpID;
										var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
												onSuccess:function(transport)
												{
													
													if($('allGroups').innerHTML.strip() == ''){
														$('blankStateGrpList').innerHTML = '<div class="strng"><table cellpadding="0px" cellspacing="0px" border="0" class="dataTable2" style="margin-bottom:0" ><tr><th width="195px">Names</th></tr></table></div>';
													}
													$('allGroups').innerHTML = '<div id="main_'+newGRpID+'">'+transport.responseText+'</div>'+$('allGroups').innerHTML;
												}
										});	
									}	
								}
						});
			
			}
		}
	 
	 function appRemGrpAddQuick(grpName){
		 
		 	$(grpName).removeClassName('err');			
			$('groupNameErr').removeClassName('inlineErr1');
			$('groupNameErr').innerHTML = '';
			$('groupNameErr').hide();
			
			var name = $(grpName).value.strip();
			
			var errMsg ='';			
			errMsg = appNameValidate(name,'Group name',50);			
			if(errMsg){
				$(grpName).addClassName('err');
				$('groupNameErr').show();
				$('groupNameErr').addClassName('inlineErr1');
				$('groupNameErr').innerHTML = errMsg;
			}else{
				$(grpName).removeClassName('err');			
				$('groupNameErr').removeClassName('inlineErr1');
				$('groupNameErr').innerHTML = '';
				$('groupNameErr').hide();
			}
			
			
			if(errMsg == ''){
				var html = $('sendButt').innerHTML;
				showLoader2('sendButt');
				var url    = '/reminders/addGrpListNFetch';	
				var rand   = Math.random(9999);
				var pars   = "groupname="+name+"&rand="+rand;
				var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
								onSuccess:function(transport)
								{
									$('sendButt').innerHTML = html;
									if(transport.responseText == "e"){
										$(grpName).addClassName('err');
										$('groupNameErr').show();
										$('groupNameErr').addClassName('inlineErr1');
										$('groupNameErr').innerHTML = 'Oops!! Something went wrong. Please try again.';
									}else if(transport.responseText == "a"){
										$(grpName).addClassName('err');
										$('groupNameErr').show();
										$('groupNameErr').addClassName('inlineErr1');
										$('groupNameErr').innerHTML = 'Group name already exists. Please try another name.';
									}else{
										if($('appRemGroupBlankState'))$('appRemGroupBlankState').remove();
										$(grpName).value = '';
										$('appRemGroupaddBox').hide();										
										$('appRemGrpLstBox').innerHTML = transport.responseText;										
									}	
								}
						});
			
			}
		}
	 function selectAllGrpMem(obj,divId,name){
			var elems = $$('div#'+divId+ ' input');
			var len = elems.length;
			if(obj.checked){
				for (var i=0;i<len;i++){
					if(elems[i].name == name){
						elems[i].checked = true;
					}
				}
			}else {
				for (var i=0;i<len;i++){
					if(elems[i].name == name){
						elems[i].checked = false;
					}
				}
			}					
		}
	 
	 	function addConToGrp(obj,grpId){
			 var con = get_r_receivers($(obj));
			 if(con == ''){
				 alert('Please select at least one contact to add');
				 return false;
			 }

			var url    = '/reminders/addConToGrp';	
			var pars   = {'conStr':con.toString(),'grpId':grpId};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
							onSuccess:function(transport)
							{								
								$('addToGrps'+grpId).innerHTML = '';
								$('addToGrps'+grpId).hide();
								var url    = '/reminders/afterConAdd';	
								var rand   = Math.random(9999);
								var pars   = "grpId="+grpId;
								var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
										onSuccess:function(transport)
										{
											$('main_'+grpId).innerHTML = transport.responseText;	
										}
								});								
							}
					});	
	 	}
	 	
	 	function addGrpToCon(obj,flId){
			 var con = get_r_receivers($(obj));
			 if(con == ''){
				 alert('Please select at least one group to add');
				 return false;
			 }

			var url    = '/reminders/addGrpToCon';	
			var pars   = {'grpStr':con.toString(),'flId':flId};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
							onSuccess:function(transport)
							{								
								$('addToCons'+flId).innerHTML = '';
								$('addToCons'+flId).hide();
								var url    = '/reminders/afterGrpAdd';	
								var rand   = Math.random(9999);
								var pars   = "flId="+flId;
								var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
										onSuccess:function(transport)
										{
											$('main_'+flId).innerHTML = transport.responseText;	
										}
								});								
							}
					});	
	 	}
	 	
	 	function moreDataUrl(noOfFields){
	 		var addMoreStr = '';
	 		var k = $('urlDataCnt').value;
	 		var total = parseInt(k)+parseInt(noOfFields);
			for(var g=k;g<total;g++){ 
				var bgcolor="#C9C299";
				if(g%2 == 1)bgcolor="#C0C0C0";			
				
				addMoreStr += '<table id="urlTable'+g+'" style="margin-top:2px;" border="0" width="100%" bgcolor="'+bgcolor+'">'
					+'<tr>'
						+'<td colspan="2">'
							+'Title: <input type="text" id="urlTitle'+g+'" style="width:85%">'
						+'</td>'
					+'</tr>'
					+'<tr>'
						+'<td style="width:20%">'
							+'<select id="urlType'+g+'" onchange="typeChange('+g+');">'								
								+'<option value="0">Image</option>'
								+'<option value="1">Video</option>'																
							+'</select>'						
						+'</td>'
						+'<td>'
							+'<input type="text" id="urlUrl'+g+'" style="width:95%">'
						+'</td>'
					+'</tr>'
				+'</table><div style="" id="urlErr'+g+'"></div>';							
			}
			$('urlDataCnt').value = g;	
			$('dataUrls').innerHTML = $('dataUrls').innerHTML + addMoreStr; 
		}
		
		function typeChange(val){
			if($('urlType'+val).options[$('urlType'+val).selectedIndex].value == 1){
				$('urlUrl'+val).value = "http://www.youtube.com/v/";
			}else{
				$('urlUrl'+val).value = "";
			}
		}
		
		function createShortUrl(pkg_id,url_id){
			var cnt = $('urlDataCnt').value;
			var title = new Array();
			var type = new Array();
			var urls = new Array();			
			
			//urlTable
			//urlErr
			var noRecArr = new Array();			
			var j = 0;
			for(var i=0;i<cnt;i++){
				$('urlTable'+i).style.border = '1px solid #FFFFFF';
				$('urlErr'+i).style.border = '1px solid #FFFFFF';
				$('urlErr'+i).innerHTML = '';
								
				if($('urlTitle'+i).value.strip() == "" && $('urlUrl'+i).value.strip() == ''){
					 noRecArr[j] = 1;
					 j++;
				}								
			}
			
			
			if(noRecArr.length == cnt){
				alert('Enter at least one record');
				return false;
			}
			
			var errVar1 = 0;
			for(var i=0;i<cnt;i++){
				$('urlTable'+i).style.border = '1px solid #FFFFFF';
				$('urlErr'+i).style.border = '1px solid #FFFFFF';
				$('urlErr'+i).innerHTML = '';
								
				if($('urlTitle'+i).value.strip() != "" && $('urlUrl'+i).value.strip() == ''){
					$('urlTable'+i).style.border = '1px solid #ec724a';
					$('urlErr'+i).style.border = '1px solid #ec724a';
					$('urlErr'+i).innerHTML = 'Please enter URL';
					errVar1 = 1; 
				}else if($('urlTitle'+i).value.strip() == "" && $('urlUrl'+i).value.strip() != ''){
					if(!isValidURL($('urlUrl'+i).value.strip())){
						$('urlTable'+i).style.border = '1px solid #ec724a';
						$('urlErr'+i).style.border = '1px solid #ec724a';
						$('urlErr'+i).innerHTML = 'Invalid URL';
						errVar1 = 1;
					}
				}else if($('urlTitle'+i).value.strip() != "" && $('urlUrl'+i).value.strip() != ''){
					if(!isValidURL($('urlUrl'+i).value.strip())){
						$('urlTable'+i).style.border = '1px solid #ec724a';
						$('urlErr'+i).style.border = '1px solid #ec724a';
						$('urlErr'+i).innerHTML = 'Invalid URL';
						errVar1 = 1;
					}
				}
				
				title[i] = $('urlTitle'+i).value;
				type[i] = $('urlType'+i).options[$('urlType'+i).selectedIndex].value;
				urls[i] = $('urlUrl'+i).value;
			}
			
			if(errVar1 == 1)
			return false;
			/*validation			
			if(title && no url){
				ERROR
			}else if(no title && but url){
				if(invalid url){
					ERROR
				}
			}else if(title && url){
				if(invalid url){
					ERROR
				}
			}*/
			
			var url    = '/groups/createShortUrl';
			showLoader2('createShortUrl');
			var rand   = Math.random(9999);
									
			var newGRpID = 1;
			var pars   = {'url_id':url_id,'pkg_id':pkg_id, 'title[]':title, 'type[]':type, 'urls[]':urls};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{
						var res = transport.responseText.split('^^^');
						$('shortDataUrl').innerHTML = "<div style='margin:10px;padding:3px;background-color:#F88017'>"+res[1]+"</div>"; 
						$('createShortUrl').innerHTML = '<input type="button" style="background-color:#657383;" value="Delete & get new" onclick="createShortUrl('+pkg_id+','+res[0]+');">';
					}
			});
		}
		
		function isValidURL(url){ 
  		  	var RegExp = /^(((ht|f){1}(tp:[/][/]){1})|((www.){1}))[-a-zA-Z0-9@:%_\+.~#?&//=]+$/; 
    		if(RegExp.test(url)){ 
        		return true; 
    		}else{ 
        		return false; 
    		} 
		} 
		
		function getCoolVideo(){
			var url    = '/groups/getCoolVideo';
			var temp = $('getCoolVideo').innerHTML;
			showLoader2('getCoolVideo');
			var rand   = Math.random(9999);
									
			var newGRpID = 1;
			var pars   = {};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
					onSuccess:function(transport)
					{
						var res = transport.responseText;
						$('coolVideo').innerHTML = res; 
						$('getCoolVideo').innerHTML = temp;
					}
			});
		}
		
		function getRetailers(pageNum,loader){					
			id = $('area').options[$('area').selectedIndex].value;
			var url = '/retailers/getRetailersByArea';
			var sndBut = $('sendButt').innerHTML;
			if(loader == 1)showLoader2('sendButt');
			
			if($('state').options[$('state').selectedIndex].value == '0'){
				$('locateErr').innerHTML = 'Please select state.';
				if(loader == 1)$('sendButt').innerHTML = sndBut;
				return false;
			}else if($('city').options[$('city').selectedIndex].value == '0'){
				$('locateErr').innerHTML = 'Please select city.';
				if(loader == 1)$('sendButt').innerHTML = sndBut;
				return false;
			}else if($('area').options[$('area').selectedIndex].value == '0'){
				$('locateErr').innerHTML = 'Please select area.';
				if(loader == 1)$('sendButt').innerHTML = sndBut;
				return false;
			}
			
			$('locateErr').innerHTML = '';
			//if(loader == 0)showLoader('retailersData');
			/*new Ajax.Updater('retailersData', url, {
		  			parameters: {area_id: id, page: pageNum},
		  			evalScripts:true
				});*/
			var pars   = {area_id: id, page: pageNum};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,evalScripts:true,
				onSuccess:function(transport)
				{
					var res = transport.responseText;
					$('retailersData').innerHTML = res;
					if(loader == 1)$('sendButt').innerHTML = sndBut;
				}
			});
				
		}
		
		
		function getAreas(id, type){
			var url = '/retailers/getAreasByCity';
			var pars   = {city_id: id, type: type};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
				onSuccess:function(transport)
				{
					var res = transport.responseText;
					$('areaDD').innerHTML = res;
				}
			});	
		}
		
		function getCities(id,type){
			
			var url = '/retailers/getCitiesByState';
			var pars   = {state_id: id, type: type};
			var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
				onSuccess:function(transport)
				{
					var res = transport.responseText;
					$('cityDD').innerHTML = res;

					//getAreas($('city').options[$('city').selectedIndex].value);
				}
			});	
		}
		
		/*function fbs_click() {
			u="http://www.smstadka.com/messages/facebook/free-credits/"+Math.floor(Math.random()*1001);
			window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u),'sharer','toolbar=0,status=0,width=626,height=436');
			return false;
		}
		*/
		function loginWin(){						
			popupSwap();
			var url = '/users/loginWin';
			var params = {};			
			showLoader2('errMessagePopUp');
			centerPos('errPopUp');
			new Ajax.Updater('messagePopUpDiv', url, {
		  			parameters: params,
		  			evalScripts:true,
		  			onComplete: function(response){$('errPopUp').hide();centerPos('popUpDiv');}
			});
		}
		
		function findQty(obj,count)
		{
			if ((obj.className) == 'start')
			{				
				if (isNaN($('start_'+count).value) || isNaN($('end_'+count).value) || $('end_'+count).value == "")
					return false;
				else
				{
					if ($('end_'+count).value < $('start_'+count).value)
					{						
						$('qty_'+count).innerHTML = "error";
					}
					else
						$('qty_'+count).innerHTML = ($('end_'+count).value - $('start_'+count).value)+1;
				}					
			}
			else
			{
				if (isNaN($('end_'+count).value) || isNaN($('start_'+count).value) || ($('start_'+count).value == ""))
					return false;
				else
				{
					if ($('end_'+count).value < $('start_'+count).value)
					{
						$('qty_'+count).innerHTML = "error";
					}
					else
						$('qty_'+count).innerHTML = ($('end_'+count).value - $('start_'+count).value)+1;
				}				
			}			 
		}
		
		function $m(quem){
			return document.getElementById(quem)
		}
		
		function remove(quem){
		 quem.removeChild(quem);
		}
		
		function addEvent(obj, evType, fn){
		
		    if (obj.addEventListener)
		        obj.addEventListener(evType, fn, true)
		    if (obj.attachEvent)
		        obj.attachEvent("on"+evType, fn)
		}
		
		function removeEvent( obj, type, fn ) {
		  if ( obj.detachEvent ) {
		    obj.detachEvent( 'on'+type, fn );
		  } else {
		    obj.removeEventListener( type, fn, false ); }
		} 
		
		function micoxUpload(form,url_action,id_element,html_show_loading,html_error_http){									 
			 form = typeof(form)=="string"?$m(form):form;
			 
			 var erro="";
			 if(form==null || typeof(form)=="undefined"){ erro += "The form of 1st parameter does not exists.\n";}
			 else if(form.nodeName.toLowerCase()!="form"){ erro += "The form of 1st parameter its not a form.\n";}
			 if($m(id_element)==null){ erro += "The element of 3rd parameter does not exists.\n";}
			 if(erro.length>0) {
			  alert("Error in call micoxUpload:\n" + erro);
			  return;
			 }
	
			
			 var iframe = document.createElement("iframe");
			 iframe.setAttribute("id","micox-temp");
			 iframe.setAttribute("name","micox-temp");
			 iframe.setAttribute("width","0");
			 iframe.setAttribute("height","0");
			 iframe.setAttribute("border","0");
			 iframe.setAttribute("style","width: 0; height: 0; border: none;");
			 
			
			 form.appendChild(iframe);
			 window.frames['micox-temp'].name="micox-temp"; //ie sucks
			 
			
			 var carregou = function() { 
			   removeEvent( $m('micox-temp'),"load", carregou);
			   var cross = "javascript: ";
			   cross += "window.parent.$m('" + id_element + "').innerHTML = document.body.innerHTML; void(0); ";
			   
			   $m(id_element).innerHTML = html_error_http;
			   $m('micox-temp').src = cross;
			
			   setTimeout(function(){ remove($m('micox-temp'))}, 250);
			  }
			 addEvent( $m('micox-temp'),"load", carregou)
			 
			
			 form.setAttribute("target","micox-temp");
			 form.setAttribute("action",url_action);
			 form.setAttribute("method","post");
			 form.setAttribute("enctype","multipart/form-data");
			 form.setAttribute("encoding","multipart/form-data");
			
			 form.submit();
			 
			
			 if(html_show_loading.length > 0){
			  $m(id_element).innerHTML = html_show_loading;
			 }		 
		}
		
		function simContact(){
			$('appRemfrndList').simulate('click');
		}
		
		function pushResult(){
			var answer = confirm("Are you sure you checked the message?")
			if (answer){
				var url    = '/retailers/pushResult';
				showLoader2('pushResult');
				var rand   = Math.random(9999);
				var pars   = {'rand' : rand,'content' :encodeURIComponent($('transl').value)};
				var myAjax = new Ajax.Request(url, {method: 'post', parameters: pars,
						onSuccess:function(transport)
						{									
							$('pushResult').innerHTML = '<input type="button" style="background-color:#657383;" value="Send" onclick="pushResult();">'+transport.responseText;
						}
				});
			}
			else{
				//alert("Thanks for sticking around!")
			}
		}
		
		function makePayment(){
			if(getCheckedValue(document.forms['paymentOptionForm'].elements['card'])== 'dp')encodeTxnRequest();
			if(getCheckedValue(document.forms['paymentOptionForm'].elements['card'])== 'icc')payICC();					
			if(getCheckedValue(document.forms['paymentOptionForm'].elements['card'])== 'oss')payOSS();			
		}
		
		function getCheckedValue(radioObj) {
			if(!radioObj)
				return "";
			var radioLength = radioObj.length;
			if(radioLength == undefined)
				if(radioObj.checked)
					return radioObj.value;
				else
					return "";
			for(var i = 0; i < radioLength; i++) {
				if(radioObj[i].checked) {
					return radioObj[i].value;
				}
			}
			return "";
		}
		
	/* app.js ends here */