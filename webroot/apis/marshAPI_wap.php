<?php
include("../../config/bootstrap.php");
include("../../controllers/components/email.php");
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die ('Error connecting to mysql');
mysql_select_db(DB_DB);

function br2newline( $input ) {
	$out = str_replace( "<br>", "\n", $input );
	$out = str_replace( "<br/>", "\n", $out );
	$out = str_replace( "<br />", "\n", $out );
	$out = str_replace( "<BR>", "\n", $out );
	$out = str_replace( "<BR/>", "\n", $out );
	$out = str_replace( "<BR />", "\n", $out );
	return $out;
}
	
function curl_post_async($url, $params=null)
{
    foreach ($params as $key => &$val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}

function emailValidation($email_x){
	if (!preg_match('/^[a-zA-Z]\w+(\.\w+)*\@\w+(\.[0-9a-zA-Z]+)*\.[a-zA-Z]{2,4}$/',$email_x)) {
        $ret = false;
    } else {
        $ret = true;
    }
    return $ret;
}

function mailShoot($sub,$body,$email,$type,$attach,$message=null,$mobile=null){
	$url = SERVER_BACKUP . 'groups/shootMail';
	$params['sub'] = $sub;
	$body = $body . "<br/><br/>Regards<br/>Marsh Employee Benefits Team";
	
	$params['body'] = $body;
			
	$params['email'] = $email;
	$params['type'] = $type;
	$params['from'] = 'Marsh Team';
	$params['username'] = MARSH_EMAIL_ID;
	$params['password'] = MARSH_PASSWORD;
	
	if($attach == 1){
		$params['path'][] = $_SERVER['DOCUMENT_ROOT'] . '/Claims Checklist.pdf';
		$params['path'][] = $_SERVER['DOCUMENT_ROOT'] . '/FHPL Pre-Authorization Form.zip';
	}
	if($message != null) {
		$params['message'] = $message;
		$params['mobile'] = $mobile;
	}
	curl_post_async($url,$params);
}

$email = "support@marsh.co.in";
$tid = trim($_REQUEST['tid']);
if(!isset($_REQUEST['mobile'])){
	$res = mysql_query("SELECT mobile FROM ussd_requests WHERE tid='$tid' order by id desc");
	$res_arr = mysql_fetch_array($res);
	$mobile = $res_arr['mobile'];
}
else {
	$mobile = trim($_REQUEST['mobile']);
}

if(strlen($mobile) > 10)
$mobile = substr($mobile, -10);

$sql = "SELECT * FROM msh_employee WHERE mobile_linked='$mobile'";
$res = mysql_query($sql);
$res_arr = mysql_fetch_array($res);
$auth = 1;
$add = false;
if(empty($res_arr)) $auth = 0;
$type = "text";
if(isset($_REQUEST['first'])){
	$session=0;
	if($auth == 0){
		$message = "Enter company code\\nEnter Employee Code\\nEnter Your Date of Birth\nE.g For 1st Jan 1972 enter 01011972";
	}
	else {
		$message =  "1:View plan info\\n2:Intimate claim\\n3:Track claim status\\n4:Find Network Hospitals\\n5:Get Imp contact details\\n6:Get claims related docs";
		$type = "link";
	}
	
}
else {
	$msg = trim(urldecode($_REQUEST['msg']));

$common = "0 for Main Menu, 9 to exit";
$common_bool = false;
$session=0;

if($msg == '0'){
	$sql = "DELETE FROM ussd WHERE tid='$tid'";
	mysql_query($sql);
	
	if($auth == 0){
		$message = "Enter company code\\nEnter Employee Code\\nEnter Your Date of Birth\nE.g For 1st Jan 1972 enter 01011972";
	}
	else {
		$message =  "1:View plan info\\n2:Intimate claim\\n3:Track claim status\\n4:Find Network Hospitals\\n5:Get Imp contact details\\n6:Get claims related docs";
		$type = "link";
	}
}
else {
	$add = true;
	$sql = "SELECT msg FROM ussd WHERE tid='$tid' order by id";
	$result = mysql_query($sql);
	$rows = mysql_num_rows($result);
	if($rows == 6){
		$row1 = mysql_fetch_array($result);
		$rowExt = mysql_fetch_array($result);
		
		if($row1['msg'] == '2'){
			$row2 = mysql_fetch_array($result);
			$employee_name = $res_arr['member_name'];
			$id = $res_arr['employee_id'];
			$reason = $row2['msg'];
			$row3 = mysql_fetch_array($result);
			$date = substr($row3['msg'],4) . "-" . substr($row3['msg'],2,2) . "-" . substr($row3['msg'],0,2);
			
			$validDate = date('Y-m-d', strtotime('-7 days'));
			if($date >= $validDate){
				$row4 = mysql_fetch_array($result);
				$hospi = $row4['msg'];
				$row5 = mysql_fetch_array($result);
				$city = $row5['msg'];
				$amount = $msg;
				$client_name = $res_arr['company_code'];
				$claimaint = $rowExt['msg'] - 1;
				
				$sql1 = "SELECT member_name,relationship FROM msh_employee WHERE employee_id='$id' AND company_code = '$client_name' limit $claimaint,1";
				$result1 = mysql_query($sql1);
				$res_row = mysql_fetch_array($result1);
				$claimaint = $res_row['member_name'];
				
				$sub = $client_name . ": Claim Intimation By $employee_name (Employee Code: $id)";
				$body = "Hi,<br/><br/>";
				$body .= "This is to inform that $employee_name with Employee Code: $id from $client_name has intimated a claim for $claimaint (" .$res_row['relationship'].") <br/><br/>";
				$body .= "Hospital Name: $hospi in $city<br/>";
				$body .= "Date of hospitalization: $date<br/>";
				$body .= "Reason of hospitalization: $reason<br/>";
				$body .= "Estimated claim amount: $amount<br/><br/>";
				
				$body .= "<b>Important contact details:</b><br/>";
				$body .= "Employee Mobile Number: ".$res_arr['mobile']."<br/>";
				
				$sql1 = "SELECT toll_free,emergency_no FROM msh_tpa WHERE tpa_id='".$res_arr['tpa_id']."'";
				$result1 = mysql_query($sql1);
				$res_row = mysql_fetch_array($result1);
				
				$body .= "TPA Call Centre Number: ".$res_row['toll_free']."<br/>";
				mailShoot($sub,$body,$email,0,0);
				mailShoot($sub,$body,$res_arr['email_id'],0,0);
				
				$message= "Claim details have been sent to TPA and Insurance company with a copy to your email id";
				$common_bool = true;
			
			}
			else {
				$sql1 = "SELECT toll_free,emergency_no FROM msh_tpa WHERE tpa_id='".$res_arr['tpa_id']."'";
				$result1 = mysql_query($sql1);
				$res_row = mysql_fetch_array($result1);
				
				$message .= "The claim should be intimated on admission to hospital or within 7 days. Since its more than 7 days, please contact TPA - " . $res_row['emergency_no'];
				$common_bool = true;
			}
		}
	
	}
	else if($rows == 5){
		$row1 = mysql_fetch_array($result);
		
		if($row1['msg'] == '2'){
			$message= "Enter Estimated claim amount:";
		}
		
	}
	else if($rows == 4){
		$row1 = mysql_fetch_array($result);
		$row2 = mysql_fetch_array($result);
		if($row1['msg'] == '2'){
			$message= "Enter Hospital - City:";
		}
		else if($row1['msg'] == '4' && $row2['msg'] == '2' && (!isset($res_arr['email_id']) || empty($res_arr['email_id']))){
			
			if(!emailValidation(trim($msg))){
				$message = "Enter valid email id";
				$add = false;
			}
			else {
				$row3 = mysql_fetch_array($result);
				$city = $row3['msg'];
				$row4 = mysql_fetch_array($result);
				$area = $row4['msg'];
				$sql = "SELECT * FROM msh_hospital WHERE lower(hospital_city) like '".strtolower($city)."' AND lower(hospital_address) like '%".strtolower($area)."%'";
				$res1 = mysql_query($sql);
				$res_arr1 = mysql_fetch_array($res1);
				
				$message = "An email is sent to ".trim($msg)." with all the search results";
				$sub = "Hospital Details for $area,$city";
				$body = "1)Name: " . $res_arr1['hospital_name'];
				$body .= "<br/>Address: ".$res_arr1['hospital_address'];
				$body .= "<br/>City: ".$res_arr1['hospital_city'];
				$i = 2;
				$msg1 = $sub . "\n" . br2newline($body) . "\n" . $message;
				while($res_arr1 = mysql_fetch_array($res1)){
					$body .= "<br/><br/>$i)Name: " . $res_arr1['hospital_name'];
					$body .= "<br/>Address: ".$res_arr1['hospital_address'];
					$body .= "<br/>City: ".$res_arr1['hospital_city'];
					$i++;
				}
				mailShoot($sub,$body,trim($msg),1,0,$msg1,$mobile);
				//mailShoot($sub,$body,trim($msg),1,0);
				$common_bool = true;
				mysql_query("UPDATE msh_employee SET email_id = '$msg' WHERE id = ". $res_arr['id']);
			}
		}
	}
	else if($rows == 3){
		$row1 = mysql_fetch_array($result);
		$row2 = mysql_fetch_array($result);
		$row3 = mysql_fetch_array($result);
		if($auth == 0){
			$date = substr($row3['msg'],4) . "-" . substr($row3['msg'],2,2) . "-" . substr($row3['msg'],0,2);
			$sql = "SELECT * FROM msh_employee WHERE lower(company_code)='".strtolower($row1['msg'])."' AND employee_id = '" . $row2['msg'] . "' AND dob = '$date' AND relationship = 'Employee'";
			$res = mysql_query($sql);
			if(!empty($res)){
				if($msg == '1'){
					$res_arr = mysql_fetch_array($res);
					$message = "Thank you for registration. For any help just call 02230932080 for interactive menu on your phone";
					$common_bool = true;
					mysql_query("UPDATE msh_employee SET mobile='$mobile',mobile_linked = '$mobile' WHERE id = " . $res_arr['id']);
					$sub = "Mobile Number Updated";
					$body = "Dear User,<br/>
Mobile Number linked to your policy has been changed to $mobile";
					mailShoot($sub,$body,$res_arr['email_id'],1,0);	
				}
				else if($msg == '2'){
					$message = "No Changes made to your profile. Thank you";
					$common_bool = true;
					//mysql_query("UPDATE msh_employee SET mobile_linked = '$mobile' WHERE id = " . $res_arr['id']);
				}
				else {
					$message= "Wrong Input !!";
				}
			}
			else {
				$message = "Authentication failed, Please check your details. Contact your HR or Marsh account manager for assistance in registration.";
				$common_bool = true;
			}
			$sql = "SELECT * FROM msh_employee WHERE lower(company_code)='".strtolower($row1['msg'])."' AND employee_id = '" . $row2['msg'] . "' AND dob = '$date'";
			$res = mysql_query($sql);
			$res_arr = mysql_fetch_array($res);
		}
		else if($row1['msg'] == '2'){
			$message= "Enter Hospital name:";
		}
		else if($row1['msg'] == '4' && $row2['msg'] == '1'){
			if(!emailValidation(trim($msg))){
				$message = "Enter valid email id";
				$add = false;
			}
			else {
				$sql = "SELECT * FROM msh_hospital WHERE hospital_address like '%".$row3['msg']."%'";
				$res1 = mysql_query($sql);
				$res_arr1 = mysql_fetch_array($res1);
				
				$message .= "An email is sent to ".trim($msg)." with all the search results";
				$sub = "Hospital Details for pincode " . $row3['msg'];
				$body = "1)Name: " . $res_arr1['hospital_name'];
				$body .= "<br/>Address: ".$res_arr1['hospital_address'];
				$body .= "<br/>City: ".$res_arr1['hospital_city'];
				$msg1 = $sub . "\n" . br2newline($body) . "\n" . $message;
				
				$i = 2;
				while($res_arr1 = mysql_fetch_array($res1)){
					$body .= "<br/><br/>$i)Name: " . $res_arr1['hospital_name'];
					$body .= "<br/>Address: ".$res_arr1['hospital_address'];
					$body .= "<br/>City: ".$res_arr1['hospital_city'];
					$i++;
				}
				//mailShoot($sub,$body,trim($msg),1,0);
				mailShoot($sub,$body,trim($msg),1,0,$msg1,$mobile);
				
				$common_bool = true;
				mysql_query("UPDATE msh_employee SET email_id = '$msg' WHERE id = ". $res_arr['id']);
			}
		}
		else if($row1['msg'] == '4' && $row2['msg'] == '2'){
			$city = $row3['msg'];
			$sql = "SELECT * FROM msh_hospital WHERE lower(hospital_city) like '".strtolower($city)."' AND lower(hospital_address) like '%".strtolower($msg)."%'";
			$res1 = mysql_query($sql);
			$res_arr1 = mysql_fetch_array($res1);
			if(!empty($res_arr1)){
				$message = $res_arr1['hospital_name'];
				$message .= "\\n".$res_arr1['hospital_address'];
				$message .= "\\n".$res_arr1['hospital_city'];
				if(isset($res_arr['email_id']) && !empty($res_arr['email_id'])){
					$message .= "\\nAn email is sent to ".$res_arr['email_id']." with all the search results";
					$sub = "Hospital Details for $msg,$city";
					$body = "1)Name: " . $res_arr1['hospital_name'];
					$body .= "<br/>Address: ".$res_arr1['hospital_address'];
					$body .= "<br/>City: ".$res_arr1['hospital_city'];
					$i = 2;
					while($res_arr1 = mysql_fetch_array($res1)){
						$body .= "<br/><br/>$i)Name: " . $res_arr1['hospital_name'];
						$body .= "<br/>Address: ".$res_arr1['hospital_address'];
						$body .= "<br/>City: ".$res_arr1['hospital_city'];
						$i++;
					}
					//mailShoot($sub,$body,$res_arr['email_id'],1,0);
					$sms = str_replace( "\\n", "\n", $message);
					mailShoot($sub,$body,$res_arr['email_id'],1,0,$sub ."\n" . $sms,$mobile);
					$common_bool = true;
				}
				else {
					$message .= "\\nEnter email id:";
				}
			}
			else {
				$message .= "No hospital found !!";
				$common_bool = true;
			}
		}
	}
	else if($rows == 2){
		$row1 = mysql_fetch_array($result);
		$row2 = mysql_fetch_array($result);
		if($auth == 0){
			$date = substr($msg,4) . "-" . substr($msg,2,2) . "-" . substr($msg,0,2);
			$sql = "SELECT * FROM msh_employee WHERE lower(company_code)='".strtolower($row1['msg'])."' AND employee_id = '" . $row2['msg'] . "' AND dob = '$date' AND relationship = 'Employee'";
			$res = mysql_query($sql);
			$res_arr = mysql_fetch_array($res);
			if(empty($res_arr)){
				$message = "Authentication failed, Please check your details. Contact your HR or Marsh account manager for assistance in registration.";
				$common_bool = true;
			}
			else if(empty($res_arr['mobile'])){
				$message = "Thank you for registration. For any help just call 02230932080 for interactive menu on your phone";
				$common_bool = true;
				mysql_query("UPDATE msh_employee SET mobile='$mobile',mobile_linked = '$mobile' WHERE id = " . $res_arr['id']);
				$sub = "Mobile Number Updated";
				$body = "Dear User,<br/>
Mobile Number linked to your policy has been updated to $mobile";
				mailShoot($sub,$body,$res_arr['email_id'],1,0);
			}
			else if($res_arr['mobile'] != $mobile){
				$message = "Mobile number is already updated for this account. Do you want to update it with new number?";
				$message .= "\\n1: Yes";
				$message .= "\\n2: No";
			}
			else {
				$message = "Thank you for registration. For any help just call 02230932080 for interactive menu on your phone";
				$common_bool = true;
				mysql_query("UPDATE msh_employee SET mobile_linked = '$mobile' WHERE id = " . $res_arr['id']);
			}
		}
		else if($row1['msg'] == '2'){
			$message= "Enter Date of hospitalization:";
			$message .= "\\nE.g For 1st Jan 1972 enter 01011972";
		}
		else if($row['msg'] == '3'){
			$id = $res_arr['employee_id'];
			$sql = "SELECT * FROM msh_claims WHERE employee_id='".$id."' AND company_code = '".$res_arr['company_code']."' order by id asc limit 2,3";
			$res1 = mysql_query($sql);
			$res_arr1 = mysql_fetch_array($res1);
			if(!empty($res_arr1)){
				$message .= "Number: " . $res_arr1['claim_number'];
				$message .= "\\nName: " . $res_arr1['member_name'];
				$message .= "\\nStatus: " . $res_arr1['claim_status'];
				$message .= "\\nClaimed Amount: " . $res_arr1['claim_amount'];
				if($res_arr1['claim_status'] == 'Settled'){
					$message .= "\\nSettled Date: " . $res_arr1['claim_settled_date'];
					$message .= "\\nPaid Amount: " . $res_arr1['paid_amount'];
				}
				if(mysql_num_rows($res1) > 1)
				$message .= "\\n\\nGo to TPA website for more claims";
				$common_bool = true;
			}
		}
		else if($row1['msg'] == '4' && $row2['msg'] == '1'){
			$sql = "SELECT * FROM msh_hospital WHERE hospital_address like '%$msg%'";
			$res1 = mysql_query($sql);
			$res_arr1 = mysql_fetch_array($res1);
			if(!empty($res_arr1)){
				$message = $res_arr1['hospital_name'];
				$message .= "\\n".$res_arr1['hospital_address'];
				$message .= "\\n".$res_arr1['hospital_city'];
				if(isset($res_arr['email_id']) && !empty($res_arr['email_id'])){
					$message .= "\\nAn email is sent to ".$res_arr['email_id']." with all the search results";
					$sub = "Hospital Details for pincode $msg";
					$body = "1)Name: " . $res_arr1['hospital_name'];
					$body .= "<br/>Address: ".$res_arr1['hospital_address'];
					$body .= "<br/>City: ".$res_arr1['hospital_city'];
					$i = 2;
					while($res_arr1 = mysql_fetch_array($res1)){
						$body .= "<br/><br/>$i)Name: " . $res_arr1['hospital_name'];
						$body .= "<br/>Address: ".$res_arr1['hospital_address'];
						$body .= "<br/>City: ".$res_arr1['hospital_city'];
						$i++;
					}
					//mailShoot($sub,$body,$res_arr['email_id'],1,0);
					$sms = str_replace( "\\n", "\n", $message);
					mailShoot($sub,$body,$res_arr['email_id'],1,0,$sub . "\n" . $sms,$mobile);
					$common_bool = true;
				}
				else {
					$message .= "\\nEnter email id:";
				}
				
			}
			else {
				$message .= "No hospital found !!";
				$common_bool = true;
			}
		}
		else if($row1['msg'] == '4' && $row2['msg'] == '2'){
			$message= "Enter your area name:";
		}
	}
	else if($rows == 1){
		$row = mysql_fetch_array($result);
		if($auth == 0){
			$message= "Enter Your Date Of Birth in DDMMYYYY format:";
			$message .= "\\nE.g For 1st Jan 1972 enter 01011972";
		}
		else if($row['msg'] == '2'){
			$message= "Enter reason for hospitalization:";
		}
		else if($row['msg'] == '3'){
			$id = $res_arr['employee_id'];
			$sql = "SELECT * FROM msh_claims WHERE employee_id='".$id."' AND company_code = '".$res_arr['company_code']."' order by id asc limit 1,3";
			$res1 = mysql_query($sql);
			$res_arr1 = mysql_fetch_array($res1);
			if(!empty($res_arr1)){
				$message .= "Number: " . $res_arr1['claim_number'];
				$message .= "\\nName: " . $res_arr1['member_name'];
				$message .= "\\nStatus: " . $res_arr1['claim_status'];
				$message .= "\\nClaimed Amount: " . $res_arr1['claim_amount'];
				if($res_arr1['claim_status'] == 'Settled'){
					$message .= "\\nSettled Date: " . $res_arr1['claim_settled_date'];
					$message .= "\\nPaid Amount: " . $res_arr1['paid_amount'];
				}
				if(mysql_num_rows($res1) > 1)
				$message .= "\\nPress 1 for more";
				else $common_bool = true;
			}
		}
		else if($row['msg'] == '4' && $msg == '1'){
			$message= "Enter your area pincode:";
		}
		else if($row['msg'] == '4' && $msg == '2'){
			$message= "Enter your city name:";
		}
		else if($row['msg'] == '5'){
			if(!emailValidation(trim($msg))){
				$message = emailValidation(trim($msg)) . " " . $msg;
				//$message = "Enter valid email id";
				$add = false;
			}
			else {
				$tpa_id = $res_arr['tpa_id'];
				$sql = "SELECT * FROM msh_tpa WHERE tpa_id='$tpa_id'";
				$res1 = mysql_query($sql);
				$res_arr1 = mysql_fetch_array($res1);
				$message = "TPA Toll Free Number: " . $res_arr1['toll_free'];
				$message .= "\\nEmergency Number: " . $res_arr1['emergency_no'];
				$message .= "\\nAn email is sent to " . trim($msg);
				$sub = "Important Contact Details";
				$body = "TPA Toll Free Number: " . $res_arr1['toll_free'];
				$body .= "<br/>Emergency Number: " . $res_arr1['emergency_no'];
				mailShoot($sub,$body,trim($msg),1,0);
				$common_bool = true;
				mysql_query("UPDATE msh_employee SET email_id = '$msg' WHERE id = ". $res_arr['id']);
			}
		}
		else if($row['msg'] == '6'){
			if(!emailValidation(trim($msg))){
				$message = "Enter valid email id";
				$add = false;
			}
			else { 
				$message = "An email containing Pre-authorization and Claim Form is sent to " . $msg;
				$sub = "Pre-authorization and Claim Form";
				$body = "Please find attached the documents<br/><br/>";
				mailShoot($sub,$body,trim($msg),1,1);
				$common_bool = true;
				mysql_query("UPDATE msh_employee SET email_id = '$msg' WHERE id = ". $res_arr['id']);
			}
		}
	}
	else {
		if($auth == 0){
			$message= 'Enter Employee Code:';
		}
		else if($msg == '1'){
			$policy = $res_arr['policy_num'];
			$sql = "SELECT sum_insured,maternity_limit,dependents,notes FROM msh_policy WHERE policy_no='".$policy."'";
			$res1 = mysql_query($sql);
			$res_arr1 = mysql_fetch_array($res1);
			$res2 = mysql_query("SELECT balance_sum_insured FROM msh_claims WHERE employee_id = '".$res_arr['employee_id']."' AND company_code = '".$res_arr['company_code']."' AND balance_sum_insured is not null");
			$res_arr2 = mysql_fetch_array($res2);
			if(empty($res_arr2)) $balance = $res_arr1['sum_insured'];
			else $balance = $res_arr2['balance_sum_insured'];
			$message = "Sum Insured: " . $res_arr1['sum_insured'];
			$message .= "\\nMaternity cover: " . $res_arr1['maternity_limit'];
			$message .= "\\nEligible Dependants: " . $res_arr1['dependents'];
			$message .= "\\nBalance Sum Insured:" . $balance;
			if(!empty($res_arr1['notes']))
				$message .= "\\nNotes:" . $res_arr1['maternity_limit'];
			$common_bool = true;
		}
		else if($msg == '2'){
			$policy = $res_arr['policy_num'];
			$sql = "SELECT relationship,member_name FROM msh_employee WHERE employee_id='".$res_arr['employee_id']."'";
			$res1 = mysql_query($sql);
			$dependents = array();
			$i = 1;
			while($res_arr1 = mysql_fetch_array($res1)){
				$message .= "$i.".$res_arr1['relationship'] . ": " .  $res_arr1['member_name'] . "\\n";
				$i++;
			}
			
			$message .= "Step 1: Select Claimant:";
		}
		else if($msg == '3'){
			$id = $res_arr['employee_id'];
			$sql = "SELECT * FROM msh_claims WHERE employee_id='".$id."' AND company_code = '".$res_arr['company_code']."' order by id asc";
			$res1 = mysql_query($sql);
			$res_arr1 = mysql_fetch_array($res1);
			if(!empty($res_arr1)){
				$message .= "Number: " . $res_arr1['claim_number'];
				$message .= "\\nName: " . $res_arr1['member_name'];
				$message .= "\\nStatus: " . $res_arr1['claim_status'];
				$message .= "\\nClaimed Amount: " . $res_arr1['claim_amount'];
				if($res_arr1['claim_status'] == 'Settled'){
					$message .= "\\nSettled Date: " . $res_arr1['claim_settled_date'];
					$message .= "\\nPaid Amount: " . $res_arr1['paid_amount'];
				}
				if(mysql_num_rows($res1) > 1)
				$message .= "\\nPress 1 for more";
				else $common_bool = true;
			}
			else {
				$message = "No claims found !!";
				$common_bool = true;
			}
		}
		else if($msg == '4'){
			$message = "Press 1: Track through Pincode";
			$message .= "\\nPress 2: Track through City And Area name";
		}
		else if($msg == '5'){
			$tpa_id = $res_arr['tpa_id'];
			$sql = "SELECT * FROM msh_tpa WHERE tpa_id='$tpa_id'";
			$res1 = mysql_query($sql);
			$res_arr1 = mysql_fetch_array($res1);
			$message = "TPA Toll Free Number: " . $res_arr1['toll_free'];
			$message .= "\\nEmergency Number: " . $res_arr1['emergency_no'];
			if(isset($res_arr['email_id']) && !empty($res_arr['email_id'])){
				$message .= "\\nAn email is sent to " . $res_arr['email_id'];
				$sub = "Important Contact Details";
				$body = "TPA Toll Free Number: " . $res_arr1['toll_free'];
				$body .= "<br/>Emergency Number: " . $res_arr1['emergency_no'];
				mailShoot($sub,$body,$res_arr['email_id'],1,0);
				$common_bool = true;
			}
			else {
				$message = "Enter email id:";
			}
		}
		else if($msg == '6'){
			if(isset($res_arr['email_id']) && !empty($res_arr['email_id'])){
				$message = "An email containing Pre-authorization and Claim Form is sent to " . $res_arr['email_id'];
				$sub = "Pre-authorization and Claim Form";
				$body = "Please find attached the documents<br/>";
				mailShoot($sub,$body,$res_arr['email_id'],1,1);
				$common_bool = true;
			}
			else {
				$message = "Enter email id:";
			}
		}
	}
	
	$msg_next = "";
	if($common_bool) {
		if(strlen($message) > 125){
			$msg_next = substr($message,105) . "\\n\\nPress $common";
			$message = substr($message,0,105) . "\\n\\nPress M to read more, $common";
		}
		else {
			$message .= "\\n\\nPress $common";
		}
	}
	else {
		if(strlen($message) > 160){
			$msg_next = substr($message,140);
			$message = substr($message,0,140) . "\\n\\nPress M to read more";
		}
	}
}
}
echo $add."***".$session."***".$message."***".$msg_next;
?>