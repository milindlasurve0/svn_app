<?php
if(isset($_REQUEST['format']) && $_REQUEST['format'] == 'json'){
	
}
else header('Content-type: text/xml');
//$site = "www.smstadka.com/apis/subscribe.php?username=USERNAME&password=PASSWORD&productId=ID&mobile=MOBILE&transId=TRANSID&retailCode=RETAILCODE&param1=PARAM1";

include("../../config/bootstrap.php");
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die ('Error connecting to mysql');
mysql_select_db(DB_DB);

$username = trim(urldecode($_REQUEST['username']));
$password = trim(urldecode($_REQUEST['password']));
$product_id = trim(urldecode($_REQUEST['productId']));
$mobile = trim($_REQUEST['mobile']);
$transaction_id = trim(urldecode($_REQUEST['transId']));
$retail_code = trim(urldecode($_REQUEST['retailCode']));
$param1 = 'NULL';
if(isset($_REQUEST['param1'])) $param1 = $_REQUEST['param1'];
$param = trim(urldecode($param1));

if(empty($username) || empty($password) || empty($product_id) || empty($mobile) || empty($transaction_id) || empty($retail_code)){
	$xml = "<error>
		<code>0</code>
		<message>Missing/Invalid Parameter(s). Please check the request url.</message>
		</error>";
}
else {

	$sql = "SELECT id FROM vendors WHERE username = '$username' and password = '$password'";
	$result = mysql_query($sql);
	if($row = mysql_fetch_array($result)){
		$sql = "INSERT INTO api_logs(vendor_id,product_id,mobile,param1,trans_id,retail_code,timestamp) VALUES (".$row['id'].",'$product_id','$mobile','$param','$transaction_id','$retail_code','".date("Y-m-d H:i:s")."')";
		$result = mysql_query($sql);
		// create a new cURL resource
		$ch = curl_init();
		$sql1 = "SELECT id FROM vendors_activations WHERE vendor_id = " . $row['id'] . " AND vendor_trans_id = '$transaction_id'";
		$result_check = mysql_query($sql1);
		
		if($row_check = mysql_fetch_array($result_check)){
			//REQUEST came again for same transaction id	
			$xml =  "<error>
			<code>2</code>
			<message>Request sent again for same transaction id</message>
			</error>";
		}
		else if(strlen($mobile) != 10){
			$xml =  "<error>
			<code>3</code>
			<message>Mobile Number is not valid</message>
			</error>";
		}
		else {
			// set URL and other appropriate options
			if($param1 == 'NULL')$param1 = -1;
			$params = "mobile_number=".$_REQUEST['mobile']."&product_id=".$_REQUEST['productId']."&param=$param1&trans_id=".$_REQUEST['transId']."&retailer_code=".$_REQUEST['retailCode']."&vendor_id=".$row['id']; //you must know what you want to post
			$url = SITE_NAME . "shops/retailerProdActivation";
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
		
			// grab URL and pass it to the browser
			$response = curl_exec($ch);
			$ret = explode("#*#",$response);
			
			if($ret[2] == "-1"){
				$xml =  "<error>
				<code>4</code>
				<message>Product Id is not valid</message>
				</error>";
			}
			else {
				if(isset($ret[4])){
					$ret[2] .= "<br/>" . $ret[4];
				}
				$xml = "<resultActivation>
				<transId>".$ret[0]."</transId>
				<result>".$ret[1]."</result>
				<message>".$ret[2]."</message>";
				if($ret[3] != '0') {
					$xml .= "<referenceNumber>".$ret[3]."</referenceNumber>";
				}
				$xml .= "</resultActivation>";
			}
			// close cURL resource, and free up system resources
			
			curl_close($ch);
		}
	}
	else {
		$xml = "<error>
			<code>1</code>
			<message>Wrong UserName or Password</message>
			</error>";
	}
}

if(isset($_REQUEST['format']) && $_REQUEST['format'] == 'json'){
	$xml = json_encode(xml2array($xml));
}

echo $xml;
function xml2array($contents, $get_attributes=1, $priority = 'tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;//Hmm...

		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();

		$current = &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble
				
			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.
				
			$result = array();
			$attributes_data = array();
				
			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}
				
			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}
				
			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;
						
					$current = &$current[$tag];
						
				} else { //There was another element with the same tag name
						
					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;

						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;
						
				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
								
							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}

		return($xml_array);
	}
?>