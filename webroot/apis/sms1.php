<?php
include("../../config/bootstrap.php");

function curl_post_async($url, $params=null)
{
    foreach ($params as $key => &$val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}

function droppedTransactions($type,$sender,$msg,$sending_time){
	$data = array();
	$url = "http://54.235.195.140/apis/dropped";
	$data['type'] = $type;
	$data['sender'] = $sender;
	$data['msg'] = $msg;
	$data['time'] = $sending_time;
	curl_post_async($url,$data);
}

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
$headers .= "From: admin@smstadka.com\r\n";
$headers .= "Reply-To: admin@smstadka.com\r\n";
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die (mail('tadka@mindsarray.com','VMN: DB connection refused inside sms incoming script','Reason: '.mysql_error(),$headers));
mysql_select_db(DB_DB);

$smsdata = array();

if(isset($_REQUEST['data'])){
	$msg = trim(urldecode($_REQUEST['data']));
	$smsdata = json_decode($msg,true);
}
else {
	$smsdata[] = $_REQUEST;
}


foreach($smsdata as $dt){
	$msg = trim(urldecode($dt['message']));
	$code = trim(urldecode($dt['code']));
	$mobile =  substr(trim(urldecode($dt['mobile'])),-10);
	if(!empty($dt['timestamp'])) $timestamp = trim(urldecode($dt['timestamp']));
	else $timestamp = date('Y-m-d H:i:s');
	
	$sql = "INSERT INTO virtual_number(mobile,message,virtual_num,sms_time,timestamp) VALUES ('$mobile','$msg','$code','$timestamp','".date("Y-m-d H:i:s")."')";
	mysql_query($sql);
		
	if(substr($code,-10) == '9619972172' || (substr($code,-10) == '9223178889' && strpos(strtoupper($msg),'DRL') !== false)){//sarfaraz
		if(empty($timestamp)){
			$timestamp = date('Y-m-d H:i:s');
		}
		$message = addslashes($msg);
		$url = "http://www.24x7sms.com/getmessage.aspx?VNumber=".substr($code,-10)."&MobileNo=$mobile&Message=".urlencode($msg)."&TimeSent=".urlencode($timestamp);
		shell_exec("nohup wget '$url' > /dev/null 2> /dev/null & echo $!");
	}
	else if(strtolower(substr($msg,0,3)) == 'pay' || strtolower(substr($msg,0,1)) == '*' || strtolower(substr($msg,0,1)) == '#'){
		$send = false;
		
		$msg1 = explode("\n",$msg);
		$msg = $msg1[0];
		$sms_time = $timestamp;
		
		if(substr($code,-10) == '9223178889'){
			$times = explode(" ", $sms_time);
        	$dates = explode("/",$times[0]);
        	$sms_time = $dates[2] . "-" . $dates[1] . "-" . $dates[0] . " " . $times[1];
		}
		
		$right_time = date('Y-m-d H:i:s',strtotime('- 7 mins'));
		
		if($sms_time < $right_time){
			$url = SERVER_BACKUP . 'users/sendMsgMails';
			$data = array();
			$data['mail_subject'] = "$code: SMS Dropped due to late delivery";
			$data['mail_body'] = "Sender: $mobile<br/>Message: $msg<br/>SMS Time: $sms_time";
			curl_post_async($url,$data);
			
			droppedTransactions('late',$mobile,$msg,$sms_time);
			mysql_query("INSERT INTO processes_pay1_dropped (sender,msg,sms_time,datetime,virtual_num,type) VALUES ('$mobile','".addslashes($msg)."','$sms_time','".date('Y-m-d H:i:s')."','".substr($code,-10)."',1)");
		}
		else if(strtolower(substr($msg,0,1)) == '*' || strpos(strtolower($msg),'pay1 tb') !== false){
			if(strtolower(substr($msg,0,1)) == '*')
				$msg = str_replace(' ', '', $msg);
		
			$timestamp1 = time() + 300;
			$code1 = substr($code,-10);
		
			if($code1 == '9223126272'){
				$msg_ret = "Dear Pay1 Retailer,
Yeh number abhi chalu nahi hai. Kripiya recharge karne ke liye 9223178889 ya 9821232431 number par recharge kijiye";
				$url = SERVER_BACKUP . 'users/sendMsgMails';
				
				$data1['sender'] = '';
				$data1['mobile'] = $sender;
				$data1['sms'] = $msg_ret;
				$data1['root'] = 'shops';
				curl_post_async($url,$data1);
				continue;
			}
			
			$sql = "INSERT INTO processes_pay1 (sender,msg,sms_time,timestamp,virtual_num) VALUES ('$mobile','".addslashes($msg)."','$sms_time','$timestamp1','".$code1."')";
			mysql_query($sql);
			$id = mysql_insert_id();
			if($id == '0'){
				droppedTransactions('repeat',$mobile,$msg,$sms_time);
				mysql_query("INSERT INTO processes_pay1_dropped (sender,msg,sms_time,datetime,virtual_num) VALUES ('$mobile','".addslashes($msg)."','$sms_time','".date('Y-m-d H:i:s')."','".$code1."')");
			}
		}
		else {
			$data = array();
			$url = "http://54.235.195.140/apis/receiveSMS";
			$sha = strtoupper(sha1($mobile.$msg.$sms_time.substr($code,-10)."51gh2345"));
			$data['mobile'] = $mobile;
			$data['message'] = $msg;
			$data['password'] = 's1tadka';
			$data['code'] = substr($code,-10);
			$data['smstime'] = $sms_time;
			$data['sha'] = $sha;
			
			curl_post_async($url,$data);
		}
	}
	else {
		$data = array();
		$url = SITE_NAME . "groups/requestBySMS";
		$data['mobile'] = urlencode($mobile);
		$data['message'] = urlencode($msg);
		$data['password'] = 's1tadka';
		$data['virtual'] = 1;
		curl_post_async($url,$data);
	}
}
?>