<?php

include("../../config/bootstrap.php");
require("vmnManager.php");

/*
 * 
 */
//$API_Receivesms = 'http://54.235.195.140/apis/receiveSMS';
$API_Receivesms = 'http://54.235.193.96/apis/receiveSMS';
//$API_Receivesms = "http://localhost/apitest/API_test.php?c=receiveSMS";
$API_sendMail = 'http://www.smstadka.com/users/sendMsgMails';
		

/*
 * 
 */

function curl_post_async($url, $params=null)
{
    foreach ($params as $key => &$val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}


/**
 * Function to call ReceivesmsAPI when valid request received for pay1 application
 */
function call_Receivesms($mobile, $msg, $sms_time, $code) {
    $data = array();
    global $API_Receivesms;
    $url = $API_Receivesms;
    $sha = strtoupper(sha1($mobile . $msg . $sms_time . substr($code, -10) . "51gh2345"));
    $data['mobile'] = $mobile;
    $data['message'] = $msg;
    $data['password'] = 's1tadka';
    $data['code'] = substr($code, -10);
    $data['smstime'] = $sms_time;
    $data['sha'] = $sha;
    curl_post_async($url, $data);
}

//---threshold decide how many request to be pass to pay1 application

$request_threshold = 2;
$vmnObj1 = new vmnManager();


while(true){
    $logger = $vmnObj1->logger('sms_', 'sms_in_Pay1');
    $logger->info("pay1 process started ===== ".date('Y-m-d H:i:s'));
    $len = $vmnObj1->length_transaction_queue();
	
    try {
        $logger->info("-----------");
    } catch (Exception $ex) {
        $logger = $vmnObj1->logger('pay1_new_script', 'sms_in_Pay1');
        $logger->info("-----------");
    }

    if($len >= 200){
		$data = array();
		$data['mail_subject'] = "Pay1 Recharge Alert !! More than 200 requests in last few secs";
		$data['mail_body'] = "All requests are on hold. Kindly check<br/>Requests deleted:";
		//$smsdata = 
		while($len > 0){
			$smsdata = $vmnObj1->pull_transaction_queue();
			$smsdata = json_decode($smsdata,true);
			/*$url = "http://54.235.195.140/apis/repeatedTrans";
			$data = array();
			$data['mobile'] = $res['sender'];
			$data['message'] = $res['msg'];
			
			$data['type'] = "1";
			curl_post_async($url,$data);*/
			//usleep(500000);
			$logger->info("pay1 requests deleted due to flux ===== ".date('Y-m-d H:i:s'). "::: VMN Number: ".$smsdata['code'].":: Sender: ".$smsdata['mobile']. ":: Message :". $smsdata['message']);
            if(!empty($smsdata)){
                $data['mail_body'] .= "<br/>VMN Number: ".$smsdata['code'].":: Sender: ".$smsdata['mobile']. ":: Message :". $smsdata['message'];
            }
			$len = $vmnObj1->length_transaction_queue();
		}
		$data['emails'] = 'backend@mindsarray.com,tadka@mindsarray.com';
		curl_post_async($API_sendMail,$data);
		sleep(2);
	}
	else {
	    $smsdata = $vmnObj1->pull_transaction_queue();
	
	    if ($smsdata === NULL) {
	        sleep(2);
	        continue;
	    } else {
	        $dt = json_decode(stripslashes(trim($smsdata,'\\r')), true);
	    }
	
            $logger->info("unq_Id : " . $dt['unique_request_id'] . "fetched data from transaction queue to process:".$dt);
	        
	    $msg = isset($dt['message']) ? urldecode(trim($dt['message'],"\\r")) : (isset($dt['msg']) ? trim(urldecode($dt['msg']),"\\r") : "");
    	$code = isset($dt['code']) ? trim(urldecode($dt['code'])) : (isset($dt['vnumber']) ? trim(urldecode($dt['vnumber'])) : "");
    	$mobile = isset($dt['mobile']) ? substr(trim(urldecode($dt['mobile'])), -10) : (isset($dt['sender']) ? substr(trim(urldecode($dt['sender'])),-10) : "");
        if (isset($dt['timestamp']) && !empty($dt['timestamp'])) {
	        $timestamp = trim(urldecode($dt['timestamp']));
	    } 
		else if (isset($dt['stime']) && !empty($dt['stime'])) {
        	$timestamp = trim(urldecode($dt['stime']));
        	$timestamp = date('Y-m-d H:i:s',strtotime($timestamp));
    	}
	    else {
	        $timestamp = date('Y-m-d H:i:s');
	    }
	    $logger->info("unq_Id : " . $dt['unique_request_id'] . "Calling Receive sms API");
	    call_Receivesms($mobile, $msg, $timestamp, $code);
	    $waittime = 1000000/$request_threshold;
	    
	    $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' sleeping for microseconds : '.$waittime);    
	    usleep($waittime);
	}
}
?>