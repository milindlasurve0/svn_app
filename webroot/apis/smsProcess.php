<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of smsProcess
 *
 * @author nandan
 */
//include("/var/www/html/smstadka/app/config/bootstrap.php");
//require("/var/www/html/smstadka/app/vendors/predis/autoload.php");
//require("/var/www/html/smstadka/app/webroot/vmnManager.php");

include("../../config/bootstrap.php");
//require("../../vendors/predis/autoload.php");
require("vmnManager.php");


/**
 * List of urls used in this script
 * 
 */

//$API_drop_trans = "http://54.235.195.140/apis/dropped";
$API_drop_trans = "http://54.235.193.96/apis/dropped";
$API_24X7 = "http://www.24x7sms.com/getmessage.aspx";
$API_sendmsgmail = SERVER_BACKUP . 'users/sendMsgMails';
$API_Receivesms = 'http://54.235.193.96/apis/receiveSMS';
$API_grouprequest = SITE_NAME . "groups/requestBySMS";
$API_redeem_freebie = "http://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/Redeem_Freebie/?";

//-- for test uncomment below lines

/*
$API_drop_trans = "http://localhost/apitest/API_test.php?c=dropped";
$API_24X7 = "http://localhost/apitest/API_test.php";
$API_sendmsgmail = "http://localhost/apitest/API_test.php?c=sendMsgMails";
$API_Receivesms = "http://localhost/apitest/API_test.php?c=receiveSMS";
$API_grouprequest = "http://localhost/apitest/API_test.php?c=requestBySMS";
*/

/**
 * Function for to make async url call
 */
function curl_post_async($url, $params = null) {
    foreach ($params as $key => &$val) {
        if (is_array($val)) {
            $val = implode(',', $val);
        }
        $post_params[] = $key . '=' . urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts = parse_url($url);

    $fp = pfsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80, $errno, $errstr, 30);

    $out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
    $out.= "Host: " . $parts['host'] . "\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: " . strlen($post_string) . "\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string))
        $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}

/**
 * This function is using curl lib
 * @param type $url
 * @param type $params
 */
function curl_post_async_follow($url,$params=null){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    $str = trim(curl_exec($ch));
    curl_close($ch);
}


/**
 * Function to notify app regarding dropped transaction via async curl function
 */
function droppedTransactions($type, $sender, $msg, $sending_time) {
    $data = array();
    global $API_drop_trans;
    $url = $API_drop_trans;
    $data['type'] = $type;
    $data['sender'] = $sender;
    $data['msg'] = $msg;
    $data['time'] = $sending_time;
    curl_post_async_follow($url, $data);
}

/**
 * Function to call ReceivesmsAPI when valid request received for pay1 application
 */
function call_Receivesms($mobile, $msg, $sms_time, $code) {
    $data = array();
    global $API_Receivesms;
    $url = $API_Receivesms;
    $sha = strtoupper(sha1($mobile . $msg . $sms_time . substr($code, -10) . "51gh2345"));
    $data['mobile'] = $mobile;
    $data['message'] = $msg;
    $data['password'] = 's1tadka';
    $data['code'] = substr($code, -10);
    $data['smstime'] = $sms_time;
    $data['sha'] = $sha;
    curl_post_async_follow($url, $data);    
}

/**
 * headers variable is set to use while sending alert to user via mail
 */
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
$headers .= "From: admin@smstadka.com\r\n";
$headers .= "Reply-To: admin@smstadka.com\r\n";

/**
 * Creating database connection
 */
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mail('tadka@mindsarray.com', 'VMN: DB connection refused inside sms incoming script', 'Reason: ' . mysql_error(), $headers));
mysql_select_db(DB_DB);

/**
 * Creating object of vmnManager
 */
$vmnObj1 = new vmnManager();
$logger = $vmnObj1->logger('sms_Process', 'sms_in_Processor');
$redisObj = $vmnObj1->get_rconnection();

while (true) {
    $logger->info("smsProcessor started ===== " . date('Y-m-d H:i:s'));
	
	$dt = "";

    /* ---  pulling data from redis queue  ----    */
    $smsdata = $vmnObj1->pull_vmn_queue();

    if ($smsdata === NULL) {
        sleep(2);
        continue;
    } else {
        $dt = json_decode(stripslashes(trim($smsdata,'\\r')), true);
    }
    try{
       $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' : fetched data from redis ' . $smsdata);
    }  catch (Exception $e){
        $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' : fetched data from redis ' . $smsdata);
    }
    $msg = isset($dt['message']) ? trim(urldecode($dt['message']),"\\r") : (isset($dt['msg']) ? trim(urldecode($dt['msg']),"\\r") : "");
    $code = isset($dt['code']) ? trim(urldecode($dt['code'])) : (isset($dt['vnumber']) ? trim(urldecode($dt['vnumber'])) : "");
    $mobile = isset($dt['mobile']) ? substr(trim(urldecode($dt['mobile'])), -10) : (isset($dt['sender']) ? substr(trim(urldecode($dt['sender'])),-10) : "");
    
    $msg = trim($msg);
    $logger->debug("unq_Id : " . $dt['unique_request_id'] . "msg,code,mobile" . $msg . " " . $code . " " . $mobile);
    $time_flag = 0;

    if (isset($dt['timestamp']) && !empty($dt['timestamp'])) {
        $timestamp = trim(urldecode($dt['timestamp']));
    }
    else if (isset($dt['stime']) && !empty($dt['stime'])) {
        $timestamp = trim(urldecode($dt['stime']));
        $timestamp = date('Y-m-d H:i:s',strtotime($timestamp));
       
    } else {
        $timestamp = date('Y-m-d H:i:s');
        $time_flag = 1;
    }
    
	if (substr($code, -10) == '9223178889' || substr($code, -10) == '9289229929') {            
    //if (substr($code, -10) == '9289229929') {            
      	if(strpos($timestamp,'/')){
      		$times = explode(' ',$timestamp);
      		$dates = explode("/", $times[0]);
      		$timestamp = $dates[2] . "-" . $dates[1] . "-" . $dates[0] . " " . $times[1];
      	}
      	$logger->debug("unq_Id : " . $dt['unique_request_id'] . " : formated timestamp pattern to : " . $timestamp);
    }
   
    //-----------------VMN AUTO---------------   
    $vmnList = $redisObj->hkeys("VMNOnOff");
    $vmnNumber = substr($code, -10);
    
    if(!empty($vmnList) && in_array($vmnNumber, $vmnList)){
        $msg_ret = $redisObj->hget("VMNOnOff", $vmnNumber);
        $url = $API_sendmsgmail;				
        $data1['sender'] = '';
        $data1['mobile'] = $mobile;
        $data1['sms'] = $msg_ret;
        $data1['root'] = 'shops';
        curl_post_async_follow($url,$data1);
        
    }elseif(strtolower(substr(trim($msg), 0, 3)) == 'red'){
        $fb_url = $API_redeem_freebie;
        $fb_data['sender'] = $mobile;
        $fb_data['message'] = $msg;
        $logger->debug("unq_Id : ".$dt['unique_request_id'].' : calling freebie redeem API : '.$API_redeem_freebie." : msg : ".json_encode($fb_data));
        curl_post_async($fb_url, $fb_data);
        
    } else if (substr($code, -10) == '9619972172' || substr($code, -10) == '9681087993' || (substr($code, -10) == '9223178889' && strpos(strtoupper($msg), 'DRL') !== false)) {//sarfaraz
        $url = $API_24X7 . "?VNumber=" . substr($code, -10) . "&MobileNo=$mobile&Message=" . urlencode($msg) . "&TimeSent=" . urlencode($timestamp);
        //shell_exec("nohup wget '$url' > /dev/null 2> /dev/null & echo $!");
        shell_exec("nohup wget -O/dev/null '$url' & echo $!");    
        $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' : Called API_24x7 : ' . $url);
    } else if (strtolower(substr($msg, 0, 3)) == 'pay' || strtolower(substr($msg, 0, 1)) == '*' || strtolower(substr($msg, 0, 1)) == '#') {
        $send = false;
        $msg1 = explode("\n", $msg);
        $msg = $msg1[0];
        $sms_time = $timestamp;

        $right_time = date('Y-m-d H:i:s', strtotime('- 7 mins'));

        if ($time_flag === 0 && strtotime($sms_time) < strtotime($right_time)) {
            $url = $API_sendmsgmail;
            $data = array();
            $data['mail_subject'] = "$code: SMS Dropped due to late delivery";
            $data['mail_body'] = "Sender: $mobile<br/>Message: $msg<br/>SMS Time: $sms_time";
            //curl_post_async($url,$data);

            $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' :  dropping data becoz of time diff more than 7 min::'. "$mobile:$msg:$sms_time:$code");

            droppedTransactions('late', $mobile, $msg, $sms_time);
           // mysql_query("INSERT INTO processes_pay1_dropped (sender,msg,sms_time,datetime,virtual_num,type) VALUES ('$mobile','" . addslashes($msg) . "','$sms_time','" . date('Y-m-d H:i:s') . "','" . substr($code, -10) . "',1)");
        } elseif (strtolower(substr($msg, 0, 1)) == '*' || strpos(strtolower($msg), 'pay1 tb') !== false) {

            if (strtolower(substr($msg, 0, 1)) == '*') {
                $msg = str_replace(' ', '', $msg);
            }

            //$timestamp1 = time() + 300;
            $code1 = substr($code, -10);

            //* ---- check if key exist  -- */
            if ($vmnObj1->check_key_exist("5minwall:".$mobile.":" . $msg)) {
                //* ---------- Drop request if exist------- */
                //$logger->debug("unq_Id : " . $dt['unique_request_id'] . ' :  dropping data becoz of repeated same request within 5 min');
				
            	$logger->debug("unq_Id : " . $dt['unique_request_id'] . ' :  dropping data becoz of repeated same request within 5 min::'. "$mobile:$msg:$sms_time:$code1");
            	
                droppedTransactions('repeat', $mobile, $msg, $sms_time);
                //mysql_query("INSERT INTO processes_pay1_dropped (sender,msg,sms_time,datetime,virtual_num) VALUES ('$mobile','" . addslashes($msg) . "','$sms_time','" . date('Y-m-d H:i:s') . "','" . $code1 . "')");
            } else {
                //* ---- set in redis with expiry of 5 mins  ------------ */
                $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' :  putting data in redis with 5 min expiry');
                $vmnObj1->set_key_with_expiry("5minwall:".$mobile.":" . $msg, $code1, 5 * 60);             
                //* ======= call receivesms API instead of entering in processes_pay1 table======= */
                $vmnObj1->push_transaction_queue($smsdata);
                $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' : inserted request in pay1TM Queue for maintaining transaction queue ');
                //call_Receivesms($mobile, $msg, $sms_time, $code);
                //$logger->debug("unq_Id : " . $dt['unique_request_id'] . ' : called ReceiveSMS API when msg in (*, pay1 tb) : ' . $API_Receivesms);
            }
        } else {
            //* ======= call receivesms API =================== */
            call_Receivesms($mobile, $msg, $sms_time, $code);
            $logger->debug("unq_Id : " . $dt['unique_request_id'] . ' : called ReceiveSMS API when msg not in (*, pay1 tb) :' . $API_Receivesms);
        }
    } else {
        $sql = "INSERT INTO virtual_number(mobile,message,virtual_num,sms_time,timestamp) VALUES ('$mobile','$msg','$code','$timestamp','" . date("Y-m-d H:i:s") . "')";
        $logger->debug("unq_Id : Inserting record in virtual_number table" . $sql);
        mysql_query($sql);
        $data = array();
        $url = $API_grouprequest;
        $data['virtual_number'] = substr($code, -10);
        $data['mobile'] = $mobile;
        $data['message'] = $msg;
        $data['password'] = 's1tadka';
        $data['virtual'] = 1;
        $logger->debug("unq_Id : calling group request API : " . $API_grouprequest);
        curl_post_async_follow($url, $data);
    }
}


