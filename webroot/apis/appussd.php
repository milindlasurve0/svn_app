<?php
include("../../config/bootstrap.php");
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die ('Error connecting to mysql');
mysql_select_db(DB_DB);

function xml2array($contents, $get_attributes=1, $priority = 'tag') {
	if(!$contents) return array();

	if(!function_exists('xml_parser_create')) {
		//print "'xml_parser_create()' function not found!";
		return array();
	}

	//Get the XML parser of PHP - PHP must have this module for the parser to work
	$parser = xml_parser_create('');
	xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
	xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
	xml_parse_into_struct($parser, trim($contents), $xml_values);
	xml_parser_free($parser);

	if(!$xml_values) return;//Hmm...

	//Initializations
	$xml_array = array();
	$parents = array();
	$opened_tags = array();
	$arr = array();

	$current = &$xml_array; //Refference

	//Go through the tags.
	$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
	foreach($xml_values as $data) {
		unset($attributes,$value);//Remove existing values, or there will be trouble
			
		//This command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data);//We could use the array by itself, but this cooler.
			
		$result = array();
		$attributes_data = array();
			
		if(isset($value)) {
			if($priority == 'tag') $result = $value;
			else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
		}
			
		//Set the attributes too.
		if(isset($attributes) and $get_attributes) {
			foreach($attributes as $attr => $val) {
				if($priority == 'tag') $attributes_data[$attr] = $val;
				else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
			}
		}
			
		//See tag status and do the needed.
		if($type == "open") {//The starting of the tag '<tag>'
			$parent[$level-1] = &$current;
			if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
				$current[$tag] = $result;
				if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
				$repeated_tag_index[$tag.'_'.$level] = 1;
					
				$current = &$current[$tag];
					
			} else { //There was another element with the same tag name
					
				if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
					$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
					$repeated_tag_index[$tag.'_'.$level]++;
				} else {//This section will make the value an array if multiple tags with the same name appear together
					$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
					$repeated_tag_index[$tag.'_'.$level] = 2;

					if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
						$current[$tag]['0_attr'] = $current[$tag.'_attr'];
						unset($current[$tag.'_attr']);
					}

				}
				$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
				$current = &$current[$tag][$last_item_index];
			}

		} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
			//See if the key is already taken.
			if(!isset($current[$tag])) { //New Key
				$current[$tag] = $result;
				$repeated_tag_index[$tag.'_'.$level] = 1;
				if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;
					
			} else { //If taken, put all things inside a list(array)
				if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

					// ...push the new element into that array.
					$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

					if($priority == 'tag' and $get_attributes and $attributes_data) {
						$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
					}
					$repeated_tag_index[$tag.'_'.$level]++;

				} else { //If it is not an array...
					$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $get_attributes) {
						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}
							
						if($attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
					}
					$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
				}
			}

		} elseif($type == 'close') { //End of tag '</tag>'
			$current = &$parent[$level-1];
		}
	}

	return($xml_array);
}
	

$data  = file_get_contents("php://input");
//{"page":{"session_id":"6285382761ba450b98d1c74b803d1e96","mobile_number":"919892471157","page_id":"0","data":"anurag","gate":"353871423333"},"page_attr":{"version":"2.0"}}
$fh = fopen('/tmp/ussd.txt','a+');
$data = xml2array($data);
$session_id = $data['page']['session_id'];

fwrite($fh,"\n".json_encode($data));
$failures = array('1' => 'USER CANCELLED','2' => 'TIMEOUT', '3' => 'CHARACTER LIMIT EXCEEDED', '4' => 'INSUFFICIENT BALANCE', '5' => 'NETWORK FAILURE', '99' => 'GENERIC FAILURE');
if(isset($data['page']['status'])){
	$error = $failures[$data['page']['status']];
	$res = mysql_query("SELECT mobile_no,app_id FROM ussd_mob_ven_app WHERE session_id='$session_id' order by id desc");
	$res_arr = mysql_fetch_array($res);
	$mobile = $res_arr['mobile_no'];
	$appid = $res_arr['app_id'];
	
	if($appid == '4' || $appid == '5' || $appid == '7'){
		$res = mysql_query("INSERT INTO ussd (mobile,tid,msg,timestamp) VALUES ('$mobile','$session_id','$error','".date('Y-m-d H:i:s')."') ");
	}else{
		$res = mysql_query("INSERT INTO ussd_calls (mobile,session_id,response,created) VALUES ('$mobile','$session_id','$error','".date('Y-m-d H:i:s')."') ");
	}
	if($data['page']['status'] == '4' || $data['page']['status'] == '5' || $data['page']['status'] == '3' || $data['page']['status'] == '99'){
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: admin@smstadka.com\r\n";
		$headers .= "Reply-To: admin@smstadka.com\r\n";

		mail('tadka@mindsarray.com','Messaging65 Not working',"Not working on mobile: $mobile, Error: $error",$headers);
	}
	return;
}

$mobile = $data['page']['mobile_number'];
$page_id = $data['page']['page_id'];
$data = $data['page']['data'];

if(!is_null($mobile) && $mobile != ''){
	if(strlen($mobile) > 10)
	$mobile = substr($mobile, -10);
	$res = mysql_query("select id,extra,app_id,extra from ussd_mob_ven_app where mobile_no = '".$mobile."' and vendor_id = '1' and session_id is null order by id desc limit 1");
	$row = mysql_fetch_array($res);		
	mysql_query("update ussd_mob_ven_app set session_id = '".$session_id."' where id=".$row['id']);	
	$appid = $row['app_id'];
	$extra = $row['extra'];
}else{
	$res = mysql_query("select mobile_no,app_id,extra from ussd_mob_ven_app where session_id = '".$session_id."' and vendor_id = '1' order by id desc limit 1");
	$row = mysql_fetch_array($res);
	$appid = $row['app_id'];
	$mobile = $row['mobile_no'];
	$extra = $row['extra'];
}
if($appid == '4' || $appid == '5' || $appid == '7'){
	$url = "Location:http://www.smstadka.com/apis/ussd.php?msisdn=$mobile&msg=".urlencode($data)."&tid=$session_id&route=2";
	if($page_id == 0){
		$url .= "&first=1";
	}
	fwrite($fh,$url);
	header($url); exit;
}else{
	
	header("Location:http://www.smstadka.com/ussds/m365Rec/".$session_id."/".$page_id."/".$data."/".$appid."/".$mobile."/".$extra); exit;
}

?>