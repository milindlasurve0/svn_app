<?php

include("../../config/bootstrap.php");
require 'vmnManager.php';

if(in_array('Rawmessage',array_keys($_REQUEST)) && in_array('VN',array_keys($_REQUEST))){
    $_REQUEST['msg'] = $_REQUEST['Rawmessage'];
    $_REQUEST['vnumber'] = $_REQUEST['VN'];
    $_REQUEST['sender'] = $_REQUEST['Send'];
    $stime = urldecode($_REQUEST['Time']);
    //$_REQUEST['stime'] = date("d/m/Y H:i:s", strtotime($stime));
    $_REQUEST['stime'] = $stime;
}

if(isset($_REQUEST['data'])){
        /*  This is handle the request with bulk data */
	$msg = trim(urldecode($_REQUEST['data']));
	$smsdata = json_decode($msg,true);
}
else {
	$smsdata[]= $_REQUEST;
}


$vmnObj1 = new vmnManager();
$logger = $vmnObj1->logger('sms_new', 'sms_in');
//===pushing data to redis queue

foreach ($smsdata as $dt) {
    $dt['unique_request_id'] = time().uniqid();
    /*  push_vmn_queue will convert data to json internally */
    //$dt['message']=  str_replace("\n", "", $dt['message']);
    if(isset($dt['message'])){
    	$msg = explode("\n", $dt['message']);
    	$dt['message']=  $msg[0];
    }
    $vmnObj1->push_vmn_queue($dt);
    $push_data = json_encode($dt);
    $logger->info("unq_Id : " . $dt['unique_request_id'] . " : pushed data in redis " . $push_data);
}
?>