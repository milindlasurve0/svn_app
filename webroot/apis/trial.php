<?php
include "php_serial.class.php";
include "pdu.php";

class communicator {
	function sendMessage($device,$mobile,$msg){
		$device = $device - 1;
		
		$serial = new phpSerial();
		$serial->deviceSet("/dev/ttyUSB".$device); //usb2 is 9833032643
		$serial->confBaudRate(9600);
		$serial->confParity("none");
		$serial->confCharacterLength(8);
		$serial->confStopBits(1);
		$serial->confFlowControl("none");
		
		// Then we need to open it
		$serial->deviceOpen();
		$pduGenerator = new Pdu();
		$pduMessage = $pduGenerator->generatePDU($mobile, $msg);
		
		$serial->sendMessage("AT+CMGF=0\r");
		sleep(1);
		$err = array('256','261');
		foreach($pduMessage as $pdu){
			$serial->sendMessage("AT+CMGS=".$pdu['cmgslen']."\r");
			sleep(1);
			$message = $pdu['pdu'];
			$serial->sendMessage("{$message}".chr(26));
			sleep(5);
			$out = $serial->readPort();
			if(strpos($out, "CME")){
				foreach ($err as $token) {
				    if (stristr($out, $token) !== FALSE) {
				        echo "String contains: $token<br/>";
				    }
				}
			}
			//echo $out;
			sleep(2);
		}
		$serial->deviceClose();
	}
	
	function sendCommand($device,$command){
		$device = $device - 1;
		
		$serial = new phpSerial();
		$serial->deviceSet("/dev/ttyUSB".$device); //usb2 is 9833032643
		$serial->confBaudRate(9600);
		$serial->confParity("none");
		$serial->confCharacterLength(8);
		$serial->confStopBits(1);
		$serial->confFlowControl("none");
		
		// Then we need to open it
		$serial->deviceOpen();
		
		$serial->sendMessage($command."\r");
		
		sleep(2);
		$out = $serial->readPort();
		sleep(2);
		$serial->deviceClose();
		return $out;
	}
	
	function get_string_between($string, $start, $end){
		$string = " ".$string;
		$ini = strpos($string,$start);
		if ($ini == 0) return "";
		$ini += strlen($start);
		$len = strpos($string,$end,$ini) - $ini;
		return substr($string,$ini,$len);
	}
	
	function getSCID($device){
		$out = sendCommand(8,"AT^SCID");
		return trim(get_string_between($out,"^SCID:","OK"));
	}
}
//sendMessage($_REQUEST['device'],$_REQUEST['mobile'],$_REQUEST['message']);
//echo getSCID(3);
//sendMessage(3,'9819032643','hello');
$fh = fopen("/home/ashish/workspace/smstadka/app/webroot/abc.txt","a+");
fwrite($fh,"\n". $argv[1] . " " . $argv[2] . " " . $argv[3]);
sleep(5);
fwrite($fh,"\n". $argv[1] . " " . $argv[2] . " " . $argv[3]);
//$comm = new communicator();
//$comm->sendMessage($argv[1],$argv[2],$argv[3]);
?>