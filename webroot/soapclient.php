<?php
// include the SOAP classes
require_once('soaplib/nusoap.php');
$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
$useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
$namespace  = "http://tempuri.org/";
$client = new nusoap_client("http://demo.osscard.com/osscardMobileservice.asmx", false,
						$proxyhost, $proxyport, $proxyusername, $proxypassword);
$err = $client->getError();
if ($err) {
	echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
	exit();
}
$client->setUseCurl($useCURL);
// This is an archaic parameter list
$params = "<strRequestXML>&lt;VerifyMobile&gt;&lt;MobileNo&gt;9099959156&lt;/MobileNo&gt;&lt;/VerifyMobile&gt;</strRequestXML>";
$headers = "<MobileServiceAccessAuthentication xmlns='http://tempuri.org/'><MSAccessID>1</MSAccessID><AuthKey>MSA_VF_Access</AuthKey></MobileServiceAccessAuthentication>";
/*$headers = "<SOAP-ENV:Header>
<MobileServiceAccessAuthentication xmlns='http://www.osscard.com'>
<MSAccessID>1</MSAccessID>
<AuthKey>Test Auth Key</AuthKey>
</MobileServiceAccessAuthentication>
</SOAP-ENV:Header>";*/
$result = $client->call('VerifyMobileNo', $params,'http://tempuri.org/',$namespace.'VerifyMobileNo',$headers);
var_dump($result);
if ($client->fault) {
	echo '<h2>Fault (Expect - The request contains an invalid SOAP body)</h2><pre>'; print_r($result); echo '</pre>';
} else {
	$err = $client->getError();
	if ($err) {
		echo '<h2>Error</h2><pre>' . $err . '</pre>';
	} else {
		echo '<h2>Result</h2><pre>'; print_r($result); echo '</pre>';
	}
}
echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
?>
