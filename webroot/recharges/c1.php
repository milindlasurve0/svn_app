<?php

require("socket_handler.php");
require("sysconfig.php");
//$addr = "127.0.0.1";
$addr = "cdev.pay1.in";
$port = "9191";
$vendor_id = VENDOR_ID;
$socket_type = "TCP";


function start_socket_client($addr, $port, $vendor_id, $socket_type) {
    $sockObj = new socket_handler();
    echo "client started\n";
    $data['ipaddr'] = $addr;
    $wait_time = 10;
    $data['port'] = $port;
    $data['content'] = "reg~v~" . $vendor_id;
    $p_sock = $sockObj->send_msg_via_socket($data, $socket_type);
    echo "client registed to server with name " . $vendor_id . "\n";
    $incomplete_cont = "";
    if ($p_sock != FALSE) {
        try {
            $sock = $p_sock['sock'];
            $ndata = array('sock' => $sock);
            while (1) {
                $req_arr = array();
                $req_all_arr = array();
                
                $sockRes = $sockObj->receive_msg_via_socket($ndata, $socket_type);
                if ($sockRes != FALSE && strlen(trim($sockRes['message'])) > 1) {                    
                    $sockRes['message'] = $incomplete_cont.$sockRes;
                    $req_all_arr = explode("|<>|", $sockRes['message']);
                    foreach ($req_all_arr as $req_content) {
                        if (substr(trim($req_content), -4) === "|()|") {
                            $req_arr = substr(trim($req_arr), 0,strlen($req_arr)-4);
                            $req_arr = explode("|", $req_content);
                            print_r($req_arr);
                            if (isset($req_arr[0]) && substr(trim($req_arr[0]), 0, 2) == "ID") {
                                $processfile = DOCUMENT_ROOT . "recharges/socketProcess.php ";
                                $params = $req_arr['0'] . "|" . $req_arr['1'];
                                $cmd = "nohup php " . $processfile . " '" . $params . "'  > /dev/null 2> /dev/null & echo $!";
                                echo $cmd . "\n";
                                shell_exec($cmd);
                            }
                        } else {
                            $incomplete_cont = $req_content;
                        }
                    }
                } else {
                    throw new Exception("connection lost from server");
                }
            }
        } catch (Exception $ex) {
            echo "Exception occured. \n";
            print_r($ex);
            echo "client with reconnect after $wait_time sec \n";
            sleep($wait_time);
            start_socket_client($addr, $port, $vendor_id, $socket_type);
        }
    } else {
        echo "No connection Found with Server. \n";
        echo "client with reconnect after $wait_time sec \n";
        sleep($wait_time);
        start_socket_client($addr, $port, $vendor_id, $socket_type);
    }
}

start_socket_client($addr, $port, $vendor_id, $socket_type);
?>