<?php
include "sysconfig.php";
/*STOP_FLAG:1->when stop from panel or 100 sms limit is used
			2->inprocess+failure
			3-> -ve diff
			5-> sim stop by server if vendors target sale is completed
*/
/*RechargeMethod
 * 0.Default
 * 1.Application
 * 2.SMS
 * 3.USSD
 * 4.WEB*/
//devices tables state flag
define('STATE_READY',0);
define('STATE_BUSY',1);
define('STATE_STOP',2);
//sim detection issue
define('TRIALS',10);
//Sent flag
define('SENDING',2);
define('SENT',1);
define('TOBESEND',0);
//check check

define('FAILURE_CAP_LIMIT',300);
//status
define('TRANS_PROCESS',0);
define('TRANS_SUCCESS',1);
define('TRANS_FAIL',2);

define('SMS_LIMIT',95);
define('SMS_LIMIT_TXN',80);
define('SMS_LIMIT_CHECKBAL',15);
		
define('RIGHT_OPRS','2,4,5,7,8,15');

define('FAIL_1','Invalid Amount');
define('FAIL_2','Try after some time');
define('FAIL_3','Wrong Mobile Number/Subscriber ID');
define('FAIL_4','No balance in sim');
define('FAIL_5','Reason not known');
define('FAIL_6','Special Recharge');
defined('AIRTEL_USSD_RECHARGE') || define('AIRTEL_USSD_RECHARGE',false);
defined('VIDEOCON') || define('VIDEOCON',false);
defined('PORT') || define('PORT','ttyUSB');
defined('INTERNET') || define('INTERNET',false);
defined('ELECTRICITY') || define('ELECTRICITY',false);
//defined('SECONDARY_CON') || define('SECONDARY_CON','HUAWEI-CONNECTION');
//defined('PRIMARY_CON') || define('PRIMARY_CON','pay1');
define('TRANSACTION_RECHARGE',0);
define('TRANSACTION_REVERSAL',1);

define('SPECIAL_CHARS','@,$,_,^,{,},\,[,~,],|');

//define('SERVER_BACKUP','http://107.22.174.199/');
define('SMS_MAIL_SERVER','http://smstadka.com/');
define('SERVER_URL','https://processor.pay1.in/');
define('SERVER_URL_BACKUP','http://54.235.195.140/');
define('Time_Diff',1);
defined('MEMCACHE') || define('MEMCACHE',1);

$conn = mysql_pconnect(DB_HOST, DB_USER, DB_PASS); //or die ('Error connecting to mysql');
if(!$conn){
	$mailbody = "Mysql Connection error:".mysql_error();
	//getMails("Connection Not created",$mailbody,"lalit@mindsarray.com,chetan@mindsarray.com",'mail');
}
mysql_select_db(DB_DB);
mysql_query("SET time_zone = '+5:30'");
date_default_timezone_set('Asia/Calcutta');

if(MEMCACHE){
$memcacheObj = new Memcache();
$memcacheObj->pconnect('localhost',11211);
}

function curl_post_async($url, $params=null)
{
	foreach ($params as $key => &$val) {
		if (is_array($val)) $val = implode(',', $val);
		$post_params[] = $key.'='.urlencode($val);
	}
	$post_string = implode('&', $post_params);

	$parts=parse_url($url);

	$fp = pfsockopen($parts['host'],
	isset($parts['port'])?$parts['port']:80,
	$errno, $errstr, 60);

	$out = "POST ".$parts['path']." HTTP/1.1\r\n";
	$out.= "Host: ".$parts['host']."\r\n";
	$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	$out.= "Content-Length: ".strlen($post_string)."\r\n";
	$out.= "Connection: Close\r\n\r\n";
	if (isset($post_string)) $out.= $post_string;

	fwrite($fp, $out);
	fclose($fp);
}

function redis_post($method, $params=null){
    if(empty($params)){
        return;
    }
    else if(is_array($params)){
            $data_to_post = $params;
    }
    $data_to_post['method_name'] = $method;
    require_once "redis_manager.php";
	$rm = new redisManager();
    $uuid = VENDOR_ID."_".MACHINE_ID."_".(microtime(true) * 10000);
    logData("redis_post.txt","uuid: $uuid :: data ::".json_encode($data_to_post));
    $rm->send_modem_updates($uuid, $data_to_post);
}

function curl_post_async1($url, $params=null,$type='POST')
{
        if(empty($params)){
                $post_string = "";
        }
        else if(is_array($params)){
                foreach ($params as $key => &$val) {
                        if (is_array($val)) $val = implode(',', $val);
                        $post_params[] = $key.'='.urlencode($val);
                        //else $post_params[] = $val;
                }
                $post_string = implode('&', $post_params);
        }
        else {
                $post_string = $params;
        }   
        
        $ch = curl_init($url);                                         
        if($type == 'POST'){                                           
                curl_setopt($ch, CURLOPT_POST,1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);    
        }
        else { 
                curl_setopt($ch, CURLOPT_POST,0);                      
        }   
                        
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);                
        curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
        curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);                          
        try{    
            $out = curl_exec($ch);
            curl_close ($ch);
        }catch(Exception $ex){
            print_r($ex);
        }
}


function curl_post($url, $server=SERVER_URL,$params=null,$type='POST')
{
	if(empty($params)){
		$post_string = "";
	}
	else if(is_array($params)){
		foreach ($params as $key => &$val) {
			if (is_array($val)) $val = implode(',', $val);
			else $post_params[] = $val;
		}
		$post_string = implode('&', $post_params);
	}
	else {
		$post_string = $params;
	}

	$ch = curl_init($server.$url);
	if($type == 'POST'){
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	}
	else {
		curl_setopt($ch, CURLOPT_POST,0);
	}

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
	curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
	curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	$out = trim(curl_exec($ch));

	$info = curl_getinfo($ch);

	if(!curl_errno($ch)){
		curl_close($ch);
		if($info['http_code'] == '404'){
			return array('output'=>$out,'status'=>'failure');
		}
		else return array('output'=>$out,'status'=>'success');
	}
	else {
		$errno = curl_errno($ch);
		curl_close($ch);
		if(in_array($errno,array(6,7)) || $info['connect_time'] > 10){
			logData("curl_error.txt","Error $errno:: url ::".$server.$url);
			if($server == SERVER_URL){//failover call wrt dns issues
				curl_post($url,SERVER_URL_BACKUP,$params,$type);
			}
			return array('output'=>$out,'status'=>'failure');//connection timeout
		}
		return array('output'=>$out,'status'=>'success','description'=>$errno);
	}
}

function getMails($subject,$body,$emails,$type=null){
	$url = SMS_MAIL_SERVER . 'redis/insertInQsms';
	//$url = SERVER_BACKUP . 'users/sendMsgMails';
	$data = array();
	$data['mail_subject'] = $subject;
	$data['mail_body'] = $body;
	if(!empty($emails) && is_array($emails)){
		$data['emails'] = implode(",",$emails);
	}
	else $data['emails'] = $emails;
	if($type == 'mail')
	curl_post_async1($url,$data);
}

function getSender($opr_id){
	$ops = array(
		'1'=>array('Aircel','Etopup','DD-Etopup'),
		'2'=>array('Airtel','ERecharge'),
		'3'=>array('BSNL','CTOPUP'),
		'4'=>array('IDEA','3891','3893','148'),
		'5'=>array('LOOP','50568','50569','Rechag'),
		'6'=>array('MTS'),
		'7'=>array('RELIANCE','52811'),
		'8'=>array('RELIANCE','52811'),
		'9'=>array('DOCOMO','52200','9032012345'),
		'10'=>array('DOCOMO','52200','9032012345'),
		'11'=>array('uninor','ETOPUP'),
		'12'=>array('V-Recharge','RECHRG'),
		'13'=>array('Virgin'),
		'14'=>array('Virgin'),
		'15'=>array('Vodafone','140','190','191'),
		'16'=>array('DTH Payment'),
		'17'=>array('53730','53731'),
		'18'=>array('DishTV','EASPAY','Dishps','ITZCSH'),
	    '19'=>array('SUN'),
		'20'=>array('Alerts','MyTsky'),
		'21'=>array('VIDDTH','VM-VIDDTH','VIDEOCON'),
		'27'=>array('DOCOMO','52200','9032012345'),
		'28'=>array('V-Recharge'),
		'29'=>array('uninor'),
		'30'=>array('TrumpRch','MTNL','MM-TRMPRC','919757007667','919869654958'),
		'31'=>array('TrumpRch','MTNL','MM-TRMPRC','919757007667','919869654958'),
		'34'=>array('BSNL','CTOPUP'),
		'181'=>array('DishTV','EASPAY','Dishps')
	);

	return $ops[$opr_id];
}

function getOperator($opr_id){
	$ops = array(
		'1'=>'Aircel',
		'2'=>'Airtel',
		'3'=>'BSNL',
		'4'=>'Idea',
		'5'=>'Loop',
		'6'=>'MTS',
		'7'=>'Reliance CDMA',
		'8'=>'Reliance GSM',
		'9'=>'Tata Docomo',
		'10'=>'Tata Indicom',
		'11'=>'Uninor',
		'12'=>'Videocon',
		'13'=>'Virgin CDMA',
		'14'=>'Virgin GSM',
		'15'=>'Vodafone',
		'16'=>'Airtel DTH',
		'17'=>'Big TV DTH',
		'18'=>'Dish TV',
		'181'=>'Dish TV HV',
	    '19'=>'SUN TV DTH',
		'20'=>'Tata Sky',
		'21'=>'Videocon d2h',
		'27'=>'Tata SV',
		'28'=>'Videocon SV',
		'29'=>'Uninor SV',
		'30'=>'MTNL',
		'31'=>'MTNL SV',
		'34'=>'BSNL SV'
		);

		return $ops[$opr_id];
}

function numberOfChars($message){
	$special_2chars = array("^","{","}","\\","[","~","]","|");
	$length = strlen( $message );
	$i = 1;
	$num = 0;
	while ($i <= $length) {
		// Convert this character to a 7 bits value and insert it into the array
		$char = substr( $message ,$i-1,1);
		if(!in_array($char,$special_2chars)){
			$num++;
		}
		else {
			$num = $num + 2;
		}
		$i++;
	}
	return $num;
}

function logData($file,$data,$index = null){
	$fh = fopen('/mnt/logs/'.$file,"a+");
	if(!empty($index))$data .= $index."::";
	fwrite($fh,date('Y-m-d H:i:s')."::".$data."\n");
	fclose($fh);
}

function matchTemplate1($sms, $template,$varStart="@__",$varEnd="__@"){
	$sms = trim($sms);
	$template = trim($template);
	$template = str_replace($varStart,"|~|",$template);
	$template = str_replace($varEnd,"|~|",$template);

	$t=explode("|~|",$template);

	$vars = array();
	$ret = true;
	$i = 0;
	$start = 0;
	$log = "";

	$out['sms'] = $sms;
	for($i=0;$i<=count($t);$i=$i+2){
		if($t[$i] == null){
			if($i != 0){
				$vars[$start] = $sms;
				$vars[$t[$i-1]] = $sms;
			}
		}
		else {
			$log .= "Checking ".$t[$i];
			$index = stripos($sms,$t[$i]);
			$log .= ": $index\n";
			if($index === false){
				$ret = false;
				break;
			}
			else {
				$var = substr($sms,0,$index);
				if($i != 0){
					$vars[$start] = trim($var);
					$vars[$t[$i-1]] = trim($var);
					$start++;
				}
				$sms = substr($sms,$index+strlen($t[$i]));
			}
		}
	}

	if(count($t) == 1 && $out['sms'] != $template){
		$ret = false;
	}

	if($ret){
		$out['status'] = 'success';
		$out['vars'] = $vars;
	}
	else {
		$out['status'] = 'failure';
		$out['vars'] = $vars;
	}
	$out['logs'] = $log;
	return $out;
}

function getParentOperator($opr_id){
	$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3','181'=>'18');
	$opr = (isset($arr_map[$opr_id])) ? $arr_map[$opr_id] : $opr_id;
	return $opr;
}


function matchTemplate($sms, $template){
	$explode = explode('@__123__@',$template);
	
	$vars = array();
	$ret = true;
	$i = 0;
	$start = 0;
	$log = "";

	$out['sms'] = $sms;
	foreach($explode as $exp){
		$log .= "Checking $exp";
		if(!empty($exp)){
			$index = strpos($sms,$exp);
			$log .= ": $index\n";
			if($index === false){
				$ret = false;
				break;
			}
			else {
				if($i != 0){
					$var = substr($sms,0,$index);
					$vars[] = trim($var);
				}
				$sms = substr($sms,$index+strlen($exp));
			}
		}
		else if($i != 0){
			$vars[] = $sms;
		}
		$i++;
	}
	
	if($ret){
		$out['status'] = 'success';
		$out['vars'] = $vars;
		$out['sms'] = $sms;
	}
	else {
		$out['status'] = 'failure';
		$out['vars'] = $vars;
		$out['sms'] = $sms;
	}
	$out['logs'] = $log;
	return $out;
}

function getOperatorData($opr_id){
	$operatorValue = array(  "9" => array("opr_id" => array("9","27"), "opr_name" => array("Tata Docomo")),
									"27" => array("opr_id" => array("9","27"), "opr_name" => array("Tata SV")),
									"11" => array("opr_id" => array("11", "29"), "opr_name" => array("Uninor")),
									"29" => array("opr_id" => array("11", "29"), "opr_name" => array("Uninor SV")),
									"30" => array("opr_id" => array("30", "31"), "opr_name" => array("MTNL")),
									"31" => array("opr_id" => array("30", "31"), "opr_name" => array("MTNL SV")),
									"3" => array("opr_id" => array("3", "34"), "opr_name" => array("BSNL")),
									"34" => array("opr_id" => array("3", "34"), "opr_name" => array("BSNL SV")),
									"12" => array("opr_id" => array("12", "28"), "opr_name" => array("Videocon")),
									"28" => array("opr_id" => array("12", "28"), "opr_name" => array("Videocon SV")),
									"7" => array("opr_id" => array("8", "7"), "opr_name" => array("Reliance CDMA")),
									"8" => array("opr_id" => array("8", "7"), "opr_name" => array("Reliance GSM"))
	);
	
	$ret = (isset($operatorValue[$opr_id])) ? $operatorValue[$opr_id]['opr_id'] : array($opr_id);
		
	return $ret;	
}

function checkMysqlConn(){
	if(!mysql_ping()){
		logData("mysql_new_conn.txt","NEw connection created::".mysql_error());
		$conn = mysql_pconnect(DB_HOST, DB_USER, DB_PASS) or die ('Error connecting to mysql');
		mysql_select_db(DB_DB);
	}
}


?>
