<?php
//link1: http://www.dreamfabric.com/sms/
//http://www.dreamfabric.com/sms/hello.html
//http://www.dreamfabric.com/sms/default_alphabet.html
//http://mobileforensics.files.wordpress.com/2007/06/understanding_sms.pdf
//link2: http://youngindia99.com/SMS_PDU_MODE.aspx
//link3: http://mobiletidings.com/2009/02/18/combining-sms-messages/
//link4: http://hiteshagja.wordpress.com/2010/04/04/send-long-sms/
//link5: http://hiteshagja.wordpress.com/2010/06/27/encode-text-message-to-pdu/
//http://stackoverflow.com/questions/5713963/gsm7-bit-padding-for-sms-concatenation-in-c-sharp
include "config.php";

class Pdu {

    // Last error code
    private $lastErrorCode;
    

    // Set last error code
    private function setErrorCode( $errorcode ) {
        $this->lastErrorCode = $errorcode;
    }

    // Get last error code
    public function getErrorCode() {
        return $this->lastErrorCode;
    }

    // Get last error string
    public function getErrorString() {
        if($this->getErrorCode()==1) {
            return 'Error #1: Telephone/gsm number not valid.';
        }
        if($this->getErrorCode()==2) {
            return 'Error #2: Message too short.';
        }
        if($this->getErrorCode()==3) {
            return 'Error #3: Message too long.';
        }
        return 'No error.';
    }

    // Generate PDU string
    public function generatePDU($receiverNumber,$message) {

        $smsType = '01';
        $udh = false;
        if( numberOfChars($message) > 160 ) {
        	$smsType = '41';
        	$udh = true;
        }

        // Length of servicecenter number (normally automatically fixed by phone)
        $serviceCenterNumberLength = '00';

        // Number length
        // Type of phone adress: (81=Unknown=10dec, 91=InternationalFormat, 92=National?)
        if(strlen($receiverNumber) < 10){
        	$numberLength = $this->strToHexLen($receiverNumber);
        	$numberType = '81';//91
        }
        else {
        	$numberLength = '0C';
        	$numberType = '91';
        }

        // Get the PDU version of the number
        $number = $this->getNumberAsPDU( $receiverNumber );

        // TP-PID (Protocol Identifier)
        $protocolId = '00';

        // TP-DCS (Data coding scheme)
        $dataCodingScheme = '00';
        
        // TP-Validity-Period (timestamp), AA=4days expiry, disabled for SonyEricsson support.
        $validityPeriod = '';
        // $validityPeriod = 'AA'; // Add this if the PDU command fails
        $messages = array();
        if(numberOfChars($message) <= 160){
        	$messages[] = $message;
        }
        else {
        	$messages = str_split($message, 153);
        }
        $data = array();
        
        $i = 0;
        foreach($messages as $msg){
        	// TP Message Reference: (placeholder)
	        $messageRef = '0'.$i;
	        // Data length of message (in hex format)
	        $dataLength = '';
	        $udhHeader = '';
	        if(!$udh){
	        	$dataLength = $this->strToHexLen($msg);
	        	// Convert message, string > 7bits > 8bits > hex
	        	$hexMessage = $this->bit7tohex( $this->strto7bit( $msg ) );
	        }
	        else {
	        	/*$len = numberOfChars($msg);
	        	$payload = strtoupper(dechex($len+7));
	        	if(strlen($payload) < 2 ) {
		            $payload = '0'.$payload;
		        }*/
		        
	        	$fixed = "05000300";
	        	$max = "0" .count($messages);
	        	$msgnum = "0".($i+1);
	        	
	        	//$udhHeader = $payload.$fixed.$max.$msgnum;
	        	// Convert message, string > 7bits > 8bits > hex
	        	$hexMessage = $this->bit7tohex($this->strto7bit( $msg ),1);
	        	$len = floor(strlen($fixed.$max.$msgnum.$hexMessage)*4/7);
	        	$payload = strtoupper(dechex($len));
		        if(strlen($payload) < 2 ) {
		            $payload = '0'.$payload;
		        }
	        	$udhHeader = $payload.$fixed.$max.$msgnum;
	        }
			
	        // Create the complete PDU string
	        $pdu = $serviceCenterNumberLength . $smsType . $messageRef . $numberLength .
	                $numberType . $number . $protocolId . $dataCodingScheme .
	                $validityPeriod . $udhHeader . $dataLength . $hexMessage;
	
	        /*
	         * Generate the length of var $pdu (pdu/2 minus 1) as pdu format requests
	         * The -1 is because we don't count the first two characters '00', needed for this command: 'cmgs=24'
	         */
	        $cmgslen = strlen($pdu)/2-1;
	
	        // Build data array to return with required information
	        $data[$i]['pdu'] = $pdu;
	        $data[$i]['cmgslen'] = $cmgslen;
	        $i++;
        }
        // Return the data array with PDU information
        return $data;
    }


    // Generate PDU formatted cellphone number
   function getNumberAsPDU($number) {

   		if(strlen($number)%2 == 1)$number .= 'F';
        // Length of number divided by 2 handle two characters each time
        $length = strlen( $number )/2;
        // Set counter to 1 for strlen
        $i = 1;
        $pduNumber = '';

        // Loop to handle every 2 characters of the phone number. 06 12 34 56 78
        while ($i <= $length) {
            // Get 2 characters of the complete string depending on the number of the current loop.
            // Then reverse these 2 characters and put them in var $pduNumber (06 = 60)
            $pduNumber .= strrev( substr( $number,$i*2-2,2) );
            // Counter + 1
            $i++;
        }

        // Return the generated number
        return $pduNumber;
    }


    /* Function to convert ascii character to 8 bits
     * Much more efficient than holding a complete ASCII table
     * Thanks to Mattijs F.
     */
    function asc2bin($input, $length=8) {

        $bin_out = '';
        // Loop through every character in the string
        for($charCount=0; $charCount < strlen($input); $charCount++) {
            $charAscii = ord($input{$charCount}); // ascii value of character
            $charBinary = decbin($charAscii); // decimal to binary
            $charBinary = str_pad($charBinary, $length, '0', STR_PAD_LEFT);
            $bin_out .= $charBinary;
        }

        // Return complete generated string
        return $bin_out;
    }


    // String to 7 bits array
    function strto7bit($message) {
        //$message = trim($message);
        $length = strlen( $message );
        $i = 1;
        $bitArray = array();
        $special_mapping = array('@' => '0000000','$' => '0000010','_' => '0010001','^' => '00110110010100','{' => '00110110101000','}' => '00110110101001','\\'=>'00110110101111','['=>'00110110111100','~'=>'00110110111101',']'=>'00110110111110','|' => '00110111000000');
		
        // Loop through every character in the string
        while ($i <= $length) {
            // Convert this character to a 7 bits value and insert it into the array
            $char = substr( $message ,$i-1,1);
            if(!in_array($char,explode(",",SPECIAL_CHARS))){
            	$bitArray[] = $this->asc2bin($char,7);
            }
            else {
            	$new = $special_mapping[$char];
            	$bitArray[] = substr($new,0,7);
            	if(strlen($new) > 7){
            		$bitArray[] = substr($new,7,7);
            	}
            }
            $i++;
        }

        // Return array containing 7 bits values
        return $bitArray;
    }


    // Convert 8 bits binary string to hex values (like F2)
    function bit8tohex($bin, $padding=false, $uppercase=true) {
        $hex = '';
        // Last item for counter (for-loop)
        $last = strlen($bin)-1;
        // Loop for every item
        for($i=0; $i<=$last; $i++) {
            $hex += $bin[$last-$i] * pow(2,$i);
        }

        // Convert from decimal to hexadecimal
        $hex = dechex($hex);
        // Add a 0 (zero) if there is only 1 value returned, like 'F'
        if($padding && strlen($hex) < 2 ) {
            $hex = '0'.$hex;
        }

        // If we want the output returned as UPPERCASE do this
        if($uppercase) {
            $hex = strtoupper($hex);
        }

        // Return the hexadecimal value
        return $hex;
    }


    // Convert 7 bits binary to hex, 7 bits > 8 bits > hex
    function bit7tohex($bits,$padding=null) {

        $i = 0;
        $hexOutput = '';
        $running = true;
		
        if($padding != null){
        	$bits[0] = str_pad($bits[0], $padding + 7, '0', STR_PAD_RIGHT);
        }
        // For every 7 bits character array item
        while($running) {

            if(count($bits)==$i+1) {
                $running = false;
            }

            $value = $bits[$i];

            if($value=='') {
                $i++;
                continue;
            }

            // Convert the 7 bits value to the 8 bits value
            // Merge a part of the next array element and a part of the current one

            // Default: new value is current value
            $new = $value;

            if(key_exists(($i+1), $bits)) {
                // There is a next array item so make it 8 bits
                $neededChar = 8 - strlen($value);
                // Get the char;s from the next array item
                $charFromNext = '';
                if($neededChar != 0){
                	$charFromNext = substr($bits[$i+1], -$neededChar);
                	// Remove used bit's from next array item
                	$bits[$i+1] = substr($bits[$i+1], 0, strlen($bits[$i+1])-$neededChar );
                }
                // New value is characters from next value and current value
                $new = $charFromNext.$value;
            }

            if($new!='') {
                // Always make 8 bits
                $new = str_pad($new, 8, '0', STR_PAD_LEFT);
                // The 8 bits to hex conversion
                $hexOutput .= $this->bit8tohex($new, true);
            }

            $i++;
        }

        // Return the 7bits->8bits->hexadecimal generated value
        return $hexOutput;
    }

    // String to length in Hex, String > StringLength > Hex
    function strToHexLen($message) {

        // Length of the string (message)
        $length = strlen( $message );
        // Hex value of this string length
        $hex = dechex($length);

        // Length of the hex value
        $hexLength = strlen($hex);
        // If the hex strng length is lower dan 2
        if($hexLength < 2) {
            // Add a 0 (zero) before it
            $hex = '0'.$hex;
        }

        // Return the hex value in UPPERCASE characters
        return strtoupper($hex);
    }
    
    /*public function splitMessage($message, $n){
    	
    }*/
}
/*$pduGenerator = new Pdu();
$msg1 = "he said he liked our services,but from few days wen he is sending msgs from bulakkad,he said smstadka likh kar jaraha hai mera no. nai jaraha,he said dats y stopped using our services";
$msg2 = "he said he liked our services,but from few days wen he is sending msgs from bulakkad,he said smstadka likh kar jaraha hai mera no. nai jaraha,he said dats y sto"; 
$msg3 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
$msg4 = "Nargis Fakhri can't stop gushing about Katrina Kaif after seeing Chinki Chameli video. Nargis Fakhri can't stop gushing about Katrina Kaif after seeing Chinki Chameli video. Nargis Fakhri can't stop gushing about Katrina Kaif after seeing Chinki Chameli video. Nargis Fakhri can't stop gushing about Katrina Kaif after seeing Chinki Chameli video. Nargis Fakhri can't stop gushing about Katrina Kaif after seeing Chinki Chameli video. Nargis Fakhr";
$msg5 = "hello world";
$msg6 = "*Movie Reviews*
#Players (2/5 Rediff; 2/5 DNA; 3/5 NDTV; 3.5/5 TOI)
Its a messed up remake of The Italian Job as it tries to act too smart. The action s^{asadjsjaj";
//echo $pduGenerator->bit7tohex($bit7,1);
$msg = "a
v";
$msg = str_replace("\r\n","\n",$msg);
$pduGenerator = new Pdu();
//$msg =  $pduGenerator->strto7bit('@');
$pduMessage = $pduGenerator->generatePDU('9819032643',$msg);
print_r($pduMessage);*/
?>