#!/bin/bash

VENDOR_ID=`cat /home/pay1/workspace/recharges/sysconfig.php | grep -i "VENDOR_ID"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep VENDOR_ID | sed 's/VENDOR_ID//g' | tr -d " "` 

CUR_PROC_CNT=$(($(ps -ef | grep rsync | wc -l) - 1 ))

if [ $CUR_PROC_CNT -lt 1 ]
then 
    /usr/bin/rsync -avze  "ssh -o StrictHostKeyChecking=no" /var/lib/mysql_backup/ mysqlpay1server.ddns.net:/var/lib/mysql_$VENDOR_ID;
fi
