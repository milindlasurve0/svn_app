#!/bin/bash

CUR_PROC_CNT=$(($(ps -ef | grep healthcheck.sh | wc -l) - 3 ))
if [ $CUR_PROC_CNT -gt 0 ];
then
    exit
fi

cd /home/pay1/workspace/recharges/

LOAD_THRESHOLD=8
LOAD_MAIL_THRESHOLD=20
RESTART_THRESHOLD=100
RAM_THRESHOLD=350
DISK_THRESHOLD=8000
VENDOR_NAME=`cat sysconfig.php | grep -i "VENDOR_NAME"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep VENDOR_NAME | sed 's/VENDOR_NAME//g'`
EMAILTO='chetan@mindsarray.com,milind@mindsarray.com,wasima@mindsarray.com'
MAIL_URL='http://www.smstadka.com/users/sendMsgMails?'
MAIL_SUBJECT='(SOS) LIVE SERVER HEALTH ('$VENDOR_NAME')'
MAIL_SUBJECT_NEW='(SOS) System restarted due to heavy load ('$VENDOR_NAME')'

system_load=`uptime | head -1 | awk -F'average' '{ print $2 }' | sed 's/,/:/g' | awk -F': ' '{ print $2 }'`

free_ram_available=`free -m | head -2 | tail -1 | awk -F' ' '{ print $4 }'`

disk_space_mb=`df -mT / | tail -1 | awk -F' ' '{ print $5 }'`

currentTime=`date`

echo "******************************************"
echo "current DateTime : "$currentTime" \n" >> /mnt/logs/healthchk.log
echo "current system status \n" >> /mnt/logs/healthchk.log
echo "LOAD      : "$system_load" \n" >> /mnt/logs/healthchk.log
echo "RAM  (MB) : "$free_ram_available" \n" >> /mnt/logs/healthchk.log
echo "DISK (MB) : "$disk_space_mb" \n" >> /mnt/logs/healthchk.log

echo "******************************************"

if [ $( echo "$LOAD_THRESHOLD < $system_load" | bc ) -ne 0 ]
then
   echo "Load crossed  to threshhold " $system_load
   PORTS=`dmesg -t | tail -100 | grep "urb failed to clear flow control" | awk -F':' '{print $1}' | awk -F'ttyUSB' '{print $2}' | sort | uniq`

   PORT_CND=`echo $PORTS | wc -c`
   echo "Port list : "$PORT_CND" \n" >> /mnt/logs/healthchk.log
   echo "PORTS : "$PORTS" \n" >> /mnt/logs/healthchk.log
   if [ $PORT_CND -gt 1 ] 
   then
        for logf in $PORTS
          do 
            BUS_DEV=`/sbin/udevadm info --name=/dev/ttyUSB"$logf" --attribute-walk | sed -n 's/\s*ATTRS{\(\(devnum\)\|\(busnum\)\)}==\"\([^\"]\+\)\"/\1\ \4/p' | head -n 2 | awk '{$1 = sprintf("%s %03d", $1, $2); print $1;}' | awk '{print $2$4}'`
        done
    echo "BUS_DEV : "$BUS_DEV" \n" >> /mnt/logs/healthchk.log
        IDX=0
        for BUS in $BUS_DEV
          do
            if test $IDX -eq 0
            then
              BUS_NUM=$BUS
            else
              DEV_NUM=$BUS
            fi  
            IDX=$((IDX+1))
          done
          echo "BUS_NUM : "$BUS_NUM"  | DEV_NUM " $DEV_NUM" \n" >> /mnt/logs/healthchk.log
          echo "/usr/sbin/usb_modeswitch -R -b "$BUS_NUM" -g "$DEV_NUM" -v 0403 -p 6011 \n"  >> /mnt/logs/healthchk.log
        `/usr/sbin/usb_modeswitch -R -b $BUS_NUM -g $DEV_NUM -v 0403 -p 6011`	 
         
        MAIL_BODY="Load crossed  to threshhold $system_load <br>/usr/sbin/usb_modeswitch -R -b $BUS_NUM -g $DEV_NUM -v 0403 -p 6011"
        DMESG_LOG=`dmesg -T | tail -50 | sed 's/$/<br\/>/g' | tr -d "\n"`
        MAIL_BODY=$MAIL_BODY"<br/>DMSEG Log::<br/>"$DMESG_LOG
        MAIL_SUBJECT="RESETTING THE DEVICE('$VENDOR_NAME')"
        URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
        echo $URL
        wget -O/dev/null "$URL"
    else
    	if [ $( echo "$RESTART_THRESHOLD < $system_load" | bc ) -ne 0 ]
    	then
    		echo "Load crossed  to threshhold " $system_load
			MAIL_BODY="Load crossed  to threshhold $system_load"
			DMESG_LOG=`dmesg -T |  tail -50 | sed 's/$/<br\/>/g' | tr -d "\n"`
			MAIL_BODY=$MAIL_BODY"<br/>DMSEG Log::<br/>"$DMESG_LOG
			URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT_NEW"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
			echo $URL
			wget -O/dev/null "$URL"
			shutdown -r now
		else
			if [ $( echo "$LOAD_MAIL_THRESHOLD < $system_load" | bc ) -ne 0 ]
			then
   				echo "Load crossed  to threshhold " $system_load
   				MAIL_BODY="Load crossed  to threshhold $system_load"
   				DMESG_LOG=`dmesg -T |  tail -50 | sed 's/$/<br\/>/g' | tr -d "\n"`
   				MAIL_BODY=$MAIL_BODY"<br/>DMSEG Log::<br/>"$DMESG_LOG
   				URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
   				echo $URL
   				wget -O/dev/null "$URL"
			fi
		fi
	fi
fi

if [ $RAM_THRESHOLD -gt $free_ram_available ]
then
   echo "RAM usage crossed  to threshhold  $free_ram_available"
   `echo 3 > /proc/sys/vm/drop_caches`
   free_ram_available=`free -m | head -2 | tail -1 | awk -F' ' '{ print $4 }'`
   if [ $RAM_THRESHOLD -gt $free_ram_available ]
   then
     MAIL_BODY="RAM usage crossed  to threshhold  $free_ram_available"
   	 URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
   	 wget -O/dev/null "$URL"
   fi
fi

if [ $DISK_THRESHOLD -gt $disk_space_mb ]
then
   echo "disk usage exceeded  to threshhold  $disk_space_mb"
   find /root/recharges_*.sql -mtime +8 -exec rm {} \;
   echo 3 > /proc/sys/vm/drop_caches;
   > /var/log/mysql/mysql-slow.log
   MAIL_BODY="disk usage exceeded  to threshhold  $disk_space_mb"
   URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
   echo $URL
   wget -O/dev/null "$URL"
fi

IP=`hostname`
MAIL_BODY="$IP mysql service working properly now"
MAIL_SUBJECT="check mysql service start"
EMAILTO="milind@mindsarray.com,chetan@mindsarray.com,wasima@mindsarray.com"
MAIL_URL="http://www.smstadka.com/users/sendMsgMails?"
MAIL_SUBJECT="$IP mysql service check"
MAIL_BODY="mysql service was stop now  working fine"
URL2="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
echo $URL2
#`tail -5 | grep -Fxq "Check that you do not already have another mysqld process"`
#if [ `echo "$?"` -eq 1 ]
#then
#        `pkill -9 php`;
#        sleep 1;
#        `pkill -9 mysql`;
#        sleep 1;
#		val=`cat /var/log/mysql/error.log |tail -50`;
#		URL3="$MAIL_URL"mail_subject=Another mysqld running&mail_body=$val"&"emails="lalit@mindsarray.com,chetan@mindsarray.com"
#		wget -O/dev/null "$URL3"
#else
#        echo "Not found";
#fi

`netstat -antp | grep 3306 > /dev/null`
if [ `echo "$?"` -eq 0 ]
then
        echo "service running"
else
	
        echo "service is not running "
#        `/etc/init.d/mysql restart > /dev/null `
sleep 1
wget -O/dev/null "$URL2"
fi

#cat error.log |tail -50 | tr -d "\n" |grep -i "Check that you do not already have another mysqld process" | sed 's/error/\n/g' | sort | uniq -c | awk -F':' '{print $3}' | grep -i "Check that you do not already have another mysqld process" | sed 's/ //g'| uniq -c | awk '{print $2}' | wc -w
