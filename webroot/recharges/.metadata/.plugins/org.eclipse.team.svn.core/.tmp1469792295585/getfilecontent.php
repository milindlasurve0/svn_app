<?php

include "php_serial.class.php";
include "pdu.php";
include "decodePdu.php";
include 'onlineRecharge.php';
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

class communicator {
	
	function sendMessage($device,$mobile,$msg,$pduMessage=null,$unq=null,$devid=null){
		if(strlen($mobile) == 10) $mobile = "91" . $mobile;
		if(empty($unq))$unq = $device . "-" . time();
		
		$device = $device - 1;
		$ret = true;
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		$stat = array();
		
		if($devicePos){
			$serial->confBaudRate(115200);
			//$serial->confBaudRate(9600);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
			
			// Then we need to open it
			$serial->deviceOpen();
			logData("log_$devid.txt","In sendMessage, device opened",$unq);
			
			if($pduMessage == null){
				$pduGenerator = new Pdu();
				$pduMessage = $pduGenerator->generatePDU($mobile, $msg);
			}
			$serial->sendMessage("AT+CMGF=0\r",1,$unq);
			$check = $serial->readPort(0,$unq);
			$pos = strpos($check,"\r\nOK\r\n");
			
			if($pos === false)$ret = false;
			
			if($ret){
				$err_cme = array('256','261');
				$err_cms = array('500','50','0','38','47');
				$cms_num = -1;
				$cme_num = -1;
				$delay = false;
				$stop = false;
				$num = -1;
				
				$out = "";
				foreach($pduMessage as $pdu){
					$serial->sendMessage("AT+CMGS=".$pdu['cmgslen']."\r",1,$unq);
					$v = $serial->readPort(0,$unq);
					if(empty($v))
						$v = $serial->readPort(1,$unq);
					$message = $pdu['pdu'];
					$serial->sendMessage("{$message}".chr(26),1,$unq);
					
					$m = 0;
					while(1){
                        $read.= $serial->readPort(0,$unq);
                        if(strstr($read,"+CMGS: ") || $m> 6){
                            break;
                        }else{
                            sleep(1);
                            $m++;
                        }
                    }
					$out .= $read;
				}
				
				preg_match_all('/\+CMGS:/i', $out, $matches);
				if(count($matches['0']) != count($pduMessage)){
					$ret = false;
				}
				
				logData("log_$devid.txt","In sendMessage, output: $out",$unq);
				echo $out;
				
				if(!$ret){//if ret is false .. find out the reason
				
					preg_match('/CME ERROR: [0-9]+/', $out, $matches1);
					preg_match('/CMS ERROR: [0-9]+/', $out, $matches2);
					preg_match('/ERROR/', $out, $matches3);
						
					if(!empty($matches1)){
						$cme_num = substr($matches1[0],strlen('CME ERROR: '));
						$cme_num = trim($cme_num);
						if(in_array($cme_num,$err_cme)){
							$ret = false;
							$cme_num = -1;
						}
						else if(in_array($cme_num,array('258','100'))){//ok to go
							$cme_num = -1;
						}
						else if($cme_num == '10'){
							$stop = true;
						}
					}
					
					if(!empty($matches2)){
						$cms_num = substr($matches2[0],strlen('CMS ERROR: '));
						$cms_num = trim($cms_num);
						if(in_array($cms_num,$err_cms)){
							$ret = false;
							if($cms_num == '38' || $cms_num == '47') $delay = true;
							$cms_num = -1;
						}
						else if($cms_num == '21'){
							$stop = true;
						}
					}
					if(!empty($matches3)){
						$param = true;
					}else {
						$param = false;
					}
					if($cme_num != -1){
						$num = $cme_num;
					}
					else if($cms_num != -1){
						$num = $cms_num;
					}
				}
				$serial->deviceClose();
				
				$stat['status'] = $ret;
				$stat['out'] = $out;
				$stat['num'] = $num;
				$stat['delay'] = $delay;
				$stat['stop'] = $stop;
				$stat['param'] = $param;
				return $stat;
			}
			else return array('stop' => true,'restart' => false);
		}
		else return array('stop' => true,'restart' => true);
	}
	
	function checkSignalStrength($serial,$unq=null){
		
		$serial->sendMessage("AT+CSQ\r",1,$unq);
		$out = $serial->readPort(0,$unq);
			
		$signal = matchTemplate($out,'@__123__@+CSQ: @__123__@,@__123__@');
		if($signal['status'] == 'failure') {
			return 0;
		}
		else return $signal['vars'][1];
	}
	
	function receiveSMS($device,$scid=null,$devid=null){
		$unq = $device .'-'. time();
		$device = $device - 1;
		
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		
		if($devicePos){
			$serial->confBaudRate(115200);
			//$serial->confBaudRate(9600);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
			
			// Then we need to open it
			$serial->deviceOpen();
			logData("log_$devid.txt","In receive sms",$unq);
			
			$scid_curr = $this->getSCID($device+1,$serial,$unq,$devid);
			if($scid !== $scid_curr){
				logData("log_$devid.txt","Output receive sms: Sim not working",$unq);
				return array('reset' => true);
			}
			
			$signal = $this->checkSignalStrength($serial,$unq,$devid);
			
			if($signal == 0) {
				logData("log_$devid.txt","Output receive sms: Signal not working",$unq);
				return array('signal' => $signal);
			}
			
			$serial->sendMessage("AT+CMGF=1\r",1,$unq);
			$serial->sendMessage("AT+CPMS=\"MT\"\r",1,$unq);
			$out = $serial->readPort(0,$unq);
			
			$msgCount = matchTemplate($out,"@__123__@+CPMS:@__123__@\n@__123__@");
			$mem = 0;
			if($msgCount['status'] == 'success') {
				$mem = $msgCount['vars'][1];
				$mem1 = explode(",",$mem);
				$mem = $mem1[0];
			}
			
			$loop = 0;
			$smses = array();
				
			while($mem > 0 && $loop <= 1){
				$loop++;
				$serial->sendMessage("AT+CMGL=\"ALL\"\r",5,$unq);
				$out = $serial->readPort(0,$unq);
				$out = str_replace("\r\nOK\r\n","",$out);
				$array = explode("+CMGL: ",$out);
				$count = count($array);
				$i = 0;
				foreach($array as $sms){
					
					$sms1 = str_replace("\r\nOK\r\n","",$sms);
					$sms = matchTemplate($sms1,"@__123__@,\"@__123__@\"@__123__@");
					
					if($sms['status'] == 'success'){
						$params = $sms['vars'];
						$data = array();
						$data['id'] = $params[0];
						
						$sms = matchTemplate($sms1,"@__123__@,\"@__123__@\",\"@__123__@\",,@__123__@\r\n@__123__@");
						if($sms['status'] == 'success'){
							$params = $sms['vars'];
							$data = array();
							$data['id'] = $params[0];
							if($i == $count && $count > 2)break;
							if($params[1] == "REC UNREAD" || $params[1] == "REC READ"){
								$params[2] = trim($params[2]);
								$params[4] = trim($params[4]);
								
								preg_match('/^\d{3,10}$/',$params[2],$matches);
							 	if(empty($matches) && ctype_xdigit($params[2])){
    						    	$params[2] = $this->hextostr($params[2]);
    						    	$params[2] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $params[2]);
    						    	//$params[2] = str_replace('\u0000','', $params[2]);
    						    	$params[2] = trim($params[2]);
							 	}
    
								$data['sender'] = $params[2];
								$params[3] = str_replace('"','', trim($params[3]));
								
								$dates = explode(",",trim($params[3]));
								$later = $dates[1];
								$dates = explode("/",trim($dates[0]));
								$data['time'] = "20" . $dates[0] . "-" . $dates[1] . "-" . $dates[2] . " " . substr($later,0,8);
								$data['sms'] = $sms1;
								
								$exp = explode("\r\n\r\n+CIEV:",$params[4]);
								$params[4] = $exp[0];
								
								if(ctype_xdigit($params[4])){
    						    	$params[4] = $this->hextostr($params[4]);
    						    	$params[4] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $params[4]);
    						    	//$params[4] = str_replace('\u0000','', $params[4]);
    						    	$params[4] = trim($params[4]);
							 	}
								$data['msg'] = $params[4];
								$smses[] = $data;
							}
						}
						
						$serial->sendMessage("AT+CMGD=".$data['id']."\r",1,$unq);
					}
					$i++;
				}
				$mem = 0;
				$serial->sendMessage("AT+CPMS=\"MT\"\r",1,$unq);
				$out = $serial->readPort(0,$unq);
				$msgCount = matchTemplate($out,"@__123__@+CPMS: @__123__@\n@__123__@");
				if($msgCount['status'] == 'success') {
					$mem = $msgCount['vars'][1];
					$mem1 = explode(",",$mem);
					$mem = $mem1[0];
				}
			}
			
			$serial->deviceClose();
			$ret = array('signal' => $signal,'sms' => $smses);
			logData("log_$devid.txt","Final output of receivesms ". json_encode($ret),$unq);
			return $ret;
		}
		else {
			return array('reset' => true);
		}
	}
	
	function receiveSMSPDU($device,$scid=null,$devid=null){
		$unq = $device .'-'. time();
		$device = $device - 1;
	
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
	
		if($devicePos){
			$serial->confBaudRate(115200);
			//$serial->confBaudRate(9600);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
				
			// Then we need to open it
			$serial->deviceOpen();
			logData("log_$devid.txt","In receive sms_pdu",$unq);
				
			$scid_curr = $this->getSCID($device+1,$serial,$unq,$devid);
			if($scid !== $scid_curr){
				logData("log_$devid.txt","Output receive sms_pdu: Sim not working",$unq);
				return array('reset' => true);
			}
				
			$signal = $this->checkSignalStrength($serial,$unq,$devid);
				
			if($signal == 0) {
				logData("log_$devid.txt","Output receive sms_pdu: Signal not working",$unq);
				return array('signal' => $signal);
			}
				
			$serial->sendMessage("AT+CMGF=0\r",0.5,$unq);
			$serial->readPort(0,$unq);
			$serial->sendMessage("AT+CPMS=\"MT\"\r",1,$unq);
			$out = $serial->readPort(0,$unq);
			
			$msgCount = matchTemplate($out,"@__123__@+CPMS:@__123__@\n@__123__@");
			$mem = 0;
			if($msgCount['status'] == 'success') {
				$mem = $msgCount['vars'][1];
				$mem1 = explode(",",$mem);
				$mem = $mem1[0];
			}else if(strpos($out,'ERROR')!==false){
				$subject = "Vendor (" .VENDOR_NAME. ") and device id = $devid unable to read sms";
				$mailbody = $out;
				$emails = 'lalit@mindsarray.com';
				getMails($subject,$mailbody,$emails);
			}
				
			$loop = 0;
			$smses = array();
	
			while($mem > 0 && $loop <= 1){
				$loop++;
				$serial->sendMessage("AT+CMGL=4\r",0.5,$unq);
				$out = $serial->readPort(0,$unq);
				//echo $out;
				$k=1;
				while(strpos($out,"OK")===false && $k<=20){
					$out.=$serial->readPort(1,$unq);
					if(strpos($out,"ERROR")!==false)break;
					$k++;
				}
				$out = str_replace("\r\nOK\r\n","",$out);
				//print_r($out);
				$array = explode("+CMGL: ",$out);
				//print_r($array);
				$count = count($array);
				$i = 0;
				foreach($array as $sms){
					
					$sms1 = str_replace("\r\nOK\r\n","",$sms);
					$sms = matchTemplate($sms1,"@__123__@,@__123__@,@__123__@,@__123__@\r\n@__123__@");
					//print_r($sms);
					if($sms['status'] == 'success'){
						
						$params = $sms['vars'];
						$data = array();
						$data['id'] = $params[0];
						if($i == $count && $count > 2)break;
						$insert_flag = true;
						if($params[1] == 1 || $params[1] == 0){//RE
							$params[2] = trim($params[2]);
							$params[4] = trim($params[4]);
							$par=explode("\r\n",$params[4]);
							$params[4]=$par[0];
							/*if($params[1] == 1){
								$res = mysql_query("SELECT sms_log from read_sms where sms_log like '".addslashes($params[4])."' AND date = curdate()");
								if(!$res){
									logData("log_$devid.txt","could not run query ".mysql_error());
								}
								else{
									if(mysql_num_rows($res)>=1){
										logData("defaultsms.txt","Duplicate sms already added, not adding:\n".json_encode($params),$unq);
										$insert_flag = false;
									}
										
								}
							}*/
							
							if($insert_flag){
								$sms_pdu_decode = new sms_pdu_decode();
								$sms_pdu_decode->pdu = $params[4];
								
								try {
									$sms_pdu_decode->decode();
								} catch (Exception $e) {
									logData("log_$devid.txt","Exception while reading pdu ".$params[4].": Exception:". $e->getMessage(),$unq);
									$serial->sendMessage("AT+CMGD=".trim($data['id'])."\r",1,$unq);
									continue;
								}
								
								$time = $sms_pdu_decode->date . " " . $sms_pdu_decode->time;
								if(time() - strtotime($time) > 12*60*60){
									logData("defaultsms.txt","Very old sms, not adding:\n".$sms_pdu_decode->user_data,$unq);
									$serial->sendMessage("AT+CMGD=".trim($data['id'])."\r",1,$unq);
									continue;
								}
									
								if(empty($sms_pdu_decode->total_parts)){
									$data['sender'] = $sms_pdu_decode->sender_number;
									$data['time'] = $time;
									$data['sms'] = $params[4];
									$data['msg'] = $sms_pdu_decode->user_data;
									$data['read'] = $params[1];
									$data['uid'] = $sms_pdu_decode->timestamp.$sms_pdu_decode->sender_number.$params[0].$devid;
									$smses[] = $data;
								}
								else {
									logData('sms_uniq.txt',"SCID::$scid sms header=".$sms_pdu_decode->uniqe);
									if(strpos($sms_pdu_decode->sender_number,'VIDDTH') !== false){
										$uid = strtotime($sms_pdu_decode->date." ".date("H",strtotime($sms_pdu_decode->time)).":0")."".$sms_pdu_decode->uniqe;
										$uid = $uid.$devid;
									}else 
										$uid = strtotime($sms_pdu_decode->date . " " . date("H:i",strtotime($sms_pdu_decode->time)))."".$sms_pdu_decode->uniqe;
									$data['time'] = $sms_pdu_decode->date . " " . $sms_pdu_decode->time;
									$qry = "insert into message (`id`,`time`,`port`,`mobile`,`uid`,`total_part`,`part_number`,`message`,`pdu`) values('','".$data['time']."','".($device+1)."','".addslashes($sms_pdu_decode->sender_number)."','".addslashes($uid)."','".$sms_pdu_decode->total_parts."','".$sms_pdu_decode->parts_no."','".addslashes($sms_pdu_decode->user_data)."','".addslashes($params[4])."')";
								
									$result = mysql_query($qry);
								}	
							}
						}
						//sleep(1);
						try {
							$serial->sendMessage("AT+CMGD=".trim($data['id'])."\r",1,$unq);
						} catch (Exception $e) {
							logData("log_$devid.txt","Exception while Deleting Message Exception:". $e->getMessage(),$unq);
							continue;
						}
						
						$sss = $serial->readPort(0,$unq);
						//$pos = strpos($sss,"\r\nOK");
						$k=1;
						while(strpos($sss,"OK")===false && $k<=10){
							$sss.=$serial->readPort(1,$unq);
							if(strpos($sss,"ERROR")!==false)break;
							$k++;
						}
						logData("log_$devid.txt","Output of AT+CMGD :".$sss,$unq);
					}
					$i++;
				}
				$mem = 0;
				/*$serial->sendMessage("AT+CPMS=\"MT\"\r",1,$unq);
				$out = $serial->readPort(0,$unq);
				
				$msgCount = matchTemplate($out,"@__123__@+CPMS: @__123__@\n@__123__@");
				if($msgCount['status'] == 'success') {
					$mem = $msgCount['vars'][1];
					$mem1 = explode(",",$mem);
					$mem = $mem1[0];
				}*/
			}
			$serial->sendMessage("AT^SSMSS=1\r",0.3,$unq);
			$z = 0;
			$ssmss = '';
			$ssmss = $serial->readPort(0,$unq);
			while(strpos($ssmss,'OK')===false && $z<10){
				$ssmss.= $serial->readPort(0.3,$unq);
				if(strpos($ssmss,'ERROR')!==false)break;
				$z++;
			}
			
			$serial->deviceClose();
			$ret = array('signal' => $signal,'sms' => $smses);
			logData("log_$devid.txt","Final output of receivesms_pdu". json_encode($ret),$unq);
			//print_r($ret);
			return $ret;
		}
		else {
			return array('reset' => true);
		}
	}
	
	function sendCommand($device,$command,$wait,$serial=null,$unq=null,$devid=null){
		$flag = false;
		if($serial == null){
			$flag = true;
			$device = $device - 1;
			try{
				$serial = new phpSerial();
				$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
				if(!$devicePos) return;
				//$serial->confBaudRate(9600);
				$serial->confBaudRate(115200);
				$serial->confCharacterLength(8);
				$serial->confParity("none");
				$serial->confStopBits(1	);
				//$serial->confFlowControl("none");
				$serial->deviceOpen();
			}catch (Exception $e){
				logData("log_$devid.txt","Exception while setting php serial class: Exception:". $e->getMessage(),$unq);
				return;
			}
			// Then we need to open it
		}
		
		$serial->sendMessage($command."\r",$wait,$unq);
		
		$out = $serial->readPort(0,$unq);
		
		//echo $out;
		if(empty($out)){
			sleep(2);
			$out = $serial->readPort(0,$unq);
		}
		if($flag)$serial->deviceClose();
		return $out;
	}
	
	function sendMessage1($device,$mobile,$msg){
		$device = $device - 1;
		
		$serial = new phpSerial();
		$serial->deviceSet("/dev/".PORT.$device); //usb2 is 9833032643
		//$serial->confBaudRate(9600);
		$serial->confBaudRate(115200);
		$serial->confParity("none");
		$serial->confCharacterLength(8);
		$serial->confStopBits(1);
		$serial->confFlowControl("none");
		
		// Then we need to open it
		$serial->deviceOpen();
		$serial->sendMessage("AT+CMGF=1\r");
		sleep(1);
		$serial->sendMessage("AT+CMGS=$mobile\r");
		sleep(1);
		$serial->sendMessage("{$msg}".chr(26));
		sleep(2);
		$out = $serial->readPort();
		$serial->deviceClose();
		return $out;
	}
	//simStatus($id)
	function simStatus($device,$devid,$opr){
		logData("log_$devid.txt","in sim status");
		$unq = $device . "-" . time();
		if(empty($type))$type = 1;
	
		$command = array(
				 '1' => '*125#',				
				 '2' => "*123#",
				 '3' => '*123#',
				 '4' => "*121#",
				 '8' => '*333#',
				 '9' => '*111#',
				'11' => '*222*2#',
				'15' => "*141#",
				'30' => '*444#'
		);
	
		$device = $device - 1;
	
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos){
			$serial->confBaudRate(115200);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
	
			//Then we need to open it
			$serial->deviceOpen();
			$cmd = $command[$opr];
			$action = "AT+CUSD=1,\"$cmd\",15";
			$serial->sendMessage($action."\r",6,$unq);
			sleep(1);
			$out = trim($serial->readPort(0,$unq));
			$match = matchTemplate($out,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
			$try=1;
			while($match['status'] != 'success' && $try<=10){
				$out.= trim($serial->readPort(2,$unq));
				$match = matchTemplate($out,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
				logData("log_$devid.txt","template match::".json_encode($match));
				$try++;
			}
				
			logData("log_$devid.txt","ussd output:".json_encode($match),$unq);
				
			if($match['status'] == 'success'){
				$sms = $match['vars'][1];
				return array('status'=>'success','out'=>$sms);
			}else return array('status'=>'failure','out'=>$sms);
		}
	}
	
	function smsBalance($device,$opr,$type,$devid=null,$scid=null){
		
		$unq = $device . "-" . time();
		if(empty($type))$type = 1;
	
		$command = array(
				'2' => "*123*5#",
				'4' => "*212*5#",
				'15' => "*141*1#"
		);
	
		$device = $device - 1;
	
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos){
			$serial->confBaudRate(115200);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
	
			//Then we need to open it
			$serial->deviceOpen();
			$serial->sendMessage($command[$opr]."\r",6,$unq);
			sleep(1);
			$out = trim($serial->readPort(0,$unq));
			$match = matchTemplate($out,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
			$try=1;
			while($match['status'] != 'success' && $try<=10){
				$out.= trim($serial->readPort(2,$unq));
				$match = matchTemplate($out,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
				logData("log_$devid.txt","template match::".json_encode($match));
				$try++;
			}
			$serial->deviceClose();
			logData("log_$devid.txt","ussd output:".json_encode($match),$unq);
			
			if($match['status'] == 'success'){
				$sms = $match['vars'][1];
				return array('status'=>true,'out'=>$sms);
			}
		}
	}
	
	function checkBalance($device,$opr,$type,$pin,$devid=null,$scid=null,$retMobile,$rechargeMethod){
		$unq = $device . "-" . time();
		logData("log_$devid.txt","checking balance $opr:$type:$pin",$unq);
		if ($rechargeMethod==4){
			logData("log_$devid.txt","inside recharge ",$unq);
			$obj = new onlineRecharge();
			//curlRecharge($type,$device,$opr,$devid,$userName,$pass,$mobile=null,$amount=null,$param=null,$scid=null)
			$result = $obj->curlBalance('bal',$device,$opr,$devid,$retMobile,$pin,$mobile,$amount,$param,$scid);
			logData("log_$devid.txt","final output recharge::".json_encode($result),$unq);
			if($result['status']=='success')
				$result['status']=true;
			else 
				$result['status']=false;
			return $result;
		}
		
		if($type>2)$type = 1;
		if(empty($type))$type = 1;
		
		$sms_arr = array(
			 '9_1' => array('to' => '52200', 'msg' => "ER BAL $pin"),
			'10_1' => array('to' => '52200', 'msg' => "ER BAL $pin"),
			'15_1' => array('to' => '191', 'msg' => "BAL $pin"),
			'17_1' => array('to' => '53731', 'msg' => "BTVERBAL"),
			'18_1' => array('to' => '9223053474', 'msg' => "EBAL"),
			'18_2' => array('to' => '7200057575', 'msg' => "IEBAL"),
			'19_1' => array('to' => '9246201040', 'msg' => "3"),
			'20_1' => array('to' => '9227056633', 'msg' => "5.$pin.2"),
			'21_1' => array('to' => '9212012299', 'msg' => "BALANCE $pin"),
			'27_1' => array('to' => '52200', 'msg' => "ER BAL $pin")
		);
		
		logData("log_$devid.txt","checking balance $opr:$type:$pin",$unq);
		/*if($opr == 21 && VIDEOCON){
			//Balance
			$obj = new d2h();
			$result = $obj->curlMethod('Balance',$device,$opr,null,null,$pin,null,$scid,$devid,$retMobile);
			logData("log_$devid.txt","final output recharge::".json_encode($result),$unq);
			return $result;
		}*/	

		if(isset($sms_arr[$opr."_1"]) && !isset($sms_arr[$opr."_".$type])) $type = 1;
		
		$scid_curr = $this->getSCID($device,null,$unq,$devid);
			
		if($scid !== $scid_curr)return array('status'=>false,'reset'=>true);
		
		if(isset($sms_arr[$opr."_".$type])){
			$results = $this->sendMessage($device,$sms_arr[$opr."_".$type]['to'],trim($sms_arr[$opr."_".$type]['msg']),null,$unq,$devid);
			//logData("log_$devid.txt","In checking balance::output send message".json_encode($results));
			return $results;
		}
		else {
			$device = $device - 1;

			$serial = new phpSerial();
			$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
			if($devicePos){
				//$serial->confBaudRate(9600);
				$serial->confBaudRate(115200);
				$serial->confParity("none");
				$serial->confCharacterLength(8);
				$serial->confStopBits(1);
				$serial->confFlowControl("none");

				//Then we need to open it
				$serial->deviceOpen();
				if($opr==11||$opr==29){
					
					logData("log_$devid.txt","Balance calling ");
					$res=$this->uninorBalace1($device,$pin,$unq,$devid);
					logData("log_$devid.txt","Balance check output".json_encode($res),$unq);
					if($res['status']=='success'){
						return array('status'=>true);
					}else{
						return array('status'=>false);
					}
				}
				if(in_array($opr, array(5,8))){//loop, reliance via ussd
					if($opr == 5){
						$command = "AT+CUSD=1,\"*145*3#\",15";
					}
					else if($opr == 8){
						$command = "AT+CUSD=1,\"*301#\",15";
					}
					
					$serial->sendMessage($command."\r",4,$unq);
					sleep(1);
					$out = trim($serial->readPort(0,$unq));
					$match = matchTemplate($out,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
					$try=1;
					while($match['status'] != 'success' && $try<=10){
						$out.= trim($serial->readPort(2,$unq));
						$match = matchTemplate($out,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
						//logData("log_$devid.txt","template match::".json_encode($match));
						$try++;
					}
					logData("log_$devid.txt","ussd output:".json_encode($match),$unq);
					if($match['status'] == 'success'){
						$sms = $match['vars'][1];
						return array('status'=>true,'out'=>$sms);
					}
				}
				else {
					$ret = true;
					$commands = $this->getCommands($opr,"bal_$type",null,null,$pin);
													//($operator,$type,$mobile,$amount,$pin,$param=null)
					$index = 2;
					foreach($commands as $command){
						$tosend = $command['command'];
						$tosend = str_replace('$index',$index,$tosend);
						$expected_reply = $command['reply'];
						$wait = $command['wait'];
						echo "Sending Command: $tosend\n";
						$serial->sendMessage("$tosend\r",$wait,$unq);
						$exp = $serial->readPort(0,$unq);
							
						$trials = 0;
						$command_ret = false;
						if(!empty($expected_reply)){
							while(!$command_ret && $trials < 10){
								echo "Reply: $exp\n";
								//$pos = strpos($exp,$expected_reply);
								$expected_array = explode(',',$expected_reply);
								foreach ($expected_array as $expected_reply){
									$pos = strpos($exp,$expected_reply);
									if($pos !== false)break;
								}
								if($pos !== false){
									$command_ret = true;
									$aa = $this->findIndex($exp,$expected_reply,'APP');
									if($aa!==false)
										$index=$aa;
									break;
								}
								usleep($wait*1000000);
								$exp .= $serial->readPort(0,$unq);
									
								$trials++;
							}
						}
						else {
							$command_ret = true;
						}
						
						if($tosend == "AT^SSTR=19,0" ){
							$command_ret = true;
							$count=1;
							$res='';
							while(strpos($res,'^SSTN: 254')==false && $count<=5){
								$serial->sendMessage("AT^SSTR?\r",0.3,$unq);
								$res=$serial->readPort(0,$unq);
								$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
								if(!$match)
									preg_match('/[0-9],[0-9]/',$res,$matches);
								//print_r($matches);
								$x = explode(',',$matches[0]);
								if($x[0]==3){
									$serial->sendMessage("AT^SSTGI=$x[1]\r",0.3,$unq);
									$res=$serial->readPort(0,$unq);
									echo $res;
									if($x[1]==19 || $x[1]==33)
										sleep(1);
									$serial->sendMessage("AT^SSTR=$x[1],0\r",0.3,$unq);
									$res=$serial->readPort(0,$unq);
									echo $res;
								}else if($x[0]==2 || $x[0] == 1){
									break;
								}
								$count++;
							}
						
						}
						
						if(!$command_ret){
							if(strpos($expected_reply,'Balance')!==false && $opr==2){
								$kk = mysql_query("UPDATE devices set type = IF(type=2,1,2) where par_bal=$devid");
								if(!$kk){
									logData("log_$devid.txt","quey not executed ".mysql_error());
								}
							}
							$ret = false;
							$serial->sendMessage("AT^SMSO\r",6,$unq);
							$serial->readPort(0,$unq);
							break;
						}
							
					}
					
					return array('status'=>$ret,'out'=>$exp);
				}
				$serial->deviceClose();
			}
		}
		return array('status'=>true);
	}
	
	function recharge($device,$opr,$mobile,$amount,$type,$pin,$param=null,$scid=null,$devid=null,$trials=null,$retMobile=null,$rechargeMethod=null){
		$unq = $device . "-" .time();
		$device = $device - 1;
		if ($rechargeMethod==4){
			logData("log_$devid.txt","inside recharge ",$unq);
			$obj = new onlineRecharge();
						  //curlRecharge($type,$device,$opr,$devid,$userName,$pass,$mobile=null,$amount=null,$param=null,$scid=null)
			$result = $obj->curlRecharge($type,$device,$opr,$devid,$retMobile,$pin,$mobile,$amount,$param,$scid);
			logData("log_$devid.txt","final output recharge::".json_encode($result),$unq);
			return $result;
		}
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos){
			//$serial->confBaudRate(9600);
			$serial->confBaudRate(115200);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");
			
			//Then we need to open it
			$serial->deviceOpen();
			$out = $this->checkSignalStrength($serial,$unq);
			$ret = true;
			logData("log_$devid.txt","inside recharge with signal strength at $devid is $out",$unq);
			
			$scid_curr = $this->getSCID($device+1,$serial,$unq,$devid);
			//logData("log_$devid.txt","inside recharge expected scid=$scid and currunt scid=$scid_curr ");
			if($scid !== $scid_curr)$ret = false;
			
			if($ret){
				if(in_array($opr,array(7,8)) && (empty($rechargeMethod) || $rechargeMethod===0 || $rechargeMethod === 3)){
					$command = "AT+CUSD=1,\"*302*".$mobile."*".intval($amount)."*".$pin."#\",15";
					$ret_o = $this->sendCommand($device,$command,8,$serial,$unq,$devid);
					$serial->deviceClose();
					$ret_o = trim($ret_o);
					
					if(!empty($ret_o)){
						$out = matchTemplate($ret_o,'+CUSD: 2,"@__123__@",15');
						if($out['status'] == 'success'){
							$serial->deviceClose();
							return array('status' => 'success','get' => $out, 'out' => $ret_o);	
						}
						//preg_match('/CME ERROR: [0-9]+/', $out, $matches1);
						//preg_match('/CMS ERROR: [0-9]+/', $out, $matches2);//+CME ERROR: 258//phone busy
						else {
							//+CMTI: "ME",1;
							$pos1 = strpos($ret_o,'AT+CUSD=1,"*302*'.$mobile.'*'.intval($amount).'*'.$pin.'#",15');
							$pos2 = strpos($ret_o,'OK');	
							$pos3 = strpos($ret_o,'+CMTI:');	
							if($pos1 !== false || $pos2 !== false || $pos3 !== false){
								if(strpos($ret_o,'ss not executed')!==false ||strpos($ret_o,'ERROR')!==false)
									return array('status' => 'failure','out' => $ret_o);
								else 
									return array('status' => 'process','out' => $ret_o);
							}
							else {
								
								return array('status' => 'failure','out' => $ret_o);
							}
						}
					}
					else {
						return array('status' => 'process');
					}
				}
				
				if($opr == 15){
					$out = $this->vodaoffer($device,$mobile,intval($amount),$unq,$devid);
				}
				else if($opr == 2 && $param == 1){
					if($rechargeMethod == 3 && $trials < 1){
						$out = $this->airtelRecharge($device,$mobile,intval($amount),$pin,$unq,$devid);
						//$out = $this->USSDRecharge($device,$opr,$mobile,intval($amount),$pin,$unq,$devid);
						logData("log_$devid.txt","final result airtel ussd".json_encode($out));
						return $out;
					}/*else if(!AIRTEL_USSD_RECHARGE){
						$check = intval(date('i'))%5;
						if($check == 0)$out = $this->airteloffer($device,$mobile,intval($amount),$unq,$devid);
					}*/
				}
				
				
				if(in_array($opr,array(30,31)) && (MTNL_USSD_RECHARGE || $rechargeMethod === 3)){
					//$out = $this->USSDRecharge($device,$opr,$mobile,intval($amount),$pin,$unq,$devid);
					$out = $this->mtnlRecharge($serial,$device,$opr,$mobile,$amount,$pin,$unq=null,$devid=null);
					logData("log_$devid.txt","final result mtnl ussd".json_encode($out));
					return $out;
				}
				
				
				if(in_array($opr,array(1,9,10,15,17,18,181,19,20,21,27)) && (empty($rechargeMethod) || $rechargeMethod==0 || $rechargeMethod == 2)){
					if($opr == 1){
						$to = AIRCEL_NUM;//1028
						if(!empty($pin)){
							$msg = "RC $mobile ".intval($amount) . " $pin";	
						}
						else {
							$msg = "RC $mobile ".intval($amount);
						}
					}
					else if($opr == 15){
						$to = '190';
						if(!empty($pin)){
							$msg = "RC $pin $mobile ".intval($amount);	
						}
						else {
							$msg = "RC $mobile ".intval($amount);
						}
					}
					else if($opr == 17){
						$to = '53730';
						$msg = "BTVER ".intval($amount)." $param $pin $mobile";
					}
					else if($opr == 18 || $opr == 181){
						if(strlen($param) == 10 && strpos($param,'15') == 0)
								$param = "0".$param;
							
						if($type == 1){
							$to = '9223053474';
							$msg = "CSPAY1 ".intval($amount)." $param $mobile";
						}
						else {
							$to = '7200057575';
							$msg = "ICSPAY ".intval($amount)." $param";
						}
						//balance EBAL
					}
					else if($opr == 19){
                        $to = '9246201040';
                        $amount = intval($amount);
                        $msg = "2.$param.$amount.$mobile";
                    }
					else if($opr == 20){
						$to = '9220056633';
						$msg = "4.$pin.1.".intval($amount).".$param.2";
					}
					else if(in_array($opr,array('9','10'))){
						$to = '52200';
						$msg = "ER GFLX ".$pin." ".$mobile." ".intval($amount);
					}
					else if($opr == 27){
						$to = '52200';
						$msg = "ER GSPL ".$pin." ".$mobile." ".intval($amount);
					}
					else if($opr == 21){
						$to = '9212012299';
						$msg = "RCA $pin $param ".intval($amount);
						//balance BALANCE MYPIN
					}
					
					$out1 = $this->sendMessage($device+1,$to,$msg,null,$unq,$devid);
					if($out1['status']){
						if(in_array($opr,array('18','181','20','21'))){
							$res = mysql_query("select sms_limit_txn, sms_check_bal from checkbalance where device_id=$devid");
							$ree = mysql_fetch_assoc($res);
							$sms_limit = $ree['sms_limit_txn'];
							$sms_check_bal = $ree['sms_check_bal'];
							if($sms_limit>0){
								mysql_query("update checkbalance set sms_limit_txn=sms_limit_txn-1 WHERE device_id=$devid");
							}else{
								mysql_query("update checkbalance set sms_check_bal=sms_check_bal-1 WHERE device_id=$devid");
							}
						}
					}
					
					if($out1['status']){
						logData("log_$devid.txt","final output recharge:: success",$unq);
						return array('status' => 'success','out' => $out1['out']);	
					}
					else {
						if($out1['stop']){
							if($out1['num'] == '10'){
								$out1['out'] = "SIM Not inserted properly";
							}
							else if($out1['num'] == '21'){
								$out1['out'] = "No balance in SMS Sending SIM";
							}
							logData("log_$devid.txt","final output recharge:: failure, we need to stop sim here",$unq);
					
							return array('status' => 'failure','stop' => true,'out' => $out1['out'],'code' => 1);
						}
						else {
							if($opr==21 && $out1['param']){
								logData("log_$devid.txt","final output recharge:: failure",$unq);
								return array('status' => 'failure','out' => $out1['out'],'code' => 1);
							}else{
								logData("log_$devid.txt","final output recharge:: pending, output: ". $out1['out'],$unq);
								return array('status' => 'process','out' => $out1['out'],'code' => 1);
							}
						}
					}
					
					
				}
				
				if($opr == 5){
					return $this->loop($serial,$mobile,intval($amount),$pin,$unq,$devid);
				}
				
				/*if($opr==2){
					$this->roffer($device,$unq,$devid);
					
				}*/
				
				if($opr == 11 || $opr==29){
					$vals=$this->uninorRecharge($device,$mobile,intval($amount),$pin,$unq,$devid,$opr);
					logData("log_$devid.txt","final output recharge".json_encode($vals),$unq);
					return $vals;					
				}
				
				$out = "";
			
				$commands = $this->getCommands($opr,$type,$mobile,$amount,$pin,$param);
				if(empty($commands)){
					logData("log_$devid.txt","final output recharge:: commands not found",$unq);
					return array('status' => 'failure','stop' => true,'out' => 'Commands not found','code' => 1);	
				}
				$index = 2;
				foreach($commands as $command){
					$tosend = $command['command'];
					$tosend = str_replace('$index',$index,$tosend);
					$expected_reply = $command['reply'];
					$wait = $command['wait'];
					echo "Sending Command: $tosend\n";
					$serial->sendMessage("$tosend\r",$wait,$unq);
					$exp = $serial->readPort(0,$unq);
					
					$trials = 0;
					$command_ret = false;
					if(!empty($expected_reply)){
						while(!$command_ret && $trials < TRIALS){
							echo "Reply: $exp\n";
							$expected_array = explode(',',$expected_reply);
							foreach ($expected_array as $expected_reply){
								$pos = strpos($exp,$expected_reply);
								if($pos !== false)break;
							}
							if($pos !== false){
								$command_ret = true;
								$aa = $this->findIndex($exp,$expected_reply,'APP');
								if($aa!==false)
									$index=$aa;
								break;
							}
							//usleep(500000);
							$exp .= $serial->readPort(0,$unq);
							$trials++;
						}
					}
					else {
						$command_ret = true;
					}
					
					$out .= $tosend . "\n" . $exp;
					
					if($tosend == "AT^SSTR=19,0" ){
						$command_ret = true;
						$count=1;
						$res='';
						while(strpos($res,'^SSTN: 254')==false && $count <= 5){
							$serial->sendMessage("AT^SSTR?\r",0.3,$unq);
							$res=$serial->readPort(0,$unq);
							$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
							if(!$match)
								preg_match('/[0-9],[0-9]/',$res,$matches);
							//print_r($matches);
							$x = explode(',',$matches[0]);
							if($x[0]==3){
								$serial->sendMessage("AT^SSTGI=$x[1]\r",0.3,$unq);
								$res=$serial->readPort(0,$unq);
								echo $res;
								if($x[1]==19 || $x[1]==33)
									sleep(1);
								$serial->sendMessage("AT^SSTR=$x[1],0\r",0.3,$unq);
								$res=$serial->readPort(0,$unq);
								echo $res;
							}else if($x[0]==2 ||$x[0]==1){
								break;
							}
							$count++;
						}
						
					}
					
					if(!$command_ret){
						$ret = false;
						break;
					}
					
				}
		
			if(!$ret){
					$serial->sendMessage("AT^SMSO\r",5,$unq);
					echo $serial->readPort(0,$unq);
				}
				
				$serial->deviceClose();
				logData("log_$devid.txt","final output recharge:: $ret",$unq);
				
				if($ret){
					return array('status' => 'success','out' => $out);
				}
				else {
					return array('status' => 'failure','out' => $out,'code' => 1);
				}
			}
			else {
				logData("log_$devid.txt","final output recharge:: SIM NOT Working",$unq);
				$serial->deviceClose();
				return array('status' => 'failure','stop' => true,'out' => 'SIM Not Working','code' => 1);	
			}
			
		}
		else {
			logData("log_$devid.txt","final output recharge:: Serial port NOT Working",$unq);
			return array('status' => 'failure','reset' => true,'out' => 'Serial Port Not Working','code' => 1);
		}
	}
	
	function roffer($device,$unq,$devid){
		$my=mysql_query("SELECT mobile from devices where id=$devid");
		$my_num = mysql_num_rows($my);
		if($my_num>0){
			$mob = mysql_fetch_assoc($my);
			$val=$mob['mobile'];
			logData("log_$devid.txt","Airtel mobile number ".$val);
			if(mysql_query("INSERT INTO airtel (`mobile`) VALUES ('$val')")){
				$to="51619";
				$msg="ROFFER $mobile";
				$res=$this->sendMessage($device+1,$to,$msg,null,$unq,$devid);
				return true;
			}
			else{
				logData("log_$devid.txt","Already sent to this Airtel mobile number ".$val);
				return false;
			}
		}else{
			logData("log_$devid.txt","mobile number not found to end msg");
		}
	} 
	
	function loop($serial,$mobile,$amount,$pin,$unq=null,$devid=null){
		$command = "AT+CUSD=1,\"*145#\",15";
					
		$serial->sendMessage($command."\r",4,$unq);
		$out = trim($serial->readPort(0,$unq));
		$match = matchTemplate($out,'@__123__@CUSD: 1,"@__123__@",15@__123__@');
		//$succ = false;
		$log = $out;
		if($match['status'] == 'success'){
			$commands = array("1",$mobile,$amount,$pin);
			$i = 0;
			foreach($commands as $command){
				echo "Sending Command: $command\n";
			
				$serial->sendMessage($command.chr(26),3,$unq);
				$out1 = $serial->readPort(0,$unq);
				if(empty($out1)){
					sleep(2);
					$out1 = $serial->readPort(0,$unq);
				}
				$match = matchTemplate($out1,'@__123__@CUSD: 1,"@__123__@",15@__123__@');
				$log .= $out1;
				if($match['status'] != 'success'){
					break;
				}
				$i++;
			}
			
			if($i == count($commands)){
				$serial->sendMessage("1".chr(26),4,$unq);
				$out1 = $serial->readPort(0,$unq);
				$out1 .= "asdbd";
				$match = matchTemplate($out1,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
				$log .= $out1;
				if($match['status'] == 'success'){
					echo "Success: $log\n";
					sleep(5);
					return array('status' => 'success','out' => $log);
				}
				else {
					return array('status' => 'process','out' => $log);
				}
			}
		}
		
		return array('status' => 'failure','out' => $log);
	}
	
	function uninorBalace($device,$pin,$unq=null,$devid=null){
		//logData("log_$devid.txt","In balcheck function",$unq);
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			//$serial1->confBaudRate(9600);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
				
			//Then we need to open it
			$serial1->deviceOpen();
			$output='';
			$result = array();
			$command = "AT^SSTR?";
			$ready_recharge = false;
			$serial1->sendMessage($command."\r",0.5,$unq);
			$out = $serial1->readPort(0,$unq);
			echo $out;
			$output.=$out;
			$match=preg_match('/[0-9],[0-9][0-9]/',$out,$matches);
			if(!$match)
				preg_match('/[0-9],[0-9]/',$out,$matches);
			$v = explode(',',$matches[0]);
			//print_r($v);
			if($v[0]==0 ||$v[0]==1){
				echo "In state 0";
				$serial1->sendMessage("AT^SSTA=1,0\r",0.5,$unq);
				$out = $serial1->readPort(0,$unq);
				$output.=$out;
				if($this->idealState($serial1))
					$ready_recharge=true;
				else {
					$result['status']='failure';
					$result['msg']=$output;
				}
			}elseif ($v[0]==2||$v[0]==3 ||$v[0]==4){
				//echo "in ideal state";
				$ready_recharge =true;
			}
	
			if($ready_recharge){
				echo "In rechrge";
				$serial1->sendMessage("AT^SSTR?\r",0.5,$unq);
				$res=$serial1->readPort(0,$unq);
				//echo $res;
				$output.=$res;
				$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
				if(!$match)
					preg_match('/[0-9],[0-9]/',$res,$matches);
				$x = explode(',',$matches[0]);
				if($x[0]==2){
					$serial1->sendMessage("AT^SSTR=211,0,5\r",1,$unq);
					$res=$serial1->readPort(0,$unq);
					//echo $res;
					$output.=$res;
				}
				$count=0;
				$m=false;
				while(strpos($output,'^SSTN: 254')==false && $count<=30){
	
					$serial1->sendMessage("AT^SSTR?\r",0.5,$unq);
					$res=$serial1->readPort(0,$unq);
					echo $res;
					$output.=$res;
					$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
					if(!$match)
						preg_match('/[0-9],[0-9]/',$res,$matches);
					$x = explode(',',$matches[0]);
					if($x[0]==3){
						$serial1->sendMessage("AT^SSTGI=$x[1]\r",0.3,$unq);
						$res = $serial1->readPort(0,$unq);
						//echo $res;
						$output.=$res;
						if(strpos($res,"Balance Check")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,2\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							//echo $res;
							$output.=$res;
						}elseif(strpos($res,"My Account")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,1\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							//echo $res;
							$output.=$res;
						}elseif (strpos($res,"Help")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,5\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							//echo $res;
							$output.=$res;
						}elseif (strpos($res,"Enter MPIN")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,,$pin\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							//echo $res;
							$output.=$res;
							$m = true;
						}else{
							$serial1->sendMessage("AT^SSTR=$x[1],0\r",0.3,$unq);
							if($x[1]==19)
								sleep(12);
							elseif($x[1]==33)
							sleep(2);
							$res = $serial1->readPort(0,$unq);
							//echo $res;
							$output.=$res;
							//sleep(5);
							
							if($x[1]==19){
								//echo "Status Inprocess";
								$result['status']='success';
								logData("log_$devid.txt","In else and balance check status is ".$result['status'],$unq);
							}elseif(!$m){
								$result['status']='failure';
								$serial1->sendMessage("AT^SMSO\r",5,$unq);
								$serial1->readPort(0,$unq);
							}
						}
					}elseif ($x[0]==4){
						$serial1->sendMessage("AT^SMSO\r",5,$unq);
						$serial1->readPort(0,$unq);
						//$output.=$res;
						if(!$m){
							
							$result['status']='failure';
						}
					}
					$count++;
				}
			}
			$result['msg']=$output;
			$serial1->deviceClose();
			return $result;
		}else{
			$result['status']='failure';
			$result['msg']="serial port not working";
			return $result;
		}
	}
	
	function uninorBalace1($device,$pin,$unq=null,$devid=null){
		//logData("log_$devid.txt","In balcheck function",$unq);
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			//$serial1->confBaudRate(9600);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
	
			//Then we need to open it
			$serial1->deviceOpen();
			$output='';
			$result = array();
			$command = "AT^SSTR?";
			$ready_recharge = false;
			$serial1->sendMessage($command."\r",0.5,$unq);
			$out = $serial1->readPort(0,$unq);
			echo $out;
			$output.=$out;
			$match=preg_match('/[0-9],[0-9][0-9]/',$out,$matches);
			if(!$match)
				preg_match('/[0-9],[0-9]/',$out,$matches);
				$v = explode(',',$matches[0]);
				//print_r($v);
				if($v[0]==0 ||$v[0]==1){
					echo "In state 0";
					$serial1->sendMessage("AT^SSTA=1,0\r",0.5,$unq);
					$out = $serial1->readPort(0,$unq);
					$output.=$out;
					if($this->idealState($serial1))
						$ready_recharge=true;
						else {
							$result['status']='failure';
							$result['msg']=$output;
						}
				}elseif ($v[0]==2||$v[0]==3 ||$v[0]==4){
					//echo "in ideal state";
					$ready_recharge =true;
				}
	
				if($ready_recharge){
					$serial1->sendMessage("AT^SSTR?\r",0.5,$unq);
					$res=$serial1->readPort(0,$unq);
					$output.=$res;
					$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
					if(!$match)
						preg_match('/[0-9],[0-9]/',$res,$matches);
						$x = explode(',',$matches[0]);
						if($x[0]==2){
							$serial1->sendMessage("AT^SSTR=211,0,5\r",1,$unq);
							$res=$serial1->readPort(0,$unq);
							$arr = explode('^SSTGI: ',$res);
							$output.=$res;
						}
						$count=0;
						$m=false;
						$marked = array();
						while(strpos($output,'^SSTN: 254')==false && $count<=30){
							$serial1->sendMessage("AT^SSTR?\r",0.5,$unq);
							$res=$serial1->readPort(0,$unq);
							$output.=$res;
							$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
							if(!$match)
								preg_match('/[0-9],[0-9]/',$res,$matches);
								$x = explode(',',$matches[0]);
								if($x[0]==3){
									$serial1->sendMessage("AT^SSTGI=$x[1]\r",0.3,$unq);
									$res = $serial1->readPort(0,$unq);
									$expected = array('1'=>'Help',
											'2'=>'My Account',
											'3'=>'Balance Check',
											'4'=>'Enter MPIN'
									);
									foreach($expected as $key=>$val){
										if(in_array($key,$marked))continue;
										$index = $this->findIndex($res,$val,'APP');
										if($index!==false){
											$marked[] = $key;
											break;
										}
									}
									logData("log_$devid.txt","Index=$index");
									$output.=$res;
									if(strpos($res,"Balance Check")!==false && $index!==false){
										$serial1->sendMessage("AT^SSTR=$x[1],0,$index\r",0.3,$unq);
										$res = $serial1->readPort(0,$unq);
										$output.=$res;
									}elseif(strpos($res,"My Account")!==false && $index!==false){
										$serial1->sendMessage("AT^SSTR=$x[1],0,$index\r",0.3,$unq);
										$res = $serial1->readPort(0,$unq);
										$output.=$res;
									}elseif (strpos($res,"Help")!==false && $index!==false){
										$serial1->sendMessage("AT^SSTR=$x[1],0,$index\r",0.3,$unq);
										$res = $serial1->readPort(0,$unq);
										$output.=$res;
									}elseif (strpos($res,"Enter MPIN")!==false && $index!==false){
										$serial1->sendMessage("AT^SSTR=$x[1],0,,$pin\r",0.3,$unq);
										$res = $serial1->readPort(0,$unq);
										$output.=$res;
										$m = true;
									}else{
										$serial1->sendMessage("AT^SSTR=$x[1],0\r",0.3,$unq);
										if($x[1]==19)
											sleep(6);
											elseif($x[1]==33)
											sleep(2);
											$res = $serial1->readPort(0,$unq);
											$output.=$res;
											if($x[1]==19){
												$result['status']='success';
												logData("log_$devid.txt","In else and balance check status is ".$result['status'],$unq);
											}elseif(!$m){
												$result['status']='failure';
												$serial1->sendMessage("AT^SMSO\r",5,$unq);
												$serial1->readPort(0,$unq);
												break;
											}
									}
								}elseif ($x[0]==4){
									$serial1->sendMessage("AT^SMSO\r",5,$unq);
									$serial1->readPort(0,$unq);
									//$output.=$res;
									if(!$m){
											
										$result['status']='failure';
									}
									break;
								}
								$count++;
								if(strpos($output,'^SSTN: 254')!==false){
									$serial1->sendMessage("AT^SSTR?\r",0.5,$unq);
									$res=$serial1->readPort(0,$unq);
									$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
									if(!$match)
										preg_match('/[0-9],[0-9]/',$res,$matches);
									$x = explode(',',$matches[0]);
									if($x[0]==3){
										str_replace("^SSTN: 254","",$output);
									}
								}
						}
				}
				$result['msg']=$output;
				$serial1->deviceClose();
				return $result;
		}else{
			$result['status']='failure';
			$result['msg']="serial port not working";
			return $result;
		}
	}
	
	function findIndex($res,$find,$from){
		$arr = explode("\n",$res);
		$n=0;
		$flag = false;
		$x='';
		foreach($arr as $val){
			if(strpos($val,$find)){
				$x=$val;
				$flag = true;
				break;
			}
			$n++;
		}
		if(!empty($x) && $from === 'APP'){
			$match = matchTemplate($x,'@__123__@,@__123__@,"@__123__@",@__123__@');
			if($match['status']=='success'){
				return $match['vars']['1'];
			}else return false;
		}else if($from === 'USSD' && $flag){
			return $n+1;
		}else return false;
	}
	
	function uninorRecharge($device,$mobile,$amount,$pin,$unq,$devid,$opr){
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			//$serial1->confBaudRate(9600);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
				
			//Then we need to open it
			$serial1->deviceOpen();
			$output='';
			$result = array();
			$command = "AT^SSTR?";
			$ready_recharge = false;
			$serial1->sendMessage($command."\r",0.5,$unq);
			$out = $serial1->readPort(0,$unq);
			echo $out;
			$output.=$out;
			$match=preg_match('/[0-9],[0-9][0-9]/',$out,$matches);
			if(!$match)
				preg_match('/[0-9],[0-9]/',$out,$matches);
			$v = explode(',',$matches[0]);
			//print_r($v);
			if($v[0]==0 ||$v[0]==1){
				$serial1->sendMessage("AT^SSTA=1,0\r",0.5,$unq);
				$out = $serial1->readPort(0,$unq);
				$output.=$out;
				if($this->idealState($serial1)){
					$ready_recharge=true;
					$v[0]=2;
				}else {
					$result['status']='failure';
					$result['msg']=$output;
				}
			}elseif ($v[0]==2){
				$ready_recharge =true;
			}elseif ($v[0]==3 ||$v[0]==4){
				if($this->idealState($serial1)){
					$ready_recharge=true;
					$v[0]=2;
				}else {
					$result['status']='failure';
					$result['msg']=$output;
				}
			}
	
			if($ready_recharge){
				if($v[0]==2){
					$serial1->sendMessage("AT^SSTR=211,0,5\r",2,$unq);
					$res=$serial1->readPort(0,$unq);
					echo $res;
					$output.=$res;
				}else {
					
				}
				$count=0;
				$m=false;
				$l=false;
				while(strpos($output,'^SSTN: 254')===false && $count<=50){

					$serial1->sendMessage("AT^SSTR?\r",1,$unq);
					$res=$serial1->readPort(0,$unq);
					echo $res;
					$output.=$res;
					$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
					if($match!=1)
						preg_match('/[0-9],[0-9]/',$res,$matches);
					$x = explode(',',$matches[0]);
					logData("log_$devid.txt","Value of preg match and  ".$x[1],$unq);
					if($x[0]==3){
						$serial1->sendMessage("AT^SSTGI=$x[1]\r",0.3,$unq);
						$res = $serial1->readPort(0,$unq);
						echo $res;
						$output.=$res;
						if(strpos($res,"Recharge")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,1\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							$output.=$res;
						}elseif (strpos($res,"Enter Customer Mobile Number")!==false ||strpos($res,"Enter Partner Number")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,,$mobile\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							echo $res;
							$output.=$res;
						}elseif (strpos($res,"Enter Amount")!==false ||strpos($res,"Amount")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,,$amount\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							echo $res;
							$output.=$res;
						}elseif (strpos($res,"Enter Keyword")!==false){
							if($opr==29)
								$key='"STV"';
							else 
								$key='" "';
							$serial1->sendMessage("AT^SSTR=$x[1],0,,$key\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							echo $res;
							$output.=$res;
						}elseif (strpos($res,"Enter MPIN")!==false){
							$serial1->sendMessage("AT^SSTR=$x[1],0,,$pin\r",0.3,$unq);
							$res = $serial1->readPort(0,$unq);
							echo $res;
							$output.=$res;
							$m=true;
						}else{
							$serial1->sendMessage("AT^SSTR=$x[1],0\r",0.3,$unq);
							if($x[1]==19)sleep(6);
							else if($x[1]==33)sleep(2);
							
							$res = $serial1->readPort(0,$unq);
							echo $res;
							$output.=$res;
							//sleep(5);
							logData("log_$devid.txt","In else and value of proactive command is ".$x[1],$unq);
							if($m){
								$l=true;
								
								logData("log_$devid.txt","In else and recharge status is ".$result['status'],$unq);
							}elseif(!$m){
								//$result['status']='failure';
							}
						}
					}elseif ($x[0]==4){
						$serial1->sendMessage("AT^SSTR=254\r",0.3,$unq);
						$res = $serial1->readPort(0,$unq);
						echo $res;
						$output.=$res;
						if(!$m){
							$result['status']='failure';
						}
					}
					$count++;
				}
				if($m && $l)
					$result['status']='success';
				else {
					$x[1] = 3;
					$c=1;
					while($x[1]>2 && $c<=3){
						$serial1->sendMessage("AT^SMSO\r",5,$unq);
						$res = $serial1->readPort(0,$unq);
						$output.=$res;
						$serial1->sendMessage("AT^SSTR?\r",2,$unq);
						$res=$serial1->readPort(0,$unq);
						$output.=$res;
						$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
						if($match!=1)
							preg_match('/[0-9],[0-9]/',$res,$matches);
						$x = explode(',',$matches[0]);
						logData("log_$devid.txt","Value of preg match and  ".$x[1],$unq);
						$c++;
					}
                    $result['status']='failure';
				}
			}
			$result['msg']=$output;
			$serial1->deviceClose();
			return $result;
		}else{
			$result['status']='failure';
			$result['msg']="serial port not working";
			return $result;
		}
	}
	
	function idealState($serial1,$unq=null){
		
		$count=0;
		$res='';
		while((strpos($res,'^SSTN: 254')==false) && $count<=10){
			$serial1->sendMessage("AT^SSTR?\r",0.5,$unq);
			$res.=$serial1->readPort(0,$unq);
			$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
			if(!$match)
				preg_match('/[0-9],[0-9]/',$res,$matches);
			$x = explode(',',$matches[0]);
			if($x[0]==3){
				$serial1->sendMessage("AT^SSTGI=$x[1]\r",0.3,$unq);
				$res.=$serial1->readPort(0,$unq);
				echo $res;
				if($x[1]==19 || $x[1]==33){
					sleep(3);
				}
				$serial1->sendMessage("AT^SSTR=$x[1],0\r",0.3,$unq);
				$res.=$serial1->readPort(0,$unq);
				
			}elseif (trim($x[0])==2){
				break;
			}elseif (trim($x[0])==4){
				$serial1->sendMessage("AT^SMSO\r",0.3,$unq);
				$res.=$serial1->readPort(0,$unq);
				break;
			}
			$count++;
		}
		$serial1->sendMessage("AT^SSTR?\r",1,$unq);
		$res=$serial1->readPort(0,$unq);
		$match=preg_match('/[0-9],[0-9][0-9]/',$res,$matches);
		if(!$match)
			preg_match('/[0-9],[0-9]/',$res,$matches);
		$x = explode(',',$matches[0]);
		if(trim($x[0])==2){
			return true;
		}else
			return false;
	}
	
	function mtnl($serial,$mobile,$amount,$pin,$type,$unq=null){
		$command = "AT+CUSD=1,\"*555#$pin#\",15";
					
		$serial->sendMessage($command."\r",4,$unq);
		$out = trim($serial->readPort(0,$unq));
		$match = matchTemplate($out,'@__123__@CUSD: 1,"@__123__@",15@__123__@');
		//$succ = false;
		$log = $out;
		if($match['status'] == 'success'){
			$commands = array("1",$mobile,$amount,$type);
			$i = 0;
			foreach($commands as $command){
				echo "Sending Command: $command\n";
			
				$serial->sendMessage($command.chr(26),3,$unq);
				$out1 = $serial->readPort(0,$unq);
				if(empty($out1)){
					sleep(2);
					$out1 = $serial->readPort(0,$unq);
				}
				$match = matchTemplate($out1,'@__123__@CUSD: 1,"@__123__@",15@__123__@');
				$log .= $out1;
				if($match['status'] != 'success'){
					break;
				}
				$i++;
			}
			
			if($i == count($commands)){
				$serial->sendMessage("1".chr(26),4,$unq);
				$out1 = $serial->readPort(0,$unq);
				$out1 .= "asdbd";
				$match = matchTemplate($out1,'@__123__@CUSD: 2,"@__123__@",15@__123__@');
				$log .= $out1;
				if($match['status'] == 'success'){
					echo "Success: $log\n";
					sleep(5);
					return array('status' => 'success','out' => $log);
				}
				else {
					return array('status' => 'process','out' => $log);
				}
			}
		}
		
		return array('status' => 'failure','out' => $log);
	}
	
	function vodafonePostpaid($device,$mobile,$amount,$pin,$unq=null,$devid=null){
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			//$serial1->confBaudRate(9600);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");

			//Then we need to open it
			$serial1->deviceOpen();
			$command = "AT^SCID";
			$serial1->sendMessage($command."\r",5,$unq);

			$out = $serial1->readPort(0,$unq);
			echo $out."\n";
			$command = "AT+CUSD=1,*400#,15";

			$serial1->sendMessage($command."\r",10,$unq);
			$serial1->sendMessage(chr(26),8,$unq);

			$out = trim($serial1->readPort(0,$unq));

			if(empty($out)){
				sleep(4);
				$out = trim($serial1->readPort(0,$unq));
			}
			
			$result = array();

			if(strpos($out,'. Bill payment')!==false){
					
				$billPayment=explode('. Bill payment',$out);
				$index1=substr($billPayment[0],-1);
				
				$serial1->sendMessage($index1.chr(26),6,$unq);
				$out= $serial1->readPort(0,$unq);
				
				if(strpos($out,'.Vodafone postpaid')!==false){
					
					$billPayment=explode('.Vodafone postpaid',$out);
					$index1=substr($billPayment[0],-1);
					$serial1->sendMessage($index1.chr(26),6,$unq);
					$out= $serial1->readPort(0,$unq);
					
					if(strpos($out,'.For other nos')!==false){
						
						$billPayment=explode('.For other nos',$out);
						$index1=substr($billPayment[0],-1);
						$serial1->sendMessage($index1.chr(26),6,$unq);
						$out= $serial1->readPort(0,$unq);
						
						if(strpos($out,'.For a new number')!==false){
							
							$billPayment=explode('.For a new number',$out);
							$index1=substr($billPayment[0],-1);
							$serial1->sendMessage($index1.chr(26),6,$unq);
							$out= $serial1->readPort(0,$unq);
							
							if(strpos($out,'.Vodafone')!==false){
								
								$billPayment=explode('.Vodafone',$out);
								$index1=substr($billPayment[0],-1);
								$serial1->sendMessage($index1.chr(26),6,$unq);
								$out= $serial1->readPort(0,$unq);
								
								if(strpos($out,'Enter mobile number')!==false){

									$serial1->sendMessage($mobile.chr(26),6,$unq);
									$out= $serial1->readPort(0,$unq);
									
									if(strpos($out,'Enter amount')!==false){
										$serial1->sendMessage($amount.chr(26),6,$unq);
										$out= $serial1->readPort(0,$unq);
										
										if(strpos($out,'Enter PIN to confirm')!==false){
											$serial1->sendMessage($pin.chr(26),6,$unq);
											$out= $serial1->readPort(0,$unq);
											
											if(strpos($out,'Transaction declined')!==false){
												
												$result['status']='Failed';
												$result['cause']='Invalide mobile No.';
												
											}else{
												
												if(strpos($out,'like to save')){
													
													$billPayment=explode('.No',$out);
													$index1=substr($billPayment[0],-1);
													$serial1->sendMessage($index1.chr(26),5,$unq);
													$out= $serial1->readPort(0,$unq);
													$result['msgs']=$out;
													
												}
												
												$result['status']='success';
											}
											
											$result['msgs'] = $out;
												
										}else{
											$result['status']='failure';
											$result['cause'] = 'Enter Pin not found';
											$result['msgs'] = $out;
										}

									}else{
										$result['status']='failure';
										$result['cause'] = 'Enter Amount not found';
										$result['msgs'] = $out;
									}
										
								}else{
									$result['status']='failure';
									$result['cause'] = 'Enter Mobile not found';
									$result['msgs'] = $out;
								}

							}else{
								$result['status']='failure';
								$result['cause'] = 'Vodafone Index not found';
								$result['msgs'] = $out;
							}
								
						}else{
							$result['status']='failure';
							$result['cause'] = 'For a new number Index not found';
							$result['msgs'] = $out;
						}

					}else{
						$result['status']='failure';
						$result['cause'] = 'For other nos Index not found';
						$result['msgs'] = $out;
					}
						
						
				}else{
					$result['status']='failure';
					$result['cause'] = 'Vodafone Postpaide Index not found';
					$result['msgs'] = $out;
				}

			}else{
				$result['status']='failure';
				$result['cause'] = 'Bill payment Index not found';
				$result['msgs'] = $out;
			}

			$serial1->deviceClose();
			sleep(2);
			return $result;
		}else{
			$result['status'] = 'failure';
			$result['reset']=true;
			$result['msgs']="Serial port not working.";
			return $result;
		}
	}

	function vodaoffer($device,$mobile,$amount,$unq=null,$devid=null){
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			//$serial1->confBaudRate(9600);
			$serial1->confBaudRate(115200);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
			
			//Then we need to open it
			$serial1->deviceOpen();
		
			$command = "AT+CUSD=1,\"*121*".$mobile."#\",15";
					
			$serial1->sendMessage($command."\r",8,$unq);
			$out = trim($serial1->readPort(0,$unq));
			/*if(empty($out)){
				sleep(2);
				$out = trim($serial1->readPort(0,$unq));
			}*/
			
			$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
			$try=0;
			while($match['status'] != 'success' && $try<=10){
				$out.= trim($serial1->readPort(2,$unq));
				$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
				//logData("log_$devid.txt","template match::".json_encode($match));
				$try++;
			}
			$ret = "";
			if(!empty($out)){
				if($match['status'] == 'success'){
					$arr = explode("\n",trim($match['vars'][2]));
					$i = 1;
					foreach($arr as $ar){
						$ar = trim($ar);
						$pos = strpos($ar,"$amount");
						if($pos !== false){
							$serial1->sendMessage($i.chr(26),4,$unq);
							$ret = $serial1->readPort(0,$unq);
							break;
						}
						$i++;
					}
				}
			$serial1->sendMessage(chr(26),0,$unq);
			}
			$serial1->deviceClose();
			sleep(2);
			
			return $ret;
		}
	}
	
	function airteloffer($device,$mobile,$amount,$unq=null,$devid=null){
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			//$serial1->confBaudRate(9600);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
	
			//Then we need to open it
			$serial1->deviceOpen();
	
			$command = "AT+CUSD=1,\"*122*".$mobile."#\",15";
	
			$serial1->sendMessage($command."\r",2,$unq);
			$serial1->sendMessage(chr(26),8,$unq);
	
			$out = trim($serial1->readPort(0,$unq));
	
			if(empty($out)){
				sleep(4);
				$out = trim($serial1->readPort(0,$unq));
			}
	
			$ret = "";
			$match = matchTemplate($out,'@__123__@CUSD: @__123__@');
				
			$i = 0;
			while($match['status'] != 'success' && $i < 2){
				$out = $serial1->readPort(4,$unq);
				$match = matchTemplate($out,'@__123__@CUSD: @__123__@');
				if($match['status'] == 'success'){
					$pos = strpos(trim($match['vars'][1]),"$amount");
					if($pos !== false){
						$serial1->sendMessage("1".chr(26),4,$unq);
						sleep(7);
						$out .= $serial1->readPort(0,$unq);
					}
				}
				
				$i++;
			}
			
			$serial1->sendMessage(chr(26),2,$unq);
			
			if(empty($out)){
				sleep(6);
				$out = $serial1->readPort(0,$unq);
			}
			
			$serial1->deviceClose();
			return $out;
		}
	}
	
	/*function airtelRecharge($device,$mobile,$amount,$pin,$unq=null,$devid=null){
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			//$serial1->confBaudRate(9600);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
				
			//Then we need to open it
			$serial1->deviceOpen();

			$arr = array(
					'1' => array('command'=>"AT+CUSD=1,\"*122*".$mobile."#\",15\r",'wait'=>2,'expected'=>''),
					'2' => array('command'=>$amount.chr(26),'wait'=>2,'expected'=>"Enter MPIN to confirm Recharge of Rs.$amount to mobile number:$mobile"),
					'3' => array('command'=>$pin.chr(26),'wait'=>2,'expected'=>'')
					);
			
			foreach($arr as $key => $comm){
				$flag = false;
				$tosend = $comm['command'];
				goto more;
				more:
				$serial1->sendMessage($tosend,$comm['wait'],$unq);
				$out = trim($serial1->readPort(0,$unq));
				$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
				$try=0;
				
				while($match['status'] != 'success' && $try<=10){
					$out.= trim($serial1->readPort(2,$unq));
					$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
					//logData("log_$devid.txt","template match::".json_encode($match));
					$try++;
				}
				$out1 .= $out;
				$option = $this->findIndex($match['vars'][2],$amount."=",'USSD');
				if($option!==false){
					$tosend = $option.chr(26);
					goto more;
				}else {
					if(strpos($out,"* More")!==false){
						$tosend = "*".chr(26);
						goto more;
					}
				}
				if($key != '3' && $match['status'] != 'success'){
					$serial1->sendMessage(chr(27),1,$unq);
					$serial1->deviceClose();
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['msgs']="ussd response not found.";
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					return $result;
				}
				
				if(strpos($out,'Sorry! This number is not registered as an airtel retailer number')!==false){
					$result['code'] = "Sorry! This number is not registered as an airtel retailer number";
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['stop']=true;
					$result['email']=true;
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					return $result;
				}
				
				$expected = $match['vars']['2'];
				if(!empty($comm['expected'])){
					if(strpos($expected,$comm['expected']) === false){
						$serial1->sendMessage(chr(27),1,$unq);
						$serial1->deviceClose();
						$result = array();
						$result['status'] = 'failure';
						$result['reset']=false;
						$result['msgs']="wrong recharge";
						//logData("log_$devid.txt","final recharge response::".json_encode($result));
						return $result;
					}
				}
			}

			$result = array();
			$result['status']='success';
			$result['msgs']=$out1;
			$serial1->deviceClose();
			//logData("log_$devid.txt","final recharge response::".json_encode($result));
			return $result;
		}else{
			$result['status'] = 'failure';
			$result['reset']=true;
			$result['msgs']="Serial port not working.";
			//logData("log_$devid.txt","final recharge response::".json_encode($result));
			return $result;
		}
	}*/
	
	function airtelRecharge($device,$mobile,$amount,$pin,$unq=null,$devid=null){
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			//$serial1->confBaudRate(9600);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
	
			//Then we need to open it
			$serial1->deviceOpen();
			$arr = array(
					'1' => array('command'=>"AT+CUSD=1,\"*122*".$mobile."#\",15\r",'wait'=>2,'expected'=>''),
					'2' => array('command'=>$amount.chr(26),'wait'=>2,'expected'=>"Enter MPIN to confirm Recharge of Rs.$amount to mobile number:$mobile"),
					'3' => array('command'=>$pin.chr(26),'wait'=>2,'expected'=>'')
			);
			$option = 0;
			$found = false;
			$again = true;
			$i=1;
			while (!isset($result['status']) && $i<=count($arr)){
				if($found){
					$tosend = $option.chr(26);
					$wait = 1;
					$expected = '';
				}else {
					$tosend = $arr[$i]['command'];
					$wait = $arr[$i]['wait'];
					$expected = $arr[$i]['expected'];
					$i++;
				}
				$pin_entered=false;
				if($pin.chr(26)==$tosend){
					$pin_entered = true;
				}
				$serial1->sendMessage($tosend,$wait,$unq);
				$out = trim($serial1->readPort(0,$unq));
				$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
				$try=0;
	
				while($match['status'] !== 'success' && $try<=10){
					$out.= trim($serial1->readPort(2,$unq));
					$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
					//logData("log_$devid.txt","template match::".json_encode($match));
					$try++;
				}
	
				$out1 .= $out;
				if($match['status'] === 'success' && $again){
					$option = $this->findIndex($match['vars']['2'],$amount."=",'USSD');
					if($option!==false){
						$found = true;
						$again = false;
					}else {
						if(strpos($out,"* More")!==false){
							$option = "*";
							$found = true;
							$again = true;
						}else {
							$again = false;
							$found =false;
						}
					}
				}else {
					$found = false;
				}
				//$match['status'] = 'failure';
				if(!$pin_entered && $match['status'] !== 'success'){
					$serial1->sendMessage(chr(13),1,$unq);
					$serial1->sendMessage(chr(26),1,$unq);
					$ou.= trim($serial1->readPort(0,$unq));
					$match = matchTemplate($ou,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
					$try=0;
					
					while($match['status'] !== 'success' && $try<=10){
						$ou.= trim($serial1->readPort(0,$unq));
						$match = matchTemplate($ou,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
						//logData("log_$devid.txt","template match::".json_encode($match));
						$try++;
					}
					$out.=$ou;
					$serial1->deviceClose();
					
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['out'] = $out1;
					$result['msgs']="ussd response not found.".$out;
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					return $result;
				}
	
				if(strpos($out,'Sorry! This number is not registered as an airtel retailer number')!==false){
					$result['code'] = "Sorry! This number is not registered as an airtel retailer number";
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['stop']=true;
					$result['email']=true;
					$serial1->deviceClose();
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					$kk = mysql_query("UPDATE devices set recharge_method = IF(type=3,1,3) where par_bal=$devid");
					if(!$kk){
						logData("log_$devid.txt","quey not executed ".mysql_error());
					}else {
						logData("log_$devid.txt","Q:UPDATE devices set recharge_method = IF(type=3,1,3) where par_bal=$devid");
					}
					return $result;
				}
	
				//$expected = $match['vars']['2'];
				if(!empty($expected)){
					if(strpos($match['vars']['2'],$expected) === false){
						$serial1->sendMessage(chr(13),1,$unq);
						$serial1->sendMessage(chr(26),1,$unq);
						$ou.= trim($serial1->readPort(0,$unq));
						$match = matchTemplate($ou,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
						$try=0;
					
						while($match['status'] !== 'success' && $try<=10){
							$ou.= trim($serial1->readPort(0,$unq));
							$match = matchTemplate($ou,'@__123__@CUSD: @__123__@,"@__123__@",15@__123__@');
							//logData("log_$devid.txt","template match::".json_encode($match));
							$try++;
						}
						$out.=$ou;
						$serial1->deviceClose();
						$result = array();
						$result['status'] = 'failure';
						$result['reset']=false;
						$result['msgs']="wrong recharge";
						//logData("log_$devid.txt","final recharge response::".json_encode($result));
						return $result;
					}
				}
			}
			$result['status']='success';
			$result['out'] = $out1;
			$result['msgs'] = $out;
			return $result;
		}else{
			$result['status'] = 'failure';
			$result['reset']=true;
			$result['msgs']="Serial port not working.";
			//logData("log_$devid.txt","final recharge response::".json_encode($result));
			return $result;
		}
	}
	
	function getSale($device,$opr,$type,$pin,$scid=null,$devid=null){
		$unq = $device . "-" .time();
		$device = $device - 1;
		$type = 1;
		$serial = new phpSerial();
		$devicePos = $serial->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos){
			//$serial->confBaudRate(9600);
			$serial->confBaudRate(115200);
			$serial->confParity("none");
			$serial->confCharacterLength(8);
			$serial->confStopBits(1);
			$serial->confFlowControl("none");

			//Then we need to open it
			$serial->deviceOpen();
			$out = $this->checkSignalStrength($serial,$unq);
			$ret = true;
			logData("log_$devid.txt","inside check sale with signal strength at $devid is $out",$unq);

			$scid_curr = $this->getSCID($device+1,$serial,$unq,$devid);
			//logData("log_$devid.txt","inside recharge expected scid=$scid and currunt scid=$scid_curr ");
			if($scid !== $scid_curr)$ret = false;
			if($ret){
				$ret = true;
				$commands = $this->getCommands($opr,"sale_$type",null,null,$pin);
				$index = 2;
				foreach($commands as $command){
					$tosend = $command['command'];
					$tosend = str_replace('$index',$index,$tosend);
					$expected_reply = $command['reply'];
					$wait = $command['wait'];
					echo "Sending Command: $tosend\n";
					$serial->sendMessage("$tosend\r",$wait,$unq);
					$exp = $serial->readPort(0,$unq);

					$trials = 0;
					$command_ret = false;
					if(!empty($expected_reply)){
						while(!$command_ret && $trials < 10){
							echo "Reply: $exp\n";
							$pos = strpos($exp,$expected_reply);
							if($pos !== false){
								$command_ret = true;
								$aa = $this->findIndex($exp,$expected_reply,'APP');
								if($aa!==false)
									$index=$aa;
								break;
							}
							usleep($wait*1000000);
							$exp .= $serial->readPort(0,$unq);
							$trials++;
						}
					}
					else {
						$command_ret = true;
					}
					if(!$command_ret){
						$ret = false;
						break;
					}
				}
				if($ret){
					logData("log_$devid.txt","final output get sale ::".json_encode(),$unq);
					$serial->deviceClose();
					return array('status' => 'success');
				}else{
					$serial->deviceClose();
					return array('status' => 'failure','out' => 'Process not completed');
				}
			}else{
				logData("log_$devid.txt","final output get sale :: SIM NOT Working",$unq);
				$serial->deviceClose();
				return array('status' => 'failure','out' => 'SIM Not Working','reset' => true);
			}
		}else{
			return array('status' => 'failure','out' => 'port does not exist','reset' => true);
		}
	}
	
	function mtnlRecharge($serial1,$device,$opr,$mobile,$amount,$pin,$unq=null,$devid=null){
		$arr = array(
					'1' => array('command'=>"AT+CUSD=1,\"*555#".$pin."#\",15\r",'wait'=>2,'expected'=>'ETOPUP'),
					'2' => array('command'=>"1".chr(26),'wait'=>2,'expected'=>'ENTER MOBILE NUMBER'),
					'3' => array('command'=>$mobile.chr(26),'wait'=>2,'expected'=>'ENTER AMOUNT'),
					'4' => array('command'=>$amount.chr(26),'wait'=>2,'expected'=>'PLAN VOUCHER'),
					'5' => array('command'=>"2".chr(26),'wait'=>2,'expected'=>"CONFIRM THE TOPUP OF Rs.$amount TO $mobile"),
					'6' => array('command'=>"1".chr(26),'wait'=>2,'expected'=>'')
			);
			//"Your TopUp request for $mobile has been registered on @__123__@. Please await confirmation  Ref No.@__123__@"
			foreach($arr as $key => $comm){
				if($key==5 && $opr==31){
					$comm['command']="1".chr(26);
					$comm['expected']= "CONFIRM THE RECHARGE OF Rs.$amount TO $mobile";
				}
				$serial1->sendMessage($comm['command'],$comm['wait'],$unq);
				$out = trim($serial1->readPort(0,$unq));
				$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",0@__123__@');
				$try=0;
	
				while($match['status'] != 'success' && $try<=10){
					$out.= trim($serial1->readPort(2,$unq));
					$match = matchTemplate($out,'@__123__@CUSD: @__123__@,"@__123__@",0@__123__@');
					//logData("log_$devid.txt","template match::".json_encode($match));
					$try++;
				}
				$out1 .= $out;
	
				if(($key != '6' || $key != '5') && $match['status'] != 'success'){
					$serial1->sendMessage("2".chr(27),1,$unq);
					$serial1->deviceClose();
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['msgs']="ussd response not found.";
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					return $result;
				}
	
				if(strpos($out,'Sorry! This number is not registered as an airtel retailer number')!==false){
					$result['code'] = "Sorry! This number is not registered as an airtel retailer number";
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['stop']=true;
					$result['email']=true;
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					return $result;
				}
	
				$expected = $match['vars']['2'];
				if(!empty($comm['expected'])){
					if(strpos($expected,$comm['expected']) === false){
						$serial1->sendMessage(chr(27),1,$unq);
						$serial1->deviceClose();
						$result = array();
						$result['status'] = 'failure';
						$result['reset']=false;
						$result['msgs']="wrong recharge";
						//logData("log_$devid.txt","final recharge response::".json_encode($result));
						return $result;
					}
				}
			}
	
			$result = array();
			$result['status']='success';
			$result['msgs']=$out1;
			$serial1->deviceClose();
			//logData("log_$devid.txt","final recharge response::".json_encode($result));
			return $result;
	}
	
	function USSDRecharge($device,$opr,$mobile,$amount,$pin,$unq=null,$devid=null){
		$serial1 = new phpSerial();
		$devicePos1 = $serial1->deviceSet("/dev/".PORT.$device,$devid); //usb2 is 9833032643
		if($devicePos1){
			$serial1->confBaudRate(115200);
			$serial1->confParity("none");
			$serial1->confCharacterLength(8);
			$serial1->confStopBits(1);
			$serial1->confFlowControl("none");
			$serial1->deviceOpen();
	
			$arr = $this->$getUSSDCommands($opr,$type,$mobile,$amount,$pin,$param=null);
			$template = array(
					 '3' => '@__123__@CUSD: @__123__@,"@__123__@",15@__123__@',
					'30' => '@__123__@CUSD: @__123__@,"@__123__@",0@__123__@',
					'31' => '@__123__@CUSD: @__123__@,"@__123__@",0@__123__@'
			);
			foreach($arr as $key => $comm){
				$serial1->sendMessage($comm['command'],$comm['wait'],$unq);
				$out = trim($serial1->readPort(0,$unq));
				$match = matchTemplate($out,$template[$opr]);
				$try=0;
	
				while($match['status'] != 'success' && $try<=10){
					$out.= trim($serial1->readPort(2,$unq));
					$match = matchTemplate($out,$template[$opr]);
					//logData("log_$devid.txt","template match::".json_encode($match));
					$try++;
				}
				$out1 .= $out;
	
				if(($key != '6' || $key != '5') && $match['status'] != 'success'){
					$serial1->sendMessage(chr(27),1,$unq);
					$serial1->deviceClose();
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['msgs']="ussd response not found.";
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					return $result;
				}
	
				if(strpos($out,'Sorry! This number is not registered as an airtel retailer number')!==false){
					$result['code'] = "Sorry! This number is not registered as an airtel retailer number";
					$result = array();
					$result['status'] = 'failure';
					$result['reset']=false;
					$result['stop']=true;
					$result['email']=true;
					//logData("log_$devid.txt","final recharge response::".json_encode($result));
					return $result;
				}
	
				$expected = $match['vars']['2'];
				if(!empty($comm['expected'])){
					if(strpos($expected,$comm['expected']) === false){
						$serial1->sendMessage(chr(27),1,$unq);
						$serial1->deviceClose();
						$result = array();
						$result['status'] = 'failure';
						$result['reset']=false;
						$result['msgs']="wrong recharge";
						//logData("log_$devid.txt","final recharge response::".json_encode($result));
						return $result;
					}
				}
			}
	
			$result = array();
			$result['status']='success';
			$result['msgs']=$out1;
			$serial1->deviceClose();
			//logData("log_$devid.txt","final recharge response::".json_encode($result));
			return $result;
		}else{
			$result['status'] = 'failure';
			$result['reset']=true;
			$result['msgs']="Serial port not working.";
			//logData("log_$devid.txt","final recharge response::".json_encode($result));
			return $result;
		}
	}
	
	
	function getUSSDCommands($operator,$type,$mobile,$amount,$pin,$param=null){
		$commands = array(
				
				'3' => array(
						'1' => array('command'=>"AT+CUSD=1,\"*122*".$mobile."#\",15\r",'wait'=>2,'expected'=>''),
						'2' => array('command'=>$amount.chr(26),'wait'=>2,'expected'=>"Enter MPIN to confirm Recharge of Rs.$amount to mobile number:$mobile"),
						'3' => array('command'=>$pin.chr(26),'wait'=>2,'expected'=>'')
					),
		
				'30' => array(
					'1' => array('command'=>"AT+CUSD=1,\"*555#".$pin."#\",15\r",'wait'=>2,'expected'=>'ETOPUP'),
					'2' => array('command'=>"1".chr(26),'wait'=>2,'expected'=>'ENTER MOBILE NUMBER'),
					'3' => array('command'=>$mobile.chr(26),'wait'=>2,'expected'=>'ENTER AMOUNT'),
					'4' => array('command'=>$amount.chr(26),'wait'=>2,'expected'=>'PLAN VOUCHER'),
					'5' => array('command'=>"2".chr(26),'wait'=>2,'expected'=>"CONFIRM THE TOPUP OF Rs.$amount TO $mobile"),
					'6' => array('command'=>"1".chr(26),'wait'=>2,'expected'=>'')
					),
				
				'31' => array(
					'1' => array('command'=>"AT+CUSD=1,\"*555#".$pin."#\",15\r",'wait'=>2,'expected'=>'ETOPUP'),
					'2' => array('command'=>"1".chr(26),'wait'=>2,'expected'=>'ENTER MOBILE NUMBER'),
					'3' => array('command'=>$mobile.chr(26),'wait'=>2,'expected'=>'ENTER AMOUNT'),
					'4' => array('command'=>$amount.chr(26),'wait'=>2,'expected'=>'PLAN VOUCHER'),
					'5' => array('command'=>"1".chr(26),'wait'=>2,'expected'=> "CONFIRM THE RECHARGE OF Rs.$amount TO $mobile"),
					'6' => array('command'=>"1".chr(26),'wait'=>2,'expected'=>'')
					),
			);
		
		if(empty($commands[$operator])) {
			logData('ussdcommands.txt',"commands not found::$operator::$mobile::$amount");
		}
		else return $commands[$operator];
	}
	
	
	function getCommands($operator,$type,$mobile,$amount,$pin,$param=null){
		//$type = 1 means normal recharge, $type =2 means special recharge
		$amount = intval($amount);
		if($operator==4 && $amount<10)
			$amount="0".$amount;
			$commands = array(
				'1' => array(
					'operator' => 'AIRCEL',
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 33', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 2),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'My Reports', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 36', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Stock Balance', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter your MPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 2),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'sale_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'My Reports', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 36', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Daily Report', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter your MPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 2),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
				),
				'2' => array(
					'operator' => 'AIRTEL',
						'1' => array(
								array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=37', 'reply' => 'Easy Charge', 'wait' => 0.3),
								array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
								array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.5),
								array('command' => 'AT^SSTGI=36', 'reply' => 'Recharge', 'wait' => 0.3),
								array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 3),
								array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Customer Mobile No.', 'wait' => 0.3),
								array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.5),
								array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Amount', 'wait' => 0.3),
								array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.5),
								array('command' => 'AT^SSTGI=35', 'reply' => 'Enter your MPIN', 'wait' => 0.3),
								array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.5),
								array('command' => 'AT^SSTGI=33', 'reply' => "Pls confirm Recharge of Rs. $amount to Mobile Number: $mobile", 'wait' => 0.3),
								array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 2),
								array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
								array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' =>2)
						),
						'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Easy Charge', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Current Balance', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter your MPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'bal_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Current Balance', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter your MPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'bal_3' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,13', 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),*/
				),
				'3' => array(
					'operator' => 'BSNL',
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Recharge-Topup', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Customer Mobile No.', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Amount', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'TOPUP', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter mPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => "Confirm TOPUP of Rs $amount for Customer Mobile nos. $mobile", 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Yes', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),*/
						'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Inventory', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Check Stock', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter mPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'bal_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)*/
				),
				'4' => array(
					'operator' => 'idea',
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid TopUp', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Recharge', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Customer Mobile No', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Confirm Mobile No', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Recharge Amount', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Confirm Amount', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'PIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.8),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.8),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 4),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),*/
						'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid TopUp', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Stock Balance', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'PIN', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'bal_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,4', 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),*/
						'sale_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid TopUp', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Daily Report', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'PIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'sale_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,5', 'reply' => '^SSTN: 35', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),*/
				),
				'30' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Recharge,ETOPUP', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'TYPE', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Customer Mobile No.', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Amount', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'TOPUP', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter mPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => "Confirm TOPUP of Rs $amount for Customer Mobile nos. $mobile", 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Yes', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
					),
						/*'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,3', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'3' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'4' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,17', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'5' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,11', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'6' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,9', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),								
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),								
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => '', 'wait' => 2),
						),
						'7' => array(
								array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=211,0,4', 'reply' => '^SSTN: 36', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
								array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
								array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
								array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 3),
								array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
								array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),*/
						'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Recharge,ETOPUP', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'INVENTORY', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Check Stock', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter mPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'bal_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,3', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'bal_3' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'bal_4' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,17', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'bal_5' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,11', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'bal_6' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,9', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => '', 'wait' => 2),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => 'OK', 'wait' => 0.3),
						),
						'bal_7' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,4', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)*/
				),
				'31' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Recharge,ETOPUP', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'TYPE', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Customer Mobile No.', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Amount', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'RECHARGE', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter mPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => "Confirm RECHARGE of Rs $amount for Customer Mobile nos. $mobile", 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Yes', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,3', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'3' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'4' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,17', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'5' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,11', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'6' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,9', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => '', 'wait' => 2),
						)*/
				),
				'34' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Recharge-Topup', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Customer Mobile No.', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Amount', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'RECHARGE', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter mPIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => "Confirm RECHARGE of Rs $amount for Customer Mobile nos. $mobile", 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Yes', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 19', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)*/
				),
				'16' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Airtel DTH', 'wait' => 1),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'DTHRecharge', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Customer ID', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$param, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Amount', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter Alternate Mobile Number', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter your MPIN', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 1),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 4),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$param, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 1),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 4),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'3' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,13', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$param, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 1),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 4),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 5)
						),*/
						'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Airtel DTH', 'wait' => 0.5),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Administration', 'wait' => 0.5),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Current Balance', 'wait' => 0.5),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Enter your MPIN', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						/*'bal_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'bal_3' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,13', 'reply' => '^SSTN: 36', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 36', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)*/
				),
				'9' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 1),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
						'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,5', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)
				),
				'27' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 1),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)
				),
				'11' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTR=211,0,5', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,," "', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 3),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTR=211,0,5', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,," "', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 7),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 5),
							array('command' => 'AT^SSTR=19,0', 'reply' => '', 'wait' => 5),
						),
					'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,5', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,5', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)
				),
				'29' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,5', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,"STV"', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 33', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 3),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)
				),
				'12' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Vcharge', 'wait' => 1),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Normal Vcharge', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Customer Mobile', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Customer Mobile', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'Vcharge value', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'VPIN', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=37', 'reply' => 'My Account', 'wait' => 0.5),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.5),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Stock Balance', 'wait' => 0.5),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 0.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'VPIN', 'wait' => 0.5),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)
				),
				'28' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,1', 'reply' => '^SSTN: 36', 'wait' => 1),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=36,0,2', 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 1),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						)
				),
				'15' => array(
					'1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,128', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 1),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					/*'2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,128', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,1', 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$mobile, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$amount, 'reply' => '^SSTN: 35', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=33', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=33,0', 'reply' => '^SSTN: 19', 'wait' => 1),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),*/
					'bal_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,128', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,3', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'bal_1_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,128', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,3', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'bal_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,129', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,3', 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'bal_2_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=211,0,129', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,3', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => 'OK', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'sale_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid Refill', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Daily Report', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 35', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'PIN', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'sale_1_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'Prepaid Refill', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,$index', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'Daily Report', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,$index', 'reply' => '^SSTN: 19', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
					),
					
					/*'sale_2' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,128', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,4', 'reply' => '^SSTN: 35', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=35', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=35,0,,'.$pin, 'reply' => '^SSTN: 19', 'wait' => 2),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
						),
					'sale_2_1' => array(
							array('command' => 'AT^SSTA=1,0', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=37', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=254,0,1', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=211,0,128', 'reply' => '^SSTN: 36', 'wait' => 0.3),
							array('command' => 'AT^SSTGI=36', 'reply' => 'OK', 'wait' => 0.3),
							array('command' => 'AT^SSTR=36,0,4', 'reply' => '^SSTN: 19', 'wait' => 1.5),
							array('command' => 'AT^SSTGI=19', 'reply' => 'OK', 'wait' => 1),
							array('command' => 'AT^SSTR=19,0', 'reply' => 'OK', 'wait' => 2)
					)*/
				),
				'5' => array(
					'1' => array(
							array('command' => 'AT+CUSD=1,*145#,15', 'reply' => '+CUSD:', 'wait' => 5),
							array('command' => 'AT+CUSD=1,1,15', 'reply' => '+CUSD:', 'wait' => 5),
						)
				),
		);
		if($operator == '10') $operator = '9';
		if($operator == '26') $operator = '2';
		if($operator == '15' && strpos($type,'bal') !== false){
			if(!empty($pin))$type .= "_1";
		}
		if($operator == '15' && strpos($type,'sale') !== false){
			if(empty($pin))$type .= "_1";
		}
		
		
		if(empty($commands[$operator][$type])) {
			if(strpos($type,'bal') !== false){
				return $commands[$operator]['bal_1'];
			}
			else return $commands[$operator]['1'];
		}
		else return $commands[$operator][$type];
	}
	
	function get_string_between($string, $start, $end){
		$string = " ".$string;
		$ini = strpos($string,$start);
		if ($ini == 0) return "";
		$ini += strlen($start);
		$len = strpos($string,$end,$ini) - $ini;
		return substr($string,$ini,$len);
	}
	
	function getSCID($device,$serial=null,$unq=null,$devid=null){
		logData("log_$devid.txt","inside scid of $devid",$unq);
		$out = $this->sendCommand($device,"AT^SCID",1,$serial,$unq,$devid);
		logData("log_$devid.txt","output of scid of $devid is $out",$unq);
		$msgCount = matchTemplate($out,"@__123__@^SCID: @__123__@\n@__123__@");
		if($msgCount['status'] == 'failure'){
			$msgCount = matchTemplate($out,"@__123__@^SCID: @__123__@\n");
		}
		
		if($msgCount['status'] == 'success') {
			return $msgCount['vars'][1];
		}
		else return '';
	}
	
	function hextostr($hex)
	{
		$str='';
		for ($i=0; $i < strlen($hex)-1; $i+=2)
		{
			$str .= chr(hexdec($hex[$i].$hex[$i+1]));
		}
		return $str;
	}
	
	function resetAll(){
		$data = mysql_query("SELECT distinct device_num FROM devices WHERE device_num != 0 AND machine_id = " . MACHINE_ID);
		
		if (!$data) {
		
			logData("recheck.txt","Could not run query:". mysql_error());
			logData("recheck.txt"," query:". "SELECT distinct device_num FROM devices WHERE device_num != 0 AND machine_id =".MACHINE_ID);
		}
		
		$nums = array();
		while($res = mysql_fetch_array($data)){
			$nums[] = $res['device_num'];
		}
		$ports = $this->findUSBDevices();
		$device_nums = array();
		
		logData('recheck.txt',"Existing ports:".json_encode($nums) . "::modem ports::".json_encode($ports));
		foreach($ports as $key => $val){
			if(trim($val)=="")continue;
			$i = intval($val) + 1;
			//logData("recheck.txt","Inside Foreach port:".$i);
			$unq = $i . "-" . time();
			if(!in_array($i,$nums)){
				$j=$i-1;
				$command = "cd /home/pay1/workspace/recharges/ && ./port $j";
				$kk = shell_exec($command);
				logData('portsetting.txt',"command::$command,output::$kk");
				$shell_query = "nohup php " . DOCUMENT_ROOT . "recharges/start.php simdetection $i  > /dev/null 2> /dev/null & echo $!";
				shell_exec($shell_query);
				logData('recheck.txt',"Detection process created port=$i");
			}
		}
	}
	
	function findUSBDevices(){
		/*$dmsegOP = shell_exec("dmesg | grep ttyUSB");

		$arr = array();
		$arr = explode("\n", $dmsegOP);
		$op = array();
		foreach ($arr as $ele){
			$template = "@__var1__@ usb @__ext1__@-@__dev__@:@__var2__@ USB Serial Device converter now attached to ttyUSB@__port__@";
			$str = trim($ele);
			$varArr = matchTemplate1($str, $template,$varStart="@__",$varEnd="__@");

			if(!empty($varArr) && !empty($varArr['vars']['dev']) ){
				if(empty($varArr['vars']['dev'])) continue;

				if(empty($op[$varArr['vars']['dev']]) || !in_array($varArr['vars']['port'],$op[$varArr['vars']['dev']])){
					$indx = $varArr['vars']['ext1']."-".$varArr['vars']['dev'];
                    $op[$indx][] = $varArr['vars']['port'];
				}
			}
		}
		ksort($op);
		 
		$portArr = $op;

		$lsusbOP = shell_exec("lsusb");
		$arrLSUSB = array();
		$arrLSUSB = explode("\n", $lsusbOP);

		$devArr = array();
		$bus = array();
		foreach ($arrLSUSB as $ele){
			$template = "Bus @__modem__@ Device @__dev__@: ID 0403:6011@__var__@";
			$str = trim($ele);
			$varArr = matchTemplate1($str, $template,$varStart="@__",$varEnd="__@");
			 
			if(!empty($varArr)){
				if(empty($varArr['vars']['dev'])) continue;
				if(!in_array($varArr['vars']['modem'].$varArr['vars']['dev'],$devArr)){
					$devArr[] = $varArr['vars']['modem'].$varArr['vars']['dev'];
					$bus[$varArr['vars']['modem'].$varArr['vars']['dev']]['bus'] = $varArr['vars']['modem'];
					$bus[$varArr['vars']['modem'].$varArr['vars']['dev']]['device'] = $varArr['vars']['dev'];
				}
			}
		}
		sort($devArr);
		
		$final = array();
		$i = 0;
		foreach($portArr as $key=>$pArr){
			foreach($pArr as $port){
				$final[$port]['device'] = trim($bus[$devArr[$i]]['device']);
				$final[$port]['bus'] = trim($bus[$devArr[$i]]['bus']);
			}
			$i++;
		}*/
		$ports = shell_exec("ls -l /dev | grep '".PORT."' | awk '{print $10}' | awk -F'".PORT."' '{print $2}' | sort -n");
		$ports = explode("\n",$ports);
		$ports = array_unique($ports);
		/*foreach($ports as $port){
			if($port != '' && !isset($final[$port])){
				$final[$port]['device'] = '';
				$final[$port]['bus'] = '';
			}
		}*/
		
		return $ports;
	}
}
?>