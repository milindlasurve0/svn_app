#!/bin/bash

lastmin=$(date --date="1 MINUTE ago" +%Y-%m-%d" "%H:%M)
lcount=$(grep "$lastmin" /mnt/logs/redis.txt  | grep -i "Unique" | wc -l)
CUR_PROC_CNT=$(($(ps -ef | grep redis_request_processor | wc -l) - 1 ))
CUR_PROC_CNT_PREV=$(($(ps -ef | grep redis_prev_requests_processor | wc -l) - 1 ))
EXP_PROC_CNT=$(( ( $lcount / 15 ) + 1 ))


if [ $EXP_PROC_CNT -lt 4 ];
then
    EXP_PROC_CNT=4
fi

if [ $CUR_PROC_CNT_PREV -lt 1 ];
then
  START_PROC=`nohup php /home/pay1/workspace/recharges/redis_prev_requests_processor.php > /dev/null 2> /dev/null & echo $!`
fi

if [ $CUR_PROC_CNT -lt $EXP_PROC_CNT ];
then
  while [ $CUR_PROC_CNT -lt $EXP_PROC_CNT ]
  do
  		START_PROC=`nohup php /home/pay1/workspace/recharges/redis_request_processor.php > /dev/null 2> /dev/null & echo $!`
  		CUR_PROC_CNT=$(( $CUR_PROC_CNT + 1 ))
  		echo "Increased current process to : $CUR_PROC_CNT"
  done
else
  echo "current process cnt : $CUR_PROC_CNT || expected process cnt : $EXP_PROC_CNT "
fi

for num in $(grep "$lastmin" /mnt/logs/redis_time.txt |awk -F' : ' '{ print $2"|"$4 }' | sed 's/|| start_time//g' | sed 's/ || request data//g' | awk -F'|' '{ print $2 - $1}'); 
do 
	lsum=$(($lsum + $num)); 
	lcnt=$(($lcnt + 1)); 
done; 

req_process_time=$(( ($lsum / $lcnt) + 1 ))

if [ $req_process_time -gt 6 ];
then
	EMAILTO='lalit@mindsarray.com'
	VENDOR_NAME=`cat /home/pay1/workspace/recharges/sysconfig.php | grep -i "VENDOR_NAME"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep VENDOR_NAME | sed 's/VENDOR_NAME//g'`
	MAIL_SUBJECT="Modem process time increased"
	MAIL_BODY='Vendor:: ('$VENDOR_NAME')<br/>  <br/> Average request processing time increased to ( '$req_process_time' )'
        if [ $req_process_time -gt 10 ]
        then            
            EMAILTO='chetan@mindsarray.com'
            MAIL_SUBJECT="Modem internet too slow"
            MAIL_BODY='Vendor:: ('$VENDOR_NAME')<br/>  <br/> Modem internet too slow. Average request processing time increased to ( '$req_process_time' )'
        fi
	MAIL_URL='http://www.smstadka.com/users/sendMsgMails?'
	URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
	wget -O/dev/null "$URL"
fi

