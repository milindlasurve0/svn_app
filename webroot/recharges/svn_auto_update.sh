#!/bin/bash

#-------- get into codes folder
cd /home/pay1/workspace/codes

#ssh -o StrictHostKeyChecking=no mysql.ddns.net 'ifconfig eth0'
rm -rf /root/.ssh/known_hosts
ssh -o StrictHostKeyChecking=no mysqlpay1server.ddns.net 'ifconfig eth0'

DB_HOST=`cat ../recharges/sysconfig.php | grep -i "DB_"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep DB_HOST | sed 's/DB_HOST//g'`
DB_USER=`cat ../recharges/sysconfig.php | grep -i "DB_"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep DB_USER | sed 's/DB_USER//g'`
DB_DB=`cat ../recharges/sysconfig.php | grep -i "DB_"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep DB_DB | sed 's/DB_DB//g'`
DB_PASS=`cat ../recharges/sysconfig.php | grep -i "DB_"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep DB_PASS | sed 's/DB_PASS//g'`
VENDOR_NAME=`cat ../recharges/sysconfig.php | grep -i "VENDOR_NAME"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep VENDOR_NAME | sed 's/VENDOR_NAME//g'`

OTHER_DATA=""

[ -e /etc/init.d/memcached ] && MEM_FLAG="Y" || MEM_FLAG="N"

if [ $MEM_FLAG = "N" ]:
then
    sudo apt-get update && sudo apt-get install --yes --force-yes memcached php5-memcached php-pear build-essential && printf "\n" | pecl install memcache && echo "extension=memcache.so" | sudo tee /etc/php5/conf.d/memcache.ini
    OTHER_DATA=`ps -ef | grep "mem" | grep -v "color" | sed 's/$/<br\/>/g' | tr -d "\n" && echo "in memcache.ini <br\>" && cat /etc/php5/conf.d/memcache.ini`
fi

#sudo apt-get install php5-memcached
#sudo apt-get install php-pear
#sudo apt-get install build-essential
#sudo pecl install memcache
#echo "extension=memcache.so" | sudo tee /etc/php5/conf.d/memcache.ini 

#------- get all updated file list
flist=`svn status -u | grep "*" | awk '{ print $NF"  "}'`

binding_file_exist=`echo $flist | grep "chk_binding" | wc -l`

create_key_file_exist=`echo $flist | grep "create_key_file.sh" | wc -l`

fexist=`echo $flist | grep "svn_auto_update.sh" | wc -l`
echo "value for svnupdate is $fexist"

fupdate=`echo $flist  | tr -d "\n" | wc -c`
if [ $fupdate -lt 1 ];
then
   exit 0
fi


if [ $fexist -gt 0 ];
then
   svn up svn_auto_update.sh
   echo "svn update is complete here\nCalling svn update again"
   cp -f /home/pay1/workspace/codes/svn_auto_update.sh /home/pay1/workspace/recharges/svn_auto_update.sh
   nohup sh /home/pay1/workspace/codes/svn_auto_update.sh > /dev/null 2> /dev/null & echo $!
   OTHER_DATA_LEN=`echo $OTHER_DATA | wc -c`
   if [ $OTHER_DATA_LEN -lt 2 ]:
   then
       exit 0
   fi
fi


svn_log="$( ( `svn up`) 2>&1 )"
echo "Code is updated in codes folder"
#--------------------------------
for file in $flist
do
  if [ "$file" = "changes.sql" ];
  then
    echo " updating database here" 
    echo "mysql -u$DB_USER -h $DB_HOST  $DB_DB -p$DB_PASS < $file"
    mysql_log="$( ( `mysql -u$DB_USER -h $DB_HOST  $DB_DB -p$DB_PASS < $file`) 2>&1 )"
  fi
done
if [ -f /mnt/logs ];
then
   chmod 777 -R /mnt/logs
else
   mkdir /mnt/logs
   chmod 777 -R /mnt/logs
fi

echo "copying code from codes folder to recharges folder"
cp -f /home/pay1/workspace/recharges/sysconfig.php /home/pay1/workspace/codes/.
cp -rf /home/pay1/workspace/codes/* /home/pay1/workspace/recharges/.

echo "==== Setting premission of recharge folder ====\n"
`chmod 700 -R ../../workspace`

echo "==== Restarting the scripts ====\n"
cd /home/pay1/workspace/
nohup php /home/pay1/workspace/recharges/restart.php > /dev/null 2> /dev/null & echo $!

## security file check and changes 

KEY_FILE="kAC9c84EqTFxc6"

# check if security key file exists 
[ -e $KEY_FILE ] && KF_FLAG="Y" || KF_FLAG="N"

# check if security key creator script exists
[ -e /home/pay1/workspace/recharges/create_key_file.sh ] && CF_FLAG="Y" || CF_FLAG="N"

# check if process file exist
[ -e /etc/init.d/chk_binding ] && PF_FLAG="Y" || PF_FLAG="N"

# if we need to recreate the key file
if [ $create_key_file_exist -gt 0 ]:
then
  KF_FLAG="N"
fi

if [ $CF_FLAG = "Y" ]:
then
    if [ $KF_FLAG = "N" ]:
    then
       sh /home/pay1/workspace/recharges/create_key_file.sh $KEY_FILE
    fi

    if [ $binding_file_exist -gt 0 ]:
    then
        if [ $PF_FLAG = "N" ]:
        then
            cp -f /home/pay1/workspace/recharges/chk_binding /etc/init.d/chk_binding
            `[ -e /etc/rc0.d/K20chk_binding ] && echo "already exists" || ln -s /etc/init.d/chk_binding /etc/rc0.d/K20chk_binding`
            `[ -e /etc/rc6.d/K20chk_binding ] && echo "already exists" || ln -s /etc/init.d/chk_binding /etc/rc6.d/K20chk_binding`            
            GREP_CMD_COUNT=`grep "chk_binding" /etc/rc.local | wc -l`
            if [ $GREP_CMD_COUNT -lt 1 ];
            then
               STARTUPCMD="\/etc\/init\.d\/chk_binding start"
               `sed -i "s/^exit 0/$( echo $STARTUPCMD)\n&/" /etc/rc.local`            
            fi            
        else
            `rsync /home/pay1/workspace/recharges/chk_binding /etc/init.d/chk_binding`
        fi
    fi 
fi



EMAILTO='ashish@mindsarray.com,lalit@mindsarray.com,nandan@mindsarray.com,chetan@mindsarray.com'
MAIL_URL='http://www.smstadka.com/users/sendMsgMails?'

MAIL_SUBJECT="Modem code updated"
MAIL_BODY='Vendor:: ('$VENDOR_NAME')<br/> Mysql error: '$mysql_log' <br/> Svn error: '$svn_log' <br/> '$OTHER_DATA' '
URL="$MAIL_URL"mail_subject=$MAIL_SUBJECT"&"mail_body=$MAIL_BODY"&"emails=$EMAILTO
wget -O/dev/null "$URL"
