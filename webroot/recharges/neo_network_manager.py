# neo_newwork_manager.py
# This script is use to switch the network connection when internet is not working on one connection	
# This script need to be run with root privilege

#Requirements: sendmail / postfix, python
#Remove connection automatically from all the connections


from subprocess import Popen, PIPE, STDOUT
import subprocess
import time
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText



#
#	This Function is use to execute system command through python
#
def command(cmd):
	try:
		p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		output = p.stdout.read()
		return output
	except Exception, e:
		print str(e)

#
#	This function is a utility to send mail through python
#
def email(sender,receivers,message):
    smtpObj = smtplib.SMTP("localhost")
    smtpObj.sendmail(sender, receivers, message)
    smtpObj.quit()
    return 1	


#
#	Custom mails function to send html in mail body
#
def mailAlert (toIds, htmlText,subject=None):
	msg = MIMEMultipart('alternative')
	if(subject != None):
		msg['Subject'] = subject
	else:
		msg['Subject'] = "Alert"
		msg['From']    = "network_monitor_script@mindarray.com"
		msg['To']      = ', '.join(toIds)
	alert_mailFrom = "system@mindsarray.com"
	HTML_BODY = MIMEText(htmlText, 'html')
	msg.attach(HTML_BODY)
	email(alert_mailFrom, toIds, msg.as_string())


#
#	This Function is use to get count of active connection
#    
def active_connection_count():
	cmd = 'nmcli dev | grep  " connected" | wc -l'
	result = command(cmd).strip()
	return result

    
#
#	This Function is use to check for internet connectivity and data loss
#    
def internet_status():
	ATTEMPT = 3
	cmd = """ping -c %s 8.8.8.8 | egrep 'received' | awk -F', ' '{print $2}' | awk '{ print $1}'"""%(str(ATTEMPT))
	try:
		cmd_result = int(command(cmd).strip())
	except:
		cmd_result = 0
	print "ping result : "+str(cmd_result)
	if (int(ATTEMPT) == int(cmd_result)):
		result = True
	else:
		result = False
	return result


#
#	This Function is to connect to a specific network 
#
def open_connection(con):
	cmd = """nmcli -t con up id %s"""%str(con) 
	cmd_result = command(cmd).strip()


#
#	This Function is to close particular network connection
#
def close_connection(con):
	print "closing connection "+str(con)
	cmd = "ifconfig %s down"%str(con) 
	#print "command to close connection : "+str(cmd)
	cmd_result = command(cmd).strip()

#
#	This Function get currently connected connections
#	
def get_connected_con():
	print "getting connected connections"
	cmd = """nmcli dev | grep  " connected" | awk '{ print $1}'"""
	cmd_result = command(cmd).strip('\n').split('\n')
	return cmd_result

#
# This Function prevent multiple network connection
#
def control_multiple_connection():
	# set it to "eth0" if primary interface is LAN and if its "wifi" keep it eth1 or wlan0. check it using "ifconfig command"
	default_con1 = lan_con	
	# This is for connection through Data Card
	default_con2 = data_con
	con_list = get_connected_con
	print "total connection "+str(len(con_list))
	if(len(con_list) > 1):
		if (default_con1 not in con_list):
			default_con1 = default_con2
		print "default connection "+str(default_con1)	
		for con in con_list:
			if (con == default_con1):
				print "connection not to close "+str(con)
				pass
			else:
				print "connection to close "+str(con)
				close_connection(con)

#
#  This function provide mail content and other parameter to mail function
#
def sendmail(con=""):
	toIds = ['nandan@mindsarray.com','ashish@mindsarray.com'] # email ids to whom mail need to be triggered
	htmlText = "System is switching its network connection to : "+str(con)
	subject = "modem network switch alert"
	try:
		mailAlert (toIds, htmlText,subject=None)
	except:
		pass


#
# This is the main process which monitor and control connections
#
def main(con="",attempt=0):
	#Keep primary network interface name
	default_con1 = lan_name 
	#keep secondary network interface name
	default_con2 = data_name 
	default_con = ""
	con_count = int(active_connection_count())
	if(con == ""):
		default_con = default_con1
	else:
		default_con = con
	print "started"
	print "current connection : "+str(con)
	if(con_count > 1):
		print "> 1 | total connection : "+str(con_count)
		control_multiple_connection()
	elif(con_count < 1):
		print "< 1 | total connection "+str(con_count)
		print "opening connection : "+str(default_con)
		open_connection(default_con)
		print "going to sleep ..."
		time.sleep(5)
		print "sleep completed ..."
	if(internet_status()):
		print "Internet working proper on "+str(default_con)
		exit(0)
	else:
		if(default_con == default_con1):
			default_con = default_con2
		elif(default_con == default_con2):
			default_con = default_con1	
	#if internet is not working properly repeate process	
	attempt = int(attempt) + 1	
	if(int(attempt) > 0):
		sendmail(default_con)
	close_connection(get_connected_con()[0])
	main(default_con,attempt)

#
#	This is the starting point of script
#		
if ( __name__ == "__main__"):
	global lan_con, data_con, lan_name, data_name
	lan_con = "eth0"
	data_con = "pppo"
	lan_name = "LAN"
	data_name = "DATA"
	main()
			
