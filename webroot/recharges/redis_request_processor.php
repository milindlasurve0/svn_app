<?php
require("redis_manager.php");
require("config.php");

$rm = new redisManager();
$unique = rand(0,1000);

function addMemcache($key,$value,$expiry){
	if(MEMCACHE){
		global $memcacheObj;
		$val = $memcacheObj->add($key,$value,0,$expiry);
			
		if($val !== false){
			return true;
		}
		else return false;
	}
	else return -1;
}

function lockDevice($device_num,$ret=false,$from=null){
	if(MEMCACHE){
		global $memcacheObj;
		$val = $memcacheObj->add("device_$device_num",1,0,300);
		if($val !== false){
			logData('lockDevice.txt',"Device locked port number in memcached=$device_num,from $from");
			return true;
		}
	}
	else if(mysql_query("INSERT INTO lock_devices (device_num,machine_id) VALUES ($device_num,".MACHINE_ID.")")){
		logData('lockDevice.txt',"Device locked port number=$device_num");
		return true;
	}

	if($ret){
		return false;
	}
	else {
		exit;
	}
}

function lockRequest($request,$ret=false,$from=null){
	if(MEMCACHE){
		global $memcacheObj;
		$val = $memcacheObj->add("request_$request",1,0,300);
		if($val !== false){
			logData('lockRequest.txt',"Request locked in memcached=$request,by $from");
			return true;
		}
	}
	else {
		mysql_query("DELETE FROM lock_requests WHERE timestamp < DATE_SUB(NOW() , INTERVAL 180 SECOND)");
		if(mysql_query("INSERT INTO lock_requests (request_id) VALUES ($request)")){
			return true;
		}
	}

	if($ret){
		return false;
	}
	else {
		exit;
	}
}

function unlockDevice($device_num){
	if(MEMCACHE){
		global $memcacheObj;
		$memcacheObj->delete("device_$device_num");
		logData('lockDevice.txt',"Device unlocked port number in memcache=$device_num");
	}
	else {
		logData('lockDevice.txt',"Device unlocked port number=$device_num");
		mysql_query("DELETE FROM lock_devices WHERE (device_num = $device_num AND machine_id = ".MACHINE_ID.") OR timestamp < DATE_SUB(NOW() , INTERVAL 120 SECOND)");
	}
}

function getDevice($device_num,$ret=false){
	if(MEMCACHE){
		global $memcacheObj;
		$val = $memcacheObj->get("device_$device_num");
		if($val !== false){
			logData('lockDevice.txt',"Not able to get device number in memcached=$device_num");
			return $val;
		}
	}
}

function setMemcache($key,$value,$expiry,$ret=false){
	if(MEMCACHE){
		global $memcacheObj;
		$val = $memcacheObj->set($key,$value,0,$expiry);
		if($val !== false){
			logData('setMemcache.txt',"SETTING KEY::$key::VAL::$value::EXPIRY::$expiry");
			return $val;
		}
	}
}

function getMemcache($key){
	if(MEMCACHE){
		global $memcacheObj;
		$val = $memcacheObj->get($key);
		if($val !== false){
			logData('setMemcache.txt',"GETTING KEY::$key::VAL::$val");
			return $val;
		}
	}
}

function addRequest($oprId,$mobile,$amount,$type,$ref_id,$param,$circle=null){

	logData("intake.txt","inside addRequest function::$oprId::$ref_id");
	$b2c_flag = B2C_FLAG;
	if(empty($b2c_flag))$b2c_flag = 0;
	$buffer_amount=$amount;
	//if($oprId==2)$buffer_amount=500+$amount;
	//else $buffer_amount=$amount;
	if((date('H:i') > '23:57' || date('H:i') < '00:00') && $b2c_flag == 0){ //No transactions can be added after 23:45
		$ret = '1';
	}
	else if (VENDOR_ID == 31 && $oprId == 2 && $amount == 48){//modem bihar, Airtel 48rs txns are not possible
		$ret = '1';
	}
	else {

		if($oprId == 9 || $oprId == 10){
			$oprId = 9;
		}

		if($oprId == 18 && $amount >= 500) {
			$oprId = 181;
			$result = mysql_query("SELECT GROUP_CONCAT(par_bal) as ids FROM devices WHERE opr_id = $oprId AND state != " . STATE_STOP . " AND active_flag=1 AND recharge_flag = 1 AND stop_flag = 0 AND balance >= $buffer_amount");
			$request = mysql_fetch_array($result);
			$ids = $request['ids'];
			if(empty($ids)) $oprId = 18;
		}

		$result = mysql_query("SELECT GROUP_CONCAT( if(roaming_limit > 0, if(circle != '$circle', if(roaming_limit - roaming_today >= $amount,par_bal,''),par_bal),par_bal)) as ids, GROUP_CONCAT(if(`limit` > 0, if(`limit` - limit_today >= $amount,par_bal,''),par_bal)) as ids1,circle FROM devices WHERE opr_id = $oprId AND active_flag=1 AND recharge_flag = 1 AND stop_flag = 0 AND block = '0' AND state = ".STATE_READY." AND balance >= bal_range+$amount AND balance > $amount");
		if(!$result){
			logData("intake.txt","inside addRequest function::$oprId::$ref_id::mysql error ".mysql_error());
		}
		$request = mysql_fetch_array($result);
		$ids = trim($request['ids']);
		$ids1 = trim($request['ids1']);
		$local_circle = trim($request['circle']);

		if(!empty($ids1) && !empty($ids)){
			$arr1 = array_filter(explode(",",$ids));
			$arr2 = array_filter(explode(",",$ids1));
			$ids = implode(",",array_intersect($arr1,$arr2));
		}
		else {
			$ids = '';
		}

		$ids_working = trim($ids);
		$ids = explode(",",$ids);
		logData("intake.txt","inside addRequest function::$oprId::$ref_id::$ids_working");
		

		$possible = count($ids);
		$ret='2';
		if($possible > 0 && !empty($ids_working)){

			$result1 = mysql_query("SELECT * FROM devices WHERE id in ($ids_working) order by supplier_priority asc, balance desc");
			logData("intake.txt","inside addRequest function::$oprId::$ref_id::".json_encode($result1));
		
			while($row = mysql_fetch_assoc($result1)){
				$dup = $mobile;
				if(!empty($param)) $dup = $param;
				
				$mul = addMemcache("recharge_".$dup."_".$row['id'],1,1800);
				if($mul === false) {
					logData("intake.txt","$oprId::$ref_id:: case of duplicate recharge of same mobile from same sim within 30 mins:: cannot assign request to device ". $row['id']);
					continue;
				}
				$lock_dev = lockDevice($row['device_num'],true,'addRequest');
				if(!$lock_dev){
					logData("intake.txt","$oprId::$ref_id::Device was locked");
					$v = getDevice($row['device_num']);
					logData('intake.txt',"$oprId::$ref_id::device memcache value ::$v::device::".$row['device_num']);
					continue;
				}
				else {
					$ret = '1';
					$microtime = microtime(true)*10000;
					$ref_code =  "GM" . MACHINE_ID . $mobile . substr($microtime,-10);
					logData("intake.txt","inside addRequest function::$oprId::$ref_id::$ref_code");
					$kim = mysql_query("INSERT INTO requests (opr_id,mobile,amount,param,circle,type,ref_code,vendor_refid,timestamp,date) VALUES ($oprId,'$mobile',$amount,'$param','$circle',$type,'$ref_code','$ref_id',now(),curdate())");
					if($kim){
						$ret = $ref_code;
						$last_id = mysql_insert_id();
						$lock_req = lockRequest($last_id,true);
						if($lock_req && !empty($last_id)){
							$shell_query = "nohup php " . DOCUMENT_ROOT . "recharges/start.php recharge " . $row['id']. " ". $last_id." > /dev/null 2> /dev/null & echo $!";
							logData("intake.txt","$oprId::$ref_id::request id $last_id assigned to device ".  $row['id']);
							shell_exec($shell_query);
							
							$rechargeTime = date('Y-m-d H:i:s');
							
							setMemcache("rechargeTime_".$row['id'],$rechargeTime,86400);
							break;
						}
						else {
							unlockDevice($row['device_num']);
							logData("intake.txt","$oprId::$ref_id::request id $last_id is not assigned here");
							break;
						}
					}else{
						$ret = '2';
						unlockDevice($row['device_num']);
						logData("intake.txt","$oprId::$ref_id::$ref_code::request was not inserted here error::".mysql_error());
						getMails("(".VENDOR_NAME.") add Request","Dear Chetan, $oprId::$ref_id::$ref_code::request was not inserted into table error::".mysql_error(),'lalit@mindsarray.com,chetan@mindsarray.com','mail');
						break;
					}
				}
			}
		}

		logData("intake.txt",date('Y-m-d H:i:s')."::$oprId::$ref_id::Ids ::'".implode(",",$ids)."' :: return is $ret:: possible $possible:: circle $circle:: local circle $local_circle:: roaming count $roaming_count:: req count $req_count");
	}

	if(in_array($ret,array("1","2"))){
		//require_once "redis_manager.php";
		//$rm = new redisManager();
		global $rm;
		$rm->remove_txn($oprId,$ref_id);
		logData("intake.txt",date('Y-m-d H:i:s')."::$oprId::$ref_id::removing txn here");
	}
	return $ret;
}


while(true){
  try{
    $response = $rm->fetch_request();
  }catch(Exception $ex){
    var_dump($ex);
    logData("redis.txt",'Retrying to connect redis server');
    sleep(30);
    $rm = new redisManager();
    continue;
  }
  
  checkMysqlConn();
  
  if(is_null($response)){
    echo "going to sleep \n";
    usleep(100000);
    continue;
  }
  logData("redis.txt","Unique $unique | Request: ".$response);
  
  parse_str($response,$req_arr);
  $rtime = time();
  $response .= "&rtime=".$rtime; 
  if($rm->check_request_validity($req_arr['uuid'])){
      if($req_arr['query'] == 'recharge'){
      	logData("redis.txt",'inside recharge:: trying to add request here:: '. $response);
      	
      	$output = addRequest($req_arr['oprId'],$req_arr['mobile'],$req_arr['amount'],$req_arr['type'],$req_arr['transId'],$req_arr['param'],$req_arr['circle']);
		$procced_time = time();
      	logData("redis.txt",'inside recharge:: added request here:: '. $response . " with output $output");
      	
      	$data = array('uuid' => $req_arr['uuid'], 'response' => $output);
        
      	$response1 = $rm->send_response($data);
      	logData("redis.txt",'inside recharge:: sending response to redis'. $response);
      	logData("redis_time.txt","req_time : ".$req_arr['reqtime']."|| start_time : $rtime || end time : ". time()." || request data : ".$req_arr['transId']. " || process time : ".$procced_time ." || output : ".$output );
      	
      }
      else {
      	shell_exec('nohup php /home/pay1/workspace/recharges/redis_request_handler.php "'.$response.'" > /dev/null 2> /dev/null & echo $!');
  	  	logData("redis.txt",'nohup php redis_request_handler.php "'.$response.'" > /dev/null 2> /dev/null & echo $!');
      }
  }
  else {
  	logData("redis_expired.txt","Request: ".$response);
  }
}

?>
